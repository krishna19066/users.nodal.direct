<?php
include "admin-function.php";
checkUserLogin();
// include "cms_functions.php";
// user_locked();
// staff_restriction();
$inqdate = date('Y/m/d');
$customerId = $_SESSION['customerID'];
$welcomeCont = new welcomeData();
$enquiryData = new manageEnquiry();
$getAllEnquiry = $enquiryData->GetAllEnquiryWithCustomerId($customerId); // Get All Inquiry Data
$getTodatInquiry = $enquiryData->GetTodayInquiry($customerId, $inqdate); // Gett Today Inquiry Data
$req_inquiry = $enquiryData->GetRequestCallInquiry($customerId); // Get RequestCall Inquiry Data
$req_TodayInquiry = $enquiryData->GetTodayReqInquiry($customerId, $inqdate); // Get RequestCall Today Inquiry Data
$totalInquiry = count($getAllEnquiry);
$today_inquiry = count($getTodatInquiry);
$totalReqInquiry = count($req_inquiry);
$today_Req_inquiry = count($req_TodayInquiry);
?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php
        adminCss();
        ?>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
       <!-- <script src="assets/pages/scripts/dashboard.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL SCRIPTS -->
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <?php
                    // die;
                    // $q=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");	
                    // $custID = $_SESSION['customerID'];
                    ?>

                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> Admin Dashboard
                                <small>statistics, charts, recent events and reports</small>
                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Dashboard</span>
                        </li>
                    </ul>
                    <!--<div class="page-toolbar">
                            <i class="icon-calendar"></i>&nbsp;
                    <?php //echo date('d-M-Y'); ?>&nbsp;
                    </div>-->

                    <!-- END PAGE BAR -->
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered"  href="all-members.php">
                                <div class="display">
                                    <div class="number">

                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="<?php echo $totalInquiry; ?>">0</span>
                                            <small class="font-green-sharp"></small>
                                        </h3>
                                        <small>Total Inquiries</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only">Today's Inquiries :</span>
                                        </span>
                                    </div>
                                    <div class="status">

                                        <div class="status-title">Today's Inquiries : </div>
                                        <div class="status-number"><?php echo $today_inquiry; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">

                                        <h3 class="font-red-haze">
                                            <span data-counter="counterup" data-value="<?php echo $totalReqInquiry; ?>">0</span>
                                        </h3>
                                        <small>Call Back Requests</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-like"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
                                            <span class="sr-only">Today's Call Back :</span>
                                        </span>
                                    </div>
                                    <div class="status">

                                        <div class="status-title">Today's Call Back :</div>
                                        <div class="status-number"><?php echo $today_Req_inquiry; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <?php
                                        // $inq2 = "select count(s_no) from property_reviews where customerID='$custID'";
                                        // $inqquer2 = mysql_query($inq2);
                                        // while($row3 = mysql_fetch_array($inqquer2))
                                        // {
                                        // $tot2 = $row3['count(s_no)'];
                                        // }
                                        ?>
                                        <h3 class="font-purple-soft">
                                            <span data-counter="counterup" data-value="<?php // echo $tot2;              ?>"></span>
                                        </h3>
                                        <small>Total Reviews</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
                                            <span class="sr-only">56% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> change </div>
                                        <div class="status-number"> 57% </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <?php
// $signup_sel = "select count(s_no) from newsletter where customerID='$custID'";
// $signup_quer = mysql_query($signup_sel);
// while($signup_row = mysql_fetch_array($signup_quer))
// {
// $tot_signup = $signup_row['count(s_no)'];
// }
                                        ?>
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="<?php //echo $tot_signup;              ?>"></span>
                                        </h3>
                                        <small>Email Subscriptions</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-basket"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
                                            <span class="sr-only">Today's Subscriptions :</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <?php
// $inqdate2 = date('Y-m-d');
// $inq3 = "select count(s_no) from newsletter where recvDate='$inqdate2' and customerID='$custID'";
// $inqquer3 = mysql_query($inq3);
// while($row4 = mysql_fetch_array($inqquer3))
// {
// $tot3 = $row4['count(s_no)'];
// }
                                        ?>
                                        <div class="status-title">Today's Email Subscriptions : </div>
                                        <div class="status-number"><?php //echo $tot3;              ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <!--------------------------------------------------------New--------------------------------->
                 <!-- Step 1: Create the containing elements. -->

<section id="auth-button"></section>
<section id="timeline"></section>

<!-- Step 2: Load the library. -->

<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>

<script>
gapi.analytics.ready(function() {

  // Step 3: Authorize the user.

  var CLIENT_ID = '371313688050-6vvnhr28g7aj0fh96iqmka71hflk6egr.apps.googleusercontent.com';

  gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
    userInfoLabel:""
  });

  // Step 5: Create the timeline chart.
  var timeline = new gapi.analytics.googleCharts.DataChart({
    reportType: 'ga',
    query: {
      'dimensions': 'ga:date',
      'metrics': 'ga:sessions',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
     'ids': "ga:31405"
    },
    chart: {
      type: 'LINE',
      container: 'timeline'
    }
  });

  gapi.analytics.auth.on('success', function(response) {
    //hide the auth-button
    document.querySelector("#auth-button").style.display='none';
    
    timeline.execute();

  });

});
</script>
                    <!--------------------------------------------end--------------------------------------------->

                    <script type="text/javascript" src="http://www.google.com/jsapi"></script>

                    <script type="text/javascript">
                        google.load("search", "1");
                        google.load("jquery", "1.4.2");
                        google.load("jqueryui", "1.7.2");
                    </script>
                    <!-- END DASHBOARD STATS 1-->

                    <script>
                        (function (w, d, s, g, js, fs) {
                        g = w.gapi || (w.gapi = {});
                        g.analytics = {q: [], ready: function (f) {
                        this.q.push(f);
                        }};
                        js = d.createElement(s);
                        fs = d.getElementsByTagName(s)[0];
                        js.src = 'https://apis.google.com/js/platform.js';
                        fs.parentNode.insertBefore(js, fs);
                        js.onload = function () {
                        g.load('analytics');
                        };
                        }(window, document, 'script'));
                    </script>

                    <div class="row">
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Site Visits</span>
                                        <span class="caption-helper"><div id="embed-api-auth-container"></div></span>
                                    </div>
                                    <div class="actions">
                                        <button style="background:#32c5d2;color:white;width:100px;height:30px;border:none;border-radius:100px;"><a href="coming_soon.php" style="text-decoration:none;color:white;"> View All </a></button>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="embed-api-auth-container"></div>
                                    <div id="chart-container"></div>
                                    <div id="view-selector-container"></div>
                                </div>

                            </div>
                            <!-- END PORTLET-->
                        </div>

                        <script>
                            gapi.analytics.ready(function () {

                            /**
                             * Authorize the user immediately if the user has already granted access.
                             * If no access has been created, render an authorize button inside the
                             * element with the ID "embed-api-auth-container".
                             */
                            gapi.analytics.auth.authorize({
                            container: 'embed-api-auth-container',
                                    clientid: '371313688050-6vvnhr28g7aj0fh96iqmka71hflk6egr.apps.googleusercontent.com'
                            });
                            /**
                             * Create a new ViewSelector instance to be rendered inside of an
                             * element with the id "view-selector-container".
                             */
                            var viewSelector = new gapi.analytics.ViewSelector({
                            container: 'view-selector-container'
                            });
                            // Render the view selector to the page.
                            viewSelector.execute();
                            /**
                             * Create a new DataChart instance with the given query parameters
                             * and Google chart options. It will be rendered inside an element
                             * with the id "chart-container".
                             */
                            var dataChart = new gapi.analytics.googleCharts.DataChart({
                            query: {
                            metrics: 'ga:sessions',
                                    dimensions: 'ga:date',
                                    'start-date': '30daysAgo',
                                    'end-date': 'yesterday'
                            },
                                    chart: {
                                    container: 'chart-container',
                                            type: 'LINE',
                                            options: {
                                            width: '100%'
                                            }
                                    }
                            });
                            /**
                             * Render the dataChart on the page whenever a new view is selected.
                             */
                            viewSelector.on('change', function (ids) {
                            dataChart.set({query: {ids: ids}}).execute();
                            });
                            });
                        </script>

                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption ">
                                        <span class="caption-subject font-dark bold uppercase">Inquiries</span>
                                        <span class="caption-helper">monthly starts...</span>
                                    </div>
                                    <div class="actions">
                                        <a href="all-members.php" class="btn btn-circle green btn-outline btn-sm">
                                            <i class="fa fa-pencil"></i> View All 
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="dashboard_amchart_3" class="CSSAnimationChart">

                                        <?php
                                        $year_org = date('Y');
                                        // $jan = "select count(recvDate) from inquiry where year='$year_org' and month='01'";
                                        // $janquer = mysql_query($jan);

                                        $jan = $enquiryData->JanInquiry($customerId, $year_org);
                                        $jan_data = count($jan);
                                        $feb = $enquiryData->FebInquiry($customerId, $year_org);
                                        $feb_data = count($feb);
                                        $mar = $enquiryData->MarInquiry($customerId, $year_org);
                                        $mar_data = count($mar);
                                        $apr = $enquiryData->AprInquiry($customerId, $year_org);
                                        $apr_data = count($apr);
                                        $may = $enquiryData->MayInquiry($customerId, $year_org);
                                        $may_data = count($may);
                                        $jun = $enquiryData->JunInquiry($customerId, $year_org);
                                        $jun_data = count($jun);
                                        $jul = $enquiryData->JulInquiry($customerId, $year_org);
                                        $jul_data = count($jul);
                                        $aug = $enquiryData->AugInquiry($customerId, $year_org);
                                        $aug_data = count($aug);
                                        $sep = $enquiryData->SepInquiry($customerId, $year_org);
                                        $sep_data = count($sep);
                                        $oct = $enquiryData->OctInquiry($customerId, $year_org);
                                        $oct_data = count($oct);
                                        $nov = $enquiryData->NovInquiry($customerId, $year_org);
                                        $nov_data = count($nov);
                                        $dec = $enquiryData->DecInquiry($customerId, $year_org);
                                        $dec_data = count($dec);


                                        /*
                                          $mar = "select count(recvDate) from inquiry where year='$year_org' and month='03'";
                                          $marquer = mysql_query($mar);
                                          while ($marrow = mysql_fetch_array($marquer)) {
                                          $mar_data = $marrow['count(recvDate)'];
                                          }

                                          $apr = "select count(recvDate) from inquiry where year='$year_org' and month='04'";
                                          $aprquer = mysql_query($apr);
                                          while ($aprrow = mysql_fetch_array($aprquer)) {
                                          $apr_data = $aprrow['count(recvDate)'];
                                          }

                                          $may = "select count(recvDate) from inquiry where year='$year_org' and month='05'";
                                          $mayquer = mysql_query($may);
                                          while ($mayrow = mysql_fetch_array($mayquer)) {
                                          $may_data = $mayrow['count(recvDate)'];
                                          }

                                          $jun = "select count(recvDate) from inquiry where year='$year_org' and month='06'";
                                          $junquer = mysql_query($jun);
                                          while ($junrow = mysql_fetch_array($junquer)) {
                                          $jun_data = $junrow['count(recvDate)'];
                                          }

                                          $jul = "select count(recvDate) from inquiry where year='$year_org' and month='07'";
                                          $julquer = mysql_query($jul);
                                          while ($julrow = mysql_fetch_array($julquer)) {
                                          $jul_data = $junrow['count(recvDate)'];
                                          }

                                          $aug = "select count(recvDate) from inquiry where year='$year_org' and month='08'";
                                          $augquer = mysql_query($aug);
                                          while ($augrow = mysql_fetch_array($augquer)) {
                                          $aug_data = $augrow['count(recvDate)'];
                                          }

                                          $sep = "select count(recvDate) from inquiry where year='$year_org' and month='09'";
                                          $sepquer = mysql_query($sep);
                                          while ($seprow = mysql_fetch_array($sepquer)) {
                                          $sep_data = $seprow['count(recvDate)'];
                                          }

                                          $oct = "select count(recvDate) from inquiry where year='$year_org' and month='10'";
                                          $octquer = mysql_query($oct);
                                          while ($octrow = mysql_fetch_array($octquer)) {
                                          $oct_data = $octrow['count(recvDate)'];
                                          }

                                          $nov = "select count(recvDate) from inquiry where year='$year_org' and month='11'";
                                          $novquer = mysql_query($nov);
                                          while ($novrow = mysql_fetch_array($novquer)) {
                                          $nov_data = $novrow['count(recvDate)'];
                                          }

                                          $dec = "select count(recvDate) from inquiry where year='$year_org' and month='12'";
                                          $decquer = mysql_query($dec);
                                          while ($decrow = mysql_fetch_array($decquer)) {
                                          $dec_data = $decrow['count(recvDate)'];
                                          }

                                          /* $mon_org = date('m');
                                          $cou = 1;
                                          while($daterow = mysql_fetch_array($datequer))
                                          {
                                          $redate = $daterow['recvDate'];
                                          $redate_org = explode("-",$redate);
                                          $mon = $redate_org[1];
                                          $year = $redate_org[0];
                                          if($year==$year_org)
                                          {
                                          $jan =
                                          }

                                          } */
                                        $anb = "[
                                                                ['JAN', $jan_data],
                                                                ['FEB', $feb_data],
                                                                ['MAR', $mar_data],
                                                                ['APR', $apr_data],
                                                                ['MAY', $may_data],
                                                                ['JUN', $jun_data],
                                                                ['JUL', $jul_data],
                                                                ['AUG', $aug_data],
                                                                ['SEP', $sep_data],
                                                                ['OCT', $oct_data],
                                                                ['NOV', $nov_data],
                                                                ['DEC', $dec_data]
                                                        ]";
                                        echo '<script>var arrayFromPhp = ' . $anb . ';</script>';
                                        // print_r($anb);
                                        $a = array('JAN' => $jan_data, 'FEB' => $feb_data, 'MAR' => $mar_data, 'APR' => $apr_data, 'MAY' => $mar_data, 'JUN' => $jun_data, 'JUL' => $jul_data, 'AUG' => $aug_data, 'SEP' => $sep_data, 'OCT' => $oct_data, 'NOV' => $nov_data, 'DEC' => $dec_data);
                                        $count = 0;
                                        foreach ($a as $key => $val) {
                                            if ($val > $count) {
                                                $count = $val;
                                                $c = $key;
                                            }
                                        }

                                        if ($count == $jan_data) {
                                            $hiegest_jan = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $feb_data) {
                                            $hiegest_feb = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $mar_data) {
                                            $hiegest_mar = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $apr_data) {
                                            $hiegest_apr = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $may_data) {
                                            $hiegest_may = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $jun_data) {
                                            $hiegest_jun = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $jul_data) {
                                            $hiegest_jul = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $aug_data) {
                                            $hiegest_aug = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $sep_data) {
                                            $hiegest_sep = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $oct_data) {
                                            $hiegest_oct = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $nov_data) {
                                            $hiegest_nov = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } elseif ($count == $dec_data) {
                                            $hiegest_dec = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                        } else {
                                            $hiegest = '';
                                        }
                                        ?>
                                        <!-------------------------------------->
                                        <script type="text/javascript">
                                            window.onload = function () {
                                            var chart = new CanvasJS.Chart("chartContainer",
                                            {

                                            title:{
                                            text: "Inquiries - per month - <?php echo $year_org; ?>"
                                            },
                                                    axisX: {
                                                    valueFormatString: "MMM",
                                                            interval:1,
                                                            intervalType: "month"
                                                    },
                                                    axisY:{
                                                    includeZero: false

                                                    },
                                                    data: [
                                                    {
                                                    type: "line",
                                                            dataPoints: [
                                                            { x: new Date(2018, 00, 1), y: <?php echo $jan_data; ?>,<?php echo $hiegest_jan; ?> },
                                                            { x: new Date(2018, 01, 1), y: <?php echo $feb_data; ?>,<?php echo $hiegest_feb; ?>},
                                                            { x: new Date(2018, 02, 1), y: <?php echo $mar_data; ?>,<?php echo $hiegest_mar; ?>},
                                                            { x: new Date(2018, 03, 1), y: <?php echo $apr_data; ?>,<?php echo $hiegest_apr; ?> },
                                                            { x: new Date(2018, 04, 1), y: <?php echo $may_data; ?>,<?php echo $hiegest_may; ?> },
                                                            { x: new Date(2018, 05, 1), y: <?php echo $jun_data; ?>,<?php echo $hiegest_jun; ?> },
                                                            { x: new Date(2018, 06, 1), y: <?php echo $jul_data; ?>,<?php echo $hiegest_jul; ?> },
                                                            { x: new Date(2018, 07, 1), y: <?php echo $aug_data; ?>,<?php echo $hiegest_aug; ?> },
                                                            { x: new Date(2018, 08, 1), y: <?php echo $sep_data; ?>,<?php echo $hiegest_sep; ?> },
                                                            { x: new Date(2018, 09, 1), y: <?php echo $oct_data; ?>,<?php echo $hiegest_oct; ?> },
                                                            { x: new Date(2018, 10, 1), y: <?php echo $nov_data; ?>,<?php echo $hiegest_nov; ?> },
                                                            { x: new Date(2018, 11, 1), y: <?php echo $dec_data; ?>,<?php echo $hiegest_dec; ?> }
                                                            ]
                                                    }
                                                    ]
                                            });
                                            chart.render();
                                            }
                                        </script>
                                        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

                                        <!-------------------------------------->                                    



                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-lg-6 col-xs-12 col-sm-12">
                        <!-- BEGIN PORTLET-->
                        <!--<div class="portlet light bordered">
                                <div class="portlet-title">
                                        <div class="caption">
                                                <i class="icon-share font-red-sunglo hide"></i>
                                                <span class="caption-subject font-dark bold uppercase">Inquiries</span>
                                                <span class="caption-helper">monthly stats...</span>
                                        </div>
                                        <div class="actions">
                                                <button style="background:#32c5d2;color:white;width:100px;height:30px;border:none;border-radius:100px;"><a href="all-members.php" style="text-decoration:none;color:white;"> View All </a></button>
                                        </div>
                                </div>
                                <div class="portlet-body">
                                        <div id="site_activities_loading">
                                                <img src="../assets/global/img/loading.gif" alt="loading" /> 
                                        </div>
                                        <div id="site_activities_content" class="display-none">
                                                <div id="site_activities" style="height: 228px;"></div>
                                        </div>
                                        
                                </div>
                        </div>
                        <!-- END PORTLET-->
                        <!--</div>-->
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class=" icon-social-twitter font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Inquiries</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_actions_pending" data-toggle="tab"> Pending </a>
                                        </li>
                                        <li>
                                            <a href="#tab_actions_completed" data-toggle="tab"> Completed </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_actions_pending">
                                            <!-- BEGIN: Actions -->
                                            <div class="mt-actions">
                                                <?php
                                                $inquiryData1 = $welcomeCont->getInquiryData('', $admincustId, 'order by slno desc limit 5');
                                                $inquiryCount = count($inquiryData1);
                                                if ($inquiryCount == "0") {
                                                    ?>
                                                    <div class="mt-action">
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author"></span>
                                                                        <p class="mt-action-desc">Sorry! No Records Found Yet.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><br>
                                                        <?php
                                                    } else {
                                                        foreach ($inquiryData1 as $inquiryData) {
                                                            ?>
                                                            <div class="mt-action">
                                                                <div class="mt-action-img">
                                                                    <i class="fa fa-user"></i> </div>
                                                                <div class="mt-action-body">
                                                                    <div class="mt-action-row">
                                                                        <div class="mt-action-info ">
                                                                            <div class="mt-action-icon ">
                                                                                <i class="icon-magnet"></i>
                                                                            </div>
                                                                            <div class="mt-action-details ">
                                                                                <span class="mt-action-author"><?php echo $inquiryData->name; ?></span>
                                                                                <p class="mt-action-desc"><?php echo $inquiryData->inquiryType; ?></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="mt-action-datetime ">
                                                                            <span class="mt-action-date"><?php echo $inquiryData->recvDate; ?></span>
                                                                            <span class="mt-action-dot bg-red"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                            </div>
                                            <!-- END: Actions -->
                                        </div>
                                        <div class="tab-pane" id="tab_actions_completed">
                                            <!-- BEGIN:Completed-->
                                            <div class="mt-actions">
                                                <?php
                                                $inquiryDataCompleted1 = $welcomeCont->getInquiryData('(Not Applicable || Confirmed || Follow Up)', $admincustId, 'order by slno desc limit 5');
                                                $inquiryCompletedCount = count($inquiryDataCompleted1);
                                                if ($inquiryCompletedCount == "0") {
                                                    ?>
                                                    <div class="mt-action">
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author"></span>
                                                                        <p class="mt-action-desc">Sorry! No Records Found Yet.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><br>
                                                        <?php
                                                    } else {
                                                        foreach ($inquiryDataCompleted1 as $inquiryDataCompleted) {
                                                            ?>
                                                            <div class="mt-action">
                                                                <div class="mt-action-img">
                                                                    <i class="fa fa-user"></i> </div>
                                                                <div class="mt-action-body">
                                                                    <div class="mt-action-row">
                                                                        <div class="mt-action-info ">
                                                                            <div class="mt-action-icon ">
                                                                                <i class="icon-action-redo"></i>
                                                                            </div>
                                                                            <div class="mt-action-details ">
                                                                                <span class="mt-action-author"><?php echo $inquiryDataCompleted->name; ?></span>
                                                                                <p class="mt-action-desc"><?php echo $inquiryDataCompleted->inquiryType; ?></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="mt-action-datetime ">
                                                                            <span class="mt-action-date"><?php echo $inquiryDataCompleted->recvDate; ?></span>
                                                                            <span class="mt-action-dot bg-green"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <!-- END: Completed -->
                                            </div>
                                        </div>
                                        <a href="manage-enquiry.php"><button style="background:#ed6b75;color:white;width:100px;height:30px;border:none;border-radius:100px;"> View All </button></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark hide"></i>
                                        <span class="caption-subject font-hide bold uppercase">Recent Users</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group">
                                            <a href="manage-user.php"><button style="background:#32c5d2;color:white;width:100px;height:30px;border:none;border-radius:100px;"> View All </button></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <?php
                                        $recentUsers1 = $welcomeCont->getRecentUsers($admincustId, 'order by last_login desc limit 3');

                                        foreach ($recentUsers1 as $recentUsers) {
                                            $picpath = $recentUsers->photo;
                                            ?>
                                            <div class="col-md-4">
                                                <!--begin: widget 1-1 -->
                                                <div class="mt-widget-1">
                                                    <div class="mt-img">
                                                        <?php
                                                        if ($picpath == '') {
                                                            ?>
                                                            <img src="https://u.o0bc.com/avatars/no-user-image.gif" class="img-responsive" alt="" style="height:100px;width:100px;"> 
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <img src="http://res.cloudinary.com/the-perch/image/upload/g_face,w_100,h_100,c_fill/admin_user/<?php echo $picpath; ?>.jpg" class="img-responsive" alt="">
                                                                    <?php
                                                                }
                                                                ?>
    <!--<img src="assets/pages/media/users/avatar80_1.jpg"> -->
                                                                </div>
                                                                <div class="mt-body">
                                                                    <h3 class="mt-username"><?php echo $recentUsers->name; ?></h3>
                                                                    <p class="mt-user-title"><b>Email :- </b> <?php echo $recentUsers->email; ?> <br> <b>Type :- </b><?php echo $recentUsers->user_type; ?> </p>
                                                                    <!--<div class="mt-stats">
                                                                            <div class="btn-group btn-group btn-group-justified">
                                                                                    <a href="javascript:;" class="btn font-red">
                                                                                            <i class="icon-bubbles"></i> 1,7k </a>
                                                                                    <a href="javascript:;" class="btn font-green">
                                                                                            <i class="icon-social-twitter"></i> 2,6k </a>
                                                                                    <a href="javascript:;" class="btn font-yellow">
                                                                                            <i class="icon-emoticon-smile"></i> 3,7k </a>
                                                                            </div>
                                                                    </div>-->
                                                                </div>
                                                                <b>Date :- </b> <?php echo $recentUsers->last_login; ?> <br>
                                                                    <b>Time :- </b><?php echo $recentUsers->lat_login_time; ?> <br>
                                                                        </div>
                                                                        <!--end: widget 1-1 -->
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-lg-6 col-xs-12 col-sm-12"></div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>	
                                                                    </body>
                                                                    </html>
                                                                    <?php
                                                                    admin_footer();
                                                                    ?>