<?php
	$bookNowCont = new staticPageData($filename);
?>


	<section class="contctTop clear">
		<div class="imgBokNow"></div>
		<div class="contctMsg">
			<span class="contctMsgHtxt">Book-Now</span>
		</div>
	</section>
	
	<section class="TextTwoSec fram_width clear">
		<div class="btmDsgn">
			<h2><b>Self Cooking, Room Service and Chefs at the Perch</b></h2>
			<span>There are several dining options at our service apartments & guest houses. The quality of our food is comparable to good restaurants in Gurgaon and Delhi</span>		
		</div>
	</section><br>

	<section class="cardFxCntr">
		<div class="fram_width setComn">
			<div class='cardFx'>
				<div class="flipr">
					<div class="circle_icon theme_bg_color"> <i class="fa fa-thumbs-up fa_line_height"></i> </div>
					<div class="cardFxTxt">
						<p class="theme_text_color text_strach text_size_fa_card">Multiple Locations </p>
						<p class="fa_card_text">HUDA Metro, Golf Course Road, Near Medanta, Sohna Road, Unitech Cyber Park</p>
					</div>
				</div>
			</div>

			<div class='cardFx'>
				<div class="flipr">
					<div class="circle_icon theme_bg_color"> <i class="fa fa-map-marker fa_line_height"></i> </div>
					<div class="cardFxTxt">
						<p class="theme_text_color text_strach text_size_fa_card">Family Apartments </p>
						<p class="fa_card_text">3 Bedroom Apartments with Living ,Dining, Balcony and Kitchen</p>
					</div>
				</div>
			</div>
				
			<div class='cardFx'>
				<div class="flipr">
					<div class="circle_icon theme_bg_color"> <i class="fa fa-cutlery  fa_line_height"></i> </div>
					<div class="cardFxTxt">
						<p class="theme_text_color text_strach text_size_fa_card">Studio Apartments</p>
						<p class="fa_card_text">Studio Apartments on Sohna Road for rent Great long term deals</p>
					</div>
				</div>
			</div>						
		</div>
		<br><br>
	</section>	
	
	<section class="TextTwoSec fram_width clear">
		<div class="btmDsgn">
			<h2><b>Cooking & Food Options at our Properties</b></h2>
			<span>Looking for the best luxury Guest houses in Gurgaon or close to Delhi? Our properties in Gurgaon have different sized rooms & suites, suitable for.</span>
			<h5 style="margin-bottom:0;"></h5>		
		</div>
	</section>
	
	<section  class="AbtTxtCrd  clear">
		<div class="fram_width AbtTxtCrd">
			<?php
				$bookcnt = 1;
				$propdata1 = $bookNowCont -> getFeatureProperty('Y','2','and status="Y"');
				$numb = count($propdata1);
				foreach($propdata1 as $propdata)
					{
						$propid = $propdata->propertyID;
						$propimgdata1 = $bookNowCont -> getPropertyImage($propid ,'property','0','order by imagesID desc limit 1');
						$propimgdata = $propimgdata1[0];
						
						$roomtypes = html_entity_decode($propdata->roomType);
						$room_br = explode("^",$roomtypes);
			?>
						<div class="AbttxtCard" style="width:50%;">
							<div class="H70">
								<h3 class="mrgn0"><?php echo html_entity_decode($propdata->propertyName).", ".html_entity_decode($propdata->cityName); ?></h3>
							</div>
							
							<img src="http://res.cloudinary.com/the-perch/image/upload/w_552,h_300,c_fill/reputize/property/<?php echo html_entity_decode($propimgdata->imageURL); ?>.jpg" class="img img-responsive" style="height:300px;width:100%" alt="<?php echo html_entity_decode($propimgdata->imageAlt); ?>">
							
							<div class="Abttext">
								<?php
									foreach($room_br as $rooms)
										{
											if($rooms=='')
												{}
											else
												{
													$roomdata1 = $bookNowCont -> getPropertyRoom($propid,'Y','2',"and roomType='$rooms'");
													foreach($roomdata1 as $roomdata)
														{
															$firstlinetag = $rooms." @".$roomdata->roomPriceINR."/ Night | Discount: ".$roomdata->weeklyroomDiscount."% @Weekly / ".$roomdata->monthlyroomDiscount."% @Monthly";
								?>
															<div class="bkpara"><?php echo $firstlinetag; ?></div>
								<?php
														}
												}
										}
								?>			
							</div>
							
							<div class="txt_center">
								<a class="them_btn theme_bg_color propBtn" href="<?php SITE_URL_PATH; ?>/<?php echo $propdata->propertyURL;?>.html"><span><i class="fa fa-bolt" style="text-align:left;"></i>&nbsp;visit</span></a>
							</div>
							<br>			
						</div>	
			<?php
						if($bookcnt%2=='0')
							{
								if($bookcnt==$numb)
									{}
								else
									{
										echo '</div></section><section  class="AbtTxtCrd  clear"><div class="fram_width AbtTxtCrd">';
									}
							}
						$bookcnt++;
					}
			?>
		</div>
	</section>	
	<br>


	<?php
		desktopFooter();
	?>
	</body>
</html>