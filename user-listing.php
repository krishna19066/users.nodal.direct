<?php
include "admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$propertyData = new propertyData();
$userData = new UserDetails();

$getAllUserData = $userData->get_AlluserDetails($customerId);
$reccnt = count($getAllUserData);

$start = (intval($start) == 0 or $start == "") ? $start = 0 : $start;
$pagesize = intval($pagesize) == 0 ? $pagesize = 10 : $pagesize;
$heading = "Manage Booking";
$record_type = "Inquiry";
$table_name = "manage_booking";
$cond = "where customerID='$customerId'";
?>

<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>         
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Manage User Listing
                                    <small>View All User List</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>

                            <li>
                                <span>User List</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">User List</span>
                                        </div>
                                        <div class='button'><br>
                                            <a href="#" id ="export" role='button'><button style="background:hsl(166, 78%, 46%);border:none;color:white;width:170px;height:40px;float:right;font-weight:bold;margin-top: -15px;">Export CSV</button></a>
                                        </div><br><br>
                                    </div>
                                    <div style="float:right;padding:10px;"><i class="fa fa-star" style="color:gold;font-size:20px;"></i> &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i><font style="font-size:18px;"> &nbsp; Not Commented </font></div><br><br>
                                  
                                    <div class='container' style="display:none;"> 
                                        <div id="dvData">
                                            <table>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Phone No.</th>
													<th>Email</th>
                                                    <th>Date</th>
                                                   
                                                <?php
                                                //$csvsel = $enquiryData->GetAllManageBookingData($customerId);
// $csvsel = "select * from inquiry order by slno desc";
// $csvquer = mysql_query($csvsel);
                                                foreach ($getAllUserData as $csvrow) {
                                                    ?>
                                                    <tr>                                                       
                                                        <td><?php echo $csvrow->managerName; ?></td>
														<td><?php echo $csvrow->mobile; ?></td>
														<td><?php echo $csvrow->email; ?></td>
                                                        <td><?php echo $csvrow->timestamp; ?> </td>
                                                       
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php
                                        if ($reccnt > 0) {
                                            ?>
                                            <div class="row">

                                                <div class="inbox">
                                                   

                                                    <div class="col-md-9" style="padding-left:0px">

                                                        <div class="inbox-body">
                                                            <div class="inbox-content" style="">
                                                                <div id="replace_inbox">
                                                                    <table class="table table-striped table-advance table-hover">
                                                                        <thead>
                                                                            <tr>
																				<th>
                                                                                    <p><b>Name</b></p>
                                                                                </th>
																				<th>
                                                                                    <p><b>Phone No</b></p>
                                                                                </th>
                                                                                
                                                                                <th>
                                                                                    <p><b>Email</b></p>
                                                                                </th>
                                                                                
                                                                                <th>
                                                                                    <p><b>Create date</b></p>
                                                                                </th>
                                                                                <th> </th>

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="inq_tab">

                                                                            <?php
                                                                            $cnt = 1;
                                                                            $sl = $start + 1;
                                                                            foreach ($getAllUserData as $data) {
                                                                                //  $data = ms_htmlentities_decode($data);
                                                                                if ($sl % 2 != 0) {
                                                                                    $color = "#ffffff";
                                                                                } else {
                                                                                    $color = "#f6f6f6";
                                                                                }
                                                                                if ($cnt == "1") {
                                                                                    $color1 = "#32c5d2";
                                                                                }
                                                                                if ($cnt == "2") {
                                                                                    $color1 = "#3598dc";
                                                                                }
                                                                                if ($cnt == "3") {
                                                                                    $color1 = "#36D7B7";
                                                                                }
                                                                                if ($cnt == "4") {
                                                                                    $color1 = "#5e738b";
                                                                                }
                                                                                if ($cnt == "5") {
                                                                                    $color1 = "#1BA39C";
                                                                                }
                                                                                if ($cnt == "6") {
                                                                                    $color1 = "#32c5d2";
                                                                                }
                                                                                if ($cnt == "7") {
                                                                                    $color1 = "#578ebe";
                                                                                }
                                                                                if ($cnt == "8") {
                                                                                    $color1 = "#8775a7";
                                                                                }
                                                                                if ($cnt == "9") {
                                                                                    $color1 = "#E26A6A";
                                                                                }
                                                                                if ($cnt == "10") {
                                                                                    $color1 = "#29b4b6";
                                                                                }
                                                                                if ($cnt == "11") {
                                                                                    $color1 = "#4B77BE";
                                                                                }
                                                                                if ($cnt == "12") {
                                                                                    $color1 = "#c49f47";
                                                                                }
                                                                                if ($cnt == "13") {
                                                                                    $color1 = "#67809F";
                                                                                }
                                                                                if ($cnt == "14") {
                                                                                    $color = "#8775a7";
                                                                                }
                                                                                if ($cnt == "15") {
                                                                                    $color1 = "#73CEBB";
                                                                                }
                                                                                ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<tr onclick="$('#div<?php // echo $cnt;                                         ?>').toggle('1000'); hdsh(this);" >-->
                                                                                <tr onclick="hdsh(<?php echo $cnt; ?>);">

                                                                                   <td class="view-message view-message"> <?php echo $data->managerName; ?> </td>
																				    <td class="view-message view-message"> <?php echo $data->mobile; ?> </td>
                                                                                    <td class="view-message view-message"> <?php echo $data->email; ?> </td>
                                                                                
                                                                                    <td class="view-message view-message"> <?php echo $data->timestamp; ?> </td>
                                                                                   
                                                                                   
                                                                                </tr>                                                                           
                                                                             
                                                                            <?php
                                                                            $cnt++;
                                                                        }
                                                                        ?>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>	<br><br>
                                                <?php echo "<br><br>" . pageNavigation($reccnt, $pagesize, $start, $link); ?>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="row">

                                                <div class="inbox">                                                 
                                                    <div class="col-md-9" style="padding-left:0px">

                                                        <div class="inbox-body">
                                                            <div class="inbox-content" style="">
                                                                <div id="replace_inbox">
                                                                    <table class="table table-striped table-advance table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    <p></p>
                                                                                </th>
                                                                               
                                                                                <th>
                                                                                    <p><b>Email</b></p>
                                                                                </th>
                                                                               
                                                                                <th>
                                                                                    <p><b>Subscribe date</b></p>
                                                                                </th>																	
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td> Sorry! No Records Found....</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
										
										
										<div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> Managed Table</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                    <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-print"></i> Print </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer"><div class="row"><div class="col-md-6 col-sm-6"><div class="dataTables_length" id="sample_1_length"><label>Show <select name="sample_1_length" aria-controls="sample_1" class="form-control input-sm input-xsmall input-inline"><option value="5">5</option><option value="15">15</option><option value="20">20</option><option value="-1">All</option></select></label></div></div><div class="col-md-6 col-sm-6"><div id="sample_1_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="sample_1"></label></div></div></div><div class="table-scrollable"><table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="sample_1" role="grid" aria-describedby="sample_1_info">
                                        <thead>
                                            <tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="
                                                    
                                                        
                                                        
                                                    
                                                " style="width: 66px;">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes">
                                                        <span></span>
                                                    </label>
                                                </th><th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label=" Username : activate to sort column descending" style="width: 142px;"> Username </th><th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Email : activate to sort column ascending" style="width: 211px;"> Email </th><th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending" style="width: 116px;"> Status </th><th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Joined : activate to sort column ascending" style="width: 113px;"> Joined </th><th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 117px;"> Actions </th></tr>
                                        </thead>
                                        <tbody>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        <tr class="gradeX odd" role="row">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="sorting_1"> coop </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com"> good@gmail.com </a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-info"> Info </span>
                                                </td>
                                                <td class="center"> 12.12.2011 </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-docs"></i> New Post </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-tag"></i> New Comment </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-user"></i> New User </a>
                                                            </li>
                                                            <li class="divider"> </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-flag"></i> Comments
                                                                    <span class="badge badge-success">4</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr><tr class="gradeX even" role="row">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="sorting_1"> foopl </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com"> good@gmail.com </a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-info"> Info </span>
                                                </td>
                                                <td class="center"> 12.12.2011 </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-docs"></i> New Post </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-tag"></i> New Comment </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-user"></i> New User </a>
                                                            </li>
                                                            <li class="divider"> </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-flag"></i> Comments
                                                                    <span class="badge badge-success">4</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr><tr class="gradeX odd" role="row">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="sorting_1"> fop </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com"> good@gmail.com </a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning"> Suspended </span>
                                                </td>
                                                <td class="center"> 12.12.2011 </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-docs"></i> New Post </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-tag"></i> New Comment </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-user"></i> New User </a>
                                                            </li>
                                                            <li class="divider"> </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-flag"></i> Comments
                                                                    <span class="badge badge-success">4</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr><tr class="gradeX even" role="row">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="sorting_1"> goop </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com"> good@gmail.com </a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning"> Suspended </span>
                                                </td>
                                                <td class="center"> 12.12.2011 </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-docs"></i> New Post </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-tag"></i> New Comment </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-user"></i> New User </a>
                                                            </li>
                                                            <li class="divider"> </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-flag"></i> Comments
                                                                    <span class="badge badge-success">4</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr><tr class="gradeX odd" role="row">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td class="sorting_1"> kop </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com"> good@gmail.com </a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning"> Suspended </span>
                                                </td>
                                                <td class="center"> 12.12.2011 </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-docs"></i> New Post </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-tag"></i> New Comment </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-user"></i> New User </a>
                                                            </li>
                                                            <li class="divider"> </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-flag"></i> Comments
                                                                    <span class="badge badge-success">4</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr></tbody>
                                    </table></div><div class="row"><div class="col-md-5 col-sm-5"><div class="dataTables_info" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 5 of 25 records</div></div><div class="col-md-7 col-sm-7"><div class="dataTables_paginate paging_bootstrap_full_number" id="sample_1_paginate"><ul class="pagination" style="visibility: visible;"><li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li><li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li><li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li></ul></div></div></div></div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>

                        .page-header { position: relative; }
                        .reviews {
                            color: #555;    
                            font-weight: bold;
                            margin: 10px auto 20px;
                        }
                        .notes {
                            color: #999;
                            font-size: 12px;
                        }
                        .media .media-object { max-width: 60px; }
                        .media-body { position: relative; }
                        .media-date { 
                            position: absolute; 
                            right: 25px;
                            top: 25px;
                        }
                        .media-date li { padding: 0; }
                        .media-date li:first-child:before { content: ''; }
                        .media-date li:before { 
                            content: '.'; 
                            margin-left: -2px; 
                            margin-right: 2px;
                        }
                        .media-comment { margin-bottom: 20px; }
                        .media-replied { margin: 0 0 20px 50px; }
                        .media-replied .media-heading { padding-left: 6px; }

                        .btn-circle {
                            font-weight: bold;
                            font-size: 12px;
                            padding: 6px 15px;
                            border-radius: 20px;
                        }
                        .btn-circle span { padding-right: 6px; }
                        .embed-responsive { margin-bottom: 20px; }
                        .tab-content {

                            border: 1px solid #ddd;
                            border-top: 0;

                        }

                        .custom-input-file:hover .uploadPhoto { display: block; }
                        .anyClass {
                            height:350px;
                            overflow-y: scroll;
                        }
                        .anyClass {
                            height:350px;
                            overflow-y: scroll;
                        }
                    </style>
                    </body>


                    <script src="<?= SITE_ADMIN_URL; ?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL PLUGINS -->
                    <script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.min.js'></script>
                    <script>
                                                                function hdsh(tro)
                                                                {
                                                                    var tr_len = document.getElementById('inq_tab').children.length / 2;
                                                                    for (i = 1; i <= tr_len; i++)
                                                                    {
                                                                        if (i == tro)
                                                                        {
                                                                            // document.getElementById('div' + i).style.display = 'block';
                                                                            $('#div' + i).slideToggle();
                                                                        } else
                                                                        {
                                                                            document.getElementById('div' + i).style.display = 'none';
                                                                        }
                                                                    }
                                                                }

                    </script>
                    <script type='text/javascript'>
                        $(document).ready(function () {

                            console.log("HELLO")
                            function exportTableToCSV($table, filename) {
                                var $headers = $table.find('tr:has(th)')
                                        , $rows = $table.find('tr:has(td)')

                                        // Temporary delimiter characters unlikely to be typed by keyboard
                                        // This is to avoid accidentally splitting the actual contents
                                        , tmpColDelim = String.fromCharCode(11) // vertical tab character
                                        , tmpRowDelim = String.fromCharCode(0) // null character

                                        // actual delimiter characters for CSV format
                                        , colDelim = '","'
                                        , rowDelim = '"\r\n"';

                                // Grab text from table into CSV formatted string
                                var csv = '"';
                                csv += formatRows($headers.map(grabRow));
                                csv += rowDelim;
                                csv += formatRows($rows.map(grabRow)) + '"';

                                // Data URI
                                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                                // For IE (tested 10+)
                                if (window.navigator.msSaveOrOpenBlob) {
                                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                                        type: "text/csv;charset=utf-8;"
                                    });
                                    navigator.msSaveBlob(blob, filename);
                                } else {
                                    $(this)
                                            .attr({
                                                'download': filename
                                                , 'href': csvData
                                                        //,'target' : '_blank' //if you want it to open in a new window
                                            });
                                }

                                //------------------------------------------------------------
                                // Helper Functions 
                                //------------------------------------------------------------
                                // Format the output so it has the appropriate delimiters
                                function formatRows(rows) {
                                    return rows.get().join(tmpRowDelim)
                                            .split(tmpRowDelim).join(rowDelim)
                                            .split(tmpColDelim).join(colDelim);
                                }
                                // Grab and format a row from the table
                                function grabRow(i, row) {

                                    var $row = $(row);
                                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                                    var $cols = $row.find('td');
                                    if (!$cols.length)
                                        $cols = $row.find('th');

                                    return $cols.map(grabCol)
                                            .get().join(tmpColDelim);
                                }
                                // Grab and format a column from the table 
                                function grabCol(j, col) {
                                    var $col = $(col),
                                            $text = $col.text();

                                    return $text.replace('"', '""'); // escape double quotes

                                }
                            }


                            // This must be a hyperlink
                            $("#export").click(function (event) {
                                // var outputFile = 'export'
                                var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                                outputFile = outputFile.replace('.csv', '') + '.csv'

                                // CSV
                                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);

                                // IF CSV, don't do event.preventDefault() or return false
                                // We actually need this to be a typical hyperlink
                            });
                        });
                    </script>
                    <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                    <?php
                    admin_footer();
                    ?>
                    </html>
                </div>