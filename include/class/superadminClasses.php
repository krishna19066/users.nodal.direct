<?php

ob_start();
date_default_timezone_set("Asia/Kolkata");
session_start();

include "../include/config.php";
include "../include/return_functions.php";

function check_session($sess_name, $REDIRECT_URL) {
    if (!strlen($_SESSION[$sess_name])) {
        header("Location:$REDIRECT_URL");
        exit;
    }
}

$customerStatArr = array("payment_recv" => "Payment Recieved", "content_recv" => "Content Recieved", "content_upd" => "Content Updated", "verified" => "Verified", "approved" => "Approved", "cont_change" => "Content Changes", "desg_change" => "Design Changes", "live" => "Live", "pending" => "Pending");
?>

<?php

//----------------------------------------------------------- Class For Repeated functions starts (Main CMS Class) ------------------------------------------------------------//
class superAdminMain {

    function getAllCmsUserData() {
        $sql = "select * from  superadmin_cmsUser where status = 'live'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getCmsUser = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return $getCmsUser;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Auth. Super Admin Login Function Starts ---------------------------------//

    var $ajaxLoginData;

    function authLogin($ajxLognDat) {
        $this->ajaxLoginData = $ajxLognDat;

        $dat = $ajxLognDat;
        $br = explode("~", $dat);
        $uid = $br[0];
        $pas = $br[1];
        $utype = $br[2];

        $sql = "select * from superadminUser where userId='$uid' and password='$pas' and userType='$utype'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getUserDet1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getUserDet = $getUserDet1[0];
            $getUserCount = count($getUserDet);

            if ($getUserCount > 0) {
                $_SESSION['login_sucess'] = $getUserDet->userId;
                $_SESSION['user_type'] = $getUserDet->userType;


                echo "<font style='color:#80f504;opacity:1;font-weight:bold;'><b>Login Successful ...</b></font>~succes";
            } else {
                echo "<font style='color:#f90345;opacity:1;font-weight:bold;'>Wrong Password / User Id</font>";
            }
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Auth. Super Admin Login Function Ends ---------------------------------//
    //---------------------------------- Send Credentials via Mail to User Function Starts ---------------------------------//

    var $emailAddress;
    var $userId;
    var $password;

    function sendCredentialMail($email, $userID, $paswrd) {
        $this->emailAddress = $email;
        $this->userId = $userID;
        $this->password = $paswrd;

        try {
            $subject2 = "Re - Requested Access Credentials for CMS Demo";
            $to_mail = $email;
            $from = "support@nodal.direct";

            $mess2 = '<body style="background:#ccc;padding:50px 0px;">
											<table cellspacing="0" cellpadding="0" border="0" width="600" style="margin:auto;background:#fff;table-layout:fixed; color:#22615a;border-radius: 5px 5px 0px 0px;">
												<tbody>
													<tr>
														<td colspan="6" style="height: 25px;">
																												
														</td>
													</tr>				
													<tr style="background:#060115;color:#fff;height: 35px;">
														<td colspan="6" style="padding-left:30px;"><a href="http://nodal.direct" target="_blank"><img src="http://www.reputize.in/sitepanel/assets/pages/img/login/nodal-logo.jpg" width="100" /></a></td>
													</tr>
													<tr style="color:#3f51b5;height: 50px;">
														<td colspan="6" style="padding-left:30px;">Below are your credentials for accessing your Nodal CMS.</td>
													</tr>
													<tr>
														<td></td>
														<td colspan="2" style="padding-left:30px;background:#04a999;border-bottom:1px solid #0c9e90;letter-spacing: 1px;">User ID :</td>
														<td colspan="2" style="background:#04a999;padding:5px;border-bottom:1px solid #0c9e90;color: #C8FCFF;letter-spacing: 2px;">' . $userID . '</td>
														<td></td>
													</tr>
													<tr>
														<td style="background:#060115;"></td>
														<td colspan="2" style="padding-left:30px;background:#04a999;border-bottom:1px solid #0c9e90;letter-spacing: 1px;">Password :</td>
														<td colspan="2" style="background:#04a999;padding:5px;border-bottom:1px solid #0c9e90;color: #C8FCFF;letter-spacing: 2px;">' . $paswrd . '</td>
														<td style="background:#060115;"></td>
													</tr>
													<tr>
														<td style="background:#060115;"></td>
														<td colspan="2" style="padding-left:30px;background:#060115;padding:5px;color:#fff;"></td>
														<td colspan="2" style="background:#060115;padding:5px;color:#fff;"></td>
														<td style="background:#060115;"></td>
													</tr>					
													<tr>
														<td style="background:#060115;"></td>
														<td colspan="2" style="padding-left:30px;background:#060115;padding:5px;color:#fff;"></td>
														<td colspan="2" style="background:#060115;padding:5px;color:#fff;"></td>
														<td style="background:#060115;"></td>
													</tr>					
													<tr>
														<td style="background:#060115;"></td>
														<td colspan="2" style="padding-left:30px;background:#060115;padding:5px;color:#fff;"></td>
														<td colspan="2" style="background:#060115;padding:5px;color:#fff;"></td>
														<td style="background:#060115;"></td>
													</tr>

													<tr style="background:#060115;">
														<td colspan="2" style="padding:10px;color:#fff;"><span style="font-size:20px;">Thank You</span></td>
														<td colspan="2"></td>
														<td><small>Powered by</small></td>
														<td><a href="http://nodal.direct" target="_blank"><img src="http://www.reputize.in/sitepanel/assets/pages/img/login/nodal-logo.jpg" width="70" /></a></td>			
													</tr>
													<tr>
														<td colspan="6" style="padding:10px;"></td>
													</tr>					
													

													
												</tbody>
											</table>
										</body>';

            $headers = "From: support@nodal.direct\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-Type: text/html; charset=utf-8\r\n";
            $headers.= "X-Priority: 1\r\n";

            mail($to_mail, $subject2, $mess2, $headers);
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Send Credentials via Mail to User Function Ends ---------------------------------//
    //---------------------------------- Insert Demo CMS User Function Starts ---------------------------------//

    var $userType;
    var $userName;
    var $createDate;

    function addDemoUser($userID, $paswrd, $type, $name, $email, $date) {
        $this->userId = $userID;
        $this->password = $paswrd;
        $this->userType = $type;
        $this->userName = $name;
        $this->emailAddress = $email;
        $this->createDate = $date;

        $sql = "insert into admin_details_demo(user_id,pwd,user_type,name,email,create_date) values('$userID','$paswrd','$type','$name','$email','$date')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Insert Demo CMS User Function Ends ---------------------------------//
    //---------------------------------- Generate User ID Function Starts ---------------------------------//

    function generateUserid() {
        try {
            //------------------------------------------------ User Id -------------------------------------
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ6789';
            $random_string_length = 6;
            $string = '';
            $max = strlen($characters) - 1;
            for ($i = 0; $i < $random_string_length; $i++) {
                $string .= $characters[mt_rand(0, $max)];
            }
            $gen_user = $string;

            //----------------------------------------Password -------------------------------------------
            $characters1 = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $random_string_length1 = 8;
            $string1 = '';
            $max1 = strlen($characters1) - 1;
            for ($i = 0; $i < $random_string_length1; $i++) {
                $string1 .= $characters1[mt_rand(0, $max1)];
            }
            $gen_pass = $string1;


            $send_data = $gen_user . "~" . $gen_pass;
            echo $send_data;
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Generate User ID Function Ends ---------------------------------//
    //---------------------------------- Generate User Password Function Starts ---------------------------------//

    function generateUserPassword() {
        try {
            //----------------------------------------Password -------------------------------------------
            $characters1 = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $random_string_length1 = 8;
            $string1 = '';
            $max1 = strlen($characters1) - 1;
            for ($i = 0; $i < $random_string_length1; $i++) {
                $string1 .= $characters1[mt_rand(0, $max1)];
            }
            $gen_pass = $string1;


            $send_data = $gen_user . "~" . $gen_pass;

            echo $send_data;
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Generate User Password Function Ends ---------------------------------//
    //---------------------------------- Add User to Superadmin Function Starts ---------------------------------//

    var $hotelName;
    var $propertyNo;
    var $phoneNo;
    var $address;
    var $city;
    var $state;
    var $country;
    var $templateType;
    var $templateSelected;
    var $domainName;
    var $domainUser;
    var $domainPassword;
    var $ftpUser;
    var $ftpPassword;
    var $sections;
    var $setUpType;
    var $setUpCost;
    var $addOnServices;
    var $addOnServiceType;
    var $addOnServiceCost;
    var $month;
    var $year;

    function addCmsUser($htlNam, $propNo, $name, $email, $userID, $paswrd, $phn, $addr, $cty, $state, $cntry, $tempType, $selectTemp, $domanNam, $domanUser, $domanPass, $ftpUsr, $ftpPass, $sectns, $setuptyp, $setupcost, $addonServ, $addonServTyp, $addonServCost, $date, $mnth, $year, $instalment_type, $Instalmentcost, $websiteType) {
        $this->hotelName = $htlNam;
        $this->propertyNo = $propNo;
        $this->userName = $name;
        $this->emailAddress = $email;
        $this->userId = $userID;
        $this->password = $paswrd;
        $this->phoneNo = $phn;
        $this->address = $addr;
        $this->city = $cty;
        $this->state = $state;
        $this->country = $cntry;
        $this->templateType = $tempType;
        $this->templateSelected = $selectTemp;
        $this->domainName = $domanNam;
        $this->domainUser = $domanUser;
        $this->domainPassword = $domanPass;
        $this->ftpUser = $ftpUsr;
        $this->ftpPassword = $ftpPass;
        $this->sections = $sectns;
        $this->setUpType = $setuptyp;
        $this->setUpCost = $setupcost;
        $this->addOnServices = $addonServ;
        $this->addOnServiceType = $addonServTyp;
        $this->addOnServiceCost = $addonServCost;
        $this->createDate = $date;
        $this->month = $mnth;
        $this->year = $year;

        $sql = "insert into superadmin_cmsUser(hotel_name,propertyNo,admin_name,email,user_id,password,phone,address,city,state,country,template_type,template_selected,domainName,domainUser,domainPass,FTP_user,FTP_password,sections,setUpType,setUpCost,addOnServices,addOnServiceType,addOnServiceCost,date,month,year,status,instalment_type,Instalmentcost,client_type,website_type) values('$htlNam','$propNo','$name','$email','$userID','$paswrd','$phn','$addr','$cty','$state','$cntry','$tempType','$selectTemp','$domanNam','$domanUser','$domanPass','$ftpUsr','$ftpPass','$sectns','$setuptyp','$setupcost','$addonServ','$addonServTyp','$addonServCost','$date','$mnth','$year','pending','$instalment_type','$Instalmentcost','Website','$websiteType')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addCmsUserOther($htlNam, $propNo, $name, $email, $userID, $phn, $addr, $cty, $state, $cntry, $domanNam, $domanUser, $domanPass, $ftpUsr, $ftpPass, $sectns, $setupcost, $addonServCost, $date, $mnth, $year, $instalment_type, $Instalmentcost) {
       // $sql = "insert into superadmin_cmsUser(hotel_name,propertyNo,admin_name,email,user_id,phone,address,city,state,country,domainName,domainUser,domainPass,FTP_user,FTP_password,setUpCost,addOnServiceCost,date,month,year,status,instalment_type,Instalmentcost,client_type) values('$htlNam','$propNo','$name','$email','$userID','$phn','$addr','$cty','$state','$cntry','$domanNam','$domanUser','$domanPass','$ftpUsr','$ftpPass','$setupcost','$addonServCost','$date','$mnth','$year','pending','$instalment_type','$Instalmentcost','Other')";
        $sql = "insert into superadmin_cmsUser set hotel_name='$htlNam', propertyNo='$propNo',admin_name='$name',email='$email',user_id='$userID',phone='$phn',address='$addr',city='$cty',state='$state',country='$cntry',domainName='$domanNam',domainUser='$domanUser',domainPass='$domanPass',FTP_user='$ftpUsr',FTP_password='$ftpPass',setUpCost='$setupcost',addOnServiceCost='$addonServCost',date='$date',month='$mnth',year='$year',status='pending',instalment_type='$instalment_type',Instalmentcost='$Instalmentcost',client_type='Other'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Add User to Superadmin Function Ends ---------------------------------//
    //--------------------------------------Update Cloud Details --------------------------->
    function updCloudData($userID, $cloudName, $apiKey, $apiSecret) {
        $sql = "update superadmin_cmsUser set cloud_name='$cloudName',api_key='$apiKey',api_secret='$apiSecret' where user_id='$userID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function updCloudDataForAdmin($userID, $cloudName, $apiKey, $apiSecret) {
        $sql = "update admin_details set cloud_name='$cloudName',api_key='$apiKey',api_secret='$apiSecret' where user_id='$userID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //----------------------Update CMS User Details--------------------------------------------------------//
    function updCMSUserDetails($customerID, $property_url, $addOns) {
        $sql = "update superadmin_cmsUser set hotel_name='$_REQUEST[hotel_name]',propertyNo='$_REQUEST[propertyNo]',admin_name='$_REQUEST[admin_name]',email='$_REQUEST[admin_email]',phone='$_REQUEST[phone]',address='$_REQUEST[address]',city='$_REQUEST[city]',state='$_REQUEST[state]',country='$_REQUEST[country]',template_type='$_REQUEST[template_type]',template_selected='$_REQUEST[template_selec]',domainName='$_REQUEST[domain_name]',domainUser='$_REQUEST[domain_user]',domainPass='$_REQUEST[domain_password]',FTP_user='$_REQUEST[ftp_user]',FTP_password='$_REQUEST[ftp_password]',sections='$_REQUEST[sections]',setUpCost='$_REQUEST[customSetupCst]',property_url='$property_url',addOnServices= '$addOns' ,instalment_type='$_REQUEST[instalment_type]',Instalmentcost='$_REQUEST[Instalmentcost]' where slno='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Fetch Customer ID Function Starts ---------------------------------//

    function getCustomerId($userID, $email) {
        $this->userId = $userID;
        $this->emailAddress = $email;

        $sql = "select * from superadmin_cmsUser where user_id='$userID' and email='$email'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getcustId1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getcustId = $getcustId1[0];
            $orgCustId = $getcustId->slno;

            return $orgCustId;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Fetch Customer ID Function Ends ---------------------------------//
    //---------------------------------- Add User to Admin Function Starts ---------------------------------//

    var $customerID;
    var $status;

    function addCmsUserAdmin($userID, $custId, $paswrd, $name, $email, $sectns, $addr, $htlNam, $phn, $cntry,$booking_engine) {
        $this->userId = $userID;
        $this->customerID = $custId;
        $this->password = $paswrd;
        // $this ->userType = $type;
        // $this ->status = $stat;
        $this->userName = $name;
        $this->emailAddress = $email;
        $this->sections = $sectns;
        $this->address = $addr;
        $this->hotelName = $htlNam;
        $this->phoneNo = $phn;
        $this->country = $cntry;

        $sql = "insert into admin_details(user_id,customerID,pwd,user_type,status,name,email,section,address,companyName,phone,country,booking_engine) values('$userID','$custId','$paswrd','admin','N','$name','$email','$sectns','$addr','$htlNam','$phn','$cntry','$booking_engine')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Add User to Admin Function Ends ---------------------------------//
    //---------------------------------- Add User to Admin Function Starts ---------------------------------//

    function getCmsUsers($stat) {
        $this->status = $stat;

        $sql = "select * from superadmin_cmsUser where $stat order by slno desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getCmsUser = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return $getCmsUser;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Add User to Admin Function Ends ---------------------------------//
    //--------------------------- Add Superadmin User to Admin Function Starts --------------------------//

    var $queryType;
    var $createTime;

    function addSuperAdmin($name, $userID, $paswrd, $email, $type, $queryTyp, $date, $time) {
        $this->userName = $name;
        $this->userId = $userID;
        $this->password = $paswrd;
        $this->emailAddress = $email;
        $this->userType = $type;
        $this->queryType = $queryTyp;
        $this->createDate = $date;
        $this->createTime = $time;

        if ($queryTyp == 'insert') {
            $sql = "insert into superadminUser(name,userId,password,email,userType,date,time) values('$name','$userID','$paswrd','$email','$type','$date','$time')";
        } else {
            // $sql = "update superadminUser set name='$name',email='$email',userType='$type' where userId='$userID'";
        }

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //--------------------------- Add Superadmin User to Admin Function Ends -------------------------//
    //---------------------------Update User details --------------------------------------------------->
    function UpdateUserSupperAdmin($userID) {
        $sql = "update superadminUser set name='$_POST[type_name]',userId='$_POST[type_username]',password='$_POST[type_password]', email='$_POST[type_email]',userType=' $_POST[user_type]' where slno='$userID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------delete user data fuction-------------------------------->
    function deleteUserData($userID) {
        $sql = "DELETE FROM superadminUser WHERE slno='$userID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------- Get Superadmin User Details Function Starts -------------------------//


    function getUserDet($userID) {
        $this->userId = $userID;

        $sql = "select * from superadmin_cmsUser where user_id='$userID' order by slno desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getUserDetls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getUserDetls = $getUserDetls1[0];

            return $getUserDetls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getSuperAdminUser() {
        $sql = "select * from superadminUser order by slno desc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getCmsUser = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return $getCmsUser;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getSumOfSetUpCost($userId) {
        $sql = "SELECT sum(amount) as sumOfCost,sum(tds_amount) as tdsAmount FROM `manage_payment` WHERE user_id='$userId' and payment_type = 'SetUpCost'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getpayment = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return $getpayment;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetSuperadminDataWithID($userID) {
        $sql = "select * from superadminUser where slno ='$userID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getCmsUser = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return $getCmsUser;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------- Get Superadmin User Details Function Ends ------------------------//
    //--------------------------- Update Customer Site Status Function Starts --------------------------//

    function updCustomerStat($userID, $stat) {
        $this->userId = $userID;
        $this->status = $stat;
        $date = date('Y-m-d');
        if ($stat == 'live') {
            $sql = "update superadmin_cmsUser set status='$stat',live_date='$date',live='$date' where user_id='$userID'";
        } else {
            $sql = "update superadmin_cmsUser set status='$stat' where user_id='$userID'";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function updCustomerLiveDate($userID, $stat, $from_date) {
        $this->userId = $userID;
        $this->status = $stat;
        // $date = date('Y-m-d');      
        if ($stat == 'live') {
            $sql = "update superadmin_cmsUser set status='$stat',live_date='$from_date' where user_id='$userID'";
        } else {
            $sql = "update superadmin_cmsUser set status='$stat' where user_id='$userID'";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //--------------------------- Update Customer Site Status Function Ends -------------------------//
    //------------------------------ Add Add-on Service Function Starts -----------------------------//

    function addAddonService($addonServiceName, $addonServiceCost) {
        $this->addOnServices = $addonServ;
        $this->addOnServiceCost = $addonServCost;

        $date = date('Y-m-d');
        $sql = "insert into addOnServices(addOnName,addOnPrice,date) values('$addonServiceName','$addonServiceCost','$date')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------- Add Add-on Service Function Ends ------------------------------//
    //----------------------------- Fetch Add-on Service Function Starts ---------------------------//

    function getAddonService() {
        $sql = "select * from addOnServices  order by id desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getAddOns1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getAddOns = $getAddOns1;

            return $getAddOns;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAddonServiceWithID($addOnId) {
        $sql = "select * from addOnServices where id='$addOnId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getAddOns1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getAddOns = $getAddOns1;

            return $getAddOns;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddOnDelete($addOn_Id) {
        $sql = "DELETE FROM addOnServices WHERE id='$addOn_Id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Fetch Add-on Service Function Ends ----------------------------//
    //---------------------------- Update Add-on Service Function Starts ---------------------------//

    function updateAddonService($addonServiceName, $addonServiceCost, $addonServiceSlno) {
        $this->addOnServices = $addonServ;
        $this->addOnServiceCost = $addonServCost;

        $sql = "update addOnServices set addOnName='$addonServiceName',addOnPrice='$addonServiceCost' where id='$addonServiceSlno'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //----------------------------- Update Add-on Service Function Ends ----------------------------//
    //---------------------------- Insert Customer Bill Function Starts ---------------------------//

    var $bill;

    function insertBill($custBill, $custId, $date) {
        $this->bill = $custBill;
        $this->customerID = $custId;
        $this->createDate = $date;

        $sql = "insert into customerBils(bill,customerID,date) values('$custBill','$custId','$date')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //----------------------------- Insert Customer Bill Function Ends ----------------------------//
    //----------------------------- Get Customer Bill Function Starts ----------------------------//

    function fetchBill($custId) {
        $this->customerID = $custId;

        $sql = "select * from customerBils where customerID='$custId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getBills1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getBills = $getBills1;

            return $getBills;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Add-on Service Function Ends -----------------------------//
    ///--------------------------Add Payment Function ---------------------------------------->
    function AddPayment($customerID, $pending_amt) {
        $sql = "insert into manage_payment set customerID='$customerID',user_id='$_REQUEST[user_id]', propertyName='$_REQUEST[hotel_name]',payment_mode='$_REQUEST[payment_mode]',received_date='$_REQUEST[received_date]',from_date='$_REQUEST[from_date]',to_date='$_REQUEST[to_date]',amount='$_REQUEST[amount]',setUpCost='$_REQUEST[setUpCost]',payment_type='$_REQUEST[payment_type]',instalment_type='$_REQUEST[instalment_type]',pending_amount='$pending_amt',tds_amount='$_REQUEST[tds_amount]',tds_type='$_REQUEST[tds_type]'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------insert payment  by cron---------------------------------------->
    function InsertPaymentNextBill($hotel_name, $user_id, $live_date, $to_date_nextBill_2, $instalmentCost) {
        $sql = "insert into manage_payment set user_id='$user_id', propertyName='$hotel_name',from_date='$live_date',to_date='$to_date_nextBill_2',amount='0',pending_amount='$instalmentCost',payment_type='Instalment',payment_mode='Cron'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //--------------------Update Payment ------------------------------------------>
    function UpdatePayment($payment_id, $pending_amt) {
        $sql = "update manage_payment set payment_mode='$_REQUEST[payment_mode]',received_date='$_REQUEST[received_date]',from_date='$_REQUEST[from_date]',to_date='$_REQUEST[to_date]',amount='$_REQUEST[amount]',setUpCost='$_REQUEST[setUpCost]',payment_type='$_REQUEST[payment_type]',instalment_type='$_REQUEST[instalment_type]',pending_amount='$pending_amt',tds_amount='$_REQUEST[tds_amount]',tds_type='$_REQUEST[tds_type]' where slno='$payment_id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-----------------------Get Payment Details info ------------------------------------------->
    function getPaymentDeatils($userId) {
        $sql = "select * from manage_payment where user_id='$userId' ORDER BY `slno` ASC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPayment1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPayment = $getPayment1;
            return $getPayment;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPaymentDeatilsWithPaymentID($userId) {
        $sql = "select * from manage_payment where slno='$userId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPayment1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPayment = $getPayment1;
            return $getPayment;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getSumOfInsCost($userId, $from_date, $to_date) {
        $sql = "select sum(amount) as InstalSumCost,sum(tds_amount) as tdsAmount from manage_payment where user_id='$userId' and from_date='$from_date' and to_date='$to_date' ORDER BY `slno` ASC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPayment1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPayment = $getPayment1;
            return $getPayment;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertMailNotification($email99, $subject, $email_body) {
        $sql = "insert into manage_mail set email='$email99',subject='$subject', email_body='$email_body'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getEmailNotification() {
        $sql = "select * from manage_mail  ORDER BY `id` DESC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getNotification1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getNotification = $getNotification1;
            return $getNotification;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getHotelInfo($mail) {
        $sql = "select * from superadmin_cmsUser  where email='$mail'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getNotification1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getNotification = $getNotification1;
            return $getNotification;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertPaymentAlert($email, $subject, $email_body, $hotelName) {
        $sql = "insert into payment_alert set email='$email',subject='$subject', email_body='$email_body',hotel_name='$hotelName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function TotalBillCycle($userId) {
        $sql = "SELECT * FROM `manage_payment` WHERE user_id= '$userId' and from_date !='' GROUP BY from_date,to_date";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function PendingBillCycle($userId) {
        $sql = "SELECT * FROM `manage_payment` WHERE user_id= '$userId' and from_date !='' and pending_amount = 0 GROUP BY from_date,to_date";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getClientAllChanges($clientName) {
        $sql = "SELECT * FROM `new_changes` WHERE FIND_IN_SET('$clientName',clientName)";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertNewChanges($clientName99, $heading, $new_changes) {
        $sql = "insert into new_changes set clientName='$clientName99',heading='$heading', newChanges='$new_changes'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getClientdataLoginActivity() {
        $sql = "SELECT * FROM `admin_details` WHERE pwd != '' ORDER BY `last_login` DESC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getClientLoginWithCustomerID($customerID) {
        $sql = "SELECT * FROM `admin_details` WHERE customerID = '$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function gethomePageLogin($customerID) {
        $sql = "SELECT * FROM `homeContent` WHERE customerID = '$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAboutUsPage($customerID) {
        $sql = "SELECT * FROM `static_page_tbl` WHERE customerID = '$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getContactUsPage($customerID) {
        $sql = "SELECT * FROM `static_page_contactus` WHERE customerID = '$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyData($customerID) {
      $sql = "SELECT * FROM `propertyTable` WHERE customerID = '$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }  
    }
     function getPropertyRoomData($customerID) {
      $sql = "SELECT * FROM `propertyRoom` WHERE customerID = '$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }  
    }
    function getPropertyPhoto($propertyID){
        $sql = "SELECT timestamp FROM `propertyImages` WHERE propertyID = '$propertyID' and imageType='property' order by imagesID desc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }  
    }
    function getPropertyRoomPhoto($roomID){
         $sql = "SELECT timestamp FROM `propertyImages` WHERE roomID = '$roomID' and imageType='room' order by imagesID desc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        } 
    }
    function getGoogleAnalyticReportData(){
        $sql = "SELECT * FROM `tbl_analytic_code` where analytic_url != ''";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function getGoogleAnalyticReportDataWithCustomerID($custoID){
        $sql = "SELECT * FROM `tbl_analytic_code` where analytic_url != '' and customerID ='$custoID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function GetHotelDataBothTable($custID){
         $sql = "SELECT * FROM `superadmin_cmsUser` JOIN admin_details  on superadmin_cmsUser.slno = admin_details.customerID where superadmin_cmsUser.slno ='$custID'  ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function getAllInquiry($custID){
        $sql = "SELECT * FROM `inquiry`  where customerID ='$custID' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function getCurrentMonthInquiry($custID){
        $sql = "SELECT * FROM `inquiry`  where customerID ='$custID' and YEAR(timestamp) = YEAR(CURRENT_DATE()) AND 
      MONTH(timestamp) = MONTH(CURRENT_DATE()); ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function getPriviosMonthInquiry($custID){
        $sql = "SELECT * FROM `inquiry`  where customerID ='$custID' and YEAR(timestamp) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
AND MONTH(timestamp) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function getCurrMonthInstantBookingInq($custID){
       $sql = "SELECT * FROM `inquiry`  where customerID ='$custID' and inquiryType='instantBooking' and YEAR(timestamp) = YEAR(CURRENT_DATE()) AND 
      MONTH(timestamp) = MONTH(CURRENT_DATE()); ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        } 
    }
    function getPreMonthRequestCallInq($custID){
       $sql = "SELECT * FROM `inquiry`  where customerID ='$custID' and inquiryType='RequestaCall' and YEAR(timestamp) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
AND MONTH(timestamp) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        } 
    }
    function getCurrMonthLoggin($custID){
        $sql = "SELECT * FROM `log_table`  where customerID ='$custID'  and YEAR(curr_date) = YEAR(CURRENT_DATE()) AND 
      MONTH(curr_date) = MONTH(CURRENT_DATE()); ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        } 
    }
    function getPreMonthLoggin($custID){
        $sql = "SELECT * FROM `log_table`  where customerID ='$custID'  and YEAR(curr_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
AND MONTH(curr_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getvalue1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getvalue = $getvalue1;
            return $getvalue;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        } 
    }
    
    
                   
    function SendClientMail($email, $subject, $mess) {

        // $subject = 'Expiry Reminder Email';
        // $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";

        $to = $email;
        // $cc = $r['mail_cc'];
        $from = 'finance@theperch.in';

        $url = 'https://api.sendgrid.com/';
        $user = 'kunalsingh2000';
        $pass = 'P@ssw0rd';

        $json_string = array('to' => array($to), 'category' => 'Remainder');

        $params = array(
            'api_user' => 'kunalsingh2000',
            'api_key' => 'P@ssw0rd',
            'x-smtpapi' => json_encode($json_string),
            'to' => $to,
            'subject' => $subject,
            'html' => $mess,
            'replyto' => $to,
            // 'cc' => $cc,
            'fromname' => 'Hawthorn Technologies Pvt. Ltd.',
            'text' => 'testing body',
            'from' => $from,
        );

        //echo $row[email];
        $request = $url . 'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // obtain response
        $response = curl_exec($session);
        curl_close($session);
    }

}
?>
			
