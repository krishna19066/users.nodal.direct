<?php
	include "include/config.php";
	include "formClasses.php";
	include "include/common-functions.php";
	include "include/return_functions.php";
	
	//----------------------------------------------------------- Class For Repeated functions starts (Main Front End Class) ------------------------------------------------------------//
	class websiteMain
		{
			var $filename;
			var $status;
			
			function __construct($metaUrl)
				{
					$this ->filename = $metaUrl;
					
					$sql = "select * from metaTags where filename='$metaUrl'";
						
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
			
							$exQuey = $DB->query($sql);
							$metadata = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$ptitle = $metadata[0]->MetaTitle;
							$desc=$metadata[0]->MetaDisc;
							$kwords=$metadata[0]->MetaKwd;	
							$pageName='index';
							$canonicalurl='http://www.trustyou.in';
							$othercode=html_entity_decode($metadata[0]->othercode);
							
							desktopHeader($ptitle,$kwords,$desc,$pageName,$canonicalurl,$othercode);
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
				
			//---------------------------------- Property Featured Data Function Starts ---------------------------------//
			
			var $feature;
			var $type;
			var $custID;
			var $dataOrder;
			
			function getFeatureProperty($featured,$customerID,$datOrder)
				{
					$this ->feature = $featured;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyTable where feature='$featured' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$featurePropData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$featurePropData = $featurePropData1;
							
							return $featurePropData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//----------------------------------- Property Featured Data Function Ends -----------------------------------//
			
			
			//---------------------------------- Property Featured Image Function Starts ---------------------------------//
			
			var $propertyID;
			var $imageType;
			var $roomID;
			function getPropertyImage($propId,$imgTyp,$roomId,$datOrder)
				{
					$this ->propertyID = $propId;
					$this ->imageType = $imgTyp;
					$this ->roomID = $roomId;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyImages where propertyID='$propId' and imageType='$imgTyp' and roomID='$roomId' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$PropImageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$PropImageData = $PropImageData1;
							
							return $PropImageData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//---------------------------------- Property Featured Image Function Ends ----------------------------------//
			
			
			//----------------------------------- Property Type Fetch Function Starts -----------------------------------//
			
			var $typeName;
			function getPropertyType($typNam,$customerID)
				 {
					$this ->typeName = $typNam;
					$this ->custID = $customerID;
					
					$sql = "select * from tbl_property_type where type_name='$typNam' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$PropTypeData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$PropTypeData = $PropTypeData1[0];
							
							return $PropTypeData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//----------------------------------- Property Type Fetch Function Ends -----------------------------------//
			
			
			//-------------------------------- Property Amenities Fetch Function Starts -------------------------------//
			
			var $roomAmenties;
			function getPropertyAmenity($roomAmenties_name)
				 {
					$this ->roomAmenties = $roomAmenties_name;
					
					$sql = "select * from roomAmenties where roomAmenties_name='$roomAmenties_name'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$roomAmenityData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$roomAmenityData = $roomAmenityData1;
							
							return $roomAmenityData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//-------------------------------- Property Amenities Fetch Function Ends -------------------------------//
			
			
			
			//---------------------------------- Property Room Fetch Function Starts ---------------------------------//
			
			function getPropertyRoom($propId,$stat,$customerID,$datOrder)
				 {
					$this ->propertyID = $propId;
					$this ->status = $stat;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyRoom where propertyID='$propId' and status='$stat' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$propertyRoom1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$propertyRoom = $propertyRoom1;
							
							return $propertyRoom;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//---------------------------------- Property Room Fetch Function Ends ---------------------------------//
			
			
			//------------------------------------ Property Q & A Function Starts ----------------------------------//
			
			function getQuesAns($propId,$stat,$customerID,$datOrder)
				 {
					$this ->propertyID = $propId;
					$this ->status = $stat;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from quesAns where propertyID='$propId' and status='$stat' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$fetchQues1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$fetchQues = $fetchQues1;
							
							return $fetchQues;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//------------------------------------ Property Q & A Function Ends ----------------------------------//
			
			
			
			//---------------------------------- Similar Property Fetch Function Ends ---------------------------------//
			
			function getSimilarProperty($stat,$customerID,$datOrder)
				{
					$this ->status = $stat;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyTable where status='$stat' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$similarPropData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$similarPropData = $similarPropData1;
							
							return $similarPropData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//----------------------------------- Similar Property Fetch Function Ends -----------------------------------//
			
		}
	//---------------------------------------------------------------- Class For Repeated functions ends (Main Front End Class) ----------------------------------------------------------------//
	
	
	
	//-------------------------------------------------------------------------- Class For Home Page functions starts -------------------------------------------------------------------------//
	
	class indexData extends websiteMain
		{
				
			//-------------------------------------- Index Page Data Function Starts -----------------------------------//
			
			var $lancode;
			
			function getHomeContent($lanCode,$type,$customerID)
				{
					$this ->lancode = $lanCode;
					$this ->type = $type;
					$this ->custID = $customerID;
					
					$sql = "select * from homeContent where lan_code='$lanCode' and type='$type' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$homeData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$homeData = $homeData1[0];
							
							return $homeData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//------------------------------------------ Index Page Data Function Ends -----------------------------------//
			
			
			//---------------------------------------- Index Page Slider Function Starts ---------------------------------//
			
			function getHomeSlider($typ,$customerID)
				{
					$this ->type = $typ;
					$this ->custID = $customerID;
					
					$sql = "select * from HomeSlider where type='$typ' and customerID='$customerID' order by slno asc";
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$homeSlider1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$homeSlider = $homeSlider1;
							
							return $homeSlider;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
				
			//------------------------------------------ Index Page Slider Function Ends ---------------------------------//
		
		}
	//--------------------------------------------------------------------------- Class For Home Page functions ends --------------------------------------------------------------------------//
	
	
	
	//--------------------------------------------------------------------------- Class For Listing functions starts --------------------------------------------------------------------------//
	
	class listingData extends websiteMain
		{
				
			//-------------------------------------- Index Page Data Function Starts -----------------------------------//
			
			var $cityUrl;
			
			function getListingContent($cityURL,$stat,$customerID)
				{
					$this ->cityUrl = $cityURL;
					$this ->status = $stat;
					$this ->custID = $customerID;
					
					$sql = "select * from city_url where cityUrl='$cityURL' && status='$stat' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$serviceaptData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$serviceaptData = $serviceaptData1[0];
							
							return $serviceaptData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//------------------------------------------ Index Page Data Function Ends -----------------------------------//

			
			
			//------------------------------------------ Property Fetch Function Starts ----------------------------------//
			
			var $cityName;
			var $table_prop_Type;
			function getPropertyDetails($customerID,$cityNam,$stat,$tbl_prop_typ,$datOrder)
				{
					$this ->custID = $customerID;
					$this ->cityName = $cityNam;
					$this ->status = $stat;
					$this ->table_prop_Type = $tbl_prop_typ;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyTable where customerID='$customerID' and cityName='$cityNam' and status='$stat' and tbl_property_type='$tbl_prop_typ' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$getServProp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$getServProp = $getServProp1;
							
							return $getServProp;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
				
			//-------------------------------------------- Property Fetch Function Ends ---------------------------------//
		
		}
	//--------------------------------------------------------------------------- Class For Listing functions ends -------------------------------------------------------------------------//
	
	
	
	//----------------------------------------------------------------------- Class For Property Page functions starts ---------------------------------------------------------------------//
	
	class propertyData extends websiteMain
		{
				
			//------------------------------------ Fetching Property Data Function Starts ---------------------------------//
			var $propertyUrl;
			function getFeatureProperty($propertyURL,$customerID,$stat)
				{
					$this ->propertyUrl = $propertyURL;
					$this ->custID = $customerID;
					$this ->status = $stat;
					
					$sql = "select * from propertyTable where propertyURL='$propertyURL' and customerID='$customerID' and status='$stat'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$PropDetData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$PropDetData = $PropDetData1[0];
							
							return $PropDetData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//---------------------------------------- Fetching Property Data Function Ends ---------------------------------//
			
			
			
			//---------------------------------- Property Featured Image Function Starts ---------------------------------//
		
			var $featureStatus;
			function getPropertyFeaturedImage($propId,$featureStat,$imgTyp,$roomId,$datOrder)
				{
					$this ->propertyID = $propId;
					$this ->featureStatus = $featureStat;
					$this ->imageType = $imgTyp;
					$this ->roomID = $roomId;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyImages where propertyID='$propId' and featureStat='$featureStat' and imageType='$imgTyp' and roomID='$roomId' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$PropFeatureImageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$PropFeatureImageData = $PropFeatureImageData1[0];
							
							return $PropFeatureImageData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//---------------------------------- Property Featured Image Function Ends ----------------------------------//
			
			
			
			//----------------------------- Room Current Lowest Price Fetch Function Starts -----------------------------//
		
			var $dailyRoomDiscount;
			var $orgRoomPrice;
			function getRoomLowestPrice($roomid,$propid,$dailyroomdisc,$org_roomprice)
				{
					$this ->dailyRoomDiscount = $dailyroomdisc;
					$this ->orgRoomPrice = $org_roomprice;
					$this ->propertyID = $propid;
					$this ->roomID = $roomid;
								
					$datetoday1 = date('d');
					$datetoday1++;
					$datetoday = $datetoday1-1;
					
					$monthtoday = date('m');
					$yeartoday = date('Y');
					
					$sql = "select `$datetoday` from manage_rate where room_id='$roomid' and property_id='$propid' and month='$monthtoday' and year='$yeartoday'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$priceshowdata1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$priceshowdata = $priceshowdata1[0];
							$countRate = count($priceshowdata1);
							
							if($countRate=="0" || $priceshowdata[$datetoday]==null)
								{
									// ------------------- Discount Calculation Starts -----------------------------------//
									
									$fororgprice = ($dailyroomdisc/100)*$org_roomprice;
									$fororgfinalprice = $org_roomprice-$fororgprice;
									
									// ------------------- Discount Calculation Ends -----------------------------------//
									
									$rateValue .= '<del><i class="fa fa-inr rsIcn"></i> '.$org_roomprice.'</del>';
									$rateValue .= '<div itemprop="offers" itemscope itemtype="http://schema.org/Offer"><meta itemprop="priceCurrency" content="INR">  <i class="fa fa-inr rsIcn" aria-hidden="true"></i> <span itemprop="price">'.$fororgfinalprice.' / night*</span></div>';
									
									return $rateValue;
								}
							else
								{
									// ------------------- Discount Calculation Starts -----------------------------------//
									
									$forinventoryprice = ($dailyroomdisc/100)*$priceshowdata[$datetoday];
									$forinventoryfinalprice = $priceshowdata[$datetoday]-$forinventoryprice;
									
									// ------------------- Discount Calculation Ends -----------------------------------//
									
									$rateValue .= '<del><i class="fa fa-inr rsIcn"></i> '.$priceshowdata[$datetoday].'</del>';
									$rateValue .= '<div itemprop="offers" itemscope itemtype="http://schema.org/Offer"><meta itemprop="priceCurrency" content="INR">  <i class="fa fa-inr rsIcn" aria-hidden="true"></i> <span itemprop="price">'.$fororgfinalprice.' / night*</span></div>';
									
									return $rateValue;
								}
							
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//----------------------------- Room Current Lowest Price Fetch Function Ends -----------------------------//
			
			
			//---------------------------------- Property Room Fetch Function Starts ---------------------------------//
			
			function getPropertySlideRoom($propId,$stat,$customerID,$roomId,$datOrder)
				 {
					$this ->propertyID = $propId;
					$this ->status = $stat;
					$this ->custID = $customerID;
					$this ->roomID = $roomId;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from propertyRoom where propertyID='$propId' and status='$stat' and customerID='$customerID' and roomID='$roomId' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$propertySlideRoom1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$propertySlideRoom = $propertySlideRoom1;
							
							return $propertySlideRoom;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//---------------------------------- Property Room Fetch Function Ends ---------------------------------//
		
		}
	//------------------------------------------------------------------------ Class For Property Page functions ends ---------------------------------------------------------------------//
	
	
	
	
	//----------------------------------------------------------------------- Class For Static Pages functions starts ---------------------------------------------------------------------//
	
	class staticPageData extends websiteMain
		{
			
			//------------------------------------ Fetching About Us Data Function Starts ---------------------------------//
			var $staticPageUrl;
			function aboutUsData($statPagUrl,$customerID)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					
					$sql = "select * from static_page_tbl where page_url='$statPagUrl' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$aboutUsData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$aboutUsData = $aboutUsData1[0];
							
							return $aboutUsData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//-------------------------------------- Fetching About Us Data Function Ends -------------------------------//
			
			
			//------------------------------------- Fetching Team Members Data Function Starts ------------------------------//
			
			function teamMembersData($customerID,$datOrder)
				{
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from team_members where customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$teamMemData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$teamMemData = $teamMemData1;
							
							return $teamMemData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//--------------------------------------- Fetching Team Members Data Function Ends -----------------------------//
			
			
			//--------------------------------------- Fetching Contact Us Data Function Starts -----------------------------//
			
			function contactUsData($statPagUrl,$customerID)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					
					$sql = "select * from static_page_contactus where page_url='$statPagUrl' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$contactUsData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$contactUsData = $contactUsData1[0];
							
							return $contactUsData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//--------------------------------------- Fetching Contact Us Data Function Ends ---------------------------//
			
			
			
			//--------------------------------------- Fetching Contact Us Data Function Starts -----------------------------//
			
			function foodData($statPagUrl,$customerID,$datOrder)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from static_page_food where page_url='$statPagUrl' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$foodData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$foodData = $foodData1;
							
							return $foodData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//--------------------------------------- Fetching Contact Us Data Function Ends ---------------------------//
			
			
			
			//------------------------------------- Fetching Our Clients Data Function Starts ---------------------------//
			
			function ourClientsData($statPagUrl,$customerID)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					
					$sql = "select * from static_page_clientmain where page_url='$statPagUrl' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$ourClientData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$ourClientData = $ourClientData1[0];
							
							return $ourClientData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//------------------------------------- Fetching Our Clients Data Function Ends ---------------------------//
			
			
			
			//--------------------------------------- Fetching Why Us Data Function Starts ----------------------------//
			
			function whyUsData($statPagUrl,$customerID)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					
					$sql = "select * from static_page_whyperch where page_url='$statPagUrl' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$whyUsData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$whyUsData = $whyUsData1[0];
							
							return $whyUsData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//--------------------------------------- Fetching Why Us Data Function Ends ----------------------------//
			
			
			
			//------------------------------------- Fetching Policies Data Function Starts ----------------------------//
			
			function policies($statPagUrl,$customerID,$datOrder)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from static_page_policy where page_url='$statPagUrl' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$policiesData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$policiesData = $policiesData1;
							
							return $policiesData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//------------------------------------- Fetching Policies Data Function Ends ----------------------------//
			
			
			
			//----------------------------------- Fetching Client-Sub Data Function Starts ----------------------------//
			
			function clientSubData($statPagUrl,$customerID)
				{
					$this ->staticPageUrl = $statPagUrl;
					$this ->custID = $customerID;
					
					$sql = "select * from static_page_clientsub where page_url='$statPagUrl' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$clientSubData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$clientSubData = $clientSubData1[0];
							
							return $clientSubData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//----------------------------------- Fetching Client-Sub Data Function Ends ----------------------------//
		}
		
	//----------------------------------------------------------------------- Class For Static Pages functions ends ---------------------------------------------------------------------//
	
	
	
	//--------------------------------------------------------------------------- Class For Listing functions starts --------------------------------------------------------------------------//
	
	class masterFileFunctions
		{	
			private $propertyUrl;
			private $status;
			private $custID;
			private $cityUrl;
			private $pageUrl;
			
			//------------------------------------ Fetching Property Data Function Starts ---------------------------------//
			
			function getFeatureProperty($propertyURL,$customerID,$stat)
				{
					$this ->propertyUrl = $propertyURL;
					$this ->custID = $customerID;
					$this ->status = $stat;
					
					$sql = "select * from propertyTable where propertyURL='$propertyURL' and customerID='$customerID' and status='$stat'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$PropDetData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$PropDetData = $PropDetData1[0];
							
							return $PropDetData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//---------------------------------------- Fetching Property Data Function Ends ---------------------------------//
			

			
			//-------------------------------------- Index Page Data Function Starts -----------------------------------//
			
			function getListingContent($cityURL,$stat,$customerID)
				{
					$this ->cityUrl = $cityURL;
					$this ->status = $stat;
					$this ->custID = $customerID;
					
					$sql = "select * from city_url where cityUrl='$cityURL' && status='$stat' and customerID='$customerID'";
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$serviceaptData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$serviceaptData = $serviceaptData1[0];
							
							return $serviceaptData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//------------------------------------------ Index Page Data Function Ends -----------------------------------//
			
			
			
			//---------------------------------- Similar Property Fetch Function Ends ---------------------------------//
			
			function getStaticPages($pagurl,$stat,$typ,$customerID,$datOrder)
				{
					$this ->pageUrl = $pagurl;
					$this ->status = $stat;
					$this ->type = $typ;
					$this ->custID = $customerID;
					$this ->dataOrder = $datOrder;
					
					$sql = "select * from static_pages where page_url='$pagurl' and status='$stat' and type='$typ' and customerID='$customerID' ".$datOrder;
					
					try
						{
							$DB = new connectDB();
							$DB = $DB->connect();
							
							$exQuey = $DB->query($sql);
							$staticPagData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
							$DB = null;
							
							$staticPagData = $staticPagData1[0];
							print_r($staticPagData1);
							die;
							
							return $staticPagData;
						}
					catch(PDOExecption $e)
						{
							echo "{'error':'{'text':'.$e->getMessage().'}'";				
						}
				}
			
			//----------------------------------- Similar Property Fetch Function Ends -----------------------------------//
		
		}
	//--------------------------------------------------------------------------- Class For Listing functions ends -------------------------------------------------------------------------//
	
?>