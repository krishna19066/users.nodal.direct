<?php
if (!isset($_SESSION)) {
    session_start();
}
ob_start();

date_default_timezone_set("Asia/Kolkata");

include SITE_URL_PATH . "include/config.php";
include SITE_URL_PATH . "include/common-functions.php";
include SITE_URL_PATH . "include/return_functions.php";

// include "../sitepanel/admin-function.php";

function check_session($sess_name, $REDIRECT_URL) {
    if (!strlen($_SESSION[$sess_name])) {
        header("Location:$REDIRECT_URL");
        exit;
    }
}
?>

<?php

//----------------------------------------------------------- Class For Repeated functions starts (Main CMS Class) ------------------------------------------------------------//
class adminFunction {

    var $custID;
    var $propertyID;
    var $roomId;
    var $pageUrl;
    //---------------------------------- Fetch New Booking Number Function Starts ---------------------------------//

    var $checDate;

    function newBookingNumber($cheDat, $customerID) {
        $this->checDate = $cheDat;
        $this->custID = $customerID;
        $sql = "select * from manage_booking where date='$cheDat' and customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $newBookNumData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $newBookNumData = $newBookNumData1;
            return $newBookNumData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Fetch New Booking Number Function Ends ---------------------------------//
    //---------------------------------- Fetch New Inquiries Number Function Starts ---------------------------------//


    var $views;

    function newInquiryNumber($view, $customerID) {
        $this->views = $view;
        $this->custID = $customerID;

        $sql = "select slno from inquiry where view='$view' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $inquiryNumData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $inquiryNumData = $inquiryNumData1;
            return $inquiryNumData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getNewFeature() {
        $sql = "select * from new_update_feature where status='N' order by id desc limit 10";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $discountData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $discountData = $discountData1;
            return $discountData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateNewChangeStatus($id) {
        $sql = "update new_update_feature set status='Y' where id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Fetch New Inquiries Number Function Starts ---------------------------------//
    //------------------------------------ Fetch Inquiries Data Function Starts -----------------------------------//
    var $commentStat;
    var $dataorder;

    function getInquiryData($comStat, $customerID, $datOrder) {
        $this->commentStat = $comStat;
        $this->custID = $customerID;
        $this->dataorder = $datOrder;
        $sql = "select * from inquiry where com_stat='$comStat' and customerID='$customerID' " . $datOrder;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $inquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $inquiryData = $inquiryData1;
            return $inquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Fetch Inquiries Data Function Starts -----------------------------------//
    //-------------------------------------- Display User Data Function Starts -------------------------------------//

    var $userId;

    function getUserData($user_id, $customerID) {
        $this->userId = $user_id;
        $this->custID = $customerID;
        $sql = "select * from admin_details where user_id='$user_id' and customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $profilePicData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $profilePicData = $profilePicData1[0];
            return $profilePicData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------------------- Display User Data Function Ends -------------------------------------//
    //------------------------------------- Fetch Language Data Function Starts ------------------------------------//

    var $langCond;

    function getLangData($customerID, $lanCon) {
        $this->custID = $customerID;
        $this->langCond = $lanCon;

        $sql = "select * from lang_sudomian where customerID='$customerID' and $lanCon";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $langData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $langCnt = count($langData1);

            if ($langCnt == "0") {
                
            } elseif ($langCnt == "1") {
                $langData_se = $langData1[0];

                $langData = $langData_se->langcode;
            } else {
                $langData_se = $langData1;

                $lanconc = '';
                foreach ($langData_se as $lanrow) {
                    $lanconc .= $lanrow->langcode . "^";
                    $langData = $lanconc;
                }
            }

            return $langData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Fetch Language Data Function Ends -----------------------------------//
    //-------------------------------------- Get User IP Address Function Starts -------------------------------------//

    function getUserIP() {
        try {
            $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else if ($_SERVER['HTTP_X_FORWARDED']) {
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            } else if ($_SERVER['HTTP_FORWARDED_FOR']) {
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            } else if ($_SERVER['HTTP_FORWARDED']) {
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            } else if ($_SERVER['REMOTE_ADDR']) {
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            } else {
                $ipaddress = 'UNKNOWN';
            }

            return $ipaddress;
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------------------- Get User IP Address Function Ends -------------------------------------//
    //-------------------------------------- Connect Cloudinary User Function Starts -------------------------------------//

    function connectCloudinaryAccount($customerID) {
        $this->custID = $customerID;

        require '../Cloudinary.php';
        require '../Uploader.php';
        require '../Api.php';

        $sql = "select * from admin_details where customerID='$customerID' order by slno asc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cloudinaryCredentials1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cloudinaryCredentials = $cloudinaryCredentials1[0];

            Cloudinary::config(array(
                "cloud_name" => $cloudinaryCredentials->cloud_name,
                "api_key" => $cloudinaryCredentials->api_key,
                "api_secret" => $cloudinaryCredentials->api_secret
            ));

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------------------- Connect Cloudinary User Function Ends -------------------------------------//
    //------------------------------------ Get Full Month Name Function Starts -----------------------------------//

    var $cuurentMonth;

    function getMonthFullName($passMonth) {
        $this->cuurentMonth = $passMonth;

        try {
            if ($passMonth == "01") {
                $fullMonthName = "January";
            }
            if ($passMonth == "02") {
                $fullMonthName = "February";
            }
            if ($passMonth == "03") {
                $fullMonthName = "March";
            }
            if ($passMonth == "04") {
                $fullMonthName = "April";
            }
            if ($passMonth == "05") {
                $fullMonthName = "May";
            }
            if ($passMonth == "06") {
                $fullMonthName = "June";
            }
            if ($passMonth == "07") {
                $fullMonthName = "July";
            }
            if ($passMonth == "08") {
                $fullMonthName = "August";
            }
            if ($passMonth == "09") {
                $fullMonthName = "September";
            }
            if ($passMonth == "10") {
                $fullMonthName = "October";
            }
            if ($passMonth == "11") {
                $fullMonthName = "November";
            }
            if ($passMonth == "12") {
                $fullMonthName = "December";
            }

            return $fullMonthName;
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Full Month Name Function Ends -----------------------------------//
}

//----------------------------------------------------------- Class For Graphs of Dashboard ------------------------------------------------------------//

class inquiryGraph {

    var $month;
    var $custId;

    //------------------------------------ Inquiry Graph Function Starts -----------------------------------//

    function getInquiryGraphData($mon, $customerID) {
        $this->month = $mon;
        $this->custId = $customerID;

        $year_org = date('Y');

        $sql = "select recvDate from inquiry where year='$year_org' and month='$mon' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $inquiryMonData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $inquiryMonData = count($inquiryMonData1);

            return $inquiryMonData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Inquiry Graph Function Ends -----------------------------------//	
    //---------------------------------- Site Visit Graph Function Starts -------------------------------//

    var $week;

    function getSiteVisitGraphData($weeks, $customerID) {
        $this->week = $weeks;
        $this->custId = $customerID;

        $sql = "select * from google_analytic where week='$weeks' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $siteVisitData2 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $siteVisitData1 = $siteVisitData2[0];
            $siteVisitData = $siteVisitData1->sess;

            return $siteVisitData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Site Visit Graph Function Ends -------------------------------//
}

//----------------------------------------------------------- Class For Index Page of CMS ------------------------------------------------------------//

class indexData extends adminFunction {

    var $password;
    var $userType;

    //------------------------------------ Index Page Data Function Starts -----------------------------------//

    function authUserLogin($userid, $passwrd, $usertype) {
        $this->userId = $userid;
        $this->password = $passwrd;
        $this->userType = $usertype;

        $sql = "select slno,last_login,user_type,user_id,customerID,email from admin_details where user_id='$userid' and pwd='$passwrd' and user_type='$usertype'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $indexpageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $indexpageData = $indexpageData1[0];

            return $indexpageData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function authUserLogin22($userid, $passwrd, $usertype) {
        $this->userId = $userid;
        $this->password = $passwrd;
        $this->userType = $usertype;

        $sql = "select * from admin_details where user_id='$userid' and pwd='$passwrd' and user_type='$usertype' and (mode='D' or mode='V')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $indexpageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $indexpageData = $indexpageData1[0];

            return $indexpageData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function authSubUserLogin($userid, $passwrd) {
        $sql = "select * from add_user where user_id='$userid' AND password='$passwrd'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $indexpageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $indexpageData = $indexpageData1[0];
            return $indexpageData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getDemoUserLogin() {
        $sql = "select * from admin_details where mode='V'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $indexpageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $indexpageData = $indexpageData1[0];

            return $indexpageData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertDemoUserData($email) {
        $sql = "insert into  demo_users(email) values('$email')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateLoginLg = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Index Page Data Function Ends -----------------------------------//
    //------------------------------------ Updating Login Log Function Starts -----------------------------------//

    var $ipAddress;
    var $currentDate;
    var $currentTime;

    function updateLoginLog($userid, $ipAddr, $currDate, $currTime, $customerID) {
        $this->userId = $userid;
        $this->ipAddress = $ipAddr;
        $this->currentDate = $currDate;
        $this->currentTime = $currTime;
        $this->custID = $customerID;

        $sql = "insert into log_table(customerID,user_id,ipaddr,curr_date,curr_time) values('$customerID','$userid','$ipAddr','$currDate','$currTime')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateLoginLg = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Updating Login Log Function Ends -----------------------------------//
    //------------------------------------ Updating Login Time in Admin Details Function Starts -----------------------------------//

    function updateAdmindetail($userid, $passwrd, $usertype, $currDate, $currTime, $customerID) {
        $this->userId = $userid;
        $this->password = $passwrd;
        $this->userType = $usertype;
        $this->currentDate = $currDate;
        $this->currentTime = $currTime;
        $this->custID = $customerID;

        $sql = "update admin_details set last_login='$currDate' where user_id='$userid' and pwd='$passwrd' and user_type='$usertype' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Updating Login Time in Admin Details Function Ends -----------------------------------//
    //------------------------------------ Sending Mail Function Starts -----------------------------------//

    var $toMail;

    function sendLoginMail($userid, $ipAddr, $currDate, $currTime, $customerID, $mailerAddr) {
        $this->userId = $userid;
        $this->ipAddress = $ipAddr;
        $this->currentDate = $currDate;
        $this->currentTime = $currTime;
        $this->custID = $customerID;
        $this->toMail = $mailerAddr;

        try {
            $subject2 = "Regarding Login Details";
            $toBeSentMail = '
												<div style="border:1px solid #9e9a9a;height:auto;width:500px;padding:20px;background:rgb(242, 255, 250);padding-bottom:50px;">
													<p style="color:#08a3bd;font-size:26px;"> Dear Admin, </p><br>

													<p style="font-size:20px;color:#607D8B;"> Please Review the last login details. </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> User ID : <font style="color:#FF5722;"> ' . $userid . ' </font> </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> IP Address : <font style="color:#FF5722;"> ' . $ipAddr . ' </font> </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> Date Of Login : <font style="color:#FF5722;"> ' . $currDate . ' </font> </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> Time of Login : <font style="color:#FF5722;"> ' . $currTime . ' </font> </p>
													
													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> Customer ID : <font style="color:#FF5722;"> ' . $customerID . ' </font> </p><br>

													<p style="color:#607D8B;font-size:20px;"> Thank You </p><br><br>

													<p style="color:#607D8B;font-size:20px;"> Regards </p>

													<p style="color:#607D8B;font-size:20px;"> Nodal.Direct Team </p>
												</div>';

            $to_mail = $mailerAddr;
            $from = "support@nodal.direct";


            $headers = "From:" . $from . "\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-Type: text/html; charset=utf-8\r\n";
            $headers.= "X-Priority: 1\r\n";
            mail($to_mail, $subject2, $toBeSentMail, $headers);
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Sending Mail Function Ends -----------------------------------//
}

//----------------------------------------------------------- Class For Welcome Page of CMS ------------------------------------------------------------//

class welcomeData extends adminFunction {

    //------------------------------------ Index Page Data Function Starts -----------------------------------//

    function getRecentUsers($customerID, $datOrder, $user_id) {
        $this->custID = $customerID;
        $this->dataorder = $datOrder;
        if ($user_id) {
            $sql = "select * from add_user where customerID='$customerID' and user_id='$user_id' ";
        } else {
            $sql = "select * from admin_details where customerID='$customerID' " . $datOrder;
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $recentUsers1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $recentUsers = $recentUsers1;

            return $recentUsers;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Index Page Data Function Ends -----------------------------------//
}

//----------------------------------------------------------- Class For Property Page of CMS ------------------------------------------------------------//

class propertyData extends adminFunction {

    function InsertData($sql_query) {
        $sql = $sql_query;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getTableData($sql_query) {
        $sql = $sql_query;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAllCountryListing() {
        $sql = "select * from countries";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getInvoiceData($customerId) {
        $sql = "select * from billing_invoice where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWebsiteLanguage($customerId, $select_language) {
        $sql = "update admin_details set language='$select_language' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAllCityListing($countryID) {
        $sql = "select * from cities where country_id='$countryID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllServiceData($countryID) {
        $sql = "select * from places_table where customerID='$countryID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
		function GetAllServiceDataWithCity($customerId,$cityName){
		$sql = "select * from places_table where customerID='$customerId' and cityName='$cityName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
	}
	function getAllServiceCityListing($customerId){
		 $sql = "select * from places_table where customerID='$customerId' GROUP BY cityName";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
	}

    function GetServiceDataWithServiceID($customerID, $service_Id) {
        $sql = "select * from places_table where customerID='$customerID' and id ='$service_Id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateServiceData($customerId, $service_Id, $serviceName, $serviceURL, $countryName, $cityName, $countryID, $city_id, $serviceType, $rate_type, $rate, $description) {
        $sql = "update places_table set serviceName='$serviceName',serviceURL='$serviceURL',countryName='$countryName',cityName='$cityName',country_id='$countryID',city_id='$city_id',service_type='$serviceType',rate='$rate',rate_type='$rate_type',description='$description' where id='$service_Id' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetcountryWithID($countryID) {
        $sql = "select * from countries where id = '$countryID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetcityWithID($city_id) {
        $sql = "select * from cities where id = '$city_id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAllServiceType() {
        $sql = "select * from service_type ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetCookiesBannerData($customerId) {
        $sql = "select * from cookies_banner where customerID='$customerId' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertCookiesBanner($customerId, $cookies_content, $link, $status) {
        $sql = "insert into cookies_banner set customerID='$customerId', text='$cookies_content',link='$link',status='$status'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateCookiesBanner($customerId, $cookies_content, $link, $status) {
        $sql = "update cookies_banner set text='$cookies_content', link='$link', status='$status' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddService($customerId, $serviceName, $serviceURL, $countryName, $cityName, $countryID, $city_id, $serviceType, $rate_type, $rate, $description) {
        $sql = "insert into places_table set customerID='$customerId', serviceName='$serviceName',serviceURL='$serviceURL',countryName='$countryName',cityName='$cityName',country_id='$countryID',city_id='$city_id',service_type='$serviceType',rate='$rate',rate_type='$rate_type',description='$description'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateData($sql_query) {
        $sql = $sql_query;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteData($sql_query) {
        $sql = $sql_query;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    //---------------------Add Google sheet Url------------------------------------->
    function AddGoogleSheetUrl($customerId, $propertyName, $sheetUrl, $folderUrl) {
        $sql = "insert into googlesheet set customerID='$customerId', PropertyName='$propertyName',sheetUrl='$sheetUrl',folderUrl='$folderUrl'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddVideoReviewData($customerId, $video_review_link, $categoryName) {
        $sql = "insert into video_review set customerID='$customerId', review_link='$video_review_link',category_name='$categoryName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllVideoReviewData($customerId) {
        $sql = "select * from video_review where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddTestionialCategory($customerId, $categoryName) {
        $sql = "insert into testimonial_category set customerID='$customerId', category_name='$categoryName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getServiceDataWithID($customerId, $service_Id) {
        $sql = "select * from service_photo where customerID='$customerId' and service_id='$service_Id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddServicePhoto($customerId, $service_id, $timestmp, $alt_tag) {
        $sql = "insert into service_photo set customerID='$customerId', service_id='$service_id',imageUrl='$timestmp',alt_tag='$alt_tag'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function SetFeatureServicePhoto($feature, $imagesID, $customerId) {
        $sql = "update service_photo set feature='$feature' where id='$imagesID' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddExperienceCategory($customerId, $categoryName, $url, $content) {
        $sql = "insert into experience_category set customerID='$customerId',url='$url',content='$content', category_name='$categoryName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function SetFeatureExpPhoto($feature, $imagesID, $customerId) {
        $sql = "update experience_photo set feature='$feature' where id='$imagesID' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function deleteServicePhoto($imagesID, $customerId) {
        $sql = "DELETE FROM service_photo WHERE id='$imagesID' and customerID ='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function deleteExpPhoto($imagesID, $customerId) {
        $sql = "DELETE FROM experience_photo WHERE id='$imagesID' and customerID ='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getExpCategory($customerId) {
        $sql = "select * from experience_category where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getExpCategoryWithID($id, $customerId) {
        $sql = "select * from experience_category where customerID='$customerId' and id='$id'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateExpCategory($id, $customerId, $categoryName, $content, $url) {
        $sql = "update experience_category set url='$url',content='$content', category_name='$categoryName' where id='$id' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddExperienceData($customerId, $name, $categoryName, $city, $state, $rate, $from_date, $to_date, $exp_content, $slug_url) {
        $sql = "insert into add_experience set customerID='$customerId', category='$categoryName',name='$name',city='$city',state='$state',rate='$rate',from_date='$from_date',to_date='$to_date',exp_content='$exp_content',url='$slug_url'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllExperienceData($customerId) {
        $sql = "select * from add_experience where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddExperiencePhoto($customerId, $exp_Id, $timestmp, $alt_tag) {
        $sql = "insert into experience_photo set customerID='$customerId', exp_id='$exp_Id',imageURL='$timestmp',alt_tag='$alt_tag'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getExpreriencePhoto($exp_Id) {
        $sql = "select * from experience_photo where exp_id='$exp_Id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getExperienceDataWithID($customerId, $exp_id) {
        $sql = "select * from add_experience where id='$exp_id' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateExperienceData($customerId, $exp_Id, $name, $city, $state, $rate, $from_date, $to_date, $categoryName, $exp_content) {
        $sql = "update add_experience set name='$name',city='$city',state='$state',category = '$categoryName',rate='$rate',from_date='$from_date',to_date='$to_date',exp_content='$exp_content' where id='$exp_Id' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getExperienceCategory($customerId) {
        $sql = "select * from experience_category where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getTestionialCategory($customerId) {
        $sql = "select * from testimonial_category where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetTestimonialDataWithId($recordID) {
        $sql = "select * from testimonials where slno='$recordID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $testi_data = $data;
            return $testi_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateTestimonialData($recordID, $cond) {
        $sql = "update testimonials set name='$_REQUEST[name]',review='$_REQUEST[review]',detail='$_REQUEST[occupation]', category_name = '$_REQUEST[testimonial_cate]' $cond  where slno='$recordID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //----------------------Get Details from super Admin cms user----------------->
    function GetSupperAdminData($customerID) {
        $sql = "select * from superadmin_cmsUser where slno='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $data = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $cms_data = $data;
            return $cms_data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City Listing Function Starts -----------------------------------//

    function getCityList($customerID, $user_id) {
        $this->custID = $customerID;
        $this->dataorder = $datOrder;
        if ($user_id) {
            $sql = "select * from propertyTable where customerID='$customerID' and user_id='$user_id' group by cityName";
        } else {
            $sql = "select * from propertyTable where customerID='$customerID' group by cityName";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cityList = $cityList1;

            return $cityList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetPropertyDataWithId($propId) {
        $sql = "select * from propertyTable where propertyID = '$propId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cityList = $cityList1;

            return $cityList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City Listing Function Ends -----------------------------------//	
    //---------------------------- Get Property Number in a City Function Starts ---------------------------//

    var $cityName;

    function getPropertyCount($customerID, $cityNam, $user_id) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;
        if ($user_id) {
            $sql = "select propertyID from propertyTable where customerID='$customerID' and cityName='$cityNam' and user_id = '$user_id'";
        } else {
            $sql = "select propertyID from propertyTable where customerID='$customerID' and cityName='$cityNam'";
        }
        // echo $sql;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyCount99($customerID, $user_id) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;
        if ($user_id) {
            $sql = "select * from propertyTable where customerID='$customerID' and user_id='$user_id' group by cityName";
        } else {
            $sql = "select * from propertyTable where customerID='$customerID' group by cityName";
        }
        // echo $sql;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Number in a City Function Ends ---------------------------//	
    //---------------------------- Get Property Number in a City Function Starts ---------------------------//

    function getCityProperty($customerID, $cityNam, $user_id) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;
        if ($user_id) {
            $sql = "select * from propertyTable where cityName='$cityNam' and customerID='$customerID' and user_id='$user_id' and (status='Y'||status='N')";
        } else {
            $sql = "select * from propertyTable where cityName='$cityNam' and customerID='$customerID' and (status='Y'||status='N')";
        }
        // echo $sql;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Number in a City Function Ends ---------------------------//	
    //---------------------------- Get Property Details in a City by Type Function Starts ---------------------------//

    var $propertyType;

    function getCityPropertyByType($customerID, $cityNam, $propTypID) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;
        $this->propertyTypeID = $propTypID;

        if ($cityNam == 'All') {
            $passingValue = '';
        } else {
            $passingValue = "(cityName='" . $cityNam . "'||subCity='" . $cityNam . "') and ";
        }

        $sql = "select * from propertyTable where " . $passingValue . "customerID='$customerID' and tbl_property_type='$propTypID'";
        // echo $sql;
        // die;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getCityPropertyType($customerID, $cityName) {
        $sql = "select * from city_url where customerID='$customerID' and cityName='$cityName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getpropertyTypeCount($customerId, $cityName, $typeId) {
        $sql = "select * from propertyTable where customerID='$customerId' and cityName='$cityName' and tbl_property_type='$typeId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Details in a City by Type Function Ends ---------------------------//	
    //---------------------------- Get Image Count of a Property Function Starts ---------------------------//


    function getPropertyPhotoCount($propID, $customerID) {
        $this->propertyID = $propID;
        $this->custID = $customerID;

        $sql = "select imagesID from propertyImages where propertyID='$propID' and imageType='property' order by imagesID DESC limit 10";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityPropCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $cityPropCount = $cityPropCount1;

            return $cityPropCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getRoomPropertyPhotoCount($propID, $customerID) {
        $this->propertyID = $propID;
        $this->custID = $customerID;

        $sql = "select imagesID from propertyImages where propertyID='$propID' and imageType='room'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityPropCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $cityPropCount = $cityPropCount1;

            return $cityPropCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletePropertyImages($imagesID) {
        $sql = "DELETE FROM propertyImages WHERE imagesID='$imagesID' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Image Count of a Property Function Ends ---------------------------//	
    //---------------------------- Get Property Type Name Function Starts ---------------------------//

    var $propertyTypeID;

    function getPropertyTypeName($propTypID, $customerID) {
        $this->propertyTypeID = $propTypID;
        $this->custID = $customerID;

        $sql = "select * from tbl_property_type where type_id='$propTypID'";
        // echo $sql;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propType1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propType = $propType1;

            return $propType;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Type Name Function Ends ---------------------------//	
    //---------------------------- Get Total Booking Function Starts ---------------------------//

    var $propertyName;
    var $month;

    function getTotalBooking($mon, $propnam, $customerID, $datordr) {
        $this->month = $mon;
        $this->propertyName = $propnam;
        $this->custID = $customerID;
        $this->dataorder = $datordr;

        $year = date('Y');
        // $month = date('m');
        $sql = "select * from manage_booking where start_month='$mon' and start_year='$year' and property_enquired='$propnam' and customerID='$customerID'" . $datordr;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $totalBooking1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $totalBooking = $totalBooking1;

            return $totalBooking;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Type Name Function Ends ---------------------------//	
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getRoomDetail($propID) {
        $this->propertyID = $propID;

        $sql = "select * from propertyRoom where propertyID='$propID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomDetail1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomDetail = $roomDetail1;

            return $roomDetail;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getRoomDetailData($prop, $room) {
        // $this->propertyID = $propID;

        $sql = "select * from propertyRoom where propertyID='$prop' and roomID='$room'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomDetail1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomDetail = $roomDetail1;

            return $roomDetail;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateRoomRate($price_final, $propert, $roo) {
        $sql = "update propertyRoom set roomPriceINR='$price_final',rateType='$_REQUEST[rateType]' where propertyID='$propert' and roomID='$roo'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

//--------------------------------Update Food Menu Function------------------------------->
    function UpdateFoodMenuLink($propertyID, $file) {
        $food_menu_link = 'http://www.stayondiscount.com/sitepanel/' . $file['name'];
        $sql = "update propertyTable set foodmenu_link='$food_menu_link' where propertyID='$propertyID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateRoomRateData($paxcn, $pax, $propert, $roo) {
        $sql = "update propertyRoom set $paxcn='$pax' where propertyID='$propert' and roomID='$roo'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddHomePageVideoSlider($customerId, $timestmp) {
        $sql = "update admin_details set videoSlider='$timestmp' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateExtraRoom($extra_bed, $propert, $roo, $roomPriceINR) {
        $sql = "update propertyRoom set extra_bed_rate='$extra_bed',roomPriceINR='$roomPriceINR' where propertyID='$propert' and roomID='$roo'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePassword($customerId, $pass, $user_id) {
        if ($user_id) {
            $sql = "update add_user set password='$pass' where customerID='$customerId' and user_id='$user_id'";
        } else {
            $sql = "update admin_details set pwd='$pass' where customerID='$customerId'";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateUserDetails($customerId, $full_name, $phone, $occup, $about, $company, $user_id) {
        if ($user_id) {
            $sql = "update add_user set managerName='$full_name',mobile='$phone' where customerID='$customerId' and user_id='$user_id'";
        } else {
            $sql = "update admin_details set name='$full_name',phone='$phone',occupation='$occup',about='$about',companyName='$company' where customerID='$customerId'";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateUserPic($customerId, $nam, $user_id) {
        if ($user_id) {
            $sql = "update add_user set photo='$nam' where customerID='$customerId' and user_id='$user_id'";
        } else {
            $sql = "update admin_details set photo='$nam' where customerID='$customerId'";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateTripAdvisorLink($propertyID, $trip_advisor, $trip_advisor_2) {
        $sql = "update propertyTable set visirTrip_advisor='$trip_advisor',visirTrip_advisor_2='$trip_advisor_2' where propertyID='$propertyID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getAllPropTyp($customerID) {
        //$this->custID = $customerID;

        $sql = "select * from tbl_property_type";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allPropTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allPropTyp = $allPropTyp1;

            return $allPropTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getpropertyTypewithName($customerID, $typeName) {
        $sql = "select * from tbl_property_type where  type_name = '$typeName'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allPropTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allPropTyp = $allPropTyp1;

            return $allPropTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get City List Function Starts -----------------------------//

    function getAllCity($customerID) {
        $this->custID = $customerID;
        $sql = "select * from city_url where customerID='$customerID' GROUP BY cityName";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allCity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allCity = $allCity1;

            return $allCity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAllCityWithId($customerID, $cityId) {
        $sql = "select * from city_url where customerID='$customerID' and cityID='$cityId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allCity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allCity = $allCity1;

            return $allCity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateCityName($customerId, $cityName, $city_name) {
        $sql = "update city_url set cityName='$city_name' where cityName='$cityName' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateCityContent($cityId, $cond1, $cond2, $cond3, $cond4,$serv_cond1,$serv_cond2,$serv_cond3) {
        @extract($_REQUEST);
        $sql = "update city_url set h1sa='$h1',h2sa='$h2',h3sa='$h3',h2sacontent='$h2content',cityUrl='$type_url',samainhead1='$mainhead1',samainhead2='$mainhead2',samainhead3='$mainhead3',sasubhead1='$subhead1',sasubheadcontent1='$subheadcontent1',sasubhead2='$subhead2',sasubheadcontent2='$subheadcontent2',sasubhead3='$subhead3',sasubheadcontent3='$subheadcontent3',sasubhead4='$subhead4',sasubheadcontent4='$subheadcontent4',sacontent='$content',sacontentheading='$contentheading',saawardhead='$awardhead',saawardcontent='$awardcontent',saimage1Name='$saimg1content',saimage2Name='$saimg2content',saimage3Name='$saimg3content',savideohead='$videohead',savideocontent='$videocontent',samobilecontent1='$mobilecontent1',samobilecontent2='$mobilecontent2',savideourl='$videourl',service_head1='$service_head1',service_head2='$service_head2',service_head3='$service_head3',service_content1='$service_content1',service_content2='$service_content2',service_content3='$service_content3' $cond1 $cond2 $cond3 $cond4 $serv_cond1 $serv_cond2 $serv_cond3 where cityID='$cityId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateCityStatus($cityId, $status) {
        $sql = "update city_url set status='$status' where cityID='$cityId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddPropertyTypeData($customerId, $property_type) {
        // $sql = "INSERT INTO city_url set  customerID='$customerId',cityName ='$_REQUEST[cityName]', property_type='$property_type',status='Y' ON DUPLICATE KEY UPDATE property_type='$property_type'";
        //  $sql = "insert into city_url set  customerID='$customerId',cityName ='$_REQUEST[cityName]', property_type='$property_type',status='Y'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletePropertyCity($customerId, $cityName) {
        $sql = "DELETE FROM city_url WHERE customerID='$customerId' and cityName='$cityName' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get City List Function Ends -----------------------------//
    //------------------------------ Get Sub-City List Function Starts -----------------------------//

    function getAllSubCity($customerID) {
        $this->custID = $customerID;
        $sql = "select * from subcity_url where customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allSubCity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allSubCity = $allSubCity1;

            return $allSubCity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getSubCityWithID($subCityID) {
        $sql = "select * from subcity_url where subcityID='$subCityID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allSubCity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $allSubCity = $allSubCity1;
            return $allSubCity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Sub-City List Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getAllRoomTyp($customerID) {
        $this->custID = $customerID;
        $sql = "select * from roomType where customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allRoomTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allRoomTyp1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetRoomTypeWithID($roomTypeID) {
        $sql = "select * from roomType where apt_type_id='$roomTypeID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allRoomTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allRoomTyp1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getRoomTypesWithName($roomTypeItem) {
        $sql = "select * from roomType where apt_type_name='$roomTypeItem'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allRoomTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allRoomTyp1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyRoomType($recordID, $roomTypeItem) {
        $sql = "select * from propertyRoom where propertyID='$recordID' && roomType='$roomTypeItem'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allRoomTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allRoomTyp1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateRoomData($recordID, $roomID, $customerId, $businessType1) {
        $sql = "update propertyRoom set propertyID='$recordID',roomType='$_REQUEST[roomType]',roomOverview='$_REQUEST[roomOverview]',occupancy='$_REQUEST[occupancy]',room_no='$_REQUEST[room_num]',extra_bed='$_REQUEST[extra_bed]',roomRateType='$_REQUEST[roomRateType]',dailyroomDiscount='$_REQUEST[daily_dis]',roomPriceINR='$_REQUEST[roomPriceINR]',room_url='$_REQUEST[room_url]',roomAmenties='$businessType1' where propertyID='$recordID' && roomID='$roomID' && customerID='$customerId'";
        // echo $sql;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteRoomID($recordID) {
        $sql = "update propertyRoom set status='D' where roomID='$recordID'";
        // echo $sql;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteTestimonialData($recordID) {
        $sql = "DELETE FROM testimonials WHERE slno='$recordID' ";
        // echo $sql;
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePropertyRoomStatus($stat, $pro) {
        $sql = "update propertyRoom set status='$stat' where roomID='$pro'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddPropertyPhoto($timestmp, $alt_tag, $pro_type, $roo_id, $pro_id, $type,$img_seq) {
       echo  $sql = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type,img_resource) values('$timestmp','$alt_tag','$pro_type','$roo_id','$pro_id','$type','$img_seq')";
        
       try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    // Image resource
    function S3AddPropertyPhoto($name, $alt_tag, $pro_type, $roo_id, $pro_id, $type,$img_resource,$property_order) {
        $sql = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type,img_resource,property_order) values('$name','$alt_tag','$pro_type','$roo_id','$pro_id','$type','$img_resource','$property_order')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    // Insert Home Page Slider ---------------------------------------------------->

    function InsertHomePageSlider($customerId, $langua, $img_name, $type,$img_resource) {
        $sql = "insert into HomeSlider set customerID='$customerId', lan_code='en',hshead1='$_REQUEST[hshead1]',hshead2='$_REQUEST[hshead2]',linktext='$_REQUEST[linktext]',linkURL='$_REQUEST[linkURL]',hsimg='$img_name',type='$type',img_resource='$img_resource'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    // AWS S3
    function S3InsertHomePageSlider($customerId, $langua, $name, $type,$img_resource) {
        $sql = "insert into HomeSlider set customerID='$customerId', lan_code='en',hshead1='$_REQUEST[hshead1]',hshead2='$_REQUEST[hshead2]',linktext='$_REQUEST[linktext]',linkURL='$_REQUEST[linkURL]',hsimg='$name',type='$type',img_resource='$img_resource'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetHomepageSliderData($customerId) {
        $sql = "select * from HomeSlider where customerID='$customerId' order by image_sequence asc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allphoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allphoto1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    
    // AWS S3
    function S3GetHomepageSliderData($customerId) {
        $sql = "select * from HomeSlider where customerID='$customerId' and img_resource='S3' order by image_sequence asc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allphoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allphoto1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    

    function GetHomepageSliderVideo($customerId) {
        $sql = "select videoSlider from admin_details where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allphoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allphoto1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function HomeSliderWithID($recordID) {
        $sql = "select * from HomeSlider where slno='$recordID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allphoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allphoto1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateHomeSlider($cond) {
        $sql = "update HomeSlider set hshead1='$_REQUEST[hshead1]',hshead2='$_REQUEST[hshead2]',linktext='$_REQUEST[linktext]',linkURL='$_REQUEST[linkURL]',type='home' $cond where slno='$_REQUEST[slno]'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteHomeSlider($imagesID1) {
        $sql = "DELETE FROM HomeSlider WHERE slno='$imagesID1' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddFeaturedProperty($imagesID) {
        $sql = "update propertyImages set featureStat ='Y' where imagesID='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function RemoveFeaturedProperty($imagesID) {
        $sql = "update propertyImages set featureStat ='0' where imagesID='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddFeaturedPropertyRoom($imagesID, $status) {
        $sql = "update propertyImages set roomFeatureStat ='$status' where imagesID='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function RemoveFeaturedPropertyRoom($imagesID) {
        $sql = "update propertyImages set roomFeatureStat ='0' where imagesID='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateRoomStatus($roomID, $status) {
        $sql = "update propertyRoom set status ='$status' where roomID='$roomID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getAmenitiesList() {

        $sql = "select * from roomAmenties";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomAmenity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomAmenity = $roomAmenity1;

            return $roomAmenity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    var $googleMapUrl;
    var $trustYouUrl;
    var $tagLine;
    var $propertyFeature;
    var $serviceAmenities;
    var $safetyPolicies;
    var $inApartmentFacilty;
    var $kitchenFeatures;
    var $entertainmentFacility;

    function addNewProperty($customerId, $roomTypes99, $roomAmenities99) {
        $this->propertyName = $propnam;
        $this->googleMapUrl = $gmapUrl;
        $this->trustYouUrl = $trustUrl;
        $this->tagLine = $taglin;
        $this->propertyFeature = $propFeature;
        $this->serviceAmenities = $serviceAmnty;
        $this->safetyPolicies = $safetyPlcy;
        $this->inApartmentFacilty = $inAprtmntFclty;
        $this->kitchenFeatures = $kitchenFeture;
        $this->entertainmentFacility = $entertainment;
        $sql = "insert into propertyTable set customerID='$customerId', roomType='$roomTypes99', roomAmenties='$roomAmenities99', propertyName='$_REQUEST[propertyName]',gmapurl='$_REQUEST[gmapurl]',trustURL='$_REQUEST[trustURL]',tagLine='$_REQUEST[tagLine]',propertyfeatures='$_REQUEST[propertyfeatures]',servicesnamenities='$_REQUEST[servicesnamenities]',safetynsecurity='$_REQUEST[safetynsecurity]',inapartmentfacilities='$_REQUEST[inapartmentfacilities]',kitchenfeatures='$_REQUEST[kitchenfeatures]',entertainmentleisure='$_REQUEST[entertainmentleisure]',mobpropertyfeatures='$_REQUEST[propertyfeatures]',mobservicesnamenities='$_REQUEST[servicesnamenities]',mobsafetynsecurity='$_REQUEST[safetynsecurity]',mobinapartmentfacilities='$_REQUEST[inapartmentfacilities]',mobkitchenfeatures='$_REQUEST[kitchenfeatures]',mobentertainmentleisure='$_REQUEST[entertainmentleisure]',propertyURL='$_REQUEST[propertyURL]',booking_engine='$_REQUEST[bookingengiType]', bookingURL='$_REQUEST[bookingURL]', tbl_property_type='$_REQUEST[tbl_property_type]', address='$_REQUEST[address]',subCity='$_REQUEST[subCity]',cityName='$_REQUEST[cityName]', state='$_REQUEST[state]', country='$_REQUEST[country]', floorsno='$_REQUEST[floorsno]',totalRooms='$_REQUEST[totalRooms]', checkIN='$_REQUEST[checkIN]', checkOut='$_REQUEST[checkOut]', propertyInfo='$_REQUEST[propertyInfo]', propertyPolicy='$_REQUEST[propertyPolicy]', mobpropertyInfo='$_REQUEST[propertyInfo]', mobpropertyPolicy='$_REQUEST[propertyPolicy]',managerName='$_REQUEST[managerName]',videoURL='$_REQUEST[videoURL]', managerContactNO='$_REQUEST[managerContactNO]',propertyPhone='$_REQUEST[propertyPhone]',property_email='$_REQUEST[property_email]', nearBy='$_REQUEST[nearBy]', twitter_link='$_REQUEST[propertyTwitterLink]', facebook_link='$_REQUEST[propertyFacebookLink]', linkedin_link='$_REQUEST[propertyLinkedinLink]', google_link='$_REQUEST[propertyGoogleLink]',pg_type='$_REQUEST[pg_type]',displayMap='$_REQUEST[displayMap]',locality='$_REQUEST[locality]',feature='$_REQUEST[featured]',user_id='$_REQUEST[user_id]', status='Y'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateProperty($customerId, $roomTypes99, $roomAmenities99, $propId) {

        $sql = "update propertyTable set roomType='$roomTypes99', roomAmenties='$roomAmenities99', propertyName='$_REQUEST[propertyName]',gmapurl='$_REQUEST[gmapurl]',trustURL='$_REQUEST[trustURL]',tagLine='$_REQUEST[tagLine]',propertyfeatures='$_REQUEST[propertyfeatures]',servicesnamenities='$_REQUEST[servicesnamenities]',safetynsecurity='$_REQUEST[safetynsecurity]',inapartmentfacilities='$_REQUEST[inapartmentfacilities]',kitchenfeatures='$_REQUEST[kitchenfeatures]',entertainmentleisure='$_REQUEST[entertainmentleisure]',mobpropertyfeatures='$_REQUEST[propertyfeatures]',mobservicesnamenities='$_REQUEST[servicesnamenities]',mobsafetynsecurity='$_REQUEST[safetynsecurity]',mobinapartmentfacilities='$_REQUEST[inapartmentfacilities]',mobkitchenfeatures='$_REQUEST[kitchenfeatures]',mobentertainmentleisure='$_REQUEST[entertainmentleisure]',propertyURL='$_REQUEST[propertyURL]', bookingURL='$_REQUEST[bookingURL]', tbl_property_type='$_REQUEST[tbl_property_type]', address='$_REQUEST[address]',subCity='$_REQUEST[subCity]',cityName='$_REQUEST[cityName]', state='$_REQUEST[state]', country='$_REQUEST[country]', floorsno='$_REQUEST[floorsno]',totalRooms='$_REQUEST[totalRooms]', checkIN='$_REQUEST[checkIN]', checkOut='$_REQUEST[checkOut]', propertyInfo='$_REQUEST[propertyInfo]', propertyPolicy='$_REQUEST[propertyPolicy]', mobpropertyInfo='$_REQUEST[propertyInfo]', mobpropertyPolicy='$_REQUEST[propertyPolicy]',managerName='$_REQUEST[managerName]',videoURL='$_REQUEST[videoURL]', managerContactNO='$_REQUEST[managerContactNO]',propertyPhone='$_REQUEST[propertyPhone]',property_email='$_REQUEST[property_email]', nearBy='$_REQUEST[nearBy]', feature='$feature', twitter_link='$_REQUEST[propertyTwitterLink]', facebook_link='$_REQUEST[propertyFacebookLink]', linkedin_link='$_REQUEST[propertyLinkedinLink]', google_link='$_REQUEST[propertyGoogleLink]',pg_type='$_REQUEST[pg_type]',displayMap='$_REQUEST[displayMap]',locality='$_REQUEST[locality]', feature='$_REQUEST[featured]',booking_engine='$_REQUEST[bookingengiType]', status='Y' where propertyID = '$propId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addNewRooms($customerId, $roomAmenities99, $propertyID) {

        $sql = "insert into propertyRoom set propertyID='$propertyID', customerID='$customerId', roomType='$_REQUEST[roomType]', roomAmenties='$roomAmenities99', roomOverview='$_REQUEST[roomOverview]',occupancy='$_REQUEST[occupancy]',extra_bed='$_REQUEST[extra_bed]',room_no='$_REQUEST[room_num]',room_url='$_REQUEST[room_url]',roomRateType='$_REQUEST[roomRateType]',roomPriceINR='$_REQUEST[roomPriceINR]',dailyroomDiscount='$_REQUEST[daily_dis]'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addProPertyQuestion($customerId, $propertyID) {
        $sql = "insert into quesAns set propertyID='$propertyID', customerID='$customerId',question ='$_REQUEST[question]', answer='$_REQUEST[answer]',status='Y'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addCity($customerId, $property_type) {
        $sql = "insert into city_url set  customerID='$customerId',cityName ='$_REQUEST[cityName]', property_type='$property_type',status='Y'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addSubCity($customerId) {
        $sql = "insert into subcity_url set  customerID='$customerId',subCity ='$_REQUEST[SubcityName]', cityID='$_REQUEST[cityID]'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateSubCity($subCityID) {
        $sql = "update subcity_url set subCity ='$_REQUEST[SubcityName]',cityID='$_REQUEST[cityID]' where subcityID='$subCityID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteSubCity($subCityID) {
        $sql = "DELETE FROM subcity_url WHERE subcityID='$subCityID' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addPropertyTypes($customerId) {
        $sql = "insert into tbl_property_type set  customerID='$customerId',type_name ='$_REQUEST[type_name]'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetPropertyTypeWithID($proTypeID) {
        $sql = "select * from tbl_property_type where type_id='$proTypeID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allRoomTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $allRoomTyp = $allRoomTyp1;
            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePropertyType($proTypeID) {
        $sql = "update tbl_property_type set type_name ='$_REQUEST[type_name]' where type_id='$proTypeID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletePropertyType($proTypeID) {
        $sql = "DELETE FROM tbl_property_type WHERE type_id='$proTypeID' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function addRoomTypes($customerId) {
        $sql = "insert into roomType set  customerID='$customerId',apt_type_name ='$_REQUEST[room_typeName]'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateRoomTypes($roomTypeID) {
        $sql = "update roomType set apt_type_name ='$_REQUEST[room_typeName]' where apt_type_id='$roomTypeID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteRoomTypeID($roomTypeID) {
        $sql = "DELETE FROM roomType WHERE apt_type_id='$roomTypeID' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //---------------------------- Get Property Photo Function Starts ---------------------------//

    function getPropertyPhoto($propID) {
        $this->propertyID = $propID;
        $sql = "select * from propertyImages where propertyID='$propID' and imageType='property' and type='home' and property_order !='P2' and property_order !='P3' ORDER BY img_order ASC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropPhoto = $getPropPhoto1;

            return $getPropPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    // s3 image of the property
    function S3getPropertyPhoto($propID) {
        $this->propertyID = $propID;
        $sql = "select * from propertyImages where propertyID='$propID' and imageType='property' and type='home' and img_resource='S3' and property_order !='P2' and property_order !='P3' ORDER BY img_order ASC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropPhoto = $getPropPhoto1;

            return $getPropPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------Get Award Function ---------------------------------------->
    function GetAwardListData($customerId, $propertyId) {
        $sql = "select * from awards where customerID='$customerId' and propertyID = '$propertyId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $awardList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $awardList = $awardList1;
            return $awardList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllAwardListData($customerId) {
        $sql = "select * from awards where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $awardList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $awardList = $awardList1;
            return $awardList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddAwardsPhoto($customerId, $timestmp, $alt_tag, $pro_id) {
        $sql = "insert into awards(customerID,propertyID,awardphoto,imagehead) values('$customerId','$pro_id','$timestmp','$alt_tag')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------Get Testimonial ---------------------------------------->
    function GetTestimonialListData($customerId, $propertyId) {
        $sql = "select * from testimonials where customerID='$customerId' and propertyID = '$propertyId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $awardList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $awardList = $awardList1;
            return $awardList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllTestimonialData($customerId) {
        $sql = "select * from testimonials where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $awardList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $awardList = $awardList1;
            return $awardList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------Add Testimonial Data------------------------------------->
    function AddTestimonialData($customerId, $pro_id, $review, $name, $categoryName, $video_review, $timestmp) {
        $sql = "insert into testimonials(customerID,propertyID,review,photo,name,category_name,video_review) values('$customerId','$pro_id','$review','$timestmp','$name','$categoryName','$video_review')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Photo Function Ends ---------------------------//
    //---------------------------- Get Room Photo Function Starts ---------------------------//

    function GetPropertyRoomPhoto($recordID, $roomID) {
        $sql = "select * from propertyImages where propertyID='$recordID' && roomID='$roomID' and imageType='room' and type='home' order by img_order asc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropPhoto = $getPropPhoto1;

            return $getPropPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    
    function S3GetPropertyRoomPhoto($recordID, $roomID) {
        $sql = "select * from propertyImages where propertyID='$recordID' && roomID='$roomID' and imageType='room' and img_resource='S3' and type='home' order by img_order asc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropPhoto = $getPropPhoto1;

            return $getPropPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    

    function getRoomPhoto($propID, $roomID, $datOrder) {
        $this->propertyID = $propID;
        $this->roomId = $roomID;
        $this->dataorder = $datOrder;

        $sql = "select * from propertyImages where propertyID='$propID' and roomID='$roomID' and imageType='room' " . $datOrder;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getRoomPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getRoomPhoto = $getRoomPhoto1;

            return $getRoomPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //----------------------------Update Property Image Sequesnce--------------------------->
    function updateOrder($id_array) {
        $DB = new connectDB();
        $DB = $DB->connect();
        $count = 1;
        foreach ($id_array as $id) {
            $update = $DB->query("UPDATE propertyImages SET img_order = $count WHERE imagesID = $id");
            $count ++;
        }
        return TRUE;
    }

    //---------------------------- Get Room Photo Function Ends ---------------------------//
    //---------------------------- Get Property Questions Function Starts ---------------------------//

    function getPropertyQuestions($propID) {
        $this->propertyID = $propID;

        $sql = "select * from quesAns where propertyID='$propID' order by quesID desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropQuest1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropQuest = $getPropQuest1;

            return $getPropQuest;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetQuestionData($quesID) {
        $sql = "select * from quesAns where quesID='$quesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getPropQuest1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getPropQuest = $getPropQuest1;
            return $getPropQuest;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteQuestion($quesID) {
        $sql = "DELETE FROM quesAns WHERE quesID='$quesID' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateQuesAns($question, $answer, $quesID) {
        $sql = "update quesAns set question='$question',answer='$answer' where quesID='$quesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

//----------------Delete Property ------------------------>
    function DeletePropertyData($recordID) {
        $sql = "update propertyTable set status='D' where propertyID='$recordID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateCurrency($currency, $propID) {
        $sql = "update propertyTable set currency='$currency' where propertyID='$propID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePropertyStatus($stat, $pro) {
        $sql = "update propertyTable set status='$stat' where propertyID='$pro'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePropertyAffilliate($affi, $pro) {
        $sql = "update propertyTable set affilate='$affi' where propertyID='$pro'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateBaseUrl($customerId, $basicUrl, $reservationNo, $center_email, $fb_link, $tw_link, $insta_link, $linkedin_link, $trip_advisor, $google_link) {
        $sql = "update admin_details set website='$basicUrl',reservationNo='$reservationNo',center_email='$center_email',fb_link='$fb_link',tw_link='$tw_link',insta_link='$insta_link',linkedin_link='$linkedin_link',trip_advisor= '$trip_advisor',google_plus_link='$google_link' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateLogo($customerId, $timestmp, $alt_tag) {
        $sql = "update admin_details set logo_img='$timestmp',logo_alt='$alt_tag' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UploadFevicon($customerId, $timestmp) {
        $sql = "update admin_details set fevicon_img='$timestmp' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function get_Cloud_AdminDetails($customerId) {
        $sql = "select * from admin_details where customerID='$customerId' order by slno asc limit 1";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getQuery = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getQuery1 = $getQuery;
            return $getQuery1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertPropRoomImage($timestmp, $alt_tag, $pro_type, $roo_id, $pro_id, $type) {
        $sql = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type) values('$timestmp','$alt_tag','$pro_type','$roo_id','$pro_id','$type')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    // AWS code
    
    function S3InsertPropRoomImage($name, $alt_tag, $pro_type, $roo_id, $pro_id, $type,$img_resource) {
        $sql = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type,img_resource) values('$name','$alt_tag','$pro_type','$roo_id','$pro_id','$type','$img_resource')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    

    function AddTeamMember($customerId, $teamName, $teamDepart, $teamDesig, $teamDescrip, $cond) {
        $sql = "insert into team_members set customerID='$customerId',name='$teamName',depart='$teamDepart',desig='$teamDesig',about='$teamDescrip' $cond";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getVideoGallery($customerId) {
        $sql = "select * from static_page_video_gallery where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getQuery = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getQuery1 = $getQuery;
            return $getQuery1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddVideoGallery($customerId, $video_heading, $video_url, $db_page_url) {
        $sql = "insert into static_page_video_gallery set customerID='$customerId',video_heading='$video_heading',video_url='$video_url',page_url='$db_page_url'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateVideoGalleryData($customerId, $video_heading, $video_url) {
        $sql = "update static_page_video_gallery set video_heading='$video_heading',video_url='$video_url' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateVideoGalleryWithID($videoID, $customerId, $video_heading, $video_url) {
        $sql = "update static_page_video_gallery set video_heading='$video_heading',video_url='$video_url' where customerID='$customerId' and id='$videoID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetRoomPhotoWithImageID($imagesID1) {
        $sql = "select * from propertyImages where imagesID='$imagesID1'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $getQuery = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $getQuery1 = $getQuery;
            return $getQuery1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteRoomPhoto($imagesID1) {
        $sql = "DELETE FROM propertyImages WHERE imagesID='$imagesID1'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletePropertyVideo($videoID) {
        $sql = "DELETE FROM static_page_video_gallery WHERE id='$videoID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateImageAltTag($imagesID, $imageAlt1) {
        $sql = "update propertyImages set imageAlt='$imageAlt1' where imagesID='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    // this is for the set_cover_photo
//    function setcoverphoto($timestmp, $pro_type, $roo_id, $pro_id, $type, $property_order) {
//       //echo  $sql = "insert into propertyImages(imageURL,imageType,roomID,propertyID,type,property_order) values('$timestmp','$pro_type','$roo_id','$pro_id','$type','$property_order')";
//       echo  $sql="insert into propertyImages(imageURL, imageType, roomID, propertyID, type,) values('$timestmp','$pro_type','$roo_id','$pro_id','$type')";
//         
//       try {
//            $DB = new connectDB();
//            $DB = $DB->connect();
//            
//            $exQuey1 = $DB->query($sql);
//        } catch (PDOExecption $e) {
//            echo "{'error':'{'text':'.$e->getMessage().'}'";
//        }
//    }
    
    function setcoverphoto($timestmp, $pro_type, $roo_id, $pro_id, $type, $property_order) {
        $sql = "insert into propertyImages(imageURL,imageType,roomID,propertyID,type,property_order) values('$timestmp','$pro_type','$roo_id','$pro_id','$type','$property_order')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    function getcoverphoto($propID) {
        $this->propertyID = $propID;
        $sql = "select * from propertyImages where propertyID='$propID' and imageType='property' and type='home' and  property_order!=' ' ORDER BY img_order ASC";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropPhoto = $getPropPhoto1;

            return $getPropPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    function setcoverWithID($imagesID) {
        $sql = "select * from propertyImages where imagesID ='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $allphoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allphoto1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    function updatecoverphoto($imagesID,$timestmp,$property_order) {
        $sql = "update propertyImages set imageURL='$timestmp', property_order= '$property_order' where imagesID='$imagesID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    //---------------------------- Get Property Questions Function Ends ---------------------------//
}

class inventoryData extends adminFunction {
    
    //------------------------------------ Get City Listing Function Starts -----------------------------------//


    function getPropertyListingForChannelManager($customerID, $user_id) {
        $this->custID = $customerID;
        if ($user_id) {
            $sql = "select * from propertyTable where customerID='$customerID' and status='Y' and user_id='$user_id' order by propertyID desc";
        } else {
            $sql = "select * from propertyTable where customerID='$customerID' and status='Y' order by propertyID desc";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propList = $propList1;

            return $propList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyListing($customerID, $user_id) {
        $this->custID = $customerID;
        if ($user_id) {
            $sql = "select * from propertyTable where customerID='$customerID' and user_id ='$user_id' order by propertyID desc";
        } else {
            $sql = "select * from propertyTable where customerID='$customerID' order by propertyID desc";
        }
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propList = $propList1;

            return $propList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAllGoogleSheetData() {
        $sql = "select * from googlesheet where customerID = 25";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propList = $propList1;

            return $propList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyWithNameData($property) {
        $sql = "select * from propertyTable where propertyName ='$property'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propList = $propList1;

            return $propList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyDetailsWithCustomerID($property, $customerId) {
        $sql = "select propertyID from propertyTable where propertyName='$property' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propList = $propList1;

            return $propList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City Listing Function Ends -----------------------------------//	
    //------------------------------------ Get Top Date Calendar Header Function Starts -----------------------------------//

    var $currentDate;

    function getCalendarHeader($passMonth, $passDate) {
        $this->cuurentMonth = $passMonth;
        $this->currentDate = $passDate;

        if ($passMonth == "01" || $passMonth == "03" || $passMonth == "05" || $passMonth == "07" || $passMonth == "08" || $passMonth == "10" || $passMonth == "12") {
            for ($i = 1; $i <= 31; $i++) {
                ?>
                <td style="width:3%;<?php if ($i == $passDate) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                    <?php echo $i; ?>
                    <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                </td>			
                <?php
            }
        } elseif ($passMonth == "04" || $passMonth == "06" || $passMonth == "09" || $passMonth == "11") {
            for ($i = 1; $i <= 30; $i++) {
                ?>
                <td style="width:3%;<?php if ($i == $passDate) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                    <?php echo $i; ?>
                    <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                </td>
                <?php
            }
        } elseif ($passMonth == "02") {
            for ($i = 1; $i <= 28; $i++) {
                ?>
                <td style="width:3%;<?php if ($i == $passDate) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                    <?php echo $i; ?>
                    <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                </td>
                <?php
            }
        }
    }

    //------------------------------------ Get Top Date Calendar Header Function Ends -----------------------------------//	
    //------------------------------------ Get Property Details By Name Function Starts -----------------------------------//


    var $propertyFullName;

    function getPropDetailsByName($propertyName, $customerID) {
        $this->propertyFullName = $propertyName;
        $this->custID = $customerID;

        $sql = "select * from propertyTable where propertyName='$propertyName' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propDataName1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propDataName = $propDataName1;

            return $propDataName;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property Details By Name Function Ends -----------------------------------//
    //------------------------------------ Get Room Details Function Starts -----------------------------------//


    function getRoomDetails($propID) {
        $this->propertyID = $propID;

        $sql = "select * from propertyRoom where propertyID='$propID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomData = $roomData1;

            return $roomData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getRoomDetailsWithRoomID($room_nam) {

        $sql = "select * from propertyRoom where roomID='$room_nam'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomData = $roomData1;

            return $roomData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Room Details Function Ends -----------------------------------//
    //------------------------------------ Get Inventory Details Function Starts -----------------------------------//


    var $currentYear;

    function getInventoryDetails($propID, $roomID, $passMonth, $passYear) {
        $this->propertyID = $propID;
        $this->roomId = $roomID;
        $this->cuurentMonth = $passMonth;
        $this->currentYear = $passYear;

        $sql = "select * from manage_inventory where property_id='$propID' and room_id='$roomID' and month='$passMonth' and year='$passYear'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomInventory1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomInventory = $roomInventory1;

            return $roomInventory;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getInventoryRateDetails($propJsonId, $roomJsonId, $jsonMonth, $jsonYear) {
        $sql = "select * from manage_rate where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomInventoryRate1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomInventoryRate = $roomInventoryRate1;

            return $roomInventoryRate;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Inventory Details Function Ends -----------------------------------//
    //------------------------------------ Display Final Inventory Details Function Starts -----------------------------------//


    var $inventoryDataCount;
    var $roomType;
    var $roomAvailability;
    var $fullInventoryData;

    function displayInventory($inventryDataCnt, $propID, $roomTyp, $roomNo, $passMonth, $passDate, $invntryDat) {
        $this->inventoryDataCount = $inventryDataCnt;
        $this->propertyID = $propID;
        $this->roomType = $roomTyp;
        $this->roomAvailability = $roomNo;
        $this->cuurentMonth = $passMonth;
        $this->currentDate = $passDate;

        $this->fullInventoryData = $invntryDat;

        if ($inventryDataCnt == "0") {

            if ($passMonth == "01" || $passMonth == "03" || $passMonth == "05" || $passMonth == "07" || $passMonth == "08" || $passMonth == "10" || $passMonth == "12") {
                for ($j = 1; $j <= 31; $j++) {
                    $newIdForDetails = $roomTyp . "~" . $j;
                    ?>
                    <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                        <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                    </td>		
                    <?php
                }
            } elseif ($passMonth == "04" || $passMonth == "06" || $passMonth == "09" || $passMonth == "11") {
                for ($j = 1; $j <= 30; $j++) {
                    $newIdForDetails = $roomTyp . "~" . $j;
                    ?>
                    <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                        <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                    </td>
                    <?php
                }
            } elseif ($passMonth == "02") {
                for ($j = 1; $j <= 28; $j++) {
                    $newIdForDetails = $roomTyp . "~" . $j;
                    ?>
                    <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                        <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                    </td>
                    <?php
                }
            }
            echo '<center>No Inventory Updated.</center>';
        } else {
            // echo 'yes dta';
            foreach ($invntryDat as $inventoryPassedData) {
                if ($passMonth == "01" || $passMonth == "03" || $passMonth == "05" || $passMonth == "07" || $passMonth == "08" || $passMonth == "10" || $passMonth == "12") {
                    for ($j = 1; $j <= 31; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        if ($inventoryPassedData->{$j} == null) {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        } else {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $inventoryPassedData->{$j}; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" style="<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        }
                    }
                } elseif ($passMonth == "04" || $passMonth == "06" || $passMonth == "09" || $passMonth == "11") {
                    for ($j = 1; $j <= 30; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        if ($inventoryPassedData->{$j} == null) {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        } else {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $inventoryPassedData->{$j}; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" style="<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        }
                    }
                } elseif ($passMonth == "02") {
                    for ($j = 1; $j <= 28; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        if ($inventoryPassedData->{$j} == null) {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        } else {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $inventoryPassedData->{$j}; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" style="<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        }
                    }
                }
            }
        }
    }

    function getGoogleSheetData($PropertyName) {
        $sql = "select * from googlesheet where PropertyName='$PropertyName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $googlesheet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $googlesheet1 = $googlesheet;

            return $googlesheet1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertInventoryData($customerId, $jsonDate, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear, $jsonPrice) {
        $sql = "insert into manage_inventory(customerID,property_id,room_id,month,year,`$jsonDate`) values('$customerId','$propJsonId','$roomJsonId','$jsonMonth','$jsonYear','$jsonPrice')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateInventoryData($jsonDate, $jsonPrice, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear) {
        $sql = "update manage_inventory set`$jsonDate`='$jsonPrice' where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertInventoryRateData($customerId, $jsonDate, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear, $jsonPrice) {
        $sql = "insert into manage_rate(customerID,property_id,room_id,month,year,`$jsonDate`) values('$customerId','$propJsonId','$roomJsonId','$jsonMonth','$jsonYear','$jsonPrice')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateInventoryRateData($jsonDate, $jsonPrice, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear) {
        $sql = "update manage_rate set`$jsonDate`='$jsonPrice' where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertBookingEngineData($customerId, $engineName, $token, $property_id) {
        $sql = "insert into api_token(customerID,booking_engine,access_token,status,propertyID) values('$customerId','$engineName','$token','Y','$property_id')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function ConnectMailChimp($customerId, $access_token, $dc, $list_id, $user_id, $api_key, $api_endpoint, $accountname, $login_id, $login_name, $login_email) {
        $sql = "insert into mail_chimp(customerID,access_token,dc,status,list_id,user_id,api_key,api_endpoint,accountname,login_id,login_name,login_email) values('$customerId','$access_token','$dc','Y','$list_id','$user_id','$api_key','$api_endpoint','$accountname','$login_id','$login_name','$login_email')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getMailChimpData($customerId) {
        $sql = "select * from mail_chimp where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $googlesheet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $googlesheet1 = $googlesheet;
            return $googlesheet1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateMailChimpStatus($sta, $customerId) {
        $sql = "update mail_chimp set status = '$sta' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateInstagramStatus($sta, $customerId) {
        $sql = "update instagram set status = '$sta' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getInstagramData($customerId) {
        $sql = "select * from instagram where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $googlesheet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $googlesheet1 = $googlesheet;
            return $googlesheet1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getbookingEngineData($customerId) {
        $sql = "select * from api_token where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $googlesheet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $googlesheet1 = $googlesheet;
            return $googlesheet1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateBookingEngineData($customerId, $engineName, $token) {
        $sql = "update api_token set status = 'Y',booking_engine='$engineName',access_token='$token' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DisconnectBookingEngine($customerId) {
        $sql = "update api_token set status = 'N' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertGoogleMyBusiness($customerId, $account_id, $accountName, $location_id, $locationName, $primaryPhone, $total_property, $accesstoken) {
        $sql = "insert into google_my_business(customerID,account_id,account_name,status,location_id,location_name,phone,total_size,access_token) values('$customerId','$account_id','$accountName','Y','$location_id','$locationName','$primaryPhone','$total_property','$accesstoken')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getGoogleMyBusinessData($customerId) {
        $sql = "select * from google_my_business where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $googlemybusiness = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $googlemybusiness1 = $googlemybusiness;
            return $googlemybusiness1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateGoogleMyBusinessStatus($sta, $customerId, $account_id) {
        $sql = "update google_my_business set status = '$sta' where customerID='$customerId' and account_id='$account_id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertGoogleMyBusinessReview($customerId, $account_id, $locationName, $reviewerName, $starRating, $review, $createTime, $updateTime, $profilePhotoUrl, $avg_ratting, $totalReviewCount) {
        $sql = "insert into google_review(customerID,account_id,propertyName,name,review,starRating,createTime,updateTime,image,avg_ratting,totalReviewCount) values('$customerId','$account_id','$locationName','$reviewerName','$review','$starRating','$createTime','$updateTime','$profilePhotoUrl','$avg_ratting','$totalReviewCount')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateGoogleReviewStatus($sta, $customerId, $propoertyName) {
        $sql = "update google_my_business set review_status = '$sta' where customerID='$customerId' and location_name='$propoertyName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateGoogleReview($sta, $customerId, $propoertyName, $propertyID) {
        $sql = "update google_review set status = '$sta',propertyID='$propertyID' where customerID='$customerId' and propertyName='$propoertyName'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getGoogleActiveReview($customerId, $propID) {
        $sql = "select * from google_review where customerID='$customerId' and propertyID='$propID' and status='Y'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $googlemybusiness = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $googlemybusiness1 = $googlemybusiness;
            return $googlemybusiness1;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Display Final Inventory Details Function Ends -----------------------------------//	
}

class staticPageData extends adminFunction {

    //------------------------------------ Get Static Pages Listing Function Starts -----------------------------------//

    function getStaticPageListing($customerID) {
        $this->custID = $customerID;

        $sql = "select * from static_pages where type='desktop' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $staticPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $staticPageList = $staticPageList1;

            return $staticPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Static Pages Listing Function Ends -----------------------------------//	
    //------------------------------------ Get Client Page Data Function Starts -----------------------------------//


    function getClientPageData($filename, $customerID) {
        $this->pageUrl = $filename;
        $this->custID = $customerID;

        $sql = "select * from static_page_clientsub where page_url='$filename' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getOurClientPageData($filename, $customerID) {
        $this->pageUrl = $filename;
        $this->custID = $customerID;

        $sql = "select * from static_page_clientmain where page_url='$filename' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getContactUsPageData($filename, $customerID) {
        $this->pageUrl = $filename;
        $this->custID = $customerID;

        $sql = "select * from static_page_contactus where page_url='$filename' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getServicePageData($filename, $customerID) {
        $sql = "select * from static_page_services where page_url='$filename' and customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }


    function getServicePagewithid($filename, $customerID,$id) {

        $sql = "select * from static_page_services where page_url='$filename' and customerID='$customerID' and id='$id'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getBlogPageData($filename, $customerID) {
        $sql = "select * from static_page_blog where page_url='$filename' and customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

//------------------------------Gallery function Start-------------------------------------------->
    function getGalleryData($customerId) {
        $sql = "select * from static_page_gallery where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddGalleryHeading($customerId, $heading, $img_header) {
        $sql = "insert into static_page_gallery set customerID='$customerId', main_heading='$heading',header_img='$img_header'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateServicePageData($customerId, $page_url, $heading, $content, $box_heading, $box_content, $img_header, $box_img) {
        $sql = "update static_page_services set heading='$heading' $box_img $img_header,content='$content',box_heading='$box_heading',box_content='$box_content' where customerID='$customerId' and page_url='$page_url'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateBlogPageData($customerId, $page_url, $heading, $content, $box_heading, $box_content, $img_header, $box_img, $blog_url) {
        $sql = "update static_page_blog set heading='$heading' $box_img $img_header,content='$content',box_heading='$box_heading',box_content='$box_content',blog_url='$blog_url' where customerID='$customerId' and page_url='$page_url'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function Getpagesection($customerId, $section, $page) {
         $sql = "select * from page_section where customerID='$customerId' and section ='$section' and page='$page'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePageSection($customerId, $section, $page, $status) {
        $sql = "update page_section set status='$status' where customerID='$customerId' and page='$page' and section ='$section'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertBlogPageData($customerId, $page_url, $heading, $content, $box_heading, $box_content, $img_header, $box_img, $blog_url) {
        $sql = "insert into static_page_blog set customerID='$customerId',page_url='$page_url', heading='$heading',content='$content',box_heading='$box_heading',box_content='$box_content',blog_url='$blog_url' $img_header $box_img";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertServicePageData($customerId, $page_url, $heading, $content, $box_heading, $box_content, $img_header, $box_img, $blog_url,$publish_date) {

        $sql = "insert into static_page_services set customerID='$customerId',page_url='$page_url',blog_url='$blog_url', heading='$heading',content='$content',box_heading='$box_heading',box_content='$box_content' $img_header $box_img,publish_date='$publish_date' ";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getservice_page_data_withID($customerId, $id) {
        $sql = "select * from static_page_services where customerID='$customerId' and id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getblog_page_data_withID($customerId, $id) {
        $sql = "select * from static_page_blog where customerID='$customerId' and id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

   function UpdateServicePageDataWithID($customerId, $page_url, $heading, $content, $box_heading, $box_content, $img_header, $box_img,$id,$blog_url,$publish_date) {

        $sql = "update static_page_services set publish_date='$publish_date',box_heading='$box_heading',box_content='$box_content',heading='$heading',content='$content',blog_url='$blog_url' $img_header $box_img where customerID='$customerId' and page_url='$page_url' and id='$id'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }


        function Deleteservice($pageUrl, $customerId, $id) {

        $sql = "DELETE FROM static_page_services WHERE id='$id' and customerID='$customerId' and page_url='$pageUrl'";

        try {

            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateBlogPageDataWithID($customerId, $page_url, $id, $box_heading, $box_content, $cond) {
        $sql = "update static_page_blog set box_heading='$box_heading',box_content='$box_content' $cond where customerID='$customerId' and page_url='$page_url' and id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertOurClientPageData($customerId, $page_url, $h1, $h1content, $collapseh1, $collapsecont1, $h2, $divhead1, $divcontent1, $divhead2, $divcontent2, $divhead3, $divcontent3, $sidedivheading, $upd1, $upd2, $upd3, $cond1, $cond2, $cond3) {
        $sql = "insert into static_page_clientmain set customerID='$customerId',page_url='$page_url', h1='$h1',h1content='$h1content',collapsehead1='$collapseh1',collapsecont1='$collapsecont1',h2='$h2',divhead1='$divhead1',divcontent1='$divcontent1',divhead2='$divhead2',divcontent2='$divcontent2',divhead3='$divhead3',divcontent3='$divcontent3',sidedivheading='$sidedivheading'$upd1 $upd2 $upd3 $cond1 $cond2 $cond3";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateGalleryHeading($customerId, $heading, $img_header) {
        $sql = "update static_page_gallery set main_heading='$heading',header_img='$img_header' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //--------------------------------Gallery Function End---------------------------------------------->
    function getWhyPerchPageData($filename, $customerID) {
        $this->pageUrl = $filename;
        $this->custID = $customerID;

        $sql = "select * from static_page_whyperch where page_url='$filename' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPolicyPageData($customerID) {
        $sql = "select * from static_page_policy where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getFoodPageData($customerID) {
        $sql = "select * from static_page_food where customerID='$customerID' order by s_no asc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getFoodPageMoreData($customerID) {
        $sql = "select * from static_page_food where customerID='$customerID' order by s_no asc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAboutUsPageData($customerID) {
        $sql = "select * from static_page_tbl where customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getTermsAndConditionsPageData($customerID) {
        $sql = "select * from static_page_t_and_c where customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateTermsAndConditions($customerId, $heading, $content) {
        $sql = "update static_page_t_and_c set heading='$heading',content='$content' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateOurClientPageData($customerId, $page_url, $h1, $h1content, $collapseh1, $collapsecont1, $h2, $divhead1, $divcontent1, $divhead2, $divcontent2, $divhead3, $divcontent3, $sidedivheading, $upd1, $upd2, $upd3, $cond1, $cond2, $cond3) {
        $sql = "update static_page_clientmain set h1='$h1',h1content='$h1content',collapsehead1='$collapseh1',collapsecont1='$collapsecont1',h2='$h2',divhead1='$divhead1',divcontent1='$divcontent1',divhead2='$divhead2',divcontent2='$divcontent2',divhead3='$divhead3',divcontent3='$divcontent3',sidedivheading='$sidedivheading' $upd1 $upd2 $upd3 $cond1 $cond2 $cond3 where customerID='$customerId' and page_url='$page_url'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getTeamMember($customerID) {
        $sql = "select * from team_members where customerID='$customerID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetTeamMemberWithID($slno) {
        $sql = "select * from team_members where s_no='$slno'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateTeamMemberData($slno, $customerId, $teamName, $teamDepart, $teamDescrip, $teamDesig, $cond1) {
        $sql = "update team_members set name='$teamName',depart='$teamDepart',desig='$teamDesig',about='$teamDescrip' $cond1 where customerID='$customerId' and s_no='$slno'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteTeamMember($slno) {
        $sql = "DELETE FROM team_members WHERE s_no='$slno'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertWhyUsPage($customerId, $pageUrl, $h1, $boxicon1, $boxhead1, $boxcontent1, $boxicon2, $boxhead2, $boxcontent2, $boxicon3, $boxhead3, $boxcontent3, $h2, $subhead2, $imgboxh1, $imgboxcontent1, $imgboxh2, $imgboxcontent2, $imgboxh3, $imgboxcontent3, $imgboxh4, $imgboxcontent4, $imgboxh5, $imgboxcontent5, $imgboxh6, $imgboxcontent6, $imgboxh7, $imgboxcontent7, $imgboxh8, $imgboxcontent8, $imgboxh9, $imgboxcontent9, $imgboxh10, $imgboxh11, $imgboxcontent11, $imgboxh12, $imgboxcontent12, $imgboxcontent10, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $cond9, $cond10, $cond11, $cond12, $img_header) {
        // $sql = "insert into static_page_whyperch(customerID,page_url,h1,box1_icon,box1_head,box1_content,box2_icon,box2_head,box2_content,box3_icon,box3_head,box3_content,h2,subhead2,imgbox_h1,imgbox_content1,imgbox_h2,imgbox_content2,imgbox_h3,imgbox_content3,imgbox_h4,imgbox_content4,imgbox_h5,imgbox_content5,imgbox_h6,imgbox_content6,imgbox_h7,imgbox_content7,imgbox_h8,imgbox_content8,imgbox_h9,imgbox_content9,imgbox_h10,imgbox_content10) values('$customerId','$pageUrl','$h1','$boxicon1','$boxhead1','$boxcontent1','$boxicon2','$boxhead2','$boxcontent2','$boxicon3','$boxhead3','$boxcontent3','$h2','$subhead2','$imgboxh1','$imgboxcontent1','$imgboxh2','$imgboxcontent2','$imgboxh3','$imgboxcontent3','$imgboxh4','$imgboxcontent4','$imgboxh5','$imgboxcontent5','$imgboxh6','$imgboxcontent6','$imgboxh7','$imgboxcontent7','$imgboxh8','$imgboxcontent8','$imgboxh9','$imgboxcontent9','$imgboxh10','$imgboxcontent10' $cond1 $cond2 $cond3 $cond4 $cond5 $cond6 $cond7 $cond8 $cond9 $con10)";
        $sql = "insert into static_page_whyperch set customerID='$customerId',page_url='$pageUrl', h1='$h1' $img_header ,box1_icon='$boxicon1', imgbox_h1='$imgboxh1',box1_head='$boxhead1',box1_content='$boxcontent1',box2_icon='$boxicon2',box2_head='$boxhead2',box2_content='$boxcontent2',box3_icon='$boxicon3',box3_head='$boxhead3',box3_content='$boxcontent3',imgbox_content1='$imgboxcontent1',imgbox_h2='$imgboxh2',imgbox_content2='$imgboxcontent2', imgbox_h3='$imgboxh3',imgbox_content3='$imgboxcontent3',imgbox_h4='$imgboxh4',imgbox_content4='$imgboxcontent4', imgbox_h5='$imgboxh5',imgbox_content5='$imgboxcontent5', imgbox_h6='$imgboxh6',imgbox_content6='$imgboxcontent6', imgbox_h7='$imgboxh7',imgbox_content7='$imgboxcontent7', imgbox_h8='$imgboxh8',imgbox_content8='$imgboxcontent8',imgbox_h9='$imgboxh9',imgbox_content9='$imgboxcontent9',imgbox_h10='$imgboxh10',imgbox_content10='$imgboxcontent10',imgbox_h11='$imgboxh11',imgbox_content11='$imgboxcontent11',imgbox_h12='$imgboxh12',imgbox_content12='$imgboxcontent12' $cond1 $cond2 $cond3 $cond4 $cond5 $cond6  $cond7 $cond8 $cond9 $cond10 $cond11 $cond12";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUs($customerId, $h1, $cond1, $cond2, $img_header) {
        $sql = "update static_page_whyperch set h1='$h1' $cond1 $cond2 header_img='$img_header' where customerID='$custID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsMain($customerId, $h1, $boxicon1, $boxhead1, $boxcontent1, $boxicon2, $boxhead2, $boxcontent2, $boxicon3, $boxhead3, $boxcontent3, $h2, $subhead2, $imgboxh1, $imgboxcontent1, $imgboxh2, $imgboxcontent2, $imgboxh3, $imgboxcontent3, $imgboxh4, $imgboxcontent4, $imgboxh5, $imgboxcontent5, $imgboxh6, $imgboxcontent6, $imgboxh7, $imgboxcontent7, $imgboxh8, $imgboxcontent8, $imgboxh9, $imgboxcontent9, $imgboxh10, $imgboxcontent10, $imgboxh11, $imgboxcontent11, $imgboxh12, $imgboxcontent12, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $cond9, $cond10, $cond11, $cond12, $img_header, $pageUrl) {
        $sql = "update static_page_whyperch set h1='$h1' $img_header ,box1_icon='$boxicon1', imgbox_h1='$imgboxh1',box1_head='$boxhead1',box1_content='$boxcontent1',box2_icon='$boxicon2',box2_head='$boxhead2',box2_content='$boxcontent2',box3_icon='$boxicon3',box3_head='$boxhead3',box3_content='$boxcontent3',h2='$h2',subhead2='$subhead2',imgbox_content1='$imgboxcontent1',imgbox_h2='$imgboxh2',imgbox_content2='$imgboxcontent2', imgbox_h3='$imgboxh3',imgbox_content3='$imgboxcontent3',imgbox_h4='$imgboxh4',imgbox_content4='$imgboxcontent4', imgbox_h5='$imgboxh5',imgbox_content5='$imgboxcontent5', imgbox_h6='$imgboxh6',imgbox_content6='$imgboxcontent6', imgbox_h7='$imgboxh7',imgbox_content7='$imgboxcontent7', imgbox_h8='$imgboxh8',imgbox_content8='$imgboxcontent8',imgbox_h9='$imgboxh9',imgbox_content9='$imgboxcontent9',imgbox_h10='$imgboxh10',imgbox_content10='$imgboxcontent10',imgbox_h11='$imgboxh11',imgbox_content11='$imgboxcontent11',imgbox_h12='$imgboxh12',imgbox_content12='$imgboxcontent12' $cond1 $cond2 $cond3 $cond4 $cond5 $cond6  $cond7 $cond8 $cond9 $cond10 $cond11 $cond12  where customerID='$customerId'and page_url='$pageUrl'";

        try {

            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsNewBox($customerId, $boxicon4, $boxhead4, $boxcontent4, $boxicon5, $boxhead5, $boxhead5, $boxcontent5, $boxicon6, $boxhead6, $boxcontent6, $boxcontent6) {
        $sql = "update static_page_whyperch set box4_icon='$boxicon4',box4_head='$boxhead4',box4_content='$boxcontent4',box5_icon='$boxicon5',box5_head='$boxhead5',box5_content='$boxcontent5',box6_icon='$boxicon6',box6_head='$boxhead6',box6_content='$boxcontent6' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsPage1($customerId, $h1, $boxicon1, $boxhead1, $boxcontent1, $boxicon2, $boxhead2, $boxcontent2, $boxicon3, $boxhead3, $boxcontent3, $boxcontent3, $h2, $subhead2, $imgboxh1, $imgboxcontent1, $imgboxh2, $imgboxcontent2, $cond1, $cond2) {
        $sql = "update static_page_whyperch set h1='$h1',box1_icon='$boxicon1',box1_head='$boxhead1',box1_content='$boxcontent1',box2_icon='$boxicon2',box2_head='$boxhead2',box2_content='$boxcontent2',box3_icon='$boxicon3',box3_head='$boxhead3',box3_content='$boxcontent3',h2='$h2',subhead2='$subhead2',imgbox_h1='$imgboxh1',imgbox_content1='$imgboxcontent1',imgbox_h2='$imgboxh2',imgbox_content2='$imgboxcontent2' $cond1 $cond2 where  customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsPage2($customerId, $boxicon4, $boxhead4, $boxcontent4, $boxicon5, $boxhead5, $boxcontent5, $boxicon6, $boxhead6, $boxcontent6) {
        $sql = "update static_page_whyperch set box4_icon='$boxicon4',box4_head='$boxhead4',box4_content='$boxcontent4',box5_icon='$boxicon5',box5_head='$boxhead5',box5_content='$boxcontent5',box6_icon='$boxicon6',box6_head='$boxhead6',box6_content='$boxcontent6' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsPage3($customerId, $imgboxh3, $imgboxcontent3, $imgboxh4, $imgboxcontent4, $cond3, $cond4) {
        $sql = "update static_page_whyperch set imgbox_h3='$imgboxh3',imgbox_content3='$imgboxcontent3',imgbox_h4='$imgboxh4',imgbox_content4='$imgboxcontent4' $cond3 $cond4 where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsPage4($customerId, $imgboxh5, $imgboxcontent5, $imgboxh6, $imgboxcontent6, $cond5, $cond6) {
        $sql = "update static_page_whyperch set imgbox_h5='$imgboxh5',imgbox_content5='$imgboxcontent5',imgbox_h6='$imgboxh6',imgbox_content6='$imgboxcontent6' $cond5 $cond6 where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsPage5($customerId, $imgboxh7, $imgboxcontent7, $imgboxh8, $imgboxcontent8, $cond7, $cond8) {
        $sql = "update static_page_whyperch set imgbox_h3='$imgboxh7',imgbox_content7='$imgboxcontent7',imgbox_h8='$imgboxh8',imgbox_content8='$imgboxcontent8' $cond7 $cond8 where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsImgBox3($customerId, $imgboxh3, $imgboxcontent3, $imgboxh4, $imgboxcontent4, $cond3, $cond4) {
        $sql = "update static_page_whyperch set imgbox_h3='$imgboxh3',imgbox_content3='$imgboxcontent3',imgbox_h4='$imgboxh4',imgbox_content4='$imgboxcontent4' $cond3 $cond4 where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsImgBox5($customerId, $imgboxh5, $imgboxcontent5, $imgboxh6, $imgboxcontent6, $cond5, $cond6) {
        $sql = "update static_page_whyperch set imgbox_h5='$imgboxh5',imgbox_content5='$imgboxcontent5',imgbox_h6='$imgboxh6',imgbox_content6='$imgboxcontent6' $cond5 $cond6 where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsImgBox7($customerId, $imgboxh7, $imgboxcontent7, $imgboxh8, $imgboxcontent8, $cond7, $cond8) {
        $sql = "update static_page_whyperch set imgbox_h7='$imgboxh7',imgbox_content7='$imgboxcontent7',imgbox_h8='$imgboxh8',imgbox_content8='$imgboxcontent8' $cond7 $cond8 where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateWhyUsBoxcon4($customerId, $boxicon4, $boxhead4, $boxcontent4, $boxicon5, $boxhead5, $boxcontent5, $boxicon6, $boxhead6, $boxcontent6) {
        $sql = "update static_page_whyperch set box4_icon='$boxicon4',box4_head='$boxhead4',box4_content='$boxcontent4',box5_icon='$boxicon5',box5_head='$boxhead5',box5_content='$boxcontent5',box6_icon='$boxicon6',box6_head='$boxhead6',box6_content='$boxcontent6' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getMetaTagsData($customerID, $filename) {
        $sql = "select * from metaTags where filename='$filename' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getCityUrlData($customerID) {
        $sql = "select cityUrl from city_url where customerID='$customerID' and status='Y' and cityUrl!=''";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getMetaTagsByID($customerId, $metano) {
        $sql = "select * from metaTags where  metano='$metano'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getMetaTagsByFileName($customerId, $saur) {
        $sql = "select * from metaTags where filename='$saur' and customerID='$customerId' ";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getStaticPageUrlData($customerId) {
        $sql = "select page_url from static_pages where customerID='$customerId' and status='Y' and type='desktop' and page_url!=''";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyPageUrlData($customerId) {
        $sql = "select propertyURL from propertyTable where customerID='$customerId' and status='Y' and propertyURL!=''";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getRoomPageUrlData($customerId) {
        $sql = "select room_url from propertyRoom where customerID='$customerId' and status='Y' and room_url!=''";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------Get Customer Review----------------------->
    function getCustomerReview($customerId) {
        $sql = "select * from property_reviews where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $cust_review1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $cust_review = $cust_review1;
            return $cust_review;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateReviewStatus($customerId, $reviewID, $status) {
        $sql = "update property_reviews set status='$status' where s_no='$reviewID' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getTestimonialCategoryList($customerId) {
        $sql = "select * from testimonial_category where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Client Page Data Function Ends -----------------------------------//

    function AddHomeContentData($customerId, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $testimonial_cate, $video_review_cate, $award_image) {
        $sql = "insert into homeContent set customerID='$customerId',h1tag='$_REQUEST[h1tag]',H1Content='$_REQUEST[H1Content]',subhead1='$_REQUEST[subhead1]',subheadcontent1='$_REQUEST[subheadcontent1]',subhead2='$_REQUEST[subhead2]',subheadcontent2='$_REQUEST[subheadcontent2]',subhead3='$_REQUEST[subhead3]',subheadcontent3='$_REQUEST[subheadcontent3]',subhead4='$_REQUEST[subhead4]',subheadcontent4='$_REQUEST[subheadcontent4]',aptgalleryhead='$_REQUEST[aptgalleryhead]',aptgalleryheadcontent='$_REQUEST[aptgalleryheadcontent]',maphead='$_REQUEST[maphead]',mapheadcontent='$_REQUEST[mapheadcontent]',splheadmain='$_REQUEST[splheadmain]', splheadmaincontent='$_REQUEST[splheadmaincontent]', splheadcorporate='$_REQUEST[splheadcorporate]',spllinkcorporate='$_REQUEST[spllinkcorporate]',splheadcorporatecontent='$_REQUEST[splheadcorporatecontent]',splheadjp='$_REQUEST[splheadjp]', spllinkjp='$_REQUEST[spllinkjp]', splheadjpcontent='$_REQUEST[splheadjpcontent]', splheadgoa='$_REQUEST[splheadgoa]', spllinkgoa='$_REQUEST[spllinkgoa]', splheadgoacontent='$_REQUEST[splheadgoacontent]', reviewhead='$_REQUEST[reviewhead]', content1='$_REQUEST[content1]', awardheading='$_REQUEST[awardheading]',awardcontent='$_REQUEST[awardcontent]',videoheading='$_REQUEST[videoheading]',videocontent='$_REQUEST[videocontent]', videourl='$_REQUEST[videourl]',award1alttag='$_REQUEST[award1alttag]',award2alttag='$_REQUEST[award2alttag]' ,award3alttag='$_REQUEST[award3alttag]',award4alttag='$_REQUEST[award4alttag]',reviewheadcontent='$_REQUEST[reviewheadcontent]',awarddivhead='$_REQUEST[awarddivhead]',awarddivcontent='$_REQUEST[awarddivcontent]',awarddivimage='$award_image',awardsdetail='$awrdarrHome',testimonialHead='$_REQUEST[testimonialHead]',testimonialContent='$_REQUEST[testimonialContent]',testimonialCategory='$testimonial_cate',testimonialLimit='$_REQUEST[testimonialLimit]',video_review_heading='$_REQUEST[video_review_heading]',video_limit='$_REQUEST[video_limit]',video_review_cate='$video_review_cate' $cond1 $cond2 $cond3 $cond4 $cond5 $cond6 $cond7 $cond8";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getHomePageData($customerID) {
        //  echo $customerID;
        $sql = "select * from homeContent where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $clientPageList = $clientPageList1;
            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Home Page Data Function Ends -----------------------------------//

    function UpdateHomeContentData($customerID, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $testimonial_cate, $video_review_cate, $award_image) {

        /*  $sql = "update homeContent set h1tag='$_REQUEST[h1tag]',H1Content='$_REQUEST[H1Content]',"
          . "subhead1='$_REQUEST[subhead1]' , subheadcontent1='$_REQUEST[subheadcontent1]',"
          . "subhead2='$_REQUEST[subhead2]',"
          . "subheadcontent2='$_REQUEST[subheadcontent2]',"
          . "subhead3='$_REQUEST[subhead3]',"
          . "subheadcontent3='$_REQUEST[subheadcontent3]',"
          . "subhead4='$_REQUEST[subhead4]',"
          . "subheadcontent4='$_REQUEST[subheadcontent4]',"
          . "aptgalleryhead='$_REQUEST[aptgalleryhead]',"
          . "aptgalleryheadcontent='$_REQUEST[aptgalleryheadcontent]',"
          . "splheadmain='$_REQUEST[splheadmain]',"
          . "splheadmaincontent='$_REQUEST[splheadmaincontent]',"
          . "content1='$_REQUEST[content1]',"
          . "awardheading='$_REQUEST[awardheading]',"
          . "awardcontent='$_REQUEST[awardcontent]',"
          . "videoheading='$_REQUEST[videoheading]',"
          . "videocontent='$_REQUEST[videocontent]',"
          . "videourl='$_REQUEST[videourl]',"
          . "awarddivhead='$_REQUEST[awarddivhead]',"
          . "awarddivcontent='$_REQUEST[awarddivcontent]'"
          . " where customerID='$customerID'";
         * 
         */
        $sql = "update homeContent set h1tag='$_REQUEST[h1tag]',H1Content='$_REQUEST[H1Content]',subhead1='$_REQUEST[subhead1]',subheadcontent1='$_REQUEST[subheadcontent1]',subhead2='$_REQUEST[subhead2]',subheadcontent2='$_REQUEST[subheadcontent2]',subhead3='$_REQUEST[subhead3]',subheadcontent3='$_REQUEST[subheadcontent3]',subhead4='$_REQUEST[subhead4]',subheadcontent4='$_REQUEST[subheadcontent4]',aptgalleryhead='$_REQUEST[aptgalleryhead]',aptgalleryheadcontent='$_REQUEST[aptgalleryheadcontent]',maphead='$_REQUEST[maphead]',mapheadcontent='$_REQUEST[mapheadcontent]',splheadmain='$_REQUEST[splheadmain]', splheadmaincontent='$_REQUEST[splheadmaincontent]', splheadcorporate='$_REQUEST[splheadcorporate]',spllinkcorporate='$_REQUEST[spllinkcorporate]',splheadcorporatecontent='$_REQUEST[splheadcorporatecontent]',splheadjp='$_REQUEST[splheadjp]', spllinkjp='$_REQUEST[spllinkjp]', splheadjpcontent='$_REQUEST[splheadjpcontent]', splheadgoa='$_REQUEST[splheadgoa]', spllinkgoa='$_REQUEST[spllinkgoa]', splheadgoacontent='$_REQUEST[splheadgoacontent]', reviewhead='$_REQUEST[reviewhead]', content1='$_REQUEST[content1]', awardheading='$_REQUEST[awardheading]',awardcontent='$_REQUEST[awardcontent]',videoheading='$_REQUEST[videoheading]',videocontent='$_REQUEST[videocontent]', videourl='$_REQUEST[videourl]',award1alttag='$_REQUEST[award1alttag]',award2alttag='$_REQUEST[award2alttag]' ,award3alttag='$_REQUEST[award3alttag]',award4alttag='$_REQUEST[award4alttag]',reviewheadcontent='$_REQUEST[reviewheadcontent]',awarddivhead='$_REQUEST[awarddivhead]',awarddivcontent='$_REQUEST[awarddivcontent]',awarddivimage='$award_image',awardsdetail='$awrdarrHome',testimonialHead='$_REQUEST[testimonialHead]',testimonialContent='$_REQUEST[testimonialContent]',testimonialCategory='$testimonial_cate',testimonialLimit='$_REQUEST[testimonialLimit]',video_review_heading='$_REQUEST[video_review_heading]',video_limit='$_REQUEST[video_limit]',video_review_cate='$video_review_cate' $cond1 $cond2 $cond3 $cond4 $cond5 $cond6 $cond7 $cond8 where  customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Update Home Page Data Function Ends -----------------------------------//

    function getStaticMainPageData($page_name) {
        $sql = "select * from static_pages_main where page='$page_name' and type='desktop'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getStaticMainPageDataForMobile($page_name) {
        $sql = "select * from static_pages_main where page='mobile-$page_name' and type='mobile'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getStaticPageData($customerId, $page_name) {
        $sql = "select * from static_pages where customerID='$customerId' and page='$page_name'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $PropUrls = $PropUrls1;
            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function insertDBPage($customerId, $new_page_url, $pgdb) {
        $sql = "insert into $pgdb set customerID='$customerId', page_url='$new_page_url'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddCustomeSection($customerId, $heading, $custom_code, $pageURL) {
        $sql = "insert into custom_html set customerID='$customerId', pageURL='$pageURL',heading='$heading',custom_code='$custom_code'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getCustomSectionData($customerId, $pageURL) {
        $sql = "select * from custom_html where customerID='$customerId' and pageURL='$pageURL'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $PropUrls = $PropUrls1;
            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertPolicyPageData($customerId, $page_url, $tab, $tabhead, $tabconte) {
        $sql = "insert into static_page_policy set customerID='$customerId', page_url='$page_url', tab_name='$tab',tab_heading='$tabhead',tab_content='$tabconte'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddNewQuesAnsFood($customerId, $ques, $ans) {
        $sql = "insert into static_page_food set customerID='$customerId', question='$ques',answer='$ans'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddMetaTags($customerId, $meta_title, $filename, $dec, $date) {
        //$sql = "insert into metaTags set customerID='$customerId',filename='$_REQUEST[filename]',MetaTitle='$_REQUEST[MetaTitle]',MetaDisc='$_REQUEST[MetaDisc]',MetaKwd='$_REQUEST[MetaKwd]',othercode='$_REQUEST[othercode]',editdate='$date'";
        $sql = "insert into metaTags set customerID='$customerId',filename='$filename',MetaTitle='$meta_title',MetaDisc='$dec',editdate='$date'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertStaticPageData($customerId, $page_name, $pgnm, $new_page_url, $pgdb, $pgcms) {
        $sql = "insert into static_pages set customerID='$customerId', page='$page_name',page_name='$pgnm',page_url='$new_page_url',dbName='$pgdb',status='Y',cms_page='$pgcms',type='desktop'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertNewPageData($customerId, $page_name, $pgnm, $new_page_url, $pgdb, $pgcms) {
        $sql = "insert into static_pages set customerID='$customerId', page='$page_name',page_name='$pgnm',page_url='$new_page_url',dbName='$pgdb',status='Y',cms_page='$pgcms',type='desktop'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertNewPageDataMobile($customerId, $mo_page, $mo_pgnm, $new_page_url, $mo_pgdb, $mo_pgcms) {
        $sql = "insert into static_pages set customerID='$customerId', page='$mo_page',page_name='$mo_pgnm',page_url='$new_page_url',dbName='$mo_pgdb',status='Y',cms_page='$mo_pgcms',type='mobile'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertContactUsPage($customerId, $mapurl, $tab1name, $tab1heading, $tab1form, $tab1subhead, $tab1content, $box1head, $box1content, $box2head, $box2content, $box3head, $box3content, $box4head, $box4content, $img_header, $page_url) {
        $sql = "insert into static_page_contactus set customerID='$customerId', map_url='$mapurl',tab1_name='$tab1name',tab1_heading='$tab1heading',tab1_formName='$tab1form',tab1_subheading='$tab1subhead',tab1_content='$tab1content',address1head='$box1head',address1='$box1content',address2head='$box2head',address2='$box2content',emailhead='$box3head',email='$box3content',phonehead='$box4head',phone='$box4content' $img_header";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertNewFoodPageData($customerId, $h1, $subhead1, $box1icon, $box1head, $box1content, $box2icon, $box2head, $box2content, $box3icon, $box3head, $box3content, $h2, $subhead2, $qadivhead, $question, $answer, $head3, $iframe, $file, $img_header) {
        $food_menu_Name = 'http://www.stayondiscount.com/sitepanel/' . $file['name'];
        $sql = "insert into static_page_food set customerID='$customerId', h1='$h1',subhead1='$subhead1',box1icon='$box1icon',box1head='$box2head',box1content='$box2content',box2icon='$box2icon',box2head='$box2head',box2content='$box2content',box3icon='$box3icon',box3head='$box3head',box3content='$box3content',h2='$h2',subhead2='$subhead2',qaheading='$qadivhead',question='$head3',answer='$answer',head3='$head3',iframe='$iframe',food_menu='$food_menu_Name' $img_header";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertAboutUsData($customerId, $h1, $h1_cont, $h2, $h2_cont, $collapse_h1, $collapse_h1_cont, $collapse_h2, $collapse_h2_cont, $collapse_h3, $collapse_h3_cont, $collapse_h4, $collapse_h4_cont, $querystr, $h3, $img_header) {
        $sql = "insert into static_page_tbl set customerID='$customerId',page_url='$querystr', about_h1='$h1',about_h1_cont='$h1_cont',about_h2='$h2',about_h2_cont='$h2_cont',about_collapseh1='$collapse_h1',about_collapseh1_cont='$collapse_h1_cont',about_collapseh2='$collapse_h2',about_collapseh2_cont='$collapse_h2_cont',about_collapseh3='$collapse_h3',about_collapseh3_cont='$collapse_h3',about_collapseh3_cont='$collapse_h3_cont',about_collapseh4='$collapse_h4',about_collapseh4_cont='$collapse_h4_cont',about_h3='$h3',header_img='$img_header'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateMetaTags($metano) {
        $sql = " update metaTags set filename='$_REQUEST[filename]',MetaTitle='$_REQUEST[MetaTitle]',MetaDisc='$_REQUEST[MetaDisc]',MetaKwd='$_REQUEST[MetaKwd]',othercode='$_REQUEST[othercode]' where  metano='$metano'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
	
	
	function UpdatedMetaTags($metano,$filename,$metatitle,$metadisciption) {
        $sql = " update metaTags set filename='$filename',MetaTitle='$metatitle',MetaDisc='$metadisciption' where  metano='$metano'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateStaticPageData($customerId, $page_name, $new_page_url) {

        $sql = "update static_pages set page_url='$new_page_url',status='Y' where page='$page_name' and customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateFoodQuesAnswer($quesSno, $quesCont, $answCont) {
        $sql = "update static_page_food set question='$quesCont',answer='$answCont' where s_no='$quesSno'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateActivePage($activateId) {

        $sql = "update static_pages set status='Y' where s_no='$activateId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateDeactivePage($deactiveId) {

        $sql = "update static_pages set status='N' where s_no='$deactiveId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateContactUsPage($customerId, $mapurl, $tab1name, $tab1heading, $tab1form, $tab1subhead, $tab1content, $box1head, $box1content, $box2head, $box2content, $box3head, $box3content, $box4head, $box4content, $img_header, $page_url) {
        $sql = "update static_page_contactus set map_url='$mapurl',tab1_name='$tab1name',tab1_heading='$tab1heading',tab1_formName='$tab1form',tab1_subheading='$tab1subhead',tab1_content='$tab1content',address1head='$box1head',address1='$box1content',address2head='$box2head',address2='$box2content',emailhead='$box3head',email='$box3content',phonehead='$box4head',phone='$box4content' $img_header where customerID='$customerId' and page_url='$page_url'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateClientPage($customerId, $h1, $h1_cont, $box1, $box1_cont, $box2, $box2_cont, $box3, $box3_cont, $box4, $box4_cont, $h2, $h2_subhead1, $h2_subhead1_cont, $h2_subhead2, $h2_subhead2_cont, $h3, $h3_video1, $h3_video2, $img_header, $box1_img, $box2_img, $box3_img, $box4_img, $page_url) {

        $sql = "update static_page_clientsub set client_h1='$h1',client_h1_cont='$h1_cont',client_box1='$box1',client_box1_cont='$box1_cont',client_box2='$box2',client_box2_cont='$box2_cont',client_box3='$box3',client_box3_cont='$box3_cont',client_box4='$box4',client_box4_cont='$box4_cont',client_h2='$h2',client_h2_subhead1='$h2_subhead1',client_h2_subhead1_cont='$h2_subhead1_cont',client_h2_subhead2='$h2_subhead2',client_h2_subhead2_cont='$h2_subhead2_cont',client_h3='$h3',client_h3_video1='$h3_video1',client_h3_video2='$h3_video2' $img_header,box1_img='$box1_img',box2_img='$box2_img',box3_img='$box3_img',box4_img='$box4_img' where customerID='$customerId' and page_url='$page_url'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdatePolicyPageData($tab, $tabhead, $tabconte, $tabno) {
        $sql = "update static_page_policy set tab_name='$tab',tab_heading='$tabhead',tab_content='$tabconte' where s_no='$tabno'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateFoodPageData($customerId, $h1, $subhead1, $box1icon, $box1head, $box1content, $box2icon, $box2head, $box2content, $box3icon, $box3head, $box3content, $h2, $subhead2, $qadivhead, $head3, $iframe, $cond1, $file, $img_header, $page_url) {
        $food_menu_Name = 'http://www.stayondiscount.com/sitepanel/' . $file['name'];
        $sql = "update static_page_food set h1='$h1',subhead1='$subhead1',box1icon='$box1icon',box1head='$box1head',box1content='$box1content',box2icon='$box2icon',box2head='$box2head',box2content='$box2content',box3icon='$box3icon',box3head='$box3head',box3content='$box3content',h2='$h2',subhead2='$subhead2',qaheading='$qadivhead',head3='$head3',iframe='$iframe' $cond1,food_menu='$food_menu_Name' $img_header where customerID='$customerId' and page_url='$page_url'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateAboutUsData($customerId, $h1, $h1_cont, $h2, $h2_cont, $collapse_h1, $collapse_h1_cont, $collapse_h2, $collapse_h2_cont, $collapse_h3, $collapse_h3_cont, $collapse_h4, $collapse_h4_cont, $querystr, $h3, $img_header, $page_url) {
        $sql = "update static_page_tbl set about_h1='$h1',about_h1_cont='$h1_cont',about_h2='$h2',about_h2_cont='$h2_cont',about_collapseh1='$collapse_h1',about_collapseh1_cont='$collapse_h1_cont',about_collapseh2='$collapse_h2',about_collapseh2_cont='$collapse_h2_cont',about_collapseh3='$collapse_h3',about_collapseh3_cont='$collapse_h3_cont',about_collapseh4='$collapse_h4',about_collapseh4_cont='$collapse_h4_cont',about_h3='$h3' $img_header where customerID='$customerId'and page_url='$page_url'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteMetaTags($customerId, $metano) {

        $sql = "DELETE FROM metaTags WHERE metano='$metano' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddFoodGalleryPhoto($customerId, $timestmp, $alt_tag) {
        $sql = "insert into food_gallery set customerID='$customerId',image='$timestmp', img_alt='$alt_tag'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getFoodGalleryPhoto($customerId) {
        $sql = "select * from food_gallery where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $PropUrls = $PropUrls1;
            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

}

class menuNavigationPage extends adminFunction {

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function getPropertyUrls($customerId) {
        $this->custID = $customerID;

        $sql = "select propertyURL from propertyTable where customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getThemeColor($customerId) {
        $sql = "select *  from theme_color where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $PropUrls = $PropUrls1;
            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertThemeColor($customerId) {
        $sql = "insert into theme_color set customerID='$customerId', header_color='$_REQUEST[header_color]',header_font_size='$_REQUEST[header_font_size]',header_font_family='$_REQUEST[header_font_family]', footer_color='$_REQUEST[footer_color]',footer_font_size='$_REQUEST[footer_font_size]',footer_font_family='$_REQUEST[footer_font_family]', heading_color='$_REQUEST[heading_color]',icon_color='$_REQUEST[icon_color]',icon_color_hover='$_REQUEST[icon_color_hover]',button_color='$_REQUEST[button_color]',button_color_hover='$_REQUEST[button_color_hover]',h1_color='$_REQUEST[h1_color]',h2_color='$_REQUEST[h2_color]',h3_color='$_REQUEST[h3_color]',h4_color='$_REQUEST[h4_color]',h5_color='$_REQUEST[h5_color]',h6_color='$_REQUEST[h6_color]',h1_size='$_REQUEST[h1_size]',h2_size='$_REQUEST[h2_size]',h3_size='$_REQUEST[h3_size]',h4_size='$_REQUEST[h4_size]',h5_size='$_REQUEST[h5_size]',h6_size='$_REQUEST[h6_size]',h1_family='$_REQUEST[h1_family]',h2_family='$_REQUEST[h2_family]',h3_family='$_REQUEST[h3_family]',h4_family='$_REQUEST[h4_family]',h5_family='$_REQUEST[h5_family]',h6_family='$_REQUEST[h6_family]',content_font_color='$_REQUEST[content_font_color]',content_font_size='$_REQUEST[content_font_size]',content_font_family='$_REQUEST[content_font_family]'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateThemeColor($customerId, $header_color, $footer_color, $heading_color, $icon_color) {
        $sql = "update theme_color set header_color='$header_color',header_font_size='$_REQUEST[header_font_size]',header_font_family='$_REQUEST[header_font_family]', footer_color='$footer_color',footer_font_size='$_REQUEST[footer_font_size]',footer_font_family='$_REQUEST[footer_font_family]', heading_color='$heading_color',icon_color='$icon_color',icon_color_hover='$_REQUEST[icon_color_hover]',button_color='$_REQUEST[button_color]',button_color_hover='$_REQUEST[button_color_hover]',h1_color='$_REQUEST[h1_color]',h2_color='$_REQUEST[h2_color]',h3_color='$_REQUEST[h3_color]',h4_color='$_REQUEST[h4_color]',h5_color='$_REQUEST[h5_color]',h6_color='$_REQUEST[h6_color]',h1_size='$_REQUEST[h1_size]',h2_size='$_REQUEST[h2_size]',h3_size='$_REQUEST[h3_size]',h4_size='$_REQUEST[h4_size]',h5_size='$_REQUEST[h5_size]',h6_size='$_REQUEST[h6_size]',h1_family='$_REQUEST[h1_family]',h2_family='$_REQUEST[h2_family]',h3_family='$_REQUEST[h3_family]',h4_family='$_REQUEST[h4_family]',h5_family='$_REQUEST[h5_family]',h6_family='$_REQUEST[h6_family]',content_font_color='$_REQUEST[content_font_color]',content_font_size='$_REQUEST[content_font_size]',content_font_family='$_REQUEST[content_font_family]',status='Y' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function ResetThemeSetting($customerId) {
        $sql = "update theme_color set status='N' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyWithName($property) {

        $sql = "select * from propertyTable where propertyName='$property'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetLanguageData($customerId) {
        $this->custID = $customerId;

        $sql = "select * from manage_header_footer where lan_code='en' and category='Header' and browser_type='Desktop' and customerID='$customerId' GROUP BY primary_link order by primary_sequence asc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetLanguageFooterData($customerId) {
        $sql = "select * from manage_header_footer where lan_code='en' and category='Footer' and browser_type='Desktop' and customerID='$customerId' GROUP BY primary_link order by primary_sequence asc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetHeaderFooterData($customerId) {
        $this->custID = $customerId;

        $sql = "select * from manage_header_footer where customerID='$customerId' and browser_type='Desktop' and category='Header' order by primary_sequence desc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetFooterData($customerId) {
        $sql = "select * from manage_header_footer where customerID='$customerId' and browser_type='Desktop' and category='Footer' order by primary_sequence desc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetHeaderFooterWithID($customerId, $id) {
        $sql = "select * from manage_header_footer where s_no='$id' and lan_code='en' and category='Header' and browser_type='Desktop' and customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetHeaderFooterWithPriLink($customerId, $name) {
        $sql = "select * from manage_header_footer where customerID='$customerId' and browser_type='Desktop' and category='Header' and primary_link='$name' order by secondary_sequence desc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetHeaderFooterWithPriName($customerId, $name) {
        $sql = "select * from manage_header_footer where customerID='$customerId' and browser_type='Desktop'and category='Header' and primary_link='$name' order by secondary_sequence asc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetHeaderFooterWithPriNameForFooter($customerId, $name) {
        $sql = "select * from manage_header_footer where customerID='$customerId' and browser_type='Desktop'and category='Footer' and primary_link='$name' order by secondary_sequence asc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetSecFooterData($customerId, $language, $category, $id) {
        $sql = "select * from manage_header_footer where s_no='$id' and lan_code='$language' and category='$category' and browser_type='Desktop' and customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetFooterPrimaryNameData($customerId, $name, $category) {
        $sql = "select * from manage_header_footer where customerID='$customerId' and browser_type='Desktop' and category='$category' and primary_link='$name' order by secondary_sequence desc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateFooterMenu($customerId, $footer_menu, $old_menu) {
        $sql = "update manage_header_footer set primary_link='$footer_menu' where customerID='$customerId' and category='Footer' and primary_link='$old_menu'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateFooterMenuWithID($customerId, $footer_menu, $menu_id) {
        $sql = "update manage_header_footer set secondary_link='$footer_menu' where customerID='$customerId' and s_no='$menu_id' and category='Footer'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertPrimaryMenu($customerId, $primary_menu, $prim_url, $new_prim_seq) {
        $sql = "insert into manage_header_footer set customerID='$customerId', lan_code='en',browser_type='Desktop',category='Header',primary_link='$primary_menu',secondary_link='0',primary_link_url='$prim_url',primary_sequence='$new_prim_seq'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertSecoundaryMenu($customerId, $new_sec_seq, $name, $ur, $sec_menu, $sec_url, $pm_sec, $cat) {
        $sql = "insert into manage_header_footer set customerID='$customerId', lan_code='en',browser_type='Desktop',category='$cat',primary_link='$name',secondary_link='$sec_menu',primary_link_url='$ur',secondary_link_url='$sec_url',primary_sequence='$pm_sec',secondary_sequence='$new_sec_seq'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertFooterPrimary($customerId, $category, $language, $primary_menu, $new_prim_seq) {
        $sql = "insert into manage_header_footer set customerID='$customerId', lan_code='$language',browser_type='Desktop',category='$category',primary_link='$primary_menu',secondary_link='0',primary_link_url='$ur',primary_sequence='$new_sec_seq'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletedMenu($customerId, $name, $ur) {

        $sql = "delete from manage_header_footer where  primary_link='$name' and secondary_link='0' and customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteSecMenu($datas, $customerId) {
        $sql = "delete from manage_header_footer where s_no='$datas' and category='Header' and customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletePrimaryMenuHeader($datas, $customeID) {
        $sql = "delete from manage_header_footer where customerID='$customeID' and primary_link='$datas' and category='Header'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeletePrimaryMenuFooter($datas, $customerID) {
        $sql = "delete from manage_header_footer where customerID='$customerID' and primary_link='$datas' and category='Footer'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
    //------------------------------------ Get Static Page URLs Function Starts -----------------------------------//


    function getStaticPageUrls($customerId) {
        $this->custID = $customerID;

        $sql = "select * from static_pages where customerID='$customerId' and type='desktop' ";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $staticPageUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $staticPageUrls = $staticPageUrls1;

            return $staticPageUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Static Page URLs Function Ends -----------------------------------//	
    //------------------------------------ Get City URLs Function Starts -----------------------------------//


    function getCityUrls($customerId) {
        $this->custID = $customerID;

        $sql = "select cityUrl from city_url where customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cityUrls = $cityUrls1;

            return $cityUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City URLs Function Ends -----------------------------------//	
    //------------------------------------ Get Static Pages Listing Function Starts -----------------------------------//

    function displayAddMenuTable($customerID) {
        $this->custID = $customerID;
        ?>
        <form action="update_header.php" method="post">
            <div class="col-md-12">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box yellow-soft">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Add More Menu</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <br><br>
                        <div class="form-group">
                            <label class="control-label col-md-3">Primary Menu Name :
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="primary_menu" placeholder="e.g. Home" />
                                <span class="help-block">Enter the desired name of the menu </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <font style="font-size:15px;font-weight:400;"> Select a link for the Menu</font>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <!-- BEGIN Portlet PORTLET-->
                                <div class="portlet box yellow-casablanca">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Property Url </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:200px">
                                            <?php
                                            $propUrlListings = $this->getPropertyUrls($customerID);

                                            foreach ($propUrlListings as $propUrlsData) {
                                                ?>
                                                <?php
                                                if ($propUrlsData->propertyURL != null) {
                                                    ?>
                                                    <input type="checkbox" name="prim_url" class="checkbox-custom" value="<?php echo $propUrlsData->propertyURL; ?>"> <font style="color:#191970;"><?php echo $propUrlsData->propertyURL; ?>.html </font><br><br>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Portlet PORTLET-->
                            </div>

                            <div class="col-md-4">
                                <!-- BEGIN Portlet PORTLET-->
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Static Pages Url </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:200px">
                                            <?php
                                            $staticUrlListings = $this->getStaticPageUrls($customerID);

                                            foreach ($staticUrlListings as $staticUrlData) {
                                                ?>
                                                <?php
                                                if ($staticUrlData->pageUrl != null) {
                                                    ?>
                                                    <input type="checkbox" name="prim_url" value="<?php echo $staticUrlData->pageUrl; ?>"> <font style="color:#191970;"><?php echo $staticUrlData->pageUrl; ?>.html </font><br><br>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Portlet PORTLET-->
                            </div>

                            <div class="col-md-4">
                                <!-- BEGIN Portlet PORTLET-->
                                <div class="portlet box yellow-crusta">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>City Url </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:200px">
                                            <?php
                                            $cityUrlListings = $this->getCityUrls($customerID);

                                            foreach ($cityUrlListings as $cityUrlData) {
                                                ?>

                                                <?php
                                                if ($cityUrlData->cityUrl != null) {
                                                    ?>
                                                    <input type="checkbox" name="prim_url" id="lable1" value="<?php echo $cityUrlData->cityUrl; ?>"> <label for="lable1"><?php echo $cityUrlData->cityUrl; ?>.html </label><br><br>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Portlet PORTLET-->
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="javascript:;" class="btn default button-previous">
                                    <i class="fa fa-angle-left"></i> Back 
                                </a>

                                <input type="submit" class="btn red-mint button-submit" name="prim_submit" value="Add Menu" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>
        </form>
        <?php
    }

    function HomeSliderUpdateOrder($id_array) {
        $DB = new connectDB();
        $DB = $DB->connect();
        $count = 1;

        foreach ($id_array as $id) {
            $update = $DB->query("UPDATE HomeSlider SET image_sequence = $count WHERE slno = $id");
            $count ++;
        }
        return TRUE;
    }

    function PrimaryMenuUpdateOrder($id_array) {
        $DB = new connectDB();
        $DB = $DB->connect();
        $count = 1;

        foreach ($id_array as $id) {
            $update = $DB->query("UPDATE manage_header_footer SET primary_sequence = $count WHERE s_no = $id");
            $count ++;
        }
        return TRUE;
    }

    function SecondaryMenuUpdateOrder($id_array) {
        $DB = new connectDB();
        $DB = $DB->connect();
        $count = 1;

        foreach ($id_array as $id) {
            $update = $DB->query("UPDATE manage_header_footer SET secondary_sequence = $count WHERE s_no = $id");
            $count ++;
        }
        return TRUE;
    }

    //------------------------------------ Get Static Pages Listing Function Ends -----------------------------------//	
}

class analyticsPage extends adminFunction {

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function getMetaTagsData($customerID) {
        $this->custID = $customerID;

        $sql = "select * from metaTags where customerID='$customerID' order by metano desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $metaTags1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $metaTags = $metaTags1;

            return $metaTags;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getAnalyticCodeData($customerId) {

        $sql = "select * from tbl_analytic_code where customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $metaTags1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $metaTags = $metaTags1;

            return $metaTags;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddAnalyticCode($customerId,$analytic_code,$javascript_code) {
        $sql = "insert into tbl_analytic_code set customerID='$customerId', analyticcode='$javascript_code',analyticcode2='$analytic_code'";
      

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateAnalyticCode($customerId,$analytic_code,$javascript_code) {
        $sql = "update tbl_analytic_code set analyticcode='$javascript_code',analyticcode2='$analytic_code' where customerID='$customerId'";
       
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getUsersData($customerId) {
        $sql = "select * from admin_details where customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $metaTags1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $metaTags = $metaTags1;

            return $metaTags;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------Chat table Data function Start----------------------------------------->
    function getChatScriptData($customerId) {
        $sql = "select * from chat_table where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $chatScript1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $chatScript = $chatScript1;

            return $chatScript;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertChatScriptData($customerId, $chat_Script, $chat_Script_body, $chat_Script_footer) {
        $sql = "insert into chat_table set customerID='$customerId', chat_script='$chat_Script',chat_script_body='$chat_Script_body',chat_script_footer='$chat_Script_footer'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateChatScriptCode($customerId, $chat_Script, $chat_Script_body, $chat_Script_footer) {
        $sql = "update chat_table set chat_script='$chat_Script',chat_script_body='$chat_Script_body',chat_script_footer='$chat_Script_footer' where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------End function----------------------------------------------------------->
    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
}

class discountOffers extends adminFunction {

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function getDiscounts($customerID) {
        $this->custID = $customerID;

        $sql = "select * from tbl_discount where customerID='$customerID' order by discountID desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $discountData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $discountData = $discountData1;

            return $discountData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getDiscountWithId($RecordID) {
        $sql = "select * from tbl_discount where discountID='$RecordID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $discountData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $discountData = $discountData1;

            return $discountData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getDiscountBanner($customerId) {
        $sql = "select * from offer_banner where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $discountData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $discountData = $discountData1;

            return $discountData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function InsertofferBanner($customerId, $timestmp, $banner_link) {
        $sql = "insert into offer_banner(customerID,banner,banner_url) values('$customerId','$timestmp','$banner_link')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddNewDiscount($customerId, $discountType, $discountName, $discountFrom, $discountTo, $propName, $roomName, $discountPercent, $lastMinute, $earlyBird, $longStay) {
        $sql = "insert into tbl_discount(customerID,discountType,discountName,discountFrom,discountTo,propertyName,roomName,discountValue,lastMinute,earlybird,longStay) values('$customerId','$discountType','$discountName','$discountFrom','$discountTo','$propName','$roomName','$discountPercent','$lastMinute','$earlyBird','$longStay')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateDiscount($recID, $discountType, $discountName, $discountFrom, $discountTo, $propName, $roomName, $discountPercent, $lastMinute, $earlyBird, $longStay) {
        $sql = "update tbl_discount set discountType='$discountType',discountName='$discountName',discountFrom='$discountFrom',discountTo='$discountTo',propertyName='$propName',roomName='$roomName',discountValue='$discountPercent',lastMinute='$lastMinute',earlybird='$earlyBird',longStay='$longStay' where discountID='$recID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateStatusDeactive($recordiD) {
        $sql = "update tbl_discount set status='N' where discountID='$recordiD'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateStatusActive($recordiD) {
        $sql = "update tbl_discount set status='Y' where discountID='$recordiD'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
}

//----------------------------------Upload Pdf file-------------------------------------------------------->
class File {

    private $_suportedFormate = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif', 'application/pdf'];

    function UploadFile($file) {
        /// print_r($file);
        if (is_array($file)) {
            if (in_array($file['type'], $this->_suportedFormate)) {
                move_uploaded_file($file['tmp_name'], $file['name']);
                echo 'Successfully Uploaded file';
            } else {
                echo 'file formate is not supported';
            }
        } else {
            echo 'No file Uploaded';
        }
    }

}

class manageEnquiry extends adminFunction {

    function GetAllEnquiryWithCustomerId($customerId) {
        $sql = "select * from inquiry where customerID='$customerId' order by slno desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllEnquiryData($customerId) {
        $sql = "select * from inquiry where customerID='$customerId' order by slno desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DeleteEnquiryData($customerId) {
        $sql = "delete from inquiry where slno='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    
    function DeletesubscribeData($customerId,$s_no,$email) {
        $sql = "delete from newsletter where customerID='$customerId' and s_no= '$s_no' and email='$email' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetPropertyInquiry($customerId, $propID) {
        $sql = "select * from inquiry where customerID='$customerId' and propertyID ='$propID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetPropertyInquiryType($customerId, $propID, $inqid) {
        $sql = "select * from inquiry where  inquiryType='$inqid' and customerID='$customerId' and propertyID ='$propID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetInquiryType($customerId, $inqid) {
        $sql = "select * from inquiry where  inquiryType='$inqid' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetInquiryDateFilter($customerId, $startDate, $endDate) {
        $sql = "select * from inquiry where customerID='$customerId' and  (timestamp BETWEEN '$startDate' AND '$endDate')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetInquiryStatusByFilter($customerId, $status) {
        $sql = "select * from inquiry where customerID='$customerId' and com_stat='$status' ";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddInquiryPanel($customerId, $username, $email, $mobile, $commets, $recvDate) {
        $sql = "insert into inquiry set customerID='$customerId', name='$username',email='$email',mobile='$mobile',comments='$commets',recvDate='$recvDate'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetScheduleEnquiry($customerId, $inquiry_type) {
        $sql = "select * from inquiry where  inquiryType='$inquiry_type' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $enquiryData = $enquiryData1;
            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetMailSignature() {
        $sql = "select * from inquiry_mail_signature";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetTodayInquiry($customerId, $inqdate) {
        $sql = "select * from inquiry where recvDate='$inqdate' AND customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetRequestCallInquiry($customerId) {
        $sql = "select * from inquiry where inquiryType='RequestaCall' AND customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetTodayReqInquiry($customerId, $inqdate) {
        $sql = "select * from inquiry where inquiryType='RequestaCall' AND recvDate='$inqdate' AND customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $enquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $enquiryData = $enquiryData1;

            return $enquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateInquiryStatus($customerId, $nam, $maill, $com_up, $sl, $stat) {
        $sql = "update inquiry set comments='$com_up',com_stat='$stat' where slno='$sl'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AddComments($customerId, $sl, $com_up, $stat, $nam) {
        $sql = "insert into manage_comments(customerID,comments_id,comments,name,status) values('$customerId','$sl','$com_up','$nam','$stat')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetMultipleComments($sl, $customerId) {
        $sql = "select * from manage_comments where comments_id='$sl' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateInquiryMail($com, $sl) {
        $sql = "update inquiry set comm='$com' where slno='$sl'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

//-----------Get Mail Data For mail template-----------------------------------
    function GetMailTemplate($customerId) {
        $sql = "select * from mail_templates where user='admin' and customerID='$customerId'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $mailData = $mailData1;

            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetMailTemplateWithID($id) {
        $sql = "select * from mail_templates where s_no='$id'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //--------------Get All Month Inquiry Function Start------------>

    function JanInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='01'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function FebInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='02'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function MarInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='03'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AprInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='04'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function MayInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='05'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function JunInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='06'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function JulInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='07'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function AugInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='08'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function SepInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='09'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function OctInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='10'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function NovInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='11'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function DecInquiry($customerId, $year_org) {
        $sql = "select * from inquiry where customerID = '$customerId' and year='$year_org' and month='12'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-----------Insert Mail Data For mail template-----------------------------------
    function InsertMailData($customerId, $send_stat, $temp_name, $temp_type, $temp_subject, $temp_signature, $temp_body, $mail_from, $mail_to, $date, $status, $form1, $cc) {
        $sql = "insert into mail_templates(customerID,name,type,user,subject,signature,body,mail_from,mail_to,mail_cc,date,status,form1,`$send_stat`) values ('$customerId','$temp_name','$temp_type','admin','$temp_subject','$temp_signature','$temp_body','$mail_from','$mail_to','$cc','$date','$status','$form1','Active')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateMailTemplate($temp_name, $temp_type, $temp_subject, $temp_body, $mail_from, $mail_to, $date, $status, $s_no, $form1, $cc) {
        $sql = "update mail_templates set name='$temp_name', type='$temp_type',user='admin',subject='$temp_subject',signature='$temp_signature',body='$temp_body',mail_from='$mail_from',mail_to='$mail_to',mail_cc='$cc',date='$date',status='$status',form1='$form1' where s_no='$s_no'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getnewLetterToday($customerId, $inqdate2) {
        $sql = "select * from newsletter where customerID = '$customerId' and recvDate='$inqdate2'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getnewLetter($customerId) {
        $sql = "select * from newsletter where customerID = '$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyReview($customerId) {
        $sql = "select * from property_reviews where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $mailData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $mailData = $mailData1;
            return $mailData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllManageBookingData($customerId) {
        $sql = "select * from manage_booking where customerID='$customerId' order by s_no desc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetBookingStatus($customerId, $inqid) {
        $sql = "select * from manage_booking where customerID='$customerId' and status='$inqid' order by s_no desc";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetBookingByID($sl_no, $customerId) {
        $sql = "select * from manage_booking where customerID='$customerId' and s_no='$sl_no'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

}
?>
<?php

class UserDetails {

    function CreateUser($customerId, $user_id, $password, $manager_name, $user_email, $mobile) {
        $sql = "insert into add_user(customerID,user_id,password,managerName,email,mobile) values('$customerId','$user_id','$password','$manager_name','$user_email','$mobile')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
     function ApprovedUser($id,$status){
        $sql = "update add_user set status='$status' where id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    

    function get_AlluserDetails($customerId) {
        $sql = "select * from add_user where customerID='$customerId' and status !='D'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
	  function get_user_data_withID($id) {
        $sql = "select * from add_user where id = '$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function get_userDetailsWithID($customerId, $user_id) {
        $sql = "select * from add_user where customerID='$customerId' and id='$user_id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function UpdateUser($customerId, $user_id, $password, $manager_name, $user_email, $mobile) {
        $sql = "update add_user set  password='$password',managerName='$manager_name',email='$user_email',mobile='$mobile' where id='$user_id' and customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function getPropertyIDWithUser($customerID, $user_id) {
        $sql = "select * from propertyTable where customerID='$customerID' and user_id='$user_id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetAllEnquiryWithPropertyID($customerId, $propertyID) {
        $sql = "select * from inquiry where customerID='$customerId' and propertyID='$propertyID'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function GetUserdataWithUserID($customerId, $user_id) {
        $sql = "select * from add_user where customerID='$customerId' and user_id='$user_id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    function ConnectToInstagram($customerId, $username, $fullname) {
        $sql = "insert into instagram(customerID,user_name,full_name,status) values('$customerId','$username','$fullname','Y')";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey1 = $DB->query($sql);
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
    function get_all_service_booking($customerId) {
        $sql = "select * from service_booking where customerID='$customerId'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
	 function get_user_seller($id) {
        $sql = "select * from add_user where id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }
	function get_service_data($id) {
        $sql = "select * from places_table where id='$id'";
        try {
            $DB = new connectDB();
            $DB = $DB->connect();
            $exQuey = $DB->query($sql);
            $Data1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $Data = $Data1;
            return $Data;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

}
?>


<?php

class modifyTable {

    var $tableName;
    var $condition;
    var $updateColumn;

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function deleteRecord($table, $cond) {
        $this->tableName = $table;
        $this->condition = $cond;

        $sql = "delete from " . $table . " where " . $cond;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
}
?>
	