<?php
ob_start();
date_default_timezone_set("Asia/Kolkata");
session_start();

include SITE_URL_PATH . "include/config.php";
include SITE_URL_PATH . "include/common-functions.php";
include SITE_URL_PATH . "include/return_functions.php";

// include "../sitepanel/admin-function.php";


function check_session($sess_name, $REDIRECT_URL) {
    if (!strlen($_SESSION[$sess_name])) {
        header("Location:$REDIRECT_URL");
        exit;
    }
}
?>

<?php

//----------------------------------------------------------- Class For Repeated functions starts (Main CMS Class) ------------------------------------------------------------//
class adminFunction {

    var $custID;
    var $propertyID;
    var $roomId;
    var $pageUrl;
    //---------------------------------- Fetch New Booking Number Function Starts ---------------------------------//

    var $checDate;

    function newBookingNumber($cheDat, $customerID) {
        $this->checDate = $cheDat;
        $this->custID = $customerID;

        $sql = "select * from manage_booking where date='$cheDat' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $newBookNumData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $newBookNumData = $newBookNumData1;

            return $newBookNumData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Fetch New Booking Number Function Ends ---------------------------------//
    //---------------------------------- Fetch New Inquiries Number Function Starts ---------------------------------//


    var $views;

    function newInquiryNumber($view, $customerID) {
        $this->views = $view;
        $this->custID = $customerID;

        $sql = "select slno from inquiry where view='$view' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $inquiryNumData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $inquiryNumData = $inquiryNumData1;

            return $inquiryNumData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Fetch New Inquiries Number Function Starts ---------------------------------//
    //------------------------------------ Fetch Inquiries Data Function Starts -----------------------------------//



    var $commentStat;
    var $dataorder;

    function getInquiryData($comStat, $customerID, $datOrder) {
        $this->commentStat = $comStat;
        $this->custID = $customerID;
        $this->dataorder = $datOrder;


        $sql = "select * from inquiry where com_stat='$comStat' and customerID='$customerID' " . $datOrder;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $inquiryData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $inquiryData = $inquiryData1;

            return $inquiryData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Fetch Inquiries Data Function Starts -----------------------------------//
    //-------------------------------------- Display User Data Function Starts -------------------------------------//

    var $userId;

    function getUserData($user_id, $customerID) {
        $this->userId = $user_id;
        $this->custID = $customerID;


        $sql = "select * from admin_details where user_id='$user_id' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $profilePicData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $profilePicData = $profilePicData1[0];

            return $profilePicData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------------------- Display User Data Function Ends -------------------------------------//
    //------------------------------------- Fetch Language Data Function Starts ------------------------------------//

    var $langCond;

    function getLangData($customerID, $lanCon) {
        $this->custID = $customerID;
        $this->langCond = $lanCon;

        $sql = "select * from lang_sudomian where customerID='$customerID' and $lanCon";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $langData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $langCnt = count($langData1);

            if ($langCnt == "0") {
                
            } elseif ($langCnt == "1") {
                $langData_se = $langData1[0];

                $langData = $langData_se->langcode;
            } else {
                $langData_se = $langData1;

                $lanconc = '';
                foreach ($langData_se as $lanrow) {
                    $lanconc .= $lanrow->langcode . "^";
                    $langData = $lanconc;
                }
            }

            return $langData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Fetch Language Data Function Ends -----------------------------------//
    //-------------------------------------- Get User IP Address Function Starts -------------------------------------//

    function getUserIP() {
        try {
            $ipaddress = '';
            if ($_SERVER['HTTP_CLIENT_IP']) {
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            } else if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else if ($_SERVER['HTTP_X_FORWARDED']) {
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            } else if ($_SERVER['HTTP_FORWARDED_FOR']) {
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            } else if ($_SERVER['HTTP_FORWARDED']) {
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            } else if ($_SERVER['REMOTE_ADDR']) {
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            } else {
                $ipaddress = 'UNKNOWN';
            }

            return $ipaddress;
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------------------- Get User IP Address Function Ends -------------------------------------//
    //-------------------------------------- Connect Cloudinary User Function Starts -------------------------------------//

    function connectCloudinaryAccount($customerID) {
        $this->custID = $customerID;

        require '../Cloudinary.php';
        require '../Uploader.php';
        require '../Api.php';

        $sql = "select * from admin_details where customerID='$customerID' order by slno asc limit 1";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cloudinaryCredentials1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cloudinaryCredentials = $cloudinaryCredentials1[0];

            Cloudinary::config(array(
                "cloud_name" => $cloudinaryCredentials->cloud_name,
                "api_key" => $cloudinaryCredentials->api_key,
                "api_secret" => $cloudinaryCredentials->api_secret
            ));

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //-------------------------------------- Connect Cloudinary User Function Ends -------------------------------------//
    //------------------------------------ Get Full Month Name Function Starts -----------------------------------//

    var $cuurentMonth;

    function getMonthFullName($passMonth) {
        $this->cuurentMonth = $passMonth;

        try {
            if ($passMonth == "01") {
                $fullMonthName = "January";
            }
            if ($passMonth == "02") {
                $fullMonthName = "February";
            }
            if ($passMonth == "03") {
                $fullMonthName = "March";
            }
            if ($passMonth == "04") {
                $fullMonthName = "April";
            }
            if ($passMonth == "05") {
                $fullMonthName = "May";
            }
            if ($passMonth == "06") {
                $fullMonthName = "June";
            }
            if ($passMonth == "07") {
                $fullMonthName = "July";
            }
            if ($passMonth == "08") {
                $fullMonthName = "August";
            }
            if ($passMonth == "09") {
                $fullMonthName = "September";
            }
            if ($passMonth == "10") {
                $fullMonthName = "October";
            }
            if ($passMonth == "11") {
                $fullMonthName = "November";
            }
            if ($passMonth == "12") {
                $fullMonthName = "December";
            }

            return $fullMonthName;
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Full Month Name Function Ends -----------------------------------//
}

//----------------------------------------------------------- Class For Graphs of Dashboard ------------------------------------------------------------//

class inquiryGraph {

    var $month;
    var $custId;

    //------------------------------------ Inquiry Graph Function Starts -----------------------------------//

    function getInquiryGraphData($mon, $customerID) {
        $this->month = $mon;
        $this->custId = $customerID;

        $year_org = date('Y');

        $sql = "select recvDate from inquiry where year='$year_org' and month='$mon' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $inquiryMonData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $inquiryMonData = count($inquiryMonData1);

            return $inquiryMonData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Inquiry Graph Function Ends -----------------------------------//	
    //---------------------------------- Site Visit Graph Function Starts -------------------------------//

    var $week;

    function getSiteVisitGraphData($weeks, $customerID) {
        $this->week = $weeks;
        $this->custId = $customerID;

        $sql = "select * from google_analytic where week='$weeks' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $siteVisitData2 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $siteVisitData1 = $siteVisitData2[0];
            $siteVisitData = $siteVisitData1->sess;

            return $siteVisitData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------------- Site Visit Graph Function Ends -------------------------------//
}

//----------------------------------------------------------- Class For Index Page of CMS ------------------------------------------------------------//

class indexData extends adminFunction {

    var $password;
    var $userType;

    //------------------------------------ Index Page Data Function Starts -----------------------------------//

    function authUserLogin($userid, $passwrd, $usertype) {
        $this->userId = $userid;
        $this->password = $passwrd;
        $this->userType = $usertype;

        $sql = "select slno,last_login,user_type,user_id,customerID,email from admin_details where user_id='$userid' and pwd='$passwrd' and user_type='$usertype'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $indexpageData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $indexpageData = $indexpageData1[0];

            return $indexpageData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Index Page Data Function Ends -----------------------------------//
    //------------------------------------ Updating Login Log Function Starts -----------------------------------//

    var $ipAddress;
    var $currentDate;
    var $currentTime;

    function updateLoginLog($userid, $ipAddr, $currDate, $currTime, $customerID) {
        $this->userId = $userid;
        $this->ipAddress = $ipAddr;
        $this->currentDate = $currDate;
        $this->currentTime = $currTime;
        $this->custID = $customerID;

        $sql = "insert into log_table(customerID,user_id,ipaddr,curr_date,curr_time) values('$customerID','$userid','$ipAddr','$currDate','$currTime')";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateLoginLg = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Updating Login Log Function Ends -----------------------------------//
    //------------------------------------ Updating Login Time in Admin Details Function Starts -----------------------------------//

    function updateAdmindetail($userid, $passwrd, $usertype, $currDate, $currTime, $customerID) {
        $this->userId = $userid;
        $this->password = $passwrd;
        $this->userType = $usertype;
        $this->currentDate = $currDate;
        $this->currentTime = $currTime;
        $this->custID = $customerID;

        $sql = "update admin_details set last_login='$currDate',lat_login_time='$currTime' where user_id='$userid' and pwd='$passwrd' and user_type='$usertype' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            // $updateAdminDet = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Updating Login Time in Admin Details Function Ends -----------------------------------//
    //------------------------------------ Sending Mail Function Starts -----------------------------------//

    var $toMail;

    function sendLoginMail($userid, $ipAddr, $currDate, $currTime, $customerID, $mailerAddr) {
        $this->userId = $userid;
        $this->ipAddress = $ipAddr;
        $this->currentDate = $currDate;
        $this->currentTime = $currTime;
        $this->custID = $customerID;
        $this->toMail = $mailerAddr;

        try {
            $subject2 = "Regarding Login Details";
            $toBeSentMail = '
												<div style="border:1px solid #9e9a9a;height:auto;width:500px;padding:20px;background:rgb(242, 255, 250);padding-bottom:50px;">
													<p style="color:#08a3bd;font-size:26px;"> Dear Admin, </p><br>

													<p style="font-size:20px;color:#607D8B;"> Please Review the last login details. </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> User ID : <font style="color:#FF5722;"> ' . $userid . ' </font> </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> IP Address : <font style="color:#FF5722;"> ' . $ipAddr . ' </font> </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> Date Of Login : <font style="color:#FF5722;"> ' . $currDate . ' </font> </p>

													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> Time of Login : <font style="color:#FF5722;"> ' . $currTime . ' </font> </p>
													
													<p style="font-size:17px;color:#2196F3;font-weight:bold;margin-left:40px;"> Customer ID : <font style="color:#FF5722;"> ' . $customerID . ' </font> </p><br>

													<p style="color:#607D8B;font-size:20px;"> Thank You </p><br><br>

													<p style="color:#607D8B;font-size:20px;"> Regards </p>

													<p style="color:#607D8B;font-size:20px;"> Nodal.Direct Team </p>
												</div>';

            $to_mail = $mailerAddr;
            $from = "support@nodal.direct";


            $headers = "From:" . $from . "\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-Type: text/html; charset=utf-8\r\n";
            $headers.= "X-Priority: 1\r\n";
            mail($to_mail, $subject2, $toBeSentMail, $headers);
        } catch (Execption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Sending Mail Function Ends -----------------------------------//
}

//----------------------------------------------------------- Class For Welcome Page of CMS ------------------------------------------------------------//

class welcomeData extends adminFunction {

    //------------------------------------ Index Page Data Function Starts -----------------------------------//

    function getRecentUsers($customerID, $datOrder) {
        $this->custID = $customerID;
        $this->dataorder = $datOrder;

        $sql = "select * from admin_details where customerID='$customerID' " . $datOrder;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $recentUsers1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $recentUsers = $recentUsers1;

            return $recentUsers;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Index Page Data Function Ends -----------------------------------//
}

//----------------------------------------------------------- Class For Property Page of CMS ------------------------------------------------------------//

class propertyData extends adminFunction {

    //------------------------------------ Get City Listing Function Starts -----------------------------------//

    function getCityList($customerID) {
        $this->custID = $customerID;
        $this->dataorder = $datOrder;

        $sql = "select * from propertyTable where customerID='$customerID' group by cityName";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cityList = $cityList1;

            return $cityList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City Listing Function Ends -----------------------------------//	
    //---------------------------- Get Property Number in a City Function Starts ---------------------------//

    var $cityName;

    function getPropertyCount($customerID, $cityNam) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;

        $sql = "select propertyID from propertyTable where customerID='$customerID' and cityName='$cityNam'";
        // echo $sql;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Number in a City Function Ends ---------------------------//	
    //---------------------------- Get Property Number in a City Function Starts ---------------------------//

    function getCityProperty($customerID, $cityNam) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;

        $sql = "select * from propertyTable where cityName='$cityNam' and customerID='$customerID' and status='Y'||status='N'";
        // echo $sql;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Number in a City Function Ends ---------------------------//	
    //---------------------------- Get Property Details in a City by Type Function Starts ---------------------------//

    var $propertyType;

    function getCityPropertyByType($customerID, $cityNam, $propTypID) {
        $this->custID = $customerID;
        $this->cityName = $cityNam;
        $this->propertyTypeID = $propTypID;

        if ($cityNam == 'All') {
            $passingValue = '';
        } else {
            $passingValue = "(cityName='" . $cityNam . "'||subCity='" . $cityNam . "') and ";
        }

        $sql = "select * from propertyTable where " . $passingValue . "customerID='$customerID' and tbl_property_type='$propTypID'";
        // echo $sql;
        // die;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propertyCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propertyCount = $propertyCount1;

            return $propertyCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Details in a City by Type Function Ends ---------------------------//	
    //---------------------------- Get Image Count of a Property Function Starts ---------------------------//


    function getPropertyPhotoCount($propID, $customerID) {
        $this->propertyID = $propID;
        $this->custID = $customerID;

        $sql = "select imagesID from propertyImages where propertyID='$propID' and imageType='property'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityPropCount1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;
            $cityPropCount = $cityPropCount1;

            return $cityPropCount;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Image Count of a Property Function Ends ---------------------------//	
    //---------------------------- Get Property Type Name Function Starts ---------------------------//

    var $propertyTypeID;

    function getPropertyTypeName($propTypID) {
        $this->propertyTypeID = $propTypID;
        $this->custID = $customerID;

        $sql = "select * from tbl_property_type where type_id='$propTypID' and customerID='$customerID'";
        // echo $sql;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propType1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propType = $propType1[0];

            return $propType;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Type Name Function Ends ---------------------------//	
    //---------------------------- Get Total Booking Function Starts ---------------------------//

    var $propertyName;
    var $month;

    function getTotalBooking($mon, $propnam, $customerID, $datordr) {
        $this->month = $mon;
        $this->propertyName = $propnam;
        $this->custID = $customerID;
        $this->dataorder = $datordr;

        $year = date('Y');
        // $month = date('m');
        $sql = "select * from manage_booking where start_month='$mon' and start_year='$year' and property_enquired='$propnam' and customerID='$customerID'" . $datordr;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $totalBooking1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $totalBooking = $totalBooking1;

            return $totalBooking;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Type Name Function Ends ---------------------------//	
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getRoomDetail($propID) {
        $this->propertyID = $propID;

        $sql = "select * from propertyRoom where propertyID='$propID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomDetail1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomDetail = $roomDetail1;

            return $roomDetail;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getAllPropTyp($customerID) {
        $this->custID = $customerID;

        $sql = "select * from tbl_property_type where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allPropTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allPropTyp = $allPropTyp1;

            return $allPropTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get City List Function Starts -----------------------------//

    function getAllCity($customerID) {
        $this->custID = $customerID;

        $sql = "select * from city_url where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allCity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allCity = $allCity1;

            return $allCity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get City List Function Ends -----------------------------//
    //------------------------------ Get Sub-City List Function Starts -----------------------------//

    function getAllSubCity($customerID) {
        $this->custID = $customerID;

        $sql = "select * from subcity_url where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allSubCity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allSubCity = $allSubCity1;

            return $allSubCity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Sub-City List Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getAllRoomTyp($customerID) {
        $this->custID = $customerID;

        $sql = "select * from roomType where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $allRoomTyp1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $allRoomTyp = $allRoomTyp1;

            return $allRoomTyp;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    function getAmenitiesList() {

        $sql = "select * from roomAmenties";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $roomAmenity1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $roomAmenity = $roomAmenity1;

            return $roomAmenity;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //------------------------------ Get Room Detail Function Starts -----------------------------//

    var $googleMapUrl;
    var $trustYouUrl;
    var $tagLine;
    var $propertyFeature;
    var $serviceAmenities;
    var $safetyPolicies;
    var $inApartmentFacilty;
    var $kitchenFeatures;
    var $entertainmentFacility;

    function addNewProperty() {
        $this->propertyName = $propnam;
        $this->googleMapUrl = $gmapUrl;
        $this->trustYouUrl = $trustUrl;
        $this->tagLine = $taglin;
        $this->propertyFeature = $propFeature;
        $this->serviceAmenities = $serviceAmnty;
        $this->safetyPolicies = $safetyPlcy;
        $this->inApartmentFacilty = $inAprtmntFclty;
        $this->kitchenFeatures = $kitchenFeture;
        $this->entertainmentFacility = $entertainment;




        $sql = "insert into propertyTable set propertyName='$_REQUEST[propertyName]',gmapurl='$_REQUEST[gmapurl]',trustURL='$_REQUEST[trustURL]',tagLine='$_REQUEST[tagLine]',propertyfeatures='$_REQUEST[propertyfeatures]',servicesnamenities='$_REQUEST[servicesnamenities]',safetynsecurity='$_REQUEST[safetynsecurity]',inapartmentfacilities='$_REQUEST[inapartmentfacilities]',kitchenfeatures='$_REQUEST[kitchenfeatures]',entertainmentleisure='$_REQUEST[entertainmentleisure]',mobpropertyfeatures='$_REQUEST[propertyfeatures]',mobservicesnamenities='$_REQUEST[servicesnamenities]',mobsafetynsecurity='$_REQUEST[safetynsecurity]',mobinapartmentfacilities='$_REQUEST[inapartmentfacilities]',mobkitchenfeatures='$_REQUEST[kitchenfeatures]',mobentertainmentleisure='$_REQUEST[entertainmentleisure]',propertyURL='$propertyURL', bookingURL='$_REQUEST[bookingURL]', tbl_property_type='$_REQUEST[tbl_property_type]', address='$_REQUEST[address]',subCity='$_REQUEST[subCity]',cityName='$_REQUEST[cityName]', state='$_REQUEST[state]', country='$_REQUEST[country]', floorsno='$_REQUEST[floorsno]',totalRooms='$_REQUEST[totalRooms]', roomType='^$businessType1^', roomAmenties='^$roomAmenity^', checkIN='$checkIN $checkIN_am_pm', checkOut='$_REQUEST[checkOut] $checkOut_am_pm', propertyInfo='$_REQUEST[propertyInfo]', propertyPolicy='$_REQUEST[propertyPolicy]', mobpropertyInfo='$_REQUEST[propertyInfo]', mobpropertyPolicy='$_REQUEST[propertyPolicy]',managerName='$_REQUEST[managerName]',videoURL='$_REQUEST[videoURL]', managerContactNO='$_REQUEST[managerContactNO]',propertyPhone='$_REQUEST[propertyPhone]', nearBy='$nearBy', feature='$feature', twitter_link='$_REQUEST[propertyTwitterLink]', facebook_link='$_REQUEST[propertyFacebookLink]', linkedin_link='$_REQUEST[propertyLinkedinLink]', google_link='$_REQUEST[propertyGoogleLink]', status='Y'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey1 = $DB->query($sql);
         
           
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------ Get Room Detail Function Ends -----------------------------//
    //---------------------------- Get Property Photo Function Starts ---------------------------//

    function getPropertyPhoto($propID) {
        $this->propertyID = $propID;

        $sql = "select * from propertyImages where propertyID='$propID' and imageType='property' and type='home' order by imagesID desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropPhoto = $getPropPhoto1;

            return $getPropPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Photo Function Ends ---------------------------//
    //---------------------------- Get Room Photo Function Starts ---------------------------//

    function getRoomPhoto($propID, $roomID, $datOrder) {
        $this->propertyID = $propID;
        $this->roomId = $roomID;
        $this->dataorder = $datOrder;

        $sql = "select * from propertyImages where propertyID='$propID' and roomID='$roomID' and imageType='room' " . $datOrder;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getRoomPhoto1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getRoomPhoto = $getRoomPhoto1;

            return $getRoomPhoto;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Room Photo Function Ends ---------------------------//
    //---------------------------- Get Property Questions Function Starts ---------------------------//

    function getPropertyQuestions($propID) {
        $this->propertyID = $propID;

        $sql = "select * from quesAns where propertyID='$propID' order by quesID desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $getPropQuest1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $getPropQuest = $getPropQuest1;

            return $getPropQuest;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //---------------------------- Get Property Questions Function Ends ---------------------------//
}

class inventoryData extends adminFunction {

    //------------------------------------ Get City Listing Function Starts -----------------------------------//

    function getPropertyListing($customerID) {
        $this->custID = $customerID;

        $sql = "select * from propertyTable where customerID='$customerID' order by propertyID desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $propList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $propList = $propList1;

            return $propList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City Listing Function Ends -----------------------------------//	
    //------------------------------------ Get Top Date Calendar Header Function Starts -----------------------------------//

    var $currentDate;

    function getCalendarHeader($passMonth, $passDate) {
        $this->cuurentMonth = $passMonth;
        $this->currentDate = $passDate;

        if ($passMonth == "01" || $passMonth == "03" || $passMonth == "05" || $passMonth == "07" || $passMonth == "08" || $passMonth == "10" || $passMonth == "12") {
            for ($i = 1; $i <= 31; $i++) {
                ?>
                <td style="width:3%;<?php if ($i == $passDate) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                <?php echo $i; ?>
                    <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                </td>			
                    <?php
                }
            } elseif ($passMonth == "04" || $passMonth == "06" || $passMonth == "09" || $passMonth == "11") {
                for ($i = 1; $i <= 30; $i++) {
                    ?>
                <td style="width:3%;<?php if ($i == $passDate) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                <?php echo $i; ?>
                    <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                </td>
                    <?php
                }
            } elseif ($passMonth == "02") {
                for ($i = 1; $i <= 28; $i++) {
                    ?>
                <td style="width:3%;<?php if ($i == $passDate) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                <?php echo $i; ?>
                    <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                </td>
                    <?php
                }
            }
        }

        //------------------------------------ Get Top Date Calendar Header Function Ends -----------------------------------//	
        //------------------------------------ Get Property Details By Name Function Starts -----------------------------------//


        var $propertyFullName;

        function getPropDetailsByName($propertyName, $customerID) {
            $this->propertyFullName = $propertyName;
            $this->custID = $customerID;

            $sql = "select * from propertyTable where propertyName='$propertyName' and customerID='$customerID'";

            try {
                $DB = new connectDB();
                $DB = $DB->connect();

                $exQuey = $DB->query($sql);
                $propDataName1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
                $DB = null;

                $propDataName = $propDataName1;

                return $propDataName;
            } catch (PDOExecption $e) {
                echo "{'error':'{'text':'.$e->getMessage().'}'";
            }
        }

        //------------------------------------ Get Property Details By Name Function Ends -----------------------------------//
        //------------------------------------ Get Room Details Function Starts -----------------------------------//


        function getRoomDetails($propID) {
            $this->propertyID = $propID;

            $sql = "select * from propertyRoom where propertyID='$propID'";

            try {
                $DB = new connectDB();
                $DB = $DB->connect();

                $exQuey = $DB->query($sql);
                $roomData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
                $DB = null;

                $roomData = $roomData1;

                return $roomData;
            } catch (PDOExecption $e) {
                echo "{'error':'{'text':'.$e->getMessage().'}'";
            }
        }

        //------------------------------------ Get Room Details Function Ends -----------------------------------//
        //------------------------------------ Get Inventory Details Function Starts -----------------------------------//


        var $currentYear;

        function getInventoryDetails($propID, $roomID, $passMonth, $passYear) {
            $this->propertyID = $propID;
            $this->roomId = $roomID;
            $this->cuurentMonth = $passMonth;
            $this->currentYear = $passYear;

            $sql = "select * from manage_inventory where property_id='$propID' and room_id='$roomID' and month='$passMonth' and year='$passYear'";

            try {
                $DB = new connectDB();
                $DB = $DB->connect();

                $exQuey = $DB->query($sql);
                $roomInventory1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
                $DB = null;

                $roomInventory = $roomInventory1;

                return $roomInventory;
            } catch (PDOExecption $e) {
                echo "{'error':'{'text':'.$e->getMessage().'}'";
            }
        }

        //------------------------------------ Get Inventory Details Function Ends -----------------------------------//
        //------------------------------------ Display Final Inventory Details Function Starts -----------------------------------//


        var $inventoryDataCount;
        var $roomType;
        var $roomAvailability;
        var $fullInventoryData;

        function displayInventory($inventryDataCnt, $propID, $roomTyp, $roomNo, $passMonth, $passDate, $invntryDat) {
            $this->inventoryDataCount = $inventryDataCnt;
            $this->propertyID = $propID;
            $this->roomType = $roomTyp;
            $this->roomAvailability = $roomNo;
            $this->cuurentMonth = $passMonth;
            $this->currentDate = $passDate;

            $this->fullInventoryData = $invntryDat;

            if ($inventryDataCnt == "0") {
                if ($passMonth == "01" || $passMonth == "03" || $passMonth == "05" || $passMonth == "07" || $passMonth == "08" || $passMonth == "10" || $passMonth == "12") {
                    for ($j = 1; $j <= 31; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        ?>
                    <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                        <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                    </td>		
                    <?php
                }
            } elseif ($passMonth == "04" || $passMonth == "06" || $passMonth == "09" || $passMonth == "11") {
                for ($j = 1; $j <= 30; $j++) {
                    $newIdForDetails = $roomTyp . "~" . $j;
                    ?>
                    <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                        <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                    </td>
                    <?php
                }
            } elseif ($passMonth == "02") {
                for ($j = 1; $j <= 28; $j++) {
                    $newIdForDetails = $roomTyp . "~" . $j;
                    ?>
                    <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                        <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                    </td>
                    <?php
                }
            }
        } else {
            foreach ($invntryDat as $inventoryPassedData) {
                if ($passMonth == "01" || $passMonth == "03" || $passMonth == "05" || $passMonth == "07" || $passMonth == "08" || $passMonth == "10" || $passMonth == "12") {
                    for ($j = 1; $j <= 31; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        if ($inventoryPassedData->{$j} == null) {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        } else {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $inventoryPassedData->{$j}; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" style="<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        }
                    }
                } elseif ($passMonth == "04" || $passMonth == "06" || $passMonth == "09" || $passMonth == "11") {
                    for ($j = 1; $j <= 30; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        if ($inventoryPassedData->{$j} == null) {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        } else {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $inventoryPassedData->{$j}; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" style="<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        }
                    }
                } elseif ($passMonth == "02") {
                    for ($j = 1; $j <= 28; $j++) {
                        $newIdForDetails = $roomTyp . "~" . $j;
                        if ($inventoryPassedData->{$j} == null) {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;<?php if ($j < $passDate) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $roomNo; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" <?php if ($j < $passDate) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        } else {
                            ?>
                            <td id="input_css<?php echo $newIdForDetails; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                <input name="<?php echo $roomTyp . "~" . $propID; ?>" id="<?php echo $newIdForDetails; ?>" type="text" value="<?php echo $inventoryPassedData->{$j}; ?>" onchange="update_avail('<?php echo $newIdForDetails; ?>')" class="input_css" style="<?php if ($inventoryPassedData->{$j} == "0") { ?><?php if ($j < $passDate) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $passDate) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $passDate) { ?> readonly="" <?php } ?> />
                            </td>
                            <?php
                        }
                    }
                }
            }
        }
    }

    //------------------------------------ Display Final Inventory Details Function Ends -----------------------------------//	
}

class staticPageData extends adminFunction {

    //------------------------------------ Get Static Pages Listing Function Starts -----------------------------------//

    function getStaticPageListing($customerID) {
        $this->custID = $customerID;

        $sql = "select * from static_pages where type='desktop' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $staticPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $staticPageList = $staticPageList1;

            return $staticPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Static Pages Listing Function Ends -----------------------------------//	
    //------------------------------------ Get Client Page Data Function Starts -----------------------------------//


    function getClientPageData($filename, $customerID) {
        $this->pageUrl = $filename;
        $this->custID = $customerID;

        $sql = "select * from static_page_clientsub where page_url='$filename' and customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $clientPageList1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $clientPageList = $clientPageList1;

            return $clientPageList;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Client Page Data Function Ends -----------------------------------//	
}

class menuNavigationPage extends adminFunction {

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function getPropertyUrls($customerID) {
        $this->custID = $customerID;

        $sql = "select propertyURL from propertyTable where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $PropUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $PropUrls = $PropUrls1;

            return $PropUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
    //------------------------------------ Get Static Page URLs Function Starts -----------------------------------//


    function getStaticPageUrls($customerID) {
        $this->custID = $customerID;

        $sql = "select pageUrl from tbl_static_pages where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $staticPageUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $staticPageUrls = $staticPageUrls1;

            return $staticPageUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Static Page URLs Function Ends -----------------------------------//	
    //------------------------------------ Get City URLs Function Starts -----------------------------------//


    function getCityUrls($customerID) {
        $this->custID = $customerID;

        $sql = "select cityUrl from city_url where customerID='$customerID'";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $cityUrls1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $cityUrls = $cityUrls1;

            return $cityUrls;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get City URLs Function Ends -----------------------------------//	
    //------------------------------------ Get Static Pages Listing Function Starts -----------------------------------//

    function displayAddMenuTable($customerID) {
        $this->custID = $customerID;
        ?>
        <form action="update_header.php" method="post">
            <div class="col-md-12">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box yellow-soft">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Add More Menu</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <br><br>
                        <div class="form-group">
                            <label class="control-label col-md-3">Primary Menu Name :
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="primary_menu" placeholder="e.g. Home" />
                                <span class="help-block">Enter the desired name of the menu </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <font style="font-size:15px;font-weight:400;"> Select a link for the Menu</font>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <!-- BEGIN Portlet PORTLET-->
                                <div class="portlet box yellow-casablanca">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Property Url </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:200px">
        <?php
        $propUrlListings = $this->getPropertyUrls($customerID);

        foreach ($propUrlListings as $propUrlsData) {
            ?>
                                                <?php
                                                if ($propUrlsData->propertyURL != null) {
                                                    ?>
                                                    <input type="checkbox" name="prim_url" class="checkbox-custom" value="<?php echo $propUrlsData->propertyURL; ?>"> <font style="color:#191970;"><?php echo $propUrlsData->propertyURL; ?>.html </font><br><br>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Portlet PORTLET-->
                            </div>

                            <div class="col-md-4">
                                <!-- BEGIN Portlet PORTLET-->
                                <div class="portlet box blue-steel">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Static Pages Url </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:200px">
        <?php
        $staticUrlListings = $this->getStaticPageUrls($customerID);

        foreach ($staticUrlListings as $staticUrlData) {
            ?>
                                                <?php
                                                if ($staticUrlData->pageUrl != null) {
                                                    ?>
                                                    <input type="checkbox" name="prim_url" value="<?php echo $staticUrlData->pageUrl; ?>"> <font style="color:#191970;"><?php echo $staticUrlData->pageUrl; ?>.html </font><br><br>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Portlet PORTLET-->
                            </div>

                            <div class="col-md-4">
                                <!-- BEGIN Portlet PORTLET-->
                                <div class="portlet box yellow-crusta">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>City Url </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="javascript:;" class="reload"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:200px">
        <?php
        $cityUrlListings = $this->getCityUrls($customerID);

        foreach ($cityUrlListings as $cityUrlData) {
            ?>

                                                <?php
                                                if ($cityUrlData->cityUrl != null) {
                                                    ?>
                                                    <input type="checkbox" name="prim_url" id="lable1" value="<?php echo $cityUrlData->cityUrl; ?>"> <label for="lable1"><?php echo $cityUrlData->cityUrl; ?>.html </label><br><br>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Portlet PORTLET-->
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="javascript:;" class="btn default button-previous">
                                    <i class="fa fa-angle-left"></i> Back 
                                </a>

                                <input type="submit" class="btn red-mint button-submit" name="prim_submit" value="Add Menu" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>
        </form>
        <?php
    }

    //------------------------------------ Get Static Pages Listing Function Ends -----------------------------------//	
}

class analyticsPage extends adminFunction {

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function getMetaTagsData($customerID) {
        $this->custID = $customerID;

        $sql = "select * from metaTags where customerID='$customerID' order by metano desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $metaTags1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $metaTags = $metaTags1;

            return $metaTags;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
}

class discountOffers extends adminFunction {

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function getDiscounts($customerID) {
        $this->custID = $customerID;

        $sql = "select * from tbl_discount where customerID='$customerID' order by discountID desc";

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $discountData1 = $exQuey->fetchAll(PDO::FETCH_OBJ);
            $DB = null;

            $discountData = $discountData1;

            return $discountData;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
}
?>




<?php

class modifyTable {

    var $tableName;
    var $condition;
    var $updateColumn;

    //------------------------------------ Get Property URLs Function Starts -----------------------------------//


    function deleteRecord($table, $cond) {
        $this->tableName = $table;
        $this->condition = $cond;

        $sql = "delete from " . $table . " where " . $cond;

        try {
            $DB = new connectDB();
            $DB = $DB->connect();

            $exQuey = $DB->query($sql);
            $DB = null;

            return true;
        } catch (PDOExecption $e) {
            echo "{'error':'{'text':'.$e->getMessage().'}'";
        }
    }

    //------------------------------------ Get Property URLs Function Ends -----------------------------------//	
}
?>
	