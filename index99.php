<?php
	require_once("include/class/websiteClasses.php");
?> 
<?php
	
	$useragent=$_SERVER['HTTP_USER_AGENT'];

	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|Apple-iPhone|iPhone|iPad|Apple-IpadAir|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		{	
			include_once("mobile-script/mobile-index.php");
			exit;
		}
	else
		{	
?>
			<?php
				$homeCont = new indexData('index');
				$indexdata = $homeCont -> getHomeContent('en','home','2');
			?>
			
			<section class="background_white slider">
				<div class="w3-content w3-section" style="margin:0px ! important;overflow: hidden;max-height:500px !important;">
					<?php
						$homeslidedata1 = $homeCont -> getHomeSlider('home','2');
						foreach($homeslidedata1 as $homeslidedata)
							{
					?>
								<div class= " mySlides">
									<div class="slider_text" style="position:absolute; z-index:510;" class="">
										 <div class="sldrTxt" >
											<?php echo html_entity_decode($homeslidedata->hshead1); ?><br>
											<?php echo html_entity_decode($homeslidedata->hshead2); ?>
										</div>
									</div>
									
									<img class=" slider_image img-responsive" src="http://res.cloudinary.com/the-perch/image/upload/h_550,w_1500,c_fill/reputize/homepage-slider/<?php echo html_entity_decode($homeslidedata->hsimg); ?>.jpg" style="width:100%; " alt="<?php echo html_entity_decode($homeslidedata->hshead1); ?>" />
								</div>
					<?php
							}
					?>
				</div>
				<!--<div class="baaner">
					<center>
						<div class="bottomLbl">Welcome to Baikunth</div>
					</center> 
				</div>-->
			</section>
			
			<br><br><br>
		
		
			<center>
				<div class="head"><h1 class="head"><?php echo html_entity_decode($indexdata->h1tag); ?></h1></div>
				
				<div class="botm">
					<div class="RCircle flot_lft"></div>
					<hr class="hedingHr flot_lft">
					<div class="RCircle flot_rt"></div>
				</div>
			</center>
		
			<div class="msgSectn">
				<div class="fram_width">
					<p style="font-family: ;"><?php echo html_entity_decode($indexdata->H1Content); ?></p>
				</div>
			</div>
	
		
			<section class="cardFxCntr">
				<div class="fram_width setComn">
					<div class='cardFx'>
						<div class="flipr">
							<div class="circle_icon theme_bg_color"> <i class="fa fa-thumbs-up fa_line_height"></i> </div>
							<div class="cardFxTxt">
								<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($indexdata->subhead1); ?></p>
								<p class="fa_card_text"><?php echo html_entity_decode($indexdata->subheadcontent1); ?></p>
							</div>
						</div>
					</div>

					<div class='cardFx'>
						<div class="flipr">
							<div class="circle_icon theme_bg_color"> <i class="fa fa-map-marker fa_line_height"></i> </div>
							<div class="cardFxTxt">
								<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($indexdata->subhead2); ?></p>
								<p class="fa_card_text"><?php echo html_entity_decode($indexdata->subheadcontent2); ?></p>
							</div>
						</div>
					</div>
					
					<div class='cardFx'>
						<div class="flipr">
							<div class="circle_icon theme_bg_color"> <i class="fa fa-cutlery  fa_line_height"></i> </div>
							<div class="cardFxTxt">
								<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($indexdata->subhead3); ?></p>
								<p class="fa_card_text"><?php echo html_entity_decode($indexdata->subheadcontent3); ?></p>
							</div>
						</div>
					</div>	
					<div class='cardFx'>
						<div class="flipr">
							<div class="circle_icon theme_bg_color"> <i class="fa fa-building-o fa_line_height"></i> </div>
							<div class="cardFxTxt">
								<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($indexdata->subhead4); ?></p>
								<p class="fa_card_text"><?php echo html_entity_decode($indexdata->subheadcontent4); ?></p>
							</div>
						</div>
					</div>					
				</div>
			</section>
			<br><br><br>
	
			<section class="heading" id="animateStrt"><br>
				<center>
					<div class="head"><h1 class="head"><?php echo html_entity_decode($indexdata->splheadmain); ?></h1></div>
					
					<div class="botm">
						<div class="RCircle flot_lft"></div>
						<hr class="hedingHr flot_lft">
						<div class="RCircle flot_rt"></div>
					</div>
				</center>
				<br>
			</section>
		
			<section class="fram_width proplistsec clear">
				<div class="propContnr">
					<div class="prop">
						<center><img src="images/<?php echo html_entity_decode($indexdata->image2); ?>"/></center>
						<div class="textHding clear">
							<center>
								<div style="margin-top: 10px;">
									<div class="vrLin"></div>
									<p class="hdng"><?php echo html_entity_decode($indexdata->splheadcorporate); ?></p>
								</div>
								
								<p class="para"><?php echo html_entity_decode($indexdata->splheadcorporatecontent); ?></p>
								<a class="them_btn theme_bg_color pull-right propBtn" href="<?php echo html_entity_decode($indexdata->spllinkcorporate); ?>.html"><span><i class="fa fa-bolt" style="text-align:left;"></i>&nbsp;visit</span></a>	
							</center>
						</div>
					</div>
				</div>
			
				<div class="propContnr">
					<div class="prop">
						<center><img src="images/<?php echo html_entity_decode($indexdata->image3); ?>"/></center>
						<div class="textHding clear">
							<center>
								<div style="margin-top: 10px;">
									<div class="vrLin"></div>
									<p class="hdng"><?php echo html_entity_decode($indexdata->splheadjp); ?></p>
								</div>
								<p class="para"><?php echo html_entity_decode($indexdata->splheadjpcontent); ?></p>
								<a class="them_btn theme_bg_color pull-right propBtn" href="<?php echo html_entity_decode($indexdata->spllinkjp); ?>.html"><span><i class="fa fa-bolt" style="text-align:left;"></i>&nbsp;visit</span></a>						
							</center>
						</div>
					</div>
				</div>
			
				<!--<div class="propContnr" id="prop1">
					<div class="prop">
						<center><img src="images/<?php //echo $indexdata['image4']; ?>"/></center>
						<div class="textHding">
							<center>
								<div style="margin-top: 10px;">
									<div class="vrLin"></div>
									<p class="hdng"><?php //echo $indexdata['splheadgoa']; ?></p>
								</div>
								<p class="para"><?php //echo $indexdata['splheadgoacontent']; ?></p>
								<a class="them_btn theme_bg_color pull-right propBtn" href="<?php //echo $indexdata['spllinkgoa']; ?>.html"><span><i class="fa fa-bolt" style="text-align:left;"></i>&nbsp;visit</span></a>
							</center>
						</div>
					</div>
				</div>	-->
			</section>
	
			<script>
				onece=0;
				$(window).scroll(function() {
					var scroll = $(window).scrollTop();
					var getElmy = $('#animateStrt').offset();
					 scrlMatch = getElmy.top;
					  
				   if (scroll > scrlMatch-400){
					
				   if(onece==0){
						$(".prop1").fadeIn();
						$(".prop1").animate({left: "850px"},1000,'swing');
						onece++;
						}
						 
				   }
					
				});	
			</script>
		
			<br><br>
			<hr>
	
			<center>
				<div class="head"><h2 class="head"><?php echo html_entity_decode($indexdata->aptgalleryhead); ?></h2></div>
			
				<p><?php echo html_entity_decode($indexdata->aptgalleryheadcontent); ?></p>
			
				<div class="botm">
					<div class="RCircle flot_lft"></div>
					<hr class="hedingHr flot_lft">
					<div class="RCircle flot_rt"></div>
				</div>
			</center>
			<br><br>
		
			<section class="fram_width clear">
				<?php
					$featurePropDat1 = $homeCont -> getFeatureProperty('Y','2','order by propertyID desc limit 4');
					foreach($featurePropDat1 as $featurePropDat)
						{
							$propid = $featurePropDat->propertyID;
							$faeturePropImg1 = $homeCont -> getPropertyImage($propid ,'property','0','order by imagesID desc limit 1');
							$faeturePropImg = $faeturePropImg1[0];
				?>
							<figure class="mrProp">
								<img src="http://res.cloudinary.com/the-perch/image/upload/w_564,h_400,c_fill/reputize/property/<?php echo $faeturePropImg->imageURL; ?>.jpg" class="img img-responsive" alt="<?php echo $faeturePropImg->imageAlt; ?>" title="<?php echo $faeturePropImg->imageAlt; ?>" />
								
								<div class="mrPropOrley"></div>
								<div class="mrPropOrleyHovr"></div>
								<h3 class="titleHead h3Txt them_bdr_clor"><?php echo strip_tags(trim(ucfirst(substr($featurePropDat->propertyName, 0, 22)))); ?>...</h3>
								
								<div class="imgpara" style="font-size:14px !important;"><?php echo html_entity_decode(strip_tags(trim(ucfirst(substr($featurePropDat->propertyInfo, 0, 322))))); ?>......</div>
								
								<a class="them_btn theme_bg_color pull-right prop2Btn" href="<?php echo $featurePropDat->propertyURL; ?>.html"><span><i class="fa fa-bolt"></i>&nbsp;&nbsp;Vist this</span></a>
							</figure>
				<?php
						}
				?>
			</section>
			
		
			<script>
				$('.mrProp').mouseenter(function(){
					$('.imgpara').css({'transition':'0.8s', '-webkit-transition-delay': '0.4s'}); 
					$('.mrProp .them_btn').css({'transition':'0.8s', '-webkit-transition-delay': '0.4s'}); 
					$('.pric').css({'transition':'0.9s'}); 
				});
			</script>	
		
			<script>
				$('.mrProp').mouseleave(function(){
					$('.imgpara').css({'transition':'0.2s'}); 
					$('.pric').css({'transition':'0.35s'}); 
					$('.mrProp .them_btn').css({'transition':'0.2s'});
				});
			</script>
			<br><br>
		  
			<script>
				var myIndex = 0;
				carousel();
				var numItems = $('.mySlides');
				var itemsLnth = $('.mySlides').length;
				function carousel() 
					{  
						$('.mySlides').each(function()
							{
							  $(this).fadeOut();
							});
						myIndex++;
						
						if (myIndex > itemsLnth) {myIndex = 1}
						<!-- alert(myIndex-1); -->
						$('.mySlides').eq(myIndex-1).fadeIn(2000);
						setTimeout(carousel, 9000);
					}
			</script>
			
			<hr>
			<section class="reviv clear fram_width" id="review">
				<div class="wrapq1">
					<div class="staticTxt">
						<p style="color:#d7930f;font-size:25px;">Testimonials</p>
						<h2 class="rHead"><?php echo html_entity_decode($indexdata->reviewhead); ?></h2>
					</div>
					
					<center>
						<div class="botm">
							<div class="RCircle flot_lft">
							</div>
							<hr class="hedingHr flot_lft">
							<div class="RCircle flot_rt"></div>
						</div>
					</center>
					
					<section class="animate" >
						 <div class= " mySlides2">
							<p class="quote"><i class="fa fa-quote-left ">&nbsp;</i>&nbsp; Very Homely Place with Helpful and Friendly Staff  <br>
							&nbsp;<i class="fa fa-quote-right "></i></p>
							<div class="userPic" style="background:url(images/user1.jpg);background-position:center center;background-size: 200px 200px;background-repeat: no-repeat;"></div>
							<p class="testUname">Mr. Vijay Divecha</p>
							<i class="fa fa-circle" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
						 </div>
						 
						 <div class= "mySlides2 ">
							<p class="quote"><i class="fa fa-quote-left ">&nbsp;</i>&nbsp; Excellent.Far exceeded our expectations!!! &nbsp;<i class="fa fa-quote-right "></i></p>
							<div class="userPic" style="background:url(images/user2.jpg);background-position:center center;background-size: 200px 200px;background-repeat: no-repeat;"></div>
							<p class="testUname">Mr. Amit Mahajan</p>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>					
							<i class="fa fa-circle-o" aria-hidden="true"></i>					
						 </div>
						 
						 <div class= " mySlides2">
							<p class="quote"><i class="fa fa-quote-left ">&nbsp;</i>&nbsp; Really nice place to stay!!!  &nbsp;<i class="fa fa-quote-right "></i></p>
							<div class="userPic" style="background:url(images/user3.jpg);background-position:center center;background-size: 200px 200px;background-repeat: no-repeat;"></div>
							<p class="testUname">Mr. Sayed J</p>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
						 </div>
						 
						 <div class= " mySlides2">
							<p class="quote"><i class="fa fa-quote-left ">&nbsp;</i>&nbsp; Very good Service Apartments  &nbsp;<i class="fa fa-quote-right "></i></p>
							<div class="userPic" style="background:url(images/user2.jpg);background-position:center center;background-size: 200px 200px;background-repeat: no-repeat;"></div>
							<p class="testUname">Ms. Anna Streiter</p>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle-o" aria-hidden="true"></i>
							<i class="fa fa-circle" aria-hidden="true"></i>
						 </div>
					</section>
				</div>
			</section>
			<br>
			<script>
				var myIndex2 = 0;
				TestimonialsAnimate();
				var itemsLnth2 = $('.mySlides2').length;
				function TestimonialsAnimate() {  
					$('.mySlides2').each(function()
					{
						  $(this).fadeOut();
					});
					myIndex2++;
					
					if (myIndex2 > itemsLnth2) {myIndex2 = 1}
					
					$('.mySlides2').eq(myIndex2-1).fadeIn(1000);
					 setTimeout(TestimonialsAnimate, 3000);
				}
			</script>
			<?php
				desktopFooter();
			?>
					
<?php
		}
?>