<?php
	include "include/inc_func.php";
	checkUser();
	
	@extract($_REQUEST);
?>

<?php
	if(isset($_POST['user_det']))
		{
			$name = $_POST['user_name'];
			$email = $_POST['user_email'];
			$user_id = $_POST['user_id'];
			$pass = $_POST['user_pass'];
			$type = "admin";
			$date = date('Y-m-d');
			$time = date("h:i:sa");
			
			// echo $name."<br>".$email."<br>".$user_id."<br>".$pass."<br>".$type."<br>".$date;
			// die;
			$addDemoUserCont = new superAdminMain();
			$insertDemoUser = $addDemoUserCont -> addDemoUser($user_id,$pass,$type,$name,$email,$date);
			$mailCredential = $addDemoUserCont -> sendCredentialMail($email,$user_id,$pass);
			
			Header("location:add-demo-user.php");
			exit;
		//----------------------------------------------------- Mail User ID and Password Ends --------------------------------------------//
		}
?>
<html>
	<head>
		<?php
			styleSheets();
		?>
	</head>
	
	<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
		<?php top_header(); ?>
		
		<div class="clearfix"> </div>
		
		<div class="page-container">
			<?php side_menu(); ?>
			
			<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Demo Users
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
					</div>
					<ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Demo Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add Demo User</span>
                        </li>
                    </ul>
					
					<div class="row">
                        <div class="col-md-12">
							<div class="portlet box blue-hoki">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-gift"></i>Form Actions On Top </div>
									<div class="tools">
										<a href="javascript:;" class="collapse"> </a>
										<a href="#portlet-config" data-toggle="modal" class="config"> </a>
										<a href="javascript:;" class="reload"> </a>
										<a href="javascript:;" class="remove"> </a>
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form action="" method="post" class="form-horizontal">
										<div class="form-body">
											<div class="form-group">
												<label class="col-md-3 control-label">Name</label>
												<div class="col-md-4">
													<input type="text" name="user_name" class="form-control" placeholder="Enter text">
													<span class="help-block"> Demo User Name Here.. </span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Email Address</label>
												<div class="col-md-4">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-envelope"></i>
														</span>
														<input type="email" name="user_email" class="form-control" placeholder="Email Address"> 
													</div>
													<span class="help-block"> Demo User Email Id Here.. </span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">User ID & Password</label>
												<div class="col-md-4">
													<div class="input-group">
														<button type="button" class="btn green" id="create_user" onclick="generateCredential();"><i class="fa fa-plus"></i> Create User ID & Password</button>
													</div>
												</div>
											</div>
											
											<div id="hide_wrap" style="display:none;">
												<div class="form-group">
													<label class="col-md-3 control-label">User ID </label>
													<div class="col-md-4">
														<div class="input-group">
															<input id="user_post" name="user_id" type="text" class="form-control" value="" style="color:#d87c07;font-weight:bold;border:none;" readonly="">
														</div>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-3 control-label">Password</label>
													<div class="col-md-4">
														<div class="input-group">
															<input id="pass_post" name="user_pass" type="text" class="form-control" value="" style="color:#0087ff;font-weight:bold;border:none;" readonly="">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-actions top">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" name="user_det" class="btn green">Submit</button>
													<a href="home.php"><button type="button" class="btn default">Cancel</button></a>
												</div>
											</div>
										</div>
									</form>
									<!-- END FORM-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<script>
	function generateCredential()
	{
		$.ajax({ url: 'ajax.php',
		 data: {gen_user:'Deepanshu'},
		 type: 'post',
		 success: function(output) {
					var re_data = output.split("~");
					$('#user_post').val(re_data[0]);
					$('#pass_post').val(re_data[1]);
					$('#hide_wrap').fadeIn(1000);
				  }
			});
		
	}
</script>

<?php
	scrip();
?>