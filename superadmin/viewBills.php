<?php
	include "include/inc_func.php";
	checkUser();
	
	@extract($_REQUEST);
?>
<?php
	$userDetCont = new superAdminMain();
	$getUserDet = $userDetCont -> getUserDet($userId);
	
	$custID = $getUserDet->slno;
?>
<html>
	<head>
		<?php
			styleSheets();
		?>
	</head>
	
	<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
		<?php top_header(); ?>
		
		<div class="clearfix"> </div>
		
		<div class="page-container">
			<?php side_menu(); ?>
			
			<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Users
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
					</div>
					<ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage User</span>
                        </li>
                    </ul>
					
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Manage CMS User</span>
                                    </div>
                                </div>
								<div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="Cancel">Cancel</option>
                                                <option value="Cancel">Hold</option>
                                                <option value="Cancel">On Hold</option>
                                                <option value="Close">Close</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%">
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                            <span></span>
                                                        </label>
                                                    </th>
                                                    <th width="5%"> Sl. No. </th>
                                                    <th width="15%"> Date </th>
                                                    <th width="200"> Hotel Name </th>
                                                    <th width="15%"> User ID </th>
                                                    <th width="25%"> Bill </th>
                                                    <th width="10%"> Actions </th>
                                                </tr>
												<tr role="row" class="heading">
                                                    <th width="2%" style="background:#dde5f9;"></th>
                                                    <th width="5%" style="background:#dde5f9;"></th>
                                                    <th width="15%" style="background:#dde5f9;"></th>
                                                    <th width="200" style="background:#dde5f9;"></th>
                                                    <th width="10%" style="background:#dde5f9;"></th>
                                                    <th width="10%" style="background:#dde5f9;"></th>
                                                    <th width="10%" style="background:#dde5f9;"></th>
                                                    <th width="10%" style="background:#dde5f9;"></th>
                                                    <th width="10%" style="background:#dde5f9;"></th>
                                                </tr>
                                            </thead>
                                            
											<tbody> 
												<?php
													$count = 1;
													$billscont = $userDetCont -> fetchBill($custID);
													foreach($billscont as $billData)
														{
												?>
															<tr role="row" class="odd">
																<td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="1"><span></span></label></td>
																<td><?php echo $count; ?></td>
																<td><?php echo $billData->date; ?></td>
																<td><?php echo $getUserDet->hotel_name; ?></td>
																<td><?php echo $getUserDet->user_id; ?></td>
																<td><?php echo $billData->bill; ?></td>
																<td><a href="bills/<?php echo $billData->bill; ?>" target="_blank" class="btn btn-sm btn-outline grey-salsa"><i class="fa fa-search"></i> View</a></td>
															</tr>
												<?php
															$count++;
														}
												?>
											</tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

<?php
	scrip();
?>
