<?php

include_once("../include/class/superadminClasses.php");
//checkUser();

@extract($_REQUEST);

//---------------------------------Cron Mail Function --------------------------------------------->

$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getAllCmsUserData();
// print_r($getUserDet);
$currentDate = date("Y-m-d");
date('Y-m-d', strtotime("+30 days"));


foreach ($getUserDet as $res) {
    $customerID = $res->slno;
    $user_id = $res->user_id;
    $instalmentType = $res->instalment_type;
    $instalmentCost = $res->Instalmentcost;
    $live_date = $res->live_date;
    $hotel_name = $res->hotel_name;
    $phone = $res->phone;
    $email = $res->email;
    $addOnServiceCost = $res->addOnServiceCost;
    $setUpCost = $res->setUpCost;
    $status = 'live';


    $OldDate = new DateTime($live_date);
    $now = new DateTime(Date('Y-m-d'));
    // print_r($OldDate->diff($now));
    $data = $OldDate->diff($now);
    $days = $data->days; // 365
    /* echo $instalmentType . '=', $days . '<br/>';
      $start = new DateTime($res->live_date, new DateTimeZone("Asia/Kolkata"));
      $end = clone $start;
      $end->modify('+1 month');
      while (($start->format('m') + 1) % 12 != $end->format('m') % 12) {
      $end->modify('-1 day');
      }
      $to_date_nextBill = $end->format('Y-m-d');
      $live_date = $to_date_nextBill;
      $from_date = date('Y-m-d', strtotime("+5 days"));
      echo 'from Date =' . $from_date . '- to date' . $to_date_nextBill . $hotel_name . '<br/>';
     * 
     */

    //--------------------------------Instalment Type Start------------------------------------------------>


    if ($instalmentType == 'Monthly') {   ///       Monthly packages  mail sent to be 5 days before
        if ($days == '25') {
            //echo $days;          
            // // calculate next bill cycle
            $start = new DateTime($res->live_date, new DateTimeZone("Asia/Kolkata"));
            $end = clone $start;
            $end->modify('+1 month');
            while (($start->format('m') + 1) % 12 != $end->format('m') % 12) {
                $end->modify('-1 day');
            }
            $to_date_nextBill = $end->format('Y-m-d');
            $liveDate = $to_date_nextBill;
            $from_date = $liveDate;
            $todate_start = new DateTime($from_date, new DateTimeZone("Asia/Kolkata"));
            $todate_end = clone $todate_start;
            $todate_end->modify('+1 month');
            while (($todate_start->format('m') + 1) % 12 != $todate_end->format('m') % 12) {
                $todate_end->modify('-1 day');
            }
            $to_date_nextBill_2 = $todate_end->format('Y-m-d');
            // Update live date
            $upd_date = $userDetCont->updCustomerLiveDate($user_id, $status, $from_date);
            // insert value payment table
            $insert = $userDetCont->InsertPaymentNextBill($hotel_name, $user_id, $from_date, $to_date_nextBill_2, $instalmentCost);

            //-------------- mail sent start-----------------

            $subject = 'Expiry Reminder Email';
            $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";

            $to = 'Finance@theperch.in';
            // $cc = $r['mail_cc'];
            $from = 'tech@theperch.in';

            $url = 'https://api.sendgrid.com/';
            $user = 'kunalsingh2000';
            $pass = 'P@ssw0rd';

            $json_string = array('to' => array($to), 'category' => 'Remainder');

            $params = array(
                'api_user' => 'kunalsingh2000',
                'api_key' => 'P@ssw0rd',
                'x-smtpapi' => json_encode($json_string),
                'to' => $to,
                'subject' => $subject,
                'html' => $mess,
                'replyto' => $to,
                // 'cc' => $cc,
                'fromname' => 'Super Admin',
                'text' => 'testing body',
                'from' => $from,
            );

            //echo $row[email];
            $request = $url . 'api/mail.send.json';

            // Generate curl request
            $session = curl_init($request);
            // Tell curl to use HTTP POST
            curl_setopt($session, CURLOPT_POST, true);
            // Tell curl that this is the body of the POST
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            // Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, false);
            // Tell PHP not to use SSLv3 (instead opting for TLS)
            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // obtain response
            $response = curl_exec($session);
            curl_close($session);
        } else {
            // echo $days . '<br/>';
        }
    } elseif ($instalmentType == 'Quarterly') {   ///  Quarterly packages  mail sent to be 5 days before
        if ($days == '85') {
            // echo $days;

            $start = new DateTime($res->live_date, new DateTimeZone("Asia/Kolkata"));
            $end = clone $start;
            $end->modify('+3 month');
            while (($start->format('m') + 3) % 12 != $end->format('m') % 12) {
                $end->modify('-1 day');
            }
            $to_date_nextBill = $end->format('Y-m-d');
            $liveDate = $to_date_nextBill;
            $from_date = $liveDate;

            $todate_start = new DateTime($from_date, new DateTimeZone("Asia/Kolkata"));
            $todate_end = clone $todate_start;
            $todate_end->modify('+3 month');
            while (($todate_start->format('m') + 3) % 12 != $todate_end->format('m') % 12) {
                $todate_end->modify('-1 day');
            }
            $to_date_nextBill_2 = $todate_end->format('Y-m-d');
            //  echo $instalmentType . '=', $days . '<br/>';
            // echo 'from Date =' . $from_date . '- to date' . $to_date_nextBill . $hotel_name . '<br/>';
            // Update live date
            $upd_date = $userDetCont->updCustomerLiveDate($user_id, $status, $from_date);
            // insert value payment table
            $insert = $userDetCont->InsertPaymentNextBill($hotel_name, $user_id, $from_date, $to_date_nextBill_2, $instalmentCost);
            //-------------- mail sent start-----------------

            $subject = 'Expiry Reminder Email';
            $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";

            $to = 'Finance@theperch.in';
            // $cc = $r['mail_cc'];
            $from = 'tech@theperch.in';

            $url = 'https://api.sendgrid.com/';
            $user = 'kunalsingh2000';
            $pass = 'P@ssw0rd';

            $json_string = array('to' => array($to), 'category' => 'Remainder');

            $params = array(
                'api_user' => 'kunalsingh2000',
                'api_key' => 'P@ssw0rd',
                'x-smtpapi' => json_encode($json_string),
                'to' => $to,
                'subject' => $subject,
                'html' => $mess,
                'replyto' => $to,
                // 'cc' => $cc,
                'fromname' => 'Super Admin',
                'text' => 'testing body',
                'from' => $from,
            );

            //echo $row[email];
            $request = $url . 'api/mail.send.json';

            // Generate curl request
            $session = curl_init($request);
            // Tell curl to use HTTP POST
            curl_setopt($session, CURLOPT_POST, true);
            // Tell curl that this is the body of the POST
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            // Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, false);
            // Tell PHP not to use SSLv3 (instead opting for TLS)
            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // obtain response
            $response = curl_exec($session);
            curl_close($session);
        } else {
            // echo $days . '<br/>';
        }
    } elseif ($instalmentType == 'Yearly') {     ///  Yearly packages  mail sent to be 5 days before
        if ($days == '360') {
            // echo $days;
            $start = new DateTime($res->live_date, new DateTimeZone("Asia/Kolkata"));
            $end = clone $start;
            $end->modify('+1 year');
            while (($start->format('y') + 1) % 12 != $end->format('y') % 12) {
                $end->modify('-1 day');
            }
            $to_date_nextBill = $end->format('Y-m-d');
            $liveDate = $to_date_nextBill;
            $from_date = $liveDate;

            $todate_start = new DateTime($from_date, new DateTimeZone("Asia/Kolkata"));
            $todate_end = clone $todate_start;
            $todate_end->modify('+1 year');
            while (($todate_start->format('y') + 1) % 12 != $todate_end->format('y') % 12) {
                $todate_end->modify('-1 day');
            }
            $to_date_nextBill_2 = $todate_end->format('Y-m-d');

            // Update live date
            $upd_date = $userDetCont->updCustomerLiveDate($user_id, $status, $from_date);
            // insert value payment table
            $insert = $userDetCont->InsertPaymentNextBill($hotel_name, $user_id, $from_date, $to_date_nextBill_2, $instalmentCost);

            //-------------- mail sent start-----------------

            $subject = 'Expiry Reminder Email';
            $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";

            $to = 'balkrishn@theperch.in';
            // $cc = $r['mail_cc'];
            $from = 'tech@theperch.in';

            $url = 'https://api.sendgrid.com/';
            $user = 'kunalsingh2000';
            $pass = 'P@ssw0rd';

            $json_string = array('to' => array($to), 'category' => 'Remainder');

            $params = array(
                'api_user' => 'kunalsingh2000',
                'api_key' => 'P@ssw0rd',
                'x-smtpapi' => json_encode($json_string),
                'to' => $to,
                'subject' => $subject,
                'html' => $mess,
                'replyto' => $to,
                // 'cc' => $cc,
                'fromname' => 'Super Admin',
                'text' => 'testing body',
                'from' => $from,
            );

            //echo $row[email];
            $request = $url . 'api/mail.send.json';

            // Generate curl request
            $session = curl_init($request);
            // Tell curl to use HTTP POST
            curl_setopt($session, CURLOPT_POST, true);
            // Tell curl that this is the body of the POST
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            // Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, false);
            // Tell PHP not to use SSLv3 (instead opting for TLS)
            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // obtain response
            $response = curl_exec($session);
            curl_close($session);
        } else {
            // echo $days . '<br/>';
        }
    } elseif ($instalmentType == 'Half Yearly') {   /// Half Yearly packages  mail sent to be 5 days before
        if ($days == '175') {
            // echo $days;
            $start = new DateTime($res->live_date, new DateTimeZone("Asia/Kolkata"));
            $end = clone $start;
            $end->modify('+6 month');
            while (($start->format('m') + 6) % 12 != $end->format('m') % 12) {
                $end->modify('-1 day');
            }
            $to_date_nextBill = $end->format('Y-m-d');
            $liveDate = $to_date_nextBill;
            $from_date = $liveDate;

            $todate_start = new DateTime($from_date, new DateTimeZone("Asia/Kolkata"));
            $todate_end = clone $todate_start;
            $todate_end->modify('+1 month');
            while (($todate_start->format('m') + 1) % 12 != $todate_end->format('m') % 12) {
                $todate_end->modify('-1 day');
            }
            $to_date_nextBill_2 = $todate_end->format('Y-m-d');

            // Update live date
            $upd_date = $userDetCont->updCustomerLiveDate($user_id, $status, $from_date);
            // insert value payment table
            $insert = $userDetCont->InsertPaymentNextBill($hotel_name, $user_id, $from_date, $to_date_nextBill_2, $instalmentCost);

            //-------------- mail sent start-----------------

            $subject = 'Expiry Reminder Email';
            $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";

            $to = 'Finance@theperch.in';
            // $cc = $r['mail_cc'];
            $from = 'tech@theperch.in';

            $url = 'https://api.sendgrid.com/';
            $user = 'kunalsingh2000';
            $pass = 'P@ssw0rd';

            $json_string = array('to' => array($to), 'category' => 'Remainder');

            $params = array(
                'api_user' => 'kunalsingh2000',
                'api_key' => 'P@ssw0rd',
                'x-smtpapi' => json_encode($json_string),
                'to' => $to,
                'subject' => $subject,
                'html' => $mess,
                'replyto' => $to,
                // 'cc' => $cc,
                'fromname' => 'Super Admin',
                'text' => 'testing body',
                'from' => $from,
            );

            //echo $row[email];
            $request = $url . 'api/mail.send.json';

            // Generate curl request
            $session = curl_init($request);
            // Tell curl to use HTTP POST
            curl_setopt($session, CURLOPT_POST, true);
            // Tell curl that this is the body of the POST
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            // Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, false);
            // Tell PHP not to use SSLv3 (instead opting for TLS)
            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // obtain response
            $response = curl_exec($session);
            curl_close($session);
        } else {
            // echo $days . '<br/>';
        }
    }
}
?>