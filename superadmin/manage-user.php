<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>
<?php
if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];

    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance" || $userTyp == 'tech' || $userTyp == 'admin') {
        if ($cliend == 'Approved') {
            $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' ||  status='live' || status='cont_change' || status='desg_change' || status='approved')";
        } else {
            $statToBeUpdated1 = "(status='pending')";
        }
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified')";
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    }

    //------------------------- Status Update Options Ends ------------------------//
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
        <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Users
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage User</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Manage CMS User</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="Cancel">Cancel</option>
                                                <option value="Cancel">Hold</option>
                                                <option value="Cancel">On Hold</option>
                                                <option value="Close">Close</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th width="2%">Sl No.</th>
                                <th width="10%">Date</th>
                                <th width="10%">Instalment</th>
                                <th width="10%">Next Bill Cycle </th>
                                <th width="10%">Hotel Name</th>
                                <th width="10%">Admin Name</th>
                                <th width="10%">User ID</th>
                                <th width="10%"> Client Type </th>
                                <th width="10%"> Template Type </th>
                                <th width="10%"> Status </th>
                                <th width="10%"> Actions </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cmsUsersCont = new superAdminMain();
                            $cmsUsers = $cmsUsersCont->getCmsUsers($statToBeUpdated1);
                            $count = 1;
                            foreach ($cmsUsers as $userData) {
                                $custStat = $userData->status;
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $userData->date; ?></td>
                                    <td><?php echo $userData->instalment_type; ?></td>
                                    <td>
                                        <?php
                                        if ($userData->instalment_type == 'Monthly') {
                                            $start = new DateTime($userData->live_date, new DateTimeZone("Asia/Kolkata"));
                                            $end = clone $start;
                                            $end->modify('+1 month');
                                            while (($start->format('m') + 1) % 12 != $end->format('m') % 12) {
                                                $end->modify('-1 day');
                                            }
                                            $nextBill = $end->format('d-M-Y');
                                            echo '<span style="color:green">' . $nextBill . '</span><br/>';

                                            $today = new DateTime();
                                            $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
                                            $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
                                            echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/>';
                                        } elseif ($userData->instalment_type == 'Quarterly') {
                                            $start = new DateTime($userData->live_date, new DateTimeZone("Asia/Kolkata"));
                                            $end = clone $start;
                                            $end->modify('+3 month');
                                            while (($start->format('m') + 3) % 12 != $end->format('m') % 12) {
                                                $end->modify('-1 day');
                                            }
                                            $nextBill = $end->format('d-M-Y');
                                            echo '<span style="color:green">' . $nextBill . '</span><br/>';
                                            $today = new DateTime();
                                            $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
                                            $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
                                            echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/>';
                                        } elseif ($userData->instalment_type == 'Half Yearly') {
                                            $start = new DateTime($userData->live_date, new DateTimeZone("Asia/Kolkata"));
                                            $end = clone $start;
                                            $end->modify('+6 month');
                                            while (($start->format('m') + 6) % 12 != $end->format('m') % 12) {
                                                $end->modify('-1 day');
                                            }
                                            $nextBill = $end->format('d-M-Y');
                                            echo '<span style="color:green">' . $nextBill . '</span><br/>';
                                            $today = new DateTime();
                                            $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
                                            $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
                                            echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/>';
                                        } elseif ($userData->instalment_type == 'Yearly') {
                                            $start = new DateTime($userData->live_date, new DateTimeZone("Asia/Kolkata"));
                                            $end = clone $start;
                                            $end->modify('+1 year');
                                            while (($start->format('y') + 1) % 12 != $end->format('y') % 12) {
                                                $end->modify('-1 day');
                                            }
                                            $nextBill = $end->format('d-M-Y');
                                            echo '<span style="color:green">' . $nextBill . '</span><br/>';
                                            $today = new DateTime();
                                            $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
                                            $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
                                            echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/>';
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $userData->hotel_name; ?></td>
                                    <td><?php echo $userData->admin_name; ?></td>
                                    <td><?php echo $userData->user_id; ?></td>
                                    <td><?php echo $userData->client_type; ?></td>
                                    <td><?php echo $userData->template_type; ?></td>
                                    <td><span class="label label-sm label-success"><?php echo $customerStatArr[$custStat]; ?></span></td>
                                    <?php if ($userTyp == "finance" || $userTyp == 'tech' || $userTyp == 'admin') { ?>
                                        <td><a href = "manage-payment.php?userId=<?php echo $userData->user_id; ?>" class = "btn btn-sm btn-outline blue"><i class = "fa fa-plus"></i> Add Payment</a></td>
                                        <?php
                                    }
                                    ?>
                                    <td><a href="viewUser.php?userId=<?php echo $userData->user_id; ?>" class="btn btn-sm btn-outline grey-salsa"><i class="fa fa-search"></i> View</a></td>

                                </tr>

                                <?php
                                $count++;
                            }
                            ?>

                        </tbody>         
                    </table>
                </div>
            </div>

        </div>
    </body>
</html>


<?php
scrip();
?>





<!--<tr role="row" class="filter">
        <td> </td>
        <td>
                <input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
        <td>
                <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                        <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                        <span class="input-group-btn">
                                <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-calendar"></i>
                                </button>
                        </span>
                </div>
                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                        <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                        <span class="input-group-btn">
                                <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-calendar"></i>
                                </button>
                        </span>
                </div>
        </td>
        <td>
                <input type="text" class="form-control form-filter input-sm" name="order_customer_name"> </td>
        <td>
                <input type="text" class="form-control form-filter input-sm" name="order_ship_to"> </td>
        <td>
                <div class="margin-bottom-5">
                        <input type="text" class="form-control form-filter input-sm" name="order_price_from" placeholder="From" /> </div>
                <input type="text" class="form-control form-filter input-sm" name="order_price_to" placeholder="To" /> </td>
        <td>
                <div class="margin-bottom-5">
                        <input type="text" class="form-control form-filter input-sm margin-bottom-5 clearfix" name="order_quantity_from" placeholder="From" /> </div>
                <input type="text" class="form-control form-filter input-sm" name="order_quantity_to" placeholder="To" /> </td>
        <td>
                <select name="order_status" class="form-control form-filter input-sm">
                        <option value="">Select...</option>
                        <option value="pending">Pending</option>
                        <option value="closed">Closed</option>
                        <option value="hold">On Hold</option>
                        <option value="fraud">Fraud</option>
                </select>
        </td>
        <td>
                <div class="margin-bottom-5">
                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                <i class="fa fa-search"></i> Search</button>
                </div>
                <button class="btn btn-sm red btn-outline filter-cancel">
                        <i class="fa fa-times"></i> Reset</button>
        </td>
</tr>-->