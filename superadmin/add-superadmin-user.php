<?php
include "include/inc_func.php";
$insertNewUserOb = new superAdminMain();
checkUser();

@extract($_REQUEST);
?>
<?php
if (isset($_POST['user_insert'])) {
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $insertNewUserOb = new superAdminMain();
    $insertNewUser = $insertNewUserOb->addSuperAdmin($_POST['type_name'], $_POST['type_username'], $_POST['type_password'], $_POST['type_email'], $_POST['user_type'], 'insert', $date, $time);

    echo '<script type="text/javascript">
                alert("User has been added succesfully");              
window.location = "manage-superadmin-user.php";
            </script>';
    exit;
}
if ($type == 'edit') {
    $getdata = new superAdminMain();
    $getSuperAdmin = $getdata->GetSuperadminDataWithID($userID);
    $res = $getSuperAdmin[0];
}
if (isset($_POST['user_update'])) {
    $userID;
    $insertNewUser = $insertNewUserOb->UpdateUserSupperAdmin($userID);
    echo '<script type="text/javascript">
                alert("User has been Updated succesfully");              
window.location = "manage-superadmin-user.php";
            </script>';
    exit;
}
if ($type == 'delete') {
    $userID;
    $insertNewUserOb->deleteUserData($userID);
    header("location:manage-superadmin-user.php");
    exit;
}
?>
<html lang="en"> 
    <head>
        <?php
        styleSheets();
        ?>
    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">

            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Superadmin Users
                                <small>Add new Superadmin Users and their details</small>
                            </h1>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Superadmin Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add Superadmin User</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" class="form-horizontal">
                                        <div class="form-body">				
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="type_name" value="<?= $res->name; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                                    <span class="help-block"> Provide Name of User</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Username (use as login id)
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="type_username" value="<?= $res->userId; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                                    <span class="help-block"> Provide Login ID</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="password" class="form-control" name="type_password" value="<?= $res->password; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                                    <span class="help-block"> Provide User Password</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Email
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="type_email" value="<?= $res->email; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                                    <span class="help-block"> Provide User Email</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> User Type
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="radio-list">
                                                        <?php
                                                        $typ = $res->userType;
                                                        ?>

                                                        <table>
                                                            <tr>
                                                                <td>
                                                            <center><input type="radio" id="radio-1-0" name="user_type" value="admin" required <?php
                                                        if ($type == 'edit') {
                                                            if ($typ == "admin") {
                                                                echo "checked";
                                                            }
                                                        }
                                                        ?>/>
                                                                <center><span class="help-block"> Administrator</span>
                                                                    </td>

                                                                    <td style="padding-left:100px;">
                                                                    <center><input type="radio" id="radio-1-1" name="user_type" value="finance" <?php
                                                                if ($type == 'edit') {
                                                                    if ($typ == "finance") {
                                                                        echo "checked";
                                                                    }
                                                                }
                                                        ?>/>
                                                                        <center><span class="help-block"> Finance</span>
                                                                            </td>

                                                                            <td style="padding-left:100px;">
                                                                            <center><input type="radio" id="radio-1-1" name="user_type" value="content" <?php
                                                                        if ($type == 'edit') {
                                                                            if ($typ == "content") {
                                                                                echo "checked";
                                                                            }
                                                                        }
                                                        ?>/>
                                                                                <center><span class="help-block"> Content</span>
                                                                                    </td>

                                                                                    <td style="padding-left:100px;">
                                                                                    <center><input type="radio" id="radio-1-1" name="user_type" value="sales" <?php
                                                                                if ($type == 'edit') {
                                                                                    if ($typ == "sales") {
                                                                                        echo "checked";
                                                                                    }
                                                                                }
                                                        ?>/>
                                                                                        <center><span class="help-block">Sales</span>
                                                                                            </td>

                                                                                            <td style="padding-left:100px;">
                                                                                            <center><input type="radio" id="radio-1-1" name="user_type" value="tech" <?php
                                                                                        if ($type == 'edit') {
                                                                                            if ($typ == "tech") {
                                                                                                echo "checked";
                                                                                            }
                                                                                        }
                                                        ?>/>
                                                                                                <center><span class="help-block">Tech</span>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                    </table>
                                                                                                    </div>
                                                                                                    <span class="required"> Please Select any one of the above to continue </span>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    <?php if ($type == 'edit') { ?>

                                                                                                        <div class="form-actions top">
                                                                                                            <div class="row">
                                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                                    <input type="hidden" name="userID" value="<?php echo $res->slno; ?>"/>
                                                                                                                    <button type="submit" name="user_update" class="btn green">Update</button>
                                                                                                                    <a href="home.php"><button type="button" class="btn default">Cancel</button></a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    <?php } else { ?>
                                                                                                        <div class="form-actions top">
                                                                                                            <div class="row">
                                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                                    <button type="submit" name="user_insert" class="btn green">Submit</button>
                                                                                                                    <a href="home.php"><button type="button" class="btn default">Cancel</button></a>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    <?php } ?>
                                                                                                    </form>
                                                                                                    <!-- END FORM-->
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </div>
                                                                                                    </body>
                                                                                                    </html>


                                                                                                    <?php
                                                                                                    scrip();
                                                                                                    ?>