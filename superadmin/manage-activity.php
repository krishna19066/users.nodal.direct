<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
$cmsUsersCont = new superAdminMain();
$get_login_activity = $cmsUsersCont->getClientdataLoginActivity();
?>
<?php
if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];

    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        if ($cliend == 'Approved') {
            $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' ||  status='live' || status='cont_change' || status='desg_change' || status='approved')";
        } else {
            $statToBeUpdated1 = "(status='pending')";
        }
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified')";
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    }

    //------------------------- Status Update Options Ends ------------------------//
    if (isset($_REQUEST['search_client'])) {
        $clientName = $_REQUEST['ClientName'];
        $get_client_changes = $cmsUsersCont->getClientAllChanges($clientName);
        $resCount = count($get_client_changes);
    }
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Client Activity
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage Activity</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Manage Client Activity</span>
                                    </div>
                                </div>
                              
                            <div class="portlet-body">

                                <div class="table-container">

                                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="2%">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th width="5%"> Sl. No. </th>
                                                <th width="15%">Last Login Time</th>
                                                <th width="10%"> Hotel Name </th>
                                                <th width="10%">Customer ID </th>   
                                                <th width="10%">Status</th>   
                                                <th width="10%">Action</th>

                                            </tr>
                                            <tr role="row" class="heading">
                                                <th width="2%" style="background:#dde5f9;"></th>
                                                <th width="5%" style="background:#dde5f9;"></th>
                                                <th width="15%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th> 
                                                <th width="10%" style="background:#dde5f9;"></th>                            
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            <?php
                                            $count = 1;
                                            foreach ($get_login_activity as $res) {
                                                ?>
                                                <tr role="row" class="odd">
                                                    <td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="1"><span></span></label></td>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php
                                                        $date = $res->last_login;
                                                        if ($date != null) {
                                                            $dateTime = date("jS  M, Y", strtotime($date)) . ',' . $res->lat_login_time;
                                                            echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                        } else {
                                                            echo '---';
                                                        }
                                                        $OldDate = new DateTime($date);
                                                        $now = new DateTime(Date('Y-m-d'));
                                                        // print_r($OldDate->diff($now));
                                                        $data = $OldDate->diff($now);
                                                        $days = $data->days;
                                                        $months = $data->m;
                                                        echo '<span style="color:blue;"> Total Days =' . $days . '</span><br/>';
                                                        echo '<span style="color:red;">Months =' . $months . '</span>';
                                                        ?>

                                                    </td>
                                                    <td><?php echo $res->user_id; ?> </td>                                                      
                                                    <td style="color:blue"><?php echo $res->customerID; ?>  </td>  
                                                     <td><?php
                                                        $date = $res->last_login;
                                                        if ($date != null) {
                                                            $dateTime = date("jS  M, Y", strtotime($date)) . ',' . $res->lat_login_time;
                                                           // echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                        } else {
                                                            echo '---';
                                                        }
                                                        $OldDate = new DateTime($date);
                                                        $now = new DateTime(Date('Y-m-d'));
                                                        // print_r($OldDate->diff($now));
                                                        $data = $OldDate->diff($now);
                                                        $days = $data->days;
                                                        $months = $data->m;
                                                         if($months == 0 || $months == 1 || $months ==2 || $months == 3){ ?>
                                                        <span class="label label-success">Active Client</span>
                                                        <?php } 
                                                         else if($months == 4 || $months == 5 ||$months == 6){ ?>
                                                        <span class="label label-warning">Slow Update</span>
                                                        <?php }
                                                         else if(7 <= $months){ ?>
                                                        <span class="label label-danger">Client Inactive</span>
                                                        <?php } ?>
                                                       
                                                    </td>

                                                    <td>
                                                        <!--<button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal<?php //echo $res->customerID;  ?>">View Details</button>-->
                                                        <a href="activity-details.php?customerID=<?php echo $res->customerID; ?>" class="btn btn-info">View Details</a>
                                                    </td>  
                                                    <!-- Modal -->                                        
                                                </tr>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<?php
scrip();
?>


