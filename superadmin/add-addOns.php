<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);

$addOnPost = new superAdminMain();
?>

<?php
if (isset($addOnId)) {
    $addOnPost = new superAdminMain();

    $getSelAddon1 = $addOnPost->getAddonServiceWithID($addOnId);
    $getSelAddon = $getSelAddon1[0];
}
?>

<?php
if (isset($_POST['addon_det'])) {

    $addNewService = $addOnPost->addAddonService($_POST['addon_name'], $_POST['addon_price']);
    echo '<script type="text/javascript">
                alert("Succesfuly Added Services");              
window.location = "add-addOns.php";
            </script>';
    exit();
}

if (isset($_POST['update_addon_det'])) {
    $updateService = $addOnPost->updateAddonService($_POST['addon_name'], $_POST['addon_price'], $_POST['addonId']);

    Header("location:add-addOns.php?addOnId=" . $_POST['addonId']);
    exit;
}

?>
<html lang="en"> 
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">

            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Add-On Services
                                <small>Add new Add-On Services</small>
                            </h1>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Form Actions On Top </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" class="form-horizontal">
                                        <?php
                                        if (isset($addOnId)) {
                                            ?>
                                            <input type="hidden" name="addonId" value="<?php echo $getSelAddon->id; ?>" />
                                            <?php
                                        }
                                        ?>
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Add-on Service Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="addon_name" class="form-control" value="<?php echo $getSelAddon->addOnName; ?>">
                                                    <span class="help-block"> Add-on Service Name Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Add-on Service Price</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="addon_price" class="form-control" value="<?php echo $getSelAddon->addOnPrice; ?>">
                                                    <span class="help-block"> Add-on Service Price Here.. </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions top">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if (isset($addOnId)) {
                                                        ?>
                                                        <button type="submit" name="update_addon_det" class="btn green">Update</button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button type="submit" name="addon_det" class="btn green">Submit</button>
                                                        <?php
                                                    }
                                                    ?>
                                                    <a href="home.php"><button type="button" class="btn default">Cancel</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
scrip();
?>