<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>

<?php
$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getUserDet($userId);

$org_stat = $getUserDet->status;

if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];
    if ($userTyp == "finance" || $userTyp == "content") {
        $blockStat = "Y";
    } elseif ($userTyp == "admin" || $userTyp == "sales" || $userTyp == "tech") {
        $blockStat = "N";
    }
    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        $statToBeUpdated1 = array("payment_recv");
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = array("content_recv", "content_upd", "verified");
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = array("approved", "cont_change", "desg_change");
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = array("live");
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = array("payment_recv", "content_recv", "content_upd", "verified", "approved", "cont_change", "desg_change", "live", "pending");
    }

    //------------------------- Status Update Options Ends ------------------------//
}

//------------------------ Update Status Starts -----------------------//

if (isset($_POST['upd_stat'])) {
    $statToBeupd = $_POST['stat'];
    $getUserDet = $userDetCont->updCustomerStat($userId, $statToBeupd);

    if ($statToBeupd == "payment_recv") {
        Header("location:/generatePDF/examples/generateBill.php?userId=" . $userId);
        exit;
    } else {
        Header("location:manage-user.php");
        exit;
    }
}
if (isset($_POST['submit'])) {
    $customerID = $_POST['customerID'];
    $userID = $_POST['user_id'];
    $cloudName = $_POST['cloud_name'];
    $apiKey = $_POST['api_key'];
    $apiSecret = $_POST['api_secret'];
    $updCloud = $userDetCont->updCloudData($userID, $cloudName, $apiKey, $apiSecret);
    $updCloudAdmin = $userDetCont->updCloudDataForAdmin($userID, $cloudName, $apiKey, $apiSecret);

    //------- User Folder Creation Starts ------------------------------------------------------------------//

     $dirMain = $customerID;
    
    chdir("../");
    //echo getcwd();
    //  die;
    if (file_exists($dirMain)) {
        
    } else {
        $newdir = $dirMain;

        $createnew = mkdir($newdir);
    }

//------- User Folder Creation Ends ------------------------------------------------------------------//
//------- File Transfer Starts ------------------------------------------------------------------//

    $tem_typ = $getUserDet->template_type;
   // $temp_sel = $getUserDet->template_selected;

    $fetchFilesPath = '/home/hawthorntech/public_html/stayondiscount.com/' . $tem_typ;

    $src = $fetchFilesPath;
    $dst = $dirMain;

// Function to Copy folders and files       
    function rcopy($src, $dst) {

        if (file_exists($dst))
            rmdir($dst);
        if (is_dir($src)) {

            mkdir($dst);
            $files = scandir($src);
            // print_r($files);
            //  echo $src;
            //  die();
            foreach ($files as $file)
                if ($file != "." && $file != "..")
                    rcopy("$src/$file", "$dst/$file");
        } else if (file_exists($src))
            copy($src, $dst);
        rmdir($src);
    }
    rcopy($src, $dst);  // Call function 
// rcopy($fetchFilesPath, $dirMain);

    Header("location:manage-user.php");
    exit;
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>View Details - <?php echo $getUserDet->hotel_name; ?>
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">View User Details</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">View Details - <?php echo $getUserDet->hotel_name; ?></span>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <div class="portlet light form-fit bordered">
                                    <div class="portlet-title">
                                        <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Update Status</span> &nbsp; &nbsp;

                                        <select name="stat" style="width:200px;height:35px;border:1px solid #03A9F4;color:#607D8B;font-weight:bold;">
                                            <option value="<?php echo $org_stat; ?>"><?php echo $customerStatArr[$org_stat]; ?></option>
                                            <?php
                                            foreach ($statToBeUpdated1 as $statToBeUpdated) {
                                                $value = $customerStatArr[$statToBeUpdated];
                                                ?>
                                                <option value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <input type="submit" name="upd_stat" style="height:35px;border:none;background:#00BCD4;color:white;" value="Update Status" />
                                        <?php if ($userTyp == "tech" && $getUserDet->client_type != 'Other' ) { ?>
                                            <a href="addCloudDetails.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:blue;color:white;float:right;margin-left: 33px;">Add Cloud Details</button></a>
                                        <?php } ?>
                                        <a href="viewBills.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:#FF5722;color:white;float:right;">View Bills</button></a>
                                    </div>
                                </div>
                        </div>
                        </form>

                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">
                                <!--<div class="portlet-title">
                                        <div class="caption">
                                                <i class="fa fa-gift"></i>Form Actions On Top </div>
                                        <div class="tools">
                                                <a href="javascript:;" class="collapse"> </a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                <a href="javascript:;" class="reload"> </a>
                                                <a href="javascript:;" class="remove"> </a>
                                        </div>
                                </div>-->

                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Hotel Name <span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <input type="hidden" name="user_id" class="form-control" value="<?php echo $getUserDet->user_id; ?>"/>
                                                    <input type="hidden" name="customerID" class="form-control" value="<?php echo $getUserDet->slno; ?>"/>
                                                    <input type="text" name="hotel_name" class="form-control" value="<?php echo $getUserDet->hotel_name; ?>" <?php if($getUserDet->hotel_name!=null){?>  readonly <?php } ?> />
                                                    <span class="help-block"> CMS User Hotel Name Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cloud Name <span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <input type="text" name="cloud_name" class="form-control" value="<?php echo $getUserDet->cloud_name; ?>" required=""  />
                                                    <span class="help-block"> CMS Cloud Name Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">API Key <span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <input type="text" name="api_key" class="form-control" value="<?php echo $getUserDet->api_key; ?>" required=""  />
                                                    <span class="help-block"> CMS API Key Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">API Secret
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="api_secret" class="form-control" value="<?php echo $getUserDet->api_secret; ?>" required="" />
                                                    <span class="help-block"> Provide API Secret </span>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="submit" class="btn green button-submit" name="submit" value="Submit" <?php if($getUserDet->status =='live'){ ?> disabled="" <?php }?> /> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        </div>
    </body>
</html>

<?php
scrip();
?>
