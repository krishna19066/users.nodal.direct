<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>
<?php
if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];

    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        if ($cliend == 'Approved') {
            $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' ||  status='live' || status='cont_change' || status='desg_change' || status='approved')";
        } else {
            $statToBeUpdated1 = "(status='pending')";
        }
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified')";
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    }

    //------------------------- Status Update Options Ends ------------------------//
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Users
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage User</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Manage CMS User</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="Cancel">Cancel</option>
                                                <option value="Cancel">Hold</option>
                                                <option value="Cancel">On Hold</option>
                                                <option value="Close">Close</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%">
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                            <span></span>
                                                        </label>
                                                    </th>
                                                    <th width="5%"> Sl. No. </th>
                                                    <th width="10%"> Send Date </th>
                                                    <th width="10%"> Hotel Name </th>
                                                    <th width="15%"> Email </th>
                                                    <th width="20%"> Subject </th>
                                                    <th width="20%"> Body </th>  
                                                    <th width="10%"> Action </th>

                                                </tr>
                                                <tr role="row" class="heading">
                                                    <th width="2%" style="background:#dde5f9;"></th>
                                                    <th width="5%" style="background:#dde5f9;"></th>
                                                    <th width="10%" style="background:#dde5f9;"></th>
                                                    <th width="15%" style="background:#dde5f9;"></th>
                                                    <th width="20%" style="background:#dde5f9;"></th>
                                                    <th width="20%" style="background:#dde5f9;"></th> 
                                                    <th width="10%" style="background:#dde5f9;"></th>                            
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <?php
                                                $cmsUsersCont = new superAdminMain();
                                                $cmsUsers = $cmsUsersCont->getEmailNotification();
                                                $count = 1;
                                                foreach ($cmsUsers as $userData) {
                                                    ?>
                                                    <tr role="row" class="odd">
                                                        <td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="1"><span></span></label></td>
                                                        <td><?php echo $count; ?></td>
                                                        <td><?php
                                                            $date = $userData->timestamp;
                                                            if ($date != null) {
                                                                echo date("jS  M, Y", strtotime($date));
                                                            } else {
                                                                echo '---';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><?php
                                                            $email = $userData->email;
                                                            $email_res = explode(',', $email);
                                                            ?>
                                                            <div class="panel-group">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" href="#collapse<?php echo $count; ?>">View Client Name</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse">
                                                                        <?php
                                                                        foreach ($email_res as $mail) {
                                                                            $mail;
                                                                            $hotel_info = $cmsUsersCont->getHotelInfo($mail);
                                                                            foreach ($hotel_info as $res) {
                                                                                ?>
                                                                                <div class="panel-body"><?php echo $hotelName = $res->hotel_name; ?></div>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <td style="color:blue"><?php
                                                            $email = $userData->email;
                                                            $email_res = explode(',', $email);

                                                            $i = 1;
                                                            foreach ($email_res as $mail) {
                                                                echo $mail . ',' . ' ';

                                                                if ($i++ == 3)
                                                                    break;
                                                            }
                                                            ?>

                                                        </td>
                                                        <td><?php echo $userData->subject; ?></td>
                                                        <td>

                                                            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo<?php echo $count; ?>">View Email Body</button>
                                                            <div id="demo<?php echo $count; ?>" class="collapse">
                                                                <?php echo $userData->email_body; ?>
                                                            </div>
                                                        </td>  
                                                        <td>
                                                            <button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal<?php echo $userData->id; ?>">View Details</button>

                                                        </td>  
                                                        <!-- Modal -->
                                                <div class="modal fade" id="myModal<?php echo $userData->id; ?>" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content" style="width: 850px;">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Email Notification:</h4>
                                                                <h5 style="float:right;">
                                                                    <?php
                                                                    $date = $userData->timestamp;
                                                                    if ($date != null) {
                                                                        echo date("jS  M, Y", strtotime($date));
                                                                    }
                                                                    ?>
                                                                </h5>
                                                                <h5>Sent To:</h5>
                                                                <h6 style="color:gray;"><?php
                                                                $email = $userData->email;
                                                                $email_res = explode(',', $email);
                                                                foreach ($email_res as $mail) {
                                                                    echo $mail . ',' . ' ';
                                                                }
                                                                    ?></h6>

                                                            </div>
                                                            <div class="modal-body">
                                                                <p> Subject: </p>
                                                                <p><h4 style="color:gray"><?php echo $userData->subject; ?></h4></p><hr/>
                                                                <p>Email Body:</p>
                                                                <p><?php echo $userData->email_body; ?></p>
                                                            </div>
                                                            <div class="modal-footer">                     
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>              
                                                    </div>
                                                </div>
                                                </tr>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>

<?php
scrip();
?>


