<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>

<?php
$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getUserDet($userId);

$org_stat = $getUserDet->status;

if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];
    if ($userTyp == "finance" || $userTyp == "content") {
        $blockStat = "Y";
    } elseif ($userTyp == "admin" || $userTyp == "sales" || $userTyp == "tech") {
        $blockStat = "N";
    }
    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        $statToBeUpdated1 = array("payment_recv");
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = array("content_recv", "content_upd", "verified");
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = array("approved", "cont_change", "desg_change");
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = array("live");
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = array("payment_recv", "content_recv", "content_upd", "verified", "approved", "cont_change", "desg_change", "live", "pending");
    }

    //------------------------- Status Update Options Ends ------------------------//
}

//------------------------ Update Status Starts -----------------------//

if (isset($_POST['upd_stat'])) {
    $statToBeupd = $_POST['stat'];
    $getUserDet = $userDetCont->updCustomerStat($userId, $statToBeupd);

    if ($statToBeupd == "payment_recv") {
        echo '<script type="text/javascript">
                alert("Status Updated succesfully");              
window.location = "manage-user.php";
            </script>';
        exit;
    } else {
        Header("location:manage-user.php");
        exit;
    }
}
if (isset($_POST['add_payment'])) {
    $customerID = $_POST['customerID'];
    $updCloud = $userDetCont->AddPayment($customerID);
    echo '<script type="text/javascript">
                alert("Payment has been added succesfully");              
window.location = "manage-user.php";
            </script>';
    exit;
}
if (isset($edit) == 'payment' && isset($userId) != NULL) {
    $cmsUsersCont = new superAdminMain();
    $cmsUsers = $cmsUsersCont->getPaymentDeatils($userId);
    $paymentData = $cmsUsers[0];
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>View Details - <?php echo $getUserDet->hotel_name; ?>
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">View User Details</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">View Details - <?php echo $getUserDet->hotel_name; ?></span>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <div class="portlet light form-fit bordered">
                                    <div class="portlet-title">
                                        <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Update Status</span> &nbsp; &nbsp;

                                        <select name="stat" style="width:200px;height:35px;border:1px solid #03A9F4;color:#607D8B;font-weight:bold;">
                                            <option value="<?php echo $org_stat; ?>"><?php echo $customerStatArr[$org_stat]; ?></option>
                                            <?php
                                            foreach ($statToBeUpdated1 as $statToBeUpdated) {
                                                $value = $customerStatArr[$statToBeUpdated];
                                                ?>
                                                <?php if ($org_stat == 'live') { ?>
                                                    <option disabled="" value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option  value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <input type="submit" name="upd_stat" style="height:35px;border:none;background:#00BCD4;color:white;" value="Update Status" <?php if ($org_stat == 'live') { ?> disabled="" <?php } ?> />
                                        <?php if ($userTyp == "tech") { ?>
                                            <a href="addCloudDetails.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:blue;color:white;float:right;margin-left: 33px;">Add Cloud Details</button></a>
                                        <?php } ?>
                                        <a href="view-payment.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:#FF5722;color:white;float:right;">View Payment List</button></a>
                                    </div>
                                </div>
                        </div>
                        </form>
                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">                           
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->

                                    <div style="width:100%; padding:10px; float:left; background:#1ca8dd; color:#fff; font-size:30px; text-align:center;">
                                        Invoice - <?php echo $getUserDet->hotel_name; ?>
                                    </div>
                                    <div style="width:100%; padding:0px 0px;border-bottom:1px solid rgba(0,0,0,0.2);float:left;">
                                        <div style="width:30%; float:left;padding:10px;">

                                            <span style="font-size:14px;float:left; width:100%;">
                                                <h2><?php echo $getUserDet->hotel_name; ?></h2>
                                            </span>
                                            <span style="font-size:14px;float:left;width:100%;">
                                                <?php echo $getUserDet->address; ?>
                                            </span><br/>
                                            <span style="font-size:14px;float:left;width:100%;">
                                                <?php echo $getUserDet->email; ?>
                                            </span>
                                        </div>

                                        <div style="width:40%; float:right;padding:">
                                            <span style="font-size:14px;float:right;  padding:10px; text-align:right;">
                                                <b> Receive Date : </b> <?php echo $paymentData->received_date; ?>
                                            </span>
                                            <span style="font-size:14px;float:right;  padding:10px; text-align:right;">
                                                <b>Invoice# : </b><?php echo $paymentData->slno; ?>
                                            </span>
                                        </div>
                                    </div>

                                    <div style="width:100%; padding:0px; float:left;">

                                        <div style="width:100%;float:left;background:#efefef;">
                                            <span style="float:left; text-align:left;padding:10px;width:25%;color:#888;font-weight:600;">
                                                From Date
                                            </span>
                                            <span style="float:left; text-align:left;padding:10px;width:25%;color:#888;font-weight:600;">
                                                To Date
                                            </span>
                                            <span style="font-weight:600;float:left;padding:10px ;width:20%;color:#888;text-align:right;">
                                                Instalment Type
                                            </span>
                                            <span style="font-weight:600;float:left;padding:10px ;width:20%;color:#888;text-align:right;">
                                                Payment Type
                                            </span>
                                            <span style="font-weight:600;float:left;padding:10px ;width:10%;color:#888;text-align:right;">
                                                Payment Mode
                                            </span>
                                        </div>

                                        <div style="width:100%;float:left;">
                                            <span style="float:left; text-align:left;padding:10px;width:25%;color:#888;">
                                                <?php echo $paymentData->from_date; ?>

                                            </span>
                                            <span style="float:left; text-align:left;padding:10px;width:25%;color:#888;">
                                                <?php echo $paymentData->to_date; ?>

                                            </span>
                                            <span style="float:left; text-align:right;padding:10px;width:20%;color:#888;">
                                                <?php echo $paymentData->instalment_type; ?>

                                            </span>
                                            <span style="font-weight:normal;float:left;padding:10px ;width:20%;color:#888;text-align:right;">
                                                <?php echo $paymentData->payment_type; ?>
                                            </span>
                                            <span style="font-weight:normal;float:left;padding:10px ;width:10%;color:#888;text-align:right;">
                                                <?php echo $paymentData->payment_mode; ?>
                                            </span>

                                        </div>                                      

                                        <div style="width:100%;float:left; background:#fff;">

                                            <span style="font-weight:600;float:right;padding:10px 0px;width:100%;color:#666;text-align:center;">
                                                <b> Total : <?php echo $paymentData->amount; ?> INR</b>
                                            </span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
            </div>
    </body>
</html>

<?php
scrip();
?>
