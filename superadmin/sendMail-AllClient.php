<?php

include_once("../include/class/superadminClasses.php");
//checkUser();

@extract($_REQUEST);

//---------------------------------Cron Mail Function --------------------------------------------->

$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getAllCmsUserData();
// print_r($getUserDet);
$currentDate = date("Y-m-d");
date('Y-m-d', strtotime("+30 days"));


foreach ($getUserDet as $res) {
    $customerID = $res->slno;
    $user_id = $res->user_id;
    $instalmentType = $res->instalment_type;
    $instalmentCost = $res->Instalmentcost;
    $live_date = $res->live_date;
    $hotel_name = $res->hotel_name;
    $phone = $res->phone;
    $email = $res->email;
    $addOnServiceCost = $res->addOnServiceCost;
    $setUpCost = $res->setUpCost;
    $status = 'live';


    $OldDate = new DateTime($live_date);
    $now = new DateTime(Date('Y-m-d'));
    // print_r($OldDate->diff($now));
    $data = $OldDate->diff($now);
    $days = $data->days; // 365
    //-------------------Monthly Plan-------------------------------------------->
    if ($instalmentType == 'Monthly') {
        $start = new DateTime($live_date, new DateTimeZone("Asia/Kolkata"));
        $end = clone $start;
        $end->modify('+1 month');
        while (($start->format('m') + 1) % 12 != $end->format('m') % 12) {
            $end->modify('-1 day');
        }
        $nextBill = $end->format('d-M-Y');
        $from_date = $nextBill;
        $todate_start = new DateTime($from_date, new DateTimeZone("Asia/Kolkata"));
        $todate_end = clone $todate_start;
        $todate_end->modify('+1 month');
        while (($todate_start->format('m') + 1) % 12 != $todate_end->format('m') % 12) {
            $todate_end->modify('-1 day');
        }
        $to_date = $todate_end->format('d-M-Y');

        echo '<span style="color:green">' . $hotel_name . '-' . $nextBill . '</span><br/>';

        $today = new DateTime();
        $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
        $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a');
        $subject = 'Your website subscription payment alert';
        // $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";

        if ($nbOfDaysRemainingThisMonth == 17) {
            $mess = '<div><font color="#000000" face="Raleway, sans-serif"> Dear Sir, <br/> This mail is being written as a gentle reminder to inform you that your <strong style="color: green;"> '. $instalmentType.'</strong> advance payment</font><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">&nbsp;is expire</span><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"> next 5 days <strong style="color:red"> (expire date is '. $from_date.')</strong>.&nbsp; Would thus you to make below-mentioned payment:&nbsp;</span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><br></span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><b>Date Applicable :</b>&nbsp;From : '.$from_date.'&nbsp; &nbsp; &nbsp;</span><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">&nbsp;To&nbsp; &nbsp; :&nbsp;&nbsp;<span class="m_-8914500644016095469gmail-aBn"><span class="m_-8914500644016095469gmail-aQJ"><span class="aBn" data-term="goog_1330565885" tabindex="0"><span class="aQJ">'.$to_date .'</span></span></span></span></span></div><span class="m_-8914500644016095469gmail-im"><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><br></span></div><div><table cellspacing="0" cellpadding="0" style="box-sizing:inherit;border-collapse:collapse;margin:0px 0px 1.82em;width:745.6px;color:rgb(41,41,41);font-family:&quot;Open Sans&quot;,Arial,Helvetica,sans-serif"><tbody style="box-sizing:inherit"><tr style="box-sizing:inherit"><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><span style="background-color:rgb(106,168,79)">Service</span></td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><span style="background-color:rgb(106,168,79)">No. Of Months&nbsp; &nbsp; &nbsp; &nbsp;</span></td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><span style="background-color:rgb(106,168,79)">Amount per month (INR&nbsp; &nbsp; &nbsp; &nbsp;)</span></td></tr><tr style="box-sizing:inherit"><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top">Nodal CMS, Hosting, Maintenance Charges</td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top">1</td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top">'. $instalmentCost.'</td></tr><tr style="box-sizing:inherit"><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><b><br></b></td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><b><font color="#0000ff">TOTAL</font></b></td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><b><font color="#0000ff">'. $instalmentCost.'/- (Includes GST)</font></b></td></tr><tr style="box-sizing:inherit"><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"><br></td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"></td><td valign="top" style="box-sizing:inherit;padding:0.5em;border:1px solid rgb(82,79,78);vertical-align:top"></td></tr></tbody></table>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>&nbsp;&nbsp;</b></div><div><b><font color="#0000ff"><br></font></b></div><div><span style="font-family:Raleway,sans-serif"><b><font color="#0000ff"><u>Account Details:</u></font></b></span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><br></span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">Account Name: Hawthorn Technologies Pvt. Ltd.</span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">Account No.: 002105023189</span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">Bank : ICICI Bank</span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">IFSC Code:&nbsp;</span><span style="font-family:Arial,Helvetica,sans-serif;font-weight:700">ICIC0000021</span></div><div><span style="font-family:Arial,Helvetica,sans-serif;font-weight:700"><br></span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><br></span></div></span><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">Thank you and best regards,</span><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><br></span></div><span class="m_-8914500644016095469gmail-im"><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif"><br></span></div><div><span style="color:rgb(0,0,0);font-family:Raleway,sans-serif">Thank You,</span></div><div><font color="#000000" face="Raleway, sans-serif">Regards,</font></div><div><font color="#000000" face="Raleway, sans-serif">Finance Team</font></div>';
            $send_mail = $userDetCont->SendClientMail($email, $subject, $mess);
            echo 'sent1';
        } else if ($nbOfDaysRemainingThisMonth == 0) {
            $send_mail = $userDetCont->SendClientMail($email, $subject, $mess);
            echo 'sent2';
        } elseif ($nbOfDaysRemainingThisMonth == 25) {
            $send_mail = $userDetCont->SendClientMail($email, $subject, $mess);
            echo 'sent3';
        }
        echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/><br/>';

        //-------------------Quarterly Plan-------------------------------------------->
    } elseif ($instalmentType == 'Quarterly') {
        $start = new DateTime($live_date, new DateTimeZone("Asia/Kolkata"));
        $end = clone $start;
        $end->modify('+3 month');
        while (($start->format('m') + 3) % 12 != $end->format('m') % 12) {
            $end->modify('-1 day');
        }
        $nextBill = $end->format('d-M-Y');
        echo '<span style="color:green">' . $hotel_name . '-' . $nextBill . '</span><br/>';
        $today = new DateTime();
        $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
        $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
        echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/><br/>';


        //-------------------Half Yearly Plan-------------------------------------------->
    } elseif ($instalmentType == 'Half Yearly') {
        $start = new DateTime($live_date, new DateTimeZone("Asia/Kolkata"));
        $end = clone $start;
        $end->modify('+6 month');
        while (($start->format('m') + 6) % 12 != $end->format('m') % 12) {
            $end->modify('-1 day');
        }
        $nextBill = $end->format('d-M-Y');
        echo '<span style="color:green">' . $hotel_name . '-' . $nextBill . '</span><br/>';
        $today = new DateTime();
        $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
        $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
        echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/><br/>';

        //-------------------Yearly Plan-------------------------------------------->    
    } elseif ($instalmentType == 'Yearly') {
        $start = new DateTime($live_date, new DateTimeZone("Asia/Kolkata"));
        $end = clone $start;
        $end->modify('+1 year');
        while (($start->format('y') + 1) % 12 != $end->format('y') % 12) {
            $end->modify('-1 day');
        }
        $nextBill = $end->format('d-M-Y');
        echo '<span style="color:green">' . $hotel_name . '-' . $nextBill . '</span><br/>';
        $today = new DateTime();
        $lastDayOfThisMonth = new DateTime($end->format('d-M-Y'));
        $nbOfDaysRemainingThisMonth = $lastDayOfThisMonth->diff($today)->format('%a days left');
        echo '<span style="color:red">' . $nbOfDaysRemainingThisMonth . '<span/><br/>';
    }

    //--------------------------------Instalment Type Start------------------------------------------------>
}
?>