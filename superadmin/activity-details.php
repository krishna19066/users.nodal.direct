<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
$cmsUsersCont = new superAdminMain();
$get_home = $cmsUsersCont->gethomePageLogin($customerID); // Table name homeContent get data with customer Id
$home_res = $get_home[0];
$home_timestamp = $home_res->timestamp;
$get_aboutus = $cmsUsersCont->getAboutUsPage($customerID); // Table name static_page_table get data with customer Id
$aboutus_res = $get_aboutus[0];
$aboutus_timestamp = $aboutus_res->timestamp;
$get_contactus = $cmsUsersCont->getContactUsPage($customerID); // Table name static_page_contactus get data with customer Id
$contact_res = $get_contactus[0];
$contactus_timestamp = $contact_res->timestamp;
$get_property = $cmsUsersCont->getPropertyData($customerID); // Table name propertyTable get data with customer Id
$property_res = $get_property[0];
$property_timestamp = $property_res->timestamp;
$get_propertyRoom = $cmsUsersCont->getPropertyRoomData($customerID); // Table name propertyTable get data with customer Id
$propertyRoom_res = $get_propertyRoom[0];
$propertyRoom_timestamp = $propertyRoom_res->timestamp;
$get_client_login = $cmsUsersCont->getClientLoginWithCustomerID($customerID); // table - admin_details
$admin_details = $get_client_login[0];


//print_r($get_login_activity);
//die;
?>
<?php
if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];

    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        if ($cliend == 'Approved') {
            $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' ||  status='live' || status='cont_change' || status='desg_change' || status='approved')";
        } else {
            $statToBeUpdated1 = "(status='pending')";
        }
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified')";
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    }

    //------------------------- Status Update Options Ends ------------------------//
    if (isset($_REQUEST['search_client'])) {
        $clientName = $_REQUEST['ClientName'];
        $get_client_changes = $cmsUsersCont->getClientAllChanges($clientName);
        $resCount = count($get_client_changes);
    }
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Client Activity
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage Activity</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase"> Activity  - <?php echo $admin_details->user_id; ?></span>

                                    </div>
                                </div>
                            </div>
                                    <div class="portlet light form-fit bordered">
                                        <div class="portlet-title">
                                            <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="float: right;">Send Mail</button> -->
                                            <a href="manage-email.php" class="btn btn-info btn-lg" style="float: right;" >Send Mail</a>
                                            <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Last Login</span> &nbsp; &nbsp;
                                            <?php
                                            $date = $admin_details->last_login;
                                            $dateTime = date("jS  M, Y", strtotime($date));
                                            echo '<span style="color:green;">' . $dateTime . '</span> ,';
                                            $OldDate = new DateTime($date);
                                            $now = new DateTime(Date('Y-m-d'));
                                            // print_r($OldDate->diff($now));
                                            $data = $OldDate->diff($now);
                                            $days = $data->days;
                                            $months = $data->m;
                                            echo '<span style="color:blue;"> Total Days =' . $days . '</span>,';
                                            echo '<span style="color:red;">Months =' . $months . '</span>';
                                            ?>
                                        </div>
                                    </div>                                    
                            </div>
                        
                            <div class="portlet-body">

                                <div class="table-container">

                                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="2%">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>

                                                <th width="15%">Home Page</th>
                                                <th width="10%">About Us </th>
                                                <th width="10%">Contact Us</th>                                                                                                                                         
                                                <th width="10%">Service Apartment/Our Room</th>
                                                <th width="10%">Property Details/Room Details</th>
                                                <th width="10%">Action</th>

                                            </tr>
                                            <tr role="row" class="heading">
                                                <th width="2%" style="background:#dde5f9;"></th>
                                                <th width="5%" style="background:#dde5f9;"></th>
                                                <th width="15%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th> 
                                                <th width="10%" style="background:#dde5f9;"></th>                            
                                            </tr>
                                        </thead>
                                        <tbody>                                                                                      
                                            <tr role="row" class="odd">
                                                <td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="1"><span></span></label></td>                                            
                                                <td><?php
                                                    $dateTime = date("jS  M, Y", strtotime($home_timestamp));
                                                    echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                    $OldDate = new DateTime($date);
                                                    $now = new DateTime(Date('Y-m-d'));
                                                    // print_r($OldDate->diff($now));
                                                    $data = $OldDate->diff($now);
                                                    $days = $data->days;
                                                    $months = $data->m;
                                                    echo '<span style="color:blue;"> Total Days =' . $days . '</span><br/>';
                                                    echo '<span style="color:red;">Months =' . $months . '</span>';
                                                    ?>
                                                </td>
                                                <td><?php
                                                    $dateTime = date("jS  M, Y", strtotime($aboutus_timestamp));
                                                    echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                    $OldDate = new DateTime($date);
                                                    $now = new DateTime(Date('Y-m-d'));
                                                    // print_r($OldDate->diff($now));
                                                    $data = $OldDate->diff($now);
                                                    $days = $data->days;
                                                    $months = $data->m;
                                                    echo '<span style="color:blue;"> Total Days =' . $days . '</span><br/>';
                                                    echo '<span style="color:red;">Months =' . $months . '</span>';
                                                    ?>
                                                </td>                                                 
                                                <td><?php
                                                    $dateTime = date("jS  M, Y", strtotime($contactus_timestamp));
                                                    echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                    $OldDate = new DateTime($date);
                                                    $now = new DateTime(Date('Y-m-d'));
                                                    // print_r($OldDate->diff($now));
                                                    $data = $OldDate->diff($now);
                                                    $days = $data->days;
                                                    $months = $data->m;
                                                    echo '<span style="color:blue;"> Total Days =' . $days . '</span><br/>';
                                                    echo '<span style="color:red;">Months =' . $months . '</span>';
                                                    ?>
                                                </td>

                                                <td><?php
                                                    $dateTime = date("jS  M, Y", strtotime($propertyRoom_timestamp));
                                                    echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                    $OldDate = new DateTime($date);
                                                    $now = new DateTime(Date('Y-m-d'));
                                                    // print_r($OldDate->diff($now));
                                                    $data = $OldDate->diff($now);
                                                    $days = $data->days;
                                                    $months = $data->m;
                                                    echo '<span style="color:blue;"> Total Days =' . $days . '</span><br/>';
                                                    echo '<span style="color:red;">Months =' . $months . '</span>';
                                                    ?>
                                                </td>
                                                <td><?php
                                                    $dateTime = date("jS  M, Y", strtotime($property_timestamp));
                                                    echo '<span style="color:green;">' . $dateTime . '</span><br/>';
                                                    $OldDate = new DateTime($date);
                                                    $now = new DateTime(Date('Y-m-d'));
                                                    // print_r($OldDate->diff($now));
                                                    $data = $OldDate->diff($now);
                                                    $days = $data->days;
                                                    $months = $data->m;
                                                    echo '<span style="color:blue;"> Total Days =' . $days . '</span><br/>';
                                                    echo '<span style="color:red;">Months =' . $months . '</span>';
                                                    ?>
                                                </td>

                                                <td> 

<!-- <a href="activity-details.php?customerID=<?php //echo $res->customerID;    ?>" class="btn btn-success">Send Mail</a>-->
                                                </td>  

                                            </tr>

                                        </tbody>
                                    </table>

                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" href="#collapse1">Property</a>
                                                </h4>
                                            </div>
                                            <div id="collapse1" class="panel-collapse collapse">
                                                <?php
                                                foreach ($get_property as $prop_res) {
                                                    $timestamp = $prop_res->timestamp;
                                                    $propertName = $prop_res->propertyName;
                                                    $propertyID = $prop_res->propertyID;
                                                    // $get_propertyPhoto = $cmsUsersCont->getPropertyPhoto($propertyID);
                                                    // print_r($get_propertyPhoto);
                                                    ?>
                                                    <div class="panel-body"><?php
                                                        echo $propertName . '<br/> Last Update:';
                                                        $dateTime = date("jS  M, Y", strtotime($timestamp));
                                                        echo '<span style="color:green;">' . $dateTime . '</span> ,';
                                                        $OldDate = new DateTime($timestamp);
                                                        $now = new DateTime(Date('Y-m-d'));
                                                        // print_r($OldDate->diff($now));
                                                        $data = $OldDate->diff($now);
                                                        $days = $data->days;
                                                        $months = $data->m;
                                                        echo '<span style="color:blue;"> Total Days =' . $days . '</span>,';
                                                        echo '<span style="color:red;">Months =' . $months . '</span>';
                                                        echo '<span style="float:right;"> Photo Last Upload</span><br/>';

                                                        $get_propertyPhoto = $cmsUsersCont->getPropertyPhoto($propertyID);
                                                        $photo_res = $get_propertyPhoto[0];
                                                        $photo_last_upload = $photo_res->timestamp;
                                                        echo '<span style="float:right;">' . $photo_last_upload . '</span>';
                                                        ?>
                                                    </div>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" href="#collapse2">Property Room</a>
                                                </h4>
                                            </div>
                                            <div id="collapse2" class="panel-collapse collapse">
                                                <?php
                                                foreach ($get_propertyRoom as $room_res) {
                                                    $timestamp = $room_res->timestamp;
                                                    $roomName = $room_res->roomType;
                                                    $roomID = $room_res->roomID;
                                                    ?>
                                                    <div class="panel-body"><?php
                                                        echo $roomName . '<br/> Last Update:';
                                                        $dateTime = date("jS  M, Y", strtotime($timestamp));
                                                        echo '<span style="color:green;">' . $dateTime . '</span> ,';
                                                        $OldDate = new DateTime($timestamp);
                                                        $now = new DateTime(Date('Y-m-d'));
                                                        // print_r($OldDate->diff($now));
                                                        $data = $OldDate->diff($now);
                                                        $days = $data->days;
                                                        $months = $data->m;
                                                        echo '<span style="color:blue;"> Total Days =' . $days . '</span>,';
                                                        echo '<span style="color:red;">Months =' . $months . '</span>';
                                                        echo '<span style="float:right;"> Photo Last Upload</span><br/>';

                                                        $get_propertyPhoto = $cmsUsersCont->getPropertyRoomPhoto($roomID);
                                                        $photo_res = $get_propertyPhoto[0];
                                                        $photo_last_upload = $photo_res->timestamp;
                                                        echo '<span style="float:right;">' . $photo_last_upload . '</span>';
                                                        ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <!--   <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
             <!--   <div class="modal-content" style="width: 850px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Email Notification:</h4>
                        <h5 style="float:right;">
                            <?php
                            $date = $admin_details->timestamp;
                            if ($date != null) {
                                echo date("jS  M, Y", strtotime($date));
                            }
                            ?>
                        </h5>
                        <form action="" method="post" class="form-horizontal">
                            <h5>Sent To:</h5>
                                <input type = "text" name = "subject" class = "form-control" value = "<?php echo $admin_details->email  ?>" />                             

                    </div>
                    <div class="modal-body">
                        <p>Subject:</p>
                        <div class = "form-group">
                            <input type = "text" name = "subject" class = "form-control" value = "Please Login Sitepanel and Update Content." />                   
                        </div>
                        <p>Mail Body:</p>
                        <div class = "form-group">
                            <textarea name = "email_body" class = "comment_post form-control" rows = "5" cols = "7">Type Here.. </textarea>
                        </div>
                        <script type = "text/javascript">
                            CKEDITOR.replace('email_body');
                        </script>													

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <input type="submit" class="btn green button-submit" name="send_email" value="Send Email"/>                      
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">                     
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>              
            </div>
        </div>  -->
    </body>
</html>

<?php
scrip();
?>


