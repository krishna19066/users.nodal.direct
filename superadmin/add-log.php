<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>

<?php
$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getUserDet($userId);

$org_stat = $getUserDet->status;

if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];
    if ($userTyp == "finance" || $userTyp == "content") {
        $blockStat = "Y";
    } elseif ($userTyp == "admin" || $userTyp == "sales" || $userTyp == "tech") {
        $blockStat = "N";
    }
//------------------------ Status Update Options Starts -----------------------//
//------------------------- Status Update Options Ends ------------------------//
    if (isset($_POST['remove_email'])) {
        echo $selected = '';
    }
//------------------Mail Send to all Client for Notification alert----------------------------------------------->
    if (isset($_POST['newChanges_Submit'])) {
        $clientName99 = $_POST['clientName'];
        $clientName = (explode(",", $clientName99));
        $heading = $_POST['heading'];
        $new_changes = $_POST['new_changes'];
        if ($clientName99 != null && $heading !== NULL && $new_changes != NULL) {
            $ins = $userDetCont->InsertNewChanges($clientName99, $heading, $new_changes);
                    
            echo '<script type="text/javascript">
                alert("New Changes Added Succcesfuly");              
window.location = "manage-log.php";
            </script>';
            exit;
        } else {
            echo '<script type="text/javascript">
                alert("Please Enter All Field and Select Client List");              
window.location = "manage-log.php";
            </script>';
            exit;
        }
    }
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
       <!-- <script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="../sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>

    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Clients Changes
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add Log</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">View Details - <?php echo $getUserDet->hotel_name; ?></span>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <div class="portlet light form-fit bordered">
                                    <div class="portlet-title">
                                        <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Update Status</span> &nbsp; &nbsp;

                                    </div>
                                </div>
                        </div>
                        </form>

                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">                               
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">To<span class="required"> * </span></label>                                                
                                                <div class="col-md-4">                                                 
                                                    <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal">Select Client</button>

                                                </div>
                                            </div>
                                            <?php
                                            if (isset($_POST['submit'])) {//to run PHP script on submit
                                                if (!empty($_POST['hotel_name'])) {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Change To<span class="required"> * </span></label> 
                                                        <div class = "col-md-4">

                                                            <input readonly="" type = "text" name = "clientName" class = "form-control" value = "<?php
                                                            foreach ($_POST['hotel_name'] as $selected) {
                                                                echo $selected . ',';
                                                            }
                                                            ?>" required = "" />
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>

                                            <div class = "form-group">
                                                <label class = "col-md-3 control-label">Heading <span class = "required"> * </span></label>
                                                <div class = "col-md-4">
                                                    <input type = "text" name = "heading" class = "form-control" value = "" />
                                                    <span class = "help-block"> Changes heading Here.. </span>
                                                </div>
                                            </div>
                                            <div class = "form-group">
                                                <label class = "col-md-3 control-label" for = "form_control_1">New Changes <span class = "required"> * </span></label>
                                                <div class = "col-md-6">
                                                    <textarea name = "new_changes" class = "comment_post form-control" rows = "5" cols = "7"> </textarea>
                                                </div>
                                                <script type = "text/javascript">
                                                    CKEDITOR.replace('new_changes');
                                                </script>													
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="submit" class="btn green button-submit" name="newChanges_Submit" value="Submit"/> 
                                                        <input type="submit" class="btn green button-submit" name="remove_email" value="Remove"/> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Client List</h4>
                    </div>
                    <div class="modal-body">
                        <p><h4 style="color:gray">Select Client</h4></p>
                        <form action="#" method="post">
                            <div class="col-auto my-1">
                                <?php
                                $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";

                                $cmsUsersCont = new superAdminMain();
                                $cmsUsers = $cmsUsersCont->getCmsUsers($statToBeUpdated1);
                                ?>
                                <input type="checkbox" name="select-all" id="select-all" /><b> Select All</b><hr/>
                                <?php
                                $count = 1;
                                foreach ($cmsUsers as $userData) {
                                    ?>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" name="hotel_name[]" value="<?php echo $userData->hotel_name ?>">
                                        <label class="custom-control-label" for="customControlAutosizing"><?php echo $userData->hotel_name ?></label>
                                    </div>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-info" name="submit" value="Submit"/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <script language="JavaScript">
            $('#select-all').click(function (event) {
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function () {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function () {
                        this.checked = false;
                    });
                }
            });
        </script>
        <style>

            .modal-body{
                height: 250px;
                overflow-y: auto;
            }
        </style>


    </body>
</html>

<?php
scrip();
?>
