<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>

<?php
$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getUserDet($userId);

$org_stat = $getUserDet->status;

if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];
    if ($userTyp == "finance" || $userTyp == "content") {
        $blockStat = "Y";
    } elseif ($userTyp == "admin" || $userTyp == "sales" || $userTyp == "tech") {
        $blockStat = "N";
    }
    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        $statToBeUpdated1 = array("payment_recv");
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = array("content_recv", "content_upd", "verified");
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = array("approved", "cont_change", "desg_change");
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = array("live");
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = array("payment_recv", "content_recv", "content_upd", "verified", "approved", "cont_change", "desg_change", "live", "pending");
    }

    //------------------------- Status Update Options Ends ------------------------//
}

//------------------------ Update Status Starts -----------------------//

if (isset($_POST['upd_stat'])) {
    $statToBeupd = $_POST['stat'];
    $getUserDet = $userDetCont->updCustomerStat($userId, $statToBeupd);

    if ($statToBeupd == "payment_recv") {
        echo '<script type="text/javascript">
                alert("Status Updated succesfully");              
window.location = "manage-user.php";
            </script>';
        exit;
    } else {
        Header("location:manage-user.php");
        exit;
    }
}
if (isset($_POST['add_payment'])) {
    $cmsUsersCont = new superAdminMain();
    $cmsUsers = $cmsUsersCont->getPaymentDeatils($userId);
    $userData1 = $cmsUsers[0];

    $getUserDet = $cmsUsersCont->getUserDet($userId);
    $org_setUpCost = $getUserDet->setUpCost;
    $org_instalmentCost = $getUserDet->Instalmentcost;

    $sumOfSetUpCostAmount = $cmsUsersCont->getSumOfSetUpCost($userId);
    $sumOfCost_1 = $sumOfSetUpCostAmount[0];
    $sumOfCost = $sumOfCost_1->sumOfCost;
    $tdsAmount_setUp = $sumOfCost_1->tdsAmount;
    if ($tdsAmount_setUp == 0) {
        $tdsAmount_setUp = $_POST['tds_amount'];
    }
    $customerID = $_POST['customerID'];
    $amount = $_POST['amount'];
    $tds_amount = $_POST['tds_amount'];
    $payment_type = $_POST['payment_type'];
    $from_date = $_POST['from_date'];
    $to_date = $_POST['to_date'];
    $setUpCost = $_POST['setUpCost'];
    $getSumOfInsCost = $cmsUsersCont->getSumOfInsCost($userId, $from_date, $to_date);
    $InstalSumCost1 = $getSumOfInsCost[0];
    $InstalSumCost = $InstalSumCost1->InstalSumCost;
    $tdsAmount = $InstalSumCost1->tdsAmount;
    if ($tdsAmount == 0) {
        $tdsAmount = $_POST['tds_amount'];
    }
    // print_r($getSumOfInsCost);

    if ($payment_type == 'SetUpCost') {

        if ($sumOfCost > 0) {

            $pending_amt = $org_setUpCost - (($sumOfCost + $amount) + $tdsAmount_setUp);
        } else {

            $pending_amt = $org_setUpCost - ($amount + $tds_amount);
        }
    } else {

        if ($InstalSumCost > 0) {
            $pending_amt = $org_instalmentCost - (($InstalSumCost + $amount) + $tdsAmount);
        } else {
            $pending_amt = $org_instalmentCost - ($amount + $tds_amount);
        }
    }

    if ($setUpCost != NULL) {
        $add_payment = $userDetCont->AddPayment($customerID, $pending_amt);
        echo '<script type="text/javascript">
                alert("Payment has been added succesfully");              
window.location = "view-payment.php?userId=' . $userId . '";
            </script>';
        exit;
    } else {
        echo '<script type="text/javascript">
                alert("SetUpCost is empty Not add Payment");              
window.location = "manage-user.php";
            </script>';
        exit;
    }
}
if (isset($edit) == 'payment' && isset($userId) != NULL && isset($payment_id) != NULL) {
    $cmsUsersCont = new superAdminMain();
    $cmsUsers = $cmsUsersCont->getPaymentDeatilsWithPaymentID($payment_id);
    $paymentData = $cmsUsers[0];
}
if (isset($_POST['Update_payment'])) {
    $payment_id = $_POST['payment_id'];
    $userId = $_POST['user_id'];
    $cmsUsersCont = new superAdminMain();

    $getUserDet = $cmsUsersCont->getUserDet($userId);
    $org_setUpCost = $getUserDet->setUpCost;
    $org_instalmentCost = $getUserDet->Instalmentcost;

    $sumOfSetUpCostAmount = $cmsUsersCont->getSumOfSetUpCost($userId);
    $sumOfCost_1 = $sumOfSetUpCostAmount[0];
    $sumOfCost = $sumOfCost_1->sumOfCost;
    $customerID = $_POST['customerID'];
    $amount = $_POST['amount'];
    $tds_amount = $_POST['tds_amount'];
    $payment_type = $_POST['payment_type'];
    $from_date = $_POST['from_date'];
    $to_date = $_POST['to_date'];
    $setUpCost = $_POST['setUpCost'];
    $getSumOfInsCost = $cmsUsersCont->getSumOfInsCost($userId, $from_date, $to_date);
    $InstalSumCost1 = $getSumOfInsCost[0];
    $InstalSumCost = $InstalSumCost1->InstalSumCost;
    // print_r($getSumOfInsCost);

    if ($payment_type == 'SetUpCost') {
        $pending_amt = $org_setUpCost - ($amount + $tds_amount);
    } else {
        $pending_amt = $org_instalmentCost - ($amount + $tds_amount);
    }

    $add_payment = $userDetCont->UpdatePayment($payment_id, $pending_amt);
    echo '<script type="text/javascript">
                alert("Payment has been Updated succesfully");              
window.location = "view-payment.php?userId=' . $userId . '";
            </script>';
    exit;
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>View Details - <?php echo $getUserDet->hotel_name; ?>
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">View User Details</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">View Details - <?php echo $getUserDet->hotel_name; ?></span>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <div class="portlet light form-fit bordered">
                                    <div class="portlet-title">
                                        <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Update Status</span> &nbsp; &nbsp;

                                        <select name="stat" style="width:200px;height:35px;border:1px solid #03A9F4;color:#607D8B;font-weight:bold;">
                                            <option value="<?php echo $org_stat; ?>"><?php echo $customerStatArr[$org_stat]; ?></option>
                                            <?php
                                            foreach ($statToBeUpdated1 as $statToBeUpdated) {
                                                $value = $customerStatArr[$statToBeUpdated];
                                                ?>
                                                <?php if ($org_stat == 'live') { ?>
                                                    <option disabled="" value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option  value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
                                                <?php
                                            }
                                            ?>
                                        </select>

                                        <input type="submit" name="upd_stat" style="height:35px;border:none;background:#00BCD4;color:white;" value="Update Status" <?php if ($org_stat == 'live') { ?> disabled="" <?php } ?> />

                                        <?php if ($userTyp == "tech") { ?>
                                            <a href="addCloudDetails.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:blue;color:white;float:right;margin-left: 33px;">Add Cloud Details</button></a>
                                        <?php } ?>
                                        <a href="view-payment.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:#FF5722;color:white;float:right;">View Payment List</button></a>
                                    </div>
                                </div>
                        </div>
                        </form>

                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">                           
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Hotel Name <span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <input type="hidden" name="user_id" class="form-control" value="<?php echo $getUserDet->user_id; ?>"/>
                                                    <input type="hidden" name="customerID" class="form-control" value="<?php echo $getUserDet->slno; ?>"/>
                                                    <input type="text" name="hotel_name" class="form-control" value="<?php echo $getUserDet->hotel_name; ?>" <?php if ($getUserDet->hotel_name != null) { ?>  readonly <?php } ?> />
                                                    <!--<span class="help-block"> CMS User Hotel Name Here.. </span> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Total Set up Cost <span class="required"> * </span></label>
                                                <div class="col-md-4">                                                   
                                                    <input required="" type="text" name="setUpCost" class="form-control" value="<?php echo $getUserDet->setUpCost; ?>" readonly="" />

                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                 <label class="col-md-3 control-label">Paid Amount <span class="required"> * </span></label>
                                                 <div class="col-md-4">                                                   
                                                     <input type="text" name="setUpCost" class="form-control" value="<?php echo $getUserDet->paid_amount; ?>" <?php if ($getUserDet->paid_amount != null) { ?>  readonly <?php } ?> />
 
                                                 </div>
                                             </div> -->
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Received Date  <span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <input type="date" name="received_date" class="form-control" value="<?php echo $paymentData->received_date; ?>" required=""  />
                                                    <span class="help-block"> Received date Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Mode Of Payment <span class="required"> * </span></label>
                                                <div class="col-md-4">
                                                    <select name="payment_mode" class="form-control">

                                                        <?php if ($edit == 'payment') { ?>
                                                            <option value="<?php $paymentData->payment_mode; ?>"><?php echo $paymentData->payment_mode; ?></option>
                                                        <?php } else { ?>
                                                            <option value="">Select Payment Mode</option>
                                                        <?php } ?>
                                                        <option value="cheque">Cheque</option>
                                                        <option value="cash">Cash</option>
                                                        <option value="online">Online</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Payment Type 
                                                    <span class="required"> * </span>
                                                </label>

                                                <div class="col-md-4">
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="payment_type" id="optionsRadios4" value="SetUpCost" data-title="Set Up Cost" <?php if ($paymentData->payment_type == 'SetUpCost') { ?> checked <?php } ?> onchange="$(this).parent().parent().parent().parent().next().hide();">Set Up Cost
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="payment_type" id="optionsRadios5" value="Instalment" data-title="Instalment" <?php if ($paymentData->payment_type == 'Instalment') { ?> checked <?php } ?> onchange="$(this).parent().parent().parent().parent().next().fadeIn();"> Instalment
                                                            <span></span>
                                                        </label>

                                                    </div>

                                                </div>
                                            </div>

                                            <div id="addonDetails" class="addonDetails" style="display:none;">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">From Date  <span class=""> * </span></label>
                                                    <div class="col-md-4">
                                                        <input type="date" name="from_date" class="form-control" value="<?php echo $paymentData->from_date; ?>" />
                                                        <span class="help-block"> From date Here.. </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">To Date  <span class=""> * </span></label>
                                                    <div class="col-md-4">
                                                        <input type="date" name="to_date" class="form-control" value="<?php echo $paymentData->to_date; ?>" />
                                                        <span class="help-block"> To date Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Instalment Type 
                                                        <span class=""></span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input  type="radio" name="instalment_type" id="optionsRadios4" value="Monthly" data-title="Monthly" disabled="" <?php if ($getUserDet->instalment_type == 'Monthly') { ?> checked <?php } ?>>Monthly
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input  type="radio" name="instalment_type" id="optionsRadios5" value="Quarterly" data-title="Quarterly" disabled="" <?php if ($getUserDet->instalment_type == 'Quarterly') { ?> checked <?php } ?>>Quarterly
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input  type="radio" name="instalment_type" id="optionsRadios5" value="Yearly" data-title="Yearly" disabled="" <?php if ($getUserDet->instalment_type == 'Yearly') { ?> checked <?php } ?>>Yearly
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input  type="radio" name="instalment_type" id="optionsRadios5" value="Half Yearly" data-title="Half Yearly" disabled="" <?php if ($getUserDet->instalment_type == 'Half Yearly') { ?> checked <?php } ?>>Half Yearly
                                                                <span></span>
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Amount
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="number" name="amount" class="form-control" value="<?php echo $paymentData->amount; ?>" required="" />
                                                    <span class="help-block"> Total Amount Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">TDS
                                                    <span class="required"> * </span>
                                                </label>

                                                <div class="col-md-4">
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="tds_type" id="optionsRadios6" value="No" data-title="No" <?php if ($paymentData->tds_type == 'No') { ?> checked <?php } ?> onchange="$(this).parent().parent().parent().parent().next().hide();">No
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="tds_type" id="optionsRadios7" value="Yes" data-title="Yes" <?php if ($paymentData->tds_type == 'Yes') { ?> checked <?php } ?> onchange="$(this).parent().parent().parent().parent().next().fadeIn();">Yes
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="addonDetails" class="addonDetails" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> TDS Amount
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="number" name="tds_amount" class="form-control" value="<?php echo $paymentData->tds_amount; ?>" />
                                                        <span class="help-block"> Total TDS Amount Here.. </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <?php if ($edit == 'payment') { ?>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden" name="payment_id" value="<?php echo $paymentData->slno; ?>"/>
                                                        <input type="submit" class="btn green button-submit" name="Update_payment" value="Update" /> 
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="submit" class="btn green button-submit" name="add_payment" value="Submit" /> 
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }
                                        ?>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
</body>
</html>

<?php
scrip();
?>
