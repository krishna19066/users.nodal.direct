<?php

include_once("../include/class/superadminClasses.php");
//checkUser();

@extract($_REQUEST);

//---------------------------------Cron Mail Function --------------------------------------------->
$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getAllCmsUserData();
$googleAnalytic_data = $userDetCont->getGoogleAnalyticReportData();
foreach ($googleAnalytic_data as $res) {
    $customerID = $res->customerID;
    $analytic_url = $res->analytic_url;
    $get_user_details = $userDetCont->getClientLoginWithCustomerID($customerID);
    foreach ($get_user_details as $dt) {
         $email = $dt->center_email;
         $companyName = $dt->companyName;
    }
    $currentDate = date("d");
    $subject = "Google Analytics Report";
    $mess = "hello testing mail";
    if ($currentDate == '01') {

        $subject = $companyName.'- Online Website report (Realtime)';
        $mess = '<div dir="ltr"><div style="font-family:arial,helvetica,sans-serif"><br></div><div style="font-family:arial,helvetica,sans-serif">Please find below the real-time report for tracking your website traffic. Please bookmark this link and it will always be updated (T-1, that is Today minus 1 date). This report is invaluable for studying data on site traffic, which pages are doing well, what needs to be focused on, whether to run Adwords or not, etc. Since its early days post-launch, some data items may not be comprehensive/blank (this will correct itself with passage of time)</div><div style="font-family:arial,helvetica,sans-serif"><br></div><div style="font-family:arial,helvetica,sans-serif">It will show the following real time&nbsp;info</div><div><ul><li style="margin-left:15px"><font face="arial, helvetica, sans-serif">Website summary stats (Visits, traffic, time on site)</font></li><li style="margin-left:15px"><font face="arial, helvetica, sans-serif">Demographics of users (Age, location)</font></li><li style="margin-left:15px"><font face="arial, helvetica, sans-serif">Platforms (Split between mobile, desktop)</font></li><li style="margin-left:15px"><font face="arial, helvetica, sans-serif">Organic report (This is the unpaid / organic traffic generated naturally for your site)</font></li><li style="margin-left:15px"><font face="arial, helvetica, sans-serif">Session data (How much average time spent on each page)</font></li></ul></div><div><div><span style="font-family:arial,helvetica,sans-serif">PS - This report will automatically</span>&nbsp;update itself and the default time set is last 28 days. You can change this in the report by clicking on the top right button (green). Any suggestions, ideas are most welcome! (Please bookmark below link)</div><div><div class="gmail_default" style="font-family:arial,helvetica,sans-serif">​</div><div class="gmail_default" style="font-family:arial,helvetica,sans-serif"><a href="'.$analytic_url.'" style="font-family:arial,sans-serif" target="_blank" data-saferedirecturl='.$analytic_url.' "><b>'.$companyName.' Website Report</b></a></div></div><div><br></div><div></div><div>@ Team Hawthorn</div></div><div><br></div><div><br></div><div><br></div></div>';

        $to = $email;
        // $cc = $r['mail_cc'];
        $from = 'tech@theperch.in';
        $url = 'https://api.sendgrid.com/';
        $user = 'kunalsingh2000';
        $pass = 'P@ssw0rd';
        $json_string = array('to' => array($to), 'category' => 'Remainder');

        $params = array(
            'api_user' => 'kunalsingh2000',
            'api_key' => 'P@ssw0rd',
            'x-smtpapi' => json_encode($json_string),
            'to' => $to,
            'subject' => $subject,
            'html' => $mess,
            'replyto' => $to,
            // 'cc' => $cc,
            'fromname' => 'Super Admin',
            'text' => 'testing body',
            'from' => $from,
        );

        //echo $row[email];
        $request = $url . 'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // obtain response
        $response = curl_exec($session);
        curl_close($session);
    }else{
        echo 'fail <br/>';
    }
}
?>