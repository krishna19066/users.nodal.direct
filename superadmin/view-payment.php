<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>
<?php
if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];

    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        $statToBeUpdated1 = "(status='payment_recv' || status='pending')";
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified')";
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    }

    //------------------------- Status Update Options Ends ------------------------//
}
if (isset($_POST["ExportType"])) {

    header("http://www.stayondiscount.com/superadmin/export/exportPanment.php");
}
?>
<?php
$cmsUsersCont = new superAdminMain();
$cmsUsers = $cmsUsersCont->getPaymentDeatils($userId);
$userData1 = $cmsUsers[0];

$getUserDet = $cmsUsersCont->getUserDet($userId);
$org_setUpCost = $getUserDet->setUpCost;
$org_instalmentCost = $getUserDet->Instalmentcost;

$sumOfSetUpCostAmount = $cmsUsersCont->getSumOfSetUpCost($userId);
$sumOfCost_1 = $sumOfSetUpCostAmount[0];
$sumOfCost = $sumOfCost_1->sumOfCost;
if (isset($_POST['send_email'])) {
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $email_body = $_POST['email_body'];
    $hotelName = $getUserDet->hotel_name;
    if ($email != null && $subject !== NULL && $email_body != NULL) {
        $ins = $cmsUsersCont->InsertPaymentAlert($email, $subject, $email_body, $hotelName);
        // $subject = $subject;
        // $mess = "Information :-  <br /><br /> Hotel Name :- " . $hotel_name . "<br /><br /> Email : " . $email . "<br /><br /> Mobile : " . $phone . "<br /><br /><span style='color:red;'>Left Only : 5 Days </span> <br /><br /><hr/> Package Information :- <br /><br /> Instalment Type :- " . $instalmentType . "<br /><br /><span style='color:green'> Instalment Cost : Rs." . $instalmentCost . "</span><br /><br /> Add On Service Cost :Rs. " . $addOnServiceCost . "<br /><br /><br /> Set Up Cost :Rs. " . $setUpCost . "<br /><br /><hr/>";
        $mess = $email_body;
        $to = $email;
        // $cc = $r['mail_cc'];
        $from = 'finance@theperch.in';
        $url = 'https://api.sendgrid.com/';
        $user = 'kunalsingh2000';
        $pass = 'P@ssw0rd';
        $json_string = array('to' => array($to), 'category' => 'Remainder');

        $params = array(
            'api_user' => 'kunalsingh2000',
            'api_key' => 'P@ssw0rd',
            'x-smtpapi' => json_encode($json_string),
            'to' => $to,
            'subject' => $subject,
            'html' => $mess,
            'replyto' => $to,
            // 'cc' => $cc,
            'fromname' => 'hawthorntech.com',
            'text' => 'testing body',
            'from' => $from,
        );
        $request = $url . 'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        echo '<script type="text/javascript">
                alert("Mail Sent Succcesfuly");              
window.location = "view-payment.php?userId=' . $userId . '";
            </script>';
        exit;
    } else {
        echo '<script type="text/javascript">
                alert("Please Enter All Mail Body");              
window.location = "view-payment.php?userId=' . $userId . '";
            </script>';
        exit;
    }
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
        <script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
       <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
        <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
       <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>             
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Payment -  <span class="caption-subject font-red sbold uppercase"><?php echo $userData1->propertyName; ?></span>
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage Payment</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Manage Payment</span>
                                    </div>
                                </div>
                                <a href="/superadmin/export/exportPayment.php?userId=<?php echo $userId; ?>" class="btn btn-info" style="float:right;margin-left: 16px;">Download</a>
                                <span class="caption-subject font-green sbold uppercase">
                                    <?php
                                    $total_bill = $cmsUsersCont->TotalBillCycle($userId);
                                    $totalBillCycle = count($total_bill);
                                    ?>
                                    Total Bill Cycle= <?php echo $totalBillCycle; ?>
                                </span>,
                                <span class="caption-subject font-red sbold uppercase">
                                    <?php
                                    $pending_bill = $cmsUsersCont->PendingBillCycle($userId);
                                    $pendingBillCycle_1 = count($pending_bill);
                                    $pendingBillCycle = ($totalBillCycle - $pendingBillCycle_1);
                                    ?>
                                    Pending Bill= <?php echo $pendingBillCycle; ?>
                                </span>
                                <button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal<?php echo $userId; ?>" style="float:right;">Payment Alert</button>

                                <a href="manage-payment.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:blue;color:white;float:right;margin-right: 33px;">Add Payment</button></a>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span> </span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="Cancel">Cancel</option>
                                                <option value="Cancel">Hold</option>
                                                <option value="Cancel">On Hold</option>
                                                <option value="Close">Close</option>
                                            </select>
                                            <button class="btn btn-sm green table-group-action-submit">
                                                <i class="fa fa-check"></i> Submit</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <?php
                    $cmsUsersCont = new superAdminMain();
                    $cmsUsers = $cmsUsersCont->getPaymentDeatils($userId);
                    if ($cmsUsers != NULL) {
                        ?>

                        <table id="example" class="table table-striped table-bordered table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="5%"> Sl. No. </th>
                                    <th width="10%"> Receive Date </th>
                                    <th width="10%"> Hotel Name </th>
                                    <th width="10%">From Date </th>
                                    <th width="10%"> To Date </th>
                                    <th width="10%">Set up Cost </th>
                                    <th width="10%"> Total Amount </th>
                                    <th width="10%"> Received Amount </th>
                                    <th width="10%"> Pending Amount </th>
                                    <th width="8%"> TDS Amount </th>
                                    <th width="10%"> Payment Mode </th>
                                    <th width="10%"> Payment Type </th>
                                    <th width="10%"> Actions </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($cmsUsers as $userData) {
                                    $custStat = $userData->status;
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td <?php if ($userData->payment_mode == 'Cron') { ?> style="background:red"<?php } ?>><?php
                                            $date = $userData->received_date;
                                            if ($date != null) {
                                                echo date("jS  M, Y", strtotime($date));
                                            } else {
                                                echo '---';
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $userData->propertyName; ?></td>
                                        <td><?php
                                            $date = $userData->from_date;
                                            if ($date != null) {
                                                echo date("jS  M, Y", strtotime($date));
                                            } else {
                                                echo '---';
                                            }
                                            ?>
                                        </td>
                                        <td><?php
                                            $date = $userData->to_date;
                                            if ($date != null) {
                                                echo date("jS  M, Y", strtotime($date));
                                            } else {
                                                echo '---';
                                            }
                                            ?>
                                        </td>
                                        <td style="color: goldenrod;">
                                            <?php
                                            if ($userData->payment_type == 'Instalment') {
                                                echo '---';
                                            } else {
                                                echo 'Rs.' . $org_setUpCost;
                                            }
                                            ?>
                                        </td>
                                        <td style="color: gray;">
                                            <?php
                                            if ($userData->payment_type == 'Instalment') {
                                                echo 'Rs.' . $org_instalmentCost;
                                            } else {
                                                echo '---';
                                            }
                                            ?>
                                        </td>
                                        <td style="color: blue;"><?php echo'Rs.' . $userData->amount; ?></td>
                                        <td>
                                            <?php
                                            if ($userData->pending_amount != null && $userData->pending_amount != 0) {
                                                echo "<span style='color:green'>Rs." . $userData->pending_amount . "</span>";
                                            } else {
                                                echo "<span style='color:red'>No Payment Pending</span>";
                                            }
                                            ?>
                                        </td>
                                         <td>
                                            <?php
                                            if ($userData->tds_amount != NULL && $userData->tds_amount != 0) {
                                                echo "<span style='color:gray'>Rs." . $userData->tds_amount . "</span>";
                                            } else {
                                                echo "<span style='color:red'>No</span>";
                                            }
                                            ?>
                                        </td>
                                        <td><span class="label label-sm label-success"><?php echo $userData->payment_mode; ?></span></td>
                                        <td><?php echo $userData->payment_type; ?></td>
                                        <?php if ($userTyp == "finance" || $userTyp == 'tech' || $userTyp == 'admin') { ?>

                                            <td><a href="view-invoice.php?userId=<?php echo $userData->user_id; ?>&&edit=payment" class="btn btn-sm btn-outline blue"><i class="fa fa-search"></i> View Invoice</a></td>
                                        <?php } ?>
                                        <?php if ($userTyp == "finance") { ?>

                                            <td></td>
                                        <?php } ?>
                                        <?php if ($userTyp == 'tech' || $userTyp == 'admin') { ?>
                                            <td><a href="manage-payment.php?userId=<?php echo $userData->user_id; ?>&&edit=payment&&payment_id=<?php echo $userData->slno; ?>" class="btn btn-sm btn-outline blue"><i class="fa fa-edit"></i>Edit</a></td>
                                        <?php } ?>
                                    </tr>

                                    <?php
                                    $count++;
                                }
                                ?>

                            </tbody>         
                        </table>

                    <?php } else {
                        ?>
                        <center> <h3 style="color: red;">No Record Found. Please Add Payment</h3></center>
                    <?php }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
scrip();
?>

<!-- Modal -->


<div class="modal fade" id="myModal<?php echo $userId; ?>" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width: 850px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Email Notification:</h4>
                <form action="" method="post" class="form-horizontal">
                    <h5>Sent To:</h5>
                    <h5 style="float:right;"><?php echo $getUserDet->hotel_name; ?></h5>
                    <h6 style="color:gray;"><?php echo $getUserDet->email; ?></h6>
                    <input type="hidden" name="email" value="<?php echo $getUserDet->email; ?>"/>                  

            </div>
            <div class="modal-body">
                <p>Subject:</p>
                <div class = "form-group">
                    <input type = "text" name = "subject" class = "form-control" value = "Payment Reamaming Alert" readonly="" />                   
                </div>
                <p>Mail Body:</p>
                <div class = "form-group">
                    <textarea name = "email_body" class = "comment_post form-control" rows = "5" cols = "7">Type Here.. </textarea>
                </div>
                <script type = "text/javascript">
                    CKEDITOR.replace('email_body');
                </script>													

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green button-submit" name="send_email" value="Send Email"/>                      
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">                     
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>              
    </div>
</div>
