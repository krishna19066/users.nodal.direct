<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
?>

<?php
$userDetCont = new superAdminMain();
$getUserDet = $userDetCont->getUserDet($userId);

$org_stat = $getUserDet->status;

if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];
    if ($userTyp == "finance" || $userTyp == "content") {
        $blockStat = "Y";
    } elseif ($userTyp == "admin" || $userTyp == "sales" || $userTyp == "tech") {
        $blockStat = "N";
    }


    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        $statToBeUpdated1 = array("payment_recv");
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = array("content_recv", "content_upd", "verified");
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = array("approved", "cont_change", "desg_change");
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = array("live");
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = array("payment_recv", "content_recv", "content_upd", "verified", "approved", "cont_change", "desg_change", "live", "pending");
    }

    //------------------------- Status Update Options Ends ------------------------//
}

//------------------------ Update Status Starts -----------------------//

if (isset($_POST['upd_stat'])) {
    $statToBeupd = $_POST['stat'];
    $getUserDet = $userDetCont->updCustomerStat($userId, $statToBeupd);

    if ($statToBeupd == "payment_recv") {
        Header("location:/generatePDF/examples/generateBill.php?userId=" . $userId);
        exit;
    } else {
        Header("location:manage-user.php");
        exit;
    }
}
if (isset($_POST['cms_user_update'])) {
    $addOns = implode(",", $_POST['addOns']);
    $customerID;
    $property_url;

    $updUser = $userDetCont->updCMSUserDetails();
    $updUser = $userDetCont->updCMSUserDetails($customerID, $property_url, $addOns);

    // $customerID = $addUserCont->getCustomerId($_POST['user_id'], $_POST['admin_email']);
    // $insertUser = $addUserCont->addCmsUserAdmin($_POST['user_id'], $customerID, $_POST['user_pass'], $_POST['admin_name'], $_POST['admin_email'], $_POST['sections'], $_POST['address'], $_POST['hotel_name'], $_POST['phone'], $_POST['country']);
    // $mailCredential = $addUserCont->sendCredentialMail($_POST['admin_email'], $_POST['user_id'], $_POST['user_pass']);

    Header("location:manage-user.php");
    exit;
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>View Details - <?php echo $getUserDet->hotel_name; ?>
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">View User Details</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">View Details - <?php echo $getUserDet->hotel_name; ?></span>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <div class="portlet light form-fit bordered">
                                    <div class="portlet-title">
                                        <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Update Status</span> &nbsp; &nbsp;

                                        <select name="stat" style="width:200px;height:35px;border:1px solid #03A9F4;color:#607D8B;font-weight:bold;">
                                            <option value="<?php echo $org_stat; ?>"><?php echo $customerStatArr[$org_stat]; ?></option>
                                            <?php
                                            foreach ($statToBeUpdated1 as $statToBeUpdated) {
                                                $value = $customerStatArr[$statToBeUpdated];
                                                ?>
                                                <?php if ($org_stat == 'live') { ?>
                                                    <option disabled="" value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option  value="<?php echo $statToBeUpdated; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <input type="submit" name="upd_stat" style="height:35px;border:none;background:#00BCD4;color:white;" value="Update Status" <?php if ($org_stat == 'live') { ?> disabled="" <?php } ?> />
                                        <?php if ($userTyp == "tech" && $getUserDet->client_type != 'Other') { ?>

                                            <a href="addCloudDetails.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:blue;color:white;float:right;margin-left: 33px;">Add Cloud Details</button></a>
                                        <?php } ?>
<!--  <a href="viewBills.php?userId=<?php echo $userId; ?>"><button type="button" style="height:35px;border:none;background:#FF5722;color:white;float:right;">View Bills</button></a>-->
                                    </div>
                                </div>
                        </div>
                        </form>

                        <div class="col-md-12">
                            <div class="portlet box blue-hoki">

                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="" method="post" class="form-horizontal">
                                        <input type="hidden" name="customerID" value="<?php echo $getUserDet->slno; ?>">
                                        <div class="form-body">
                                            <?php if ($userTyp == "tech" || $userTyp == 'admin') { ?>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Property URL</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="property_url" class="form-control" value="<?php echo $getUserDet->property_url; ?>" />
                                                        <span class="help-block"> Property Base Url Here.. </span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Hotel Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="hotel_name" class="form-control" value="<?php echo $getUserDet->hotel_name; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> CMS User Hotel Name Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Admin Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="admin_name" class="form-control" value="<?php echo $getUserDet->admin_name; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> CMS Admin Name Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="admin_email" class="form-control" value="<?php echo $getUserDet->email; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> CMS Admin Email Here (*Note : Email ID is Unique) </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">User ID</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="user_id" class="form-control" value="<?php echo $getUserDet->user_id; ?>" readonly />
                                                    <span class="help-block"> CMS Admin User ID Here (*Note : User ID is Unique) </span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Phone Number
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="phone" class="form-control" value="<?php echo $getUserDet->phone; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> Provide your phone number </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="address" class="form-control" value="<?php echo $getUserDet->address; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> Provide your address </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">City/Town
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="city" class="form-control" value="<?php echo $getUserDet->city; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> Provide your city or town </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">State
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="state" class="form-control" value="<?php echo $getUserDet->state; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> Provide your state </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Country
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="country" class="form-control" value="<?php echo $getUserDet->country; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> Provide your country </span>
                                                </div>
                                            </div>
                                            <?php if ($userTyp == "tech" || $userTyp == 'admin') { ?>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">No. of Properties</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="propertyNo" class="form-control" value="<?php echo $getUserDet->propertyNo; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS User Properties No. Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Template Type</label>
                                                    <div class="col-md-4">
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" name="template_type" id="optionsRadios4" value="single" data-title="single" <?php
                                                                if ($getUserDet->template_type == "single") {
                                                                    echo "checked";
                                                                }
                                                                ?> <?php if ($blockStat == 'Y') { ?> disabled <?php } ?> /> Single Property
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" name="template_type" id="optionsRadios5" value="multiple" data-title="multiple" <?php
                                                                if ($getUserDet->template_type == "multiple") {
                                                                    echo "checked";
                                                                }
                                                                ?> <?php if ($blockStat == 'Y') { ?> disabled <?php } ?> /> Multiple Property
                                                                <span></span>
                                                            </label>
                                                        </div>

                                                        <span class="help-block"> CMS Admin Selected Template Type Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Template Selected</label>
                                                    <div class="col-md-9">
                                                        <div class="SelectedTemp">
                                                            <ul>
                                                                <li><input type="radio" name="template_selec" id="cb1" value="multiple1" data-title="multiple1" <?php
                                                                    if ($getUserDet->template_selected == "1" || $getUserDet->template_selected == "multiple1") {
                                                                        echo "checked";
                                                                    }
                                                                    ?> <?php if ($blockStat == 'Y') { ?> disabled <?php } ?> />
                                                                    <label for="cb1"><img src="templatesImg/MultipleTemp1.jpg" style="width:150px;height:290px;" /><br><br> Multiple Property Template 1</label>
                                                                </li>

                                                                <li><input type="radio" name="template_selec" id="cb3" value="multiple2" data-title="multiple2" <?php
                                                                    if ($getUserDet->template_selected == "3" || $getUserDet->template_selected == "multiple2") {
                                                                        echo "checked";
                                                                    }
                                                                    ?> <?php if ($blockStat == 'Y') { ?> disabled <?php } ?> />
                                                                    <label for="cb3"><img src="templatesImg/MultipleTemp2.jpg" style="width:150px;height:290px;" /><br><br> Multiple Property Template 2</label>
                                                                </li>

                                                                <li><input type="radio" name="template_selec" id="cb2" value="single1" data-title="single1" <?php
                                                                    if ($getUserDet->template_selected == "2" || $getUserDet->template_selected == "single1") {
                                                                        echo "checked";
                                                                    }
                                                                    ?> <?php if ($blockStat == 'Y') { ?> disabled <?php } ?> />
                                                                    <label for="cb2"><img src="templatesImg/SingleTemp1.jpg" style="width:150px;height:290px;" /><br><br> Single Property Template 1</label>
                                                                </li>

                                                                <li><input type="radio" name="template_selec" id="cb4" value="single2" data-title="single2" <?php
                                                                    if ($getUserDet->template_selected == "4" || $getUserDet->template_selected == "single2") {
                                                                        echo "checked";
                                                                    }
                                                                    ?> <?php if ($blockStat == 'Y') { ?> disabled <?php } ?> />
                                                                    <label for="cb4"><img src="templatesImg/SingleTemp2.jpg" style="width:150px;height:290px;" /><br><br> Single Property Template 2</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <span class="help-block"> CMS Admin Selected Template Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Domain Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="domain_name" class="form-control" value="<?php echo $getUserDet->domainName; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS Admin Domain Name Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Domain User Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="domain_user" class="form-control" value="<?php echo $getUserDet->domainUser; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS Admin Domain User ID Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Domain Password
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="domain_password" class="form-control" value="<?php echo $getUserDet->domainPass; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS Admin Domain Password Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">FTP User Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="ftp_user" class="form-control" value="<?php echo $getUserDet->FTP_user; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS Admin FTP User Name Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">FTP Password
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="ftp_password" class="form-control" value="<?php echo $getUserDet->FTP_password; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS Admin FTP Password Here.. </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Sections
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="sections" class="form-control" value="<?php echo $getUserDet->sections; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                        <span class="help-block"> CMS Admin Provided Sections Here.. </span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Set Up Cost
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="customSetupCst" class="form-control" value="<?php echo $getUserDet->setUpCost; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?> />
                                                    <span class="help-block"> CMS Admin Provided Set Up Cost Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Add On Services
                                                    <span class="required"> * </span>
                                                </label>


                                                <div class="col-md-4">
                                                    <div class="mt-checkbox-inline">
                                                        <?php
                                                        $addOnServData1 = $userDetCont->getAddonService();
                                                        $counter = 0;
                                                        $addOns = explode(",", $getUserDet->addOnServices);

                                                        foreach ($addOnServData1 as $serviceTypeRes) {
                                                            if ($counter == 0) {
                                                                ?>
                                                                <?php
                                                            }
                                                            $chk = "";
                                                            if (in_array($serviceTypeRes->addOnName, $addOns)) {
                                                                $chk = 'checked="checked"';
                                                            }
                                                            ?>

                                                            <label class="mt-checkbox">
                                                                <input type="checkbox" id="inlineCheckbox21" name="addOns[]" value="<?= $serviceTypeRes->addOnName; ?>"  <?= $chk; ?>/><?= $serviceTypeRes->addOnName; ?>
                                                                <span></span>
                                                            </label>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>                                                    
                                                    <span class="help-block"> User Add-on Services Here.. </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Instalment Type
                                                    <span class="required"> * </span>
                                                </label>

                                                <div class="col-md-4">
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="instalment_type" id="optionsRadios4" value="Monthly" data-title="Monthly" <?php if ($getUserDet->instalment_type == 'Monthly') { ?> checked <?php } ?>>Monthly
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="instalment_type" id="optionsRadios5" value="Quarterly" data-title="Quarterly" <?php if ($getUserDet->instalment_type == 'Quarterly') { ?> checked <?php } ?>>Quarterly
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="instalment_type" id="optionsRadios5" value="Yearly" data-title="Yearly" <?php if ($getUserDet->instalment_type == 'Yearly') { ?> checked <?php } ?>>Yearly
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="instalment_type" id="optionsRadios5" value="Half Yearly" data-title="Half Yearly" <?php if ($getUserDet->instalment_type == 'Half Yearly') { ?> checked <?php } ?>>Half Yearly
                                                            <span></span>
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id='forCustomSetup'>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Instalment Cost
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Instalmentcost" class="form-control" value="<?php echo $getUserDet->Instalmentcost; ?>" <?php if ($blockStat == 'Y') { ?> readonly <?php } ?>/>
                                                        <span class="help-block"> Instalment Cost Here.. </span>
                                                    </div>
                                                </div>
                                            </div>                                               
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="submit" class="btn green button-submit" name="cms_user_update" value="Update" /> 
                                                </div>
                                            </div>
                                        </div>

                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
</body>
</html>

<?php
scrip();
?>
