<?php
include "include/inc_func.php";
checkUser();

@extract($_REQUEST);
$cmsUsersCont = new superAdminMain();
?>
<?php
if (isset($_SESSION['user_type'])) {
    $userTyp = $_SESSION['user_type'];

    //------------------------ Status Update Options Starts -----------------------//

    if ($userTyp == "finance") {
        if ($cliend == 'Approved') {
            $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' ||  status='live' || status='cont_change' || status='desg_change' || status='approved')";
        } else {
            $statToBeUpdated1 = "(status='pending')";
        }
    } elseif ($userTyp == "content") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified')";
    } elseif ($userTyp == "sales") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "tech") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    } elseif ($userTyp == "admin") {
        $statToBeUpdated1 = "(status='payment_recv' || status='content_recv' || status='content_upd' || status='verified' || status='pending' || status='live' || status='cont_change' || status='desg_change' || status='approved')";
    }

    //------------------------- Status Update Options Ends ------------------------//
    if (isset($_REQUEST['search_client'])) {
        $clientName = $_REQUEST['ClientName'];
        $get_client_changes = $cmsUsersCont->getClientAllChanges($clientName);
        $resCount = count($get_client_changes);
    }
}
?>
<html>
    <head>
        <?php
        styleSheets();
        ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php top_header(); ?>

        <div class="clearfix"> </div>

        <div class="page-container">
            <?php side_menu(); ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Manage Log
                                <!--<small>statistics, charts, recent events and reports</small>-->
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Manage log</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pin font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Manage Log</span>
                                    </div>
                                </div>
                                <form action="" method="post">
                                    <div class="portlet light form-fit bordered">
                                        <div class="portlet-title">
                                            <span class="caption-subject font-red sbold uppercase" style="color:#2196F3 !important;">Select Client</span> &nbsp; &nbsp;

                                            <select name="ClientName" style="width:200px;height:35px;border:1px solid #03A9F4;color:#607D8B;font-weight:bold;">
                                                <option value="<?php if($resCount>0){ echo $clientName; }else { ?>  <?php } ?>"><?php if($resCount>0){ echo $clientName; }else { ?> Select Client <?php } ?> </option>
                                                <?php
                                                $cmsUsers = $cmsUsersCont->getCmsUsers($statToBeUpdated1);
                                                foreach ($cmsUsers as $res) {
                                                    $hotelName = $res->hotel_name;
                                                    ?>                                                  
                                                    <option  value="<?php echo $hotelName; ?>"><?php echo $hotelName; ?></option>                                               
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <input type="submit" name="search_client" style="height:35px;border:none;background:#00BCD4;color:white;" value="Search" />
                                        </div>
                                    </div>
                            </div>
                            </form>
                            <div class="portlet-body">
                                <?php 
                                if($resCount>0){ ?>
                                <div class="table-container">

                                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="2%">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th width="5%"> Sl. No. </th>
                                                <th width="10%"> Changing Date </th>
                                                <th width="10%"> Hotel Name </th>
                                                <th width="15%"> Heading </th>
                                                <th width="20%">Changes </th>                                              
                                                <th width="10%"> Action </th>

                                            </tr>
                                            <tr role="row" class="heading">
                                                <th width="2%" style="background:#dde5f9;"></th>
                                                <th width="5%" style="background:#dde5f9;"></th>
                                                <th width="10%" style="background:#dde5f9;"></th>
                                                <th width="15%" style="background:#dde5f9;"></th>
                                                <th width="20%" style="background:#dde5f9;"></th>
                                                <th width="20%" style="background:#dde5f9;"></th> 
                                                <th width="10%" style="background:#dde5f9;"></th>                            
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            <?php                                          
                                            $count = 1;
                                            foreach ($get_client_changes as $changingData) {
                                                ?>
                                                <tr role="row" class="odd">
                                                    <td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="1"><span></span></label></td>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php
                                                        $date = $changingData->timestamp;
                                                        if ($date != null) {
                                                            echo date("jS  M, Y", strtotime($date));
                                                        } else {
                                                            echo '---';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php  echo $clientName;  ?> </td>                                                      
                                                    <td style="color:blue"><?php echo $changingData->heading; ?>  </td>                                                                                                                                                               
                                                    <td><?php echo $changingData->newChanges; ?></td>                                                     
                                                    <td>
                                                        <button type="button" class="btn btn-success " data-toggle="modal" data-target="#myModal<?php echo $changingData->id; ?>">View Details</button>
                                                    </td>  
                                                    <!-- Modal -->
                                            <div class="modal fade" id="myModal<?php echo $changingData->id; ?>" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content" style="width: 850px;">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">WebSite Changes:</h4>
                                                            <h5 style="float:right; color: blue;">
                                                                <?php
                                                                $date = $changingData->timestamp;
                                                                if ($date != null) {
                                                                    echo date("jS  M, Y", strtotime($date));
                                                                }
                                                                ?>
                                                            </h5>
                                                            <h5 style="color:green;">Client Name: <?php echo $clientName; ?></h5>
                                                                                                                         
                                                        </div>
                                                        <div class="modal-body">
                                                            <p> Heading: </p>
                                                            <p><h4 style="color:gray"><?php echo $changingData->heading; ?></h4></p><hr/>
                                                            <p>New Changes:</p>
                                                            <p><?php echo $changingData->newChanges; ?></p>
                                                        </div>
                                                        <div class="modal-footer">                     
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>              
                                                </div>
                                            </div>
                                            </tr>
                                            <?php
                                            $count++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } else{ ?>
                                <center><h3>No Record Found</h3></center>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<?php
scrip();
?>


