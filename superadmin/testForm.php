<?php
	include "include/inc_func.php";
	checkUser();
	
	@extract($_REQUEST);
?>

<?php
	if(isset($_POST['cms_user_sub']))
		{
			$date = date('Y-m-d');
			$month = date('m');
			$year = date('Y');
			$addOns = implode(",",$addOns);
			$AddonServiceCost = implode(",",$AddonServiceCost);
			
			$addUserCont = new superAdminMain();
			$insertUser = $addUserCont -> addCmsUser($_POST['hotel_name'],$_POST['propertyNo'],$_POST['admin_name'],$_POST['admin_email'],$_POST['user_id'],$_POST['user_pass'],$_POST['phone'],$_POST['address'],$_POST['city'],$_POST['state'],$_POST['country'],$_POST['template_type'],$_POST['template_selec'],$_POST['domain_name'],$_POST['domain_user'],$_POST['domain_password'],$_POST['ftp_user'],$_POST['ftp_password'],$_POST['sections'],$_POST['setupCost'],$_POST['customSetupCst'],$addOns,$AddonServiceCost,$_POST['customAddonServiceCost'],$date,$month,$year);
			
			$customerID = $addUserCont -> getCustomerId($_POST['user_id'],$_POST['admin_email']);
			
			$insertUser = $addUserCont -> addCmsUserAdmin($_POST['user_id'],$customerID,$_POST['user_pass'],$_POST['admin_name'],$_POST['admin_email'],$_POST['sections'],$_POST['address'],$_POST['hotel_name'],$_POST['phone'],$_POST['country']);
			
			$mailCredential = $addUserCont -> sendCredentialMail($_POST['admin_email'],$_POST['user_id'],$_POST['user_pass']);
			
			Header("location:add-user.php");
			exit;
		}
?>
<html lang="en"> 
    <head>
        <?php
			styleSheets();
		?>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../sitepanel/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../sitepanel/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../sitepanel/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../sitepanel/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../sitepanel/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../sitepanel/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../sitepanel/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../sitepanel/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../sitepanel/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../sitepanel/assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../sitepanel/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
		<script src="../sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>		
	</head>
		<!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
       <?php top_header(); ?>
		
		<div class="clearfix"> </div>
		
		<div class="page-container">
			
			<?php side_menu(); ?>
            
			<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Users
                                <small>Add new CMS Users and their details</small>
                            </h1>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">CMS Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add User</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Form Wizard -
                                            <span class="step-title"> Step 1 of 4 </span>
                                        </span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="#" id="submit_form" method="post">
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <ul class="nav nav-pills nav-justified steps">
                                                    <li>
                                                        <a href="#tab1" data-toggle="tab" class="step">
                                                            <span class="number"> 1 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Account</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab2" data-toggle="tab" class="step">
                                                            <span class="number"> 2 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Profile</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab3" data-toggle="tab" class="step">
                                                            <span class="number"> 3 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Domain & FTP</span>
                                                        </a>
                                                    </li>
													<li>
                                                        <a href="#tab4" data-toggle="tab" class="step active">
                                                            <span class="number"> 4 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Finance</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab5" data-toggle="tab" class="step">
                                                            <span class="number"> 5 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Confirm </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div id="bar" class="progress progress-striped" role="progressbar">
                                                    <div class="progress-bar progress-bar-success"> </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="alert alert-danger display-none">
                                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                    <div class="alert alert-success display-none">
                                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                    <div class="tab-pane active" id="tab1">
                                                        <h3 class="block">Provide your account details</h3>
														
														<div class="form-group">
															<label class="col-md-3 control-label">Hotel Name</label>
															<div class="col-md-4">
																<input type="text" name="hotel_name" class="form-control" placeholder="Enter Hotel Name">
																<span class="help-block"> CMS User Hotel Name Here.. </span>
															</div>
														</div>
														
														<!--<div class="form-group">
															<label class="col-md-3 control-label">No. of Properties</label>
															<div class="col-md-4">
																<input type="text" name="propertyNo" class="form-control" placeholder="Enter No. of Properties">
																<span class="help-block"> CMS User Properties No. Here.. </span>
															</div>
														</div>-->
														
														<div class="form-group">
															<label class="col-md-3 control-label">Admin Name</label>
															<div class="col-md-4">
																<input type="text" name="admin_name" class="form-control" placeholder="Enter Admin Name">
																<span class="help-block"> CMS Admin Name Here.. </span>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-3 control-label">Email</label>
															<div class="col-md-4">
																<input type="text" name="admin_email" class="form-control" placeholder="Enter Admin Email">
																<span class="help-block"> CMS Admin Email Here (*Note : Email ID is Unique) </span>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-3 control-label">User ID</label>
															<div class="col-md-4">
																<input type="text" name="user_id" class="form-control" placeholder="Enter User ID">
																<span class="help-block"> CMS Admin User ID Here (*Note : User ID is Unique) </span>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-3 control-label"></label>
															<div class="col-md-4">
																<div class="input-group">
																	<button type="button" class="btn green" id="create_user" onclick="generateCredential();"><i class="fa fa-plus"></i> Create Password</button>
																</div>
															</div>
														</div>
														
														<div id="hide_wrap" style="display:none;">
															<div class="form-group">
																<label class="col-md-3 control-label">Password</label>
																<div class="col-md-4">
																	<div class="input-group">
																		<input id="pass_post" name="user_pass" type="text" class="form-control" value="" style="color:#0087ff;font-weight:bold;border:none;" readonly="">
																	</div>
																</div>
															</div>
														</div>
                                                    </div>
                                                   
													<div class="tab-pane" id="tab2">
                                                        <h3 class="block">Provide your profile details</h3>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Phone Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number">
                                                                <span class="help-block"> Provide your phone number </span>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Address
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="address" class="form-control" placeholder="Enter Address">
                                                                <span class="help-block"> Provide your address </span>
                                                            </div>
                                                        </div>
                                                       
														<div class="form-group">
                                                            <label class="control-label col-md-3">City/Town
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="city" class="form-control" placeholder="Enter City">
                                                                <span class="help-block"> Provide your city or town </span>
                                                            </div>
                                                        </div>
														
														 <div class="form-group">
                                                            <label class="control-label col-md-3">State
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="state" class="form-control" placeholder="Enter State">
                                                                <span class="help-block"> Provide your state </span>
                                                            </div>
                                                        </div>
														
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country</label>
                                                            <div class="col-md-4">
                                                                <select name="country" id="country_list" class="form-control">
                                                                    <option value=""></option>
                                                                    <option value="AF">Afghanistan</option>
                                                                    <option value="AL">Albania</option>
                                                                    <option value="DZ">Algeria</option>
                                                                    <option value="AS">American Samoa</option>
                                                                    <option value="AD">Andorra</option>
                                                                    <option value="AO">Angola</option>
                                                                    <option value="AI">Anguilla</option>
                                                                    <option value="AR">Argentina</option>
                                                                    <option value="AM">Armenia</option>
                                                                    <option value="AW">Aruba</option>
                                                                    <option value="AU">Australia</option>
                                                                    <option value="AT">Austria</option>
                                                                    <option value="AZ">Azerbaijan</option>
                                                                    <option value="BS">Bahamas</option>
                                                                    <option value="BH">Bahrain</option>
                                                                    <option value="BD">Bangladesh</option>
                                                                    <option value="BB">Barbados</option>
                                                                    <option value="BY">Belarus</option>
                                                                    <option value="BE">Belgium</option>
                                                                    <option value="BZ">Belize</option>
                                                                    <option value="BJ">Benin</option>
                                                                    <option value="BM">Bermuda</option>
                                                                    <option value="BT">Bhutan</option>
                                                                    <option value="BO">Bolivia</option>
                                                                    <option value="BA">Bosnia and Herzegowina</option>
                                                                    <option value="BW">Botswana</option>
                                                                    <option value="BV">Bouvet Island</option>
                                                                    <option value="BR">Brazil</option>
                                                                    <option value="IO">British Indian Ocean Territory</option>
                                                                    <option value="BN">Brunei Darussalam</option>
                                                                    <option value="BG">Bulgaria</option>
                                                                    <option value="BF">Burkina Faso</option>
                                                                    <option value="BI">Burundi</option>
                                                                    <option value="KH">Cambodia</option>
                                                                    <option value="CM">Cameroon</option>
                                                                    <option value="CA">Canada</option>
                                                                    <option value="CV">Cape Verde</option>
                                                                    <option value="KY">Cayman Islands</option>
                                                                    <option value="CF">Central African Republic</option>
                                                                    <option value="TD">Chad</option>
                                                                    <option value="CL">Chile</option>
                                                                    <option value="CN">China</option>
                                                                    <option value="CX">Christmas Island</option>
                                                                    <option value="CC">Cocos (Keeling) Islands</option>
                                                                    <option value="CO">Colombia</option>
                                                                    <option value="KM">Comoros</option>
                                                                    <option value="CG">Congo</option>
                                                                    <option value="CD">Congo, the Democratic Republic of the</option>
                                                                    <option value="CK">Cook Islands</option>
                                                                    <option value="CR">Costa Rica</option>
                                                                    <option value="CI">Cote d'Ivoire</option>
                                                                    <option value="HR">Croatia (Hrvatska)</option>
                                                                    <option value="CU">Cuba</option>
                                                                    <option value="CY">Cyprus</option>
                                                                    <option value="CZ">Czech Republic</option>
                                                                    <option value="DK">Denmark</option>
                                                                    <option value="DJ">Djibouti</option>
                                                                    <option value="DM">Dominica</option>
                                                                    <option value="DO">Dominican Republic</option>
                                                                    <option value="EC">Ecuador</option>
                                                                    <option value="EG">Egypt</option>
                                                                    <option value="SV">El Salvador</option>
                                                                    <option value="GQ">Equatorial Guinea</option>
                                                                    <option value="ER">Eritrea</option>
                                                                    <option value="EE">Estonia</option>
                                                                    <option value="ET">Ethiopia</option>
                                                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                                                    <option value="FO">Faroe Islands</option>
                                                                    <option value="FJ">Fiji</option>
                                                                    <option value="FI">Finland</option>
                                                                    <option value="FR">France</option>
                                                                    <option value="GF">French Guiana</option>
                                                                    <option value="PF">French Polynesia</option>
                                                                    <option value="TF">French Southern Territories</option>
                                                                    <option value="GA">Gabon</option>
                                                                    <option value="GM">Gambia</option>
                                                                    <option value="GE">Georgia</option>
                                                                    <option value="DE">Germany</option>
                                                                    <option value="GH">Ghana</option>
                                                                    <option value="GI">Gibraltar</option>
                                                                    <option value="GR">Greece</option>
                                                                    <option value="GL">Greenland</option>
                                                                    <option value="GD">Grenada</option>
                                                                    <option value="GP">Guadeloupe</option>
                                                                    <option value="GU">Guam</option>
                                                                    <option value="GT">Guatemala</option>
                                                                    <option value="GN">Guinea</option>
                                                                    <option value="GW">Guinea-Bissau</option>
                                                                    <option value="GY">Guyana</option>
                                                                    <option value="HT">Haiti</option>
                                                                    <option value="HM">Heard and Mc Donald Islands</option>
                                                                    <option value="VA">Holy See (Vatican City State)</option>
                                                                    <option value="HN">Honduras</option>
                                                                    <option value="HK">Hong Kong</option>
                                                                    <option value="HU">Hungary</option>
                                                                    <option value="IS">Iceland</option>
                                                                    <option value="IN">India</option>
                                                                    <option value="ID">Indonesia</option>
                                                                    <option value="IR">Iran (Islamic Republic of)</option>
                                                                    <option value="IQ">Iraq</option>
                                                                    <option value="IE">Ireland</option>
                                                                    <option value="IL">Israel</option>
                                                                    <option value="IT">Italy</option>
                                                                    <option value="JM">Jamaica</option>
                                                                    <option value="JP">Japan</option>
                                                                    <option value="JO">Jordan</option>
                                                                    <option value="KZ">Kazakhstan</option>
                                                                    <option value="KE">Kenya</option>
                                                                    <option value="KI">Kiribati</option>
                                                                    <option value="KP">Korea, Democratic People's Republic of</option>
                                                                    <option value="KR">Korea, Republic of</option>
                                                                    <option value="KW">Kuwait</option>
                                                                    <option value="KG">Kyrgyzstan</option>
                                                                    <option value="LA">Lao People's Democratic Republic</option>
                                                                    <option value="LV">Latvia</option>
                                                                    <option value="LB">Lebanon</option>
                                                                    <option value="LS">Lesotho</option>
                                                                    <option value="LR">Liberia</option>
                                                                    <option value="LY">Libyan Arab Jamahiriya</option>
                                                                    <option value="LI">Liechtenstein</option>
                                                                    <option value="LT">Lithuania</option>
                                                                    <option value="LU">Luxembourg</option>
                                                                    <option value="MO">Macau</option>
                                                                    <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                                                    <option value="MG">Madagascar</option>
                                                                    <option value="MW">Malawi</option>
                                                                    <option value="MY">Malaysia</option>
                                                                    <option value="MV">Maldives</option>
                                                                    <option value="ML">Mali</option>
                                                                    <option value="MT">Malta</option>
                                                                    <option value="MH">Marshall Islands</option>
                                                                    <option value="MQ">Martinique</option>
                                                                    <option value="MR">Mauritania</option>
                                                                    <option value="MU">Mauritius</option>
                                                                    <option value="YT">Mayotte</option>
                                                                    <option value="MX">Mexico</option>
                                                                    <option value="FM">Micronesia, Federated States of</option>
                                                                    <option value="MD">Moldova, Republic of</option>
                                                                    <option value="MC">Monaco</option>
                                                                    <option value="MN">Mongolia</option>
                                                                    <option value="MS">Montserrat</option>
                                                                    <option value="MA">Morocco</option>
                                                                    <option value="MZ">Mozambique</option>
                                                                    <option value="MM">Myanmar</option>
                                                                    <option value="NA">Namibia</option>
                                                                    <option value="NR">Nauru</option>
                                                                    <option value="NP">Nepal</option>
                                                                    <option value="NL">Netherlands</option>
                                                                    <option value="AN">Netherlands Antilles</option>
                                                                    <option value="NC">New Caledonia</option>
                                                                    <option value="NZ">New Zealand</option>
                                                                    <option value="NI">Nicaragua</option>
                                                                    <option value="NE">Niger</option>
                                                                    <option value="NG">Nigeria</option>
                                                                    <option value="NU">Niue</option>
                                                                    <option value="NF">Norfolk Island</option>
                                                                    <option value="MP">Northern Mariana Islands</option>
                                                                    <option value="NO">Norway</option>
                                                                    <option value="OM">Oman</option>
                                                                    <option value="PK">Pakistan</option>
                                                                    <option value="PW">Palau</option>
                                                                    <option value="PA">Panama</option>
                                                                    <option value="PG">Papua New Guinea</option>
                                                                    <option value="PY">Paraguay</option>
                                                                    <option value="PE">Peru</option>
                                                                    <option value="PH">Philippines</option>
                                                                    <option value="PN">Pitcairn</option>
                                                                    <option value="PL">Poland</option>
                                                                    <option value="PT">Portugal</option>
                                                                    <option value="PR">Puerto Rico</option>
                                                                    <option value="QA">Qatar</option>
                                                                    <option value="RE">Reunion</option>
                                                                    <option value="RO">Romania</option>
                                                                    <option value="RU">Russian Federation</option>
                                                                    <option value="RW">Rwanda</option>
                                                                    <option value="KN">Saint Kitts and Nevis</option>
                                                                    <option value="LC">Saint LUCIA</option>
                                                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                                                    <option value="WS">Samoa</option>
                                                                    <option value="SM">San Marino</option>
                                                                    <option value="ST">Sao Tome and Principe</option>
                                                                    <option value="SA">Saudi Arabia</option>
                                                                    <option value="SN">Senegal</option>
                                                                    <option value="SC">Seychelles</option>
                                                                    <option value="SL">Sierra Leone</option>
                                                                    <option value="SG">Singapore</option>
                                                                    <option value="SK">Slovakia (Slovak Republic)</option>
                                                                    <option value="SI">Slovenia</option>
                                                                    <option value="SB">Solomon Islands</option>
                                                                    <option value="SO">Somalia</option>
                                                                    <option value="ZA">South Africa</option>
                                                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                                    <option value="ES">Spain</option>
                                                                    <option value="LK">Sri Lanka</option>
                                                                    <option value="SH">St. Helena</option>
                                                                    <option value="PM">St. Pierre and Miquelon</option>
                                                                    <option value="SD">Sudan</option>
                                                                    <option value="SR">Suriname</option>
                                                                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                                    <option value="SZ">Swaziland</option>
                                                                    <option value="SE">Sweden</option>
                                                                    <option value="CH">Switzerland</option>
                                                                    <option value="SY">Syrian Arab Republic</option>
                                                                    <option value="TW">Taiwan, Province of China</option>
                                                                    <option value="TJ">Tajikistan</option>
                                                                    <option value="TZ">Tanzania, United Republic of</option>
                                                                    <option value="TH">Thailand</option>
                                                                    <option value="TG">Togo</option>
                                                                    <option value="TK">Tokelau</option>
                                                                    <option value="TO">Tonga</option>
                                                                    <option value="TT">Trinidad and Tobago</option>
                                                                    <option value="TN">Tunisia</option>
                                                                    <option value="TR">Turkey</option>
                                                                    <option value="TM">Turkmenistan</option>
                                                                    <option value="TC">Turks and Caicos Islands</option>
                                                                    <option value="TV">Tuvalu</option>
                                                                    <option value="UG">Uganda</option>
                                                                    <option value="UA">Ukraine</option>
                                                                    <option value="AE">United Arab Emirates</option>
                                                                    <option value="GB">United Kingdom</option>
                                                                    <option value="US">United States</option>
                                                                    <option value="UM">United States Minor Outlying Islands</option>
                                                                    <option value="UY">Uruguay</option>
                                                                    <option value="UZ">Uzbekistan</option>
                                                                    <option value="VU">Vanuatu</option>
                                                                    <option value="VE">Venezuela</option>
                                                                    <option value="VN">Viet Nam</option>
                                                                    <option value="VG">Virgin Islands (British)</option>
                                                                    <option value="VI">Virgin Islands (U.S.)</option>
                                                                    <option value="WF">Wallis and Futuna Islands</option>
                                                                    <option value="EH">Western Sahara</option>
                                                                    <option value="YE">Yemen</option>
                                                                    <option value="ZM">Zambia</option>
                                                                    <option value="ZW">Zimbabwe</option>
                                                                </select>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
															<label class="col-md-3 control-label">Template Type</label>
															<div class="col-md-4">
																<div class="mt-radio-inline">
																	<label class="mt-radio">
																		<input type="radio" name="template_type" id="optionsRadios4" value="single" data-title="single"> Single Property
																		<span></span>
																	</label>
																	<label class="mt-radio">
																		<input type="radio" name="template_type" id="optionsRadios5" value="multiple" data-title="multiple"> Multiple Property
																		<span></span>
																	</label>
																</div>
															
																<span class="help-block"> CMS Admin Selected Template Type Here.. </span>
															</div>
														</div>
														
														<div id='forMultiple' style='display:none'>
															<div class="form-group">
																<label class="col-md-3 control-label">Template Selected</label>
																<div class="col-md-9">
																	<div class="SelectedTemp">
																		<ul>
																			<li><input type="radio" name="template_selec" id="cb1" value="multiple1" data-title="multiple1" />
																				<label for="cb1"><img src="templatesImg/MultipleTemp1.jpg" style="width:150px;height:290px;" /><br><br> Multiple Property Template 1</label>
																			</li>
																		  
																			<li><input type="radio" name="template_selec" id="cb3" value="multiple2" data-title="multiple2" />
																				<label for="cb3"><img src="templatesImg/MultipleTemp2.jpg" style="width:150px;height:290px;" /><br><br> Multiple Property Template 2</label>
																			</li>
																		</ul>
																	</div>
																	<span class="help-block"> CMS Admin Selected Template Here.. </span>
																</div>
															</div>
														</div>
														
														<div id='forSingle' style='display:none'>
															<div class="form-group">
																<label class="col-md-3 control-label">Template Selected</label>
																<div class="col-md-9">
																	<div class="SelectedTemp">
																		<ul>
																			<li><input type="radio" name="template_selec" id="cb2" value="single1" data-title="single1" />
																				<label for="cb2"><img src="templatesImg/SingleTemp1.jpg" style="width:150px;height:290px;" /><br><br> Single Property Template 1</label>
																			</li>
																		  
																			<li><input type="radio" name="template_selec" id="cb4" value="single2" data-title="single2" />
																				<label for="cb4"><img src="templatesImg/SingleTemp2.jpg" style="width:150px;height:290px;" /><br><br> Single Property Template 2</label>
																			</li>
																		</ul>
																	</div>
																	<span class="help-block"> CMS Admin Selected Template Here.. </span>
																</div>
															</div>
														</div>
                                                    </div>
                                                    
													<div class="tab-pane" id="tab3">
                                                        <h3 class="block">Provide your Domain and FTP details</h3>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Domain Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="domain_name" class="form-control" placeholder="Enter Domain Name">
                                                                <span class="help-block"> CMS Admin Domain Name Here.. </span>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Domain User Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="domain_user" class="form-control" placeholder="Enter Domain User ID">
																<span class="help-block"> CMS Admin Domain User ID Here.. </span>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Domain Password
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="domain_password" class="form-control" placeholder="Enter Domain Password">
																<span class="help-block"> CMS Admin Domain Password Here.. </span>
                                                            </div>
                                                        </div>
                                                       
														<div class="form-group">
                                                            <label class="control-label col-md-3">FTP User Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
																<input type="text" name="ftp_user" class="form-control" placeholder="Enter FTP User Name">
																<span class="help-block"> CMS Admin FTP User Name Here.. </span>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">FTP Password
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="ftp_password" class="form-control" placeholder="Enter FTP Password">
																<span class="help-block"> CMS Admin FTP Password Here.. </span>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Sections
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="sections" class="form-control" placeholder="Enter Section Name">
																<span class="help-block"> CMS Admin Provided Sections Here.. </span>
                                                            </div>
                                                        </div>
                                                    </div>
													
													<div class="tab-pane" id="tab4">
                                                        <h3 class="block">Provide your Financial details</h3>
                                                        
														<div class="form-group">
															<label class="col-md-3 control-label">Set Up Cost</label>
															<div class="col-md-4">
																<div class="mt-radio-inline">
																	<label class="mt-radio">
																		<input type="radio" name="setupCost" value="fixed"> Fixed
																		<span></span>
																	</label>
																	<label class="mt-radio">
																		<input type="radio" name="setupCost" id="customSetUp" value="custom"> Custom
																		<span></span>
																	</label>
																</div>
															
																<span class="help-block"> CMS Admin Selected Template Type Here.. </span>
															</div>
														</div>
														
														<div id='forCustomSetup' style='display:none'>
															<div class="form-group">
																<label class="control-label col-md-3">Custom Set Up Cost
																	<span class="required"> * </span>
																</label>
																<div class="col-md-4">
																	<input type="text" name="customSetupCst" class="form-control" placeholder="Enter Set Up Cost">
																	<span class="help-block"> CMS Admin Provided Custom Set Up Cost Here.. </span>
																</div>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-3 control-label"></label>
															<div class="col-md-4">
																<h3>Add On Services</h3>
															</div>
														</div>
														
														<div class="expan-service-main">
															<div class="expan-service">
																<div class="form-group">
																	<label class="col-md-3 control-label">Select a Service</label>
																	<div class="col-md-4">
																		<select name="addOns[]" id="addOnsServ" class="form-control" onchange="if(this.value==''){$(this).parent().parent().next().fadeOut()} else { $(this).parent().parent().next().fadeIn()}">
																			<option value="">Select</option>
																			<option value="GMB">GMB</option>
																			<option value="forms">Forms Creation</option>
																			<option value="resavenue">Res Avenue</option>
																		</select>
																	</div>
																</div>
																
																<div id="addonDetails" class="addonDetails" style="display:none;">
																	<div class="form-group">
																		<label class="col-md-3 control-label">Add on Service Cost</label>
																		<div class="col-md-4">
																			<div class="mt-checkbox-inline">
																				<label class="mt-checkbox">
																					<input type="checkbox" name="AddonServiceCost[]" value="fixed"> Fixed
																					<span></span>
																				</label>
																				<label class="mt-checkbox">
																					<input type="checkbox" name="AddonServiceCost[]" id="customAddonServiceSetup" value="custom"
																					onchange="$(this).parent().parent().parent().parent().next().fadeToggle();"> Custom
																					<span></span>
																				</label>
																			</div>
																		
																			<span class="help-block"> Add on Service Cost Type Here.. </span>
																		</div>
																	</div>
																	
																	<div id='forAddonServiceSetp' style='display:none'>
																		<div class="form-group">
																			<label class="control-label col-md-3">Custom Add on Service Cost
																				<span class="required"> * </span>
																			</label>
																			<div class="col-md-4">
																				<input type="text" name="customAddonServiceCost" class="form-control" placeholder="Enter Set Up Cost">
																				<span class="help-block"> CMS Admin Provided Custom Add on Service Cost Here.. </span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<button class="btn blue add-more-services" style="margin-right: 70px;" type="button">Add Service</button>
													</div>
														
                                                    <div class="tab-pane" id="tab5">
                                                        <h3 class="block">Confirm your account</h3>
                                                        <h4 class="form-section">Account</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Hotel Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="hotel_name"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Admin Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="admin_name"> </p>
                                                            </div>
                                                        </div>
														
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Email:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="admin_email"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">User ID:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="user_id"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Password:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="user_pass"> </p>
                                                            </div>
                                                        </div>
                                                       
														<h4 class="form-section">Profile</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Phone Number:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="phone"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Address:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="address"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">City/Town:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="city"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">State:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="state"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Country:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="country"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Template Type:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="template_type"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Template Selected:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="template_selec"> </p>
                                                            </div>
                                                        </div>
                                                        
														
														<h4 class="form-section">FTP & Domain</h4>
														<div class="form-group">
                                                            <label class="control-label col-md-3">Domain Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="domain_name"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">Domain User Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="domain_user"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Domain Password:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="domain_password"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">FTP User Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="ftp_user"> </p>
                                                            </div>
                                                        </div>
                                                        
														<div class="form-group">
                                                            <label class="control-label col-md-3">FTP Password:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="ftp_password"> </p>
                                                            </div>
                                                        </div>
														
														<div class="form-group">
                                                            <label class="control-label col-md-3">Sections:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="sections"> </p>
                                                            </div>
                                                        </div>
														
														
														<h4 class="form-section">Billing</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Set Up Cost:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="setupCost"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Custom Set Up Cost:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="customSetupCst"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Add On Services:</label>
															<div class="col-md-4">
                                                                <p class="form-control-static" data-display="addOns[]"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Add on Service Cost:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="AddonServiceCost[]"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Custom Add on Service Cost:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="customAddonServiceCost"> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="javascript:;" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back </a>
                                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>
                                                        <input type="submit" class="btn green button-submit" name="cms_user_sub" value="Submit" /> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
        </div>
		
		
        <div class="quick-nav-overlay"></div>
       
        <!-- BEGIN CORE PLUGINS -->
        <script src="../sitepanel/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../sitepanel/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../sitepanel/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../sitepanel/assets/pages/scripts/form-wizard.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../sitepanel/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="../sitepanel/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
		<script>
			$(document).ready(function(){
				$('.portlet-body form').on('change', '[type="checkbox"]', function() {
					$(this).siblings().prop('checked', false);
				});
			})
		</script> 

		<script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
		
		<script>
			function generateCredential()
			{
				// alert(gen_cms_pass);
				$.ajax({ url: 'ajax.php',
				 data: {gen_cms_pass:'Deepanshu'},
				 type: 'post',
				 success: function(output) {
						// alert(output);
							var re_data = output.split("~");
							$('#pass_post').val(re_data[1]);
							$('#hide_wrap').fadeIn(1000);
						  }
					});
				
			}
		</script>
		
		<script>
			var expncnt = 0;
			 $(document).ready(function() {
				 expncnt++;
			  var addMore = '<div class="expan-service">';
			  addMore += $('.expan-service').html();
			  addMore += '</div>';

			  $('.add-more-services').click(function(){
				$('.expan-service-main').append(addMore)
			  });
			 })
		</script>

		<script>
				$('input[name=template_type]').click(function () {
				if (this.id == "optionsRadios4") {
					$("#forSingle").show('slow');
				} else {
					$("#forSingle").hide('slow');
				}
			});
			
			$('input[name=setupCost]').click(function () {
				if (this.id == "customSetUp") {
					$("#forCustomSetup").show('slow');
				} 
				else {
					$("#forCustomSetup").hide('slow');
				}
			});
			
			
		</script>

		<script>
				$('input[name=template_type]').click(function () {
				if (this.id == "optionsRadios5") {
					$("#forMultiple").show('slow');
				} else {
					$("#forMultiple").hide('slow');
				}
			});
		</script>
    
	</body>
</html>