<?php
	$foodCont = new staticPageData($filename);
	$food1 = $foodCont -> foodData($filename,'2','order by s_no asc limit 1');
	$food = $food1[0];
?>

	<section class="contctTop clear">
		<div class="imgCtnrFood"></div>
		<div class="contctMsg">
			<span class="contctMsgHtxt">Food-Beverages</span>
		</div>
	</section>
	
	<section class="TextTwoSec fram_width clear">
		<div class="btmDsgn">
			<h2><b><?php echo html_entity_decode($food->h1); ?></b></h2>
			<span><?php echo html_entity_decode($food->subhead1); ?></span>
		</div>
	</section><br>

	<section class="cardFxCntr">
		<div class="fram_width setComn">
			<div class='cardFx'>
				<div class="flipr">
					<div class="circle_icon theme_bg_color"> <?php echo html_entity_decode($food->box1icon); ?> </div>
					<div class="cardFxTxt">
						<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($food->box1head); ?></p>
						<p class="fa_card_text"><?php echo html_entity_decode($food->box1content); ?></p>
					</div>
				</div>
			</div>

			<div class='cardFx'>
				<div class="flipr">
					<div class="circle_icon theme_bg_color"> <?php echo html_entity_decode($food->box2icon); ?> </div>
					<div class="cardFxTxt">
						<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($food->box2head); ?></p>
						<p class="fa_card_text"><?php echo html_entity_decode($food->box2content); ?></p>
					</div>
				</div>
			</div>
				
			<div class='cardFx'>
				<div class="flipr">
					<div class="circle_icon theme_bg_color"> <?php echo html_entity_decode($food->box3icon); ?> </div>
					<div class="cardFxTxt">
						<p class="theme_text_color text_strach text_size_fa_card"><?php echo html_entity_decode($food->box3head); ?></p>
						<p class="fa_card_text"><?php echo html_entity_decode($food->box3content); ?></p>
					</div>
				</div>
			</div>						
		</div>
	</section>	
	
	<section class="TextTwoSec fram_width clear">
		<div class="btmDsgn">
			<h2><b><?php echo html_entity_decode($food->h2); ?></b></h2>
			<span><?php echo html_entity_decode($food->subhead2); ?></span>
			<h5 style="margin-bottom:0;"></h5>		
		</div>
	</section>
	
	<section  class="AbtTxtCrd  clear">
		<div class="fram_width AbtTxtCrd">
			<?php
				$qacn = 1;
				$qarow1 = $foodCont -> foodData($filename,'2','order by s_no asc');
				$qanumb = count($qarow1);
				foreach($qarow1 as $qarow)
					{
			?>
						<div class="AbttxtCard" style="width:50%;">
							<div class="H70">
								<h3 class="mrgn0"><?php echo html_entity_decode($qarow->question); ?></h3>
							</div>			
							
							<div class="Abttext"><?php echo html_entity_decode($qarow->answer); ?></div>
						</div>
			<?php
						if($qacn%2==0)
							{
								if($qacn==$qanumb)
									{}
								else
									{
										echo '</div></section><section  class="AbtTxtCrd  clear"><div class="fram_width AbtTxtCrd">';
									}
							}
						$qacn++;
					}
			?>
		</div>
	</section>	
	<br><br>
	
	<h2 class="fram_width txt_center">View Our Menu</h2>	
	<section class="aside">
		<div class="fram_width aside">
			<div class="BstTwoSec">
				<img src="images/foodMenu.png" alt="The Perch Food Menu">
			</div>
		</div>
	</section>
	<br><br>


	<?php
		desktopFooter();
	?>
	</body>
</html>