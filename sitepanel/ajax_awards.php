<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$staticPageCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$propertyId = $_REQUEST['propID'];
$PropertyList = $ajaxpropertyPhotoCont->getCityList($customerId); /// Get Property data
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId); /// Get Cloud Details
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
$property_data = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
/*
  date_default_timezone_set("Asia/Kolkata");

  require 'Cloudinary.php';
  require 'Uploader.php';
  require 'Api.php';

  Cloudinary::config(array(
  "cloud_name" => $cloud_cdnName,
  "api_key" => $cloud_cdnKey,
  "api_secret" => $cloud_cdnSecret
  ));

  if (isset($_POST['update_testimonial'])) {
  $recordID = $_POST['recordID'];
  $imagehead = $_POST['imagehead'];
  $file = $_FILES['image_org']['imagehead'];

  $timdat = date('Y-m-d');
  $timtim = date('H-i-s');
  $timestmp = $timdat . "_" . $timtim;
  // echo $timestmp;
  // die;
  //echo $alt_tag."<br>";
  $roo_id = "0";

  if ($_FILES['image_org']['imagehead'] != "") {
  $selimgs = "select awardphoto from awards where slno='$recordID'";
  $selimgsquer = mysql_query($selimgs);
  $selimgsdata = mysql_fetch_array($selimgsquer);
  $photo_path = $selimgsdata['awardphoto'];

  $imact = "reputize/awards/" . $photo_path;
  \Cloudinary\Uploader::destroy($imact, array("invalidate" => TRUE));

  \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/awards"));

  $cond = ",awardphoto='$timestmp'";
  }

  // $ins = "update awards set imagehead='$imagehead' $cond where customerID='$custID' and slno='$recordID'";
  // $quer = mysql_query($ins);

  Header("location:add-award.php");
  exit;
  }
  if (isset($_POST['submit_testimonial'])) {
  $recordID = $_POST['recordID'];
  $imagehead = $_POST['imagehead'];
  $file = $_FILES['image_org']['imagehead'];

  $timdat = date('Y-m-d');
  $timtim = date('H-i-s');
  $timestmp = $timdat . "_" . $timtim;
  // echo $timestmp;
  // die;
  //echo $alt_tag."<br>";
  $roo_id = "0";

  \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/awards"));

  //$ins = "insert into awards(customerID,imagehead,awardphoto) values('$custID','$imagehead','$timestmp')";
  // $quer = mysql_query($ins);

  Header("location:edit-awards.php");
  exit;
  }
 * 
 */
$awardData = $ajaxpropertyPhotoCont->GetAwardListData($customerId, $propertyId);
//$qry1 = "select * from awards where customerID='$custID' order by slno desc";
$reccnt = count($awardData);
$qry = $awardData[0];
?>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                             <h1> PROPERTY - <span style="color:#e44787;"><?php echo $propertyName; ?></span></h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View / Add Property Awards Photo</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body ">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Awards Photo" /></span>

                                        <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>

                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                    <div class="portlet light bordered" id="add_photo" style="display:none;">			
                        <div class="portlet-body form">
                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php">
                                <div class="form-body">
                                    <h4 align="center" style="color:#E26A6A;"><b> Add New Award Photo </b></h3><br><br>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Awards Image
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="image_org"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="form-group">
                                                <label class="control-label col-md-3"> Property Images- Original Image
                                                        <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                        <input name="image_org" type="file" class="form-control" />
                                                        <span class="help-block"> Provide your original image</span>
                                                </div>
                                        </div>-->

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Image Alt Tag
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input name="imageAlt1" type="text" class="form-control" />
                                                <span class="help-block"> Provide your image alt tag</span>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input name="recordID" type="hidden" value="<?php echo $propertyId ?>" />
                                                    <input type="submit" name="submit_award_pic" class="btn green" id="submit" value="Add Photo">
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->

                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row">
                                            <?php
                                            // $propPhotoListdata = $ajaxpropertyPhotoCont->getPropertyPhoto($propertyId);
                                            if ($reccnt > 0) {
                                                foreach ($awardData as $awardPhotoList) {
                                                    ?>
                                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                                        <div class=" mt-overlay-1">
                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_564,h_400,c_fill/reputize/awards/<?php echo $awardPhotoList->awardphoto; ?>.jpg" />

                                                            <div class="mt-overlay">
                                                                <ul class="mt-info">
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="javascript:;">
                                                                            <i class="icon-magnifier"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="javascript:;">
                                                                            <i class="icon-link"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <h3> <center> No Record Found.....</center></h3>
                                            <?php }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>             
            <div class="quick-nav-overlay"></div>
            <!-- BEGIN CORE PLUGINS -->
           <?php echo ajaxJsFooter(); ?>
    </body>
</html>