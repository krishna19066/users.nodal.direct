<?php

include "/home/hawthorntech/public_html/stayondiscount.com/sitepanel/admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$analyticsCont = new analyticsPage();
$propertyinsert = new propertyData();
$clientPageData = $ajaxclientCont->getHomePageData($customerId);

if (isset($_POST['whyperchsub'])) {

    $cloud_keySel = $propertyinsert->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));


    $type = $_POST['savecont'];

    $h1 = $_POST['h1'];

    $pageUrl = $_POST['pageUrl'];

    $boxicon1 = $_POST['boxicon1'];
    $boxhead1 = $_POST['boxhead1'];
    $boxcontent1 = $_POST['boxcontent1'];
    $boxicon2 = $_POST['boxicon2'];
    $boxhead2 = $_POST['boxhead2'];
    $boxcontent2 = $_POST['boxcontent2'];
    $boxicon3 = $_POST['boxicon3'];
    $boxhead3 = $_POST['boxhead3'];
    $boxcontent3 = $_POST['boxcontent3'];

    $h2 = $_POST['h2'];
    $subhead2 = $_POST['subhead2'];
    $imgboxh1 = $_POST['imgboxh1'];
    $imgboximg1 = $_POST['imgboximg1'];
    $imgboxcontent1 = $_POST['imgboxcontent1'];
    $imgboxh2 = $_POST['imgboxh2'];
    $imgboximg2 = $_POST['imgboximg2'];
    $imgboxcontent2 = $_POST['imgboxcontent2'];

    $imgboxh3 = $_POST['imgboxh3'];
    $imgboximg3 = $_POST['imgboximg3'];
    $imgboxcontent3 = $_POST['imgboxcontent3'];
    $imgboxh4 = $_POST['imgboxh4'];
    $imgboximg4 = $_POST['imgboximg4'];
    $imgboxcontent4 = $_POST['imgboxcontent4'];
    $imgboxh5 = $_POST['imgboxh5'];
    $imgboximg5 = $_POST['imgboximg5'];
    $imgboxcontent5 = $_POST['imgboxcontent5'];
    $imgboxh6 = $_POST['imgboxh6'];
    $imgboximg6 = $_POST['imgboximg6'];
    $imgboxcontent6 = $_POST['imgboxcontent6'];
    $imgboxh7 = $_POST['imgboxh7'];
    $imgboximg7 = $_POST['imgboximg7'];
    $imgboxcontent7 = $_POST['imgboxcontent7'];
    $imgboxh8 = $_POST['imgboxh8'];
    $imgboximg8 = $_POST['imgboximg8'];
    $imgboxcontent8 = $_POST['imgboxcontent8'];
    $imgboxh9 = $_POST['imgboxh9'];
    $imgboximg9 = $_POST['imgboximg9'];
    $imgboxcontent9 = $_POST['imgboxcontent9'];
    $imgboxh10 = $_POST['imgboxh10'];
    $imgboximg10 = $_POST['imgboximg10'];
    $imgboxcontent10 = $_POST['imgboxcontent10'];
    $imgboxh11 = $_POST['imgboxh11'];
    $imgboximg11 = $_POST['imgboximg11'];
    $imgboxcontent11 = $_POST['imgboxcontent11'];
    $imgboxh12 = $_POST['imgboxh12'];
    $imgboximg12 = $_POST['imgboximg12'];
    $imgboxcontent12 = $_POST['imgboxcontent12'];

    //------------------------------Image Manipulation ------------------------------------------->

    if ($_FILES[imgboximg1][name] != "") {
        $nam = $_FILES[imgboximg1][name];
        $img_name = "whyperchimg1-" . date("Y_m_d_H_i_s");
        // $product_image1 = upload_file($img_name, "imgboximg1", "images");
        $cond1 = ",imgbox_img1='$img_name'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg1"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/whyperch"));
    }

    if ($_FILES[imgboximg2][name] != "") {
        $nam1 = $_FILES[imgboximg2][name];
        $img_name2 = "whyperchimg2-" . date("Y_m_d_H_i_s");
        //$product_image2 = upload_file($img_name, "imgboximg2", "images");
        $cond2 = ",imgbox_img2='$img_name2'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg2"]["tmp_name"], array("public_id" => $img_name2, "folder" => "reputize/whyperch"));
    }

    if ($_FILES[imgboximg3][name] != "") {
        $nam1 = $_FILES[imgboximg3][name];
        $img_name3 = "whyperchimg3-" . date("Y_m_d_H_i_s");
        // $product_image3 = upload_file($img_name, "imgboximg3", "images");
        $cond3 = ",imgbox_img3='$img_name3'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg3"]["tmp_name"], array("public_id" => $img_name3, "folder" => "reputize/whyperch"));
    }

    if ($_FILES[imgboximg4][name] != "") {
        $nam1 = $_FILES[imgboximg4][name];
        $img_name4 = "whyperchimg4-" . date("Y_m_d_H_i_s");
        // $product_image4 = upload_file($img_name, "imgboximg4", "images");
        $cond4 = ",imgbox_img4='$img_name4'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg4"]["tmp_name"], array("public_id" => $img_name4, "folder" => "reputize/whyperch"));
    }

    if ($_FILES[imgboximg5][name] != "") {
        $nam1 = $_FILES[imgboximg5][name];
        $img_name5 = "whyperchimg5-" . date("Y_m_d_H_i_s");
        // $product_image5 = upload_file($img_name, "imgboximg5", "images");
        $cond5 = ",imgbox_img5='$img_name5'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg5"]["tmp_name"], array("public_id" => $img_name5, "folder" => "reputize/whyperch"));
    }
    if ($_FILES[imgboximg6][name] != "") {
        $nam1 = $_FILES[imgboximg6][name];
        $img_name6 = "whyperchimg6-" . date("Y_m_d_H_i_s");
        // $product_image6 = upload_file($img_name, "imgboximg6", "images");
        $cond6 = ",imgbox_img6='$img_name6'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg6"]["tmp_name"], array("public_id" => $img_name6, "folder" => "reputize/whyperch"));
    }
    if ($_FILES[imgboximg7][name] != "") {
        $nam1 = $_FILES[imgboximg7][name];
        $img_name7 = "whyperchimg7-" . date("Y_m_d_H_i_s");
        // $product_image7 = upload_file($img_name, "imgboximg7", "images");
        $cond7 = ",imgbox_img7='$img_name7'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg7"]["tmp_name"], array("public_id" => $img_name7, "folder" => "reputize/whyperch"));
    }
    if ($_FILES[imgboximg8][name] != "") {
        $nam1 = $_FILES[imgboximg8][name];
        $img_name8 = "whyperchimg8-" . date("Y_m_d_H_i_s");
        //  $product_image8 = upload_file($img_name, "imgboximg8", "images");
        $cond8 = ",imgbox_img8='$img_name8'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg8"]["tmp_name"], array("public_id" => $img_name8, "folder" => "reputize/whyperch"));
    }
    if ($_FILES[imgboximg9][name] != "") {
        $img_name9 = "whyperchimg8-" . date("Y_m_d_H_i_s");
        $cond9 = ",imgbox_img9='$img_name9'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg9"]["tmp_name"], array("public_id" => $img_name9, "folder" => "reputize/whyperch"));
    }
    if ($_FILES[imgboximg10][name] != "") {
        $img_name10 = "whyperchimg10-" . date("Y_m_d_H_i_s");
        //  $product_image8 = upload_file($img_name, "imgboximg8", "images");
        $cond10 = ",imgbox_img10='$img_name10'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg10"]["tmp_name"], array("public_id" => $img_name10, "folder" => "reputize/whyperch"));
    }
     if ($_FILES[imgboximg11][name] != "") {
        $img_name11 = "whyperchimg11-" . date("Y_m_d_H_i_s");
        //  $product_image8 = upload_file($img_name, "imgboximg8", "images");
        $cond11 = ",imgbox_img11='$img_name11'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg11"]["tmp_name"], array("public_id" => $img_name11, "folder" => "reputize/whyperch"));
    }
     if ($_FILES[imgboximg12][name] != "") {
        $img_name12 = "whyperchimg12-" . date("Y_m_d_H_i_s");
        //  $product_image8 = upload_file($img_name, "imgboximg8", "images");
        $cond12 = ",imgbox_img12='$img_name12'";
        \Cloudinary\Uploader::upload($_FILES["imgboximg12"]["tmp_name"], array("public_id" => $img_name12, "folder" => "reputize/whyperch"));
    }
    if ($_FILES[image_header][name] != "") {
        $img_header1 = "gallery-header" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image_header"]["tmp_name"], array("public_id" => $img_header1, "folder" => "reputize/staticpage"));
        $img_header = ",header_img='$img_header1'";
    }
    //------------------------------Image Manipulation ------------------------------------------->
        $meta_title = $_REQUEST['h1'] . ',' . $_REQUEST['h2'];
        $filename = $_REQUEST['page_url'];
        $dec1 = $_REQUEST['heading_content'];
        $dec = substr($dec1, 0, 250);
        $date = date('Y-m-d');

    if ($type == "Add Content") {
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        $inscont = $ajaxclientCont->InsertWhyUsPage($customerId, $pageUrl, $h1, $boxicon1, $boxhead1, $boxcontent1, $boxicon2, $boxhead2, $boxcontent2, $boxicon3, $boxhead3, $boxcontent3, $h2, $subhead2, $imgboxh1, $imgboxcontent1, $imgboxh2, $imgboxcontent2, $imgboxh3, $imgboxcontent3, $imgboxh4, $imgboxcontent4, $imgboxh5, $imgboxcontent5, $imgboxh6, $imgboxcontent6, $imgboxh7, $imgboxcontent7, $imgboxh8, $imgboxcontent8, $imgboxh9, $imgboxcontent9, $imgboxh10, $imgboxcontent10,$imgboxh11, $imgboxcontent11,$imgboxh12, $imgboxcontent12, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $cond9, $cond10,$cond11,$cond12,$img_header);
        // $inscont = "insert into static_page_whyperch(customerID,h1,box1_icon,box1_head,box1_content,box2_icon,box2_head,box2_content,box3_icon,box3_head,box3_content,h2,subhead2,imgbox_h1,imgbox_content1,imgbox_h2,imgbox_content2) values('$custID','$h1','$boxicon1','$boxhead1','$boxcontent1','$boxicon2','$boxhead2','$boxcontent2','$boxicon3','$boxhead3','$boxcontent3','$h2','$subhead2','$imgboxh1','$imgboxcontent1','$imgboxh2','$imgboxcontent2')";
        // $inscontquery = mysql_query($inscont);       
        echo '<script type="text/javascript">
                alert("Succesfuly Inserted Data");              
window.location = "manage-static-pages.php";
            </script>';
        exit();
    } elseif ($type == "Save Content") {      
        $updContent = $ajaxclientCont->UpdateWhyUsMain($customerId, $h1, $boxicon1, $boxhead1, $boxcontent1, $boxicon2, $boxhead2, $boxcontent2, $boxicon3, $boxhead3, $boxcontent3, $h2, $subhead2, $imgboxh1, $imgboxcontent1, $imgboxh2, $imgboxcontent2, $imgboxh3, $imgboxcontent3, $imgboxh4, $imgboxcontent4, $imgboxh5, $imgboxcontent5, $imgboxh6, $imgboxcontent6, $imgboxh7, $imgboxcontent7, $imgboxh8, $imgboxcontent8, $imgboxh9, $imgboxcontent9, $imgboxh10, $imgboxcontent10,$imgboxh11, $imgboxcontent11,$imgboxh12, $imgboxcontent12, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $cond9, $cond10,$cond11,$cond11,$img_header,$pageUrl);
        //$inscontentbox = "update static_page_whyperch set imgbox_h1='$imgboxh1',imgbox_content1='$imgboxcontent1',imgbox_h2='$imgboxh2',imgbox_content2='$imgboxcontent2', imgbox_h3='$imgboxh3',imgbox_content3='$imgboxcontent3',imgbox_h4='$imgboxh4',imgbox_content4='$imgboxcontent4', imgbox_h5='$imgboxh5',imgbox_content5='$imgboxcontent5', imgbox_h6='$imgboxh6',imgbox_content6='$imgboxcontent6', imgbox_h7='$imgboxh7',imgbox_content7='$imgboxcontent7', imgbox_h8='$imgboxh8',imgbox_content8='$imgboxcontent8', $cond3 $cond4 $cond5 $cond6  $cond7 $cond8  where customerID='$custID'";
        //$inscontentboxquery = mysql_query($inscontentbox);
        
       echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "manage-static-pages.php";
          </script>';
       exit();
    }
}?>