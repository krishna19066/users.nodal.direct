<?php
$roomTypeID = $_REQUEST['roomTypeID'];
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$roomType = $propertyCont->GetRoomTypeWithID($roomTypeID);
$res = $roomType[0];
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Room Type
                                <small>Update Room Type here..</small>
                            </h1>
                        </div>                    
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property-settings.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">(Edit Room Type)</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                                <a class="pull-right">
                                                    <a href="manage-property-settings.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-eye"></i> &nbsp View All City List</button></a>                                               
                                                    <a href="ajax_add-roomtype.php"> <button style="background:#3683d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; "><i class="fa fa-plus"></i> &nbsp Add Room Type</button></a>
                                                    <a href="ajax_add-city.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; margin-right: 21px;"><i class="fa fa-plus"></i> &nbsp Add City</button></a>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="../sitepanel/add_property.php" id="submit_form" method="post">
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <div class="tab-content"> 
                                                    <h3 class="block">Edit Room Type</h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">  Room Type
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="room_typeName" value="<?php echo $res->apt_type_name; ?>" />

                                                            <span class="help-block"> Provide your Room Type.</span>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a onclick="load_room_type();" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back 
                                                        </a>
                                                        <input type="hidden" name="room_type_id" value="<?php echo $roomTypeID ?>">
                                                        <input type="submit" class="btn green" name="edit_room_type" value="Submit" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>