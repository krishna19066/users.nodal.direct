<?php
include "../admin-function.php";
checkUserLogin();

require_once('MC_OAuth2Client.php');
require_once('MC_RestClient.php');
require_once('miniMCAPI.class.php');
$client = new MC_OAuth2Client();

$customerId = $_SESSION['customerID'];

?>

<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>

<?php
session_start();
?>
<html>
    <head>
        <style>
            .card{
                margin:50px;
                float:left;
                position:relative;             
                color:white;
                font-weight:bold;
            }

            .card input[type="radio"]
            {
                margin-top:-35px;
                margin-bottom:10px;
            }

            .card i{			
                font-size:55px;
                z-index:2;
                margin-top: 50%;
            }
            .overly{
                display:none;
                text-align:center;
                top:0;
                width:100%;
                height:300px;
                position:absolute;
                background:rgba(0, 0, 0, 0.5);
                color:#fff;			
            }

            .dn{
                display:none;
            }
            .db{
                display:block;
            }
            .card > :nth-child(3){
                display:block;
            }
        </style>
        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Connect To Mail Chimp</span>
                                                <span class="caption-helper">MailChimp</span>
                                            </div>
                                        </div>
                                        <!--<span style="float:left;"> <a href="booking-engine.php"><button style="background:#FF9800;color:white;border:none;height:35px;width:160px;font-size:14px; margin-top:35px;"><i class="fa fa-plus"></i> &nbsp Booking Engine</button></a></span><br><br>-->
                                       <!-- <span style="float:right;"> <a href="manage_rates.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Rates</button></a></span><br><br>-->
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="form-body">
                                            <div class="body">
                                                <div class="property portlet light bordered">
                                                   
                                                        <h3>You can connect MailChimp here:<span style="color: #009688;"> <a href="<?=$client->getLoginUri()?>">Connect To MailChip</a></span></h3>
                                                       
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#property').on('change', function () {
            var propertyID = $(this).val();
            if (propertyID) {
                $.ajax({
                    type: 'POST',
                    url: 'property_room.php',
                    data: 'property_id=' + propertyID,
                    success: function (html) {
                        $('#room').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#room').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>
<script language="JavaScript">
    $('#select-all').click(function (event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';      ?>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>