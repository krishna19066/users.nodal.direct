<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$roomDataCount = new propertyData();
//----------------------------Google My Business Api start----------------------------------------------------------------------------------->

$google_my_result_1 = $inventoryCont->getGoogleMyBusinessData($customerId);
$gmb_result = $google_my_result_1[0];
$gmb_status = $gmb_result->status;
$account_id = $gmb_result->account_id;

require_once 'GMB/google-api-php-client/src/Google/autoload.php'; // or wherever autoload.php is located
include 'GMB/google-api-php-client/src/Google/MyBusiness.php'; // or wherever autoload.php is located
//Declare your Google Client ID, Google Client secret and Google redirect uri in  php variables
$google_client_id = '718435963727-e5ln6214dsn6ki7fq9j1ho1bug39aras.apps.googleusercontent.com';
$google_client_secret = '4l-MyMKLPtyAkBDDeUlCdaNq';
$google_redirect_uri = 'http://www.users.nodal.direct/sitepanel/google-my-business.php';

//setup new google client
$client = new Google_Client();
//$client = new Google_Service_MyBusiness();
$client->setApplicationName('My application name');
$client->setClientid($google_client_id);
$client->setClientSecret($google_client_secret);
$client->setRedirectUri($google_redirect_uri);
$client->setAccessType('online');
$client->setScopes('https://www.googleapis.com/auth/plus.business.manage');
$googleImportUrl = $client->createAuthUrl();
$mybusinessService = new Google_Service_MyBusiness($client);
$reviews = $mybusinessService->accounts;

//print_r($reviews);
function curl($url, $post = "") {
    $curl = curl_init();
    $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
    curl_setopt($curl, CURLOPT_URL, $url);
    //The URL to fetch. This can also be set when initializing a session with curl_init().
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    //The number of seconds to wait while trying to connect.
    if ($post != "") {
        curl_setopt($curl, CURLOPT_POST, 5);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    }
    curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
    //The contents of the "User-Agent: " header to be used in a HTTP request.
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    //To follow any "Location: " header that the server sends as part of the HTTP header.
    curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
    //To automatically set the Referer: field in requests where it follows a Location: redirect.
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    //The maximum number of seconds to allow cURL functions to execute.
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    //To stop cURL from verifying the peer's certificate.
    $contents = curl_exec($curl);
    curl_close($curl);
    return $contents;
}

//google response with contact. We set a session and redirect back
if (isset($_GET['code'])) {
    $auth_code = $_GET["code"];
    $_SESSION['google_code'] = $auth_code;
}
/*
  Check if we have session with our token code and retrieve all contacts, by sending an authorized GET request to the following URL : https://www.google.com/m8/feeds/contacts/default/full
  Upon success, the server responds with a HTTP 200 OK status code and the requested contacts feed. For more informations about parameters check Google API contacts documentation
 */
/* if (isset($_SESSION['google_code'])) {
  $auth_code = $_SESSION['google_code'];
  $max_results = 200;
  $fields = array(
  'code' => urlencode($auth_code),
  'client_id' => urlencode($google_client_id),
  'client_secret' => urlencode($google_client_secret),
  'redirect_uri' => urlencode($google_redirect_uri),
  'grant_type' => urlencode('authorization_code')
  );
  //print_r($fields);
  $post = '';
  foreach ($fields as $key => $value) {
  $post .= $key . '=' . $value . '&';
  }
  $post = rtrim($post, '&');


  $result = curl('https://accounts.google.com/o/oauth2/token', $post);
  $response = json_decode($result);
  // print_r($response);
  // die;
  $accesstoken = $response->access_token;
  //$url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results='.$max_results.'&alt=json&v=3.0&oauth_token='.$accesstoken;
  $url = 'https://mybusiness.googleapis.com/v4/accounts?oauth_token=' . $accesstoken;
  $xmlresponse = curl($url);
  //print_r($xmlresponse);
  $contacts = json_decode($xmlresponse, true);

  $res = $contacts['accounts'];
  //print_r($res);
  $account_id = $res[0]['name'];
  $accountName = $res[0]['accountName'];
  $type = $res[0]['type'];
  $url9 = 'https://mybusiness.googleapis.com/v4/' . $account_id . '/locations?oauth_token=' . $accesstoken; // Get Location & Acoount list
  //$url9 = 'https://mybusiness.googleapis.com/v4/accounts/106210626692903564522/locations/17375209541208459910/reviews?oauth_token='.$accesstoken; // Get Review List
  $xmlresponse9 = curl($url9);
  // print_r($xmlresponse9);

  $all_location = json_decode($xmlresponse9, true);
  $res_data = $all_location['locations'];
  // print_r($res_data);
  $total_property = $all_location['totalSize'];
  foreach ($res_data as $dt) {
  $location_id = $dt['name'];
  $locationName = $dt['locationName'];
  $primaryPhone = $dt['primaryPhone'];
  $ins = $inventoryCont->InsertGoogleMyBusiness($customerId, $account_id, $accountName, $location_id, $locationName, $primaryPhone, $total_property, $accesstoken);
  }
  $url9 = 'https://mybusiness.googleapis.com/v4/accounts/106210626692903564522/locations/12925794181628294083/reviews?oauth_token=' . $access_token; // Get Review List
  $xmlresponse_review = curl($url9);
  print_r($xmlresponse_review);

  $all_review = json_decode($xmlresponse_review, true);

  //$location = $all_location['locations'][0];
  } */
//----------------------------Google My Business Api End----------------------------------------------------------------------------------->
//----------------------------Mail Chimp Api Start----------------------------------------------------------------------------------->
$mailchimp_result = $inventoryCont->getMailChimpData($customerId);
$mailchimp_result_1 = $mailchimp_result[0];
$mail_chimp_status = $mailchimp_result_1->status;
require_once('mailchimp/MC_OAuth2Client.php');
require_once('mailchimp/MC_RestClient.php');
require_once('mailchimp/miniMCAPI.class.php');
$client = new MC_OAuth2Client();
//----------------------------Mail Chimp Api End----------------------------------------------------------------------------------->
if (isset($_GET['type'])) {
    $type = $_REQUEST['type'];
    if ($type == 'connect') {
        $sta = 'Y';
    } else {
        $sta = 'N';
    }
   // $upd = $inventoryCont->UpdateMailChimpStatus($sta, $customerId);
    header('location:manage-3rd-api.php');
}
?>
<!--------------------------------------- Start Instagram------------------------------------------------->
<?php
require 'instagram/src/Instagram.php';
use MetzWeb\Instagram\Instagram;
// initialize class
$instagram = new Instagram(array(
    'apiKey' => 'af400230b6e54084bf569270f77dc94c',
    'apiSecret' => '185b0ce2f969441d9d0b5bb26eea7508',
    'apiCallback' => 'http://www.users.nodal.direct/sitepanel/instagram/success.php' // must point to success.php
));
// create login URL
$loginUrl = $instagram->getLoginUrl();

$insta_res = $inventoryCont->getInstagramData($customerId);
$insta_sta = $insta_res[0];
$insta_status = $insta_sta->status;

if (isset($_GET['type'])) {
    $type = $_REQUEST['type'];
    if ($type == 'connect') {
        $sta = 'Y';
    } else {
        $sta = 'N';
    }
    $upd = $inventoryCont->UpdateInstagramStatus($sta, $customerId);
    header('location:manage-3rd-api.php');
}

?>
<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>
<?php
session_start();
?>
<html>
    <head>
        <style>
            .card{
                margin:50px;
                float:left;
                position:relative;             
                color:white;
                font-weight:bold;
            }

            .card input[type="radio"]
            {
                margin-top:-35px;
                margin-bottom:10px;
            }

            .card i{			
                font-size:55px;
                z-index:2;
                margin-top: 50%;
            }
            .overly{
                display:none;
                text-align:center;
                top:0;
                width:100%;
                height:300px;
                position:absolute;
                background:rgba(0, 0, 0, 0.5);
                color:#fff;			
            }

            .dn{
                display:none;
            }
            .db{
                display:block;
            }
            .card > :nth-child(3){
                display:block;
            }
        </style>
        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Connect To API</span>
                                                <span class="caption-helper">API Integration</span>
                                            </div>
                                        </div>
                                        <!--<span style="float:left;"> <a href="booking-engine.php"><button style="background:#FF9800;color:white;border:none;height:35px;width:160px;font-size:14px; margin-top:35px;"><i class="fa fa-plus"></i> &nbsp Booking Engine</button></a></span><br><br>-->
                                       <!-- <span style="float:right;"> <a href="manage_rates.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Rates</button></a></span><br><br>-->
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="form-body">
                                            <div class="body">
                                                <div class="property portlet light bordered">                                                 
                                                    <!-- BEGIN Portlet PORTLET-->
                                                    <div class="portlet light">
                                                        <div class="portlet-title">
                                                            <div class="caption font-green-sharp">
                                                                <i class="icon-speech font-red-sunglo"></i>
                                                                <span class="caption-subject bold uppercase">Select API want to connect</span>
                                                                <span class="caption-helper"></span>
                                                            </div>
                                                           <!-- <div class="actions">
                                                                <?php if ($mail_chimp_status == 'Y') { ?>
                                                                    <a href="MailChimp.php?type=disconnect" onClick="return confirm('Are you sure you want to disconnect MailChimp')" class="btn  btn-danger">
                                                                        <i class="fa fa-connectdevelop"></i> Disconnect </a>
                                                                <?php } else { ?>
                                                                    <a href="MailChimp.php?type=connect" onClick="return confirm('Are you sure you want to Connect MailChimp')" class="btn  btn-success">
                                                                        <i class="fa fa-connectdevelop"></i> Connect </a>
                                                                <?php } ?>
                                                            </div>-->
                                                        </div>

                                                        <table class="table">
                                                           <thead>
                                                                <tr>
                                                                    <th style="width:10%"><img src="https://preview.ibb.co/bV0uQL/Mail-Chimp.png" style="width:170px;height:140px;"></th>
                                                                    <th style="width:60%"><p style="font-size: 13px;font-family: monospace;"><strong> MailChimp </strong> is a leading email marketing solution. More than 9 million people and businesses around the world use MailChimp to send bulk emails like newsletters and announcements. They are also great for transactional emails as well. Their features and integrations allow you to send marketing emails, automated messages, and targeted campaigns. While their detailed reports help you keep improving over time.</p></th>
                                                                    <th style="width:10%"> <?php if ($mail_chimp_status == 'Y') { ?><span style="color:green;"> Connected </span> <?php } else { ?> <span style="color:darkred;"> Disconnected</span> <?php } ?></th>
                                                                   <!-- <th scope="col"> <a href="mailchimpInsertData.php" class="btn  btn-info">
                                                                       <i class="fa fa-refresh"></i> Refresh </a></th>-->
                                                                    <th style="width:10%"> <?php if ($mail_chimp_status == 'Y') { ?>
                                                                            <a href="MailChimp.php?type=disconnect" onClick="return confirm('Are you sure you want to disconnect MailChimp')" class="btn btn-danger" style="color:white;border:none;height:35px;width:160px;font-size:14px; <?php if ($mode == 'V') { ?> pointer-events: none; cursor: not-allowed;opacity: 0.6; <?php } ?>">
                                                                                <i class="fa fa-connectdevelop"></i> Disconnect </a>
                                                                        <?php } else { ?>
                                                                            <a href="<?= $client->getLoginUri() ?>" onClick="return confirm('Are you sure you want to Connect MailChimp')" class="btn btn-success" <?php if ($mode == 'V') { ?>  style="pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?>>
                                                                                <i class="fa fa-connectdevelop"></i> Connect </a>
                                                                        <?php } ?></th>
                                                                </tr>
                                                            </thead>

                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width:10%"><img src="https://i.ibb.co/J7CngH7/gmb-logo.jpg" style="height:70px;"></th>
                                                                        <th style="width:60%"><p style="font-size: 13px;font-family: monospace;"><strong> Google My Business  </strong> is a Easily manage multiple locations at scale through the API. Use features like Posts, provide meaningful review responses and relevant menu updates, publish fresh photos, and much more. While their detailed reports help you keep improving over time.</p></th>
                                                                        <th style="width:10%"> <?php if ($gmb_status == 'Y') { ?><span style="color:green;"> Connected </span> <?php } else { ?> <span style="color:darkred;"> Disconnected</span> <?php } ?>  </span></th>
                                                                       <!-- <th scope="col"> <a href="mailchimpInsertData.php" class="btn  btn-info">
                                                                                <i class="fa fa-refresh"></i> Refresh </a></th>-->
                                                                        <th style="width:10%"> <?php if ($gmb_status == 'Y') { ?>
                                                                                <a href="google-my-business.php?type=disconnect&account_id=<?php echo $account_id; ?>" onClick="return confirm('Are you sure you want to disconnect Google My Business')" class="btn btn-danger" style="color:white;border:none;height:35px;width:160px;font-size:14px; <?php if ($mode == 'V') { ?> pointer-events: none; cursor: not-allowed;opacity: 0.6; <?php } ?>">
                                                                                    <i class="fa fa-connectdevelop"></i> Disconnect </a>
                                                                            <?php } else { ?>
                                                                                <a href="<?php echo $googleImportUrl; ?>" onClick="return confirm('Are you sure you want to Connect Google My Business')" class="btn btn-success" <?php if ($mode == 'V') { ?>  style="pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?>>
                                                                                    <i class="fa fa-connectdevelop"></i> Connect </a>
                                                                            <?php } ?></th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                              <table class="table">
                                                               <thead>
                                                                    <tr>
                                                                        <th style="width:10%"><img src="https://i.ibb.co/56VhxVS/instagram.jpg" style="height:75px;"></th>
                                                                        <th style="width:60%"><p style="font-size: 13px;font-family: monospace;"><strong> Instagram </strong> is a photo-sharing app. Those are a-dime-a-thousand, but Instagram was the first, best one. It's known for its square image format, its virtual lack of a website and, of course, those nostalgic filters. Instagram is for taking pictures, yes. While their detailed reports help you keep improving over time.</p></th>
                                                                        <th style="width:10%"> <?php if ($insta_status == 'N') { ?><span style="color:green;"> Connected </span> <?php } else { ?> <span style="color:darkred;"> Disconnected</span> <?php } ?>  </span></th>
                                                                       <!-- <th scope="col"> <a href="mailchimpInsertData.php" class="btn  btn-info">
                                                                                <i class="fa fa-refresh"></i> Refresh </a></th>-->
                                                                        <th style="width:10%"> <?php if ($insta_status == 'Y') { ?>
                                                                                <a href="manage-3rd-api.php?type=disconnect" onClick="return confirm('Are you sure you want to disconnect Instagram')" class="btn btn-danger" style="color:white;border:none;height:35px;width:160px;font-size:14px; <?php if ($mode == 'V') { ?> pointer-events: none; cursor: not-allowed;opacity: 0.6; <?php } ?>">
                                                                                    <i class="fa fa-connectdevelop"></i> Disconnect </a>
                                                                            <?php } else { ?>
                                                                                <a href="<?php echo $loginUrl ?>" onClick="return confirm('Are you sure you want to Connect Instagram')" class="btn btn-success" <?php if ($mode == 'V') { ?>  style="pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?>>
                                                                                    <i class="fa fa-connectdevelop"></i> Connect </a>
                                                                            <?php } ?></th>
                                                                    </tr>
                                                                </thead>
                                                            </table>

                                                            <h5>Synchronize your Nodal Direct User lists with GMB to send automated, customized emails to your guests or a general newsletter. You can disconnect later if you choose. </h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#property').on('change', function () {
            var propertyID = $(this).val();
            if (propertyID) {
                $.ajax({
                    type: 'POST',
                    url: 'property_room.php',
                    data: 'property_id=' + propertyID,
                    success: function (html) {
                        $('#room').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#room').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>
<script language="JavaScript">
    $('#select-all').click(function (event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';             ?>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>