<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$roomDataCount = new propertyData();
$propList1 = $inventoryCont->getPropertyListing($customerId);
?>
<?php
if (!empty($_POST["property_id"])) {
    $property_id = $_POST['property_id'];

    $roomData = $inventoryCont->getRoomDetails($property_id);
    $rowCount = count($roomData);
}
//room option list
if ($rowCount > 0) {
    echo '<option value="">Select Room</option>';
    foreach($roomData as $dt) {
        echo '<option value="' . $dt->roomID . '">' . $dt->roomType . '</option>';
    }
} else {
    echo '<option value="">Room not available</option>';
}
?>
