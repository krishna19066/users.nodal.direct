<?php
include "admin-function.php";
checkUserLogin();
// include "cms_functions.php";
// user_locked();
// staff_restriction();
$inqdate = date('Y/m/d');
$customerId = $_SESSION['customerID'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}

$welcomeCont = new welcomeData();
$enquiryData = new manageEnquiry();
$propertCount = new propertyData();
$inventoryCont = new inventoryData();
$analyticsCont = new analyticsPage();
$getAllEnquiry = $enquiryData->GetAllEnquiryWithCustomerId($customerId); // Get All Inquiry Data
$getTodatInquiry = $enquiryData->GetTodayInquiry($customerId, $inqdate); // Gett Today Inquiry Data
$req_inquiry = $enquiryData->GetRequestCallInquiry($customerId); // Get RequestCall Inquiry Data
$req_TodayInquiry = $enquiryData->GetTodayReqInquiry($customerId, $inqdate); // Get RequestCall Today Inquiry Data
//$totalInquiry = count($getAllEnquiry);
$today_inquiry = count($getTodatInquiry);
$totalReqInquiry = count($req_inquiry);
$today_Req_inquiry = count($req_TodayInquiry);
$allPropertyID = $inventoryCont->getPropertyListing($customerId, $user_id);
$analyticData = $analyticsCont->getAnalyticCodeData($customerId);
$analytic_res = $analyticData[0];
$cloud_keySel = $propertCount->get_Cloud_AdminDetails($customerId);
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;

$userData = new UserDetails();
$res_inquiery = $userData->getPropertyIDWithUser($customerId, $user_id);
$getUserAllEnquiry = array();
foreach ($res_inquiery as $re) {
    $propertyID = $re->propertyID;
    $getUserAllEnquiry[] = $userData->GetAllEnquiryWithPropertyID($customerId, $propertyID);
}
if ($userType == 'user') {
    $totalInquiry = count($getUserAllEnquiry);
} else {
    $totalInquiry = count($getAllEnquiry);
}
?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?php
        adminCss();
        ?>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
       <!-- <script src="assets/pages/scripts/dashboard.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL SCRIPTS -->
        <style>
            .bg-overlay {
                background: linear-gradient(rgba(0,0,0,.7), rgba(0,0,0,.7)), url("images/analytics_.jpg");
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center center;
                color: #fff;
                height: 467px;
                width: auto;
                padding-top: 50px;
            }

        </style>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <?php
                    // die;
                    // $q=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");	
                    // $custID = $_SESSION['customerID'];
                    ?>

                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> Admin Dashboard
                                <small>statistics, charts, recent events and reports</small>
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Dashboard</span>
                        </li>
                    </ul>
                    <!--<div class="page-toolbar">
                            <i class="icon-calendar"></i>&nbsp;
                    <?php //echo date('d-M-Y');    ?>&nbsp;
                    </div>-->                   
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered"  href="all-members.php">
                                <div class="display">
                                    <div class="number">

                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="<?php echo $totalInquiry; ?>">0</span>
                                            <small class="font-green-sharp"></small>
                                        </h3>
                                        <small>Total Inquiries</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only">Today's Inquiries :</span>
                                        </span>
                                    </div>
                                    <div class="status">

                                        <div class="status-title">Today's Inquiries : </div>
                                        <div class="status-number"><?php echo $today_inquiry; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">

                                        <h3 class="font-red-haze">
                                            <span data-counter="counterup" data-value="<?php echo $totalReqInquiry; ?>">0</span>
                                        </h3>
                                        <small>Call Back Requests</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-like"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
                                            <span class="sr-only">Today's Call Back :</span>
                                        </span>
                                    </div>
                                    <div class="status">

                                        <div class="status-title">Today's Call Back :</div>
                                        <div class="status-number"><?php echo $today_Req_inquiry; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <?php
                                        $getAllPropertyReview = $enquiryData->getPropertyReview($customerId);
                                        $propertyReviewCount = count($getAllPropertyReview);
                                        // $inq2 = "select count(s_no) from property_reviews where customerID='$custID'";
                                        // $inqquer2 = mysql_query($inq2);
                                        // while($row3 = mysql_fetch_array($inqquer2))
                                        // {
                                        // $tot2 = $row3['count(s_no)'];
                                        // }
                                        ?>
                                        <h3 class="font-purple-soft">
                                            <span data-counter="counterup" data-value="<?php echo $propertyReviewCount; ?>"></span>
                                        </h3>
                                        <small>Total Reviews</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 57%;" class="progress-bar progress-bar-success purple-soft">
                                            <span class="sr-only">56% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> change </div>
                                        <div class="status-number"> 57% </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <?php
                                        $getAllnewletter = $enquiryData->getnewLetter($customerId);
                                        if ($userType == 'user') {
                                            $newletterCounto = '0';
                                        } else {
                                            $newletterCounto = count($getAllnewletter);
                                        }

// $signup_sel = "select count(s_no) from newsletter where customerID='$custID'";
// $signup_quer = mysql_query($signup_sel);
                                        ?>
                                        <h3 class="font-blue-sharp">
                                            <span data-counter="counterup" data-value="<?php echo $newletterCounto; ?>"></span>
                                        </h3>
                                        <small>Email Subscriptions</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-envelope"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 45%;" class="progress-bar progress-bar-success blue-sharp">
                                            <span class="sr-only">Today's Subscriptions :</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <?php
                                        $inqdate2 = date('Y-m-d');
                                        $getnewletter = $enquiryData->getnewLetterToday($customerId, $inqdate2);
                                        $newletterCountoToday = count($getnewletter);
// $inq3 = "select count(s_no) from newsletter where recvDate='$inqdate2' and customerID='$custID'";
// $inqquer3 = mysql_query($inq3);
                                        ?>
                                        <div class="status-title">Today's Email Subscriptions : </div>
                                        <div class="status-number"><?php echo $newletterCountoToday; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <!--------------------------------------------------------New--------------------------------->

                    <!--------------------------------------------end--------------------------------------------->

                    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

                    <script type="text/javascript">
                        google.load("search", "1");
                        google.load("jquery", "1.4.2");
                        google.load("jqueryui", "1.7.2");
                    </script>
                    <!-- END DASHBOARD STATS 1-->

                    <script>
                        (function (w, d, s, g, js, fs) {
                        g = w.gapi || (w.gapi = {});
                        g.analytics = {q: [], ready: function (f) {
                        this.q.push(f);
                        }};
                        js = d.createElement(s);
                        fs = d.getElementsByTagName(s)[0];
                        js.src = 'https://apis.google.com/js/platform.js';
                        fs.parentNode.insertBefore(js, fs);
                        js.onload = function () {
                        g.load('analytics');
                        };
                        }(window, document, 'script'));
                    </script>

                    <div class="row">
                        <div class="col-lg-6 col-xs-12 col-sm-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Site Visits</span>
                                        <span class="caption-helper"><div id="embed-api-auth-container"></div></span>
                                    </div>
                                    <div class="actions">
                                        <button type="button" style="background:#ec8111;color:white;width:170px;height:30px;border:none;border-radius:100px;"><a href="<?php
                                            if ($analytic_res->analytic_url) {
                                                echo $analytic_res->analytic_url;
                                            } else {
                                                echo '#';
                                            }
                                            ?>"  style="text-decoration:none;color:white;"><i class="fa fa-area-chart"></i> View Analytic Report </a></button>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="container bg-overlay">
                                        <div class="row text-center">
                                            <img src="images/google-analytics-logo.png" class="img-responsive"/>
                                            <?php if ($analytic_res->analytic_url) { ?>
                                                <h1>View Analytic Report Click Button.</h1>                                       
                                                <br><br>
                                                        <a href="<?php echo $analytic_res->analytic_url; ?>" target="_blabk" class="btn btn-primary btn-lg" style="text-decoration:none;color:white;"> <i class="fa fa-area-chart"></i> View Analytic Report</a>
                                                    <?php } else { ?>

                                                        <h2> Google Analytic Report Not Available.</h2>
                                                        <h3>Please contact: support@nodal.direct</h3>
                                                    <?php } ?>
                                                    </div>
                                                    </div>                                                   
                                                    <!-- <div id="embed-api-auth-container"></div>
                                                     <div id="chart-container"></div>
                                                     <div id="view-selector-container"></div>-->
                                                    </div>

                                                    </div>
                                                    <!-- END PORTLET-->
                                                    </div>

                    <!--    <script>
                            gapi.analytics.ready(function () {

                            /**
                             * Authorize the user immediately if the user has already granted access.
                             * If no access has been created, render an authorize button inside the
                             * element with the ID "embed-api-auth-container".
                             */
                            gapi.analytics.auth.authorize({
                            container: 'embed-api-auth-container',
                                    clientid: '371313688050-6vvnhr28g7aj0fh96iqmka71hflk6egr.apps.googleusercontent.com'
                            });
                            /**
                             * Create a new ViewSelector instance to be rendered inside of an
                             * element with the id "view-selector-container".
                             */
                            var viewSelector = new gapi.analytics.ViewSelector({
                            container: 'view-selector-container'
                            });
                            // Render the view selector to the page.
                            viewSelector.execute();
                            /**
                             * Create a new DataChart instance with the given query parameters
                             * and Google chart options. It will be rendered inside an element
                             * with the id "chart-container".
                             */
                            var dataChart = new gapi.analytics.googleCharts.DataChart({
                            query: {
                            metrics: 'ga:sessions',
                                    dimensions: 'ga:date',
                                    'start-date': '30daysAgo',
                                    'end-date': 'yesterday'
                            },
                                    chart: {
                                    container: 'chart-container',
                                            type: 'LINE',
                                            options: {
                                            width: '100%'
                                            }
                                    }
                            });
                            /**
                             * Render the dataChart on the page whenever a new view is selected.
                             */
                            viewSelector.on('change', function (ids) {
                            dataChart.set({query: {ids: ids}}).execute();
                            });
                            });
                        </script>-->


                                                    <div class="col-lg-6 col-xs-12 col-sm-12">
                                                        <div class="portlet light bordered">
                                                            <div class="portlet-title">
                                                                <div class="caption ">
                                                                    <span class="caption-subject font-dark bold uppercase">Inquiries</span>
                                                                    <span class="caption-helper">monthly starts...</span>
                                                                </div>
                                                                <div class="actions">
                                                                    <a href="manage-enquiry.php" class="btn btn-circle green btn-outline btn-sm">
                                                                        <i class="fa fa-pencil"></i> View All 
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div id="dashboard_amchart_3" class="CSSAnimationChart">

                                                                    <?php
                                                                    $year_org = date('Y');
                                                                    // $jan = "select count(recvDate) from inquiry where year='$year_org' and month='01'";
                                                                    // $janquer = mysql_query($jan);

                                                                    $jan = $enquiryData->JanInquiry($customerId, $year_org);
                                                                    $jan_data = count($jan);
                                                                    $feb = $enquiryData->FebInquiry($customerId, $year_org);
                                                                    $feb_data = count($feb);
                                                                    $mar = $enquiryData->MarInquiry($customerId, $year_org);
                                                                    $mar_data = count($mar);
                                                                    $apr = $enquiryData->AprInquiry($customerId, $year_org);
                                                                    $apr_data = count($apr);
                                                                    $may = $enquiryData->MayInquiry($customerId, $year_org);
                                                                    $may_data = count($may);
                                                                    $jun = $enquiryData->JunInquiry($customerId, $year_org);
                                                                    $jun_data = count($jun);
                                                                    $jul = $enquiryData->JulInquiry($customerId, $year_org);
                                                                    $jul_data = count($jul);
                                                                    $aug = $enquiryData->AugInquiry($customerId, $year_org);
                                                                    $aug_data = count($aug);
                                                                    $sep = $enquiryData->SepInquiry($customerId, $year_org);
                                                                    $sep_data = count($sep);
                                                                    $oct = $enquiryData->OctInquiry($customerId, $year_org);
                                                                    $oct_data = count($oct);
                                                                    $nov = $enquiryData->NovInquiry($customerId, $year_org);
                                                                    $nov_data = count($nov);
                                                                    $dec = $enquiryData->DecInquiry($customerId, $year_org);
                                                                    $dec_data = count($dec);
                                                                    $anb = "[
                                                                ['JAN', $jan_data],
                                                                ['FEB', $feb_data],
                                                                ['MAR', $mar_data],
                                                                ['APR', $apr_data],
                                                                ['MAY', $may_data],
                                                                ['JUN', $jun_data],
                                                                ['JUL', $jul_data],
                                                                ['AUG', $aug_data],
                                                                ['SEP', $sep_data],
                                                                ['OCT', $oct_data],
                                                                ['NOV', $nov_data],
                                                                ['DEC', $dec_data]
                                                        ]";
                                                                    echo '<script>var arrayFromPhp = ' . $anb . ';</script>';
                                                                    // print_r($anb);
                                                                    $a = array('JAN' => $jan_data, 'FEB' => $feb_data, 'MAR' => $mar_data, 'APR' => $apr_data, 'MAY' => $mar_data, 'JUN' => $jun_data, 'JUL' => $jul_data, 'AUG' => $aug_data, 'SEP' => $sep_data, 'OCT' => $oct_data, 'NOV' => $nov_data, 'DEC' => $dec_data);
                                                                    $count = 0;
                                                                    foreach ($a as $key => $val) {
                                                                        if ($val > $count) {
                                                                            $count = $val;
                                                                            $c = $key;
                                                                        }
                                                                    }

                                                                    if ($count == $jan_data) {
                                                                        $hiegest_jan = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $feb_data) {
                                                                        $hiegest_feb = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $mar_data) {
                                                                        $hiegest_mar = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $apr_data) {
                                                                        $hiegest_apr = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $may_data) {
                                                                        $hiegest_may = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $jun_data) {
                                                                        $hiegest_jun = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $jul_data) {
                                                                        $hiegest_jul = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $aug_data) {
                                                                        $hiegest_aug = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $sep_data) {
                                                                        $hiegest_sep = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $oct_data) {
                                                                        $hiegest_oct = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $nov_data) {
                                                                        $hiegest_nov = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } elseif ($count == $dec_data) {
                                                                        $hiegest_dec = 'indexLabel: "highest",markerColor: "red", markerType: "triangle"';
                                                                    } else {
                                                                        $hiegest = '';
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    // --------------------------- Get Property Photo Count----------------------------------------->
                                                                    $dataPropertyPhoto = array();
                                                                    foreach ($allPropertyID as $res) {
                                                                        $propID = $res->propertyID;
                                                                        $propertyName = $res->propertyName;
                                                                        $propData = $propertCount->getPropertyPhotoCount($propID, $customerId);
                                                                        foreach ($propData as $dt) {
                                                                            $count = count($propData);
                                                                            $countPhoto = ($count / 100);
                                                                        }
                                                                        //print_r($propData);

                                                                        $dataPropertyPhoto[] = array("label" => $propertyName, "y" => $count);
                                                                        //  print_r($dataPoints1);
                                                                    }
                                                                    $dataPropertyPhoto;
                                                                    //print_r($dataPoints1);
                                                                    ///----------------------------Get Property photo Room----------------------------------------->
                                                                    $dataPropertyRoomPhoto = array();
                                                                    foreach ($allPropertyID as $res) {
                                                                        $propID = $res->propertyID;
                                                                        $propertyName = $res->propertyName;
                                                                        $propData = $propertCount->getRoomPropertyPhotoCount($propID, $customerId);
                                                                        foreach ($propData as $dt) {
                                                                            $count = count($propData);
                                                                            //$countPhoto = ($count / 100);
                                                                        }
                                                                        //print_r($propData);

                                                                        $dataPropertyRoomPhoto[] = array("label" => $propertyName, "y" => $count);
                                                                        //  print_r($dataPoints1);
                                                                    }
                                                                    $dataPropertyRoomPhoto;
                                                                    //-------------------------------------Get Property By city-------------------------------------->
                                                                    $allPropertyID99 = $propertCount->getPropertyCount99($customerId, $user_id);
                                                                    $dataProperty = array();
                                                                    foreach ($allPropertyID99 as $res) {
                                                                        $propID = $res->propertyID;
                                                                        $cityName = $res->cityName;
                                                                        $propData = $propertCount->getPropertyCount($customerId, $cityName);
                                                                        foreach ($propData as $dt) {
                                                                            $count = count($propData);
                                                                            //$countPhoto = ($count / 100);
                                                                        }
                                                                        //print_r($propData);

                                                                        $dataProperty[] = array("label" => $cityName, "y" => $count);
                                                                        //  print_r($dataPoints1);
                                                                    }
                                                                    $dataPropertyRoomPhoto;
                                                                    ?>

                                                                    <!-------------------------------------->
                                                                    <script type="text/javascript">
                                                                        window.onload = function() {
                                                                        var chart1 = new CanvasJS.Chart("chartContainer1",
                                                                        {

                                                                        title:{
                                                                        text: ""
                                                                        },
                                                                                axisX: {
                                                                                valueFormatString: "MMM",
                                                                                        interval:1,
                                                                                        intervalType: "month"
                                                                                },
                                                                                axisY:{
                                                                                includeZero: false

                                                                                },
                                                                                data: [
                                                                                {
                                                                                type: "line",
                                                                                        dataPoints: [
                                                                                        { x: new Date(2018, 00, 1), y: <?php echo $jan_data; ?>,<?php echo $hiegest_jan; ?> },
                                                                                        { x: new Date(2018, 01, 1), y: <?php echo $feb_data; ?>,<?php echo $hiegest_feb; ?>},
                                                                                        { x: new Date(2018, 02, 1), y: <?php echo $mar_data; ?>,<?php echo $hiegest_mar; ?>},
                                                                                        { x: new Date(2018, 03, 1), y: <?php echo $apr_data; ?>,<?php echo $hiegest_apr; ?> },
                                                                                        { x: new Date(2018, 04, 1), y: <?php echo $may_data; ?>,<?php echo $hiegest_may; ?> },
                                                                                        { x: new Date(2018, 05, 1), y: <?php echo $jun_data; ?>,<?php echo $hiegest_jun; ?> },
                                                                                        { x: new Date(2018, 06, 1), y: <?php echo $jul_data; ?>,<?php echo $hiegest_jul; ?> },
                                                                                        { x: new Date(2018, 07, 1), y: <?php echo $aug_data; ?>,<?php echo $hiegest_aug; ?> },
                                                                                        { x: new Date(2018, 08, 1), y: <?php echo $sep_data; ?>,<?php echo $hiegest_sep; ?> },
                                                                                        { x: new Date(2018, 09, 1), y: <?php echo $oct_data; ?>,<?php echo $hiegest_oct; ?> },
                                                                                        { x: new Date(2018, 10, 1), y: <?php echo $nov_data; ?>,<?php echo $hiegest_nov; ?> },
                                                                                        { x: new Date(2018, 11, 1), y: <?php echo $dec_data; ?>,<?php echo $hiegest_dec; ?> }
                                                                                        ]
                                                                                }
                                                                                ]
                                                                        });
                                                                        var chart2 = new CanvasJS.Chart("chartContainer2", {
                                                                        title:{
                                                                        text: ""
                                                                        },
                                                                                data: [
                                                                                {
                                                                                // Change type to "doughnut", "line", "splineArea", etc.
                                                                                type: "column",
                                                                                        dataPoints: <?php echo json_encode($dataProperty, JSON_NUMERIC_CHECK); ?>

                                                                                }
                                                                                ]
                                                                        });
                                                                        var chart3 = new CanvasJS.Chart("chartContainer3", {
                                                                        animationEnabled: true,
                                                                                exportEnabled: true,
                                                                                theme: "light1", // "light1", "light2", "dark1", "dark2"
                                                                                title:{
                                                                                text: ""
                                                                                },
                                                                                axisX:{
                                                                                reversed: true
                                                                                },
                                                                                toolTip:{
                                                                                shared: true
                                                                                },
                                                                                data: [{
                                                                                type: "stackedBar",
                                                                                        name: "Property Photo",
                                                                                        dataPoints: <?php echo json_encode($dataPropertyPhoto, JSON_NUMERIC_CHECK); ?>
                                                                                }, {

                                                                                type: "stackedBar",
                                                                                        name: "Room Photo",
                                                                                        indexLabel: "#total",
                                                                                        indexLabelPlacement: "outside",
                                                                                        indexLabelFontSize: 15,
                                                                                        indexLabelFontWeight: "bold",
                                                                                        dataPoints: <?php echo json_encode($dataPropertyRoomPhoto, JSON_NUMERIC_CHECK); ?>
                                                                                }]
                                                                        });
                                                                        chart1.render();
                                                                        chart2.render();
                                                                        chart3.render();
                                                                        }
                                                                    </script>
                                                                    <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
                                                                    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

                                                                    <!-------------------------------------->                                    

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="row">                       
                                                        <div class="col-lg-6 col-xs-12 col-sm-12">
                                                            <div class="portlet light bordered">
                                                                <div class="portlet-title">
                                                                    <div class="caption ">
                                                                        <span class="caption-subject font-dark bold uppercase">Property City</span>
                                                                        <span class="caption-helper"></span>
                                                                    </div>
                                                                    <div class="actions">
                                                                        <a href="manage-property.php" class="btn btn-circle green btn-outline btn-sm">
                                                                            <i class="fa fa-pencil"></i> 
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div id="dashboard_amchart_3" class="CSSAnimationChart">
                                                                        </head>
                                                                        <body>
                                                                            <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
                                                                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-xs-12 col-sm-12">
                                                            <div class="portlet light bordered">
                                                                <div class="portlet-title">
                                                                    <div class="caption ">
                                                                        <span class="caption-subject font-dark bold uppercase">Property Photo</span>
                                                                        <span class="caption-helper"></span>
                                                                    </div>
                                                                    <div class="actions">
                                                                        <a href="manage-property.php" class="btn btn-circle green btn-outline btn-sm">
                                                                            <i class="fa fa-pencil"></i> 
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div id="dashboard_amchart_3" class="CSSAnimationChart">
                                                                        </head>
                                                                        <body>
                                                                            <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
                                                                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 col-xs-12 col-sm-12">
                                                            <div class="portlet light bordered">
                                                                <div class="portlet-title tabbable-line">
                                                                    <div class="caption">
                                                                        <i class=" icon-social-twitter font-dark hide"></i>
                                                                        <span class="caption-subject font-dark bold uppercase">Inquiries</span>
                                                                    </div>
                                                                    <ul class="nav nav-tabs">
                                                                        <li class="active">
                                                                            <a href="#tab_actions_pending" data-toggle="tab"> Pending </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#tab_actions_completed" data-toggle="tab"> Completed </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane active" id="tab_actions_pending">
                                                                            <!-- BEGIN: Actions -->
                                                                            <div class="mt-actions">
                                                                                <?php
                                                                                $inquiryData1 = $welcomeCont->getInquiryData('', $customerId, 'order by slno desc limit 5');
                                                                                $inquiryCount = count($inquiryData1);
                                                                                if ($inquiryCount == "0") {
                                                                                    ?>
                                                                                    <div class="mt-action">
                                                                                        <div class="mt-action-body">
                                                                                            <div class="mt-action-row">
                                                                                                <div class="mt-action-info ">
                                                                                                    <div class="mt-action-details ">
                                                                                                        <span class="mt-action-author"></span>
                                                                                                        <p class="mt-action-desc">Sorry! No Records Found Yet.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div><br>
                                                                                        <?php
                                                                                    } else {
                                                                                        if ($userType != 'user') {
                                                                                            foreach ($inquiryData1 as $inquiryData) {
                                                                                                ?>
                                                                                                <div class="mt-action">
                                                                                                    <div class="mt-action-img">
                                                                                                        <i class="fa fa-user"></i> </div>
                                                                                                    <div class="mt-action-body">
                                                                                                        <div class="mt-action-row">
                                                                                                            <div class="mt-action-info ">
                                                                                                                <div class="mt-action-icon ">
                                                                                                                    <i class="icon-magnet"></i>
                                                                                                                </div>
                                                                                                                <div class="mt-action-details ">
                                                                                                                    <span class="mt-action-author"><?php echo $inquiryData->name; ?></span>
                                                                                                                    <p class="mt-action-desc"><?php echo $inquiryData->inquiryType; ?></p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="mt-action-datetime ">
                                                                                                                <span class="mt-action-date"><?php echo $inquiryData->recvDate; ?></span>
                                                                                                                <span class="mt-action-dot bg-red"></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            foreach ($res_inquiery as $re) {
                                                                                                $propertyID = $re->propertyID;
                                                                                                $inquiryDataCompleted2 = $userData->GetAllEnquiryWithPropertyID($customerId, $propertyID);

                                                                                                foreach ($inquiryDataCompleted2 as $inquiryDataCompleted) {
                                                                                                    ?>
                                                                                                    <div class="mt-action">
                                                                                                        <div class="mt-action-img">
                                                                                                            <i class="fa fa-user"></i> </div>
                                                                                                        <div class="mt-action-body">
                                                                                                            <div class="mt-action-row">
                                                                                                                <div class="mt-action-info ">
                                                                                                                    <div class="mt-action-icon ">
                                                                                                                        <i class="icon-action-redo"></i>
                                                                                                                    </div>
                                                                                                                    <div class="mt-action-details ">
                                                                                                                        <span class="mt-action-author"><?php echo $inquiryDataCompleted->name; ?></span>
                                                                                                                        <p class="mt-action-desc"><?php echo $inquiryDataCompleted->inquiryType; ?></p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="mt-action-datetime ">
                                                                                                                    <span class="mt-action-date"><?php echo $inquiryDataCompleted->recvDate; ?></span>
                                                                                                                    <span class="mt-action-dot bg-green"></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                            </div>
                                                                            <!-- END: Actions -->
                                                                        </div>
                                                                        <div class="tab-pane" id="tab_actions_completed">
                                                                            <!-- BEGIN:Completed-->
                                                                            <div class="mt-actions">
                                                                                <?php
                                                                                $inquiryDataCompleted1 = $welcomeCont->getInquiryData('Confirmed', $customerId, 'order by slno desc limit 5');
                                                                                $inquiryCompletedCount = count($inquiryDataCompleted1);
                                                                                if ($inquiryCompletedCount == "0") {
                                                                                    ?>
                                                                                    <div class="mt-action">
                                                                                        <div class="mt-action-body">
                                                                                            <div class="mt-action-row">
                                                                                                <div class="mt-action-info ">
                                                                                                    <div class="mt-action-details ">
                                                                                                        <span class="mt-action-author"></span>
                                                                                                        <p class="mt-action-desc">Sorry! No Records Found Yet.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div><br>
                                                                                        <?php
                                                                                    } else {
                                                                                        if ($userType != 'user') {
                                                                                            foreach ($inquiryDataCompleted1 as $inquiryDataCompleted) {
                                                                                                ?>
                                                                                                <div class="mt-action">
                                                                                                    <div class="mt-action-img">
                                                                                                        <i class="fa fa-user"></i> </div>
                                                                                                    <div class="mt-action-body">
                                                                                                        <div class="mt-action-row">
                                                                                                            <div class="mt-action-info ">
                                                                                                                <div class="mt-action-icon ">
                                                                                                                    <i class="icon-action-redo"></i>
                                                                                                                </div>
                                                                                                                <div class="mt-action-details ">
                                                                                                                    <span class="mt-action-author"><?php echo $inquiryDataCompleted->name; ?></span>
                                                                                                                    <p class="mt-action-desc"><?php echo $inquiryDataCompleted->inquiryType; ?></p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="mt-action-datetime ">
                                                                                                                <span class="mt-action-date"><?php echo $inquiryDataCompleted->recvDate; ?></span>
                                                                                                                <span class="mt-action-dot bg-green"></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            foreach ($res_inquiery as $re) {
                                                                                                $propertyID = $re->propertyID;
                                                                                                $inquiryDataCompleted2 = $userData->GetAllEnquiryWithPropertyID($customerId, $propertyID);

                                                                                                foreach ($inquiryDataCompleted2 as $inquiryDataCompleted) {
                                                                                                    ?>
                                                                                                    <div class="mt-action">
                                                                                                        <div class="mt-action-img">
                                                                                                            <i class="fa fa-user"></i> </div>
                                                                                                        <div class="mt-action-body">
                                                                                                            <div class="mt-action-row">
                                                                                                                <div class="mt-action-info ">
                                                                                                                    <div class="mt-action-icon ">
                                                                                                                        <i class="icon-action-redo"></i>
                                                                                                                    </div>
                                                                                                                    <div class="mt-action-details ">
                                                                                                                        <span class="mt-action-author"><?php echo $inquiryDataCompleted->name; ?></span>
                                                                                                                        <p class="mt-action-desc"><?php echo $inquiryDataCompleted->inquiryType; ?></p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="mt-action-datetime ">
                                                                                                                    <span class="mt-action-date"><?php echo $inquiryDataCompleted->recvDate; ?></span>
                                                                                                                    <span class="mt-action-dot bg-green"></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <!-- END: Completed -->
                                                                            </div>
                                                                        </div>
                                                                        <?php if ($userType != 'user') { ?>
                                                                            <a href="manage-enquiry.php"><button style="background:#ed6b75;color:white;width:100px;height:30px;border:none;border-radius:100px;"> View All </button></a>
                                                                        <?php } else { ?>
                                                                            <a href="user-manage-enquiry.php"><button style="background:#ed6b75;color:white;width:100px;height:30px;border:none;border-radius:100px;"> View All </button></a>

                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-xs-12 col-sm-12">
                                                            <div class="portlet light bordered">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="icon-bubble font-dark hide"></i>
                                                                        <span class="caption-subject font-hide bold uppercase">Recent Users</span>
                                                                    </div>
                                                                    <!--<div class="actions">
                                                                        <div class="btn-group">
                                                                            <a href="manage-user.php"><button style="background:#32c5d2;color:white;width:100px;height:30px;border:none;border-radius:100px;"> View All </button></a>
                                                                        </div>
                                                                    </div>-->
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="row">
                                                                        <?php
                                                                        $recentUsers1 = $welcomeCont->getRecentUsers($admincustId, 'order by last_login desc limit 3', $user_id);

                                                                        foreach ($recentUsers1 as $recentUsers) {
                                                                            $picpath = $recentUsers->photo;
                                                                            ?>
                                                                            <div class="col-md-4">
                                                                                <!--begin: widget 1-1 -->
                                                                                <div class="mt-widget-1">
                                                                                    <div class="mt-img">
                                                                                        <?php
                                                                                        if ($picpath == '') {
                                                                                            ?>
                                                                                            <img src="https://u.o0bc.com/avatars/no-user-image.gif" class="img-responsive" alt="" style="height:100px;width:100px;"> 
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/g_face,w_100,h_100,c_fill/reputize/admin_user/<?php echo $picpath; ?>.jpg" class="img-responsive" alt="">
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
    <!--<img src="assets/pages/media/users/avatar80_1.jpg"> -->
                                                                                                </div>
                                                                                                <div class="mt-body">
                                                                                                    <h3 class="mt-username"><?php echo $recentUsers->name; ?></h3>
                                                                                                    <p class="mt-user-title"><b>Email :- </b> <?php echo $recentUsers->email; ?> <br> <b>Type :- </b><?php echo $recentUsers->user_type; ?> </p>

                                                                                                </div>
                                                                                                <?php if ($user_id) { ?>
                                                                                                    <b>User Create Date :- </b> <?php echo $recentUsers->timestamp; ?> <br>

                                                                                                    <?php } else { ?>
                                                                                                        <b>Date :- </b> <?php echo $recentUsers->last_login; ?> <br>
                                                                                                            <b>Time :- </b><?php echo $recentUsers->lat_login_time; ?> <br>
                                                                                                            <?php } ?>

                                                                                                            </div>
                                                                                                            <!--end: widget 1-1 -->
                                                                                                            </div>
                                                                                                            <?php
                                                                                                        }
                                                                                                        ?>
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        </div>

                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-6 col-xs-12 col-sm-12"></div>
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        </div>	
                                                                                                        </body>
                                                                                                        </html>
                                                                                                        <?php
                                                                                                        admin_footer();
                                                                                                        ?>
                                                                                                        <script type = "text/javascript">
                                                                        document.getElementById('startButton').onclick = function() {
                                                                        introJs().setOption('doneLabel', 'Next page').start().oncomplete(function() {
                                                                        window.location.href = 'manage-property-settings.php?multipage=true';
                                                                        });
                                                                        };
                                                                                                        </script>