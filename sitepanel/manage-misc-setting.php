<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$cityCont = new propertyData();
$chatCount = new analyticsPage();
$fun_resData = $cityCont->get_Cloud_AdminDetails($customerId);
$getChatScript = $chatCount->getChatScriptData($customerId);
$res_chat = count($getChatScript);
$res = $getChatScript[0];
//print_r($resData);
$resData = $fun_resData[0];

//------------Update Base Url ------------------------------->
if (isset($_POST['add_chatScript'])) {
    $chat_Script = $_REQUEST['chat_script'];
    $chat_Script_body = $_REQUEST['chat_script_body'];
    $chat_Script_footer = $_REQUEST['chat_script_footer'];

    if ($res_chat == '0') {
        $inse = $chatCount->InsertChatScriptData($customerId, $chat_Script,$chat_Script_body,$chat_Script_footer);
        echo '<script type="text/javascript">
                alert("Succesfuly Added Chat Script");              
window.location = "manage-misc-setting.php";
            </script>';
    } else {
        $upd = $chatCount->UpdateChatScriptCode($customerId, $chat_Script,$chat_Script_body,$chat_Script_footer);
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Chat Script");              
window.location = "manage-misc-setting.php";
            </script>';
    }
}
?>
<html>
    <head>
       
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <br><br><br>
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Update Chat Script</span>
                                            <span class="caption-helper">Update Chat Script</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="You can enter chat script here!" data-original-title="Add chat script" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                   <!-- <span style="float:right;"> <a href="manage-testimonial.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Add Testimonial</button></a></span><br><br>-->
                                </div>
                                <div class="portlet-body form" style="background: white;">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                        <div class="form-body">

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Chat Script (Header)</label>
                                                <span class="required"> * </span>
                                                <div class="col-md-9">
                                                    <textarea class=" form-control" name="chat_script" rows="6"> <?php echo $res->chat_script; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Chat Script (Body)</label>
                                                <span class="required"> * </span>
                                                <div class="col-md-9">
                                                    <textarea class=" form-control" name="chat_script_body" rows="6"> <?php echo $res->chat_script_body; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Chat Script (Footer)</label>
                                                <span class="required"> * </span>
                                                <div class="col-md-9">
                                                    <textarea class=" form-control" name="chat_script_footer" rows="6"> <?php echo $res->chat_script_footer; ?></textarea>
                                                </div>
                                            </div>
                                        </div>                                           
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input name="add_chatScript" type="hidden" value="update" />
                                                    <input name="customerID" type="hidden" value="<?php echo $customerId; ?>"/>
                                                    <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Submit" />                                                      
                                                    <a href="javascript:history.back()" class="btn red">Go Back</a>

                                                </div>
                                            </div>
                                        </div>

                                </div>

                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </body>
</html>
<!-- END PAGE LEVEL PLUGINS -->
<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<?php
admin_footer();
?>