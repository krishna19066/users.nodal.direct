<?php

include "admin-function.php";
////checkUserLogin();
//$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$propertyCount = new propertyData();
//$customerId = 25;

$json = json_decode(trim(file_get_contents('php://input')));
//print_r($json);
if ($json != NULL) {
    $access_token = $json->access_token;
    $db_token = '6cdbe1fbf9009009df1b68d153631c00';
    if ($access_token == $db_token) {
        $property_id = $json->property_id;
        $room_id = $json->room_id;
        $from_date = $json->from_date;
        $to_date = $json->to_date;
        $price = $json->price;
        $resData = $propertyCount->GetPropertyDataWithId($property_id);
        $res = $resData[0];
        $customerId = $res->customerID;


        $date_from = $from_date;
        $date_from = strtotime($date_from); // Convert date to a UNIX timestamp  
// Specify the end date. This date can be any English textual format  
        $date_to = $to_date;
        $date_to = strtotime($date_to); // Convert date to a UNIX timestamp  
// Loop from the start date to end date and output all dates inbetween  
        for ($i = $date_from; $i <= $date_to; $i+=86400) {

            //echo date("Y-m-d", $i) . '<br />';
            $fr_yr = date("Y", $i);
            $fr_month = date("m", $i);
            $jsonDate = date("d", $i);

            if ($jsonDate == '01') {
                $jsonDate = '1';
            }
            if ($jsonDate == '02') {
                $jsonDate = '2';
            }
            if ($jsonDate == '03') {
                $jsonDate = '3';
            }
            if ($jsonDate == '04') {
                $jsonDate = '4';
            }
            if ($jsonDate == '05') {
                $jsonDate = '5';
            }
            if ($jsonDate == '06') {
                $jsonDate = '6';
            }
            if ($jsonDate == '07') {
                $jsonDate = '7';
            }
            if ($jsonDate == '08') {
                $jsonDate = '8';
            }
            if ($jsonDate == '09') {
                $jsonDate = '9';
            }
            $jsonDate;
            $fr_month;
            $fr_yr;

            $selectRateQueryJson = $inventoryCont->getInventoryRateDetails($property_id, $room_id, $fr_month, $fr_yr);

            $rateNumRowJson = count($selectRateQueryJson);
            if ($rateNumRowJson == 0) {
                //echo 'insert';
                //echo $jsonDate;
                $insertdata = $inventoryCont->InsertInventoryRateData($customerId, $jsonDate, $property_id, $room_id, $fr_month, $fr_yr, $price);
            } else {
                //echo 'up';
                $upd_data = $inventoryCont->UpdateInventoryRateData($jsonDate, $price, $property_id, $room_id, $fr_month, $fr_yr);
            }
        }
        $records['status'] = 200;
        $records['status_message'] = "Successfuly Update Rate";
        $records['is_valid'] = true;
        $records['success'] = true;
// $records['data'] = $res_data;
        echo json_encode($records, true);
    } else {
        $records['status'] = 401;
        $records['status_message'] = "Invalide User OR Unauthorized";
        $records['is_valid'] = false;
        $records['success'] = false;
        // $records['data'] = $res_data;
        echo json_encode($records, true);
    }
} else {
    $records['status'] = 422;
    $records['status_message'] = "Invalide Parameter";
    $records['is_valid'] = false;
    $records['success'] = false;
    // $records['data'] = $res_data;
    echo json_encode($records, true);
}


?>