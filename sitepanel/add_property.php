<?php

include "admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$propertyinsert = new propertyData();
$ajaxclientCont = new staticPageData();
$VideoGalleryListdata = $propertyinsert->getVideoGallery($customerId);

//---------Add Property ------------>
if (isset($_POST['add_prop_sub'])) {

    $room_type = $_POST['room_type'];
    foreach ($room_type as $key => $value) {
        $roomTypes99 .= $value . '^';
    }
    $room_amenity = $_POST['room_amenity'];
    foreach ($room_amenity as $key => $value1) {
        $roomAmenities99 .= $value1 . '^';
    }
    $meta_title = $_REQUEST['propertyName'];
    $filename = $_REQUEST['propertyURL'];
    $dec1 = $_REQUEST['propertyInfo'];
    $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');
    $propertyinsert->addNewProperty($customerId, $roomTypes99, $roomAmenities99);
     $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
    header("location:manage-property.php");
}
if (isset($_POST['update_prop_sub'])) {

    $room_type = $_POST['room_type'];
    $propId = $_POST['propertyId'];

    foreach ($room_type as $key => $value) {
        $roomTypes99 .= $value . '^';
    }
    $room_amenity = $_POST['room_amenity'];
    foreach ($room_amenity as $key => $value1) {
        $roomAmenities99 .= $value1 . '^';
    }
    $propertyinsert->UpdateProperty($customerId, $roomTypes99, $roomAmenities99, $propId);
    header("location:manage-property.php");
}
//----------End Property added form----->
//--------Add Property Rooms------------>
if (isset($_POST['add_prop_rooms'])) {

    $room_amenity = $_POST['businessType'];
    $propertyID = $_POST['propertyID'];
    foreach ($room_amenity as $key => $value1) {
        $roomAmenities99 .= $value1 . '^';
    }

    $propertyinsert->addNewRooms($customerId, $roomAmenities99, $propertyID);
    //header("location:manage-property.php");
     echo '<script type="text/javascript">
                alert("Succesfuly added room");              
window.location = "ajax_view-rooms.php?propID=' . $propertyID . '";
            </script>';
}
//---------End Property Room---------->
//---------Add Question -------------->
if (isset($_POST['submit_question'])) {

    $propertyID = $_POST['propertyID'];

    $propertyinsert->addProPertyQuestion($customerId, $propertyID);

    header("location:ajax_manage-question.php?propID=" . $propertyID);
}
//----------End Question ---------------------->
//-----------Add City ------------------------->
if (isset($_POST['add_city'])) {
    $propertyType = $_REQUEST['tbl_property_type'];

    foreach ($propertyType as $key => $value) {
        $property_type = $value;
        $propertyinsert->addCity($customerId, $property_type);
    }
    echo '<script type="text/javascript">
                alert("Succesfuly Added City");              
window.location = "manage-property-settings.php";
            </script>';
}

// ---------------End City ------------------------>
//--------------Add Sub City ------------------------>
if (isset($_POST['add_sub_city'])) {

    $propertyinsert->addSubCity($customerId);

    echo '<script type="text/javascript">
                alert("Succesfuly Added  Sub City");              
window.location = "manage-property-settings.php";
            </script>';
}
if (isset($_POST['edit_sub_city'])) {

    $subCityID = $_REQUEST['subCityID'];

    $propertyinsert->UpdateSubCity($subCityID);

    echo '<script type="text/javascript">
                alert("Succesfuly Updated Sub City");              
window.location = "manage-property-settings.php";
            </script>';
}
//----------End Sub City---------------------------------->
//-------------Add Property Type------------------------>
if (isset($_POST['add_property_type'])) {
    $propertyinsert->addPropertyTypes($customerId);

    echo '<script type="text/javascript">
                alert("Succesfuly Added Property Type");              
window.location = "manage-property-settings.php";
            </script>';
}
if (isset($_POST['edit_property_type'])) {
    $propTypeID = $_REQUEST['propTypeID'];

    $propertyinsert->UpdatePropertyType($propTypeID);

    echo '<script type="text/javascript">
                alert("Succesfuly Updated Property Type");              
window.location = "manage-property-settings.php";
            </script>';
}
//-------------End Property Type------------------------>
//-------------Add Room Type------------------------>
if (isset($_POST['add_room_type'])) {

    $propertyinsert->addRoomTypes($customerId);

    echo '<script type="text/javascript">
                alert("Succesfuly Added Room Type");              
window.location = "manage-property-settings.php";
            </script>';
}
if (isset($_POST['edit_room_type'])) {
    $roomTypeID = $_REQUEST['room_type_id'];

    $propertyinsert->UpdateRoomTypes($roomTypeID);

    echo '<script type="text/javascript">
                alert("Succesfuly Updated Room Type");              
window.location = "manage-property-settings.php";
            </script>';
}


//-------------End Room Type------------------------>
//-------------Upload Photo For Property------------------------>
if (isset($_POST['submit_pic'])) {
    $cloud_keySel = $propertyinsert->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    $record_type = "Property Photo";
    $script_name = "view-property-photo.php";
    $table_name = "propertyImages";

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $img_seq='cloudinary';
    $pro_id = $_POST['recordID'];
    $propId = base64_encode($pro_id);
    $pro_type = "property";
    $type = "home";
    $file = $_FILES['image_org']['name'];
    $alt_tag = $_POST['imageAlt1'];
//echo $alt_tag."<br>";
    $roo_id = "0";
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;
    if ($customerId == 1) {
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "Property"));
    } else {
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/Property"));
    }


    //  $ins = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type) values('$timestmp','$alt_tag','$pro_type','$roo_id','$pro_id','$type')";
    //$quer = mysql_query($ins);
    $propertyImg = $propertyinsert->AddPropertyPhoto($timestmp, $alt_tag, $pro_type, $roo_id, $pro_id, $type,$img_seq);
    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "ajax_view-property-photo.php?propID=' . $propId . '";
            </script>';
}
// ------------------------Add Property Featured ----------------------------------->
if (isset($_REQUEST['id'])) {
    $imagesID = $_REQUEST['imagesID1'];
    $propId1 = $_REQUEST['recordID'];
    $propId = base64_encode($propId1);
    $upd = $propertyinsert->AddFeaturedProperty($imagesID);
    echo '<script type="text/javascript">
                alert("Succesfuly Update Featured");              
window.location = "ajax_view-property-photo.php?propID=' . $propId . '";
            </script>';
}
if (isset($_REQUEST['featured'])) {
    $imagesID = $_REQUEST['imagesID1'];
    $propId1 = $_REQUEST['recordID'];
    $propId = base64_encode($propId1);

    $upd = $propertyinsert->RemoveFeaturedProperty($imagesID);
    echo '<script type="text/javascript">
                alert("Succesfuly Remove Featured");              
window.location = "ajax_view-property-photo.php?propID=' . $propId . '";
            </script>';
}

//-------------End Upload Photo Property------------------------>
//---------Awards Photo Add-------------->
if (isset($_POST['submit_award_pic'])) {
    $cloud_keySel = $propertyinsert->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    $record_type = "Property Photo";
    $script_name = "view-property-photo.php";
    $table_name = "propertyImages";

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $pro_id = $_POST['recordID'];
    $file = $_FILES['image_org']['name'];
    $alt_tag = $_POST['imageAlt1'];
//echo $alt_tag."<br>";
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/awards"));

    //  $ins = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type) values('$timestmp','$alt_tag','$pro_type','$roo_id','$pro_id','$type')";
    //$quer = mysql_query($ins);
    $propertyImg = $propertyinsert->AddAwardsPhoto($customerId, $timestmp, $alt_tag, $pro_id);
    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "ajax_awards.php?propID=' . $pro_id . '";
            </script>';
}
//------------------------Add Testimonial---------------------------------------------------->
if (isset($_POST['submit_testimonial'])) {
    $cloud_keySel = $propertyinsert->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    $record_type = "Property Photo";
    $script_name = "view-property-photo.php";
    $table_name = "propertyImages";

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $review = $_POST['review'];
    $name = $_POST['name'];
    $occupation = $_POST['occupation'];
    $file = $_FILES['image_org']['name'];
    $pro_id = $_POST['propertyID'];
    $video_review = $_POST['video_review'];

    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    if ($file) {
        $upload = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/testimonials"));
    }

    //$ins = "insert into testimonials(customerID,review,photo,name,detail) values('$customerId','$review','$timestmp','$name','$occupation')";
    // $quer = mysql_query($ins);
    $ins = $propertyinsert->AddTestimonialData($customerId, $pro_id, $review, $name, $occupation, $video_review, $timestmp);

    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "manage-property.php";
            </script>';
    exit;
}
if (isset($_REQUEST['action']) == 'delete') {
    $imagesID = $_REQUEST['imagesID'];
    $propertyinsert->DeletePropertyImages($imagesID);

    echo '<script type="text/javascript">
                alert("Delete Property Images");              
window.location = "manage-property.php";
            </script>';
}
//-----------------Upload Food Menu Url-------------------------------------------------------------------->
if (isset($_POST['update_foodmenulink'])) {
    $propertyID = $_POST['propertyID'];
    $food_menu_link = $_POST['foodmenu_link'];
    // print_r($_FILES['file']);    
    $FileUpload = new File();
    if (isset($_FILES['file'])) {
        $FileUpload->UploadFile($_FILES['file']);
    } else {
        echo 'file Was not submited';
    }

    $propertyinsert->UpdateFoodMenuLink($propertyID, $_FILES['file']);
    echo '<script type="text/javascript">
               alert("Succesfuly Update link");           
               window.location = "manage-property.php";
        </script>';
}
if (isset($_POST['visit_trip_Advisor'])) {
    $propertyID = $_POST['propertyID'];
    $trip_advisor = $_POST['visirTrip_advisor'];
    $trip_advisor_2 = $_POST['visirTrip_advisor_2'];

    $propertyinsert->UpdateTripAdvisorLink($propertyID, $trip_advisor, $trip_advisor_2);
    echo '<script type="text/javascript">
               alert("Succesfuly Update link");           
               window.location = "manage-property.php";
        </script>';
}
if (isset($_REQUEST['submit_video_gallery'])) {
    $video_heading = $_REQUEST['video_heading'];
    $video_url = $_REQUEST['video_url'];
    foreach ($VideoGalleryListdata as $res) {
        $db_video_url = $res->video_url;
        $db_page_url = $res->page_url;
    }
    if ($db_video_url == NULL) {
        $upd = $propertyinsert->UpdateVideoGalleryData($customerId, $video_heading, $video_url);
    } else {
        $ins = $propertyinsert->AddVideoGallery($customerId, $video_heading, $video_url, $db_page_url);
    }
    echo '<script type="text/javascript">
               alert("Succesfuly Add Video");           
               window.location = "manage-static-pages.php";
        </script>';
}
if (isset($_REQUEST['Update_video_gallery'])) {
    $video_heading = $_REQUEST['video_heading'];
    $video_url = $_REQUEST['video_url'];
    $videoID = $_REQUEST['videoID'];
    $upd = $propertyinsert->UpdateVideoGalleryWithID($videoID, $customerId, $video_heading, $video_url);
    echo '<script type="text/javascript">
               alert("Succesfuly Updated Video");           
               window.location = "manage-static-pages.php";
        </script>';
}
if (isset($_REQUEST['action2']) == 'VideoGalleryDelete') {
    $videoID = $_REQUEST['VideoID'];

    $propertyinsert->DeletePropertyVideo($videoID);

    echo '<script type="text/javascript">
                alert("Succesfuly Delete Property Video");              
window.location = "manage-static-pages.php";
            </script>';
}

