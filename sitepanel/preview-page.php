<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$adminDetials = new analyticsPage();
$admin_details = $adminDetials->getUsersData($customerId);
$user = $admin_details[0];
$base_url = $user->website;
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];

$clientPageData = $ajaxclientCont->getClientPageData($pageUrl, $customerId);
$pageData = $clientPageData[0];


//call Google PageSpeed Insights API
/* echo $siteURL = $base_url."/".$pageUrl.'.html';
  $googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$siteURL&screenshot=true");

  //decode json data
  $googlePagespeedData = json_decode($googlePagespeedData, true);

  //screenshot data
  $screenshot = $googlePagespeedData['screenshot']['data'];
  $screenshot = str_replace(array('_', '-'), array('/', '+'), $screenshot);

  //display screenshot image
  echo "<img src=\"data:image/jpeg;base64," . $screenshot . "\" />";
  //$about_us = '\"data:image/jpeg;base64,"' . $screenshot . '"\"';
 */


#initialize
$site = $base_url . "/" . $pageUrl . '.html';
$img_tag_attributes = "border='1'";
$use_cache = false;
$apc_is_loaded = extension_loaded('apc');

#set $use_cache
if ($apc_is_loaded) {
    apc_fetch("thumbnail:" . $site, $use_cache);
}

if (!$use_cache) {
    $image = file_get_contents("https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=$site&screenshot=true");
    $image = json_decode($image, true);
    $image = $image['screenshot']['data'];
    if ($apc_is_loaded) {
        apc_add("thumbnail:" . $site, $image, 5400);
    }
}

$image = str_replace(array('_', '-'), array('/', '+'), $image);
// echo "<img src=\"data:image/jpeg;base64,".$image."\" $img_tag_attributes />";
//echo getGooglePageSpeedScreenshot($_GET['url'], 'class="thumbnail"');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>    
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> Preview Page
                                <small>View Static page page</small>
                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="welcome.php">Home & Static Page</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View Page Screen hot</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Preview </span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <a class="pull-right">
                                                        <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="../sitepanel/updatePage_content.php" id="submit_form" method="post">
                                        <div class="form-wizard">
                                            <div class="form-body">




                                                <div class=" mt-overlay-1">
                                                    <?php echo "<a href='" . $site . "' target='_blank'><img src=\"data:image/jpeg;base64," . $image . "\" $img_tag_attributes /></a>"; ?>
                                                    <div class="mt-overlay">
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
        <div class="quick-nav-overlay"></div>
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>
