<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
//echo $cityId = $_POST['cityid'];
//die;
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add New Property City
                                <small>Add a new property City here..</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property-settings.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">(Add Property City</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                                <a class="pull-right">
                                                    <a href="manage-property-settings.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-eye"></i> &nbsp View All City List</button></a>                                               
                                                    <a href="ajax_add-roomtype.php"> <button style="background:#3683d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; "><i class="fa fa-plus"></i> &nbsp Add Room Type</button></a>
                                                    <a href="ajax_add_subcity.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; margin-right: 21px;"><i class="fa fa-plus"></i> &nbsp Add Sub City</button></a>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="../sitepanel/add_property.php" id="submit_form" method="post">
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <div class="tab-content"> 
                                                    <h3 class="block">Add City</h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">City Name
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="cityName" />

                                                            <span class="help-block"> Provide your City Name.</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Property Type:</label>
                                                        <div class="col-md-4">
                                                            <?php
                                                            $propTypList1 = $propertyCont->getAllPropTyp($customerId);
                                                            foreach ($propTypList1 as $propTypList) {
                                                                ?>
                                                                <input type="checkbox" name="tbl_property_type[]" value="<?php echo $propTypList->type_name; ?>" /> &nbsp; <?php echo $propTypList->type_name; ?>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="javascript:;" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back 
                                                        </a>
                                                        <input type="submit" class="btn green" name="add_city" value="Submit" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->


            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>