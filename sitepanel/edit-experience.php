<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$propertyCont = new propertyData();
$exp_Id = $_REQUEST['expID'];
$exp_data = $propertyCont->getExperienceDataWithID($customerId, $exp_Id);
$res_testi = $exp_data[0];
?>
<?php
@extract($_REQUEST);

if (isset($_POST['update_experience'])) {
  
    $categoryName1 = $_POST['categoryName'];
    $categoryName = implode(',', $categoryName1);
    $update = $propertyCont->UpdateExperienceData($customerId, $exp_Id,$name,$city,$state,$rate,$from_date,$to_date,$categoryName,$exp_content);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated");              
window.location = "edit-experience.php?expID=' . $exp_Id . '";
            </script>';
}
//admin_header();	
?>

<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">

                                <br><br><br>
                                <div class="portlet light bordered" style="height: 137px;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Experience</span>
                                            <span class="caption-helper">Edit Experience</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Edit Experience Those You Show your website!" data-original-title="Upadte Experience" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <span style="float:left;"><a href="manage-experience.php"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true" value="List Of Experience" /></a></span>
                                </div>

                                <div class="portlet light bordered">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;">
                                            <div class="form-body">                                               
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="name" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide Reviewer Name</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">City
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="city" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->city); ?>" required="" />
                                                        <span class="help-block"> Provide city Name</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">State
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="state" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->state); ?>" required="" />
                                                        <span class="help-block"> Provide state Name</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Rate
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="rate" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->rate); ?>" required="" />
                                                        <span class="help-block"> Provide rate</span>
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Select Category
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <table>
                                                            <tr>
                                                                <?php
                                                                $category_name = explode(',', $res_testi->category);
                                                                $cateList1 = $propertyCont->getExperienceCategory($customerId);

                                                                foreach ($cateList1 as $cat) {
                                                                    ?>
                                                                    <td style="padding-left:50px;">
                                                                        <div class="mt-checkbox-inline">
                                                                            <label class="mt-checkbox">
                                                                                <input type="checkbox" name="categoryName[]" id="inlineCheckbox21" value="<?php echo $cat->category_name; ?>" <?php
                                                                                if (in_array($cat->category_name, $category_name)) {
                                                                                    echo "checked";
                                                                                }
                                                                                ?>> <?php echo $cat->category_name; ?>
                                                                                <span></span>
                                                                            </label>

                                                                        </div>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Date
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input name="from_date" type="date" class="form-control" value="<?php echo html_entity_decode($res_testi->from_date); ?>" required="" />
                                                        <span class="help-block"> Provide From date</span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input name="to_date" type="date" class="form-control" value="<?php echo html_entity_decode($res_testi->to_date); ?>" required="" />
                                                        <span class="help-block"> Provide To date</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Content</label>
                                                <span class="required"> * </span>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="exp_content" rows="6" required> <?= $res_testi->exp_content; ?></textarea>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="submit" class="btn green" name="update_experience" class="button" id="submit" value="Update Experience" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />

                                                <a href="javascript:history.back()" class="btn red">Go Back</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                            
                        </div>
                    </div>
                </div>
                </body>
                </html>
                <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL PLUGINS -->
                <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                <?php
                admin_footer();
                ?>