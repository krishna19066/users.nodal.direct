<?php
include "admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$propertyData = new propertyData();
$enquiryData = new manageEnquiry();

$getAllEmailSub = $enquiryData->getnewLetter($customerId);
$reccnt = count($getAllEmailSub);

$inqid = $_GET['inqid'];
if ($inqid == 'InactiveBooking') {
    $inqid = 'Inactive Booking';
    $getAllEnquiry = $enquiryData->GetBookingStatus($customerId, $inqid);
}
if ($inqid == 'Completed') {

    $getAllEnquiry = $enquiryData->GetBookingStatus($customerId, $inqid);
}

if ($_GET['id'] == "delete") {
  
    $s_no = $_GET['s_no'];
    $email = $_GET['email'];
    $customerId;
    $delete_enq = $enquiryData->DeletesubscribeData($customerId,$s_no,$email);
    echo '<script type="text/javascript">
                alert("Data delete");              
window.location = "manage-email-subscribe.php";
            </script>';
    exit;
}

$start = (intval($start) == 0 or $start == "") ? $start = 0 : $start;
$pagesize = intval($pagesize) == 0 ? $pagesize = 10 : $pagesize;
$heading = "Manage Booking";
$record_type = "Inquiry";
$table_name = "manage_booking";
$cond = "where customerID='$customerId'";
?>

<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>         
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Manage Email Subscribe
                                    <small>View Email Subscribe List</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>

                            <li>
                                <span>Email Subscribe List</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Email Subscribe List</span>
                                        </div>
                                        <div class='button'><br>
                                            <a href="#" id ="export" role='button'><button style="background:hsl(166, 78%, 46%);border:none;color:white;width:170px;height:40px;float:right;font-weight:bold;margin-top: -15px;">Export CSV</button></a>
                                        </div><br><br>
                                    </div>
                                    <div style="float:right;padding:10px;"><i class="fa fa-star" style="color:gold;font-size:20px;"></i> &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i><font style="font-size:18px;"> &nbsp; Not Commented </font></div><br><br>

                                    <div class='container' style="display:none;"> 
                                        <div id="dvData">
                                            <table>
                                                <tr>
                                                    <th>Email</th>
                                                    <th>Date</th>
                                                    
                                                    <?php
                                                    //$csvsel = $enquiryData->GetAllManageBookingData($customerId);
// $csvsel = "select * from inquiry order by slno desc";
// $csvquer = mysql_query($csvsel);
                                                    foreach ($getAllEmailSub as $csvrow) {
                                                        ?>
                                                    <tr>

                                                        <td><?php echo $csvrow->email; ?></td>
                                                        <td><?php echo $csvrow->timestamp; ?> </td>
                                                        
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        <div id="replace_inbox mt-list-item">
                                            <!-- This is the test file content the links for the footer only page Manage-enquiry.php -->
                                            <?php include 'test.php'; ?>
                                            <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
                                            <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
                                            <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
                                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                                            <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
                                            <script>
                                                $(document).ready(function () {
                                                    $('#example').DataTable();
                                                });
                                            </script>
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Email</th>
                                                        <th>Date</th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $ctn = 1;
                                                    foreach ($getAllEmailSub as $data) {
                                                        ?>
                                                        <tr onclick="$('#<?php echo $ctn; ?>').show();">

                                                            <td><?php echo $data->email; ?></td>
                                                            <td><?php echo $data->timestamp; ?></td>
                                                            <td> 
                                                             <a href="manage-email-subscribe.php?s_no=<?php echo $data->s_no; ?>&id=delete&email=<?php echo $data->email; ?>" onClick="return confirm('Are you sure you want to delete this record')">
                                                                                            <i class="fa fa-trash" style="color:red;"></i>
                                                                                        </a>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $ctn++;
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Email</th>
                                                        <th>Date</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>                                                                                                                                                                                                                                                                                                                                                         <!--<tr onclick="$('#div').toggle('1000'); hdsh(this);" >-->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>

                        .page-header { position: relative; }
                        .reviews {
                            color: #555;    
                            font-weight: bold;
                            margin: 10px auto 20px;
                        }
                        .notes {
                            color: #999;
                            font-size: 12px;
                        }
                        .media .media-object { max-width: 60px; }
                        .media-body { position: relative; }
                        .media-date { 
                            position: absolute; 
                            right: 25px;
                            top: 25px;
                        }
                        .media-date li { padding: 0; }
                        .media-date li:first-child:before { content: ''; }
                        .media-date li:before { 
                            content: '.'; 
                            margin-left: -2px; 
                            margin-right: 2px;
                        }
                        .media-comment { margin-bottom: 20px; }
                        .media-replied { margin: 0 0 20px 50px; }
                        .media-replied .media-heading { padding-left: 6px; }

                        .btn-circle {
                            font-weight: bold;
                            font-size: 12px;
                            padding: 6px 15px;
                            border-radius: 20px;
                        }
                        .btn-circle span { padding-right: 6px; }
                        .embed-responsive { margin-bottom: 20px; }
                        .tab-content {

                            border: 1px solid #ddd;
                            border-top: 0;

                        }

                        .custom-input-file:hover .uploadPhoto { display: block; }
                        .anyClass {
                            height:350px;
                            overflow-y: scroll;
                        }
                        .anyClass {
                            height:350px;
                            overflow-y: scroll;
                        }
                        /* pagenation btn */
                        .paginate_button{
                            padding: 5px;
                        }
                    </style>
                    </body>


                    <script src="<?= SITE_ADMIN_URL; ?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL PLUGINS -->
<!--                    <script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.min.js'></script>-->
                    <script>
                                                            function hdsh(tro)
                                                            {
                                                                var tr_len = document.getElementById('inq_tab').children.length / 2;
                                                                for (i = 1; i <= tr_len; i++)
                                                                {
                                                                    if (i == tro)
                                                                    {
                                                                        // document.getElementById('div' + i).style.display = 'block';
                                                                        $('#div' + i).slideToggle();
                                                                    } else
                                                                    {
                                                                        document.getElementById('div' + i).style.display = 'none';
                                                                    }
                                                                }
                                                            }

                    </script>
                    <script type='text/javascript'>
                        $(document).ready(function () {

                            console.log("HELLO")
                            function exportTableToCSV($table, filename) {
                                var $headers = $table.find('tr:has(th)')
                                        , $rows = $table.find('tr:has(td)')

                                        // Temporary delimiter characters unlikely to be typed by keyboard
                                        // This is to avoid accidentally splitting the actual contents
                                        , tmpColDelim = String.fromCharCode(11) // vertical tab character
                                        , tmpRowDelim = String.fromCharCode(0) // null character

                                        // actual delimiter characters for CSV format
                                        , colDelim = '","'
                                        , rowDelim = '"\r\n"';

                                // Grab text from table into CSV formatted string
                                var csv = '"';
                                csv += formatRows($headers.map(grabRow));
                                csv += rowDelim;
                                csv += formatRows($rows.map(grabRow)) + '"';

                                // Data URI
                                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                                // For IE (tested 10+)
                                if (window.navigator.msSaveOrOpenBlob) {
                                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                                        type: "text/csv;charset=utf-8;"
                                    });
                                    navigator.msSaveBlob(blob, filename);
                                } else {
                                    $(this)
                                            .attr({
                                                'download': filename
                                                , 'href': csvData
                                                        //,'target' : '_blank' //if you want it to open in a new window
                                            });
                                }

                                //------------------------------------------------------------
                                // Helper Functions 
                                //------------------------------------------------------------
                                // Format the output so it has the appropriate delimiters
                                function formatRows(rows) {
                                    return rows.get().join(tmpRowDelim)
                                            .split(tmpRowDelim).join(rowDelim)
                                            .split(tmpColDelim).join(colDelim);
                                }
                                // Grab and format a row from the table
                                function grabRow(i, row) {

                                    var $row = $(row);
                                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                                    var $cols = $row.find('td');
                                    if (!$cols.length)
                                        $cols = $row.find('th');

                                    return $cols.map(grabCol)
                                            .get().join(tmpColDelim);
                                }
                                // Grab and format a column from the table 
                                function grabCol(j, col) {
                                    var $col = $(col),
                                            $text = $col.text();

                                    return $text.replace('"', '""'); // escape double quotes

                                }
                            }


                            // This must be a hyperlink
                            $("#export").click(function (event) {
                                // var outputFile = 'export'
                                var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                                outputFile = outputFile.replace('.csv', '') + '.csv'

                                // CSV
                                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);

                                // IF CSV, don't do event.preventDefault() or return false
                                // We actually need this to be a typical hyperlink
                            });
                        });
                    </script>
                    <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                    <?php
                    //  admin_footer();
                    ?>
                    </html>
                </div>