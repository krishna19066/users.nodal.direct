<?php
//include "/home/hawthorntech/public_html/stayondiscount.com/sitepanel/admin-function.php";
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$ajaxclientCont = new staticPageData();
$propertyCont = new propertyData();
$getHomeSlider = $propertyCont->GetHomepageSliderData($customerId);
$reccnt = count($getHomeSlider);
$clientPageData = $ajaxclientCont->getHomePageData($customerId);
$pageData = $clientPageData[0];
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$awardData = $propertyCont->GetAllAwardListData($customerId);
$filename = 'index';
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $filename);
$mets_res = $metaTags[0];

$cloud_keyData = $cloud_keySel[0];
//print_r($cloud_keyData);

$bucket = $cloud_keyData->bucket;
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
$base_url = $cloud_keyData->website;
$address = $cloud_keyData->address;
$key = $cloud_keyData->google_api_key;
$propertyName = $cloud_keyData->companyName;

$super_admin_1 = $propertyCont->GetSupperAdminData($admincustId);
$super_admin = $super_admin_1[0];
$template = $super_admin->template_selected;

require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';
Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));

$type = $_REQUEST['type'];
if($type == 'section'){
   $customerId = $_SESSION['customerID'];
   $section = $_REQUEST['section'];
   $page = $_REQUEST['page'];
   $status = $_REQUEST['status'];
   $upd = $ajaxclientCont->UpdatePageSection($customerId,$section,$page,$status);
   echo '<script type="text/javascript">
                alert("Succces!");              
window.location = "manage-home-page.php";
            </script>';
    exit;
}

?>

<div id="add_prop">
    <html lang="en">
        <head>
             <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo urlencode($key); ?>"></script>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE HOME PAGES
                                    <small>View and manage your Home pages</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Dashboard</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="manage-home-page.php">Home Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Home Page Content</span>
                            </li>
                        </ul>                     
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> Edit Home Page </span>
                                            <h5>Note: If you not require section award, testimonial etc  blank it.</h5>

                                        </div>
                                       <!-- <span style="float:right;"><a href="manage-custom-section.php?pageURL=index"><input type="button" style="background:#009688;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true" value="Add More Section" /></a></span>-->
                                        <span style="float:right;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;margin-right: 11px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                    </div>
                                    <!-- ----------Start FORM------------------------------------->
                                    <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Meta Title
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                            <span class="help-block"> Provide Meta Title </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Meta Description</label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />
                                                    <input type="hidden" class="form-control" name="pagename" value="manage-home-page" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends --------------------------------------------------------------------->
                                <?php                              
                                switch ($template) {
                                    case "multiple1":                                       
                                        include 'templates/home/home-1.php';
                                        break;
                                    case "multiple2":                                      
                                          include 'templates/home/home-1.php';
                                        break;
                                    case "multiple3":                                      
                                          include 'templates/home/home-4.php';
                                        break;
                                     case "multiple4":                                      
                                          include 'templates/home/home-5.php';
                                        break;
                                    case "single1":                                      
                                          include 'templates/home/home-2.php';
                                        break;
                                    case "single2":                                      
                                          include 'templates/home/home-3.php';
                                        break;
                                        case "single":                                      
                                          include 'templates/home/home-2.php';
                                        break;
                                    
                                    default:
                                       // echo "Your favorite color is neither red, blue, nor green!";
                                }
                                ?>


                                <div class="portlet box yellow">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Home Page Preview </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>                                      
                                            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <iframe src="<?php echo $base_url; ?>" height="600" style="width: 100%;" ></iframe>                                   
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
</body>
<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

<!-- BEGIN CORE PLUGINS -->
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
                                                function currentDiv(n) {
                                                    showDivs(slideIndex = n);
                                                }
                                                function showDivs(n) {
                                                    var i;
                                                    var x = document.getElementsByClassName("mySlides");
                                                    var dots = document.getElementsByClassName("demo");
                                                    if (n > x.length) {
                                                        slideIndex = 1
                                                    }
                                                    if (n < 1) {
                                                        slideIndex = x.length
                                                    }
                                                    for (i = 0; i < x.length; i++) {
                                                        x[i].style.display = "none";
                                                    }
                                                    for (i = 0; i < dots.length; i++) {
                                                        dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
                                                    }
                                                    x[slideIndex - 1].style.display = "block";
                                                    dots[slideIndex - 1].className += " w3-opacity-off";
                                                }
</script>      
<style>
    span{
        font-size:15px;
    }
    .box{
        padding:60px 0px;
    }

    .box-part{
        background:#FFF;
        border-radius:0;
        padding:60px 10px;
        margin:30px 0px;
    }
    .text{
        margin:20px 0px;
    }

    .fa{
        color:#4183D7;
    }
</style>

<?php
admin_footer();
?>
</html>
</div>
