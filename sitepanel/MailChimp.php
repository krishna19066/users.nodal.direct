<?php
include "admin-function.php";
checkUserLogin();

require_once('mailchimp/MC_OAuth2Client.php');
require_once('mailchimp/MC_RestClient.php');
require_once('mailchimp/miniMCAPI.class.php');
$client = new MC_OAuth2Client();

$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$roomDataCount = new propertyData();
$getresult_1 = $inventoryCont->getMailChimpData($customerId);
$resCount = count($getresult_1);
//print_r($getbookingEngine_1);
$getresult_1 = $getresult_1[0];
$status = $getresult_1->status;
$apikey = $getresult_1->api_key;
$login_name = $getresult_1->login_name;


if (isset($_GET['type'])) {
    $type = $_REQUEST['type'];
    if ($type == 'connect') {
        $sta = 'Y';
    } else {
        $sta = 'N';
    }
    $upd = $inventoryCont->UpdateMailChimpStatus($sta, $customerId);
    header('location:MailChimp.php');
}
?>

<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>

<?php
session_start();
?>
<html>
    <head>
        <style>
            .card{
                margin:50px;
                float:left;
                position:relative;             
                color:white;
                font-weight:bold;
            }

            .card input[type="radio"]
            {
                margin-top:-35px;
                margin-bottom:10px;
            }

            .card i{			
                font-size:55px;
                z-index:2;
                margin-top: 50%;
            }
            .overly{
                display:none;
                text-align:center;
                top:0;
                width:100%;
                height:300px;
                position:absolute;
                background:rgba(0, 0, 0, 0.5);
                color:#fff;			
            }

            .dn{
                display:none;
            }
            .db{
                display:block;
            }
            .card > :nth-child(3){
                display:block;
            }
        </style>
        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Connect To Mail Chimp</span>
                                                <span class="caption-helper">MailChimp</span>
                                            </div>
                                        </div>
                                        <!--<span style="float:left;"> <a href="booking-engine.php"><button style="background:#FF9800;color:white;border:none;height:35px;width:160px;font-size:14px; margin-top:35px;"><i class="fa fa-plus"></i> &nbsp Booking Engine</button></a></span><br><br>-->
                                       <!-- <span style="float:right;"> <a href="manage_rates.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Rates</button></a></span><br><br>-->
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="form-body">
                                            <div class="body">
                                                <div class="property portlet light bordered">
                                                    <?php if ($resCount > 0) { ?>
                                                        <?php
                                                        //$apikey = $session['access_token'] . '-' . $data['dc'];
                                                        $api = new MCAPI($apikey);
                                                        $api->useSecure(true);
                                                        $lists = $api->lists('', 0, 5);
                                                        $res = $lists['data'];
                                                        //print_r($res);
                                                        foreach ($lists['data'] as $list) {
                                                            ?>
                                                                                            <!--<li><?= $list['name'] ?> with <?= $list['stats']['member_count'] ?> subscribers <?= $list['id'] ?></li>-->
                                                            <?php
                                                        }
                                                        ?>
                                                        <!--<h3><img src="https://preview.ibb.co/bV0uQL/Mail-Chimp.png" style="width:150px;height: 139px;"></h3>-->

                                                        <!-- BEGIN Portlet PORTLET-->
                                                        <div class="portlet light">
                                                            <div class="portlet-title">
                                                                <div class="caption font-green-sharp">
                                                                    <i class="icon-speech font-red-sunglo"></i>
                                                                    <span class="caption-subject bold uppercase">MailChimp</span>
                                                                    <span class="caption-helper"></span>
                                                                </div>
                                                                <div class="actions">
                                                                    <?php if ($status == 'Y') { ?>
                                                                        <a href="MailChimp.php?type=disconnect" onClick="return confirm('Are you sure you want to disconnect MailChimp')" class="btn  btn-danger">
                                                                            <i class="fa fa-connectdevelop"></i> Disconnect </a>
                                                                    <?php } else { ?>
                                                                        <a href="MailChimp.php?type=connect" onClick="return confirm('Are you sure you want to Connect MailChimp')" class="btn  btn-success">
                                                                            <i class="fa fa-connectdevelop"></i> Reconnect </a>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php if ($status == 'Y') { ?>
                                                               <!-- <div class="portlet-body">
                                                                    <div class="scroller" style="height:200px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                                                                        <h4 style="font-weight:bold;"><?= $list['name'] ?> (<?php echo $login_name; ?>)</h4>
                                                                        <div class="actions" style="float:right">
                                                                            <a href="mailchimpInsertData.php" class="btn btn-circle btn-info">
                                                                                <i class="fa fa-refresh"></i> Refresh </a>  </div>
                                                                        <p> Total subscribers : <span style="color:green;"><?= $list['stats']['member_count'] ?> </span></p>
                                                                    </div>
                                                                </div>-->

                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col"><img src="https://preview.ibb.co/bV0uQL/Mail-Chimp.png" style="width:110px;height: 105px;"></th>
                                                                            <th scope="col"><span style="color:green;">Connected</span></th>
                                                                            <th scope="col"> <a href="mailchimpInsertData.php" class="btn  btn-info">
                                                                                <i class="fa fa-refresh"></i> Refresh </a></th>
                                                                            <th scope="col"> <?php if ($status == 'Y') { ?>
                                                                                <a href="MailChimp.php?type=disconnect" onClick="return confirm('Are you sure you want to disconnect MailChimp')" class="btn btn-danger" style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;">
                                                                            <i class="fa fa-connectdevelop"></i> Disconnect </a>
                                                                    <?php } else { ?>
                                                                        <a href="MailChimp.php?type=connect" onClick="return confirm('Are you sure you want to Connect MailChimp')" class="btn btn-success">
                                                                            <i class="fa fa-connectdevelop"></i> Reconnect </a>
                                                                    <?php } ?></th>
                                                                        </tr>
                                                                    </thead>
                                                                    
                                                                </table>

                                                                <h5>Synchronize your Nodal Direct User lists with MailChimp to send automated, customized emails to your guests or a general newsletter. You can disconnect later if you choose. </h5>
                                                            <?php } ?>
                                                        </div>
                                                        <!-- END Portlet PORTLET-->
                                                    </div>

                                                <?php } else { ?>
                                                    <h3>You can connect MailChimp here:<span style="color: #009688;"> <a href="<?= $client->getLoginUri() ?>">Connect To MailChimp</a></span></h3>
                                                <?php } ?> 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#property').on('change', function () {
            var propertyID = $(this).val();
            if (propertyID) {
                $.ajax({
                    type: 'POST',
                    url: 'property_room.php',
                    data: 'property_id=' + propertyID,
                    success: function (html) {
                        $('#room').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#room').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>
<script language="JavaScript">
    $('#select-all').click(function (event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';         ?>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>