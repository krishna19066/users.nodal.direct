<?php
include "admin-function.php";

$title = "Admin Login Panel";

if ($_REQUEST['id1'] == "Logout") {

    session_destroy();
    header("Location:$PHP_SELF");
}
if (isset($_SESSION['MyCpanel'])) {
    header("Location:welcome.php");
} else {

    if ($_POST['id1'] == "login") {
        $userCont = new indexData();

        $login_id = $_POST['login_id'];
        $password = $_POST['password'];
        $user_type = $_POST['user_type'];
		if ($user_type != 'user') {

        $userSelData = $userCont->authUserLogin($login_id, $password, $user_type);
        $userNumRows = count($userSelData);

        if ($userNumRows > 0) {
            $ipaddress = $userCont->getUserIP();          //------------- Get User IP Address----------//

            $date_log = date('Y-m-d');           //--------------------- Get Date --------------------//
            $time_log = date("h:i:sa");           //--------------------- Get Time --------------------//

            $_SESSION['MyCpanel'] = $userSelData->last_login;     //----------------------
            $_SESSION['MyAdminUserType'] = $userSelData->user_type;   //                    --
            $_SESSION['MyAdminUserID'] = $userSelData->user_id;    //					  -----------  Start Useful Login Sessions
            $_SESSION['MyAdminCountry'] = $userSelData->country;    //                    -----------			Here
            $_SESSION['MyAdminUserSLNO'] = $userSelData->slno;    //                    --
            $_SESSION['customerID'] = $userSelData->customerID;    //----------------------

            $toBeUsedCustID = $userSelData->customerID;
            $toBeUsedEmail = $userSelData->email;

            $insertLoginLog = $userCont->updateLoginLog($login_id, $ipaddress, $date_log, $time_log, $toBeUsedCustID); //----------------- Update Login Log Table --------------//

            //$sendLoginMail = $userCont->sendLoginMail($login_id, $ipaddress, $date_log, $time_log, $toBeUsedCustID, $toBeUsedEmail); //-------- Send Login Mail to Admin --------//

            $updateAdminData = $userCont->updateAdmindetail($login_id, $password, $user_type, $date_log, $time_log, $toBeUsedCustID); //----------- Update Admin Data -------------//

            if ($_SESSION['MyAdminUserType'] == "admin") {
                header("location:welcome.php");
                exit;
            } else {
                $redirect_url = "staff_home.php";
                header("location:" . $redirect_url);
                exit;
            }
        } else {
            $_SESSION['session_message'] = "Invalid Username/Password!! ";
            header("Location:index.php");
            exit();
        }
    }else {
                $userSelData = $userCont->authSubUserLogin($login_id, $password);
                $userNumRows = count($userSelData);
                $ipaddress = $userCont->getUserIP();          //------------- Get User IP Address----------//
                $date_log = date('Y-m-d');           //--------------------- Get Date --------------------//
                $time_log = date("h:i:sa");           //--------------------- Get Time --------------------//

                $_SESSION['MyCpanel'] = $userSelData->last_login;    
                $_SESSION['MyAdminUserType'] = $userSelData->user_type;   
                $_SESSION['MyAdminUserID'] = $userSelData->user_id;                   
                $_SESSION['customerID'] = $userSelData->customerID;
                $_SESSION['mode'] = $userSelData->mode;

                $toBeUsedCustID = $userSelData->customerID;
                $toBeUsedEmail = $userSelData->email;

                $insertLoginLog = $userCont->updateLoginLog($login_id, $ipaddress, $date_log, $time_log, $toBeUsedCustID); //----------------- Update Login Log Table --------------//

                //$sendLoginMail = $userCont->sendLoginMail($login_id, $ipaddress, $date_log, $time_log, $toBeUsedCustID, $toBeUsedEmail); //-------- Send Login Mail to Admin --------//

                $updateAdminData = $userCont->updateAdmindetail($login_id, $password, $user_type, $date_log, $time_log, $toBeUsedCustID); //----------- Update Admin Data -------------//

                if ($userNumRows > 0) {
                    header("location:welcome.php");
                    exit;
                } else {
                    $redirect_url = "index.php";
                    header("location:" . $redirect_url);
                    exit;
                }
               
               
        }
	}
    ?>

    <?php
    /* session_start();
      if(isset($_POST['forgot_pass']))
      {
      $email = $_POST['email'];
      $mail_check = "select * from admin_details where email='$email'";
      $mail_quer = mysql_query($mail_check);
      $mail_num = mysql_num_rows($mail_quer);
      //echo $mail_num;
      if($mail_num == "0")
      {
      echo "<script> alert('Given mail address is not registered with us.'); </script>";

      }
      else
      {
      while($mail_row = mysql_fetch_array($mail_quer))
      {
      $name = $mail_row['name'];
      $random_num = $mail_row['random'];
      $email1 = $mail_row['email'];
      }
      $subject2 = "Regarding Request for Password Change";
      $mes = "Dear ".$name."<br><br><br>";
      $mess2.= $mes;
      $to_mail = $email1;
      //$to_email="tech@theperch.in";
      //$subject1="Call Enquiry from ThePerch.in";
      $from = "tech@theperch.in";
      $mess2.= "We have Recieved your request for the password change successfully. <br /><br />";
      $mess2.= "Please visit the following link to reset your password for The Perch CMS. <br /><br />";
      $mess2.= "Link : ".SITE_ADMIN_URL."/change_pass.php?tp=log_pass&id=".$random_num."<br /><br />";
      $mess2.= "Thank You <br /><br /><br>";
      $mess2.= "Regards <br /><br />";
      $mess2.= "The Perch CMS Team<br /><br /><br /><br />";


      $headers = "From:".$from."\r\n";
      $headers.= "MIME-Version: 1.0\r\n";
      $headers.= "Content-Type: text/html; charset=utf-8\r\n";
      $headers.= "X-Priority: 1\r\n";
      mail($to_mail, $subject2, $mess2, $headers);
      $_SESSION['res_pass'] = "Your Instructions for resetting the password has been sent to your mail address.";
      }
      } */
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Nodal Direct CMS</title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1" name="viewport" />
            <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
            <meta content="" name="author" />
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN PAGE LEVEL STYLES -->
            <link href="assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />

            <!-- END PAGE LEVEL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <!-- END THEME LAYOUT STYLES -->            
           
            <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-96x96.png">
        </head>

        <body class=" login">
            <!-- BEGIN : LOGIN PAGE 5-1 -->
            <div class="user-login-5">
                <div class="row bs-reset">
                    <div class="col-md-6 bs-reset mt-login-5-bsfix">
                        <div class="login-bg" style="background-image:url(assets/pages/img/login/bg1.jpg)">
                            <img class="login-logo" src="https://uploads-ssl.webflow.com/5a706a79d47c3d000124fa17/5b89c4b937ffb3620ca121d9_logo2.jpg" style="width:213px;height:78px;" /> </div>
                    </div>

                    <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                        <div class="login-content">
                            <h1>Nodal Direct CMS Login</h1>
                            <?php
                            if (isset($_SESSION['res_pass'])) {
                                echo "<br><font style='color:#00688B'>" . $_SESSION['res_pass'] . "</font>";
                                unset($_SESSION['res_pass']);
                            }
                            if (isset($_SESSION['wr_mail'])) {
                                echo "<br><font style='color:#00688B'>" . $_SESSION['wr_mail'] . "</font>";
                                unset($_SESSION['wr_mail']);
                            }
                            ?>

                            <form action="" class="login-form" method="post">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button>
                                    <span>Enter any username and password. </span>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="login_id" value="<?= $adm_login_id ?>" required/> 
                                    </div>

                                    <div class="col-xs-6">
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="password" value="<?= $_POST['password'] ?>" required/> 
                                    </div>

                                    <div class="col-xs-6">	
                                        <input name="user_type" type="radio" value="admin" checked="checked" />Administrator &nbsp;
										<input name="user_type" type="radio" value="user" />User &nbsp; 
                                        <input name="user_type" type="radio" value="staff" />Admin Staff
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="rem-password">
                                            <label class="rememberme mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" name="remember" value="1" /> Remember me
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-8 text-right">
                                        <div class="forgot-password">
                                            <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                                        </div>
                                        <input type="hidden" name="id1" value="login" />
                                        <input class="btn green" name="login_sub" type="submit" value="Sign In" />
                                    </div>
                                </div>
                            </form>

                            <!-- BEGIN FORGOT PASSWORD FORM -->
                            <form class="forget-form" action="" method="post" style="display:none;">
                                <h3 class="font-green">Forgot Password ?</h3>
                                <p> Enter your e-mail address below to reset your password. </p>
                                <div class="form-group">
                                    <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                                <div class="form-actions">
                                    <button type="button" id="back-btn" class="btn green btn-outline">Back</button>

                                    <input type="submit" class="btn btn-success uppercase pull-right" name="forgot_pass" value="Submit">
                                </div>
                            </form>
                            <!-- END FORGOT PASSWORD FORM -->
                        </div>

                        <div class="login-footer">
                            <div class="row bs-reset">
                                <div class="col-xs-5 bs-reset">
                                    <ul class="login-social">
                                        <li>
                                            <a href="https://www.facebook.com/nodaldirect/" target="_blank" title="Facebook">
                                                <i class="icon-social-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/nodaldirect" target="_blank" title="Twitter">
                                                <i class="icon-social-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/channel/UCy9bQVSP1hFsT7jguzyvEnQ" target="_blank" title="Youtube">
                                                <i class="icon-social-youtube"></i>
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </div>
                                <div class="col-xs-7 bs-reset">
                                    <div class="login-copyright text-right">
                                        <p>© Nodal Direct All Rights Reserved. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        function validate_user() {
                            var frm = document.loginFrm;
                            if (frm.login_id.value == "") {
                                alert("Enter Username!!");
                                frm.login_id.focus();
                                return false;
                            }
                            if (frm.password.value == "") {
                                alert("Enter Password!!");
                                frm.password.focus();
                                return false;
                            }
                        }
                    </script>
                </div>
            </div>


            <!-- BEGIN CORE PLUGINS -->
            <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <!-- END THEME LAYOUT SCRIPTS -->

            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="assets/pages/scripts/login.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->

        </body>
    </html>

    <?php
    //admin_footer();
}
?>


<!--358bf058488c752775a75de198a9fe0b-->