<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$recordID = $_REQUEST[recordID];
//$roomID = $_REQUEST[roomID];
$id = $_REQUEST['id'];
//$conect_cloud = $cloud_data->connectCloudinaryAccount($customerId);
$get_testimonial = $propertyCont->GetTestimonialDataWithId($recordID);
//$get_room = $propertyCont->getRoomDetailData($recordID, $roomID);
//$superAdmin_data = $propertyCont->GetSupperAdminData($customerId);
//$superAdmin_res = $superAdmin_data[0];
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $bucket = $cloud_keyData->bucket;
?>
<?php
$redirect_script_name = "view-property-rooms.php?recordID=''";
@extract($_REQUEST);

//------------------------Add Testimonial---------------------------------------------------->
if (isset($_POST['Add_testimonial'])) {
    $cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    $record_type = "Property Photo";
    $script_name = "view-property-photo.php";
    $table_name = "propertyImages";

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $review = $_POST['review'];
    $name = $_POST['name'];
    $categoryName = $_POST['categoryName'];
    $file = $_FILES['image_org']['name'];
    $pro_id = $_POST['propertyID'];
    $video_review = $_POST['video_review'];

    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    if ($file) {
        $upload = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/testimonials"));
    }

    //$ins = "insert into testimonials(customerID,review,photo,name,detail) values('$customerId','$review','$timestmp','$name','$occupation')";
    // $quer = mysql_query($ins);
    $ins = $propertyCont->AddTestimonialData($customerId, $pro_id, $review, $name, $categoryName, $video_review, $timestmp);

    echo '<script type="text/javascript">
                alert("Succesfuly Added Testimonial");              
window.location = "manage-testimonial.php";
            </script>';
    exit;
}
if (isset($_POST['update_testimonial'])) {

    $cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));

    $file = $_FILES['image_org']['name'];
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    if ($file) {
        $upload = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/testimonials"));
    }
    $review = $_POST['review'];
    $occupation = $_POST['occupation'];
    $name = $_POST['name'];
    $update_testi = $propertyCont->UpdateTestimonialData($recordID, $timestmp);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated ");              
window.location = "edit-testimonials.php?id=edit&recordID=' . $recordID . '";
            </script>';
}
//admin_header();	
?>

<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">

                                <br><br><br>
                                <div class="portlet light bordered" style="height: 137px;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Add Testimonial</span>
                                            <span class="caption-helper">Add Testimonial</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="!Add Testimonial Those You Show your website!" data-original-title="Add Testimonial" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Testimonial" /></span>
                                </div>

                                <div class="portlet light bordered" id="add_photo" style="display:none;" >	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" <?php if($bucket){ ?>action="S3-createObject.php" <? } else{ ?>  action="" <?php } ?> style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Review</label>
                                                    <span class="required"> * </span>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" name="review" rows="6" required> <?= $res_testi->review; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Reviewer Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="name" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide Reviewer Name</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Category Name

                                                    </label>
                                                    <div class="col-md-4">
                                                        <select name="categoryName" id="property" class="form-control" required="">
                                                            <option value="">--- Select Category ---</option>
                                                            <?php
                                                            $cateList1 = $propertyCont->getTestionialCategory($customerId);
                                                            foreach ($cateList1 as $cat) {
                                                                ?>
                                                                <option value="<?php echo $cat->category_name; ?>"><?php echo $cat->category_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-block"> Provide Testimonial Category</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Reviewer Image

                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image_org"> 
                                                        </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a><br>
                                                        <b>Selected Photo :</b> 

                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="submit" class="btn green" name="Add_testimonial" class="button" id="submit" value="Add Testimonial" />

                                                <a href="javascript:history.back()" class="btn red">Go Back</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                            <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->

                            <?php
                             $testimonialData = $propertyCont->GetAllTestimonialData($customerId);
                                if ($testimonialData != NULL) {
                                    $cnt = 1;
                                    $sl = $start + 1;
                                    foreach ($testimonialData as $dataRes) {
                                        // $dataRes = ms_htmlentities_decode($dataRes);
                                        if ($sl % 2 != 0) {
                                            $color = "#ffffff";
                                        } else {
                                            $color = "#f6f6f6";
                                        }
                                        if ($cnt == "1") {
                                            $color = "#32c5d2";
                                        }
                                        if ($cnt == "2") {
                                            $color = "#3598dc";
                                        }
                                        if ($cnt == "3") {
                                            $color = "#36D7B7";
                                        }
                                        if ($cnt == "4") {
                                            $color = "#5e738b";
                                        }
                                        if ($cnt == "5") {
                                            $color = "#1BA39C";
                                        }
                                        if ($cnt == "6") {
                                            $color = "#32c5d2";
                                        }
                                        if ($cnt == "7") {
                                            $color = "#578ebe";
                                        }
                                        if ($cnt == "8") {
                                            $color = "#8775a7";
                                        }
                                        if ($cnt == "9") {
                                            $color = "#E26A6A";
                                        }
                                        if ($cnt == "10") {
                                            $color = "#29b4b6";
                                        }
                                        if ($cnt == "11") {
                                            $color = "#4B77BE";
                                        }
                                        if ($cnt == "12") {
                                            $color = "#c49f47";
                                        }
                                        if ($cnt == "13") {
                                            $color = "#67809F";
                                        }
                                        if ($cnt == "14") {
                                            $color = "#8775a7";
                                        }
                                        if ($cnt == "15") {
                                            $color = "#73CEBB";
                                        }
                                        ?>

                                        <div class="col-md-12">
                                            <div class="m-heading-1 m-bordered" style="border-left:10px solid <?php echo $color; ?>;border-right:1px solid <?php echo $color; ?>;border-bottom:1px solid <?php echo $color; ?>;border-top:1px solid <?php echo $color; ?>;">

                                                <div style="border:1px solid <?php echo $color; ?>; color:#fff; background:<?php echo $color; ?>; padding-left:12px;">

                                                    <p> <b><span style="font-size:large"><?php echo $dataRes->name; ?></span></b>
                                                        <span style="color:#fff; float:right; font-size:x-large">
                                                            <span id="btn_div<?php echo $cnt; ?>">
                                                                <a href="edit-testimonials.php?id=edit&recordID=<?php echo $dataRes->slno; ?>" style="color:white;"><button type="button" name="<?php echo $cnt; ?>" class="btn" style="width:95px;background:black;">Edit</button></a>
                                                            </span>

                                                            <span id="btn_div<?php echo $cnt; ?>">
                                                                <a href="edit-testimonials.php?id=delete&recordID=<?php echo $dataRes->slno; ?>" style="color:white;"><button type="button" name="<?php echo $cnt; ?>" class="btn red" style="width:95px;">Delete</button></a>
                                                            </span>
                                                        </span><br>
                                                </div>

                                                <div class="row" style="padding:10px;">
                                                    <div class="col-md-8">	
                                                        <table style="width:100%;">
                                                            <tr>
                                                                <td style="width:180px;">
                                                                    <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $dataRes->photo; ?> <? }else{ ?> http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_160,h_100,c_fill/reputize/testimonials/<?php echo $dataRes->photo; ?>.jpg <?php } ?>" style="width:160px;height: 100px;" />
                                                                </td>

                                                                <td>
                                                                    <span>
                                                                        <b>Review: </b> <?php echo strip_tags(trim(ucfirst(substr($dataRes->review, 0, 400)))); ?>.</span>	</br><br>
                                                                    <span><b>Category: </b><?php echo strip_tags(trim(ucfirst(substr($dataRes->category_name, 0, 80)))); ?>.</span>	
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $cnt++;
                                    }
                                } else {
                                    ?>
                                    <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                        <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                    </table>
                                    <?php
                                }
                           
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<?php
admin_footer();
?>