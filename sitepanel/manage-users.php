<?php
include "admin-function.php";

// include "cms_functions.php";
// user_locked();
// staff_restriction();
$customerId = $_SESSION['customerID'];
$analyticsCont = new analyticsPage();
$getusers = $analyticsCont->getUsersData($customerId);

$reccnt = count($getusers);
$start = (intval($start) == 0 or $start == "") ? $start = 0 : $start;
$pagesize = intval($pagesize) == 0 ? $pagesize = 10 : $pagesize;
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }
                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }
                tr:nth-child(odd){
                    background-color:#fff;
                }
                th {
                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                </style>

                <!-- BEGIN THEME LAYOUT STYLES -->
                <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
                <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE USERS
                                    <small>Manage your blog author here..</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Analytics & SEO</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Meta Tags</span>
                            </li>
                        </ul>

                        <div id="top_menu_container">
                            <ul id="top_menu">
                                <a href="manage-users.php" style="text-decoration:none;"><li class="OurMenuActive">Manage Users</li></a>


                            </ul>
                        </div>

                        <div id="cityCont">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                                <a class="pull-right">
                                                    <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_user();"><i class="fa fa-plus"></i> &nbsp Add User</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <?php
                                if ($reccnt > 0) {
                                    ?>
                                    <div class="portlet-body">
                                        <div class="mt-element-card mt-element-overlay">
                                            <div class="row">
                                                <?php
                                                $count = 1;
                                                $sl = $start + 1;
                                                // while ($dataRes = mysql_fetch_array($qry)) {
                                                //  $dataRes = ms_htmlentities_decode($dataRes);
                                                foreach ($getusers as $dataRes) {
                                                    if ($sl % 2 != 0) {
                                                        $color = "#ffffff";
                                                    } else {
                                                        $color = "#f6f6f6";
                                                    }
                                                    $pic = $dataRes->photo;
                                                    ?>

                                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                        <div class="mt-card-item">
                                                            <div class="mt-card-avatar mt-overlay-1">
                                                                <?php
                                                                if ($pic == '') {
                                                                    ?>	
                                                                    <img src="https://u.o0bc.com/avatars/no-user-image.gif">
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <img src="http://res.cloudinary.com/the-perch/image/upload/g_face,w_400,h_400,c_fill/admin_user/<?php echo $pic; ?>.jpg">
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <div class="mt-overlay">
                                                                            <ul class="mt-info">
                                                                                <li>
                                                                                    <a class="btn default btn-outline" href="<?= SITE_ADMIN_URL ?>/add-user.php?id=edit&recordID=<?= $dataRes->slno; ?>">
                                                                                        <i class="fa fa-edit"></i>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a class="btn default btn-outline" href="<?= $script_name; ?>?id=delete&recordID=<?= $dataRes->slno ?>" onClick="return confirm('Are you sure you want to delete this record')">
                                                                                        <i class="fa fa-trash"></i>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        </div>
                                                                        <div class="mt-card-content clear">
                                                                            <h3 class="mt-card-name"><?= $dataRes->name; ?></h3>
                                                                            <p class="mt-card-desc font-grey-mint"><?= $dataRes->email; ?></p>
                                                                            <p class="mt-card-desc font-grey-mint" style="display:inline; padding-left:-10px;"><?php echo ucwords($dataRes->user_type); ?></p><br>
                                                                                <div class="btn-group">
                                                                                    <?php
                                                                                    if ($dataRes->status == "Y") {
                                                                                        ?>
                                                                                        <div id="btn_div<?php echo $count; ?>">
                                                                                            <button type="button" name="<?php echo $count; ?>" value="manage-users.php?id=deactivate&recordID=<?= $dataRes->slno; ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;">Deactivate</button>
                                                                                        </div>
                                                                                        <?php
                                                                                    } else if ($dataRes->status == 'N') {
                                                                                        ?>
                                                                                        <div id="btn_div<?php echo $count; ?>">
                                                                                            <button type="button" name="<?php echo $count; ?>" value="manage-users.php?id=activate&recordID=<?= $dataRes->slno; ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;background:#36c6d3;border:1px solid #36c6d3;">Activate</button>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                    ?>

                                                                                </div></br></br>
                                                                        </div>
                                                                        </div>
                                                                        </div>

                                                                        <?php
                                                                        $count++;
                                                                    }
                                                                    ?>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    <div>
                                                                    <?php } else {
                                                                        ?>
                                                                        <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                                                            <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                                                        </table>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                </body>
                                                                </html>

                                                                <script>
                                                                    $('#top_menu li').click(function () {
                                                                        $('#top_menu li').removeClass('OurMenuActive');
                                                                        $(this).addClass('OurMenuActive');
                                                                    });
                                                                </script>

                                                                <script>
                                                                    function load_add_user() {
                                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                        $('#cityCont').html(loadng);


                                                                        //var replace = 'btn_div' + but;

                                                                        $.ajax({url: 'ajax_lib/ajax_add_user.php',
                                                                            type: 'post',
                                                                            success: function (output) {
                                                                                // alert(output);
                                                                                $('#cityCont').html(output);
                                                                            }
                                                                        });
                                                                    }
                                                                </script>
                                                                <script>
                                                                    function load_analytic_code() {
                                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                        $('#cityCont').html(loadng);


                                                                        //var replace = 'btn_div' + but;

                                                                        $.ajax({url: 'ajax_lib/ajax_add_analytic_code.php',
                                                                            type: 'post',
                                                                            success: function (output) {
                                                                                // alert(output);
                                                                                $('#cityCont').html(output);
                                                                            }
                                                                        });
                                                                    }
                                                                </script>

                                                                <script>
                                                                    function load_property_type() {
                                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                        $('#cityCont').html(loadng);


                                                                        //var replace = 'btn_div' + but;

                                                                        $.ajax({url: 'ajax_lib/ajax_property_type.php',
                                                                            type: 'post',
                                                                            success: function (output) {
                                                                                // alert(output);
                                                                                $('#cityCont').html(output);
                                                                            }
                                                                        });
                                                                    }
                                                                </script>
                                                                <!-- BEGIN PAGE LEVEL PLUGINS -->
                                                                <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                                                                <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                                                                <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                                                                <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                                                                <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                                                                <!-- END PAGE LEVEL PLUGINS -->
                                                                <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                                                <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
                                                                <!-- END PAGE LEVEL SCRIPTS -->
                                                                <?php
                                                                admin_footer();
                                                                ?>        
                                                                </div>	