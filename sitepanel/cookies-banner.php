<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$res = $cityListings1[0];
//$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];

$cloud_cdnName = $cloud_keyData->cloud_name;
$user_email = $cloud_keyData->email;
$hotel_name = $cloud_keyData->companyName;
$phone = $cloud_keyData->phone;
$language = $cloud_keyData->language;

$cookies_data = $propertyCont->GetCookiesBannerData($customerId);
$cookies_res = $cookies_data[0];
$status = $cookies_res->status;
$link = $cookies_res->link;
$text = $cookies_res->text;
$count_no = count($cookies_data);

$super_admin_1 = $propertyCont->GetSupperAdminData($customerId);
$super_admin = $super_admin_1[0];
//$status = $super_admin->status;
if (isset($_POST['update_cookies_banner'])) {
    $cookies_content = $_REQUEST['cookies_content'];
    $link = $_REQUEST['link'];
    $cookies = $_REQUEST['cookies'];
    if ($cookies == '') {
        // echo 'dis';
        $status = 'N';
    } else {
        // echo 'something';
        $status = 'Y';
    }
    if ($count_no > 1) {

        $upd = $propertyCont->UpdateCookiesBanner($customerId, $cookies_content, $link, $status);
    } else {
        $ins = $propertyCont->InsertCookiesBanner($customerId, $cookies_content, $link, $status);
    }
    echo '<script type="text/javascript">
                alert("Your Site Cookies has been successfuly Enable");              
window.location = "cookies-banner.php";
            </script>';
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Cookies - <span style="color:#e44787;"> Banner</span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Cookies & Visitor Data</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><h3>Cookies Banner</h3></span>                 
                                        <span style="float:right;"> <a href=""><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>
                                        <!--<span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Video Gallery </button></a></span>-->
                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row"> 
                                            <div class="col-lg-12 col-md-612 col-sm-12 col-xs-12">
                                                <h3>Select Cookies & Visitor Data</h3>
                                                <p>Place a banner on your website to inform your visitors of cookies used by your website and to receive acknowledgement of visitors.</p> 
                                                <br/><hr/>
                                                <span id="res_time" style="font-size:12px;float: right;"><?php if ($status == 'Y') {
                                                            echo 'Cookies: <span style="color:green;">Enable</span>';
                                                        } else {
                                                            echo 'Cookies: <span style="color:red;">Disable</span>';
                                                        } ?></span><br/>
                                                <form action="" method="post" class="form-horizontal">
                                                    <div class="form-check question">
                                                        <input type="checkbox" class="form-check-input cookies_content" name="cookies" <?php if ($status == 'Y') { ?>  value="Y" <?php } else { ?> value="N" <?php } ?> <?php
                                                        if ($status == 'Y') {
                                                            echo 'checked';
                                                        } else {
                                                            echo '';
                                                        }
                                                        ?>  title="Enable Cookie Banner">
                                                        <label class="form-check-label" for="materialUnchecked">Enable Cookie Banner</label><br/>
                                                        <span style="font-size:12px; margin-left: 16px;">Place a banner on your website to inform your visitors of cookies used by your website and to receive acknowledgement of visitors.</span>
                                                    </div><br/>
                                                    <div class="form-group last answer">
                                                        <span style="font-size:12px; margin-left: 16px;">Enter the confirmation text you would like to show your visitors.</span><br/>                                                   
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="cookies_content" rows="6"><?php echo $text ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group answer">
                                                        <label class="control-label col-md-4"> Cookies link(Privacy Policy OR T & C) 
                                                            <!--<span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="link" value="<?php echo $link; ?>" />
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                        <!-- Material checked -->                                             
                                        <button type="submit" name="update_cookies_banner" style="background:#009688;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;margin-top:23px;">Update </button>                                            
                                        </form>

                                    </div>                                            
                                </div>
                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
            <div id="cookieConsent" class="answer">
                <div id="closeCookieConsent">x</div>
               <?php if($count_no > 1){ echo $text; }else{ ?> This website is using cookies.<?php } ?> <a href="#" target="_blank">More info</a>. <a class="cookieConsentOK">That's Fine</a>
            </div>
        </div>

        <style>
            /* Style the element that is used to open and close the accordion class */
            p.accordion {
                background-color: #efefef;;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                text-align: left;              
                border: none;
                outline: none;
                transition: 0.4s;
                margin-bottom:10px;

            }
            /* Add a background color to the accordion if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
            p.accordion.active, p.accordion:hover {
                background-color: #ddd;
            }
            /* Unicode character for "plus" sign (+) */
            p.accordion:after {
                content: '+';
                font-size: 18px;
                color: #777;
                float: right;
                margin-left: 5px;
            }
            /* Unicode character for "minus" sign (-) */
            p.accordion.active:after {
                content: "-";
            }
            /* Style the element that is used for the panel class */
            div.panel {
                padding: 0 18px;
                background-color: white;
                max-height: 0;
                overflow: hidden;
                transition: 0.4s ease-in-out;
                opacity: 0;
                margin-bottom:10px;
            }
            div.panel.show {
                opacity: 1;
                max-height: 500px; /* Whatever you like, as long as its more than the height of the content (on all screen sizes) */
            }
            /*Cookie Consent Begin*/
            #cookieConsent {
                background-color: rgba(20,20,20,0.8);
                min-height: 26px;
                font-size: 14px;
                color: #ccc;
                line-height: 26px;
                padding: 8px 0 8px 30px;
                font-family: "Trebuchet MS",Helvetica,sans-serif;
                position: fixed;
                bottom: 0;
                left: 0;
                right: 0;
                display: none;
                z-index: 9999;
            }
            #cookieConsent a {
                color: #4B8EE7;
                text-decoration: none;
            }
            #closeCookieConsent {
                float: right;
                display: inline-block;
                cursor: pointer;
                height: 20px;
                width: 20px;
                margin: -15px 0 0 0;
                font-weight: bold;
            }
            #closeCookieConsent:hover {
                color: #FFF;
            }
            #cookieConsent a.cookieConsentOK {
                background-color: #F1D600;
                color: #000;
                display: inline-block;
                border-radius: 5px;
                padding: 0 20px;
                cursor: pointer;
                float: right;
                margin: 0 60px 0 10px;
            }
            #cookieConsent a.cookieConsentOK:hover {
                background-color: #E0C91F;
            }
            /*Cookie Consent End*/
        </style>
        <script type="text/javascript">
            $(".answer").hide();
            $(".cookies_content").click(function () {
                if ($(this).is(":checked")) {
                    $(".answer").show();
                } else {
                    $(".answer").hide();
                }
            });

            $(document).ready(function () {
                setTimeout(function () {
                    $("#cookieConsent").fadeIn(200);
                }, 4000);
                $("#closeCookieConsent, .cookieConsentOK").click(function () {
                    $("#cookieConsent").fadeOut(200);
                });
            });
        </script>
<?php echo ajaxJsFooter(); ?>
    </body>
</html>