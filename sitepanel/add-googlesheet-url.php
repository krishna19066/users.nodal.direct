<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propCont = new propertyData();
$inventoryCont = new inventoryData();
//$fun_resData = $cityCont->get_Cloud_AdminDetails($customerId);
//print_r($resData);
$resData = $fun_resData[0];

//------------Update Base Url ------------------------------->
if (isset($_POST['submitUrl'])) {
    $propertyName = $_REQUEST['PropertyName'];
    $sheetUrl = $_REQUEST['sheetUrl'];
    $folderUrl = $_REQUEST['folderUrl'];
    $upd = $propCont->AddGoogleSheetUrl($customerId, $propertyName, $sheetUrl, $folderUrl);
    echo '<script type="text/javascript">
                alert("Succesfuly Added Url");              
window.location = "add-googlesheet-url.php";
            </script>';
    exit;
}
?>

<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>

            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Add Google Sheet Url
                                    <small>Add Sheet Url</small>
                                </h1>
                            </div>
                        </div>



                        <div id="top_menu_container">
                            <ul id="top_menu">

                            </ul>
                        </div>

                        <div id="cityCont">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Property Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select name="PropertyName" class="form-control">
                                                       
                                                        <?php
                                                        $propList1 = $inventoryCont->getPropertyListing($customerId);
                                                        foreach ($propList1 as $propList) {
                                                            ?>
                                                            <option value="<?php echo $propList->propertyName; ?>" <?php
                                                            if ($propList->propertyName == $property) {
                                                                echo "selected";
                                                            }
                                                            ?>> <?php echo $propList->propertyName; ?> </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Sheet Url
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="sheetUrl"  value="" placeholder="Enter Sheet Url." required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Folder Url
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="folderUrl"  value="<?= $resData->center_email; ?>" placeholder="Enter Folder Url." required />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">                          

                                                    <input type="submit" class="btn green" name="submitUrl" class="button" id="submit" value="Submit" />

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Portlet PORTLET-->
                </div>

            </div>
            </div>

        </body>
    </html>

    <script>
        $('#top_menu li').click(function () {
            $('#top_menu li').removeClass('OurMenuActive');
            $(this).addClass('OurMenuActive');
        });
    </script>

    <script>
        function load_upload_logo() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_upload-logo.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_fevicon() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_upload-fevicon.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_room_type() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_room_type.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_room_type() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_room_type.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_city() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_city.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_edit_city(cityid) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_edit_city.php',
                type: 'post',
                data: {cityid: cityid},
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_content_city(type_id) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_content_city.php',
                type: 'post',
                data: {type_id: type_id},
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_subcity() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add_subcity.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_add_propertytype() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add-propertytype.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_roomtype() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add-roomtype.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <?php
    admin_footer();
    ?>        
</div>	