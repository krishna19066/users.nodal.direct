<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
$propertyCont = new propertyData();
$inventoryCont = new inventoryData();
$admin = new adminFunction();
$ip = $admin->getUserIP();
$superAdmin_data = $propertyCont->GetSupperAdminData($customerId);
$superAdmin_res = $superAdmin_data[0];
$id = $_GET['id'];
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
?>

<?php
if ($id == "delete") {
    $recordID = $_GET['recordID'];
    //$upd = "update propertyTable set status='D' where propertyID='$recordID'";
    // $upd_que = mysql_query($upd);
    $upd = $propertyCont->DeletePropertyData($recordID);
    Header("location:manage-property.php");
}
?>
<?php
if (isset($_POST['update_currency'])) {
    $currency = $_POST['currency'];
    $propID = $_POST['propID'];
    $upd = $propertyCont->UpdateCurrency($currency, $propID);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated Currency");              
window.location = "manage-property.php";
            </script>';
}
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>


            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>         
            <div class="clearfix"> </div>       
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> PROPERTY DETAILS
                                    <small>View all the details of your Properties</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="manage-property.php">My Properties</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Property</span>

                            </li>
                        </ul>                                                                               
                        <div class="row">
                            <div class="col-md-12">

                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <?php
                                            if ($superAdmin_res->template_type == 'single') {

                                                $cityListings1 = $propertyCont->getCityList($customerId,$user_id);
                                                $totalCity = count($cityListings1);
                                                if ($totalCity > 0) {
                                                    ?>
                                                    <i class="icon-equalizer font-red-sunglo"></i>&nbsp;
                                                    <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Can't be add! Autherity only Single Property!" data-original-title="Information" style="color:#7d6c0b;font-size:20px;"></span>
                                                    <a class="pull-right" data-toggle="tooltip" data-placement="top" title="Can't be add! Autherity only Single Property">
                                                        <a href="ajax_add-property.php" data-step="4" data-intro="Add Property here!"> <button disabled style="background:#F44336;color:white;border:none;height:35px;width:160px;font-size:14px;cursor: not-allowed;"><i class="fa fa-plus"></i> &nbsp Add Property</button></a>
                                                        <button  style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;<?php if ($mode == 'V') { ?> cursor: not-allowed;" <?php } ?> onclick="load_add_property();" <?php if ($mode == 'V') { ?> disabled title="Only view mode" <?php } ?>><i class="fa fa-plus"></i> &nbsp Export Property</button>
                                                    </a>
                                                <?php } else { ?>
                                                    <a class="pull-right">
                                                        <button  style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; <?php if ($mode == 'V') { ?> cursor: not-allowed;" <?php } ?>  onclick="load_add_property();" <?php if ($mode == 'V') { ?> disabled title="Only view mode" <?php } ?>><i class="fa fa-plus"></i> &nbsp Add Property</button>

                                                    </a>
                                                <?php }
                                                ?>

                                            <?php } else { ?>
                                                <a class="pull-right">
                                                    <a href="ajax_add-property.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;float: right; <?php if ($mode == 'V') { ?> cursor: not-allowed;" <?php } ?> ><i class="fa fa-plus"></i> &nbsp Add Property</button></a>
                                                    <!--<button  style="background:#FF9800;color:white;border:none;height:35px;width:177px;font-size:14px;" id="export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> &nbsp Export CSV Property</button>-->

                                                    <a href="#" id ="export" role='button'><button <?php if ($mode == 'V') { ?> disabled title="Only view mode" <?php } ?> style="background:#FF9800;color:white;border:none;height:35px;width:177px;font-size:14px; float: right; margin-right: 10px; <?php if ($mode == 'V') { ?> cursor: not-allowed;" <?php } ?> ><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export CSV Property</button></a>

                                                </a>
                                            <?php } ?>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <form id="live-search" action="" class="styled" method="post">
                                                    <input type="text" class="text-input form-control" id="filter-text" placeholder="Enter Ciy Name OR Property Name OR Months" value="" name="filter-text" />
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!---------------------------------- CSV Export Code------------------------------------------------------------->
                        <div class='container' style="display:none;"> 
                            <div id="dvData">
                                <table>
                                    <tr>
                                        <th>Manger Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Property Name</th>
                                        <th>City</th>
                                        <th>Featured</th>
                                        <th>Status</th>
                                    </tr>
                                    <?php
                                    $propertyData = $inventoryCont->getPropertyListing($customerId,$user_id);
                                    foreach ($propertyData as $csvrow) {
                                        ?>
                                        <tr>
                                            <td><?php echo $csvrow->managerName; ?></td>
                                            <td><?php echo $csvrow->property_email; ?></td>
                                            <td><?php echo $csvrow->propertyPhone; ?> </td>
                                            <td><?php echo $csvrow->propertyName; ?> </td>
                                            <td><?php echo $csvrow->cityName; ?> </td>
                                            <td><?php echo $csvrow->feature; ?> </td>
                                            <td><?php echo $csvrow->status; ?> </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>

                        <!----------------------------------    End     ------------------------------------------------------------------------->

                        <div id="property_change">
                            <div class="row">
                                <div class="col-md-12">	

                                    <?php
                                    $cityListings1 = $propertyCont->getCityList($customerId,$user_id);
                                    foreach ($cityListings1 as $cityListings) {
                                        $cityName = $cityListings->cityName;
                                        ?>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-list-item" onclick="load_city_detail('<?php echo $cityName; ?>');"><br><br>
                                                    <div class="dashboard-stat2 bordered">
                                                        <div class="display">
                                                            <div class="number">
                                                                <h3 class="font-green-sharp">
                                                                    <span><?php echo $cityName; ?></span>
                                                                    <small class="font-green-sharp"></small>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="progress-info">
                                                            <div class="progress">
                                                                <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
                                                                    <span class="sr-only"><?php echo $cityName; ?></span>
                                                                </span>
                                                            </div>
                                                            <div class="status">
                                                                <div class="status-title">Total Properties : </div>
                                                                <?php
                                                                $propertyCount = $propertyCont->getPropertyCount($customerId, $cityName,$user_id);
                                                                $propCount = count($propertyCount);
                                                                ?>
                                                                <div class="status-number"><?php echo $propCount; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </body>

                                                <script>
                                                    $('#top_menu li').click(function () {
                                                        $('#top_menu li').removeClass('OurMenuActive');
                                                        $(this).addClass('OurMenuActive');
                                                    });
                                                </script>
                                                <script>
                                                    function load_city_detail(urldata) {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#property_change').html(loadng);

                                                        $.ajax({url: 'ajax_lib/ajax_property.php',
                                                            data: {cityData: urldata},
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#property_change').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>


                                                <script>
                                                    function load_add_property() {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#add_prop').html(loadng);

                                                        $.ajax({url: 'ajax_lib/ajax_add-property.php',
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#add_prop').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>

                                                <script>
                                                    function load_property_photo(propertyPhotoId) {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#add_prop').html(loadng);

                                                        $.ajax({url: 'ajax_lib/ajax_view-property-photo.php',
                                                            data: {propPhotoId: propertyPhotoId},
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#add_prop').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>

                                                <script>
                                                    function load_manage_question(propertyQuestionId) {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#add_prop').html(loadng);

                                                        $.ajax({url: 'ajax_lib/ajax_manage-question.php',
                                                            data: {propQuestionId: propertyQuestionId},
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#add_prop').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>

                                                <script>
                                                    function load_add_rooms(propertyRoomsId) {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#add_prop').html(loadng);

                                                        $.ajax({url: 'ajax_lib/ajax_add-rooms.php',
                                                            data: {propRoomsId: propertyRoomsId},
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#add_prop').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>

                                                <script>
                                                    function load_view_rooms(propRoomId) {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#add_prop').html(loadng);

                                                        $.ajax({url: 'ajax_lib/ajax_property-rooms.php',
                                                            data: {propRumId: propRoomId},
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#add_prop').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>

                                                <script>
                                                    function expand_me(id_name) {

                                                        var span_name = "toggle_expand" + id_name;
                                                        var data_name = "data_box" + id_name;

                                                        if (document.getElementById(span_name).innerHTML === "Expand") {
                                                            document.getElementById(span_name).innerHTML = "Collapse";
                                                            document.getElementById(data_name).style.display = "block";
                                                        } else {
                                                            document.getElementById(span_name).innerHTML = "Expand";
                                                            document.getElementById(data_name).style.display = "none";
                                                        }

                                                    }



                                                </script>
                                                <script type='text/javascript'>
                                                    $(document).ready(function () {

                                                        console.log("HELLO")
                                                        function exportTableToCSV($table, filename) {
                                                            var $headers = $table.find('tr:has(th)')
                                                                    , $rows = $table.find('tr:has(td)')

                                                                    // Temporary delimiter characters unlikely to be typed by keyboard
                                                                    // This is to avoid accidentally splitting the actual contents
                                                                    , tmpColDelim = String.fromCharCode(11) // vertical tab character
                                                                    , tmpRowDelim = String.fromCharCode(0) // null character

                                                                    // actual delimiter characters for CSV format
                                                                    , colDelim = '","'
                                                                    , rowDelim = '"\r\n"';

                                                            // Grab text from table into CSV formatted string
                                                            var csv = '"';
                                                            csv += formatRows($headers.map(grabRow));
                                                            csv += rowDelim;
                                                            csv += formatRows($rows.map(grabRow)) + '"';

                                                            // Data URI
                                                            var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                                                            // For IE (tested 10+)
                                                            if (window.navigator.msSaveOrOpenBlob) {
                                                                var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                                                                    type: "text/csv;charset=utf-8;"
                                                                });
                                                                navigator.msSaveBlob(blob, filename);
                                                            } else {
                                                                $(this)
                                                                        .attr({
                                                                            'download': filename
                                                                            , 'href': csvData
                                                                                    //,'target' : '_blank' //if you want it to open in a new window
                                                                        });
                                                            }

                                                            //------------------------------------------------------------
                                                            // Helper Functions 
                                                            //------------------------------------------------------------
                                                            // Format the output so it has the appropriate delimiters
                                                            function formatRows(rows) {
                                                                return rows.get().join(tmpRowDelim)
                                                                        .split(tmpRowDelim).join(rowDelim)
                                                                        .split(tmpColDelim).join(colDelim);
                                                            }
                                                            // Grab and format a row from the table
                                                            function grabRow(i, row) {

                                                                var $row = $(row);
                                                                //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                                                                var $cols = $row.find('td');
                                                                if (!$cols.length)
                                                                    $cols = $row.find('th');

                                                                return $cols.map(grabCol)
                                                                        .get().join(tmpColDelim);
                                                            }
                                                            // Grab and format a column from the table 
                                                            function grabCol(j, col) {
                                                                var $col = $(col),
                                                                        $text = $col.text();

                                                                return $text.replace('"', '""'); // escape double quotes

                                                            }
                                                        }


                                                        // This must be a hyperlink
                                                        $("#export").click(function (event) {
                                                            // var outputFile = 'export'
                                                            var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                                                            outputFile = outputFile.replace('.csv', '') + '.csv'

                                                            // CSV
                                                            exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);

                                                            // IF CSV, don't do event.preventDefault() or return false
                                                            // We actually need this to be a typical hyperlink
                                                        });
                                                    });
                                                </script>
                                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("body").on('keyup', '#filter-text', function (e) {
                                                            e.preventDefault();
                                                            var txt = $('#“filter-text').val();
                                                            var regEx = new RegExp($.map($(this).val().trim().split(' '), function (v) {
                                                                return '(?=.*?' + v + ')';
                                                            }).join(''), 'i');

                                                            $('.mt-list-item').hide().filter(function () {
                                                                return regEx.exec($(this).text());
                                                            }).show();
                                                        });
                                                    });

                                                </script>
                                                <script type = "text/javascript" >
                                                    if (RegExp('multipage', 'gi').test(window.location.search)) {
                                                        introJs().start();
                                                    }
                                                </script>
                                                <?php
                                                admin_footer();
                                                ?>
                                                </html>
                                                </div>