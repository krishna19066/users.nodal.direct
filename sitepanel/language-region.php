<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$res = $cityListings1[0];
//$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];

$cloud_cdnName = $cloud_keyData->cloud_name;
$user_email = $cloud_keyData->email;
$hotel_name = $cloud_keyData->companyName;
$phone = $cloud_keyData->phone;
$language = $cloud_keyData->language;


$propertyId = base64_decode($_REQUEST['propID']);

$property_data = $propertyCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;

$super_admin_1 = $propertyCont->GetSupperAdminData($customerId);
$super_admin = $super_admin_1[0];
$status = $super_admin->status;
if (isset($_POST['update_langauge'])) {
    $select_language = $_REQUEST['select_language'];
    $upd = $propertyCont->UpdateWebsiteLanguage($customerId, $select_language);

    echo '<script type="text/javascript">
                alert("Thank You! Your Site language has been successfuly Updated");              
window.location = "language-region.php";
            </script>';
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Language - <span style="color:#e44787;">& Region</span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View Language & Region</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><h3>Language & Region</h3></span>                 
                                        <span style="float:right;"> <a href=""><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>
                                        <!--<span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Video Gallery </button></a></span>-->
                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row"> 
                                            <div class="col-lg-12 col-md-612 col-sm-12 col-xs-12">
                                                <h3>Select Language</h3>
                                                <p>Your site is currently English Language. You can change your site's language at any time.</p> 

                                                <script src="https://www.amcharts.com/lib/3/ammap.js" type="text/javascript"></script>
                                                <script src="https://www.amcharts.com/lib/3/maps/js/worldHigh.js" type="text/javascript"></script>
                                                <script src="https://www.amcharts.com/lib/3/themes/dark.js" type="text/javascript"></script>
                                                <div id="mapdiv" style="width: 1000px; height: 450px;"></div>
                                                <div style="width: 1000px; font-size: 70%; padding: 5px 0; text-align: center; background-color: #535364; margin-top: 1px; color: #B4B4B7;"><a href="https://www.amcharts.com/visited_countries/" style="color: #B4B4B7;">Create your own visited countries map</a> or check out the <a href="https://www.amcharts.com/" style="color: #B4B4B7;">JavaScript Charts</a>.</div>
                                                <script type="text/javascript">
                                                    var map = AmCharts.makeChart("mapdiv", {
                                                        type: "map",
                                                        theme: "dark",
                                                        projection: "mercator",
                                                        panEventsEnabled: true,
                                                        backgroundColor: "#535364",
                                                        backgroundAlpha: 1,
                                                        zoomControl: {
                                                            zoomControlEnabled: true
                                                        },
                                                        dataProvider: {
                                                            map: "worldHigh",
                                                            getAreasFromMap: true,
                                                            areas:
                                                                    [
                                                                        {
                                                                            "id": "BY",
                                                                            "showAsSelected": true
                                                                        },
                                                                        {
                                                                            "id": "BA",
                                                                            "showAsSelected": true
                                                                        }
                                                                    ]
                                                        },
                                                        areasSettings: {
                                                            autoZoom: true,
                                                            color: "#B4B4B7",
                                                            colorSolid: "#84ADE9",
                                                            selectedColor: "#84ADE9",
                                                            outlineColor: "#666666",
                                                            rollOverColor: "#9EC2F7",
                                                            rollOverOutlineColor: "#000000"
                                                        }
                                                    });

                                                </script><br/>
                                                <div class="TimePreview-timePreview-2fPon" id="res_time">
                                                    <div class="default-time">
                                                        <?php
                                                        //date_default_timezone_set($time_zoze);
                                                        $time = date("h:i:sa");
                                                        $today = date("F j, Y, g:i a");
                                                        ?>

                                                        <span style="font-size:20px;"><?php echo $time; ?></span><br/>
                                                        <span><?php echo $today; ?></span>
                                                    </div>
                                                    <span style="font-size:14px; float: right;">Current Language:</span><br/> <span style="font-size:14px; float: right; color:green;"> <?php if($language ==''){ ?>English<?php }else{ echo $language; } ?></span>
                                                </div>
                                                <br/><hr/>
                                                <span id="res_time" style="font-size:20px;"></span><br/>

                                                <form action="" method="post" class="form-horizontal">

                                                    <div class="form-group col-md-6">
                                                        <label for="inputState" class="control-label">Region</label> <br/><br/>                                         
                                                        <select class="" data-test="select" id="timezone" name="timezone" class="form-control" style="width: 290px;height: 50px">
                                                            <option label="Apia (Apia Standard Time) [-11:00]" value="Pacific/Apia">Apia</option>
                                                            <option label="Midway (Samoa Standard Time) [-11:00]" value="Pacific/Midway">Midway</option>                                         
                                                            <option label="Niue (Niue Time) [-11:00]" value="Pacific/Niue">Niue</option>
                                                            <option label="Pago Pago (Samoa Standard Time) [-11:00]" value="Pacific/Pago_Pago">Pago Pago</option>
                                                            <option label="Samoa (Samoa Standard Time) [-11:00]" value="Pacific/Samoa">Samoa</option>
                                                            <option label="Samoa (Samoa Standard Time) [-11:00]" value="US/Samoa">Samoa</option>
                                                            <option label="Adak (Hawaii-Aleutian Standard Time) [-10:00]" value="America/Adak">Adak</option>
                                                            <option label="Atka (Hawaii-Aleutian Standard Time) [-10:00]" value="America/Atka">Atka</option>
                                                            <option label="Fakaofo (Tokelau Time) [-10:00]" value="Pacific/Fakaofo">Fakaofo</option>
                                                            <option label="undefined (Hawaii-Aleutian Standard Time) [-10:00]" value="Pacific/Honolulu"></option>
                                                            <option label="Johnston (Hawaii-Aleutian Standard Time) [-10:00]" value="Pacific/Johnston">Johnston</option>
                                                            <option label="Rarotonga (Cook Islands Standard Time) [-10:00]" value="Pacific/Rarotonga">Rarotonga</option>
                                                            <option label="Tahiti (Tahiti Time) [-10:00]" value="Pacific/Tahiti">Tahiti</option>
                                                            <option label="Aleutian (Hawaii-Aleutian Standard Time) [-10:00]" value="US/Aleutian">Aleutian</option>
                                                            <option label="Hawaii (Hawaii Standard Time) [-10:00]" value="US/Hawaii">Hawaii</option>
                                                            <option label="Marquesas (Marquesas Time) [-09:30]" value="Pacific/Marquesas">Marquesas</option>
                                                            <option label="Anchorage (Alaska Standard Time) [-09:00]" value="America/Anchorage">Anchorage</option>
                                                            <option label="Juneau (Alaska Standard Time) [-09:00]" value="America/Juneau">Juneau</option>
                                                            <option label="Nome (Alaska Standard Time) [-09:00]" value="America/Nome">Nome</option>
                                                            <option label="Yakutat (Alaska Standard Time) [-09:00]" value="America/Yakutat">Yakutat</option>
                                                            <option label="Gambier (Gambier Time) [-09:00]" value="Pacific/Gambier">Gambier</option>
                                                            <option label="Alaska (Alaska Standard Time) [-09:00]" value="US/Alaska">Alaska</option>
                                                            <option label="Dawson (Pacific Standard Time) [-08:00]" value="America/Dawson">Dawson</option>
                                                            <option label="Ensenada (Pacific Standard Time) [-08:00]" value="America/Ensenada">Ensenada</option>
                                                            <option label="Los Angeles (Pacific Standard Time) [-08:00]" value="America/Los_Angeles">Los Angeles</option>
                                                            <option label="Santa Isabel (Pacific Standard Time) [-08:00]" value="America/Santa_Isabel">Santa Isabel</option>
                                                            <option label="Tijuana (Pacific Standard Time) [-08:00]" value="America/Tijuana">Tijuana</option>
                                                            <option label="Vancouver (Pacific Standard Time) [-08:00]" value="America/Vancouver">Vancouver</option>
                                                            <option label="Whitehorse (Pacific Standard Time) [-08:00]" value="America/Whitehorse">Whitehorse</option>
                                                            <option label="Pacific (Pacific Standard Time) [-08:00]" value="Canada/Pacific">Pacific</option>
                                                            <option label="Yukon (Pacific Standard Time) [-08:00]" value="Canada/Yukon">Yukon</option>
                                                           <option label="BajaNorte (Pacific Standard Time) [-08:00]" value="Mexico/BajaNorte">BajaNorte</option>
                                                            <option label="Pitcairn (Pitcairn Time) [-08:00]" value="Pacific/Pitcairn">Pitcairn</option>
                                                            <option label="Pacific (Pacific Standard Time) [-08:00]" value="US/Pacific">Pacific</option>
                                                            <option label="Boise (Mountain Standard Time) [-07:00]" value="America/Boise">Boise</option>
                                                            <option label="Cambridge Bay (Mountain Standard Time) [-07:00]" value="America/Cambridge_Bay">Cambridge Bay</option>
                                                            <option label="Chihuahua (Mexican Pacific Standard Time) [-07:00]" value="America/Chihuahua">Chihuahua</option>
                                                            <option label="Dawson Creek (Mountain Standard Time) [-07:00]" value="America/Dawson_Creek">Dawson Creek</option>
                                                            <option label="Denver (Mountain Standard Time) [-07:00]" value="America/Denver">Denver</option>
                                                            <option label="Edmonton (Mountain Standard Time) [-07:00]" value="America/Edmonton">Edmonton</option>
                                                            <option label="Hermosillo (Mexican Pacific Standard Time) [-07:00]" value="America/Hermosillo">Hermosillo</option>
                                                            <option label="Inuvik (Mountain Standard Time) [-07:00]" value="America/Inuvik">Inuvik</option>
                                                            <option label="Mazatlan (Mexican Pacific Standard Time) [-07:00]" value="America/Mazatlan">Mazatlan</option>
                                                            <option label="Ojinaga (Mountain Standard Time) [-07:00]" value="America/Ojinaga">Ojinaga</option>
                                                            <option label="Phoenix (Mountain Standard Time) [-07:00]" value="America/Phoenix">Phoenix</option>
                                                            <option label="Shiprock (Mountain Standard Time) [-07:00]" value="America/Shiprock">Shiprock</option>
                                                            <option label="Yellowknife (Mountain Standard Time) [-07:00]" value="America/Yellowknife">Yellowknife</option>
                                                            <option label="Mountain (Mountain Standard Time) [-07:00]" value="Canada/Mountain">Mountain</option>
                                                            <option label="BajaSur (Mountain Standard Time) [-07:00]" value="Mexico/BajaSur">BajaSur</option>
                                                            <option label="Navajo (Mountain Standard Time) [-07:00]" value="Navajo">Navajo</option>
                                                            <option label="Arizona (Mountain Standard Time) [-07:00]" value="US/Arizona">Arizona</option>
                                                            <option label="Mountain (Mountain Standard Time) [-07:00]" value="US/Mountain">Mountain</option>
                                                            <option label="Bahia Banderas (Central Standard Time) [-06:00]" value="America/Bahia_Banderas">Bahia Banderas</option>
                                                            <option label="Belize (Central Standard Time) [-06:00]" value="America/Belize">Belize</option>
                                                            <option label="Cancun (Eastern Standard Time) [-06:00]" value="America/Cancun">Cancun</option>
                                                            <option label="Chicago (Central Standard Time) [-06:00]" value="America/Chicago">Chicago</option>
                                                            <option label="Costa Rica (Central Standard Time) [-06:00]" value="America/Costa_Rica">Costa Rica</option>
                                                            <option label="El Salvador (Central Standard Time) [-06:00]" value="America/El_Salvador">El Salvador</option>
                                                            <option label="Guatemala (Central Standard Time) [-06:00]" value="America/Guatemala">Guatemala</option>
                                                            <option label="Knox, Indiana (Central Standard Time) [-06:00]" value="America/Indiana/Knox">Knox, Indiana</option>
                                                            <option label="Tell City, Indiana (Central Standard Time) [-06:00]" value="America/Indiana/Tell_City">Tell City, Indiana</option>
                                                            <option label="Knox IN (Central Standard Time) [-06:00]" value="America/Knox_IN">Knox IN</option>
                                                            <option label="Managua (Central Standard Time) [-06:00]" value="America/Managua">Managua</option>
                                                            <option label="Matamoros (Central Standard Time) [-06:00]" value="America/Matamoros">Matamoros</option>
                                                            <option label="Menominee (Central Standard Time) [-06:00]" value="America/Menominee">Menominee</option>
                                                            <option label="Merida (Central Standard Time) [-06:00]" value="America/Merida">Merida</option>
                                                            <option label="Mexico City (Central Standard Time) [-06:00]" value="America/Mexico_City">Mexico City</option>
                                                            <option label="Monterrey (Central Standard Time) [-06:00]" value="America/Monterrey">Monterrey</option>
                                                            <option label="Center, North Dakota (Central Standard Time) [-06:00]" value="America/North_Dakota/Center">Center, North Dakota</option>
                                                            <option label="New Salem, North Dakota (Central Standard Time) [-06:00]" value="America/North_Dakota/New_Salem">New Salem, North Dakota</option>
                                                            <option label="Rainy River (Central Standard Time) [-06:00]" value="America/Rainy_River">Rainy River</option>
                                                            <option label="Rankin Inlet (Central Standard Time) [-06:00]" value="America/Rankin_Inlet">Rankin Inlet</option>
                                                            <option label="Regina (Central Standard Time) [-06:00]" value="America/Regina">Regina</option>
                                                            <option label="Swift Current (Central Standard Time) [-06:00]" value="America/Swift_Current">Swift Current</option>
                                                            <option label="Tegucigalpa (Central Standard Time) [-06:00]" value="America/Tegucigalpa">Tegucigalpa</option>
                                                            <option label="Winnipeg (Central Standard Time) [-06:00]" value="America/Winnipeg">Winnipeg</option>
                                                            <option label="Central (Central Standard Time) [-06:00]" value="Canada/Central">Central</option>
                                                            <option label="Saskatchewan (Central Standard Time) [-06:00]" value="Canada/Saskatchewan">Saskatchewan</option>
                                                            <option label="EasterIsland (Easter Is. Time) [-06:00]" value="Chile/EasterIsland">EasterIsland</option>
                                                            <option label="General (Central Standard Time) [-06:00]" value="Mexico/General">General</option>
                                                            <option label="Easter (Easter Island Standard Time) [-06:00]" value="Pacific/Easter">Easter</option>
                                                            <option label="Galapagos (Galapagos Time) [-06:00]" value="Pacific/Galapagos">Galapagos</option>
                                                            <option label="Central (Central Standard Time) [-06:00]" value="US/Central">Central</option>
                                                            <option label="Indiana-Starke (Central Standard Time) [-06:00]" value="US/Indiana-Starke">Indiana-Starke</option>
                                                            <option label="Atikokan (Eastern Time) [-05:00]" value="America/Atikokan">Atikokan</option>
                                                            <option label="Bogota (Colombia Standard Time) [-05:00]" value="America/Bogota">Bogota</option>
                                                            <option label="Cayman (Eastern Standard Time) [-05:00]" value="America/Cayman">Cayman</option>
                                                            <option label="Atikokan (Eastern Standard Time) [-05:00]" value="America/Coral_Harbour">Atikokan</option>
                                                            <option label="Detroit (Eastern Standard Time) [-05:00]" value="America/Detroit">Detroit</option>
                                                            <option label="Fort Wayne (Eastern Time) [-05:00]" value="America/Fort_Wayne">Fort Wayne</option>
                                                            <option label="Grand Turk (Eastern Standard Time) [-05:00]" value="America/Grand_Turk">Grand Turk</option>
                                                            <option label="Guayaquil (Ecuador Time) [-05:00]" value="America/Guayaquil">Guayaquil</option>
                                                            <option label="Havana (Cuba Standard Time) [-05:00]" value="America/Havana">Havana</option>
                                                            <option label="Indianapolis (Eastern Time) [-05:00]" value="America/Indiana/Indianapolis">Indianapolis</option>
                                                            <option label="Marengo, Indiana (Eastern Standard Time) [-05:00]" value="America/Indiana/Marengo">Marengo, Indiana</option>
                                                            <option label="Petersburg, Indiana (Eastern Standard Time) [-05:00]" value="America/Indiana/Petersburg">Petersburg, Indiana</option>
                                                            <option label="Vevay, Indiana (Eastern Standard Time) [-05:00]" value="America/Indiana/Vevay">Vevay, Indiana</option>
                                                            <option label="Vincennes, Indiana (Eastern Standard Time) [-05:00]" value="America/Indiana/Vincennes">Vincennes, Indiana</option>
                                                            <option label="Winamac, Indiana (Eastern Standard Time) [-05:00]" value="America/Indiana/Winamac">Winamac, Indiana</option>
                                                            <option label="Indianapolis (Eastern Standard Time) [-05:00]" value="America/Indianapolis">Indianapolis</option>
                                                            <option label="Iqaluit (Eastern Standard Time) [-05:00]" value="America/Iqaluit">Iqaluit</option>
                                                            <option label="Jamaica (Eastern Standard Time) [-05:00]" value="America/Jamaica">Jamaica</option>
                                                            <option label="Louisville (Eastern Time) [-05:00]" value="America/Kentucky/Louisville">Louisville</option>
                                                            <option label="Monticello, Kentucky (Eastern Standard Time) [-05:00]" value="America/Kentucky/Monticello">Monticello, Kentucky</option><option label="Lima (Peru Standard Time) [-05:00]" value="America/Lima">Lima</option><option label="Louisville (Eastern Standard Time) [-05:00]" value="America/Louisville">Louisville</option><option label="Montreal (Eastern Time) [-05:00]" value="America/Montreal">Montreal</option><option label="Nassau (Eastern Standard Time) [-05:00]" value="America/Nassau">Nassau</option><option label="New York (Eastern Standard Time) [-05:00]" value="America/New_York">New York</option><option label="Nipigon (Eastern Standard Time) [-05:00]" value="America/Nipigon">Nipigon</option><option label="Panama (Eastern Standard Time) [-05:00]" value="America/Panama">Panama</option><option label="Pangnirtung (Eastern Standard Time) [-05:00]" value="America/Pangnirtung">Pangnirtung</option><option label="Port-au-Prince (Eastern Standard Time) [-05:00]" value="America/Port-au-Prince">Port-au-Prince</option><option label="Resolute (Central Standard Time) [-05:00]" value="America/Resolute">Resolute</option><option label="Thunder Bay (Eastern Standard Time) [-05:00]" value="America/Thunder_Bay">Thunder Bay</option><option label="Toronto (Eastern Standard Time) [-05:00]" value="America/Toronto">Toronto</option><option label="Eastern (Eastern Time) [-05:00]" value="Canada/Eastern">Eastern</option><option label="Cuba Standard Time (Cuba Standard Time) [-05:00]" value="Cuba">Cuba Standard Time</option><option label="Jamaica (Eastern Time) [-05:00]" value="Jamaica">Jamaica</option><option label="East-Indiana (Eastern Time) [-05:00]" value="US/East-Indiana">East-Indiana</option><option label="Eastern (Eastern Time) [-05:00]" value="US/Eastern">Eastern</option><option label="Michigan (Eastern Time) [-05:00]" value="US/Michigan">Michigan</option><option label="Caracas (Venezuela Time) [-04:30]" value="America/Caracas">Caracas</option><option label="Anguilla (Atlantic Standard Time) [-04:00]" value="America/Anguilla">Anguilla</option><option label="Antigua (Atlantic Standard Time) [-04:00]" value="America/Antigua">Antigua</option><option label="San Luis (Western Argentina Standard Time) [-04:00]" value="America/Argentina/San_Luis">San Luis</option><option label="Aruba (Atlantic Standard Time) [-04:00]" value="America/Aruba">Aruba</option><option label="Asunción (Paraguay Standard Time) [-04:00]" value="America/Asuncion">Asunción</option><option label="Barbados (Atlantic Standard Time) [-04:00]" value="America/Barbados">Barbados</option><option label="Blanc-Sablon (Atlantic Standard Time) [-04:00]" value="America/Blanc-Sablon">Blanc-Sablon</option><option label="Boa Vista (Amazon Standard Time) [-04:00]" value="America/Boa_Vista">Boa Vista</option><option label="Campo Grande (Amazon Standard Time) [-04:00]" value="America/Campo_Grande">Campo Grande</option><option label="Cuiaba (Amazon Standard Time) [-04:00]" value="America/Cuiaba">Cuiaba</option><option label="Curaçao (Atlantic Standard Time) [-04:00]" value="America/Curacao">Curaçao</option><option label="Dominica (Atlantic Standard Time) [-04:00]" value="America/Dominica">Dominica</option><option label="Eirunepe (Acre Standard Time) [-04:00]" value="America/Eirunepe">Eirunepe</option><option label="Glace Bay (Atlantic Standard Time) [-04:00]" value="America/Glace_Bay">Glace Bay</option><option label="Goose Bay (Atlantic Standard Time) [-04:00]" value="America/Goose_Bay">Goose Bay</option><option label="Grenada (Atlantic Standard Time) [-04:00]" value="America/Grenada">Grenada</option><option label="Guadeloupe (Atlantic Standard Time) [-04:00]" value="America/Guadeloupe">Guadeloupe</option><option label="Guyana (Guyana Time) [-04:00]" value="America/Guyana">Guyana</option><option label="Halifax (Atlantic Standard Time) [-04:00]" value="America/Halifax">Halifax</option><option label="La Paz (Bolivia Time) [-04:00]" value="America/La_Paz">La Paz</option><option label="Manaus (Amazon Standard Time) [-04:00]" value="America/Manaus">Manaus</option><option label="Marigot (Atlantic Standard Time) [-04:00]" value="America/Marigot">Marigot</option><option label="Martinique (Atlantic Standard Time) [-04:00]" value="America/Martinique">Martinique</option><option label="Moncton (Atlantic Standard Time) [-04:00]" value="America/Moncton">Moncton</option><option label="Montserrat (Atlantic Standard Time) [-04:00]" value="America/Montserrat">Montserrat</option><option label="Port of Spain (Atlantic Standard Time) [-04:00]" value="America/Port_of_Spain">Port of Spain</option><option label="Porto Acre (Amazon Time) [-04:00]" value="America/Porto_Acre">Porto Acre</option><option label="Porto Velho (Amazon Standard Time) [-04:00]" value="America/Porto_Velho">Porto Velho</option><option label="Puerto Rico (Atlantic Standard Time) [-04:00]" value="America/Puerto_Rico">Puerto Rico</option><option label="Rio Branco (Acre Standard Time) [-04:00]" value="America/Rio_Branco">Rio Branco</option><option label="Santiago (Chile Standard Time) [-04:00]" value="America/Santiago">Santiago</option><option label="Santo Domingo (Atlantic Standard Time) [-04:00]" value="America/Santo_Domingo">Santo Domingo</option><option label="St. Barthélemy (Atlantic Standard Time) [-04:00]" value="America/St_Barthelemy">St. Barthélemy</option><option label="St. Kitts (Atlantic Standard Time) [-04:00]" value="America/St_Kitts">St. Kitts</option><option label="St. Lucia (Atlantic Standard Time) [-04:00]" value="America/St_Lucia">St. Lucia</option><option label="St. Thomas (Atlantic Standard Time) [-04:00]" value="America/St_Thomas">St. Thomas</option><option label="St. Vincent (Atlantic Standard Time) [-04:00]" value="America/St_Vincent">St. Vincent</option><option label="Thule (Atlantic Standard Time) [-04:00]" value="America/Thule">Thule</option><option label="Tortola (Atlantic Standard Time) [-04:00]" value="America/Tortola">Tortola</option><option label="Virgin (Atlantic Standard Time) [-04:00]" value="America/Virgin">Virgin</option><option label="Palmer (Chile Standard Time) [-04:00]" value="Antarctica/Palmer">Palmer</option><option label="Bermuda (Atlantic Standard Time) [-04:00]" value="Atlantic/Bermuda">Bermuda</option><option label="Stanley (Falkland Islands Standard Time) [-04:00]" value="Atlantic/Stanley">Stanley</option><option label="Acre (Amazon Time) [-04:00]" value="Brazil/Acre">Acre</option><option label="West (Amazon Time) [-04:00]" value="Brazil/West">West</option><option label="Atlantic (Atlantic Standard Time) [-04:00]" value="Canada/Atlantic">Atlantic</option><option label="Continental (Chile Time) [-04:00]" value="Chile/Continental">Continental</option><option label="St. John’s (Newfoundland Standard Time) [-03:30]" value="America/St_Johns">St. John’s</option><option label="Newfoundland (Newfoundland Standard Time) [-03:30]" value="Canada/Newfoundland">Newfoundland</option><option label="Araguaina (Brasilia Standard Time) [-03:00]" value="America/Araguaina">Araguaina</option><option label="Buenos Aires (Argentine Time) [-03:00]" value="America/Argentina/Buenos_Aires">Buenos Aires</option><option label="Catamarca (Argentine Time) [-03:00]" value="America/Argentina/Catamarca">Catamarca</option><option label="ComodRivadavia (Argentine Time) [-03:00]" value="America/Argentina/ComodRivadavia">ComodRivadavia</option><option label="Cordoba (Argentine Time) [-03:00]" value="America/Argentina/Cordoba">Cordoba</option><option label="Jujuy (Argentine Time) [-03:00]" value="America/Argentina/Jujuy">Jujuy</option><option label="La Rioja (Argentina Standard Time) [-03:00]" value="America/Argentina/La_Rioja">La Rioja</option><option label="Mendoza (Argentine Time) [-03:00]" value="America/Argentina/Mendoza">Mendoza</option><option label="Rio Gallegos (Argentina Standard Time) [-03:00]" value="America/Argentina/Rio_Gallegos">Rio Gallegos</option><option label="Salta (Argentina Standard Time) [-03:00]" value="America/Argentina/Salta">Salta</option><option label="San Juan (Argentina Standard Time) [-03:00]" value="America/Argentina/San_Juan">San Juan</option><option label="Tucuman (Argentina Standard Time) [-03:00]" value="America/Argentina/Tucuman">Tucuman</option><option label="Ushuaia (Argentina Standard Time) [-03:00]" value="America/Argentina/Ushuaia">Ushuaia</option><option label="Bahia (Brasilia Standard Time) [-03:00]" value="America/Bahia">Bahia</option><option label="Belem (Brasilia Standard Time) [-03:00]" value="America/Belem">Belem</option><option label="Buenos Aires (Argentina Standard Time) [-03:00]" value="America/Buenos_Aires">Buenos Aires</option><option label="Catamarca (Argentina Standard Time) [-03:00]" value="America/Catamarca">Catamarca</option><option label="Cayenne (French Guiana Time) [-03:00]" value="America/Cayenne">Cayenne</option><option label="Cordoba (Argentina Standard Time) [-03:00]" value="America/Cordoba">Cordoba</option><option label="Fortaleza (Brasilia Standard Time) [-03:00]" value="America/Fortaleza">Fortaleza</option><option label="Nuuk (West Greenland Standard Time) [-03:00]" value="America/Godthab">Nuuk</option><option label="Jujuy (Argentina Standard Time) [-03:00]" value="America/Jujuy">Jujuy</option><option label="Maceio (Brasilia Standard Time) [-03:00]" value="America/Maceio">Maceio</option><option label="Mendoza (Argentina Standard Time) [-03:00]" value="America/Mendoza">Mendoza</option><option label="Miquelon (St. Pierre &amp; Miquelon Standard Time) [-03:00]" value="America/Miquelon">Miquelon</option><option label="Montevideo (Uruguay Standard Time) [-03:00]" value="America/Montevideo">Montevideo</option><option label="Paramaribo (Suriname Time) [-03:00]" value="America/Paramaribo">Paramaribo</option><option label="Recife (Brasilia Standard Time) [-03:00]" value="America/Recife">Recife</option><option label="Rosario (Argentine Time) [-03:00]" value="America/Rosario">Rosario</option><option label="Santarem (Brasilia Standard Time) [-03:00]" value="America/Santarem">Santarem</option><option label="Sao Paulo (Brasilia Standard Time) [-03:00]" value="America/Sao_Paulo">Sao Paulo</option><option label="Rothera (Rothera Time) [-03:00]" value="Antarctica/Rothera">Rothera</option><option label="East (Brasilia Time) [-03:00]" value="Brazil/East">East</option><option label="Noronha (Fernando de Noronha Standard Time) [-02:00]" value="America/Noronha">Noronha</option><option label="South Georgia (South Georgia Time) [-02:00]" value="Atlantic/South_Georgia">South Georgia</option><option label="DeNoronha (Fernando de Noronha Time) [-02:00]" value="Brazil/DeNoronha">DeNoronha</option><option label="Ittoqqortoormiit (East Greenland Standard Time) [-01:00]" value="America/Scoresbysund">Ittoqqortoormiit</option><option label="Azores (Azores Standard Time) [-01:00]" value="Atlantic/Azores">Azores</option><option label="Cape Verde (Cape Verde Standard Time) [-01:00]" value="Atlantic/Cape_Verde">Cape Verde</option><option label="Abidjan (Greenwich Mean Time) [+00:00]" value="Africa/Abidjan">Abidjan</option><option label="Accra (Greenwich Mean Time) [+00:00]" value="Africa/Accra">Accra</option><option label="Bamako (Greenwich Mean Time) [+00:00]" value="Africa/Bamako">Bamako</option><option label="Banjul (Greenwich Mean Time) [+00:00]" value="Africa/Banjul">Banjul</option><option label="Bissau (Greenwich Mean Time) [+00:00]" value="Africa/Bissau">Bissau</option><option label="Casablanca (Western European Standard Time) [+00:00]" value="Africa/Casablanca">Casablanca</option><option label="Conakry (Greenwich Mean Time) [+00:00]" value="Africa/Conakry">Conakry</option><option label="Dakar (Greenwich Mean Time) [+00:00]" value="Africa/Dakar">Dakar</option><option label="El Aaiun (Western European Standard Time) [+00:00]" value="Africa/El_Aaiun">El Aaiun</option><option label="Freetown (Greenwich Mean Time) [+00:00]" value="Africa/Freetown">Freetown</option><option label="Lome (Greenwich Mean Time) [+00:00]" value="Africa/Lome">Lome</option><option label="Monrovia (Greenwich Mean Time) [+00:00]" value="Africa/Monrovia">Monrovia</option><option label="Nouakchott (Greenwich Mean Time) [+00:00]" value="Africa/Nouakchott">Nouakchott</option><option label="Ouagadougou (Greenwich Mean Time) [+00:00]" value="Africa/Ouagadougou">Ouagadougou</option><option label="São Tomé (Greenwich Mean Time) [+00:00]" value="Africa/Sao_Tome">São Tomé</option><option label="Timbuktu (Greenwich Mean Time) [+00:00]" value="Africa/Timbuktu">Timbuktu</option><option label="Danmarkshavn (Greenwich Mean Time) [+00:00]" value="America/Danmarkshavn">Danmarkshavn</option><option label="Canary (Western European Standard Time) [+00:00]" value="Atlantic/Canary">Canary</option><option label="Faroe (Western European Standard Time) [+00:00]" value="Atlantic/Faeroe">Faroe</option><option label="Faroe (Western European Time) [+00:00]" value="Atlantic/Faroe">Faroe</option><option label="Madeira (Western European Standard Time) [+00:00]" value="Atlantic/Madeira">Madeira</option><option label="Reykjavik (Greenwich Mean Time) [+00:00]" value="Atlantic/Reykjavik">Reykjavik</option><option label="St. Helena (Greenwich Mean Time) [+00:00]" value="Atlantic/St_Helena">St. Helena</option><option label="Eire (Greenwich Mean Time) [+00:00]" value="Eire">Eire</option><option label="Belfast (Greenwich Mean Time) [+00:00]" value="Europe/Belfast">Belfast</option><option label="Dublin (Greenwich Mean Time) [+00:00]" value="Europe/Dublin">Dublin</option><option label="Guernsey (Greenwich Mean Time) [+00:00]" value="Europe/Guernsey">Guernsey</option><option label="Isle of Man (Greenwich Mean Time) [+00:00]" value="Europe/Isle_of_Man">Isle of Man</option><option label="Jersey (Greenwich Mean Time) [+00:00]" value="Europe/Jersey">Jersey</option><option label="Lisbon (Western European Standard Time) [+00:00]" value="Europe/Lisbon">Lisbon</option><option label="London (Greenwich Mean Time) [+00:00]" value="Europe/London">London</option><option label="GB-Eire (Greenwich Mean Time) [+00:00]" value="GB-Eire">GB-Eire</option><option label="Greenwich (Greenwich Mean Time) [+00:00]" value="Greenwich">Greenwich</option><option label="Iceland (Greenwich Mean Time) [+00:00]" value="Iceland">Iceland</option><option label="Portugal (Western European Time) [+00:00]" value="Portugal">Portugal</option><option label="Universal (Coordinated Universal Time) [+00:00]" value="Universal">Universal</option><option label="Zulu (Coordinated Universal Time) [+00:00]" value="Zulu">Zulu</option><option label="Algiers (Central European Standard Time) [+01:00]" value="Africa/Algiers">Algiers</option><option label="Bangui (West Africa Standard Time) [+01:00]" value="Africa/Bangui">Bangui</option><option label="Brazzaville (West Africa Standard Time) [+01:00]" value="Africa/Brazzaville">Brazzaville</option><option label="Ceuta (Central European Standard Time) [+01:00]" value="Africa/Ceuta">Ceuta</option><option label="Douala (West Africa Standard Time) [+01:00]" value="Africa/Douala">Douala</option><option label="Kinshasa (West Africa Standard Time) [+01:00]" value="Africa/Kinshasa">Kinshasa</option><option label="Lagos (West Africa Standard Time) [+01:00]" value="Africa/Lagos">Lagos</option><option label="Libreville (West Africa Standard Time) [+01:00]" value="Africa/Libreville">Libreville</option><option label="Luanda (West Africa Standard Time) [+01:00]" value="Africa/Luanda">Luanda</option><option label="Malabo (West Africa Standard Time) [+01:00]" value="Africa/Malabo">Malabo</option><option label="Ndjamena (West Africa Standard Time) [+01:00]" value="Africa/Ndjamena">Ndjamena</option><option label="Niamey (West Africa Standard Time) [+01:00]" value="Africa/Niamey">Niamey</option><option label="Porto-Novo (West Africa Standard Time) [+01:00]" value="Africa/Porto-Novo">Porto-Novo</option><option label="Tunis (Central European Standard Time) [+01:00]" value="Africa/Tunis">Tunis</option><option label="Windhoek (Central Africa Time) [+01:00]" value="Africa/Windhoek">Windhoek</option><option label="Longyearbyen (Central European Standard Time) [+01:00]" value="Arctic/Longyearbyen">Longyearbyen</option><option label="Jan Mayen (Central European Time) [+01:00]" value="Atlantic/Jan_Mayen">Jan Mayen</option><option label="Amsterdam (Central European Standard Time) [+01:00]" value="Europe/Amsterdam">Amsterdam</option><option label="Andorra (Central European Standard Time) [+01:00]" value="Europe/Andorra">Andorra</option><option label="Belgrade (Central European Standard Time) [+01:00]" value="Europe/Belgrade">Belgrade</option><option label="Berlin (Central European Standard Time) [+01:00]" value="Europe/Berlin">Berlin</option><option label="Bratislava (Central European Standard Time) [+01:00]" value="Europe/Bratislava">Bratislava</option><option label="Brussels (Central European Standard Time) [+01:00]" value="Europe/Brussels">Brussels</option><option label="Budapest (Central European Standard Time) [+01:00]" value="Europe/Budapest">Budapest</option><option label="Copenhagen (Central European Standard Time) [+01:00]" value="Europe/Copenhagen">Copenhagen</option><option label="Gibraltar (Central European Standard Time) [+01:00]" value="Europe/Gibraltar">Gibraltar</option><option label="Ljubljana (Central European Standard Time) [+01:00]" value="Europe/Ljubljana">Ljubljana</option><option label="Luxembourg (Central European Standard Time) [+01:00]" value="Europe/Luxembourg">Luxembourg</option><option label="Madrid (Central European Standard Time) [+01:00]" value="Europe/Madrid">Madrid</option><option label="Malta (Central European Standard Time) [+01:00]" value="Europe/Malta">Malta</option><option label="Monaco (Central European Standard Time) [+01:00]" value="Europe/Monaco">Monaco</option><option label="Oslo (Central European Standard Time) [+01:00]" value="Europe/Oslo">Oslo</option><option label="Paris (Central European Standard Time) [+01:00]" value="Europe/Paris">Paris</option><option label="Podgorica (Central European Standard Time) [+01:00]" value="Europe/Podgorica">Podgorica</option><option label="Prague (Central European Standard Time) [+01:00]" value="Europe/Prague">Prague</option><option label="Rome (Central European Standard Time) [+01:00]" value="Europe/Rome">Rome</option><option label="San Marino (Central European Standard Time) [+01:00]" value="Europe/San_Marino">San Marino</option><option label="Sarajevo (Central European Standard Time) [+01:00]" value="Europe/Sarajevo">Sarajevo</option><option label="Skopje (Central European Standard Time) [+01:00]" value="Europe/Skopje">Skopje</option><option label="Stockholm (Central European Standard Time) [+01:00]" value="Europe/Stockholm">Stockholm</option><option label="Tirane (Central European Standard Time) [+01:00]" value="Europe/Tirane">Tirane</option><option label="Vaduz (Central European Standard Time) [+01:00]" value="Europe/Vaduz">Vaduz</option><option label="Vatican (Central European Standard Time) [+01:00]" value="Europe/Vatican">Vatican</option><option label="Vienna (Central European Standard Time) [+01:00]" value="Europe/Vienna">Vienna</option><option label="Warsaw (Central European Standard Time) [+01:00]" value="Europe/Warsaw">Warsaw</option><option label="Zagreb (Central European Standard Time) [+01:00]" value="Europe/Zagreb">Zagreb</option><option label="Zurich (Central European Standard Time) [+01:00]" value="Europe/Zurich">Zurich</option><option label="Poland (Central European Time) [+01:00]" value="Poland">Poland</option><option label="Blantyre (Central Africa Time) [+02:00]" value="Africa/Blantyre">Blantyre</option><option label="Bujumbura (Central Africa Time) [+02:00]" value="Africa/Bujumbura">Bujumbura</option><option label="Cairo (Eastern European Standard Time) [+02:00]" value="Africa/Cairo">Cairo</option><option label="Gaborone (Central Africa Time) [+02:00]" value="Africa/Gaborone">Gaborone</option><option label="Harare (Central Africa Time) [+02:00]" value="Africa/Harare">Harare</option><option label="Johannesburg (South Africa Standard Time) [+02:00]" value="Africa/Johannesburg">Johannesburg</option><option label="Kigali (Central Africa Time) [+02:00]" value="Africa/Kigali">Kigali</option><option label="Lubumbashi (Central Africa Time) [+02:00]" value="Africa/Lubumbashi">Lubumbashi</option><option label="Lusaka (Central Africa Time) [+02:00]" value="Africa/Lusaka">Lusaka</option><option label="Maputo (Central Africa Time) [+02:00]" value="Africa/Maputo">Maputo</option><option label="Maseru (South Africa Standard Time) [+02:00]" value="Africa/Maseru">Maseru</option><option label="Mbabane (South Africa Standard Time) [+02:00]" value="Africa/Mbabane">Mbabane</option><option label="Tripoli (Eastern European Standard Time) [+02:00]" value="Africa/Tripoli">Tripoli</option><option label="Amman (Eastern European Standard Time) [+02:00]" value="Asia/Amman">Amman</option><option label="Beirut (Eastern European Standard Time) [+02:00]" value="Asia/Beirut">Beirut</option><option label="Damascus (Eastern European Standard Time) [+02:00]" value="Asia/Damascus">Damascus</option><option label="Gaza (Eastern European Standard Time) [+02:00]" value="Asia/Gaza">Gaza</option><option label="Istanbul (Eastern European Time) [+02:00]" value="Asia/Istanbul">Istanbul</option><option label="Jerusalem (Israel Standard Time) [+02:00]" value="Asia/Jerusalem">Jerusalem</option><option label="Nicosia (Eastern European Standard Time) [+02:00]" value="Asia/Nicosia">Nicosia</option><option label="Tel Aviv (Israel Standard Time) [+02:00]" value="Asia/Tel_Aviv">Tel Aviv</option><option label="Egypt (Eastern European Time) [+02:00]" value="Egypt">Egypt</option><option label="Athens (Eastern European Standard Time) [+02:00]" value="Europe/Athens">Athens</option><option label="Bucharest (Eastern European Standard Time) [+02:00]" value="Europe/Bucharest">Bucharest</option><option label="Chisinau (Eastern European Standard Time) [+02:00]" value="Europe/Chisinau">Chisinau</option><option label="Helsinki (Eastern European Standard Time) [+02:00]" value="Europe/Helsinki">Helsinki</option><option label="Istanbul (Eastern European Time) [+02:00]" value="Europe/Istanbul">Istanbul</option><option label="Kaliningrad (Eastern European Standard Time) [+02:00]" value="Europe/Kaliningrad">Kaliningrad</option><option label="Kiev (Eastern European Standard Time) [+02:00]" value="Europe/Kiev">Kiev</option><option label="Mariehamn (Eastern European Standard Time) [+02:00]" value="Europe/Mariehamn">Mariehamn</option><option label="Minsk (Moscow Standard Time) [+02:00]" value="Europe/Minsk">Minsk</option><option label="Nicosia (Eastern European Time) [+02:00]" value="Europe/Nicosia">Nicosia</option><option label="Riga (Eastern European Standard Time) [+02:00]" value="Europe/Riga">Riga</option><option label="Simferopol (Moscow Standard Time) [+02:00]" value="Europe/Simferopol">Simferopol</option><option label="Sofia (Eastern European Standard Time) [+02:00]" value="Europe/Sofia">Sofia</option><option label="Tallinn (Eastern European Standard Time) [+02:00]" value="Europe/Tallinn">Tallinn</option><option label="Tiraspol (Eastern European Time) [+02:00]" value="Europe/Tiraspol">Tiraspol</option><option label="Uzhhorod (Eastern European Standard Time) [+02:00]" value="Europe/Uzhgorod">Uzhhorod</option><option label="Vilnius (Eastern European Standard Time) [+02:00]" value="Europe/Vilnius">Vilnius</option><option label="Zaporozhye (Eastern European Standard Time) [+02:00]" value="Europe/Zaporozhye">Zaporozhye</option><option label="Israel Standard Time (Israel Standard Time) [+02:00]" value="Israel">Israel Standard Time</option><option label="Libya (Eastern European Time) [+02:00]" value="Libya">Libya</option><option label="Turkey (Eastern European Time) [+02:00]" value="Turkey">Turkey</option><option label="Addis Ababa (East Africa Time) [+03:00]" value="Africa/Addis_Ababa">Addis Ababa</option><option label="Asmara (Eastern African Time) [+03:00]" value="Africa/Asmara">Asmara</option><option label="Asmara (East Africa Time) [+03:00]" value="Africa/Asmera">Asmara</option><option label="Dar es Salaam (East Africa Time) [+03:00]" value="Africa/Dar_es_Salaam">Dar es Salaam</option><option label="Djibouti (East Africa Time) [+03:00]" value="Africa/Djibouti">Djibouti</option><option label="Kampala (East Africa Time) [+03:00]" value="Africa/Kampala">Kampala</option><option label="Khartoum (Central Africa Time) [+03:00]" value="Africa/Khartoum">Khartoum</option><option label="Mogadishu (East Africa Time) [+03:00]" value="Africa/Mogadishu">Mogadishu</option><option label="Nairobi (East Africa Time) [+03:00]" value="Africa/Nairobi">Nairobi</option><option label="Syowa (Syowa Time) [+03:00]" value="Antarctica/Syowa">Syowa</option><option label="Aden (Arabian Standard Time) [+03:00]" value="Asia/Aden">Aden</option><option label="Baghdad (Arabian Standard Time) [+03:00]" value="Asia/Baghdad">Baghdad</option><option label="Bahrain (Arabian Standard Time) [+03:00]" value="Asia/Bahrain">Bahrain</option><option label="Kuwait (Arabian Standard Time) [+03:00]" value="Asia/Kuwait">Kuwait</option><option label="Qatar (Arabian Standard Time) [+03:00]" value="Asia/Qatar">Qatar</option><option label="Riyadh (Arabian Standard Time) [+03:00]" value="Asia/Riyadh">Riyadh</option><option label="Moscow (Moscow Standard Time) [+03:00]" value="Europe/Moscow">Moscow</option><option label="Samara (Samara Standard Time) [+03:00]" value="Europe/Samara">Samara</option><option label="Volgograd (Moscow Standard Time) [+03:00]" value="Europe/Volgograd">Volgograd</option><option label="Antananarivo (East Africa Time) [+03:00]" value="Indian/Antananarivo">Antananarivo</option><option label="Comoro (East Africa Time) [+03:00]" value="Indian/Comoro">Comoro</option><option label="Mayotte (East Africa Time) [+03:00]" value="Indian/Mayotte">Mayotte</option><option label="W-SU (Moscow Standard Time) [+03:00]" value="W-SU">W-SU</option><option label="Tehran (Iran Standard Time) [+03:30]" value="Asia/Tehran">Tehran</option><option label="Iran Standard Time (Iran Standard Time) [+03:30]" value="Iran">Iran Standard Time</option><option label="Baku (Azerbaijan Standard Time) [+04:00]" value="Asia/Baku">Baku</option><option label="Dubai (Gulf Standard Time) [+04:00]" value="Asia/Dubai">Dubai</option><option label="Muscat (Gulf Standard Time) [+04:00]" value="Asia/Muscat">Muscat</option><option label="Tbilisi (Georgia Standard Time) [+04:00]" value="Asia/Tbilisi">Tbilisi</option><option label="Yerevan (Armenia Standard Time) [+04:00]" value="Asia/Yerevan">Yerevan</option><option label="Mahe (Seychelles Time) [+04:00]" value="Indian/Mahe">Mahe</option><option label="Mauritius (Mauritius Standard Time) [+04:00]" value="Indian/Mauritius">Mauritius</option><option label="Réunion (Reunion Time) [+04:00]" value="Indian/Reunion">Réunion</option><option label="Kabul (Afghanistan Time) [+04:30]" value="Asia/Kabul">Kabul</option><option label="Mawson (Mawson Time) [+05:00]" value="Antarctica/Mawson">Mawson</option><option label="Aqtau (West Kazakhstan Time) [+05:00]" value="Asia/Aqtau">Aqtau</option><option label="Aqtobe (West Kazakhstan Time) [+05:00]" value="Asia/Aqtobe">Aqtobe</option><option label="Ashgabat (Turkmenistan Standard Time) [+05:00]" value="Asia/Ashgabat">Ashgabat</option><option label="Ashkhabad (Turkmenistan Time) [+05:00]" value="Asia/Ashkhabad">Ashkhabad</option><option label="Dushanbe (Tajikistan Time) [+05:00]" value="Asia/Dushanbe">Dushanbe</option><option label="Karachi (Pakistan Standard Time) [+05:00]" value="Asia/Karachi">Karachi</option><option label="Oral (West Kazakhstan Time) [+05:00]" value="Asia/Oral">Oral</option><option label="Samarkand (Uzbekistan Standard Time) [+05:00]" value="Asia/Samarkand">Samarkand</option><option label="Tashkent (Uzbekistan Standard Time) [+05:00]" value="Asia/Tashkent">Tashkent</option><option label="Yekaterinburg (Yekaterinburg Standard Time) [+05:00]" value="Asia/Yekaterinburg">Yekaterinburg</option><option label="Kerguelen (French Southern &amp; Antarctic Time) [+05:00]" value="Indian/Kerguelen">Kerguelen</option><option label="Maldives (Maldives Time) [+05:00]" value="Indian/Maldives">Maldives</option><option label="Kolkata (India Standard Time) [+05:30]" value="Asia/Calcutta">Kolkata</option><option label="Colombo (India Standard Time) [+05:30]" value="Asia/Colombo">Colombo</option><option label="Kolkata (India Standard Time) [+05:30]" value="Asia/Kolkata">Kolkata</option><option label="Kathmandu (Nepal Time) [+05:45]" value="Asia/Kathmandu">Kathmandu</option><option label="Kathmandu (Nepal Time) [+05:45]" value="Asia/Katmandu">Kathmandu</option><option label="Vostok (Vostok Time) [+06:00]" value="Antarctica/Vostok">Vostok</option><option label="Almaty (East Kazakhstan Time) [+06:00]" value="Asia/Almaty">Almaty</option><option label="Bishkek (Kyrgyzstan Time) [+06:00]" value="Asia/Bishkek">Bishkek</option><option label="Dacca (Bangladesh Time) [+06:00]" value="Asia/Dacca">Dacca</option><option label="Dhaka (Bangladesh Standard Time) [+06:00]" value="Asia/Dhaka">Dhaka</option><option label="Novokuznetsk (Krasnoyarsk Standard Time) [+06:00]" value="Asia/Novokuznetsk">Novokuznetsk</option><option label="Novosibirsk (Novosibirsk Standard Time) [+06:00]" value="Asia/Novosibirsk">Novosibirsk</option><option label="Omsk (Omsk Standard Time) [+06:00]" value="Asia/Omsk">Omsk</option><option label="Qyzylorda (East Kazakhstan Time) [+06:00]" value="Asia/Qyzylorda">Qyzylorda</option><option label="Thimbu (Bhutan Time) [+06:00]" value="Asia/Thimbu">Thimbu</option><option label="Thimphu (Bhutan Time) [+06:00]" value="Asia/Thimphu">Thimphu</option><option label="Chagos (Indian Ocean Time) [+06:00]" value="Indian/Chagos">Chagos</option><option label="Yangon (Myanmar Time) [+06:30]" value="Asia/Rangoon">Yangon</option><option label="Cocos (Cocos Islands Time) [+06:30]" value="Indian/Cocos">Cocos</option><option label="Davis (Davis Time) [+07:00]" value="Antarctica/Davis">Davis</option><option label="Bangkok (Indochina Time) [+07:00]" value="Asia/Bangkok">Bangkok</option><option label="Ho Chi Minh (Indochina Time) [+07:00]" value="Asia/Ho_Chi_Minh">Ho Chi Minh</option><option label="Hovd (Hovd Standard Time) [+07:00]" value="Asia/Hovd">Hovd</option><option label="Jakarta (Western Indonesia Time) [+07:00]" value="Asia/Jakarta">Jakarta</option><option label="Krasnoyarsk (Krasnoyarsk Standard Time) [+07:00]" value="Asia/Krasnoyarsk">Krasnoyarsk</option><option label="Phnom Penh (Indochina Time) [+07:00]" value="Asia/Phnom_Penh">Phnom Penh</option><option label="Pontianak (Western Indonesia Time) [+07:00]" value="Asia/Pontianak">Pontianak</option><option label="Ho Chi Minh City (Indochina Time) [+07:00]" value="Asia/Saigon">Ho Chi Minh City</option><option label="Vientiane (Indochina Time) [+07:00]" value="Asia/Vientiane">Vientiane</option><option label="Christmas (Christmas Island Time) [+07:00]" value="Indian/Christmas">Christmas</option><option label="Casey (Casey Time) [+08:00]" value="Antarctica/Casey">Casey</option><option label="Brunei (Brunei Darussalam Time) [+08:00]" value="Asia/Brunei">Brunei</option><option label="Choibalsan (Choibalsan Standard Time) [+08:00]" value="Asia/Choibalsan">Choibalsan</option><option label="Chongqing (China Standard Time) [+08:00]" value="Asia/Chongqing">Chongqing</option><option label="Chungking (China Standard Time) [+08:00]" value="Asia/Chungking">Chungking</option><option label="Harbin (China Standard Time) [+08:00]" value="Asia/Harbin">Harbin</option><option label="Hong Kong (Hong Kong Standard Time) [+08:00]" value="Asia/Hong_Kong">Hong Kong</option><option label="Irkutsk (Irkutsk Standard Time) [+08:00]" value="Asia/Irkutsk">Irkutsk</option><option label="Kashgar (China Standard Time) [+08:00]" value="Asia/Kashgar">Kashgar</option><option label="Kuala Lumpur (Malaysia Time) [+08:00]" value="Asia/Kuala_Lumpur">Kuala Lumpur</option><option label="Kuching (Malaysia Time) [+08:00]" value="Asia/Kuching">Kuching</option><option label="Macao (China Standard Time) [+08:00]" value="Asia/Macao">Macao</option><option label="Macau (China Standard Time) [+08:00]" value="Asia/Macau">Macau</option><option label="Makassar (Central Indonesia Time) [+08:00]" value="Asia/Makassar">Makassar</option><option label="Manila (Philippine Standard Time) [+08:00]" value="Asia/Manila">Manila</option><option label="Shanghai (China Standard Time) [+08:00]" value="Asia/Shanghai">Shanghai</option><option label="Singapore (Singapore Standard Time) [+08:00]" value="Asia/Singapore">Singapore</option><option label="Taipei (Taipei Standard Time) [+08:00]" value="Asia/Taipei">Taipei</option><option label="Ujung Pandang (Central Indonesia Time) [+08:00]" value="Asia/Ujung_Pandang">Ujung Pandang</option><option label="Ulaanbaatar (Ulaanbaatar Standard Time) [+08:00]" value="Asia/Ulaanbaatar">Ulaanbaatar</option><option label="Ulan Bator (Ulaanbaatar Time) [+08:00]" value="Asia/Ulan_Bator">Ulan Bator</option><option label="Urumqi (China Standard Time) [+08:00]" value="Asia/Urumqi">Urumqi</option><option label="Perth (Australian Western Standard Time) [+08:00]" value="Australia/Perth">Perth</option><option label="West (Western Standard Time (Australia)) [+08:00]" value="Australia/West">West</option><option label="Hongkong (Hong Kong Time) [+08:00]" value="Hongkong">Hongkong</option><option label="Singapore Standard Time (Singapore Standard Time) [+08:00]" value="Singapore">Singapore Standard Time</option><option label="Eucla (Australian Central Western Standard Time) [+08:45]" value="Australia/Eucla">Eucla</option><option label="Dili (East Timor Time) [+09:00]" value="Asia/Dili">Dili</option><option label="Jayapura (Eastern Indonesia Time) [+09:00]" value="Asia/Jayapura">Jayapura</option><option label="Pyongyang (Pyongyang Time) [+09:00]" value="Asia/Pyongyang">Pyongyang</option><option label="Seoul (Korean Standard Time) [+09:00]" value="Asia/Seoul">Seoul</option><option label="Tokyo (Japan Standard Time) [+09:00]" value="Asia/Tokyo">Tokyo</option><option label="Yakutsk (Yakutsk Standard Time) [+09:00]" value="Asia/Yakutsk">Yakutsk</option><option label="Japan Standard Time (Japan Standard Time) [+09:00]" value="Japan">Japan Standard Time</option><option label="Palau (Palau Time) [+09:00]" value="Pacific/Palau">Palau</option><option label="Adelaide (Australian Central Standard Time) [+09:30]" value="Australia/Adelaide">Adelaide</option><option label="Broken Hill (Australian Central Standard Time) [+09:30]" value="Australia/Broken_Hill">Broken Hill</option><option label="Darwin (Australian Central Standard Time) [+09:30]" value="Australia/Darwin">Darwin</option><option label="North (Central Standard Time (Northern Territory)) [+09:30]" value="Australia/North">North</option><option label="South (Central Standard Time (South Australia)) [+09:30]" value="Australia/South">South</option><option label="Yancowinna (Central Standard Time (South Australia/New South Wales)) [+09:30]" value="Australia/Yancowinna">Yancowinna</option><option label="Dumont d’Urville (Dumont-d’Urville Time) [+10:00]" value="Antarctica/DumontDUrville">Dumont d’Urville</option><option label="Sakhalin (Sakhalin Standard Time) [+10:00]" value="Asia/Sakhalin">Sakhalin</option><option label="Vladivostok (Vladivostok Standard Time) [+10:00]" value="Asia/Vladivostok">Vladivostok</option><option label="ACT (Eastern Time (New South Wales)) [+10:00]" value="Australia/ACT">ACT</option><option label="Brisbane (Australian Eastern Standard Time) [+10:00]" value="Australia/Brisbane">Brisbane</option><option label="Canberra (Eastern Time (New South Wales)) [+10:00]" value="Australia/Canberra">Canberra</option><option label="Currie (Australian Eastern Standard Time) [+10:00]" value="Australia/Currie">Currie</option><option label="Hobart (Australian Eastern Standard Time) [+10:00]" value="Australia/Hobart">Hobart</option><option label="Lindeman (Australian Eastern Standard Time) [+10:00]" value="Australia/Lindeman">Lindeman</option><option label="Melbourne (Australian Eastern Standard Time) [+10:00]" value="Australia/Melbourne">Melbourne</option><option label="NSW (Eastern Time (New South Wales)) [+10:00]" value="Australia/NSW">NSW</option><option label="Queensland (Eastern Time (Queensland)) [+10:00]" value="Australia/Queensland">Queensland</option><option label="Sydney (Australian Eastern Standard Time) [+10:00]" value="Australia/Sydney">Sydney</option><option label="Tasmania (Eastern Time (Tasmania)) [+10:00]" value="Australia/Tasmania">Tasmania</option><option label="Victoria (Eastern Time (Victoria)) [+10:00]" value="Australia/Victoria">Victoria</option><option label="Chuuk (Chuuk Time) [+10:00]" value="Pacific/Chuuk">Chuuk</option><option label="Guam (Chamorro Standard Time) [+10:00]" value="Pacific/Guam">Guam</option><option label="Port Moresby (Papua New Guinea Time) [+10:00]" value="Pacific/Port_Moresby">Port Moresby</option><option label="Saipan (Chamorro Standard Time) [+10:00]" value="Pacific/Saipan">Saipan</option><option label="Chuuk (Chuuk Time) [+10:00]" value="Pacific/Truk">Chuuk</option><option label="Yap (Chuuk Time) [+10:00]" value="Pacific/Yap">Yap</option><option label="LHI (Lord Howe Standard Time) [+10:30]" value="Australia/LHI">LHI</option><option label="Lord Howe (Lord Howe Standard Time) [+10:30]" value="Australia/Lord_Howe">Lord Howe</option><option label="Macquarie (Macquarie Island Time) [+11:00]" value="Antarctica/Macquarie">Macquarie</option><option label="Anadyr (Anadyr Standard Time) [+11:00]" value="Asia/Anadyr">Anadyr</option><option label="Kamchatka (Petropavlovsk-Kamchatski Standard Time) [+11:00]" value="Asia/Kamchatka">Kamchatka</option><option label="Magadan (Magadan Standard Time) [+11:00]" value="Asia/Magadan">Magadan</option><option label="Efate (Vanuatu Standard Time) [+11:00]" value="Pacific/Efate">Efate</option><option label="Guadalcanal (Solomon Islands Time) [+11:00]" value="Pacific/Guadalcanal">Guadalcanal</option><option label="Kosrae (Kosrae Time) [+11:00]" value="Pacific/Kosrae">Kosrae</option><option label="Noumea (New Caledonia Standard Time) [+11:00]" value="Pacific/Noumea">Noumea</option><option label="Pohnpei (Pohnpei Time) [+11:00]" value="Pacific/Pohnpei">Pohnpei</option><option label="Pohnpei (Ponape Time) [+11:00]" value="Pacific/Ponape">Pohnpei</option><option label="Norfolk (Norfolk Island Time) [+11:30]" value="Pacific/Norfolk">Norfolk</option><option label="McMurdo (New Zealand Standard Time) [+12:00]" value="Antarctica/McMurdo">McMurdo</option><option label="South Pole (New Zealand Standard Time) [+12:00]" value="Antarctica/South_Pole">South Pole</option><option label="Kwajalein (Marshall Islands Time) [+12:00]" value="Kwajalein">Kwajalein</option><option label="Auckland (New Zealand Standard Time) [+12:00]" value="Pacific/Auckland">Auckland</option><option label="Fiji (Fiji Standard Time) [+12:00]" value="Pacific/Fiji">Fiji</option><option label="Funafuti (Tuvalu Time) [+12:00]" value="Pacific/Funafuti">Funafuti</option><option label="Kwajalein (Marshall Islands Time) [+12:00]" value="Pacific/Kwajalein">Kwajalein</option><option label="Majuro (Marshall Islands Time) [+12:00]" value="Pacific/Majuro">Majuro</option><option label="Nauru (Nauru Time) [+12:00]" value="Pacific/Nauru">Nauru</option><option label="Tarawa (Gilbert Islands Time) [+12:00]" value="Pacific/Tarawa">Tarawa</option><option label="Wake (Wake Island Time) [+12:00]" value="Pacific/Wake">Wake</option><option label="Wallis (Wallis &amp; Futuna Time) [+12:00]" value="Pacific/Wallis">Wallis</option><option label="NZ-CHAT (Chatham Standard Time) [+12:45]" value="NZ-CHAT">NZ-CHAT</option><option label="Chatham (Chatham Standard Time) [+12:45]" value="Pacific/Chatham">Chatham</option><option label="Enderbury (Phoenix Islands Time) [+13:00]" value="Pacific/Enderbury">Enderbury</option><option label="Tongatapu (Tonga Standard Time) [+13:00]" value="Pacific/Tongatapu">Tongatapu</option><option label="Kiritimati (Line Islands Time) [+14:00]" value="Pacific/Kiritimati">Kiritimati</option></select>

                                                    </div>

                                                    <!-- Material unchecked -->
                                                    <div class="form-group col-md-6" style="float:right;">
                                                        <label for="inputState">Select Language</label>
                                                        <select name="select_language" id="timezone99"  class="form-control" required="" style="margin-top: 3px;">
                                                            <option value="">Select Language</option>
                                                            <option value="English">English</option> 
                                                            <option value="French">French</option> 
                                                            <option value="German">German</option> 
                                                            <option value="Spanish">Spanish</option>   
                                                            <option value="Japanese">Japanese</option>
                                                            <option value="Hindi">Hindi</option>
                                                            <option value="Panjabi">Panjabi</option>
                                                        </select>
														
														

                                                        <label class="control-label">Country</label>
                                                        <div class="">
                                                            <select class="form-control gds-cr" country-data-region-id="gds-cr-one" data-language="en" data-gds-loaded="true">
                                                                <option value="">Please select a country.</option><option value="Afghanistan" data-class="af">Afghanistan</option><option value="Aland Islands" data-class="ax">Aland Islands</option><option value="Albania" data-class="al">Albania</option><option value="Algeria" data-class="dz">Algeria</option><option value="American Samoa" data-class="as">American Samoa</option><option value="Andorra" data-class="ad">Andorra</option><option value="Angola" data-class="ao">Angola</option><option value="Anguilla" data-class="ai">Anguilla</option><option value="Antarctica" data-class="aq">Antarctica</option><option value="Antigua and Barbuda" data-class="ag">Antigua and Barbuda</option><option value="Argentina" data-class="ar">Argentina</option><option value="Armenia" data-class="am">Armenia</option><option value="Aruba" data-class="aw">Aruba</option><option value="Australia" data-class="au">Australia</option><option value="Austria" data-class="at">Austria</option><option value="Azerbaijian" data-class="az">Azerbaijian</option><option value="Bahamas" data-class="bs">Bahamas</option><option value="Bahrain" data-class="bh">Bahrain</option><option value="Bangladesh" data-class="bd">Bangladesh</option><option value="Barbados" data-class="bb">Barbados</option><option value="Belarus" data-class="by">Belarus</option><option value="Belgium" data-class="be">Belgium</option><option value="Belize" data-class="bz">Belize</option><option value="Benin" data-class="bj">Benin</option><option value="Bermuda" data-class="bm">Bermuda</option><option value="Bhutan" data-class="bt">Bhutan</option><option value="Bolivia, Plurinational State of" data-class="bo">Bolivia, Plurinational State of</option><option value="Bonaire, Sint Eustatius and Saba" data-class="bq">Bonaire, Sint Eustatius and Saba</option><option value="Bosnia and Herzegovina" data-class="ba">Bosnia and Herzegovina</option><option value="Botswana" data-class="bw">Botswana</option><option value="Brazil" data-class="br">Brazil</option><option value="British Indian Ocean Territory" data-class="io">British Indian Ocean Territory</option><option value="Brunei Darussalam" data-class="bn">Brunei Darussalam</option><option value="Bulgaria" data-class="bg">Bulgaria</option><option value="Burkina Faso" data-class="bf">Burkina Faso</option><option value="Burundi" data-class="bi">Burundi</option><option value="Cabo Verde" data-class="cv">Cabo Verde</option><option value="Cambodia" data-class="kh">Cambodia</option><option value="Cameroon" data-class="cm">Cameroon</option><option value="Canada" data-class="ca">Canada</option><option value="Cayman Islands" data-class="ky">Cayman Islands</option><option value="Central African Republic" data-class="cf">Central African Republic</option><option value="Chad" data-class="td">Chad</option><option value="Chile" data-class="cl">Chile</option><option value="China" data-class="cn">China</option><option value="Christmas Island" data-class="cx">Christmas Island</option><option value="Cocos (Keeling) Islands" data-class="cc">Cocos (Keeling) Islands</option><option value="Colombia" data-class="co">Colombia</option><option value="Comoros" data-class="km">Comoros</option><option value="Congo" data-class="cg">Congo</option><option value="Congo, The Democratic Republic of The" data-class="cd">Congo, The Democratic Republic of The</option><option value="Cook Islands" data-class="ck">Cook Islands</option><option value="Costa Rica" data-class="cr">Costa Rica</option><option value="Cote D'ivoire" data-class="ci">Cote D'ivoire</option><option value="Croatia" data-class="hr">Croatia</option><option value="Cuba" data-class="cu">Cuba</option><option value="Curacao" data-class="cw">Curacao</option><option value="Cyprus" data-class="cy">Cyprus</option><option value="Czech Republic" data-class="cz">Czech Republic</option><option value="Denmark" data-class="dk">Denmark</option><option value="Djibouti" data-class="dj">Djibouti</option><option value="Dominica" data-class="dm">Dominica</option><option value="Dominican Republic" data-class="do">Dominican Republic</option><option value="Ecuador" data-class="ec">Ecuador</option><option value="Egypt" data-class="eg">Egypt</option><option value="El Salvador" data-class="sv">El Salvador</option><option value="Equatorial Guinea" data-class="gq">Equatorial Guinea</option><option value="Eritrea" data-class="er">Eritrea</option><option value="Estonia" data-class="ee">Estonia</option><option value="Ethiopia" data-class="et">Ethiopia</option><option value="Falkland Islands (Malvinas)" data-class="fk">Falkland Islands (Malvinas)</option><option value="Faroe Islands" data-class="fo">Faroe Islands</option><option value="Fiji" data-class="fj">Fiji</option><option value="Finland" data-class="fi">Finland</option><option value="France" data-class="fr">France</option><option value="French Guiana" data-class="gf">French Guiana</option><option value="French Polynesia" data-class="pf">French Polynesia</option><option value="French Southern Territories" data-class="tf">French Southern Territories</option><option value="Gabon" data-class="ga">Gabon</option><option value="Gambia" data-class="gm">Gambia</option><option value="Georgia" data-class="ge">Georgia</option><option value="Germany" data-class="de">Germany</option><option value="Ghana" data-class="gh">Ghana</option><option value="Gibraltar" data-class="gi">Gibraltar</option><option value="Greece" data-class="gr">Greece</option><option value="Greenland" data-class="gl">Greenland</option><option value="Grenada" data-class="gd">Grenada</option><option value="Guadeloupe" data-class="gp">Guadeloupe</option><option value="Guam" data-class="gu">Guam</option><option value="Guatemala" data-class="gt">Guatemala</option><option value="Guernsey" data-class="gg">Guernsey</option><option value="Guinea" data-class="gn">Guinea</option><option value="Guinea-Bissau" data-class="gw">Guinea-Bissau</option><option value="Guyana" data-class="gy">Guyana</option><option value="Haiti" data-class="ht">Haiti</option><option value="Holy See" data-class="va">Holy See</option><option value="Honduras" data-class="hn">Honduras</option><option value="Hong Kong" data-class="hk">Hong Kong</option><option value="Hungary" data-class="hu">Hungary</option><option value="Iceland" data-class="is">Iceland</option><option value="India" data-class="in">India</option><option value="Indonesia" data-class="id">Indonesia</option><option value="Iran, Islamic Republic of" data-class="ir">Iran, Islamic Republic of</option><option value="Iraq" data-class="iq">Iraq</option><option value="Ireland" data-class="ie">Ireland</option><option value="Isle of Man" data-class="im">Isle of Man</option><option value="Israel" data-class="il">Israel</option><option value="Italy" data-class="it">Italy</option><option value="Jamaica" data-class="jm">Jamaica</option><option value="Japan" data-class="jp">Japan</option><option value="Jersey" data-class="je">Jersey</option><option value="Jordan" data-class="jo">Jordan</option><option value="Kazakhstan" data-class="kz">Kazakhstan</option><option value="Kenya" data-class="ke">Kenya</option><option value="Kiribati" data-class="ki">Kiribati</option><option value="Korea, Democratic People's Republic of" data-class="kp">Korea, Democratic People's Republic of</option><option value="Korea, Republic of" data-class="kr">Korea, Republic of</option><option value="Kuwait" data-class="kw">Kuwait</option><option value="Kyrgyzstan" data-class="kg">Kyrgyzstan</option><option value="Lao People's Democratic Republic" data-class="la">Lao People's Democratic Republic</option><option value="Latvia" data-class="lv">Latvia</option><option value="Lebanon" data-class="lb">Lebanon</option><option value="Lesotho" data-class="ls">Lesotho</option><option value="Liberia" data-class="lr">Liberia</option><option value="Libya" data-class="ly">Libya</option><option value="Liechtenstein" data-class="li">Liechtenstein</option><option value="Lithuania" data-class="lt">Lithuania</option><option value="Luxembourg" data-class="lu">Luxembourg</option><option value="Macao" data-class="mo">Macao</option><option value="Macedonia, The Former Yugoslav Republic of" data-class="mk">Macedonia, The Former Yugoslav Republic of</option><option value="Madagascar" data-class="mg">Madagascar</option><option value="Malawi" data-class="mw">Malawi</option><option value="Malaysia" data-class="my">Malaysia</option><option value="Maldives" data-class="mv">Maldives</option><option value="Mali" data-class="ml">Mali</option><option value="Malta" data-class="mt">Malta</option><option value="Marshall Islands" data-class="mh">Marshall Islands</option><option value="Martinique" data-class="mq">Martinique</option><option value="Mauritania" data-class="mr">Mauritania</option><option value="Mauritius" data-class="mu">Mauritius</option><option value="Mayotte" data-class="yt">Mayotte</option><option value="Mexico" data-class="mx">Mexico</option><option value="Micronesia, Federated States of" data-class="fm">Micronesia, Federated States of</option><option value="Moldova, Republic of" data-class="md">Moldova, Republic of</option><option value="Monaco" data-class="mc">Monaco</option><option value="Mongolia" data-class="mn">Mongolia</option><option value="Montenegro" data-class="me">Montenegro</option><option value="Montserrat" data-class="ms">Montserrat</option><option value="Morocco" data-class="ma">Morocco</option><option value="Mozambique" data-class="mz">Mozambique</option><option value="Myanmar" data-class="mm">Myanmar</option><option value="Namibia" data-class="na">Namibia</option><option value="Nauru" data-class="nr">Nauru</option><option value="Nepal" data-class="np">Nepal</option><option value="Netherlands" data-class="nl">Netherlands</option><option value="New Caledonia" data-class="nc">New Caledonia</option><option value="New Zealand" data-class="nz">New Zealand</option><option value="Nicaragua" data-class="ni">Nicaragua</option><option value="Niger" data-class="ne">Niger</option><option value="Nigeria" data-class="ng">Nigeria</option><option value="Niue" data-class="nu">Niue</option><option value="Norfolk Island" data-class="nf">Norfolk Island</option><option value="Northern Mariana Islands" data-class="mp">Northern Mariana Islands</option><option value="Norway" data-class="no">Norway</option><option value="Oman" data-class="om">Oman</option><option value="Pakistan" data-class="pk">Pakistan</option><option value="Palau" data-class="pw">Palau</option><option value="Palestine, State of" data-class="ps">Palestine, State of</option><option value="Panama" data-class="pa">Panama</option><option value="Papua New Guinea" data-class="pg">Papua New Guinea</option><option value="Paraguay" data-class="py">Paraguay</option><option value="Peru" data-class="pe">Peru</option><option value="Philippines" data-class="ph">Philippines</option><option value="Pitcairn" data-class="pn">Pitcairn</option><option value="Poland" data-class="pl">Poland</option><option value="Portugal" data-class="pt">Portugal</option><option value="Puerto Rico" data-class="pr">Puerto Rico</option><option value="Qatar" data-class="qa">Qatar</option><option value="Reunion" data-class="re">Reunion</option><option value="Romania" data-class="ro">Romania</option><option value="Russian Federation" data-class="ru">Russian Federation</option><option value="Rwanda" data-class="rw">Rwanda</option><option value="Saint Barthelemy" data-class="bl">Saint Barthelemy</option><option value="Saint Helena, Ascension and Tristan Da Cunha" data-class="sh">Saint Helena, Ascension and Tristan Da Cunha</option><option value="Saint Kitts and Nevis" data-class="kn">Saint Kitts and Nevis</option><option value="Saint Lucia" data-class="lc">Saint Lucia</option><option value="Saint Martin (French Part)" data-class="mf">Saint Martin (French Part)</option><option value="Saint Pierre and Miquelon" data-class="pm">Saint Pierre and Miquelon</option><option value="Saint Vincent and The Grenadines" data-class="vc">Saint Vincent and The Grenadines</option><option value="Samoa" data-class="ws">Samoa</option><option value="San Marino" data-class="sm">San Marino</option><option value="Sao Tome and Principe" data-class="st">Sao Tome and Principe</option><option value="Saudi Arabia" data-class="sa">Saudi Arabia</option><option value="Senegal" data-class="sn">Senegal</option><option value="Serbia" data-class="rs">Serbia</option><option value="Seychelles" data-class="sc">Seychelles</option><option value="Sierra Leone" data-class="sl">Sierra Leone</option><option value="Singapore" data-class="sg">Singapore</option><option value="Sint Maarten (Dutch Part)" data-class="sx">Sint Maarten (Dutch Part)</option><option value="Slovakia" data-class="sk">Slovakia</option><option value="Slovenia" data-class="si">Slovenia</option><option value="Solomon Islands" data-class="sb">Solomon Islands</option><option value="Somalia" data-class="so">Somalia</option><option value="South Africa" data-class="za">South Africa</option><option value="South Georgia and The South Sandwich Islands" data-class="gs">South Georgia and The South Sandwich Islands</option><option value="South Sudan" data-class="ss">South Sudan</option><option value="Spain" data-class="es">Spain</option><option value="Sri Lanka" data-class="lk">Sri Lanka</option><option value="Sudan" data-class="sd">Sudan</option><option value="Suriname" data-class="sr">Suriname</option><option value="Svalbard and Jan Mayen" data-class="sj">Svalbard and Jan Mayen</option><option value="Swaziland" data-class="sz">Swaziland</option><option value="Sweden" data-class="se">Sweden</option><option value="Switzerland" data-class="ch">Switzerland</option><option value="Syrian Arab Republic" data-class="sy">Syrian Arab Republic</option><option value="Taiwan, Province of China" data-class="tw">Taiwan, Province of China</option><option value="Tajikistan" data-class="tj">Tajikistan</option><option value="Tanzania, United Republic of" data-class="tz">Tanzania, United Republic of</option><option value="Thailand" data-class="th">Thailand</option><option value="Timor-Leste" data-class="tl">Timor-Leste</option><option value="Togo" data-class="tg">Togo</option><option value="Tokelau" data-class="tk">Tokelau</option><option value="Tonga" data-class="to">Tonga</option><option value="Trinidad and Tobago" data-class="tt">Trinidad and Tobago</option><option value="Tunisia" data-class="tn">Tunisia</option><option value="Turkey" data-class="tr">Turkey</option><option value="Turkmenistan" data-class="tm">Turkmenistan</option><option value="Turks and Caicos Islands" data-class="tc">Turks and Caicos Islands</option><option value="Tuvalu" data-class="tv">Tuvalu</option><option value="Uganda" data-class="ug">Uganda</option><option value="Ukraine" data-class="ua">Ukraine</option><option value="United Arab Emirates" data-class="ae">United Arab Emirates</option><option value="United Kingdom" data-class="gb">United Kingdom</option><option value="United States" data-class="us">United States</option><option value="United States Minor Outlying Islands" data-class="um">United States Minor Outlying Islands</option><option value="Uruguay" data-class="uy">Uruguay</option><option value="Uzbekistan" data-class="uz">Uzbekistan</option><option value="Vanuatu" data-class="vu">Vanuatu</option><option value="Venezuela, Bolivarian Republic of" data-class="ve">Venezuela, Bolivarian Republic of</option><option value="Viet Nam" data-class="vn">Viet Nam</option><option value="Virgin Islands, British" data-class="vg">Virgin Islands, British</option><option value="Virgin Islands, U.S." data-class="vi">Virgin Islands, U.S.</option><option value="Wallis and Futuna" data-class="wf">Wallis and Futuna</option><option value="Western Sahara" data-class="eh">Western Sahara</option><option value="Yemen" data-class="ye">Yemen</option><option value="Zambia" data-class="zm">Zambia</option><option value="Zimbabwe" data-class="zw">Zimbabwe</option></select>
                                                        </div>
                                                    </div>


                                            </div>
                                        </div>



                                        <!-- Material checked -->                                             
                                        <button type="submit" name="update_langauge" style="background:#009688;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;margin-top:23px;">Update Language </button>                                            
                                        </form>

                                    </div>                                            
                                </div>
                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    /* Style the element that is used to open and close the accordion class */
    p.accordion {
        background-color: #efefef;;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        text-align: left;              
        border: none;
        outline: none;
        transition: 0.4s;
        margin-bottom:10px;

    }
    /* Add a background color to the accordion if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
    p.accordion.active, p.accordion:hover {
        background-color: #ddd;
    }
    /* Unicode character for "plus" sign (+) */
    p.accordion:after {
        content: '+';
        font-size: 18px;
        color: #777;
        float: right;
        margin-left: 5px;
    }
    /* Unicode character for "minus" sign (-) */
    p.accordion.active:after {
        content: "-";
    }
    /* Style the element that is used for the panel class */
    div.panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: 0.4s ease-in-out;
        opacity: 0;
        margin-bottom:10px;
    }
    div.panel.show {
        opacity: 1;
        max-height: 500px; /* Whatever you like, as long as its more than the height of the content (on all screen sizes) */
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#timezone').on('change', function () {
            var timezone = $(this).val();
            //alert(timezone);
            if (timezone) {
                $.ajax({
                    type: 'POST',
                    url: 'settime_zone.php',
                    data: 'timezone=' + timezone,
                    success: function (html) {
                        $('#res_time').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
                $('.default-time').hide();
            } else {
                $('#res_time').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        var acc = document.getElementsByClassName("accordion");
        var panel = document.getElementsByClassName('panel');
        for (var i = 0; i < acc.length; i++) {
            acc[i].onclick = function () {
                var setClasses = !this.classList.contains('active');
                setClass(acc, 'active', 'remove');
                setClass(panel, 'show', 'remove');
                if (setClasses) {
                    this.classList.toggle("active");
                    this.nextElementSibling.classList.toggle("show");
                }
            }
        }
        function setClass(els, className, fnName) {
            for (var i = 0; i < els.length; i++) {
                els[i].classList[fnName](className);
            }
        }
    });
</script>
<script type="text/javascript">

    /***********************************************
     * Cross browser Marquee II- � Dynamic Drive (www.dynamicdrive.com)
     * This notice MUST stay intact for legal use
     * Visit http://www.dynamicdrive.com/ for this script and 100s more.
     ***********************************************/
    var video = document.getElementById("myVideoBefore");
    var videoo = document.getElementById("myVideoAfter");
    var beforei = document.getElementById("beforeicon");
    var afteri = document.getElementById("aftericon");
    function playVideoo() {
        if (videoo.paused) {
            videoo.play();
            afteri.classList.add('fa-pause-circle-o');
            afteri.classList.remove('fa-play-circle-o');
            //btn.innerHTML = "Pause";
        } else {
            videoo.pause();
            afteri.classList.add('fa-play-circle-o');
            afteri.classList.remove('fa-pause-circle-o');
            //btn.innerHTML = "Play";
        }
    }

    function playVideo() {
        if (video.paused) {
            video.play();
            beforei.classList.add('fa-pause-circle-o');
            beforei.classList.remove('fa-play-circle-o');
            //btn.innerHTML = "Pause";
        } else {
            video.pause();
            beforei.classList.add('fa-play-circle-o');
            beforei.classList.remove('fa-pause-circle-o');
            //btn.innerHTML = "Play";
        }
    }
</script>
<!-- Cookie Consent by https://PrivacyPolicies.com 
<script type="text/javascript" src="//www.PrivacyPolicies.com/cookie-consent/releases/3.0.0/cookie-consent.js"></script>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        cookieconsent.run({"notice_banner_type": "simple", "consent_type": "implied", "palette": "light", "change_preferences_selector": "#changePreferences", "language": "en"});
    });
</script>

<noscript><a href="https://www.privacypolicies.com/cookie-consent/">Cookie Consent by Privacy Policies Generator</a></noscript>
<!-- End Cookie Consent -->

<?php echo ajaxJsFooter(); ?>
</body>
</html>