<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getClientPageData($pageUrl, $customerId);
$pageData = $clientPageData[0];
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>    
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> CLIENT PAGE DETAILS
                                <small>View / edit all the details of your client page</small>
                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="welcome.php">Home & Static Page</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Edit Client Page Details</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Edit Client Page </span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <a class="pull-right">
                                                        <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    </a>
                                                     <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_client" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag"/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_client" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->
                                
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="../sitepanel/updatePage_content.php" id="submit_form" method="post" enctype="multipart/form-data">
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <div class="tab-content"> 
                                                    <div class="expDiv" onclick="$('#general_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>
                                                    <div id="general_content" style="display:none;"> 
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heading 1
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h1" value="<?php echo $pageData->client_h1; ?>" />
                                                                <span class="help-block"> Provide first heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Header Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
															 <?php if ($pageData->heade_img) { ?>
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                             <?php } else { ?>
                                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/staticpage/<?php echo $pageData->header_img; ?>.jpeg" alt=""/>
                                                               <?php } ?> 
																	</div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="image_header"> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heading 1 Content
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="h1_cont"><?php echo $pageData->client_h1_cont; ?></textarea>
                                                                <span class="help-block"> Provide first heading's content </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="expDiv" onclick="$('#box_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Boxed Contents </h3></div>
                                                    <div id="box_content" style="display:none;">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 1 Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box1" value="<?php echo $pageData->client_box1; ?>" />
                                                                <span class="help-block"> Provide first feature box heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Feature Box 1 Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="box1_img"> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 1 Content
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="box1_cont"><?php echo $pageData->client_box1_cont; ?></textarea>
                                                                <span class="help-block"> Provide first feature box's content </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 2 Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box2" value="<?php echo $pageData->client_box2; ?>" />
                                                                <span class="help-block"> Provide second feature box heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Feature Box 2 Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="box2_img"> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 2 Content
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="box2_cont"><?php echo $pageData->client_box2_cont; ?></textarea>
                                                                <span class="help-block"> Provide second feature box's content </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 3 Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box3" value="<?php echo $pageData->client_box3; ?>" />
                                                                <span class="help-block"> Provide third feature box heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Feature Box 3 Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="box3_img"> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 3 Content
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="box3_cont"><?php echo $pageData->client_box3_cont; ?></textarea>
                                                                <span class="help-block"> Provide third feature box's content </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 4 Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box4" value="<?php echo $pageData->client_box4; ?>" />
                                                                <span class="help-block"> Provide fourth feature box heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Feature Box 4 Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="box4_img"> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Feature Box 4 Content
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="box4_cont"><?php echo $pageData->client_box4_cont; ?></textarea>
                                                                <span class="help-block"> Provide fourth feature box's content </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="expDiv" onclick="$('#content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3>Contents(Left/Right) </h3></div>
                                                    <div id="content" style="display:none;">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heading 2
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h2" value="<?php echo $pageData->client_h2; ?>" />
                                                                <span class="help-block"> Provide  main heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Left -> Heading 1
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h2_subhead1" value="<?php echo $pageData->client_h2_subhead1; ?>" />
                                                                <span class="help-block"> Provide  heading's heading 1 </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Left -> Heading 1 Content
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="h2_subhead1_cont"><?php echo $pageData->client_h2_subhead1_cont; ?></textarea>
                                                                <span class="help-block"> Provide Heading 1 content </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Right -> Heading 2 
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h2_subhead2" value="<?php echo $pageData->client_h2_subhead2; ?>" />
                                                                <span class="help-block"> Provide  heading's  2 </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Right -> Heading   Content 2
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control" name="h2_subhead2_cont"><?php echo $pageData->client_h2_subhead2_cont; ?></textarea>
                                                                <span class="help-block"> Provide content 2 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="expDiv" onclick="$('#video-section').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3>Video Section </h3></div>
                                                    <div id="video-section" style="display:none;">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heading 3
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h3" value="<?php echo $pageData->client_h3; ?>" />
                                                                <span class="help-block"> Provide third heading </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heading 3 Video URL 1
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h3_video1" value="<?php echo $pageData->client_h3_video1; ?>" />
                                                                <span class="help-block"> Provide first video URL </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heading 3 Video URL 2
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="h3_video2" value="<?php echo $pageData->client_h3_video2; ?>" />
                                                                <span class="help-block"> Provide second video URL </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="javascript:;" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back 
                                                        </a>
                                                        <input type="hidden"  name="page_url" value="<?php echo $pageData->page_url; ?>" />
                                                        <input type="submit" class="btn green" name="edit_client_page" value="Save" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
        <div class="quick-nav-overlay"></div>
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>
