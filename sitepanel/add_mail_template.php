<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$enquiryData = new manageEnquiry();
$get_mail = $enquiryData->GetMailTemplate($customerId);

?>
<?php
if (empty($type_id)) {
    $heading = "Add New Mail Template";
}

$script_name = "add_mail_template.php";
$redirect_script_name = "manage_mail_template.php";

#################### Add Record ##################
//admin_header();
?>
<html>

    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <style>
            .ques_table tbody:before {
                content: "-";
                display: block;
                line-height: 1em;
                color: transparent;
            }

            .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }

            tr:nth-child(odd){
                background-color:#fff;
            }

            th {

                padding:15px;
                font-size:large;}
            td {
                padding:10px;}

            .image img {
                -webkit-transition: all 1s ease; /* Safari and Chrome */
                -moz-transition: all 1s ease; /* Firefox */
                -ms-transition: all 1s ease; /* IE 9 */
                -o-transition: all 1s ease; /* Opera */
                transition: all 1s ease;
            }

            .image:hover img {
                -webkit-transform:scale(1.25); /* Safari and Chrome */
                -moz-transform:scale(1.25); /* Firefox */
                -ms-transform:scale(1.25); /* IE 9 */
                -o-transform:scale(1.25); /* Opera */
                transform:scale(1.25);
            }		

        </style>
        <script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <?php
        adminCss();
        ?>
    </head>


    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="add_temp" style="background:white;">
                            <div class="row">
                                <div  class="col-md-12">
                                    <div class="portlet light bordered" style="padding-bottom:15px ! important;">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">DESIGN YOUR TEMPLATE </span>
                                                <span class="caption-helper">Design your email template..</span>
                                            </div>
                                        </div>

                                        <div>
                                            <center>
                                                <button type="button" class="btn green-haze btn-outline sbold uppercase active" id="rmv" onclick="your_click()">Create Your Own Template</button>
                                                &nbsp&nbsp&nbsp
                                                <button type="button" class="btn green-haze btn-outline sbold uppercase" onclick="our_theam()">Our Themes</button>
                                            </center>
                                        </div>

                                        <script>
                                            function our_theam() {

                                                document.getElementById('mobile_tab').style.display = 'block';
                                                document.getElementById('your_email_template').style.display = 'none';
                                                $('#rmv').removeClass("active");
                                            }
                                            function your_click() {

                                                document.getElementById('mobile_tab').style.display = 'none';
                                                document.getElementById('your_email_template').style.display = 'block';
                                            }
                                        </script>	

                                        <div id='mobile_tab' style="display:none; padding-top:80px;">
                                            <?php
                                            //$sel = "select * from mail_templates where user='admin' and customerID='$custID'";
                                           // $quer = mysql_query($sel);
                                            if ($get_mail == true) {
                                                $count = 1;
                                                foreach ($get_mail as $row) {
                                                    $id = $row->s_no;
                                                    if ($count == "1") {
                                                        $color = "#32c5d2";
                                                    }
                                                    if ($count == "2") {
                                                        $color = "#3598dc";
                                                    }
                                                    if ($count == "3") {
                                                        $color = "#36D7B7";
                                                    }
                                                    if ($count == "4") {
                                                        $color = "#5e738b";
                                                    }
                                                    if ($count == "5") {
                                                        $color = "#1BA39C";
                                                    }
                                                    if ($count == "6") {
                                                        $color = "#ac83b1";
                                                    }
                                                    if ($count == "7") {
                                                        $color = "#578ebe";
                                                    }
                                                    if ($count == "8") {
                                                        $color = "#578ebe";
                                                    }
                                                    if ($count == "9") {
                                                        $color = "#8775a7";
                                                    }
                                                    if ($count == "10") {
                                                        $color = "#E26A6A";
                                                    }
                                                    if ($count == "11") {
                                                        $color = "#29b4b6";
                                                    }
                                                    if ($count == "12") {
                                                        $color = "#4B77BE";
                                                    }
                                                    if ($count == "13") {
                                                        $color = "#c49f47";
                                                    }
                                                    if ($count == "14") {
                                                        $color = "#67809F";
                                                    }
                                                    if ($count == "15") {
                                                        $color = "#8775a7";
                                                    }
                                                    if ($count == "16") {
                                                        $color = "#73CEBB";
                                                    }
                                                    ?>
                                                    <h5 style="padding:10px; border-left:5px solid <?php echo $color; ?>;background-color:#e5e5e5;"><a href= "edit_mail_template.php?<?php echo $id; ?>" style="color: <?php echo $color; ?>;font-weight:bold;text-decoration: none;"><?php echo $row->name; ?> ( <?php echo $row->type; ?> )</a></h5>
                                                    <?php
                                                    $count++;
                                                }
                                            }
                                            ?>

                                        </div>	

                                        <div class="portlet-body form" style="display:block;" id="your_email_template"><br><br>
                                            <form role="form" action="mail_template_insert.php" method="post">
                                                <div class="form-body">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <div class="form-group form-md-line-input" >
                                                                    <input type="text" name="temp_name" class="form-control" id="form_control_1" placeholder="Enter Template Name">
                                                                    <label for="form_control_1">Template Name</label>
                                                                    <span class="help-block">Mention the name you wish to give to your template..</span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group form-md-line-input has-info">
                                                                    <select class="form-control" name="temp_type" id="form_control_1">
                                                                        <option value=""></option>
                                                                        <option value="1">For User</option>
                                                                        <option value="2">For Admin</option>
                                                                    </select>
                                                                    <label for="form_control_1">Template Type</label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>	
                                                                <div class="form-group form-md-line-input">
                                                                    <input type="text" name="mail_from" class="form-control" id="form_control_1" placeholder="Enter The Permanent Sender's Email Address (If Any)">
                                                                    <label for="form_control_1">Mail From</label>
                                                                    <span class="help-block">Mention the mail address of the sender..</span>
                                                                </div>
                                                            </td>
                                                            <td>	
                                                                <div class="form-group form-md-line-input">
                                                                    <input type="text" name="mail_to" name="mail_from" class="form-control" id="form_control_1" placeholder="Enter The Permanent Reciever's Email Address (If Any)">
                                                                    <label for="form_control_1">Mail To</label>
                                                                    <span class="help-block">Mention the mail address of the reciever..</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group form-md-line-input" id="cc_email" style="display:none;">
                                                                    <input type="text" name="add_cc" class="form-control" id="form_control_1" placeholder="Add CC to Mail (If Any)">
                                                                    <label for="form_control_1">Add CC</label>
                                                                    <span class="help-block">Mention the mail address of the CC..</span>
                                                                </div>
                                                            </td>
                                                            <td>

                                                                <div class="form-group form-md-line-input pull-right">
                                                                    <a href="javascript:;" class="btn btn-sm grey-cascade" onclick="$('#cc_email').toggle()"> Add CC
                                                                        <i class="fa fa-plus"></i></a>
                                                                </div>													
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group form-md-line-input" >
                                                                    <input type="text" class="form-control" name="temp_subject" id="form_control_1" placeholder="Enter The Subject of The Email">
                                                                    <label for="form_control_1">Template Subject</label>
                                                                    <span class="help-block">Mention the subject of the mail..</span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group form-md-line-input has-info">
                                                                    <select class="form-control" name="temp_form" id="form_control_1" placeholder="Enter Template Type" required>
                                                                        <option value=""></option>
                                                                        <option value="popup_call_back"> Call Back Form</option>
                                                                        <option value="popup_instant_booking">Booking Form</option>
                                                                        <option value="contact_us">Contact Us Form</option>
                                                                    </select>
                                                                    <label for="form_control_1">Assign This Template To Which Form</label>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="form-group form-md-line-input">
                                                                    <textarea name="mobpropertyInfo" class="comment_post form-control" rows="5" cols="70">Design Your Email Body <br> ***************************** </textarea>
                                                                    <label for="form_control_1">Template Body</label>
                                                                    <script type="text/javascript">
                                                                        CKEDITOR.replace('mobpropertyInfo');
                                                                    </script>													
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="form-group form-md-line-input">
                                                                    <textarea name="temp_signature" class="comment_post form-control" rows="5" cols="70">Design Your Email Signature <br> </textarea>
                                                                    <label for="form_control_1">Template Signature</label>
                                                                    <script type="text/javascript">
                                                                        CKEDITOR.replace('temp_signature');
                                                                    </script>													
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="form-actions noborder pull-right">
                                                                    <input type="submit" value="Submit" name="add_template" class="btn blue" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
admin_footer();
exit;
?>
