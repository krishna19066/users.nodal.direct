<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$propertyCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
?>
<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getOurClientPageData($pageUrl, $customerId);
//print_r($clientPageData);
$pageData = $clientPageData[0];
$ourclient = $pageData;
$numb = count($clientPageData);
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
?>

<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>  
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">

            <!-- BEGIN CONTAINER -->

            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">

                    <!-- BEGIN CONTENT BODY -->

                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Our Clients Page</span>

                                            <span class="caption-helper">Add/Edit Our Clients Page Here..</span>

                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="provide page content goes here!" data-original-title="Update page data" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-body ">
                                                    <div style="width:100%;" class="clear"> 
                                                        <a class="pull-right">
                                                           
                                                            <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                        </a>
                                                         <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                      <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_ourclient" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag"/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_ourclient" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->
                                  
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <div class="expDiv" onclick="$('#general_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>
                                                <div id="general_content" style="display:none;">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Heading 1
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="h1" value="<?php echo $ourclient->h1; ?>" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Heading 1 Content
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="h1content" rows="6" required ><?php echo html_entity_decode($ourclient->h1content); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                               <div class="expDiv" onclick="$('#collapsible').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3>Collapsible Content</h3></div>
                                               <div id="collapsible" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Collapsible Heading 1
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="collapseh1" value="<?php echo $ourclient->collapsehead1; ?>" required />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Collapsible Content 1 
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" name="collapsecont1" rows="6" required ><?php echo html_entity_decode($ourclient->collapsecont1); ?></textarea>
                                                    </div>
                                                </div>
                                                <?php
                                                if ($numb == "0" || $ourclient->collapsehead2 != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsible Heading 2
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapseh2" value="<?php echo $ourclient->collapsehead2; ?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsible Content 2 
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapsecont2" rows="6"  ><?php echo html_entity_decode($ourclient->collapsecont2); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                if ($numb == "0" || $ourclient->collapsehead3 != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsible Heading 3
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapseh3" value="<?php echo $ourclient->collapsehead3; ?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsible Content 3
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapsecont3" rows="6"  ><?php echo html_entity_decode($ourclient->collapsecont3); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                if ($numb == "0" || $ourclient->collapsehead4 != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsible Heading 4
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapseh4" value="<?php echo $ourclient->collapsehead4; ?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsible Content 4
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapsecont4" rows="6"  ><?php echo html_entity_decode($ourclient->collapsecont4); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                               </div>
                                               <div class="expDiv" onclick="$('#Box_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Box Contents</h3></div>
                                                <div id="Box_content" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h2" value="<?php echo $ourclient->h2; ?>"  />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Image Path 1
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <?php if($ourclient->divimg1 == ''){ ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                            <?php }else { ?>
                                                            <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/our-client/<?php echo $ourclient->divimg1; ?>.jpg" alt="" />
                                                            <?php } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="divimg1"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                      
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="divhead1" value="<?php echo $ourclient->divhead1; ?>"  />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Div Content 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="divcontent1" rows="6" required ><?php echo html_entity_decode($ourclient->divcontent1); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Image Path 2
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                             <?php if($ourclient->divimg2 == ''){ ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                            <?php }else { ?>
                                                            <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/our-client/<?php echo $ourclient->divimg2; ?>.jpg" alt="" />
                                                            <?php } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="divimg2"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="divhead2" value="<?php echo $ourclient->divhead2; ?>"  />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Div Content 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="divcontent2" rows="6" required ><?php echo html_entity_decode($ourclient->divcontent2); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Image Path 2
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                           <?php if($ourclient->divimg3 == ''){ ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                            <?php }else { ?>
                                                            <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/our-client/<?php echo $ourclient->divimg3; ?>.jpg" alt="" />
                                                            <?php } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="divimg3"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Heading 3
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="divhead3" value="<?php echo $ourclient->divhead3; ?>"  />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Div Content 3
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="divcontent3" rows="6" required ><?php echo html_entity_decode($ourclient->divcontent3); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Side Div Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="sidedivheading" value="<?php echo $ourclient->sidedivheading; ?>" />
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php
                                                            if ($numb == "0") {
                                                                ?>
                                                                <input type="hidden"  name="page_url" value="<?php echo $pageUrl; ?>" />
                                                                <input type="hidden" name="savecont" value="Add Content">
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="hidden"  name="page_url" value="<?php echo $ourclient->page_url; ?>" />
                                                                <input type="hidden" name="savecont" value="Save Content">
                                                                <?php
                                                            }
                                                            ?>

                                                            <input type="submit" name="ourclientsub" class="btn green" id="submit" value="Save">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php echo ajaxJsFooter(); ?>
