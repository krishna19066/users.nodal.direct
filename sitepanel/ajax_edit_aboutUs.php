<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$ajaxclientCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$bucket = $cloud_keyData->bucket;
$base_url = $cloud_keyData->website;
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getAboutUsPageData($customerId);
$teammemdata = $ajaxclientCont->getTeamMember($customerId);
$aboutus = $clientPageData[0];
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?> 

    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-equalizer font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Edit About Us Page</span>
                                        <span class="caption-helper">Add/Edit About Us Page Here..</span>
                                        <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <a class="pull-right">
                                                        <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px; cursor: pointer;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    </a>
                                                    <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_aboutUs" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_aboutUs" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->

                                <div class="portlet-body form">
                                    <span style="float:right;"><a href="<?php echo $base_url; ?>/<?php echo $pageUrl; ?>.html" target="_blank"><input type="button" style="background:#03A9F4;color:white;border:none;height:35px;width:180px;font-size:14px;margin-right: 11px;margin-top: 7px;" data-html="true"  value="Preview Page" /></a></span><br/><br/>
                                    <form class="form-horizontal" name="formn" method="post" <?php if ($bucket) { ?> action="../sitepanel/S3-createObject.php" <?php } else { ?>  action="../sitepanel/updatePage_content.php"<?php } ?> enctype="multipart/form-data">                                                                                                
                                        <input type="hidden" name="querystr" value="<?php echo $querystr; ?>" />
                                        <div class="form-body">
                                            <div class="expDiv" onclick="$('#general_content').slideToggle();" style="cursor: pointer; border: 1px solid #ececec"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>
                                            <div id="general_content" style="display:none;">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Header Image

                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                            <?php if ($aboutus->heade_img) { ?>
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                            <?php } else { ?>
                                                                <?php if ($bucket) { ?> 
                                                                    <img src="https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $aboutus->header_img; ?><?php if ($customerId == '1') { ?>.jpg<?php } ?>" alt=""/>
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/aboutus/<?php echo $aboutus->header_img; ?>.jpeg" alt=""/>
                                                                <?php }
                                                            } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image_header"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 1
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h1" value="<?php echo $aboutus->about_h1; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Heading 1 Content
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" name="h1_cont" rows="6"><?php echo $aboutus->about_h1_cont; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 2
                                                       <!-- <span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h2" value="<?php echo $aboutus->about_h2; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Heading 2 Content
                                                       <!-- <span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="h2_cont" rows="6"><?php echo $aboutus->about_h2_cont; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-------------------------------------boxes start-------------------------------------------------------------->
                                            <div class="expDiv" onclick="$('#box_content').slideToggle();" style="cursor: pointer; border: 1px solid #ececec"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Boxed Contents </h3></div>
                                            <div id="box_content" style="display:none;">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="well" onclick="$('#content_box_1').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-danger"><span class="label label-danger pull-right">Edit</span><i class="fa fa-thumbs-up fa_line_height"></i> Box - 1 </h4>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="well" onclick="$('#content_box_2').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-success"><span class="label label-success pull-right">Edit</span><i class="fa fa-map-marker fa_line_height"></i> Box - 2 </h4>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="well" onclick="$('#content_box_3').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-primary"><span class="label label-primary pull-right">Edit</span><i class="fa fa-cutlery  fa_line_height"></i> Box - 3 </h4>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="well" onclick="$('#content_box_4').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-success"><span class="label label-success pull-right">Edit</span><i class="fa fa-building-o fa_line_height"></i> Box - 4 </h4>
                                                                </div>
                                                            </div>
                                                        </div><!--/row-->    
                                                    </div><!--/col-12-->
                                                </div><!--/row-->

                                                <div id="content_box_1" style="display:none;"><br/>
                                                    <table width="100%">
                                                        <td><hr /></td>
                                                        <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Box -1</td>
                                                        <td><hr /></td>
                                                    </table>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsable Heading 1
                                                            <!--<span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapse_h1" value="<?php echo $aboutus->about_collapseh1; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsable Heading 1 Content
                                                           <!-- <span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapse_h1_cont" rows="6"><?php echo $aboutus->about_collapseh1_cont; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="content_box_2" style="display:none;"><br/>
                                                    <table width="100%">
                                                        <td><hr /></td>
                                                        <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Box -2</td>
                                                        <td><hr /></td>
                                                    </table>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsable Heading 2
                                                            <!--<span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapse_h2" value="<?php echo $aboutus->about_collapseh2; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsable Heading 2 Content
                                                           <!-- <span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapse_h2_cont" rows="6"><?php echo $aboutus->about_collapseh2_cont; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="content_box_3" style="display:none;"><br/>
                                                    <table width="100%">
                                                        <td><hr /></td>
                                                        <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Box -3</td>
                                                        <td><hr /></td>
                                                    </table>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsable Heading 3
                                                            <!--<span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapse_h3" value="<?php echo $aboutus->about_collapseh3; ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsable Heading 3 Content
                                                           <!-- <span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapse_h3_cont" rows="6"><?php echo $aboutus->about_collapseh3_cont; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="content_box_4" style="display:none;"><br/>
                                                    <table width="100%">
                                                        <td><hr /></td>
                                                        <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Box -4</td>
                                                        <td><hr /></td>
                                                    </table>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsable Heading 4
                                                           <!-- <span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapse_h4" value="<?php echo $aboutus->about_collapseh4; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsable Heading 4 Content
                                                            <!--<span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapse_h4_cont" rows="6"><?php echo $aboutus->about_collapseh4_cont; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 3 (Team heading)
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h3" value="<?php echo $aboutus->about_h3; ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden"  name="page_url" value="<?php echo $aboutus->page_url; ?>" />
                                                        <input type="submit" name="edit_about_page" class="btn green" id="submit" value="Save About Us Page" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>>
                                                    </div>
                                                </div>
                                            </div>

                                    </form><br/><hr/>
                                    <div class="expDiv" onclick="$('#team-member').slideToggle();" style="border: 1px solid; color:white; width: 296px; background: #00BCD4;margin-top: 134px; cursor: pointer;">
                                        <i class="fa fa-plus pull-left" aria-hidden="true" ></i>
                                        <h3> View/Add Team Members</h3>
                                    </div>
                                    <div id="team-member" style="display:none;"><br/>
                                        <!--  <div class="form-group">
                                              <label class="control-label col-md-3"> Heading 3
                                                  <!--<span class="required"> * </span>-->
                                        <!--  </label>
                                         <div class="col-md-4">
                                             <input type="text" class="form-control" name="h3" value="<?php echo $aboutus->about_h3; ?>"/>
                                         </div>
                                     </div>-->

                                        <div class="form-group">	
                                            <label class="control-label col-md-3"> Team Members</label>	
                                            <div class="col-md-9">						
                                                <div style="width:100%;border:1px solid grey;box-shadow:1px 1px 1px grey;padding:20px;">
                                                    <table style="width:100%;">				
                                                        <tr>						
                                                            <th style="width:10%;padding-bottom:10px;">S. No. </th>
                                                            <th style="width:10%;padding-bottom:10px;">Image </th>
                                                            <th style="width:20%;padding-bottom:10px;"> Name </th>	
                                                            <th style="width:20%;padding-bottom:10px;">Category</th>	
                                                            <th style="width:30%;padding-bottom:10px;">Designation</th>	
                                                            <th style="width:20%;padding-bottom:10px;"> Action </th>	
                                                        </tr>							
                                                        <?php
                                                        $teamcount = 1;
                                                        foreach ($teammemdata as $dt) {
                                                            $slno = $dt->s_no;
                                                            $name = $dt->name;
                                                            ?>						
                                                            <tr style="border-bottom:1px solid #b3adad;">	
                                                                <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $teamcount; ?> </td>
                                                                <td style="padding-top:8px;padding-bottom:8px;">  <img src="<?php if ($bucket) { ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $dt->pic_url; ?> <?php } else { ?> http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/team_members/<?php echo $dt->pic_url; ?>.jpeg<?php } ?>" alt="" style='width: 65px;'/> </td>
                                                                <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $dt->name; ?> </td>	
                                                                <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $dt->depart; ?>
                                                                </td>
                                                                <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $dt->desig; ?> </td>
                                                                <td style="padding-top:8px;padding-bottom:8px;">		
                                                                    <a href="add_static.php?type=delete&id=<?php echo $slno; ?>" class="btn btn-xs red" onClick="return confirm('Are you sure you want to delete this record')" <?php if ($mode == 'V') { ?> style="pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?>>Delete</a> &nbsp;	

                                                                    <a href="edit_team.php?type=edit&id=<?php echo $slno; ?>" class="btn btn-xs green" <?php if ($mode == 'V') { ?> style="pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?>>Edit</a> &nbsp;	
                                                                </td>													
                                                            </tr>													
                                                            <?php
                                                            $teamcount++;
                                                        }
                                                        ?>															
                                                    </table><br><br>	
                                                    <!--<button type="button" class="btn btn-s blue" data-toggle="modal" href="#responsive"><i class="fa fa-plus"></i> Add More Team Members </button>-->
                                                    <span><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#team_member').slideToggle();" value="Add Team Members" /></span>

                                                    <!-- ----------Start FORM------------------------------------->
                                                    <div class="portlet light bordered" id="team_member" style="display:none;">	
                                                        <div class="portlet-body form">
                                                            <!-- BEGIN FORM-->
                                                            <form  <?php if ($bucket) { ?>action="S3-createObject.php" <?php } else { ?> action="add_static.php"<?php } ?> method="post" enctype="multipart/form-data">			
                                                                <div class="row">						
                                                                    <div class="col-md-6">					
                                                                        <h4>Name</h4>						
                                                                        <p>							
                                                                            <input class="form-control" type="text" name="teamName" placeholder="Enter Your Name"> 	
                                                                        </p>												
                                                                    </div>										
                                                                    <div class="col-md-6">								
                                                                        <h4>Department</h4>								
                                                                        <p>										
                                                                            <input class="form-control" type="text" name="teamDepart" placeholder="Enter Your Department"> 	
                                                                        </p>													
                                                                    </div>												
                                                                    <div class="col-md-6">										
                                                                        <h4>Designation</h4>										
                                                                        <p>										
                                                                            <input class="form-control" type="text" name="teamDesig" placeholder="Enter Your Designation"> 		
                                                                        </p>													
                                                                    </div>										
                                                                    <div class="col-md-6">								
                                                                        <h4>About Team Member</h4>						
                                                                        <p>									
                                                                            <textarea class="form-control" name="teamDescrip" rows="6" placeholder="Enter Your Description"> </textarea>	
                                                                        </p>										
                                                                    </div>										
                                                                    <div class="col-md-6">							

                                                                        <h4>Photo Team Member</h4>						
                                                                        <p>								
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">		
                                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">		
                                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>	
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>	
                                                                            <div>														
                                                                                <span class="btn default btn-file">									
                                                                                    <span class="fileinput-new"> Select image </span>						
                                                                                    <span class="fileinput-exists"> Change </span>						
                                                                                    <input type="file" name="image_org"> </span>						
                                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>	
                                                                            </div>													
                                                                        </div>													
                                                                        </p>												
                                                                    </div>											
                                                                </div>	
                                                                <div class="modal-footer">							

                                                                    <input type="submit" name="add_team" class="btn red" value="Add Team Member" style="float: left; background:#9c27b0" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>>				
                                                                </div>	
                                                            </form>

                                                            <!-- END FORM-->

                                                            <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->

                                                        </div>												
                                                    </div>					
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                function team_mem(sln)
                {
                    var mydata = sln;
                    $.ajax({url: 'ajax_change_status.php',
                        data: {edit_team: mydata},
                        type: 'post',
                        success: function (output) {
                            $('#to_be_repl').html(output);
                        }
                    });
                }
            </script>               
            <div class="quick-nav-overlay"></div>

            <?php
            echo ajaxJsFooter();
            // echo admin_footer();
            ?>

    </body>
</html>
