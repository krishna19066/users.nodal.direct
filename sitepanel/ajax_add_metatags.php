<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyPhotoCont = new propertyData();
$ajaxclientCont = new staticPageData();
$propertyId = $_REQUEST['propID'];
$serviceID = $_REQUEST['serviceID'];
if($propertyId){
	$property_data = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
	$property_res = $property_data[0];
	$propertyName = $property_res->serviceName;
	$pageUrl = $property_res->propertyURL;
	$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
	$metaRes = count($metaTags);
	$mets_res = $metaTags[0]; 
}
if($serviceID){
	$property_data = $ajaxpropertyPhotoCont->GetServiceDataWithServiceID($customerId,$serviceID);
	$property_res = $property_data[0];
	$propertyName = $property_res->serviceName;
	$pageUrl = $property_res->serviceURL;
	$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
	$metaRes = count($metaTags);
	$mets_res = $metaTags[0]; 
}

?>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>      
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>   
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header();
            ?>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }

                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }

                tr:nth-child(odd){
                    background-color:#fff;
                }

                th {

                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                </style>

                <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> PROPERTY - <span style="color:#e44787;"><?php echo $propertyName; ?></span>

                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Manage Property Meta Tags</span>
                        </li>
                    </ul>
                    <?php
                    if (isset($_REQUEST['propID']) || isset($_REQUEST['serviceID']) ) {
                        $propertyId = $_REQUEST['propID'];

                        // $connectCloudinary = $ajaxpropertyPhotoCont->connectCloudinaryAccount($customerId);
                        ?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <span style="float:left; margin-top: -20px;"><a href="manage-property.php"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true" value="Manage Property" /></a></span>

                                            <span style="float:right; margin-top: -20px;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body form" id="add_photo" style="display:none;">
                        </div>	
                        <div class="row">
                            <div class="col-md-12">	
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-body">
                                        <div style="padding-left:35px; color:rgba(0, 0, 0, 0.7);">

                                            <!-- ----------Start FORM------------------------------------->
                                            <div class="portlet light bordered" id="add_meta">	
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Meta Title
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                                    <span class="help-block"> Provide Meta Title </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Meta Description</label>
                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php if ($metaRes > 0) { ?>
                                                                <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                                <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                                <input type="hidden" class="form-control" name="propID" value="<?php echo $propertyId; ?>" />
                                                                <input type="hidden" class="form-control" name="pagename" value="ajax_add_metatags" />
                                                                <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag"/>
                                                            <?php } else { ?>
                                                                <input type="hidden" class="form-control" name="propID" value="<?php echo $propertyId; ?>" />
                                                                <input type="hidden" class="form-control" name="pagename" value="ajax_add_metatags" />
                                                                <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                                <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"/>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>

                                            <!-- END FORM-->

                                            <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>  
            <?php
        }
        ?>      
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>