<?php
ob_start();
session_start();
error_reporting(E_ERROR | E_PARSE);
function check_session($sess_name,$REDIRECT_URL){
	if(!strlen($_SESSION[$sess_name])){
		header("Location:$REDIRECT_URL");
		exit;
	}
}

//include "theme_includes.php";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
	<head>
			
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta content="width=device-width, initial-scale=1" name="viewport" />
			<meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
			<meta content="" name="author" />
			<!-- BEGIN GLOBAL MANDATORY STYLES -->
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
			<!-- END GLOBAL MANDATORY STYLES -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN THEME GLOBAL STYLES -->
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
			<!-- END THEME GLOBAL STYLES -->
			<!-- BEGIN THEME LAYOUT STYLES -->
			<link href="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=SITE_ADMIN_URL;?>/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
			<!-- END THEME LAYOUT STYLES -->
			<link rel="shortcut icon" href="favicon.ico" /> </head>
			
	</head>
	

<?php
	function themeheader()
		{
?>
			
				<!-- BEGIN HEADER -->
				<div class="page-header navbar navbar-fixed-top">
					<!-- BEGIN HEADER INNER -->
					<div class="page-header-inner ">
						<!-- BEGIN LOGO -->
						<div class="page-logo">
							<a href="index.html">
								<img src="<?=SITE_ADMIN_URL;?>/assets/pages/img/logo-big.png" alt="logo" class="logo-default" /> </a>
							<div class="menu-toggler sidebar-toggler">
								<span></span>
							</div><br><br>
						</div>
						
						<!-- END LOGO -->
						<!-- BEGIN RESPONSIVE MENU TOGGLER -->
						<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
							<span></span>
						</a>
						<!-- END RESPONSIVE MENU TOGGLER -->
						<!-- BEGIN TOP NAVIGATION MENU -->
						<div class="top-menu">
							<ul class="nav navbar-nav pull-right">
								<!-- BEGIN NOTIFICATION DROPDOWN -->
								<!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
								<!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
								<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
								<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<i class="icon-bell"></i>
										<span class="badge badge-default"> 7 </span>
									</a>
									<ul class="dropdown-menu">
										<li class="external">
											<h3>
												<span class="bold">12 pending</span> notifications</h3>
											<a href="page_user_profile_1.html">view all</a>
										</li>
										<li>
											<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
												<li>
													<a href="javascript:;">
														<span class="time">just now</span>
														<span class="details">
															<span class="label label-sm label-icon label-success">
																<i class="fa fa-plus"></i>
															</span> New user registered. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">3 mins</span>
														<span class="details">
															<span class="label label-sm label-icon label-danger">
																<i class="fa fa-bolt"></i>
															</span> Server #12 overloaded. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">10 mins</span>
														<span class="details">
															<span class="label label-sm label-icon label-warning">
																<i class="fa fa-bell-o"></i>
															</span> Server #2 not responding. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">14 hrs</span>
														<span class="details">
															<span class="label label-sm label-icon label-info">
																<i class="fa fa-bullhorn"></i>
															</span> Application error. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">2 days</span>
														<span class="details">
															<span class="label label-sm label-icon label-danger">
																<i class="fa fa-bolt"></i>
															</span> Database overloaded 68%. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">3 days</span>
														<span class="details">
															<span class="label label-sm label-icon label-danger">
																<i class="fa fa-bolt"></i>
															</span> A user IP blocked. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">4 days</span>
														<span class="details">
															<span class="label label-sm label-icon label-warning">
																<i class="fa fa-bell-o"></i>
															</span> Storage Server #4 not responding dfdfdfd. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">5 days</span>
														<span class="details">
															<span class="label label-sm label-icon label-info">
																<i class="fa fa-bullhorn"></i>
															</span> System Error. </span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="time">9 days</span>
														<span class="details">
															<span class="label label-sm label-icon label-danger">
																<i class="fa fa-bolt"></i>
															</span> Storage server failed. </span>
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<!-- END NOTIFICATION DROPDOWN -->
								<!-- BEGIN INBOX DROPDOWN -->
								<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
								<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<i class="icon-envelope-open"></i>
										<span class="badge badge-default"> 4 </span>
									</a>
									<ul class="dropdown-menu">
										<li class="external">
											<h3>You have
												<span class="bold">7 New</span> Messages</h3>
											<a href="app_inbox.html">view all</a>
										</li>
										<li>
											<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
												<li>
													<a href="#">
														<span class="photo">
															<img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
														<span class="subject">
															<span class="from"> Lisa Wong </span>
															<span class="time">Just Now </span>
														</span>
														<span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="photo">
															<img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
														<span class="subject">
															<span class="from"> Richard Doe </span>
															<span class="time">16 mins </span>
														</span>
														<span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="photo">
															<img src="../assets/layouts/layout3/img/avatar1.jpg" class="img-circle" alt=""> </span>
														<span class="subject">
															<span class="from"> Bob Nilson </span>
															<span class="time">2 hrs </span>
														</span>
														<span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="photo">
															<img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
														<span class="subject">
															<span class="from"> Lisa Wong </span>
															<span class="time">40 mins </span>
														</span>
														<span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="photo">
															<img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
														<span class="subject">
															<span class="from"> Richard Doe </span>
															<span class="time">46 mins </span>
														</span>
														<span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<!-- END INBOX DROPDOWN -->
								<!-- BEGIN TODO DROPDOWN -->
								<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
								<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<i class="icon-calendar"></i>
										<span class="badge badge-default"> 3 </span>
									</a>
									<ul class="dropdown-menu extended tasks">
										<li class="external">
											<h3>You have
												<span class="bold">12 pending</span> tasks</h3>
											<a href="app_todo.html">view all</a>
										</li>
										<li>
											<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">New release v1.2 </span>
															<span class="percent">30%</span>
														</span>
														<span class="progress">
															<span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">40% Complete</span>
															</span>
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">Application deployment</span>
															<span class="percent">65%</span>
														</span>
														<span class="progress">
															<span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">65% Complete</span>
															</span>
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">Mobile app release</span>
															<span class="percent">98%</span>
														</span>
														<span class="progress">
															<span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">98% Complete</span>
															</span>
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">Database migration</span>
															<span class="percent">10%</span>
														</span>
														<span class="progress">
															<span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">10% Complete</span>
															</span>
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">Web server upgrade</span>
															<span class="percent">58%</span>
														</span>
														<span class="progress">
															<span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">58% Complete</span>
															</span>
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">Mobile development</span>
															<span class="percent">85%</span>
														</span>
														<span class="progress">
															<span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">85% Complete</span>
															</span>
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:;">
														<span class="task">
															<span class="desc">New UI release</span>
															<span class="percent">38%</span>
														</span>
														<span class="progress progress-striped">
															<span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
																<span class="sr-only">38% Complete</span>
															</span>
														</span>
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>
								<!-- END TODO DROPDOWN -->
								<!-- BEGIN USER LOGIN DROPDOWN -->
								<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
								<li class="dropdown dropdown-user">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<img alt="" class="img-circle" src="assets/layouts/layout/img/avatar3_small.jpg" />
										<span class="username username-hide-on-mobile"> Nick </span>
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-default">
										<li>
											<a href="page_user_profile_1.html">
												<i class="icon-user"></i> My Profile </a>
										</li>
										<li>
											<a href="app_calendar.html">
												<i class="icon-calendar"></i> My Calendar </a>
										</li>
										<li>
											<a href="app_inbox.html">
												<i class="icon-envelope-open"></i> My Inbox
												<span class="badge badge-danger"> 3 </span>
											</a>
										</li>
										<li>
											<a href="app_todo.html">
												<i class="icon-rocket"></i> My Tasks
												<span class="badge badge-success"> 7 </span>
											</a>
										</li>
										<li class="divider"> </li>
										<li>
											<a href="page_user_lock_1.html">
												<i class="icon-lock"></i> Lock Screen </a>
										</li>
										<li>
											<a href="page_user_login_1.html">
												<i class="icon-key"></i> Log Out </a>
										</li>
									</ul>
								</li>
								<!-- END USER LOGIN DROPDOWN -->
								<!-- BEGIN QUICK SIDEBAR TOGGLER -->
								<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
								<li class="dropdown dropdown-quick-sidebar-toggler">
									<a href="javascript:;" class="dropdown-toggle">
										<i class="icon-logout"></i>
									</a>
								</li>
								<!-- END QUICK SIDEBAR TOGGLER -->
							</ul>
						</div>
						<!-- END TOP NAVIGATION MENU -->
					</div>
					<!-- END HEADER INNER -->
				</div>
				<!-- END HEADER -->
				<!-- BEGIN HEADER & CONTENT DIVIDER -->
				<div class="clearfix"> </div>
				<!-- END HEADER & CONTENT DIVIDER -->
<?php
	}
?>

<?php
function admin_header($sitetitle=""){ //admin_full_header..
	?>
	
	
		<?php
			if(!empty($_SESSION['MyCpanel']))
				{
		?>
					<!--<a href="index.php?id1=Logout"><img alt="Administrator Logout!" hspace=10 src="images/log_out.gif" border=0></a>-->
		<?php
				}
	    ?>
		
		<?php
			if(isset($_SESSION['MyCpanel']))
				{
		?>
		
						<!-- BEGIN SIDEBAR -->
						<div class="page-sidebar-wrapper">
							<!-- BEGIN SIDEBAR -->
							
							<div class="page-sidebar navbar-collapse collapse">
								<!-- BEGIN SIDEBAR MENU -->
								
								<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
									<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
									<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
									<li class="sidebar-toggler-wrapper hide">
										<div class="sidebar-toggler">
											<span></span>
										</div>
									</li>
									<!-- END SIDEBAR TOGGLER BUTTON -->
									<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
									<li class="sidebar-search-wrapper">
										<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
										
										<form class="sidebar-search  " action="page_general_search_3.html" method="POST" style="background:white;">
											<a href="javascript:;" class="remove">
												<i class="icon-close"></i>
											</a>
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Search..." style="border:1px solid white;background:white;padding-left:10px;height:25px;">
												<span class="input-group-btn">
													<a href="javascript:;" class="btn submit">
														<i class="icon-magnifier"></i>
													</a>
												</span>
											</div>
										</form>
										<!-- END RESPONSIVE QUICK SEARCH FORM -->
									</li>
									
									<?php
										include "../include/arrays.inc.php";
										$id = $_SESSION['MyAdminUserID'];
										//$con = mysqli_connect("localhost","root","","tes") or die("no connection");
										$sel = "select * from admin_details where user_id='$id'";
										$query = mysql_query($sel);
										while($row = mysql_fetch_array($query))
											{
												//echo $value."<br>";
												
												$type = $row['user_type'];
												if($type=="admin")
													{
									?>				
									
														<li class="nav-item start active open">
															<a href="javascript:;" class="nav-link nav-toggle">
															<i class="icon-home"></i>
															<span class="title">DASHBOARD</span>
															<span class="selected"></span>
															<span class="arrow open"></span>
															
															<ul class="sub-menu">
																<li class="nav-item start active open">
																	<a href="<?=SITE_ADMIN_URL;?>/all-members.php" class="nav-link ">
																	<i class="icon-bar-chart"></i>
																	<span class="title">All Members</span>
																	<span class="selected"></span>
																	</a>
																</li>
																		
																<li class="nav-item start ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-inquiry.php?inqid=RequestaCall" class="nav-link ">
																		<i class="icon-bulb"></i>
																		<span class="title">Customer - Call me back</span>
																		<span class="badge badge-success"></span>
																	</a>
																</li>
																
																<li class="nav-item start ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-inquiry.php?inqid=instantBooking" class="nav-link ">
																		<i class="icon-graph"></i>
																		<span class="title">Instant booking Inquiry</span>
																		<span class="badge badge-danger"></span>
																	</a>
																</li>
																
																<li class="nav-item start ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-inquiry.php?inqid=contactus" class="nav-link ">
																		<i class="icon-graph"></i>
																		<span class="title">Customer - Inquiry form</span>
																		<span class="badge badge-danger"></span>
																	</a>
																</li>
																
																<li class="nav-item start ">
																	<a href="<?=SITE_ADMIN_URL;?>/email-subscription.php" class="nav-link ">
																		<i class="icon-graph"></i>
																		<span class="title">Email subscription form</span>
																		<span class="badge badge-danger"></span>
																	</a>
																</li>
															</ul>
														</li>
														<li class="heading">
															<h3 class="uppercase">Setup & Control</h3>
														</li>
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-diamond"></i>
																<span class="title">Manage Property Pages</span>
																<span class="arrow"></span>
															</a>
															
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-property.php" class="nav-link ">
																		<span class="title">Manage Property</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-new-property.php" class="nav-link ">
																		<span class="title">Add New Property</span>
																	</a>
																</li>
															</ul>
														</li>
														
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-puzzle"></i>
																<span class="title">Home Page Content</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-home-page.php" class="nav-link ">
																		<span class="title">Edit Home Page</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-home-page-mobile.php" class="nav-link ">
																		<span class="title">Edit Home Page(Mobile)</span>
																	</a>
																</li>
															</ul>
														</li>  
													   
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-settings"></i>
																<span class="title">Manage Static Pages</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-book-now.php" class="nav-link ">
																		<span class="title">Edit Book Now page</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-about-us.php" class="nav-link ">
																		<span class="title">Edit About us</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-why-the-perch.php" class="nav-link ">
																		<span class="title">Edit Why the Perch</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-food.php" class="nav-link ">
																		<span class="title">Edit Food</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-our-clients.php" class="nav-link ">
																		<span class="title">Edit our Clients</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-corporate-clients.php" class="nav-link ">
																		<span class="title">Edit Corporate Clients</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-japanese-guests.php" class="nav-link ">
																		<span class="title">Edit Japanese Guests</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-medical-tourists.php" class="nav-link ">
																		<span class="title">Edit Medical Tourists</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-policies.php" class="nav-link ">
																		<span class="title">Edit Policies Page</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-contact-us.php" class="nav-link ">
																		<span class="title">Edit Contact us</span>
																	</a>
																</li>
															</ul>
														</li>
														
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-bulb"></i>
																<span class="title">Manage Slider</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-home-page-slider.php?type=home" class="nav-link ">
																		<span class="title">Manage Home Page Slider</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-slider.php?type=home" class="nav-link ">
																		<span class="title">Add imaage in Home Page Slider</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-home-page-slider.php?type=mobile" class="nav-link ">
																		<span class="title">Mobile - Manage Home Page Slider</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-slider.php?type=mobile" class="nav-link ">
																		<span class="title">Mobile - Add imaage in Home Page Slider</span>
																	</a>
																</li>
															</ul>
														</li>  
															
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-briefcase"></i>
																<span class="title">Manage City / Sub City</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-city.php" class="nav-link ">
																		<span class="title">Manage Property City</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-city.php" class="nav-link ">
																		<span class="title">Add City</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-sub-city.php" class="nav-link ">
																		<span class="title">Manage Property Sub City</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-sub-city.php" class="nav-link ">
																		<span class="title">Add Sub City</span>
																	</a>
																</li>
															</ul>
														</li>   
														
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-bar-chart"></i>
																<span class="title">Manage SEO</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/managae-meta-tags.php" class="nav-link ">
																		<span class="title">Manage Meta Tags</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-meta-tags.php" class="nav-link ">
																		<span class="title">Add Meta Tags</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-analytic-code.php" class="nav-link ">
																		<span class="title">Manage Analytic Code</span>
																	</a>
																</li>
															</ul>
														</li>    
													   
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-bar-chart"></i>
																<span class="title">Manage Data Settings</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-property-type.php" class="nav-link ">
																		<span class="title">Manage Property Type</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-room-type.php" class="nav-link ">
																		<span class="title">Manage Room Type</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-property-amenties.php" class="nav-link ">
																		<span class="title">Manage Property Amenties</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-property-type.php" class="nav-link ">
																		<span class="title">Add Property Type</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-property-room.php" class="nav-link ">
																		<span class="title">Add Room Type</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-property-amenties.php" class="nav-link ">
																		<span class="title">Add Property Amenties</span>
																	</a>
																</li>
															</ul>
														</li>    		
														
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-wallet"></i>
																<span class="title">Manage Countries</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_flags.php" class="nav-link ">
																		<span class="title">Manage Flags</span>
																	</a>
																</li>
															</ul>
														</li>
																
													   <li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-pointer"></i>
																<span class="title">Blog Management</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-author.php" class="nav-link ">
																		<span class="title">Add Author</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-author.php" class="nav-link ">
																		<span class="title">Manage Author</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-category.php" class="nav-link ">
																		<span class="title">Add Category</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-categories.php" class="nav-link ">
																		<span class="title">Manage Category</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-post.php" class="nav-link ">
																		<span class="title">Add Blog</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-posts.php" class="nav-link ">
																		<span class="title">Manage Blog</span>
																	</a>
																</li>
															</ul>
														</li>    		
																
													   <li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-layers"></i>
																<span class="title">Manage All Bookings</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add_new_booking.php" class="nav-link ">
																		<span class="title">Add New Booking</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_booking.php" class="nav-link ">
																		<span class="title">Manage Customers</span>
																	</a>
																</li>
															</ul>
														</li>
														 
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-feed"></i>
																<span class="title">User Section</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-user.php" class="nav-link ">
																		<span class="title">Add user</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-users.php" class="nav-link ">
																		<span class="title">Manage Users</span>
																	</a>
																</li>
															</ul>
														</li>			
														



														
														<li class="heading">
															<h3 class="uppercase">Content & Maketing</h3>
														</li>
																
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-paper-plane"></i>
																<span class="title">Property Reviews</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_reviews.php" class="nav-link ">
																		<span class="title">Manage Reviews</span>
																	</a>
																</li>
															</ul>
														</li>				
																
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class=" icon-wrench"></i>
																<span class="title">Discounts & Offers</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add-new-discount.php" class="nav-link ">
																		<span class="title">Add New Discount</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage-discounts.php" class="nav-link ">
																		<span class="title">Manage Discount</span>
																	</a>
																</li>
															</ul>
														</li>				
													
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-basket"></i>
																<span class="title">Email Templates</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add_mail_template.php" class="nav-link ">
																		<span class="title">Add New Template</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_mail_template.php" class="nav-link ">
																		<span class="title">Manage Templates</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/map_mail_template.php" class="nav-link ">
																		<span class="title">Assign Templates</span>
																	</a>
																</li>
															</ul>
														</li>			
															



															
														<li class="heading">
															<h3 class="uppercase">Admin</h3>
														</li>			
													 
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-docs"></i>
																<span class="title">Manage Header and Footer</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_heder.php" class="nav-link ">
																		<span class="title">Manage Header</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_footer.php" class="nav-link ">
																		<span class="title">Manage Footer</span>
																	</a>
																</li>
															</ul>
														</li>	
													
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-user"></i>
																<span class="title">Manage Forms</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/add_new_form.php" class="nav-link ">
																		<span class="title">Add Forms</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/manage_form.php" class="nav-link ">
																		<span class="title">Manage Forms</span>
																	</a>
																</li>
															</ul>
														</li>	
																
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-social-dribbble"></i>
																<span class="title">Google Analytics</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/analytics.php" class="nav-link ">
																		<span class="title">Google Analytics</span>
																	</a>
																</li>
															</ul>
														</li>			
															
														<li class="nav-item  ">
															<a href="javascript:;" class="nav-link nav-toggle">
																<i class="icon-settings"></i>
																<span class="title">Admin Section</span>
																<span class="arrow"></span>
															</a>													
																				
															<ul class="sub-menu">
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/admin_change_pass.php" class="nav-link ">
																		<span class="title">Change Username & Password</span>
																	</a>
																</li>
																<li class="nav-item  ">
																	<a href="<?=SITE_ADMIN_URL;?>/index.php?id1=Logout" class="nav-link ">
																		<span class="title">Logout</span>
																	</a>
																</li>
															</ul>
														</li>
										<?php
											
													}
												else
													{
										?>
									
										<?php
														$cont = $row['section'];
														$a = explode("^",$cont);
														//echo $a[1];
														foreach($a as $value)
															{
																$t = $admin_section[$value];
																//echo $t."<br>";
																if($t=="Manage Booking Panel")
																	{
										?>		  
																		<li class="nav-item start active open">
																			<a href="javascript:;" class="nav-link nav-toggle">
																			<i class="icon-home"></i>
																			<span class="title">Manage Booking Panel</span>
																			<span class="selected"></span>
																			<span class="arrow open"></span>
																			
																			<ul class="sub-menu">
																				<li class="nav-item start active open">
																					<a href="<?=SITE_ADMIN_URL;?>/all-members.php" class="nav-link ">
																					<i class="icon-bar-chart"></i>
																					<span class="title">All Members</span>
																					<span class="selected"></span>
																					</a>
																				</li>
																						
																				<li class="nav-item start ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-inquiry.php?inqid=RequestaCall" class="nav-link ">
																						<i class="icon-bulb"></i>
																						<span class="title">Customer - Call me back</span>
																						<span class="badge badge-success">1</span>
																					</a>
																				</li>
																				
																				<li class="nav-item start ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-inquiry.php?inqid=instantBooking" class="nav-link ">
																						<i class="icon-graph"></i>
																						<span class="title">Instant booking Inquiry</span>
																						<span class="badge badge-danger">5</span>
																					</a>
																				</li>
																				
																				<li class="nav-item start ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-inquiry.php?inqid=contactus" class="nav-link ">
																						<i class="icon-graph"></i>
																						<span class="title">Customer - Inquiry form</span>
																						<span class="badge badge-danger">5</span>
																					</a>
																				</li>
																				
																				<li class="nav-item start ">
																					<a href="<?=SITE_ADMIN_URL;?>/email-subscription.php" class="nav-link ">
																						<i class="icon-graph"></i>
																						<span class="title">Email subscription form</span>
																						<span class="badge badge-danger">5</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php
																	}
																if($t=="Manage Property Pages")
																	{
									?>
																		<li class="heading">
																			<h3 class="uppercase">Setup & Control</h3>
																		</li>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-diamond"></i>
																				<span class="title">Manage Property Pages</span>
																				<span class="arrow"></span>
																			</a>
																			
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-property.php" class="nav-link ">
																						<span class="title">Manage Property</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-new-property.php" class="nav-link ">
																						<span class="title">Add New Property</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php	
																	}
																if($t=="Manage Home Page Content")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-puzzle"></i>
																				<span class="title">Manage Home Page Content</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-home-page.php" class="nav-link ">
																						<span class="title">Edit Home Page</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-home-page-mobile.php" class="nav-link ">
																						<span class="title">Edit Home Page</span>
																					</a>
																				</li>
																			</ul>
																		</li>  
									<?php
																	}
																if($t=="Manage Static Pages")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-settings"></i>
																				<span class="title">Manage Static Pages</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-book-now.php" class="nav-link ">
																						<span class="title">Edit Book Now page</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-about-us.php" class="nav-link ">
																						<span class="title">Edit About us</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-why-the-perch.php" class="nav-link ">
																						<span class="title">Edit Why the Perch</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-food.php" class="nav-link ">
																						<span class="title">Edit Food</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-our-clients.php" class="nav-link ">
																						<span class="title">Edit our Clients</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-corporate-clients.php" class="nav-link ">
																						<span class="title">Edit Corporate Clients</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-japanese-guests.php" class="nav-link ">
																						<span class="title">Edit Japanese Guests</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-medical-tourists.php" class="nav-link ">
																						<span class="title">Edit Medical Tourists</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-policies.php" class="nav-link ">
																						<span class="title">Edit Policies Page</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-contact-us.php" class="nav-link ">
																						<span class="title">Edit Contact us</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php		
																	}
																if($t=="Manage Slider")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-bulb"></i>
																				<span class="title">Manage Slider</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-home-page-slider.php?type=home" class="nav-link ">
																						<span class="title">Manage Home Page Slider</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-slider.php?type=home" class="nav-link ">
																						<span class="title">Add imaage in Home Page Slider</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-home-page-slider.php?type=mobile" class="nav-link ">
																						<span class="title">Mobile - Manage Home Page Slider</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-slider.php?type=mobile" class="nav-link ">
																						<span class="title">Mobile - Add imaage in Home Page Slider</span>
																					</a>
																				</li>
																			</ul>
																		</li>  
									<?php		
																	}
																if($t=="Manage City / Sub City")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-briefcase"></i>
																				<span class="title">Manage City / Sub City</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-city.php" class="nav-link ">
																						<span class="title">Manage Property City</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-city.php" class="nav-link ">
																						<span class="title">Add City</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-sub-city.php" class="nav-link ">
																						<span class="title">Manage Property Sub City</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-sub-city.php" class="nav-link ">
																						<span class="title">Add Sub City</span>
																					</a>
																				</li>
																			</ul>
																		</li> 
									<?php					
																	}
																if($t=="Manage SEO")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-bar-chart"></i>
																				<span class="title">Manage SEO</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/managae-meta-tags.php" class="nav-link ">
																						<span class="title">Manage Meta Tags</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-meta-tags.php" class="nav-link ">
																						<span class="title">Add Meta Tags</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-analytic-code.php" class="nav-link ">
																						<span class="title">Manage Analytic Code</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php
																	}
																if($t=="Manage Data Settings")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-bar-chart"></i>
																				<span class="title">Manage Data Settings</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-property-type.php" class="nav-link ">
																						<span class="title">Manage Property Type</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-room-type.php" class="nav-link ">
																						<span class="title">Manage Room Type</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-property-amenties.php" class="nav-link ">
																						<span class="title">Manage Property Amenties</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-property-type.php" class="nav-link ">
																						<span class="title">Add Property Type</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-property-room.php" class="nav-link ">
																						<span class="title">Add Room Type</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-property-amenties.php" class="nav-link ">
																						<span class="title">Add Property Amenties</span>
																					</a>
																				</li>
																			</ul>
																		</li>    
									<?php			
																	}
																if($t=="Manage Email Templates")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-basket"></i>
																				<span class="title">Email Templates</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add_mail_template.php" class="nav-link ">
																						<span class="title">Add New Template</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage_mail_template.php" class="nav-link ">
																						<span class="title">Manage Templates</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/map_mail_template.php" class="nav-link ">
																						<span class="title">Assign Templates</span>
																					</a>
																				</li>
																			</ul>
																		</li>		
									<?php	
																	}
																if($t=="Manage Reviews")
																	{
									?>
																		<li class="heading">
																			<h3 class="uppercase">Content & Maketing</h3>
																		</li>
																				
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-paper-plane"></i>
																				<span class="title">Property Reviews</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage_reviews.php" class="nav-link ">
																						<span class="title">Manage Reviews</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php	
																	}
																if($t=="Manage Flags")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-wallet"></i>
																				<span class="title">Manage Countries</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage_flags.php" class="nav-link ">
																						<span class="title">Manage Flags</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php	
																	}
																if($t=="Blog Management")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-pointer"></i>
																				<span class="title">Blog Management</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-author.php" class="nav-link ">
																						<span class="title">Add Author</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-author.php" class="nav-link ">
																						<span class="title">Manage Author</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-category.php" class="nav-link ">
																						<span class="title">Add Category</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-categories.php" class="nav-link ">
																						<span class="title">Manage Category</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add-post.php" class="nav-link ">
																						<span class="title">Add Blog</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage-posts.php" class="nav-link ">
																						<span class="title">Manage Blog</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php	
																	}
																if($t=="Manage Forms")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-user"></i>
																				<span class="title">Manage Forms</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add_new_form.php" class="nav-link ">
																						<span class="title">Add Forms</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage_form.php" class="nav-link ">
																						<span class="title">Manage Forms</span>
																					</a>
																				</li>
																			</ul>
																		</li>	
									<?php	
																	}
																if($t=="Manage All Bookings")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-layers"></i>
																				<span class="title">Manage All Bookings</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add_new_booking.php" class="nav-link ">
																						<span class="title">Add New Booking</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage_booking.php" class="nav-link ">
																						<span class="title">Manage Customers</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php		
																	}
																if($t=="Manage Header and Footer")
																	{
									?>
																		<li class="nav-item  ">
																			<a href="javascript:;" class="nav-link nav-toggle">
																				<i class="icon-docs"></i>
																				<span class="title">Manage Header and Footer</span>
																				<span class="arrow"></span>
																			</a>													
																								
																			<ul class="sub-menu">
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/add_new_form.php" class="nav-link ">
																						<span class="title">Manage Header</span>
																					</a>
																				</li>
																				<li class="nav-item  ">
																					<a href="<?=SITE_ADMIN_URL;?>/manage_footer.php" class="nav-link ">
																						<span class="title">Manage Footer</span>
																					</a>
																				</li>
																			</ul>
																		</li>
									<?php	
																	}
															}
													}
											}
									?>
								</ul>
							</div>
						</div>
					
<?php
				}
		}
?>
						
						
						
						
						
						
						
						
						
						
						
						
						
<!--------------------------------------------------------------------------------------------==============================================--------------------------------------------------------------------------------------->


<?php
function admin_footer(){//footer for control panel...
  ?>
 </td>
  </tr>
	</table>
	<div id="footer">
	 <div class="copyright"> 2014 © Metronic. Admin Dashboard Template. <?=WEBSITE_NAME;?>&nbsp;All Rights Reserved. <br /></div> 
	</div>
	<!-- BEGIN CORE PLUGINS -->
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
		<script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=SITE_ADMIN_URL;?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?=SITE_ADMIN_URL;?>/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/pages/scripts/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
	</body></html>
	<?php
}


function pop_header($title){
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	 <title><?=$title;?></title>
	 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	 <link href="<?=SITE_ADMIN_URL;?>/admin.css" rel="stylesheet" type="text/css" />
	 <script language="javascript" src="admin_js.js"></script>
	</head>
	<body>
	<p bgcolor="#f1f1f1" align="center"><br><a href="javascript:void(0);" OnClick="javascript:window.close();">Close Window</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" OnClick="javascript:window.print();">Print Details</a></p>
	<?php
}


function pop_footer(){
	?>
	<br /><br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	 <tr>
	  <td align="center" bgcolor="#f1f1f1"><strong>Copyright <?=date('Y');?> &copy; <?=WEBSITE_NAME;?></strong></td>
	 </tr>
	</table>
	<p bgcolor="#f1f1f1" align="center"><br><a href="javascript:void(0);" OnClick="javascript:window.close();">Close Window</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" OnClick="javascript:window.print();">Print Details</a></p>
	</body>
	</html>
	<?php
}


function admin_change_pass(){
	$res=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");
	?>
	<script type="text/javascript">
	function validate_form() {
		var frm=document.chgPwd;
		if (frm.username.value==0) {
			alert("Please enter username");
			frm.username.focus();
			return false;
		}
		if (frm.old_password.value==0) {
			alert("Please enter your current password");
			frm.old_password.focus();
			return false;
		}
		if (frm.password.value==0) {
			alert("Please enter your new password");
			frm.password.focus();
			return false;
		}
		if (frm.repassword.value==0) {
			alert("Please confirm your new password");
			frm.repassword.focus();
			return false;
		}
		if (frm.password.value != frm.repassword.value) {
			alert("Password and Confirm Password mismatched");
			frm.repassword.focus();
			return false;
		}
	}
	</script>
	<form action="" method="post" name="chgPwd" id="chgPwd" onsubmit="return validate_form();">
	<table width="550" border="0" align="center" cellpadding="0" cellspacing="0" class="tableForm"> 
	 <tr>
	  <td class="tdLabel" colspan="2" align="center">
	   <?php
	   if($_SESSION[session_message]!=''){
		   echo '<font color="#C05813">'.print_message().'</font><br>';
	   }
	   ?>
	  </td>
	 </tr>
	 <tr>
	  <td width="120" class="tdLabel">Username:<font color="red">*</font></td>
	  <td><input type="text" name="username" value="<?=$res[user_id];?>" class="textfield"></td>
	 </tr>
	 <tr>
	  <td width="120" class="tdLabel">Current Password:<font color="red">*</font></td>
	  <td><input type="password" name="old_password" class="textfield" /></td>
	 </tr>
	 <tr>
	  <td class="tdLabel">New Password:<font color="red">*</font></td>
	  <td><input type="password" name="password" class="textfield"></td>
	 </tr>
	 <tr>
	  <td class="tdLabel">Confirm Password:<font color="red">*</font></td>
	  <td><input type="password" name="repassword" class="textfield"></td>
	 </tr>
	 <tr>
	  <td class="label">&nbsp;</td>
	  <td>
	   <input type="image" name="imageField" src="images/buttons/submit.gif" />
	   <input type="hidden" name="action" value="Update">
	  </td>
	 </tr> 
	</table> 
	</form>
	<?php
}


function login_frm(){
	?>
	
	<div class="content"><div class="logo">
            <a href="welcome.php">
                <img src="<?=SITE_ADMIN_URL;?>/assets/pages/img/logo-big.png" alt="" /> </a>
                <h4><?=WEBSITE_NAME;?></h4>
        </div> <form class="login-form" action="index.php" method="post">
                <h3 class="form-title font-green">Sign In</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                 <script type="text/javascript">
	               function validate_user(){
		               var frm=document.loginFrm;
		               if(frm.login_id.value==""){
			               alert("Enter Username!!");
			               frm.login_id.focus();
			               return false;
		               }
		               if(frm.password.value==""){
			               alert("Enter Password!!");
			               frm.password.focus();
			               return false;
		               }
	               }
	               </script>
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" name="login_id"  autocomplete="off" placeholder="Username"  id="login_id" value="<?=$adm_login_id?>" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" value="<?=$_POST['password']?>" /> </div>
                     
	                  <input name="user_type" type="radio" value="admin" checked="checked" />Administrator &nbsp;
	                  <input name="user_type" type="radio" value="staff" />Admin Staff
	                
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <input type="hidden" name="id1" value="login" />
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Remember
                        <span></span>
                    </label>
                   
                </div>
               
            </form>
            </div>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
	<?php
}


function welcome()
	{
		$q=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");	
?>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Dashboard</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp;
					<span class="thin uppercase hidden-xs"></span>&nbsp;
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"><font style="padding-left:20px;"> Admin Dashboard
			<small>statistics, charts, recent events and reports</small>
		</h1>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<!-- BEGIN DASHBOARD STATS 1-->
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-v2 blue" href="#">
					<div class="visual">
						<i class="fa fa-comments"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="1349">0</span>
						</div>
						<div class="desc"> New Feedbacks </div>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-v2 red" href="#">
					<div class="visual">
						<i class="fa fa-bar-chart-o"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="12,5">0</span>M$ </div>
						<div class="desc"> Total Profit </div>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-v2 green" href="#">
					<div class="visual">
						<i class="fa fa-shopping-cart"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="549">0</span>
						</div>
						<div class="desc"> New Orders </div>
					</div>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-v2 purple" href="#">
					<div class="visual">
						<i class="fa fa-globe"></i>
					</div>
					<div class="details">
						<div class="number"> +
							<span data-counter="counterup" data-value="89"></span>% </div>
						<div class="desc"> Brand Popularity </div>
					</div>
				</a>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- END DASHBOARD STATS 1-->
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bar-chart font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Site Visits</span>
							<span class="caption-helper">weekly stats...</span>
						</div>
						<div class="actions">
							<div class="btn-group btn-group-devided" data-toggle="buttons">
								<label class="btn red btn-outline btn-circle btn-sm active">
									<input type="radio" name="options" class="toggle" id="option1">New</label>
								<label class="btn red btn-outline btn-circle btn-sm">
									<input type="radio" name="options" class="toggle" id="option2">Returning</label>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div id="site_statistics_loading">
							<img src="../assets/global/img/loading.gif" alt="loading" /> </div>
						<div id="site_statistics_content" class="display-none">
							<div id="site_statistics" class="chart"> </div>
						</div>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-red-sunglo hide"></i>
							<span class="caption-subject font-dark bold uppercase">Revenue</span>
							<span class="caption-helper">monthly stats...</span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter Range
									<span class="fa fa-angle-down"> </span>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> Q1 2014
											<span class="label label-sm label-default"> past </span>
										</a>
									</li>
									<li>
										<a href="javascript:;"> Q2 2014
											<span class="label label-sm label-default"> past </span>
										</a>
									</li>
									<li class="active">
										<a href="javascript:;"> Q3 2014
											<span class="label label-sm label-success"> current </span>
										</a>
									</li>
									<li>
										<a href="javascript:;"> Q4 2014
											<span class="label label-sm label-warning"> upcoming </span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div id="site_activities_loading">
							<img src="../assets/global/img/loading.gif" alt="loading" /> </div>
						<div id="site_activities_content" class="display-none">
							<div id="site_activities" style="height: 228px;"> </div>
						</div>
						<div style="margin: 20px 0 10px 30px">
							<div class="row">
								<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
									<span class="label label-sm label-success"> Revenue: </span>
									<h3>$13,234</h3>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
									<span class="label label-sm label-info"> Tax: </span>
									<h3>$134,900</h3>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
									<span class="label label-sm label-danger"> Shipment: </span>
									<h3>$1,134</h3>
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6 text-stat">
									<span class="label label-sm label-warning"> Orders: </span>
									<h3>235090</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title tabbable-line">
						<div class="caption">
							<i class="icon-bubbles font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Comments</span>
						</div>
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#portlet_comments_1" data-toggle="tab"> Pending </a>
							</li>
							<li>
								<a href="#portlet_comments_2" data-toggle="tab"> Approved </a>
							</li>
						</ul>
					</div>
					<div class="portlet-body">
						<div class="tab-content">
							<div class="tab-pane active" id="portlet_comments_1">
								<!-- BEGIN: Comments -->
								<div class="mt-comments">
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar1.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Michael Baker</span>
												<span class="mt-comment-date">26 Feb, 10:30AM</span>
											</div>
											<div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-pending">Pending</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar6.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Larisa Maskalyova</span>
												<span class="mt-comment-date">12 Feb, 08:30AM</span>
											</div>
											<div class="mt-comment-text"> It is a long established fact that a reader will be distracted. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-rejected">Rejected</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar8.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Natasha Kim</span>
												<span class="mt-comment-date">19 Dec,09:50 AM</span>
											</div>
											<div class="mt-comment-text"> The generated Lorem or non-characteristic Ipsum is therefore or non-characteristic. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-pending">Pending</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar4.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Sebastian Davidson</span>
												<span class="mt-comment-date">10 Dec, 09:20 AM</span>
											</div>
											<div class="mt-comment-text"> The standard chunk of Lorem or non-characteristic Ipsum used since the 1500s or non-characteristic. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-rejected">Rejected</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- END: Comments -->
							</div>
							<div class="tab-pane" id="portlet_comments_2">
								<!-- BEGIN: Comments -->
								<div class="mt-comments">
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar4.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Michael Baker</span>
												<span class="mt-comment-date">26 Feb, 10:30AM</span>
											</div>
											<div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-approved">Approved</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar8.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Larisa Maskalyova</span>
												<span class="mt-comment-date">12 Feb, 08:30AM</span>
											</div>
											<div class="mt-comment-text"> It is a long established fact that a reader will be distracted by. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-approved">Approved</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar6.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Natasha Kim</span>
												<span class="mt-comment-date">19 Dec,09:50 AM</span>
											</div>
											<div class="mt-comment-text"> The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-approved">Approved</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="mt-comment">
										<div class="mt-comment-img">
											<img src="../assets/pages/media/users/avatar1.jpg" /> </div>
										<div class="mt-comment-body">
											<div class="mt-comment-info">
												<span class="mt-comment-author">Sebastian Davidson</span>
												<span class="mt-comment-date">10 Dec, 09:20 AM</span>
											</div>
											<div class="mt-comment-text"> The standard chunk of Lorem Ipsum used since the 1500s </div>
											<div class="mt-comment-details">
												<span class="mt-comment-status mt-comment-status-approved">Approved</span>
												<ul class="mt-comment-actions">
													<li>
														<a href="#">Quick Edit</a>
													</li>
													<li>
														<a href="#">View</a>
													</li>
													<li>
														<a href="#">Delete</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- END: Comments -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title tabbable-line">
						<div class="caption">
							<i class=" icon-social-twitter font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Quick Actions</span>
						</div>
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_actions_pending" data-toggle="tab"> Pending </a>
							</li>
							<li>
								<a href="#tab_actions_completed" data-toggle="tab"> Completed </a>
							</li>
						</ul>
					</div>
					<div class="portlet-body">
						<div class="tab-content">
							<div class="tab-pane active" id="tab_actions_pending">
								<!-- BEGIN: Actions -->
								<div class="mt-actions">
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar10.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-magnet"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Natasha Kim</span>
														<p class="mt-action-desc">Dummy text of the printing</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-green"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar3.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class=" icon-bubbles"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Gavin Bond</span>
														<p class="mt-action-desc">pending for approval</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-red"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar2.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-call-in"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Diana Berri</span>
														<p class="mt-action-desc">Lorem Ipsum is simply dummy text</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-green"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar7.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class=" icon-bell"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">John Clark</span>
														<p class="mt-action-desc">Text of the printing and typesetting industry</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-red"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar8.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-magnet"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Donna Clarkson </span>
														<p class="mt-action-desc">Simply dummy text of the printing</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-green"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar9.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-magnet"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Tom Larson</span>
														<p class="mt-action-desc">Lorem Ipsum is simply dummy text</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-green"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- END: Actions -->
							</div>
							<div class="tab-pane" id="tab_actions_completed">
								<!-- BEGIN:Completed-->
								<div class="mt-actions">
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar1.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-action-redo"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Frank Cameron</span>
														<p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-red"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar8.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-cup"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Ella Davidson </span>
														<p class="mt-action-desc">Text of the printing and typesetting industry</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-green"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar5.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class=" icon-graduation"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Jason Dickens </span>
														<p class="mt-action-desc">Dummy text of the printing and typesetting industry</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-red"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="mt-action">
										<div class="mt-action-img">
											<img src="../assets/pages/media/users/avatar2.jpg" /> </div>
										<div class="mt-action-body">
											<div class="mt-action-row">
												<div class="mt-action-info ">
													<div class="mt-action-icon ">
														<i class="icon-badge"></i>
													</div>
													<div class="mt-action-details ">
														<span class="mt-action-author">Jan Kim</span>
														<p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
													</div>
												</div>
												<div class="mt-action-datetime ">
													<span class="mt-action-date">3 jun</span>
													<span class="mt-action-dot bg-green"></span>
													<span class="mt=action-time">9:30-13:00</span>
												</div>
												<div class="mt-action-buttons ">
													<div class="btn-group btn-group-circle">
														<button type="button" class="btn btn-outline green btn-sm">Appove</button>
														<button type="button" class="btn btn-outline red btn-sm">Reject</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- END: Completed -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-directions font-green hide"></i>
							<span class="caption-subject bold font-dark uppercase "> Activities</span>
							<span class="caption-helper">Horizontal Timeline</span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> Action 1</a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;">Action 2</a>
									</li>
									<li>
										<a href="javascript:;">Action 3</a>
									</li>
									<li>
										<a href="javascript:;">Action 4</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="cd-horizontal-timeline mt-timeline-horizontal" data-spacing="60">
							<div class="timeline">
								<div class="events-wrapper">
									<div class="events">
										<ol>
											<li>
												<a href="#0" data-date="16/01/2014" class="border-after-red bg-after-red selected">16 Jan</a>
											</li>
											<li>
												<a href="#0" data-date="28/02/2014" class="border-after-red bg-after-red">28 Feb</a>
											</li>
											<li>
												<a href="#0" data-date="20/04/2014" class="border-after-red bg-after-red">20 Mar</a>
											</li>
											<li>
												<a href="#0" data-date="20/05/2014" class="border-after-red bg-after-red">20 May</a>
											</li>
											<li>
												<a href="#0" data-date="09/07/2014" class="border-after-red bg-after-red">09 Jul</a>
											</li>
											<li>
												<a href="#0" data-date="30/08/2014" class="border-after-red bg-after-red">30 Aug</a>
											</li>
											<li>
												<a href="#0" data-date="15/09/2014" class="border-after-red bg-after-red">15 Sep</a>
											</li>
											<li>
												<a href="#0" data-date="01/11/2014" class="border-after-red bg-after-red">01 Nov</a>
											</li>
											<li>
												<a href="#0" data-date="10/12/2014" class="border-after-red bg-after-red">10 Dec</a>
											</li>
											<li>
												<a href="#0" data-date="19/01/2015" class="border-after-red bg-after-red">29 Jan</a>
											</li>
											<li>
												<a href="#0" data-date="03/03/2015" class="border-after-red bg-after-red">3 Mar</a>
											</li>
										</ol>
										<span class="filling-line bg-red" aria-hidden="true"></span>
									</div>
									<!-- .events -->
								</div>
								<!-- .events-wrapper -->
								<ul class="cd-timeline-navigation mt-ht-nav-icon">
									<li>
										<a href="#0" class="prev inactive btn btn-outline red md-skip">
											<i class="fa fa-chevron-left"></i>
										</a>
									</li>
									<li>
										<a href="#0" class="next btn btn-outline red md-skip">
											<i class="fa fa-chevron-right"></i>
										</a>
									</li>
								</ul>
								<!-- .cd-timeline-navigation -->
							</div>
							<!-- .timeline -->
							<div class="events-content">
								<ol>
									<li class="selected" data-date="16/01/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">New User</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_3.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">16 January 2014 : 7:45 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, mi felis, aliquam at iaculis mi felis, aliquam
												at iaculis finibus eu ex. Integer efficitur tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur odio non est rhoncus volutpat.</p>
											<a href="javascript:;" class="btn btn-circle red btn-outline">Read More</a>
											<a href="javascript:;" class="btn btn-circle btn-icon-only blue">
												<i class="fa fa-plus"></i>
											</a>
											<a href="javascript:;" class="btn btn-circle btn-icon-only green pull-right">
												<i class="fa fa-twitter"></i>
											</a>
										</div>
									</li>
									<li data-date="28/02/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Sending Shipment</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_3.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Hugh Grant</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">28 February 2014 : 10:15 AM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle btn-outline green-jungle">Download Shipment List</a>
											<div class="btn-group dropup pull-right">
												<button class="btn btn-circle blue-steel dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu pull-right" role="menu">
													<li>
														<a href="javascript:;">Action </a>
													</li>
													<li>
														<a href="javascript:;">Another action </a>
													</li>
													<li>
														<a href="javascript:;">Something else here </a>
													</li>
													<li class="divider"> </li>
													<li>
														<a href="javascript:;">Separated link </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li data-date="20/04/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Blue Chambray</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_1.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue">Rory Matthew</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">20 April 2014 : 10:45 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<a href="javascript:;" class="btn btn-circle red">Read More</a>
										</div>
									</li>
									<li data-date="20/05/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">20 May 2014 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="09/07/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Event Success</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_1.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Matt Goldman</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">9 July 2014 : 8:15 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde.</p>
											<a href="javascript:;"
												class="btn btn-circle btn-outline purple-medium">View Summary</a>
											<div class="btn-group dropup pull-right">
												<button class="btn btn-circle green dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu pull-right" role="menu">
													<li>
														<a href="javascript:;">Action </a>
													</li>
													<li>
														<a href="javascript:;">Another action </a>
													</li>
													<li>
														<a href="javascript:;">Something else here </a>
													</li>
													<li class="divider"> </li>
													<li>
														<a href="javascript:;">Separated link </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li data-date="30/08/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Conference Call</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_1.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Rory Matthew</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">30 August 2014 : 5:45 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<img class="timeline-body-img pull-left" src="../assets/pages/media/blog/5.jpg" alt="">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<a href="javascript:;" class="btn btn-circle red">Read More</a>
										</div>
									</li>
									<li data-date="15/09/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Conference Decision</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_5.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Jessica Wolf</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">15 September 2014 : 8:30 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<img class="timeline-body-img pull-right" src="../assets/pages/media/blog/6.jpg" alt="">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut.</p>
											<a href="javascript:;" class="btn btn-circle green-sharp">Read More</a>
										</div>
									</li>
									<li data-date="01/11/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">1 November 2014 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="10/12/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">10 December 2015 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="19/01/2015">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">19 January 2015 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="03/03/2015">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">3 March 2015 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
								</ol>
							</div>
							<!-- .events-content -->
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-directions font-green hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Events</span>
							<span class="caption-helper">Horizontal Timeline</span>
						</div>
						<div class="actions">
							<div class="btn-group btn-group-devided" data-toggle="buttons">
								<label class="btn green btn-outline btn-circle btn-sm active">
									<input type="radio" name="options" class="toggle" id="option1">Actions</label>
								<label class="btn  green btn-outline btn-circle btn-sm">
									<input type="radio" name="options" class="toggle" id="option2">Tools</label>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="cd-horizontal-timeline mt-timeline-horizontal" data-spacing="60">
							<div class="timeline mt-timeline-square">
								<div class="events-wrapper">
									<div class="events">
										<ol>
											<li>
												<a href="#0" data-date="16/01/2014" class="border-after-blue bg-after-blue selected">Expo 2016</a>
											</li>
											<li>
												<a href="#0" data-date="28/02/2014" class="border-after-blue bg-after-blue">New Promo</a>
											</li>
											<li>
												<a href="#0" data-date="20/04/2014" class="border-after-blue bg-after-blue">Meeting</a>
											</li>
											<li>
												<a href="#0" data-date="20/05/2014" class="border-after-blue bg-after-blue">Launch</a>
											</li>
											<li>
												<a href="#0" data-date="09/07/2014" class="border-after-blue bg-after-blue">Party</a>
											</li>
											<li>
												<a href="#0" data-date="30/08/2014" class="border-after-blue bg-after-blue">Reports</a>
											</li>
											<li>
												<a href="#0" data-date="15/09/2014" class="border-after-blue bg-after-blue">HR</a>
											</li>
											<li>
												<a href="#0" data-date="01/11/2014" class="border-after-blue bg-after-blue">IPO</a>
											</li>
											<li>
												<a href="#0" data-date="10/12/2014" class="border-after-blue bg-after-blue">Board</a>
											</li>
											<li>
												<a href="#0" data-date="19/01/2015" class="border-after-blue bg-after-blue">Revenue</a>
											</li>
											<li>
												<a href="#0" data-date="03/03/2015" class="border-after-blue bg-after-blue">Dinner</a>
											</li>
										</ol>
										<span class="filling-line bg-blue" aria-hidden="true"></span>
									</div>
									<!-- .events -->
								</div>
								<!-- .events-wrapper -->
								<ul class="cd-timeline-navigation mt-ht-nav-icon">
									<li>
										<a href="#0" class="prev inactive btn blue md-skip">
											<i class="fa fa-chevron-left"></i>
										</a>
									</li>
									<li>
										<a href="#0" class="next btn blue md-skip">
											<i class="fa fa-chevron-right"></i>
										</a>
									</li>
								</ul>
								<!-- .cd-timeline-navigation -->
							</div>
							<!-- .timeline -->
							<div class="events-content">
								<ol>
									<li class="selected" data-date="16/01/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Expo 2016 Launch</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Lisa Bold</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">23 February 2014</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod mi felis, aliquam at iaculis eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis mi felis, aliquam
												at iaculis eu, onsectetur adipiscing elit finibus eu ex. Integer efficitur leo eget dolor tincidunt, et dignissim risus lacinia. Nam in egestas onsectetur adipiscing elit nunc. Suspendisse
												potenti</p>
											<a href="javascript:;" class="btn btn-circle dark btn-outline">Read More</a>
											<a href="javascript:;" class="btn btn-circle btn-icon-only green pull-right">
												<i class="fa fa-twitter"></i>
											</a>
										</div>
									</li>
									<li data-date="28/02/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Sending Shipment</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_3.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Hugh Grant</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">28 February 2014 : 10:15 AM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle btn-outline green-jungle">Download Shipment List</a>
											<div class="btn-group dropup pull-right">
												<button class="btn btn-circle blue-steel dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu pull-right" role="menu">
													<li>
														<a href="javascript:;">Action </a>
													</li>
													<li>
														<a href="javascript:;">Another action </a>
													</li>
													<li>
														<a href="javascript:;">Something else here </a>
													</li>
													<li class="divider"> </li>
													<li>
														<a href="javascript:;">Separated link </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li data-date="20/04/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Blue Chambray</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_1.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue">Rory Matthew</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">20 April 2014 : 10:45 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<a href="javascript:;" class="btn btn-circle red">Read More</a>
										</div>
									</li>
									<li data-date="20/05/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">20 May 2014 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="09/07/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Event Success</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_1.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Matt Goldman</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">9 July 2014 : 8:15 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde.</p>
											<a href="javascript:;"
												class="btn btn-circle btn-outline purple-medium">View Summary</a>
											<div class="btn-group dropup pull-right">
												<button class="btn btn-circle green dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Actions
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu pull-right" role="menu">
													<li>
														<a href="javascript:;">Action </a>
													</li>
													<li>
														<a href="javascript:;">Another action </a>
													</li>
													<li>
														<a href="javascript:;">Something else here </a>
													</li>
													<li class="divider"> </li>
													<li>
														<a href="javascript:;">Separated link </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li data-date="30/08/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Conference Call</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_1.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Rory Matthew</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">30 August 2014 : 5:45 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<img class="timeline-body-img pull-left" src="../assets/pages/media/blog/5.jpg" alt="">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut. laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut. </p>
											<a href="javascript:;" class="btn btn-circle red">Read More</a>
										</div>
									</li>
									<li data-date="15/09/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Conference Decision</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_5.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Jessica Wolf</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">15 September 2014 : 8:30 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<img class="timeline-body-img pull-right" src="../assets/pages/media/blog/6.jpg" alt="">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis
												qui ut.</p>
											<a href="javascript:;" class="btn btn-circle green-sharp">Read More</a>
										</div>
									</li>
									<li data-date="01/11/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">1 November 2014 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="10/12/2014">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">10 December 2014 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="19/01/2015">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">19 January 2015 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
									<li data-date="03/03/2015">
										<div class="mt-title">
											<h2 class="mt-content-title">Timeline Received</h2>
										</div>
										<div class="mt-author">
											<div class="mt-avatar">
												<img src="../assets/pages/media/users/avatar80_2.jpg" />
											</div>
											<div class="mt-author-name">
												<a href="javascript:;" class="font-blue-madison">Andres Iniesta</a>
											</div>
											<div class="mt-author-datetime font-grey-mint">3 March 2015 : 12:20 PM</div>
										</div>
										<div class="clearfix"></div>
										<div class="mt-content border-grey-steel">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eleifend ipsum, at posuere augue. Pellentesque mi felis, aliquam at iaculis eu, finibus eu ex. Integer efficitur leo eget
												dolor tincidunt, et dignissim risus lacinia. Nam in egestas nunc. Suspendisse potenti. Cras ullamcorper tincidunt malesuada. Sed sit amet molestie elit, vel placerat ipsum. Ut consectetur
												odio non est rhoncus volutpat. Nullam interdum, neque quis vehicula ornare, lacus elit dignissim purus, quis ultrices erat tortor eget felis. Cras commodo id massa at condimentum. Praesent
												dignissim luctus risus sed sodales.</p>
											<a href="javascript:;" class="btn btn-circle green-turquoise">Read More</a>
										</div>
									</li>
								</ol>
							</div>
							<!-- .events-content -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Recent Activities</span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a class="btn btn-sm blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By
									<i class="fa fa-angle-down"></i>
								</a>
								<div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
									<label class="mt-checkbox mt-checkbox-outline">
										<input type="checkbox" /> Finance
										<span></span>
									</label>
									<label class="mt-checkbox mt-checkbox-outline">
										<input type="checkbox" checked="" /> Membership
										<span></span>
									</label>
									<label class="mt-checkbox mt-checkbox-outline">
										<input type="checkbox" /> Customer Support
										<span></span>
									</label>
									<label class="mt-checkbox mt-checkbox-outline">
										<input type="checkbox" checked="" /> HR
										<span></span>
									</label>
									<label class="mt-checkbox mt-checkbox-outline">
										<input type="checkbox" /> System
										<span></span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
							<ul class="feeds">
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-info">
													<i class="fa fa-check"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> You have 4 pending tasks.
													<span class="label label-sm label-warning "> Take action
														<i class="fa fa-share"></i>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> Just now </div>
									</div>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-success">
														<i class="fa fa-bar-chart-o"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc"> Finance Report for year 2013 has been released. </div>
												</div>
											</div>
										</div>
										<div class="col2">
											<div class="date"> 20 mins </div>
										</div>
									</a>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-danger">
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> You have 5 pending membership that requires a quick review. </div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 24 mins </div>
									</div>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-info">
													<i class="fa fa-shopping-cart"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> New order received with
													<span class="label label-sm label-success"> Reference Number: DR23923 </span>
												</div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 30 mins </div>
									</div>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-success">
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> You have 5 pending membership that requires a quick review. </div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 24 mins </div>
									</div>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-default">
													<i class="fa fa-bell-o"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> Web server hardware needs to be upgraded.
													<span class="label label-sm label-default "> Overdue </span>
												</div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 2 hours </div>
									</div>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-default">
														<i class="fa fa-briefcase"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc"> IPO Report for year 2013 has been released. </div>
												</div>
											</div>
										</div>
										<div class="col2">
											<div class="date"> 20 mins </div>
										</div>
									</a>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-info">
													<i class="fa fa-check"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> You have 4 pending tasks.
													<span class="label label-sm label-warning "> Take action
														<i class="fa fa-share"></i>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> Just now </div>
									</div>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-danger">
														<i class="fa fa-bar-chart-o"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc"> Finance Report for year 2013 has been released. </div>
												</div>
											</div>
										</div>
										<div class="col2">
											<div class="date"> 20 mins </div>
										</div>
									</a>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-default">
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> You have 5 pending membership that requires a quick review. </div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 24 mins </div>
									</div>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-info">
													<i class="fa fa-shopping-cart"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> New order received with
													<span class="label label-sm label-success"> Reference Number: DR23923 </span>
												</div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 30 mins </div>
									</div>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-success">
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> You have 5 pending membership that requires a quick review. </div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 24 mins </div>
									</div>
								</li>
								<li>
									<div class="col1">
										<div class="cont">
											<div class="cont-col1">
												<div class="label label-sm label-warning">
													<i class="fa fa-bell-o"></i>
												</div>
											</div>
											<div class="cont-col2">
												<div class="desc"> Web server hardware needs to be upgraded.
													<span class="label label-sm label-default "> Overdue </span>
												</div>
											</div>
										</div>
									</div>
									<div class="col2">
										<div class="date"> 2 hours </div>
									</div>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-briefcase"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc"> IPO Report for year 2013 has been released. </div>
												</div>
											</div>
										</div>
										<div class="col2">
											<div class="date"> 20 mins </div>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<div class="scroller-footer">
							<div class="btn-arrow-link pull-right">
								<a href="javascript:;">See All Records</a>
								<i class="icon-arrow-right"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light tasks-widget bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Tasks</span>
							<span class="caption-helper">tasks summary...</span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a class="btn blue-oleo btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> More
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> All Project </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;"> AirAsia </a>
									</li>
									<li>
										<a href="javascript:;"> Cruise </a>
									</li>
									<li>
										<a href="javascript:;"> HSBC </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;"> Pending
											<span class="badge badge-danger"> 4 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;"> Completed
											<span class="badge badge-success"> 12 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;"> Overdue
											<span class="badge badge-warning"> 9 </span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="task-content">
							<div class="scroller" style="height: 312px;" data-always-visible="1" data-rail-visible1="1">
								<!-- START TASK LIST -->
								<ul class="task-list">
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Present 2013 Year IPO Statistics at Board Meeting </span>
											<span class="label label-sm label-success">Company</span>
											<span class="task-bell">
												<i class="fa fa-bell-o"></i>
											</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Hold An Interview for Marketing Manager Position </span>
											<span class="label label-sm label-danger">Marketing</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> AirAsia Intranet System Project Internal Meeting </span>
											<span class="label label-sm label-success">AirAsia</span>
											<span class="task-bell">
												<i class="fa fa-bell-o"></i>
											</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Technical Management Meeting </span>
											<span class="label label-sm label-warning">Company</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Kick-off Company CRM Mobile App Development </span>
											<span class="label label-sm label-info">Internal Products</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Prepare Commercial Offer For SmartVision Website Rewamp </span>
											<span class="label label-sm label-danger">SmartVision</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Sign-Off The Comercial Agreement With AutoSmart </span>
											<span class="label label-sm label-default">AutoSmart</span>
											<span class="task-bell">
												<i class="fa fa-bell-o"></i>
											</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group dropup">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li>
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> Company Staff Meeting </span>
											<span class="label label-sm label-success">Cruise</span>
											<span class="task-bell">
												<i class="fa fa-bell-o"></i>
											</span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group dropup">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li class="last-line">
										<div class="task-checkbox">
											<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
												<input type="checkbox" class="checkboxes" value="1" />
												<span></span>
											</label>
										</div>
										<div class="task-title">
											<span class="task-title-sp"> KeenThemes Investment Discussion </span>
											<span class="label label-sm label-warning">KeenThemes </span>
										</div>
										<div class="task-config">
											<div class="task-config-btn btn-group dropup">
												<a class="btn btn-sm default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
													<i class="fa fa-cog"></i>
													<i class="fa fa-angle-down"></i>
												</a>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;">
															<i class="fa fa-check"></i> Complete </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-pencil"></i> Edit </a>
													</li>
													<li>
														<a href="javascript:;">
															<i class="fa fa-trash-o"></i> Cancel </a>
													</li>
												</ul>
											</div>
										</div>
									</li>
								</ul>
								<!-- END START TASK LIST -->
							</div>
						</div>
						<div class="task-footer">
							<div class="btn-arrow-link pull-right">
								<a href="javascript:;">See All Records</a>
								<i class="icon-arrow-right"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">General Stats</span>
						</div>
						<div class="actions">
							<a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
								<i class="fa fa-repeat"></i> Reload </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-4">
								<div class="easy-pie-chart">
									<div class="number transactions" data-percent="55">
										<span>+55</span>% </div>
									<a class="title" href="javascript:;"> Transactions
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="easy-pie-chart">
									<div class="number visits" data-percent="85">
										<span>+85</span>% </div>
									<a class="title" href="javascript:;"> New Visits
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="easy-pie-chart">
									<div class="number bounce" data-percent="46">
										<span>-46</span>% </div>
									<a class="title" href="javascript:;"> Bounce
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-equalizer font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Server Stats</span>
							<span class="caption-helper">monthly stats...</span>
						</div>
						<div class="tools">
							<a href="" class="collapse"> </a>
							<a href="#portlet-config" data-toggle="modal" class="config"> </a>
							<a href="" class="reload"> </a>
							<a href="" class="remove"> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-4">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_bar5"></div>
									<a class="title" href="javascript:;"> Network
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_bar6"></div>
									<a class="title" href="javascript:;"> CPU Load
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_line"></div>
									<a class="title" href="javascript:;"> Load Rate
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<!-- BEGIN REGIONAL STATS PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-share font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Regional Stats</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-cloud-upload"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-wrench"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" data-container="false" data-placement="bottom" href="javascript:;"> </a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-trash"></i>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div id="region_statistics_loading">
							<img src="../assets/global/img/loading.gif" alt="loading" /> </div>
						<div id="region_statistics_content" class="display-none">
							<div class="btn-toolbar margin-bottom-10">
								<div class="btn-group btn-group-circle" data-toggle="buttons">
									<a href="" class="btn grey-salsa btn-sm active"> Users </a>
									<a href="" class="btn grey-salsa btn-sm"> Orders </a>
								</div>
								<div class="btn-group pull-right">
									<a href="" class="btn btn-circle grey-salsa btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Select Region
										<span class="fa fa-angle-down"> </span>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="javascript:;" id="regional_stat_world"> World </a>
										</li>
										<li>
											<a href="javascript:;" id="regional_stat_usa"> USA </a>
										</li>
										<li>
											<a href="javascript:;" id="regional_stat_europe"> Europe </a>
										</li>
										<li>
											<a href="javascript:;" id="regional_stat_russia"> Russia </a>
										</li>
										<li>
											<a href="javascript:;" id="regional_stat_germany"> Germany </a>
										</li>
									</ul>
								</div>
							</div>
							<div id="vmap_world" class="vmaps display-none"> </div>
							<div id="vmap_usa" class="vmaps display-none"> </div>
							<div id="vmap_europe" class="vmaps display-none"> </div>
							<div id="vmap_russia" class="vmaps display-none"> </div>
							<div id="vmap_germany" class="vmaps display-none"> </div>
						</div>
					</div>
				</div>
				<!-- END REGIONAL STATS PORTLET-->
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title tabbable-line">
						<div class="caption">
							<i class="icon-globe font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Feeds</span>
						</div>
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" class="active" data-toggle="tab"> System </a>
							</li>
							<li>
								<a href="#tab_1_2" data-toggle="tab"> Activities </a>
							</li>
						</ul>
					</div>
					<div class="portlet-body">
						<!--BEGIN TABS-->
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
								<div class="scroller" style="height: 339px;" data-always-visible="1" data-rail-visible="0">
									<ul class="feeds">
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-success">
															<i class="fa fa-bell-o"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> You have 4 pending tasks.
															<span class="label label-sm label-info"> Take action
																<i class="fa fa-share"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> Just now </div>
											</div>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New version v1.4 just lunched! </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> 20 mins </div>
												</div>
											</a>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-danger">
															<i class="fa fa-bolt"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> Database server #12 overloaded. Please fix the issue. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 24 mins </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 30 mins </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-success">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 40 mins </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-warning">
															<i class="fa fa-plus"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New user registered. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 1.5 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-success">
															<i class="fa fa-bell-o"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> Web server hardware needs to be upgraded.
															<span class="label label-sm label-default "> Overdue </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 2 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-default">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 3 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-warning">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 5 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 18 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-default">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 21 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 22 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-default">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 21 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 22 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-default">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 21 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 22 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-default">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 21 hours </div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-bullhorn"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> New order received. Please take care of it. </div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 22 hours </div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="tab-pane" id="tab_1_2">
								<div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
									<ul class="feeds">
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New order received </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> 10 mins </div>
												</div>
											</a>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-danger">
															<i class="fa fa-bolt"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc"> Order #24DOP4 has been rejected.
															<span class="label label-sm label-danger "> Take action
																<i class="fa fa-share"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date"> 24 mins </div>
											</div>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-bell-o"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc"> New user registered </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> Just now </div>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet light calendar bordered">
					<div class="portlet-title ">
						<div class="caption">
							<i class="icon-calendar font-dark hide"></i>
							<span class="caption-subject font-dark bold uppercase">Feeds</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="calendar"> </div>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-hide hide"></i>
							<span class="caption-subject font-hide bold uppercase">Chats</span>
						</div>
						<div class="actions">
							<div class="portlet-input input-inline">
								<div class="input-icon right">
									<i class="icon-magnifier"></i>
									<input type="text" class="form-control input-circle" placeholder="search..."> </div>
							</div>
						</div>
					</div>
					<div class="portlet-body" id="chats">
						<div class="scroller" style="height: 525px;" data-always-visible="1" data-rail-visible1="1">
							<ul class="chats">
								<li class="out">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Lisa Wong </a>
										<span class="datetime"> at 20:11 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="out">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar2.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Lisa Wong </a>
										<span class="datetime"> at 20:11 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="in">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Bob Nilson </a>
										<span class="datetime"> at 20:30 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="in">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Bob Nilson </a>
										<span class="datetime"> at 20:30 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="out">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Richard Doe </a>
										<span class="datetime"> at 20:33 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="in">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Richard Doe </a>
										<span class="datetime"> at 20:35 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="out">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Bob Nilson </a>
										<span class="datetime"> at 20:40 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="in">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar3.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Richard Doe </a>
										<span class="datetime"> at 20:40 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
									</div>
								</li>
								<li class="out">
									<img class="avatar" alt="" src="../assets/layouts/layout/img/avatar1.jpg" />
									<div class="message">
										<span class="arrow"> </span>
										<a href="javascript:;" class="name"> Bob Nilson </a>
										<span class="datetime"> at 20:54 </span>
										<span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. sed diam nonummy nibh euismod tincidunt ut laoreet.
											</span>
									</div>
								</li>
							</ul>
						</div>
						<div class="chat-form">
							<div class="input-cont">
								<input class="form-control" type="text" placeholder="Type a message here..." /> </div>
							<div class="btn-cont">
								<span class="arrow"> </span>
								<a href="" class="btn blue icn-only">
									<i class="fa fa-check icon-white"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bubble font-dark hide"></i>
							<span class="caption-subject font-hide bold uppercase">Recent Users</span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> Option 1</a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;">Option 2</a>
									</li>
									<li>
										<a href="javascript:;">Option 3</a>
									</li>
									<li>
										<a href="javascript:;">Option 4</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-4">
								<!--begin: widget 1-1 -->
								<div class="mt-widget-1">
									<div class="mt-icon">
										<a href="#">
											<i class="icon-plus"></i>
										</a>
									</div>
									<div class="mt-img">
										<img src="../assets/pages/media/users/avatar80_8.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Diana Ellison</h3>
										<p class="mt-user-title"> Lorem Ipsum is simply dummy text. </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red">
													<i class="icon-bubbles"></i> 1,7k </a>
												<a href="javascript:;" class="btn font-green">
													<i class="icon-social-twitter"></i> 2,6k </a>
												<a href="javascript:;" class="btn font-yellow">
													<i class="icon-emoticon-smile"></i> 3,7k </a>
											</div>
										</div>
									</div>
								</div>
								<!--end: widget 1-1 -->
							</div>
							<div class="col-md-4">
								<!--begin: widget 1-2 -->
								<div class="mt-widget-1">
									<div class="mt-icon">
										<a href="#">
											<i class="icon-plus"></i>
										</a>
									</div>
									<div class="mt-img">
										<img src="../assets/pages/media/users/avatar80_7.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Jason Baker</h3>
										<p class="mt-user-title"> Lorem Ipsum is simply dummy text. </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-yellow">
													<i class="icon-bubbles"></i> 1,7k </a>
												<a href="javascript:;" class="btn font-blue">
													<i class="icon-social-twitter"></i> 2,6k </a>
												<a href="javascript:;" class="btn font-green">
													<i class="icon-emoticon-smile"></i> 3,7k </a>
											</div>
										</div>
									</div>
								</div>
								<!--end: widget 1-2 -->
							</div>
							<div class="col-md-4">
								<!--begin: widget 1-3 -->
								<div class="mt-widget-1">
									<div class="mt-icon">
										<a href="#">
											<i class="icon-plus"></i>
										</a>
									</div>
									<div class="mt-img">
										<img src="../assets/pages/media/users/avatar80_6.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Julia Berry</h3>
										<p class="mt-user-title"> Lorem Ipsum is simply dummy text. </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-yellow">
													<i class="icon-bubbles"></i> 1,7k </a>
												<a href="javascript:;" class="btn font-red">
													<i class="icon-social-twitter"></i> 2,6k </a>
												<a href="javascript:;" class="btn font-green">
													<i class="icon-emoticon-smile"></i> 3,7k </a>
											</div>
										</div>
									</div>
								</div>
								<!--end: widget 1-3 -->
							</div>
						</div>
					</div>
				</div>
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Recent Works</span>
							<span class="caption-helper">default option...</span>
						</div>
						<div class="actions">
							<div class="btn-group btn-group-devided" data-toggle="buttons">
								<label class="btn red btn-outline btn-circle btn-sm active">
									<input type="radio" name="options" class="toggle" id="option1">Settings</label>
								<label class="btn  red btn-outline btn-circle btn-sm">
									<input type="radio" name="options" class="toggle" id="option2">Tools</label>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-6">
								<div class="mt-widget-2">
									<div class="mt-head" style="background-image: url(../assets/pages/img/background/32.jpg);">
										<div class="mt-head-label">
											<button type="button" class="btn btn-success">Manhattan</button>
										</div>
										<div class="mt-head-user">
											<div class="mt-head-user-img">
												<img src="../assets/pages/img/avatars/team7.jpg"> </div>
											<div class="mt-head-user-info">
												<span class="mt-user-name">Chris Jagers</span>
												<span class="mt-user-time">
													<i class="icon-emoticon-smile"></i> 3 mins ago </span>
											</div>
										</div>
									</div>
									<div class="mt-body">
										<h3 class="mt-body-title"> Thomas Clark </h3>
										<p class="mt-body-description"> It is a long established fact that a reader will be distracted </p>
										<ul class="mt-body-stats">
											<li class="font-green">
												<i class="icon-emoticon-smile"></i> 3,7k</li>
											<li class="font-yellow">
												<i class=" icon-social-twitter"></i> 3,7k</li>
											<li class="font-red">
												<i class="  icon-bubbles"></i> 3,7k</li>
										</ul>
										<div class="mt-body-actions">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn">
													<i class="icon-bubbles"></i> Bookmark </a>
												<a href="javascript:;" class="btn ">
													<i class="icon-social-twitter"></i> Share </a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mt-widget-2">
									<div class="mt-head" style="background-image: url(../assets/pages/img/background/43.jpg);">
										<div class="mt-head-label">
											<button type="button" class="btn btn-danger">London</button>
										</div>
										<div class="mt-head-user">
											<div class="mt-head-user-img">
												<img src="../assets/pages/img/avatars/team3.jpg"> </div>
											<div class="mt-head-user-info">
												<span class="mt-user-name">Harry Harris</span>
												<span class="mt-user-time">
													<i class="icon-user"></i> 3 mins ago </span>
											</div>
										</div>
									</div>
									<div class="mt-body">
										<h3 class="mt-body-title"> Christian Davidson </h3>
										<p class="mt-body-description"> It is a long established fact that a reader will be distracted </p>
										<ul class="mt-body-stats">
											<li class="font-green">
												<i class="icon-emoticon-smile"></i> 3,7k</li>
											<li class="font-yellow">
												<i class=" icon-social-twitter"></i> 3,7k</li>
											<li class="font-red">
												<i class="  icon-bubbles"></i> 3,7k</li>
										</ul>
										<div class="mt-body-actions">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn ">
													<i class="icon-bubbles"></i> Bookmark </a>
												<a href="javascript:;" class="btn ">
													<i class="icon-social-twitter"></i> Share </a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-xs-12 col-sm-12">
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Recent Projects</span>
							<span class="caption-helper">default option...</span>
						</div>
						<div class="actions">
							<div class="btn-group btn-group-devided" data-toggle="buttons">
								<label class="btn blue btn-outline btn-circle btn-sm active">
									<input type="radio" name="options" class="toggle" id="option1">Actions</label>
								<label class="btn  blue btn-outline btn-circle btn-sm">
									<input type="radio" name="options" class="toggle" id="option2">Tools</label>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-4">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="../assets/pages/img/background/34.jpg" /> </div>
									<div class="mt-container bg-purple-opacity">
										<div class="mt-head-title"> Website Revamp & Deployment </div>
										<div class="mt-body-icons">
											<a href="#">
												<i class=" icon-pencil"></i>
											</a>
											<a href="#">
												<i class=" icon-map"></i>
											</a>
											<a href="#">
												<i class=" icon-trash"></i>
											</a>
										</div>
										<div class="mt-footer-button">
											<button type="button" class="btn btn-circle btn-danger btn-sm">Dior</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="../assets/pages/img/background/46.jpg" /> </div>
									<div class="mt-container bg-green-opacity">
										<div class="mt-head-title"> CRM Development & Deployment </div>
										<div class="mt-body-icons">
											<a href="#">
												<i class=" icon-social-twitter"></i>
											</a>
											<a href="#">
												<i class=" icon-bubbles"></i>
											</a>
											<a href="#">
												<i class=" icon-bell"></i>
											</a>
										</div>
										<div class="mt-footer-button">
											<button type="button" class="btn btn-circle blue-ebonyclay btn-sm">Nike</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="../assets/pages/img/background/37.jpg" /> </div>
									<div class="mt-container bg-dark-opacity">
										<div class="mt-head-title"> Marketing Campaigns </div>
										<div class="mt-body-icons">
											<a href="#">
												<i class=" icon-bubbles"></i>
											</a>
											<a href="#">
												<i class=" icon-map"></i>
											</a>
											<a href="#">
												<i class=" icon-cup"></i>
											</a>
										</div>
										<div class="mt-footer-button">
											<button type="button" class="btn btn-circle btn-success btn-sm">Honda</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="portlet light portlet-fit bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Activities</span>
							<span class="caption-helper">default option...</span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a class="btn red btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;"> Option 1</a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;">Option 2</a>
									</li>
									<li>
										<a href="javascript:;">Option 3</a>
									</li>
									<li>
										<a href="javascript:;">Option 4</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-4">
								<div class="mt-widget-3">
									<div class="mt-head bg-blue-hoki">
										<div class="mt-head-icon">
											<i class=" icon-social-twitter"></i>
										</div>
										<div class="mt-head-desc"> Lorem Ipsum is simply dummy text of the ... </div>
										<span class="mt-head-date"> 25 Jan, 2015 </span>
										<div class="mt-head-button">
											<button type="button" class="btn btn-circle btn-outline white btn-sm">Add</button>
										</div>
									</div>
									<div class="mt-body-actions-icons">
										<div class="btn-group btn-group btn-group-justified">
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-align-justify"></i>
												</span>RECORD </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-camera"></i>
												</span>PHOTO </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-calendar"></i>
												</span>DATE </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-record"></i>
												</span>RANC </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="mt-widget-3">
									<div class="mt-head bg-red">
										<div class="mt-head-icon">
											<i class="icon-user"></i>
										</div>
										<div class="mt-head-desc"> Lorem Ipsum is simply dummy text of the ... </div>
										<span class="mt-head-date"> 12 Feb, 2016 </span>
										<div class="mt-head-button">
											<button type="button" class="btn btn-circle btn-outline white btn-sm">Add</button>
										</div>
									</div>
									<div class="mt-body-actions-icons">
										<div class="btn-group btn-group btn-group-justified">
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-align-justify"></i>
												</span>RECORD </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-camera"></i>
												</span>PHOTO </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-calendar"></i>
												</span>DATE </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-record"></i>
												</span>RANC </a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="mt-widget-3">
									<div class="mt-head bg-green">
										<div class="mt-head-icon">
											<i class=" icon-graduation"></i>
										</div>
										<div class="mt-head-desc"> Lorem Ipsum is simply dummy text of the ... </div>
										<span class="mt-head-date"> 3 Mar, 2015 </span>
										<div class="mt-head-button">
											<button type="button" class="btn btn-circle btn-outline white btn-sm">Add</button>
										</div>
									</div>
									<div class="mt-body-actions-icons">
										<div class="btn-group btn-group btn-group-justified">
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-align-justify"></i>
												</span>RECORD </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-camera"></i>
												</span>PHOTO </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-calendar"></i>
												</span>DATE </a>
											<a href="javascript:;" class="btn ">
												<span class="mt-icon">
													<i class="glyphicon glyphicon-record"></i>
												</span>RANC </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		
		
<!----------------------------------------------------------------------------=============================================================================--------------------------------------------------------------------->
		
	<?php	
	}


function addEditLinks($id){
	$num=getCount("property_table"," where pageID='$id'");
	if($num>0){
		$res=getResult("property_table"," where pageID='$id'");		
		echo '<script type="text/javascript">document.getElementById("pgtitle _Container").innerHTML="'.$res[pageTitle].'";</script>';
		$var1='
		<form method="post" action="" name="faqfrm" enctype="multipart/form-data">
		 <table width="99%" cellpadding="2" cellspacing="0" border="0">
		  <tr>
		   <td align="center" width="100%">
		    <hr size=1 color="maroon" width="100%">
		    <table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
		     <tr><td colspan="3" align="right">(<font color="red">*</font>) mandatory fields</td></tr>		     
		   <tr>
		      <td width="10%" align="left" valign="top"><b><font color="red">*</font>Title</b></td>
		      <td width="1%" valign="top">:&nbsp;</td>
		      <td align="left"><textarea name="pageName" rows=2 cols=100  class="txtfieldadmin">'. $res[pageName].'</textarea></td>
		     </tr>
		     <tr>
		      <td width="10%" align="left" valign="top"><b><font color="red">*</font>Page URL</b></td>
		      <td width="1%" valign="top">:&nbsp;</td>
		      <td align="left"><textarea name="pageUrl" rows=2 cols=100  class="txtfieldadmin">'. $res[pageUrl].'</textarea>.html</td>
		     </tr>
		      <tr>
		      <td width="10%" align="left" valign="top"><b><font color="red">*</font>Meta Title</b></td>
		      <td width="1%" valign="top">:&nbsp;</td>
		      <td align="left"><textarea name="title" rows=2 cols=100  class="txtfieldadmin">'. $res[title].'</textarea></td>
		     </tr>
		        <tr>
		      <td width="10%" align="left" valign="top"><b><font color="red">*</font>Meta Discription</b></td>
		      <td width="1%" valign="top">:&nbsp;</td>
		      <td align="left"><textarea name="metaDiscription" rows=2 cols=100  class="txtfieldadmin">'. $res[metaDiscription].'</textarea></td>
		     </tr>
		        <tr>
		      <td width="10%" align="left" valign="top"><b><font color="red">*</font>Meta Keyword</b></td>
		      <td width="1%" valign="top">:&nbsp;</td>
		      <td align="left"><textarea name="metaKeyword" rows=2 cols=100  class="txtfieldadmin">'. $res[metaKeyword].'</textarea></td>
		     </tr>
		      <tr>
		      <td width="10%" align="left" valign="top"><b><font color="red">*</font>Overview</b></td>
		      <td width="1%" valign="top">:&nbsp;</td>
		      <td align="left"><textarea name="overview" rows=30 cols=100  class="txtfieldadmin">'. $res[overview].'</textarea></td>
		     </tr>
		     
		     <tr><td colspan="3" align="center">&nbsp;</td></tr>
		     <tr>
		      <td colspan="3" align="left" style="padding-left:95px;">
		       <input type="hidden" name="st_id" value="'.$id.'">
		       <input type="submit" name="add_ct" value="Save" class="button">
		      </td>
		     </tr>
		    </table>
		   </td>
		  </tr>
		 </table>
		</form>';
		
		$var1.='
		<script type="text/javascript">
		 var oFCKeditor = new FCKeditor(\'pageDesc\');
		 oFCKeditor.BasePath = "fckeditor/";
		 oFCKeditor.ReplaceTextarea() ;
		</script>';		
	}
	else {
		echo '<script type="text/javascript">document.getElementById("pgtitle _Container").innerHTML="????";</script>';
		$var1='<br><br><center>No Record Found</center>';
	}
					
	return $var1;	
}


function validate_session(){
	$mem=explode("~",$_SESSION[SessionId]);
	$res=getResult("admin_details"," where adminId='$mem[0]'");
	$un1= substr(md5($res[username]), 0, 10);
	$pn1= substr(md5($res[pwd]), 0, 10);	
	if($un1==$mem[1] && $pn1==$mem[2]){
		return "V";
	}else{
		return "I";
	}
}


function adminName(){
	$mem=explode("~",$_SESSION[SessionId]);
	$res=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");
	if($res[name]!=""){
		echo $res[name].",";
	}else{
		echo "Admin,";
	}
}


function required(){
	$mem=explode("~",$_SESSION[SessionId]);
	$res=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");
	if(($res[name]=="" || $res[email]=="" || $res[website]=="")&& $_SESSION[SessionId]!=""){
		return "<br /><center><font style='color:#ee0000;'>Some Required Fields are empty.Please enter this fields by <a href='<?=SITE_ADMIN_URL;?>/myaccount.php'>clicking here</a></font></center>";
	}else{
		return 0;
	}
}


function showdatetime($date){
	$split_time=explode(" ",$date);
	$cdate=explode("-",$split_time[0]);
	$month=$cdate[1];
	$day=$cdate[2];
	$year=$cdate[0];

	$ctime=explode(":",$split_time[1]);
	$hours=$ctime[0];
	$minutes=$ctime[1];
	$seconds=$ctime[2];
	return date('d F, Y G:i:s', mktime($hours, $minutes, $seconds, $month, $day, $year));
}


function date_format_only($date){
	$cdate=explode("-",$date);
	$month=$cdate[1];
	$day=$cdate[2];
	$year=$cdate[0];
	return date('d/m/Y', mktime(0, 0, 0, $month, $day, $year));
}


function showdate($date){
	$cdate=explode("-",$date);
	$month=$cdate[1];
	$day=$cdate[2];
	$year=$cdate[0];

	if($date!="" && $date!="0000-00-00") {
		return date('d-m-Y', mktime($hours, $minutes, $seconds, $month, $day, $year));
	}
	else {
		return "";
	}
}


function adminheader_popup($title1) {
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <title><?=$title?></title>
	 <link href="<?=SITE_ADMIN_URL;?>/css/admin.css" rel="stylesheet" type="text/css" />
	 <link href="<?=SITE_ADMIN_URL;?>/css/styles_dg.css" rel="stylesheet" type="text/css" />
	 <link href="<?=SITE_URL_PATH;?>/css/redapple-preet.css" rel="stylesheet" type="text/css" />
	<link href="<?=SITE_URL_PATH;?>/css/additional.css" rel="stylesheet" type="text/css" />
	<script src="<?=SITE_URL_PATH;?>/js/common.js" type="text/javascript"></script>
	<script src="<?=SITE_URL_PATH;?>/js/validation.js" type="text/javascript"></script>
	 <script language="JavaScript1.2" src="<?=SITE_ADMIN_URL;?>/js/admin.js" type="text/javascript"></script>	 
	 <script type="text/javascript" src="<?=SITE_URL_PATH;?>/js/showHint.js"></script>
	 <link rel="stylesheet" href="<?=SITE_URL_PATH;?>/css/lightbox.css" type="text/css" media="screen" />
	 <script src="<?=SITE_URL_PATH;?>/js/prototype.js" type="text/javascript"></script>
	 <script src="<?=SITE_URL_PATH;?>/js/lightbox.js" type="text/javascript"></script>
	 <script src="<?=SITE_URL_PATH;?>/js/scriptaculous.js?load=effects" type="text/javascript"></script>
	</head>
	<body>
	<center>
	<div id="Body_Area">
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td valign="top" id="Container">
	<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="Margin_top_five" id="Forum_Area">
	<tr>
	<td align="left" width="4" height="4"><img src="images/gray_top-lt-cor.gif" alt="" width="5" height="6" /></td>
	<td width="4" height="4"><img src="images/spacer.gif" alt="" width="1" height="1" /></td>
	<td width="4" height="4" align="right"><img src="images/gray_top-rt-cor.gif" alt="" width="6" height="5" /></td>
	</tr>
	<tr>
	<td><img src="<?php echo SITE_ADMIN_URL;?>/images/spacer.gif" alt="" width="1" height="1" /></td>
	<td style="width:100%;">
	<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="lightBorder">
	<tr  valign="top" >
	<td height="25" valign="middle" class="Bluebg Head2"><?=$title1?></td>
	<td class="Bluebg PadFive white" align="right"><b>Close</b><a href="javascript:window.close()"> <img src="<?=SITE_ADMIN_URL; ?>/images/logout.gif" align="absmiddle" border=0 title="Close Window"></a></td>
	</tr>
	<tr>
	<td bgcolor="#FFFFFF" valign="top" colspan="2"><br />
	<?php
}


function adminfooter_popup(){
	?>
	<br />
	</td></tr></table></td>
	<td width="4" align="right"><img src="<?php echo SITE_URL_PATH;?>/images/spacer.gif" alt="" width="1" height="1" /></td>
	</tr>
	<tr>
	 <td width="4" height="4" align="left" valign="bottom"><img src="<?php echo SITE_URL_PATH;?>/images/gray_bot-lt-cor.gif" alt="" width="6" height="5" /></td>
	 <td width="4" height="4"><img src="<?php echo SITE_URL_PATH;?>/images/spacer.gif" alt="" width="1" height="1" /></td>
	 <td width="4" height="4" align="right" valign="bottom"><img src="<?php echo SITE_URL_PATH;?>/images/gray_bot-rt-cor.gif" alt="" width="5" height="6" /></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</div>
	</center>	
	</body>
	</html>
	<?php 
}


function table_offset($pageno,$num,$wcnt,$num3,$num31,$link2) {
	if (substr($link2,-1)!="?") {
		$link2 = "$link2&";
	}
	if (!strlen($pageno)) {
		$pageno=1;
	}
	$num1 = ($num%$num3);
	$num2 = intval($num/$num3);
	if ($num1>0) {
		$num2=$num2+1;
	}
	if ($wcnt>=$num31) {
		$num5 = ($wcnt%$num31);
		if ($num31>0){
			$num51 = intval($wcnt/$num31);
		}
		$num51=$num51+1;
		if (!strlen($pageno)) {
			$pageno1 = ($wcnt%$num3);
			$pageno = intval($wcnt/$num3);
			if ($pageno1>0) {
				$pageno++;
			}
		}
	}
	$gotopage = "<table width=\"100%\" border=0 cellpadding=1 cellspacing=2 align=center class=\"lightGrayBg\"><tr><td width=\"79%\" align=\"center\">";
	if (empty($offset)) {
		$offset=1;
	}
	$offset_1  = $wcnt+$num3;
	if ($offset_1 >  $num) {
		$offset_1=$num;
	}
	
	//Search Result display - 1 - 20 of 2938
	if ($num2>=1) {
		$gotopage .= SEARCH_RESULT." ". ($wcnt+1) ." - ". $offset_1 ." ".OF." ".$num." &nbsp;&nbsp;</td><td height=\"30\" align=\"right\" class=\"jobsby\" id=\"paging\">";
	}
	else {
		//$gotopage .= "</td><td height=\"30\" align=\"right\" class=\"jobsby\" id=\"paging\">";
	}
	$offset1=0;
	if ($num51<1) {
		$wcnt5=0;
	}
	elseif ($num51==1) {
		$wcnt5=0;
	}
	else {
		$wcnt5=($num51-1)*10;
	}
	if ($wcnt5==0) {
		$offset1=0;
	}
	else {
		$offset1=$wcnt5*$num3;
	}
	
	//$gotopage  .= "PAGE : ";
	for ($iv=$wcnt5+1;$iv<=$wcnt5+10;$iv++) {
		if ($pageno == $iv) {
			$gotopage  .= "<span class=\"navBarTxtCurrent\">$iv</span>";
		}
		else {
			$gotopage  .= " <a class=\"navBarTxt\" href=\"$link2"."offset=$offset1&pageno=$iv#a1\">$iv</a> ";
		}
		$offset1=$iv*$num3;
		if ($num<=$offset1) {
			break;
		}
	}
	$pageno=$wcnt5+10+1;
	$gotopage .= '</td>';
	if ($wcnt>$num31) {
		$pageno1=(($num51-1)*10);
		$offset1=(($num51-1)*$num31)-$num3;
		$gotopage .= "<td width=\"13%\"><a href=\"$link2"."offset=$offset1&pageno=$pageno1#a1\"><img src=\"../".$cimageFolder."/previous.gif\" width=\"68\" height=\"18\" border=\"0\" class=\"lightBorder2\"></a></td>";
	}
	if ($num>$offset1) {
		$gotopage .= "<td width=\"8%\" align=\"right\"><a href=\"$link2"."offset=$offset1&pageno=$pageno1#a1\" class=\"red1\"><img src=\"../".$cimageFolder."\" width=\"47\" height=\"18\" border=\"0\" class=\"lightBorder2\"></a></td>";
	}
	$gotopage .='</td></tr></table>';
	return $gotopage;
}


function rss_write() {
	$pgnm=SITE_DIR_PATH."/rss/news_feed.rss";
	$fp=fopen("$pgnm","w");
	
	$newsCnt=getCount("news_tbl"," where status='Y'");
	if ($newsCnt > 0) {
		$rss_content='<?xml version="1.0" encoding="iso-8859-1" ?> '."\n".'<rss version="2.0">'."\n".'<channel>'."\n".'<title>Red Apple</title>'."\n".'<link>'.$link_rss.'</link>'."\n".'<description>Latest '.$categoryRes['categoryName'].' Articles</description>'."\n".'<language>en-us</language>'."\n".'<generator>Weblinkindia.Net</generator>'."\n";
		$newsQry=db_query("select * from news_tbl where status='Y'");
		while ($newsRes=mysql_fetch_array($newsQry)) {
			$newsRes=ms_htmlentities_decode($newsRes);
			
			$rss_content.='<item>'."\n";
			$look=array('\'','"','&','<','>');
			$replace=array('&quot;', '&quot;', '&amp;', '&lt;', '&gt;');
			$newsHeading=str_replace($look, $replace, $newsRes['newsHeading']);
			$newsDescription=str_replace($look, $replace, $newsRes['newsDescription']);
			$rss_content.='<title>'.$newsHeading.'</title>'."\n";
			$rss_content.='<link>'.$link1.'</link>'."\n";
			$rss_content.='<description>'."\n\t\t".$newsDescription."\n".'</description>'."\n";
			$rss_content.='</item>'."\n";
		}
		$rss_content.='</channel>'."\n";
		$rss_content.='</rss>'."\n";
	}	
	fwrite($fp,$rss_content);
	fclose($fp);	
}
?>