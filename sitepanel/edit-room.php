<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$propertyCont = new propertyData();
$recordID = $_REQUEST[recordID];
$roomID = $_REQUEST[roomID];
$id = $_REQUEST['id'];
//$conect_cloud = $cloud_data->connectCloudinaryAccount($customerId);
$get_prop = $propertyCont->GetPropertyDataWithId($recordID);
$get_room = $propertyCont->getRoomDetailData($recordID, $roomID);
$superAdmin_data = $propertyCont->GetSupperAdminData($customerId);
$superAdmin_res = $superAdmin_data[0];
?>
<?php
$redirect_script_name = "view-property-rooms.php?recordID=''";
@extract($_REQUEST);

/* -------------------------------- CustomerID Updation ------------------------------------------- */
if ($id == "delete") {
    //  updateTable($table_name, " status='D' where roomID='$recordID' ");
    $upd_status = $propertyCont->DeleteRoomID($recordID);


    $_SESSION[session_message] = $record_type . ' has been Delelted successfully.';
    header("Location: manage-property.php");
    exit;
}


if ($id = 'edit' && $recordID != '' && $roomID != '') {

    //$res = getResult("propertyRoom", " where propertyID='$recordID' && roomID='$roomID' && status='Y' || status='N'");
    $res = $get_room[0];

    if ($res->propertyID == '') {
        header("Location: " . SITE_ADMIN_URL . "/" . $redirect_script_name);
    }
}

if ($_REQUEST[action_register] == "update" && $roomID != "" && $recordID != "") {
    @extract($_REQUEST);

    // $numCnt = getCount("propertyRoom", " where roomID='$roomID' && propertyID='$recordID'");
    $numCnt = count($res);
    if ($numCnt == 0) {
        $_SESSION[session_message] = $record_type . ' already exist';
        header("Location: add-room.php?id=edit&recordID=" . $recordID . "&roomID=" . $roomID);

        exit;
    }

    $businessType[] = $_REQUEST['$businessType'];
    for ($x = 0; $x < sizeof($_POST[businessType]); $x++) {
        $arr = $_POST[businessType];
    }
    $businessType1 = implode("^", $arr);
    //echo $businessType1 . "asasasa";
    // updateTable("propertyRoom", " propertyID='$recordID',roomType='$_REQUEST[roomType]',roomOverview='$_REQUEST[roomOverview]',occupancy='$_REQUEST[occupancy]',room_no='$_REQUEST[room_num]',extra_bed='$_REQUEST[extra_bed]',roomAmenties='$businessType1' where propertyID='$recordID' && roomID='$roomID' && customerID='$custID'");
    $update_room = $propertyCont->UpdateRoomData($recordID, $roomID, $customerId, $businessType1);

    $_SESSION[session_message] = $record_type . ' has been updated successfully';
    //header("Location: edit-room.php?id=edit&recordID=" . $recordID . "&roomID=" . $roomID);
    echo '<script type="text/javascript">
                alert("Succesfuly update room data");              
window.location = "ajax_view-rooms.php?propID=' . $recordID . '";
            </script>';
    exit;
}
//admin_header();	
?>

<html>
    <head>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <br><br><br>
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Property Room</span>
                                            <span class="caption-helper">Please Edit Property Rooms</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>

                                        </div>
                                        <span style="float:right;"> <a href="ajax_add-rooms.php?propID=<?php echo $recordID; ?>"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  value="Add Room" /></a></span>
                                                                                                           <!-- <span style="float:left;"><input type="button" style="background:#363bd3;color:white;border:none;height:35px;width:180px;font-size:14px; margin-left: 18px;" data-html="true" onclick="load_ReorderRoomPhoto(<?php echo $propID; ?>);" value="Reorder Photo" /></span>-->
                                        <span style="float:right;"> <a href="ajax_view-rooms.php?propID=<?php echo $recordID; ?>"><button style="background:#FF9800;color:white;border:none;height:35px;width:180px;font-size:14px; margin-right: 15px;">View Room List </button></a></span>

                                        <br><br><br><br></br>
                                    </div>

                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <?php
                                                if ($id = 'edit' && $recordID != '' && $roomID != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Room Type
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="roomType" >
                                                                <option value="<?= $res->roomType; ?>"  ><?= $res->roomType; ?></option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3">Room Overview</label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control" name="roomOverview" rows="6"> <?= $res->roomOverview; ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Room Occupancy
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="occupancy" type="number" max="12000" class="form-control" value="<?= $res->occupancy; ?>" min="0">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">No. of Extra Beds
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="extra_bed" type="number" max="12000" class="form-control" value="<?= $res->extra_bed; ?>" min="0">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">No. of Rooms / Type
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="room_num" type="number" max="12000" class="form-control" value="<?= $res->room_no; ?>" min="0">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Edit Room Price
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-2">
                                                            <input name="roomPriceINR" type="number" max="500000" class="form-control" value="<?= $res->roomPriceINR; ?>" min="0">

                                                        </div>
                                                         <div class="col-md-2">
                                                            <select class = "form-control" name = "roomRateType" required = "">
                                                                <option value = "<?= $res->roomRateType; ?>"><?= $res->roomRateType; ?></option>                                                              
                                                                <option value="night">Daily</option> 
                                                                <option value="month">Monthly</option> 
                                                            </select> 
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Room Amenities</label>
                                                        <div class="col-md-9">
                                                            <div class="mt-checkbox-inline">
                                                                <?php
                                                                //$roomTypeSQL = "select * from roomAmenties";
                                                                //$roomTypequery = db_query($roomTypeSQL);
                                                                $roomAmData = $propertyCont->getAmenitiesList();
                                                                $counter = 0;
                                                                $roomType = explode('^', $res->roomAmenties);

                                                                foreach ($roomAmData as $roomTypeRes) {
                                                                    if ($counter == 0) {
                                                                        ?>

                                                                        <?php
                                                                    }
                                                                    $chk = "";
                                                                    if (in_array($roomTypeRes->roomAmenties_name, $roomType)) {
                                                                        $chk = 'checked="checked"';
                                                                    }
                                                                    ?>

                                                                    <label class="mt-checkbox">
                                                                        <input type="checkbox" id="inlineCheckbox21" name="businessType[]" value="<?= $roomTypeRes->roomAmenties_name; ?>"  <?= $chk; ?>/><?= $roomTypeRes->roomAmenties_name; ?>
                                                                        <span></span>
                                                                    </label>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--   <div class="form-group">
                                                           <label class="control-label col-md-3">Daily Discount in (%) 
                                                               <span class="required"> </span>
                                                           </label>
                                                           <div class="col-md-4">
                                                               <input type="text" class="form-control" max="100" name="daily_dis" value="<?= $res->dailyroomDiscount; ?>" />
                                                               <span class="help-block"> Provide your Daily Discount in (%)  </span>
                                                           </div>
                                                       </div>

                                                       <div class="form-group">
                                                           <label class="control-label col-md-3">Weekly Discount in (%) 
                                                               <span class="required"> </span>
                                                           </label>
                                                           <div class="col-md-4">
                                                               <input type="text" class="form-control" max="100" name="weekly_dis" value="<?= $res->weeklyroomDiscount; ?>" />
                                                               <span class="help-block"> Provide Weekly Discount in (%) . </span>
                                                           </div>
                                                       </div>

                                                       <div class="form-group">
                                                           <label class="control-label col-md-3">Monthly Discount in (%) 
                                                               <span class="required"></span>
                                                           </label>
                                                           <div class="col-md-4">
                                                               <input type="text" class="form-control" max="100" name="monthly_dis" value="<?= $res->monthlyroomDiscount; ?>" />
                                                               <span class="help-block"> Provide Monthly Discount in (%)  </span>
                                                           </div>
                                                       </div>-->

                                                    <?php if ($superAdmin_res->template_type != 'multiple') { ?>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Room Url 
                                                                <span class="required"> </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="hidden" class="form-control"   name="room_url" value="<?= $res->room_url; ?>"  />
                                                                <input type="text" class="form-control"   name="room_url9999" value="<?= $res->room_url; ?>" disabled=""  />
                                                                <span class="help-block"> Provide Room URL  </span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>

                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php
                                                            if ($id = 'edit' && $recordID != '' && $roomID != '') {
                                                                ?> 
                                                                <input name="recordID" type="hidden" value="<?= $res->propertyID; ?>" />
                                                                <input name="roomID" type="hidden" value="<?= $res->roomID; ?>" />
                                                                <input name="action_register" type="hidden" value="update" />
                                                                <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input name="action_register" type="hidden" value="save" />
                                                                <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />
                                                                <?php
                                                            }
                                                            ?>                                                                                                                            
                                                            <a href="javascript:history.back()" class="btn red">Go Back</a>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                    </div>

                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</body>
</html>

<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<?php
admin_footer();
?>