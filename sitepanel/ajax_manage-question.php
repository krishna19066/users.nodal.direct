<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyPhotoCont = new propertyData();
$propertyId = $_REQUEST['propID'];
$property_data = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
?>
<html lang="en">
    <head>
      <?php echo ajaxCssHeader(); ?>      
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>   
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header();
            ?>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }




                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }

                tr:nth-child(odd){
                    background-color:#fff;
                }

                th {

                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                </style>

                <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> PROPERTY - <span style="color:#e44787;"><?php echo $propertyName; ?></span>

                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Manage Property Question</span>
                        </li>
                    </ul>
                    <?php
                    if (isset($_REQUEST['propID'])) {
                        $propertyId = $_REQUEST['propID'];

                        // $connectCloudinary = $ajaxpropertyPhotoCont->connectCloudinaryAccount($customerId);
                        ?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <span style="float:left; margin-top: -20px;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Questions" /></span>

                                            <span style="float:right; margin-top: -20px;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body form" id="add_photo" style="display:none;">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" action="../sitepanel/add_property.php" name="register_member_form" method="post" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Question
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="question"  value=""   />
                                            <input type="hidden" value="<?php echo $propertyId; ?>" name="propertyID">

                                        </div>
                                    </div>

                                    <div class="form-group last">
                                        <label class="control-label col-md-3">Answer</label>
                                        <div class="col-md-9">
                                            <textarea class="ckeditor form-control" name="answer" rows="6"> </textarea>
                                        </div>
                                    </div>                
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input name="recordID" type="hidden" value="12" />
                                                <input name="action_register" type="hidden" value="submit" />
                                                <input type="submit" class="btn green" name="submit_question" class="button" id="submit" value="Add Property Question" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>	

                        <div class="row">
                            <div class="col-md-12">	
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-body">
                                        <div style="padding-left:35px; color:rgba(0, 0, 0, 0.7);">
                                            <?php
                                            $propQuestListdata = $ajaxpropertyPhotoCont->getPropertyQuestions($propertyId);
                                            if ($propQuestListdata != NULL) {
                                                ?>
                                                <table class="ques_table" width="90%">
                                                    <tr style="padding:10px; border-bottom: 1px solid rgb(144, 139, 141);">
                                                        <th style="background-color:#fff; color:#e44787;">Questions List</th>
                                                        <th style="background-color:#fff; border-bottom: 1px solid #fff;"></th>
                                                    </tr>
                                                    <tbody>
                                                        <?php
                                                        foreach ($propQuestListdata as $propQuestList) {
                                                            ?>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr style="border-left: 10px solid <?php echo $color1; ?>;">
                                                                <td><?php echo $propQuestList->question; ?></td>
                                                                <td style="padding-left:50px; text-align:right;">
                                                                    <a href="manage-question.php?id=edit&qid=<?php echo $propQuestList->quesID; ?>&recordID=<?php echo $propQuestList->propertyID; ?>" class="btn btn-xs blue">
                                                                        <i class="fa fa-edit"></i> Edit 
                                                                    </a> &nbsp &nbsp &nbsp &nbsp 
                                                                    <a href="manage-question.php?id=delete&quesID1=<?php echo $propQuestList->quesID; ?>&recordID=<?php echo $propQuestList->propertyID; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-xs red">
                                                                        <i class="fa fa-trash"></i> Delete
                                                                    </a></span>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $cnt++;
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                            <?php } else { ?>
                                                <h3>No Record Found.....</h3>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>  
            <?php
        }
        ?>      
       <?php echo ajaxJsFooter(); ?>
    </body>
</html>