<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$staticPageCont = new staticPageData();
$adminDetials = new analyticsPage();
$admin_details = $adminDetials->getUsersData($customerId);
$user = $admin_details[0];
$base_url = $user->website;
?>
<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <?php
            adminCss();
            ?>
        </head>
        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE STATIC PAGES
                                    <small>View and manage your static pages</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="manage-static-pages.php">Static Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Static Pages</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <a class="pull-right">
                                                <a href="ajax_add-static-page.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;"><i class="fa fa-plus"></i> &nbsp Add Page</button></a>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> Manage Static Pages </span>
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                            <span></span>
                                                        </label>
                                                    </th>
                                                    <th> S. No. </th>
                                                    <th> Added Static Page </th>
                                                    <th> Static Page URL </th>
                                                    <th> Actions </th>
                                                    <th> Status </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $staticPageLists = $staticPageCont->getStaticPageListing($customerId);

                                                $count = 1;
                                                foreach ($staticPageLists as $staticPageData) {
                                                    $pageStat = $staticPageData->status;
                                                    $cmspage = $staticPageData->cms_page;
                                                    $pageurl = $staticPageData->page_url;
                                                    $s_no = $staticPageData->s_no;
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td>
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                <input type="checkbox" class="checkboxes" value="1" />
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td> <?php echo $count; ?> </td>
                                                        <td>
                                                            <font style="color:#2196f3;"> <?php echo $staticPageData->page_name; ?> </font>
                                                        </td>
                                                        <td>
                                                            <font style="color:#ff5722;"><?php echo $staticPageData->page_url; ?></font>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if ($pageStat == "Y") {
                                                                ?>
                                                                <a href="create_static_page.php?deactivate=<?php echo $s_no; ?>" class="btn btn-xs blue">Deactivate</a>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <a href="create_static_page.php?activate=<?php echo $s_no; ?>" class="btn btn-xs blue">Activate</a>
                                                                <?php
                                                            }
                                                            ?>
                                                            <!--<div class="btn-group">
                                                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                            <i class="fa fa-angle-down"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-left" role="menu">
                                                                            <li>
                                                                                    <a href="javascript:;">
                                                                                            <i class="icon-docs"></i> New Post </a>
                                                                            </li>
                                                                            <li>
                                                                                    <a href="javascript:;">
                                                                                            <i class="icon-tag"></i> New Comment </a>
                                                                            </li>
                                                                            <li>
                                                                                    <a href="javascript:;">
                                                                                            <i class="icon-user"></i> New User </a>
                                                                            </li>
                                                                            <li class="divider"> </li>
                                                                            <li>
                                                                                    <a href="javascript:;">
                                                                                            <i class="icon-flag"></i> Comments
                                                                                            <span class="badge badge-success">4</span>
                                                                                    </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>-->
                                                            <a href="ajax_<?php echo $cmspage ?>.php?pageUrlData=<?php echo $pageurl; ?>" onclick="load_static_edit('<?php echo $cmspage; ?>~<?php echo $pageurl; ?>');" class="btn btn-xs green">Edit</a>
                                                            <a href="<?php echo $base_url.'/'. $pageurl; ?>.html"  class="btn btn-xs blue-soft" target="_blank">Preview</a>
                                                        </td>
                                                         <td>
                                                            <?php
                                                            if ($pageStat == "Y") {
                                                                ?>
                                                             <span style="color:green;"> Activate</span>
                                                                <?php
                                                            } else {
                                                                ?>
                                                             <span style="color:red;">Deactivate</span>
                                                                <?php
                                                            }
                                                            ?>
                                                          
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $count++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        <script>
            function load_add_static_page() {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_add-static-page.php',
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>

        <script>
            function load_static_edit(inoundData) {
                var pageData = inoundData.split("~");
                var pageName = pageData[0];
                var pageUrl = pageData[1];           
                var pageLocation = 'ajax_lib/ajax_' + pageName + '.php';
                // alert(pageLocation);
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: pageLocation,
                    data: {pageUrlData: pageUrl},
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>

        <?php
        admin_footer();
        ?>
    </html>
</div>
