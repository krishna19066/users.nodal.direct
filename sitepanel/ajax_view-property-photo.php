<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
 if($customerId){ 
$propertyCont = new propertyData();
$res = $cityListings1[0];
//$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$bucket = $cloud_keyData->bucket;
$cloud_cdnName = $cloud_keyData->cloud_name;

 $propertyId = base64_decode($_REQUEST['propID']);

//$propertyId = $propID;
//$connectCloudinary = $propertyCont->connectCloudinaryAccount($customerId);
$property_data = $propertyCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
$img_seq=$property_res->img_seq;


//-------------Upload Photo For Property------------------------>
if (isset($_POST['submit_pic_aws'])) {
    $cloud_keySel = $propertyinsert->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    $record_type = "Property Photo";
    $script_name = "view-property-photo.php";
    $table_name = "propertyImages";

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $img_seq=$_POST['img_seq'];
    $pro_id = $_POST['recordID'];
    $propId = base64_encode($pro_id);
    $pro_type = "property";
    $type = "home";
    $file = $_FILES['image_org']['name'];
    $alt_tag = $_POST['imageAlt1'];
//echo $alt_tag."<br>";
    $roo_id = "0";
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;
    if ($customerId == 1) {
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "Property"));
    } else {
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/Property"));
    }


    //  $ins = "insert into propertyImages(imageURL,imageAlt,imageType,roomID,propertyID,type) values('$timestmp','$alt_tag','$pro_type','$roo_id','$pro_id','$type')";
    //$quer = mysql_query($ins);
    $propertyImg = $propertyinsert->AddPropertyPhoto($timestmp, $alt_tag, $pro_type, $roo_id, $pro_id, $type,$img_seq);
    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "ajax_view-property-photo.php?propID=' . $propId . '";
            </script>';
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
	<script type='text/javascript'>
					function ValidateSize(file) {
						var FileSize = file.files[0].size / 1024 / 1024; // in MB
						if (FileSize > 2) {
							alert("Image size exceeds 2 MB! We can't accept! please choose less than 2 MB");
							// var file = $(file).val(''); //for clearing with Jquery
							var file = document.getElementById('file').value = '';
						} else {

						}
					}
            </script>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
		
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                       <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> PROPERTY - <span style="color:#e44787;"><?php echo $propertyName; ?></span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View / Add Property Photo</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body ">
                                    <div style="width:100%;" class="clear"> 
                                        <?php if($bucket){ ?>
                                        <span ><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:200px;font-size:14px;" data-html="true"  onclick="$('#add_photo_aws').slideToggle();$('#add_photo').hide();" value="Add Property Photo on AWS" /></span>
                                            <?php } else{?>
                                        <span ><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:240px;font-size:14px;margin-left: 15px;" data-html="true"  onclick="$('#add_photo_aws').hide();$('#add_photo').slideToggle();" value="Add Property Photo on Cloudinary" /></span>
                                        <?php } ?>
                                        <span ><a href="img-sequence.php?propertyId=<?php echo $propertyId; ?>"><input type="button" style="background:#363bd3;color:white;border:none;height:35px;width:180px;font-size:14px; margin-left: 18px;" data-html="true" onclick="load_ReorderpropertyPhoto(<?php echo $propertyId; ?>);" value="Reorder Property Photo" /></a></span>
                                       <span style="float:right;"> <a href="set_cover_photo.php?propID=<?php echo $propertyId; ?>"><button style="background:#ff5722;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;">Set Cover Photo</button></a></span>
                                        <!--<span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Video Gallery </button></a></span>-->

                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <?php //if($img_seq=='aws'){ ?>
                    
                     <!---------------------------------------------------------------- Add Photo Section Starts on AWS  --------------------------------------------------------------------->
                    <div class="portlet light bordered" id="add_photo_aws" style="display:none;">			
                        <div class="portlet-body form">
                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="S3-createObject.php">
                                <div class="form-body">
                                    <h4 align="center" style="color:#E26A6A;"><b> Add New Property Photo On AWS </b></h3><br><br>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Property Image
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                    <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="image_org" id="file" onchange="ValidateSize(this)"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    <span style="font-size:12px; color: red;">Note:image size should be less than 2MB </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="form-group">
                                                <label class="control-label col-md-3"> Property Images- Original Image
                                                        <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                        <input name="image_org" type="file" class="form-control" />
                                                        <span class="help-block"> Provide your original image</span>
                                                </div>
                                        </div>-->

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Image Alt Tag
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input name="imageAlt1" type="text" class="form-control"/>
                                                <span class="help-block"> Provide your image alt tag</span>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input name="recordID" type="hidden" value="<?php echo $propertyId ?>" />
                                                    <input name="action_register" type="hidden" value="s3propertyimg" />
                                                    <input type="submit" name="submit_pic" class="btn green" id="submit" value="Add Photo">
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </form>
                        </div>
                    </div><?php// } else{ ?>
                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                    
                    <!---------------------------------------------------------------- Add Photo Section Starts Cloudinary  --------------------------------------------------------------------->
                    <div class="portlet light bordered" id="add_photo" style="display:none;">			
                        <div class="portlet-body form">
                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php">
                                <div class="form-body">
                                    <h4 align="center" style="color:#E26A6A;"><b> Add New Property Photo On Cloudinary </b></h3><br><br>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Property Image
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                    <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="image_org" id="file" onchange="ValidateSize(this)"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    <span style="font-size:12px; color: red;">Note:image size should be less than 2MB </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="form-group">
                                                <label class="control-label col-md-3"> Property Images- Original Image
                                                        <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                        <input name="image_org" type="file" class="form-control" />
                                                        <span class="help-block"> Provide your original image</span>
                                                </div>
                                        </div>-->

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Image Alt Tag
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input name="imageAlt1" type="text" class="form-control"/>
                                                <span class="help-block"> Provide your image alt tag</span>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input name="recordID" type="hidden" value="<?php echo $propertyId ?>" />
                                                   
                                                    <input type="submit" name="submit_pic" class="btn green" id="submit" value="Add Photo">
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <?php //} ?>
                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                    
                    
                    
                     <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row">
                                            <?php
                                            if($bucket){
                                                $propPhotoListdata = $propertyCont->S3getPropertyPhoto($propertyId);
                                            }
                                            else{
                                                $propPhotoListdata = $propertyCont->getPropertyPhoto($propertyId);
                                            }
                                            if ($propPhotoListdata != NULL) {
                                                foreach ($propPhotoListdata as $propPhotoList) {
                                                    ?>
                                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                                        <div class=" mt-overlay-1">
                                                            <?php if($bucket){ ?>
                                                                <img src="//<?php echo $bucket; ?>.s3.amazonaws.com/<?= $propPhotoList->imageURL; ?><?php if($customerId === '1'){ ?>.jpg <?php } ?>" style="height:200px;" />
                                                                <?php } else{ ?>
                                                            <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/<?php if ($customerId != 1) { ?>reputize<?php } ?>/property/<?php echo $propPhotoList->imageURL; ?>.jpg" style="height:200px;" />
                                                            
                                                            <?php } ?>
                                                            <div class="mt-overlay">
                                                                <ul class="mt-info">
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="javascript:;">
                                                                            <i class="icon-magnifier"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="../sitepanel/add_property.php?action=delete&imagesID=<?php echo $propPhotoList->imagesID; ?>">
                                                                            <i class="icon-trash"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <?php
                                                                if ($propPhotoList->featureStat == 'Y') {
                                                                    ?>
                                                                    <center><div>
                                                                             <a href="add_property.php?featured=featured-removed&imagesID1=<?= $propPhotoList->imagesID ?>&recordID=<?= $propPhotoList->propertyID; ?>&roomID=<?= $propPhotoList->roomID; ?>"><input type="button" name="feature" class="btn red" value="Featured" /></a>
                                                                            <!-- <input type="button" name="feature" class="btn red" value="Featured" /></a>-->
                                                                        </div></center>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <center><div>
                                                                            <a href="add_property.php?id=featured&imagesID1=<?= $propPhotoList->imagesID ?>&recordID=<?= $propPhotoList->propertyID; ?>&roomID=<?= $propPhotoList->roomID; ?>"><input type="button" name="feature" class="btn green" value="Set Featured" /></a>
                                                                        </div></center>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <center> <h3>No Property Photo Found.....</h3></center>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
 <?php } ?>
<?php echo ajaxJsFooter(); ?>
</body>
</html>