<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$menuCont = new menuNavigationPage();
$getlanguage = $menuCont->GetLanguageFooterData($customerId);
$getPropertyUrl = $menuCont->getPropertyUrls($customerId);
$getStaticUrl = $menuCont->getStaticPageUrls($customerId);
$getCityUrl = $menuCont->getCityUrls($customerId);
@extract($_REQUEST);
//admin_header();
if (isset($pri_menu)) {
    $res = $pri_menu;
    //$getHeaderWithLink = $menuCont->GetHeaderFooterWithPriNameForFooter($customerId, $pri_menu);
    //print_r($getHeaderWithLink);
    $menu = 'primary';
  
}
if (isset($menuname)) {
    $res = $menuname;
    $id;
     $menu = 'secondary';
  
}

if(isset($_POST['update_primary_menu'])){
   $footer_menu = $_POST['footer_menu'];
   $old_menu = $_POST['old_menu'];
   $upd = $menuCont->UpdateFooterMenu($customerId,$footer_menu,$old_menu);
     echo '<script type="text/javascript">
                alert("Succesfuly Update data");              
window.location = "manage_footer.php";
            </script>';
    exit;
}

if(isset($_POST['update_secondary_menu'])){
    $footer_menu = $_POST['footer_menu'];
    $menu_id = $_POST['menu_id'];
    
    $upd = $menuCont->UpdateFooterMenuWithID($customerId,$footer_menu,$menu_id);
     echo '<script type="text/javascript">
                alert("Succesfuly Update data");              
window.location = "manage_footer.php";
            </script>';
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <?php
//include "include/config.php"; 
//$con = mysqli_connect("localhost","root","","perch") or die("no connection");
        error_reporting(E_ERROR);
        ?>
        <style>
            .sub-table tr:nth-child(odd){
                background-color:#fff;
            }


            td {
                padding:10px;}

            .list :hover{
                cursor: pointer;
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" />

        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <!--------------------------------------------------------------------------------------- Top Section Mand. ------------------------------------------------------------------------------>
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div  class="col-md-12">

                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Manage Your Footer </span>
                                            <span class="caption-helper">Manage Your Footer details here..</span>
                                        </div>
                                    </div>
                                    <span style="float:right;"> <a href="manage_heder.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Header</button></a></span><br><br>
                                            <br><br>                                                 
                                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Footer Menu
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input name="footer_menu" type="text" class="form-control" value="<?php echo html_entity_decode($res); ?>" required="" />
                                                                    <span class="help-block"> Provide Footer Menu</span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <?php if ($menu == 'primary') { ?>
                                                                        <input name="update_primary_menu" type="hidden" value="save" />
                                                                         <input name="old_menu" type="hidden" value="<?php echo $res; ?>" />
                                                                    <?php } else { ?>

                                                                         <input name="update_secondary_menu" type="hidden" value="save" />
                                                                         <input name="menu_id" type="hidden" value="<?php echo $id; ?>" />
                                                                    <?php } ?>
                                                                    <input type="submit" class="btn green" name="update_testimonial" class="button" id="submit"  value="Update" />

                                                                    <a href="manage_footer.php" class="btn red">Go Back</a>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        </div>

                                                    </form>

                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </body>
                                                    </html>

