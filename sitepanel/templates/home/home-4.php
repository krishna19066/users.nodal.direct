<!--------------------This is one blackswanresorts single (ReactJs)  Template Home page CMS ---------------------------->
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Edit Home Page Section </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>                                          
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <ul class="nav nav-tabs tabs-left">
                    <li class="active">
                        <a href="#tab_6_1" data-toggle="tab" aria-expanded="true"> Video Slider</a>
                    </li>
                    <li class="">
                        <a href="#tab_6_2" data-toggle="tab" aria-expanded="false">Welcome</a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Feature
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#tab_feature_1" tabindex="-1" data-toggle="tab"> Feature 1 </a>
                            </li>
                            <li>
                                <a href="#tab_feature_2" tabindex="-1" data-toggle="tab"> Feature 2 </a>
                            </li>
                            <li>
                                <a href="#tab_feature_3" tabindex="-1" data-toggle="tab"> Feature 3 </a>
                            </li> 
                            <li>
                                <a href="#tab_feature_4" tabindex="-1" data-toggle="tab"> Feature 4 </a>
                            </li>
                             
                        </ul>
                    </li>
                    <li class="">
                        <a href="#tab_property" data-toggle="tab" aria-expanded="false">Property</a>
                    </li>
                     <li class="">
                        <a href="#tab_service" data-toggle="tab" aria-expanded="false">Service</a>
                    </li>
                    <!--<li class="">
                        <a href="#tab_video" data-toggle="tab" aria-expanded="false"> Video & Content </a>
                    </li>-->
                    <li class="">
                        <a href="#tab_review" data-toggle="tab" aria-expanded="false"> Review </a>
                    </li>
                    <!--<li class="">
                        <a href="#tab_award" data-toggle="tab" aria-expanded="false"> Award </a>
                    </li>
                     <li class="">
                        <a href="#tab_google_map" data-toggle="tab" aria-expanded="false"> Google Map </a>
                    </li>-->
                    <li class="">
                        <a href="#tab_instagram" data-toggle="tab" aria-expanded="false">Instagram </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <form  method="post" enctype="multipart/form-data" action="updatePage_content.php" class="form-horizontal" onsubmit="return validate_register_member_form();">
                    <div class="tab-content">                                              
                        <div class="tab-pane active in" id="tab_6_1">
                            <div class="section" style="float: right; width:117px;height:43px;">
                                <?php
                                $section = 'Slider';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Slider&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" >Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Slider&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" >Show Section</button></a><br/>
                                <?php } ?>
                            </div>
                             <div class="portlet-body">                                    
                                    <div class="tabbable-custom nav-justified">
                                        <ul class="nav nav-tabs nav-justified">
                                            
                                            <li class="active">
                                                <a href="#tab1_image_slider" data-toggle="tab" aria-expanded="true">Image Slider</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab2_video_slider" data-toggle="tab" aria-expanded="false">Video Slider</a>
                                            </li>                                                                                    
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1_image_slider">                                             
                                              <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
                                <div class="w3-content" style="max-width:1200px">
                                    <?php
                                    $ctn = 1;
                                    foreach ($getHomeSlider as $dataRes) {
                                        ?>
                                        <img class="mySlides" src="<?php if($bucket){?> https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php  echo $dataRes->hsimg; ?> <?php } else{ ?> http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/c_fill/<?php if ($customerId != 1) { ?>reputize/<?php } ?>homepage-slider/<?php echo $dataRes->hsimg; ?>.jpg<?php } ?> " style="width:100%;height: 400px;<?php if ($ctn != 1) { ?> display:none<?php } ?>">
                                        <?php
                                        $ctn++;
                                    }
                                    ?>
                                    <div class="w3-row-padding w3-section"><br/>
                                        <?php
                                        $ctn = 1;
                                        foreach ($getHomeSlider as $dataRes) {
                                            ?>
                                            <div class="w3-col s2">
                                                <img class="demo w3-opacity w3-hover-opacity-off" src="<?php if($bucket){?> https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $dataRes->hsimg; ?> <?php } else{ ?> http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/w_323,h_150,c_fill/<?php if ($customerId != 1) { ?>reputize/<?php } ?>homepage-slider/<?php echo $dataRes->hsimg; ?>.jpg<?php } ?> " style="width:100%;cursor:pointer;  <?php if ($ctn == 6) { ?> margin-top: 13px; <?php } ?>" onclick="currentDiv(<?php echo $ctn; ?>)">
                                            </div>
                                            <?php
                                            $ctn++;
                                        }
                                        ?>
                                        <div class="w3-col s2">
                                            <a href="manage-slider.php"> <img class="demo w3-opacity w3-hover-opacity-off" src="https://i.ibb.co/4pdnS6F/add.png" style="width:80px; height:80px;cursor:pointer;  <?php if ($ctn == 6) { ?> margin-top: 13px; <?php } ?>" onclick="currentDiv(<?php echo $ctn; ?>)" title="Add More Image"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2_video_slider">
                            <div class="form-group">
                                <label class="control-label col-md-3">Video Url
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="slider_video_url" value="<?php echo $pageData->slider_video_url; ?>"/>

                                    <span class="help-block"> Provide your Video URL </span>
                                </div>
                             </div>
                                <div class="video-main-div webview" style="height: 500px;">
                                    <div><video autoplay="" loop="">
                                            <source src="<?php echo $pageData->slider_video_url; ?>" type="video/mp4">Your browser does not support the video tag.</video>
                                    </div>                                   
                                </div>                                                                   
                            </div>
                    </div>
                </div>                                
            </div>                                                     
                        </div>
                        <div class="tab-pane fade" id="tab_6_2">
                              <div class="section" style="float: right; width:117px;height:30px;">
                            <?php
                              $section = 'Welcome';
                              $page = 'Home';
                              $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                              $page_sec = $page_sec1[0];
                              $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Welcome&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" style="margin-top: -10px;">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Welcome&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" style="margin-top: -10px;">Show Section</button></a><br/>
                                <?php } ?>
                                 
                            </div><br/>
                            <div class="form-group">
                                <label class="control-label col-md-3">H1 Tag Name
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="h1tag" value="<?php echo $pageData->h1tag; ?>" required />

                                    <span class="help-block"> Provide your H1 Tag Name </span>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3">H1 Content 
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="H1Content" rows="6"> <?php echo $pageData->H1Content; ?></textarea>
                                    <span class="help-block"> Provide your H1 Content </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_feature_1">
                            <div class="section" style="float: right; width:117px;height:43px;">
                                <?php
                                $section = 'Feature';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Feature&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" >Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Feature&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" >Show Section</button></a><br/>
                                <?php } ?>
                    
                            </div>
                            <table width="100%">
                                <td><hr /></td>
                                <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;">Feature - 1</td>
                                <td><hr /></td>
                            </table>
                            <div class="form-group">
                                <label class="control-label col-md-3"> Icon 1 Heading 
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead1" value="<?php echo $pageData->subhead1; ?>"  />
                                    <span class="help-block"> Provide your Icon box 1 Heading  </span>
                                </div>
                            </div>

                            <div class="form-group last">
                                <label class="control-label col-md-3">Sub Heading icon box1 Content 
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="subheadcontent1" rows="6" cols="70"> <?php echo $pageData->subheadcontent1; ?></textarea>
                                    <span class="help-block"> Provide your Sub Heading icon box1 Content </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_feature_2">
                            <table width="100%">
                                <td><hr /></td>
                                <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;">Feature - 2</td>
                                <td><hr /></td>
                            </table>
                            <div class="form-group">
                                <label class="control-label col-md-3">Heading icon box2
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead2" value="<?php echo $pageData->subhead2; ?>"  />
                                    <span class="help-block"> Provide your icon box2 Heading </span>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3">Sub Heading icon box2 Content 
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="subheadcontent2" rows="6" cols="70"><?php echo $pageData->subheadcontent2; ?></textarea>
                                    <span class="help-block"> Provide your Sub Heading icon box2 Content </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_feature_3">
                            <table width="100%">
                                <td><hr /></td>
                                <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;">Feature - 3</td>
                                <td><hr /></td>
                            </table>
                            <div class="form-group">
                                <label class="control-label col-md-3"> Heading icon box3
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead3" value="<?php echo $pageData->subhead3; ?>"  />
                                    <span class="help-block"> Provide your icon box3 Heading </span>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3">Sub Heading icon box3 Content
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="subheadcontent3" rows="6" cols="70"><?php echo $pageData->subheadcontent3; ?></textarea>
                                    <span class="help-block"> Provide your Sub Heading icon box3 Content </span>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="tab_feature_4">
                            <table width="100%">
                                <td><hr /></td>
                                <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;">Feature - 4</td>
                                <td><hr /></td>
                            </table>
                            <div class="form-group">
                                <label class="control-label col-md-3"> Heading icon box4
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead4" value="<?php echo $pageData->subhead4; ?>"  />
                                    <span class="help-block"> Provide your icon box4 Heading</span>
                                </div>
                            </div>

                            <div class="form-group last">
                                <label class="control-label col-md-3">Sub Heading icon box4 Content
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="subheadcontent4" rows="6" cols="70"><?php echo $pageData->subheadcontent4; ?></textarea>
                                    <span class="help-block"> Provide your Sub Heading icon box4 Content </span>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="tab_property">
                             <div class="section" style="float: right; width:117px;height:29px;">
                                 <?php
                                $section = 'Property';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Property&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" style="margin-top: -10px;">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Property&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" style="margin-top: -10px;">Show Section</button></a><br/>
                                <?php } ?>       
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Main Heading
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="aptgalleryhead" value="<?php echo $pageData->aptgalleryhead; ?>"  />
                                    <span class="help-block"> Provide your main Heading </span>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3">Sub Heading
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="aptgalleryheadcontent" rows="6" cols="70"><?php echo $pageData->aptgalleryheadcontent; ?></textarea>
                                    <span class="help-block"> Provide your Sub Heading</span>
                                </div>
                            </div>                                            
                        </div>
                        <div class="tab-pane fade" id="tab_service">
                             <div class="section" style="float: right; width:117px;height:29px;">
                                <?php
                                $section = 'Service';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Service&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" style="margin-top: -10px;">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Service&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" style="margin-top: -10px;">Show Section</button></a><br/>
                                <?php } ?>                                
                            </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Main Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="splheadmain" value="<?php echo $pageData->splheadmain; ?>"  />
                                        <span class="help-block"> Provide your Service Heading </span>
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <label class="control-label col-md-3"> Sub Heading
                                        <span class="required"> * </span>
                                    </label>
                                   <div class="col-md-9">
                                         <input type="text" class="form-control" name="splheadmaincontent" value="<?php echo $pageData->splheadmaincontent; ?>"  />                                    
                                        <span class="help-block"> Provide your sub Heading </span>
                                    </div>
                                </div> <br/>
                            
                             <div class="portlet-body">                                   
                                <div class="tabbable-custom nav-justified">
                                        <ul class="nav nav-tabs nav-justified">                                            
                                            <li class="active">
                                                <a href="#tab_Service_1" data-toggle="tab" aria-expanded="true">Service-1</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_Service_2" data-toggle="tab" aria-expanded="false">Service-2</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab_Service_3" data-toggle="tab" aria-expanded="false">Service-3</a>
                                            </li>                                           
                                        </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_Service_1">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Service 1 Image
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <?php if ($pageData->image2 == NULL) { ?>
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                    <?php } else { ?>
                                                        <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/team_members/<?php echo $pageData->image2; ?>.jpg" alt=""/>
                                                    <?php } ?>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="image2"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                                <!--Uploaded Image :- <a href="http://www.stayondiscount.com/images/<?php echo $pageData->image2; ?>" target="_blank"><?php echo $pageData->image2; ?></a>-->
                                            </div>
                                         </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Service 1 Link
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <select name="spllinkcorporate" class="form-control">
                                                    <option value="index" <?php
                                                    if ($pageData->spllinkcorporate == "index") {
                                                        echo "selected";
                                                    }
                                                    ?>>index</option>

                                                    <!--------------------------------------------- Service Apartment URL ----------------------------------------------->
                                                    <?php
                                                    /*
                                                      $opt_sel = "select cityUrl from city_url where customerID='$custID' and status='Y' and cityUrl!=''";
                                                      $opt_quer = mysql_query($opt_sel);
                                                      while ($opt_row = mysql_fetch_array($opt_quer)) {
                                                      ?>
                                                      <option value="<?php echo $opt_row['cityUrl']; ?>" <?php if ($res['spllinkcorporate'] == $opt_row['cityUrl']) {
                                                      echo "selected";
                                                      } ?>><?php echo $opt_row['cityUrl']; ?></option>
                                                      <?php
                                                      }
                                                      ?>


                                                      <!---------------------------------------------- Static Pages URL ---------------------------------------------->
                                                      <?php
                                                      $opt_sel1 = "select page_url from static_pages where customerID='$custID' and status='Y' and type='desktop' and page_url!=''";
                                                      $opt_quer1 = mysql_query($opt_sel1);
                                                      while ($opt_row1 = mysql_fetch_array($opt_quer1)) {
                                                      ?>
                                                      <option value="<?php echo $opt_row1['page_url']; ?>" <?php if ($res['spllinkcorporate'] == $opt_row1['page_url']) {
                                                      echo "selected";
                                                      } ?>><?php echo $opt_row1['page_url']; ?></option>
                                                      <?php
                                                      }
                                                      ?>

                                                      <!---------------------------------------------- Property Pages URL ---------------------------------------------->
                                                      <?php
                                                      $opt_sel2 = "select propertyURL from propertyTable where customerID='$custID' and status='Y' and propertyURL!=''";
                                                      $opt_quer2 = mysql_query($opt_sel2);
                                                      while ($opt_row2 = mysql_fetch_array($opt_quer2)) {
                                                      ?>
                                                      <option value="<?php echo $opt_row2['propertyURL']; ?>" <?php if ($res['spllinkcorporate'] == $opt_row2['propertyURL']) {
                                                      echo "selected";
                                                      } ?>><?php echo $opt_row2['propertyURL']; ?></option>
                                                      <?php
                                                      } */
                                                    ?>
                                                </select>
                                                <span class="help-block"> Provide your Link Here</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Service 1 Heading 
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="splheadcorporate" value="<?php echo $pageData->splheadcorporate; ?>"  />
                                                <span class="help-block"> Provide your  Heading </span>
                                            </div>
                                        </div>
                                        <div class="form-group last">
                                            <label class="control-label col-md-3">Service 1 Content
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <textarea class="ckeditor form-control" name="splheadcorporatecontent" rows="6" cols="70"><?php echo $pageData->splheadcorporatecontent; ?></textarea>
                                                <span class="help-block"> Provide your Content</span>
                                            </div>
                                        </div>                                            
                                    </div>
                            <div class="tab-pane" id="tab_Service_2">
                                <div class="form-group">
                                        <label class="control-label col-md-3"> Service 2 Image
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if ($pageData->image2 == NULL) { ?>
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                <?php } else { ?>
                                                    <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/team_members/<?php echo $pageData->image3; ?>.jpg" alt=""/>
                                                <?php } ?>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="image3"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                           <!-- Uploaded Image :- <a href="http://www.stayondiscount.com/images/<?php echo $pageData->image3; ?>" target="_blank"><?php echo $pageData->image3; ?></a>-->
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Service  2 Link 
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <select name="spllinkjp" class="form-control">
                                                    <option value="index" <?php
                                                    if ($pageData->spllinkjp == "index") {
                                                        echo "selected";
                                                    }
                                                    ?>>index</option>

                                                    <!--------------------------------------------- Service Apartment URL ----------------------------------------------->
                                                    <?php
                                                    /*
                                                      $opt_sel = "select cityUrl from city_url where customerID='$custID' and status='Y' and cityUrl!=''";
                                                      $opt_quer = mysql_query($opt_sel);
                                                      while ($opt_row = mysql_fetch_array($opt_quer)) {
                                                      ?>
                                                      <option value="<?php echo $opt_row['cityUrl']; ?>" <?php if ($res['spllinkjp'] == $opt_row['cityUrl']) {
                                                      echo "selected";
                                                      } ?>><?php echo $opt_row['cityUrl']; ?></option>
                                                      <?php
                                                      }
                                                      ?>


                                                      <!---------------------------------------------- Static Pages URL ---------------------------------------------->
                                                      <?php
                                                      $opt_sel1 = "select page_url from static_pages where customerID='$custID' and status='Y' and type='desktop' and page_url!=''";
                                                      $opt_quer1 = mysql_query($opt_sel1);
                                                      while ($opt_row1 = mysql_fetch_array($opt_quer1)) {
                                                      ?>
                                                      <option value="<?php echo $opt_row1['page_url']; ?>" <?php if ($res['spllinkjp'] == $opt_row1['page_url']) {
                                                      echo "selected";
                                                      } ?>><?php echo $opt_row1['page_url']; ?></option>
                                                      <?php
                                                      }
                                                      ?>

                                                      <!---------------------------------------------- Property Pages URL ---------------------------------------------->
                                                      <?php
                                                      $opt_sel2 = "select propertyURL from propertyTable where customerID='$custID' and status='Y' and propertyURL!=''";
                                                      $opt_quer2 = mysql_query($opt_sel2);
                                                      while ($opt_row2 = mysql_fetch_array($opt_quer2)) {
                                                      ?>
                                                      <option value="<?php echo $opt_row2['propertyURL']; ?>" <?php if ($res['spllinkjp'] == $opt_row2['propertyURL']) {
                                                      echo "selected";
                                                      } ?>><?php echo $opt_row2['propertyURL']; ?></option>
                                                      <?php
                                                      }
                                                     * */
                                                    ?>
                                                </select>
                                                <span class="help-block"> Provide your Link </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Service 2 Heading
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="splheadjp" value="<?php echo $pageData->splheadjp; ?>"  />
                                                <span class="help-block"> Provide your  Heading</span>
                                            </div>
                                        </div>
                                        <div class="form-group last">
                                            <label class="control-label col-md-3">Service 2 Content
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <textarea class="ckeditor form-control" name="splheadjpcontent" rows="6" cols="70"><?php echo $pageData->splheadjpcontent; ?></textarea>
                                                <span class="help-block"> Provide your Service 2 Content</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_Service_3">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Service 3 Image
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <?php if ($pageData->image4 == NULL) { ?>
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                    <?php } else { ?>
                                                        <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/team_members/<?php echo $pageData->image4; ?>.jpg" alt=""/>
                                                    <?php } ?>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="image4"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                               <!-- Uploaded Image :- <a href="http://www.stayondiscount.com/images/<?php echo $pageData->image4; ?>" target="_blank"><?php echo $pageData->image4; ?></a>-->
                                            </div>
                                         </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Service 3 Link 
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select name="spllinkgoa" class="form-control">
                                                            <option value="index" <?php
                                                            if ($pageData->spllinkgoa == "index") {
                                                                echo "selected";
                                                            }
                                                            ?>>index</option>

                                                            <!--------------------------------------------- Service Apartment URL ----------------------------------------------->
                                                            <?php
                                                            /*
                                                              $opt_sel = "select cityUrl from city_url where customerID='$custID' and status='Y' and cityUrl!=''";
                                                              $opt_quer = mysql_query($opt_sel);
                                                              while ($opt_row = mysql_fetch_array($opt_quer)) {
                                                              ?>
                                                              <option value="<?php echo $opt_row['cityUrl']; ?>" <?php if ($res['spllinkgoa'] == $opt_row['cityUrl']) {
                                                              echo "selected";
                                                              } ?>><?php echo $opt_row['cityUrl']; ?></option>
                                                              <?php
                                                              }
                                                              ?>


                                                              <!---------------------------------------------- Static Pages URL ---------------------------------------------->
                                                              <?php
                                                              $opt_sel1 = "select page_url from static_pages where customerID='$custID' and status='Y' and type='desktop' and page_url!=''";
                                                              $opt_quer1 = mysql_query($opt_sel1);
                                                              while ($opt_row1 = mysql_fetch_array($opt_quer1)) {
                                                              ?>
                                                              <option value="<?php echo $opt_row1['page_url']; ?>" <?php if ($res['spllinkgoa'] == $opt_row1['page_url']) {
                                                              echo "selected";
                                                              } ?>><?php echo $opt_row1['page_url']; ?></option>
                                                              <?php
                                                              }
                                                              ?>

                                                              <!---------------------------------------------- Property Pages URL ---------------------------------------------->
                                                              <?php
                                                              $opt_sel2 = "select propertyURL from propertyTable where customerID='$custID' and status='Y' and propertyURL!=''";
                                                              $opt_quer2 = mysql_query($opt_sel2);
                                                              while ($opt_row2 = mysql_fetch_array($opt_quer2)) {
                                                              ?>
                                                              <option value="<?php echo $opt_row2['propertyURL']; ?>" <?php if ($res['spllinkgoa'] == $opt_row2['propertyURL']) {
                                                              echo "selected";
                                                              } ?>><?php echo $opt_row2['propertyURL']; ?></option>
                                                              <?php
                                                              }
                                                             * */
                                                            ?>
                                                        </select>
                                                        <span class="help-block"> Provide your Link Service 3</span>
                                                    </div>                                              
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Service 3 Heading 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="splheadgoa" value="<?php echo $pageData->splheadgoa; ?>"  />
                                                    <span class="help-block"> Provide your Service 3 Sub heading</span>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Service 3 Content
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="splheadgoacontent" rows="6" cols="70"><?php echo $pageData->splheadgoacontent; ?></textarea>
                                                    <span class="help-block"> Provide your  Content</span>
                                                </div>
                                            </div>                                              
                                        </div>
                                    </div>                                                                                        
                                </div>                                                                     
                        </div>
                        </div>

                        <div class="tab-pane fade" id="tab_video">
                            <div class="section" style="float: right; width:117px;height:43px;">
                                <?php
                                $section = 'video_content';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=video_content&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=video_content&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline">Show Section</button></a><br/>
                                <?php } ?>                               
                            </div>

                            <div id="review_cont"><br/>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Top Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="content1" value="<?php echo $pageData->content1; ?>"  />
                                        <span class="help-block"> Provide your Top Heading</span>
                                    </div>
                                </div>
                                
                                <div class="portlet-body">
                                    
                                    <div class="tabbable-custom nav-justified">
                                        <ul class="nav nav-tabs nav-justified">
                                            
                                            <li class="active">
                                                <a href="#tab1_left" data-toggle="tab" aria-expanded="true">Left Side Content</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab2_right" data-toggle="tab" aria-expanded="false">Right Side Content</a>
                                            </li>
                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1_left">
                                             
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Heading
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="awardheading" value="<?php echo $pageData->awardheading; ?>"  />
                                                        <span class="help-block"> Provide your Heading</span>
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Content
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" name="awardcontent" rows="6"> <?php echo $pageData->awardcontent; ?></textarea>
                                                        <span class="help-block"> Provide your Content</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab2_right">
                                                   
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Video heading
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="videoheading" value="<?php echo $pageData->videoheading; ?>"  />
                                                        <span class="help-block"> Provide your Video heading</span>
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Video Content
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" name="videocontent" rows="6"> <?php echo $pageData->videocontent; ?></textarea>
                                                        <span class="help-block"> Provide your Video Content</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Video URL
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="videourl" value="<?php echo $pageData->videourl; ?>"  />
                                                        <span class="help-block"> Provide your embed video URL</span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                   
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_review">
                            <div class="section" style="float: right; width:117px;height:43px;">
                                <?php
                                $section = 'Review';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Review&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Review&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline">Show Section</button></a><br/>
                                <?php } ?>                                 
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Testimonial Div Heading
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="testimonialHead" value="<?php echo $pageData->testimonialHead; ?>"  />
                                    <span class="help-block"> Provide your Testimonial Div Heading</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Testimonial Div Content
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="testimonialContent" value="<?php echo $pageData->testimonialContent; ?>"  />
                                    <span class="help-block"> Provide your Testimonial Div Content</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Testimonial Limit
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" name="testimonialLimit" value="<?php echo $pageData->testimonialLimit; ?>"  />
                                    <span class="help-block"> Provide your Testimonial Limit </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Testimonial Category</label>
                                <div class="col-md-9">
                                    <div class="mt-checkbox-inline">
                                        <?php
                                        //$roomTypeSQL = "select * from roomAmenties";
                                        //$roomTypequery = db_query($roomTypeSQL);
                                        $testCatData = $ajaxclientCont->getTestimonialCategoryList($customerId);
                                        $counter = 0;
                                        $CategoryName = explode(',', $pageData->testimonialCategory);

                                        foreach ($testCatData as $catTypeRes) {
                                            if ($counter == 0) {
                                                ?>

                                                <?php
                                            }
                                            $chk = "";
                                            if (in_array($catTypeRes->category_name, $CategoryName)) {
                                                $chk = 'checked="checked"';
                                            }
                                            ?>
                                            <label class="mt-checkbox">
                                                <input type="checkbox" id="inlineCheckbox21" name="testimonial_cate[]" value="<?= $catTypeRes->category_name; ?>"  <?= $chk; ?>/><?= $catTypeRes->category_name; ?>
                                                <span></span>
                                            </label>
                                            <?php
                                        }
                                        ?>
                                        <span class="help-block">You can select multiple category</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Add More Testimonial
                                </label>
                                <div class="col-md-9">
                                    <a href="manage-testimonial.php"> <img class="demo w3-opacity w3-hover-opacity-off" src="https://i.ibb.co/4pdnS6F/add.png" style="width:50px; height:50px;cursor:pointer;" title="Add More Video Review"></a>
                                </div>
                            </div>
                        </div>                                                   
                        <div class="tab-pane fade" id="tab_award">
                            <div class="form-group">
                                <label class="control-label col-md-3">Award Div Heading
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="awarddivhead" value="<?php echo $pageData->awarddivhead; ?>"  />
                                    <span class="help-block"> Provide your Award Div Heading</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Award Div Content
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                   <!-- <input type="text" class="form-control" name="awarddivcontent" value="<?php //echo $pageData->awarddivcontent;                                   ?>"  />-->
                                    <textarea class="ckeditor form-control" name="awarddivcontent" rows="8"> <?php echo $pageData->awarddivcontent; ?></textarea>
                                    <span class="help-block"> Provide your Award Div Content</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Select Award Image
                                    <span class="required"> * </span>
                                </label>
                                <div style="">
                                    <?php
                                    if ($awardData != null) {
                                        $cnt = 1;
                                        $sl = $start + 1;
                                        foreach ($awardData as $dataRes) {
                                            //$dataRes = ms_htmlentities_decode($dataRes);
                                            if ($cnt = $cnt) {
                                                //$color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                                                $color = '#2A56AC';
                                            }
                                            ?>
                                            <div class="col-md-2">
                                                <!-- BEGIN Portlet PORTLET-->
                                                <div class="portlet box" style="background:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>;">
                                                    <div class="portlet-title">
                                                        <div class="caption">                                                                         
                                                            <?php
                                                            $counter = 0;
                                                            $awardimageID = explode(',', $pageData->awarddivimage);
                                                            if ($counter == 0) {
                                                                ?>

                                                                <?php
                                                            }
                                                            $chk = "";
                                                            if (in_array($dataRes->slno, $awardimageID)) {
                                                                $chk = 'checked="checked"';
                                                            }
                                                            ?>

                                                            <label class="mt-checkbox">
                                                                <input type="checkbox" id="inlineCheckbox21" name="awarddivimage[]" value="<?= $dataRes->slno; ?>"  <?= $chk; ?>/>
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body" style="height:90px;">
                                                        <div class="" data-handle-color="red">
                                                            <img class="img-responsive" src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/c_fill/reputize/awards/<?php echo $dataRes->awardphoto; ?>.jpg" width="100%" style="height: 72px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                            $cnt++;
                                        }
                                        ?>
                                        <?php
                                    } else {
                                        ?>

                                        <center><h4>No Award image found Please add award go to my property side menu.............</h4></center>

                                        <?php
                                    }
                                    ?>

                                </div>                                            
                            </div>                                                   
                        </div>    
                        <div class="tab-pane fade" id="tab_google_map">
                            <div class="section" style="float: right; width:117px;height:29px;">
                                  <?php
                                $section = 'Award';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Award&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" style="margin-top:-10px;">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Award&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" style="margin-top:-10px;">Show Section</button></a><br/>
                                <?php } ?>                                 
                            </div>
                            <div class="portlet-body">                                    
                                    <div class="tabbable-custom nav-justified">
                                        <ul class="nav nav-tabs nav-justified">
                                            
                                            <li class="active">
                                                <a href="#tab1_school" data-toggle="tab" aria-expanded="true">School</a>
                                            </li>
                                            <li class="">
                                                <a href="#tab2_hospital" data-toggle="tab" aria-expanded="false">Hospital</a>
                                            </li>
                                             <li class="">
                                                <a href="#tab3_shopping_mall" data-toggle="tab" aria-expanded="false">Shopping Malls</a>
                                            </li>
                                           
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1_school">                                             
                                               <?php include 'googlemap.php'; ?>
                                                <div id="map-canvas" style="height:365px;  margin-top:25px; border:3px solid #acacac;"></div>
                                            </div>
                                            <div class="tab-pane" id="tab2_hospital">
                                            <?php include 'googlemap.php'; ?>
                                              <div id="hospital" style="height:358px; margin-top:25px; border:3px solid #acacac; "> </div>
                                            </div>
                                            <div class="tab-pane" id="tab3_shopping_mall">
                                             
                                              <?php include 'googlemap.php'; ?>
                                                 <div id="shopping_mall" style="height:358px;margin-top:25px; border:3px solid #acacac;"> </div>
                                            </div>                                           
                                        </div>
                                    </div>                                
                                </div>
                        </div>
                        <div class="tab-pane fade" id="tab_instagram">
                            <div class="section" style="float: right; width:117px;height:29px;">
                                  <?php
                                $section = 'Instagram';
                                $page = 'Home';
                                $page_sec1 = $ajaxclientCont->Getpagesection($customerId,$section,$page);
                                $page_sec = $page_sec1[0];
                                $sec_status = $page_sec->status;                                                              
                                if($sec_status == 'Y'){
                                    ?>
                                  <a href="manage-home-page.php?section=Instagram&type=section&page=Home&status=N"><button type="button" class="btn red btn-outline" style="margin-top:-10px;">Hide Section</button></a><br/>
                                <?php }else{ ?>
                                  <a href="manage-home-page.php?section=Instagram&type=section&page=Home&status=Y"><button type="button" class="btn green btn-outline" style="margin-top:-10px;">Show Section</button></a><br/>
                                <?php } ?>                                 
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Instagram User name
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="instagram_user" value="<?php echo $pageData->instagram_user; ?>"  />
                                    <span class="help-block"> Provide your Instagram User Name</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input name="cust_id" type="hidden" value="<?php echo $customerId; ?>" />
                    <input type="submit" class="btn green" name="Home_Page_Update" class="button" id="submit" value="Submit/Update" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />
                    <input name="action_register" type="hidden" value="update" />                 
                </form>
            </div>
        </div>
    </div>
</div>