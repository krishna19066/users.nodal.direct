<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>City Content </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <ul class="nav nav-tabs tabs-left">
                    <li class="active">
                        <a href="#tab_header_img" data-toggle="tab" aria-expanded="true">Header Image & url</a>
                    </li>                                                      
                    <li class="">
                        <a href="#tab_feature" data-toggle="tab" aria-expanded="false">Feature</a>
                    </li>
                    <li class="">
                        <a href="#tab_service" data-toggle="tab" aria-expanded="false">Service</a>
                    </li>

                    <li class="">
                        <a href="#tab_property" data-toggle="tab" aria-expanded="false">Property</a>
                    </li>
                    <li>
                        <a href="#tab_award_video" data-toggle="tab">Award & Video</a>
                    </li>

                    <li>
                        <a href="#tab_city_content" data-toggle="tab">City Content</a>
                    </li>
                </ul>
            </div>
            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" <?php if($bucket){?>action="../sitepanel/S3-createObject.php" <?php } else{ ?>action="../sitepanel/manage-property-settings.php"<?php } ?> style="display:inline;" onsubmit="return validate_register_member_form();">
                <div class="col-md-9 col-sm-9 col-xs-9">
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_header_img">
                            <div class="form-group">
                                <label class="col-md-3 control-label"> City URL
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-inline input-medium" name="type_url" value="<?php echo $res->cityUrl; ?>" />
                                    <span class="help-inline"> .html </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Header Images
                                    <span class="required"> * </span>
                                </label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <?php if ($res->imageMain == '') { ?>
                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                        <?php } else { ?>
                                            <img src="<?php if($bucket){?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->imageMain; ?> <?php }else{ ?>https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/awards/<?php echo $res->imageMain ?>.jpeg <?php } ?>" alt="">
                                        <?php } ?>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="imageMain"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_feature">
                            <div class="form-group">
                                <label class="control-label col-md-3"> Main Heading H1
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="h1" value="<?php echo $res->h1sa; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?>Feature Heading 1
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead1" value="<?php echo $res->sasubhead1; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Content 1
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subheadcontent1" value="<?php echo $res->sasubheadcontent1; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Heading 2
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead2" value="<?php echo $res->sasubhead2; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Content 2
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subheadcontent2" value="<?php echo $res->sasubheadcontent2; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Heading 3
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead3" value="<?php echo $res->sasubhead3; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Content 3
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subheadcontent3" value="<?php echo $res->sasubheadcontent3; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Heading 4
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subhead4" value="<?php echo $res->sasubhead4; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Feature Content 4
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="subheadcontent4" value="<?php echo $res->sasubheadcontent4; ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_service">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Service 1
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="mainhead1" value="<?php echo $res->samainhead1; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Service 2
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="mainhead2" value="<?php echo $res->samainhead2; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= $text; ?> Service 3
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="mainhead3" value="<?php echo $res->samainhead3; ?>"  />
                                </div>
                            </div>
							
							 <div class="portlet-body">                                   
                                <div class="tabbable-custom nav-justified">
                                    <ul class="nav nav-tabs nav-justified">                                            
                                        <li class="active">
                                            <a href="#tab_Service_1" data-toggle="tab" aria-expanded="true">Service-1</a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_Service_2" data-toggle="tab" aria-expanded="false">Service-2</a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_Service_3" data-toggle="tab" aria-expanded="false">Service-3</a>
                                        </li>                                           
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_Service_1">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Service 1 Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res->service_img1 == NULL) { ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                        <?php } else { ?>
                                                            <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->service_img1; ?> <?php }else{ ?> https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/awards/<?php echo $res->service_img1; ?>.jpg<?php } ?>" alt=""/>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="service_img1"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Service 1 Heading 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="service_head1" value="<?php echo $res->service_head1; ?>"  />
                                                    <span class="help-block"> Provide your  Heading </span>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Service 1 Content
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="service_content1" rows="6" cols="70"><?php echo $res->service_content1; ?></textarea>
                                                    <span class="help-block"> Provide your Content</span>
                                                </div>
                                            </div>                                            
                                        </div>
                                        <div class="tab-pane" id="tab_Service_2">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Service 2 Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res->service_img2 == NULL) { ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                        <?php } else { ?>
                                                            <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->service_img2; ?> <?php }else{ ?> https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/awards/<?php echo $res->service_img2; ?>.jpg<?php } ?> " alt=""/>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="service_img2"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Service 2 Heading
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="service_head2" value="<?php echo $res->service_head2; ?>"  />
                                                    <span class="help-block"> Provide your  Heading</span>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Service 2 Content
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="service_content2" rows="6" cols="70"><?php echo $res->service_content2; ?></textarea>
                                                    <span class="help-block"> Provide your Service 2 Content</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_Service_3">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Service 3 Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res->service_img3 == NULL) { ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                        <?php } else { ?>
                                                            <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->service_img3; ?> <?php }else{ ?> https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/awards/<?php echo $res->service_img3; ?>.jpg<?php } ?>" alt=""/>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="service_img3"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Service 3 Heading 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="service_head3" value="<?php echo $res->service_head3; ?>"  />
                                                    <span class="help-block"> Provide your Service 3 Sub heading</span>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Service 3 Content
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="service_content3" rows="6" cols="70"><?php echo $res->service_content3; ?></textarea>
                                                    <span class="help-block"> Provide your  Content</span>
                                                </div>
                                            </div>                                              
                                        </div>
									</div>
								</div>
							</div>
							</div>

                        <div class="tab-pane fade" id="tab_property">
                            <div class="form-group">
                                <label class="control-label col-md-3">Heading H2
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="h2" value="<?php echo $res->h2sa; ?>"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Sub Heading
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="h2content" value="<?php echo $res->h2sacontent; ?>"  />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_award_video">
                            <div class="form-group">
                                <label class="control-label col-md-3">Main Heading 4
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="h3" value="<?php echo $res->h3sa; ?>" />
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="tabbable-custom nav-justified">
                                    <ul class="nav nav-tabs nav-justified">
                                        <li class="active">
                                            <a href="#tab1_left" data-toggle="tab" aria-expanded="true">Left Side Award</a>
                                        </li>
                                        <li class="">
                                            <a href="#tab2_right" data-toggle="tab" aria-expanded="false">Right Side Video</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab1_left">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Award Heading
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="awardhead" value="<?php echo $res->saawardhead; ?>"  />
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Award Content</label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="awardcontent" rows="6"> <?php echo $res->saawardcontent; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Image1
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res->saimage1 == '') { ?>
                                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                        <?php } else { ?>
                                                            <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->saimage1; ?>.jpg <?php }else{ ?> https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/awards/<?php echo $res->saimage1 ?>.jpeg<?php } ?>" alt="">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image1"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Image 1 Content</label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="saimg1content" rows="6"> <?php echo $res->saimage1Name; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Image2
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res->saimage2 == '') { ?>
                                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                        <?php } else { ?>
                                                            <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->saimage2; ?>.jpg <?php }else{ ?> https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/awards/<?php echo $res->saimage2 ?>.jpeg<?php } ?>" alt="">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image2"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Image 2 Content</label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="saimg2content" rows="6"> <?php echo $res->saimage2Name; ?></textarea>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Image3
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res->saimage3 == '') { ?>
                                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                        <?php } else { ?>
                                                            <img src="<?php if($bucket){ ?>https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->saimage3; ?>.jpg <?php }else{ ?> https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/awards/<?php echo $res->saimage3 ?>.jpeg<?php } ?>" alt="">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image3"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Image 3 Content</label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="saimg3content" rows="6"> <?php echo $res->saimage3Name; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2_right">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $text; ?> Video Heading 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="videohead" value="<?php echo $res->savideohead; ?>"  />
                                                </div>
                                            </div> 
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Video Content</label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="videocontent" rows="6"> <?php echo $res->savideocontent; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= $text; ?>  Video URL
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="videourl" value="<?php echo $res->savideourl; ?>"  />
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_city_content">
                            <div class="form-group">
                                <label class="control-label col-md-3"> City Heading
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="contentheading" value="<?php echo $res->sacontentheading; ?>"  />
                                </div>
                            </div> 
                            <div class="form-group last">
                                <label class="control-label col-md-3">City Content</label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="content" rows="6"> <?php echo $res->sacontent; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <?php
                            if ($res->cityID != '') {
                                ?>
                                <input type="hidden" class="form-control" name="db_filename" value="<?php echo $mets_res->filename; ?>" />
                                <input type="submit" class="btn green" name="update_city_content"  class="button" value="Update <?= $record_type; ?>" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />
                                <input type="hidden" name="action" value="update_city_content">
                                <input type="hidden" name="city_id" value="<?= $res->cityID; ?>">
                                <input type="hidden" name="propertyCounter" value="<?= $res->propertyCounter; ?>">
                                <?php
                            }
                            ?>
                            <a href="manage-property-settings.php"><input type="button" name="back" class="btn red" value="Back" /></a>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>