<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
?>
<?php
    $pageUrl = $_POST['pageUrlData'];
    $clientPageData = $ajaxclientCont->getPolicyPageData($customerId);
    $pageData = $clientPageData[0];
    $numb = count($clientPageData);
    
    $metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
    $metaRes = count($metaTags);
    $mets_res = $metaTags[0];
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
           <?php echo ajaxCssHeader(); ?>  
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">

                    <!-- BEGIN CONTENT BODY -->

                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Policies Page</span>

                                            <span class="caption-helper">Add/Edit Policies Page Here..</span>

                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-body ">
                                                    <div style="width:100%;" class="clear"> 
                                                        <a class="pull-right">
                                                            <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                        </a>
                                                        <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                      <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_policy" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag"/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_policy" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->

                                    <?php
                                    $cnt = 1;
                                    if ($numb == "0") {
                                        ?>
                                        <div class="portlet-body form">
                                            <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab Name
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="tab" required />
                                                            <input type="hidden" name="page_url" value="<?php echo $pageUrl ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab Content Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="tabheading" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Tab Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="tabcontent" rows="6" required></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <input type="submit" name="tabsub" class="btn green" id="submit" value="Add">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="portlet-body form">
                                            <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                                <div class="form-body">
                                                    <?php
                                                    foreach ($clientPageData as $row1) {
                                                        ?>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Tab <?php echo $cnt; ?> Name
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="tab<?php echo $cnt; ?>" value="<?php echo $row1->tab_name; ?>" required />
                                                                <input type="hidden" name="page_url" value="<?php echo $pageUrl ?>"/>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Tab <?php echo $cnt; ?> Content Heading
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="tab<?php echo $cnt; ?>heading" value="<?php echo $row1->tab_heading; ?>" required />
                                                            </div>
                                                        </div>

                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3"> Tab <?php echo $cnt; ?> Content
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control"  name="tab<?php echo $cnt; ?>content" rows="6" required><?php echo $row1->tab_content; ?></textarea>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="s<?php echo $cnt; ?>" value="<?php echo $row1->s_no; ?>" />
                                                        <?php
                                                        $cnt++;
                                                    }
                                                    ?>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <input type="submit" name="tabupd" class="btn green" id="submit" value="Save">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>					
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if ($numb == "5" || $numb == "0") {
                                        
                                    } else {
                                        ?>
                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_tab').slideToggle();" value="Add More Tabs" /></span><br><br>

                                        <div class="portlet-body form" id="add_tab" style="display:none;">
                                            <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab Name
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="newtab" required />
                                                            <input type="hidden" name="page_url" value="<?php echo $pageUrl ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab Content Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="newtabheading" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Tab Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="newtabcontent" rows="6" required></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <input type="submit" name="newtabsub" class="btn green" id="submit" value="Save">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>               
            <div class="quick-nav-overlay"></div>

            <?php echo ajaxJsFooter(); ?>
        </body>
    </html>
   