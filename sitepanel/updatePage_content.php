<?php

include "admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$analyticsCont = new analyticsPage();
$propertyCont = new propertyData();
$clientPageData = $ajaxclientCont->getHomePageData($customerId);
$homRes = count($clientPageData);

$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;

require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';

Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));
//print_r($clientPageData);
$id = $_GET['id'];
$pageData = $clientPageData[0];
if (isset($_POST['Home_Page_Update'])) {

    $res = $clientPageData[0];
    $list = $_POST['testimonial_cate'];
    $testimonial_cate = implode(",", $list);
    $list_1 = $_POST['video_review_cate'];
    $video_review_cate = implode(",", $list_1);
    $awardData = $_POST['awarddivimage'];
    $award_image = implode(",", $awardData);

    $basicUrl = "http://www.users.nodal.direct";
    if ($_FILES[image1][name] != "") {

        $path1 = $basicUrl . "/images/slider/" . $res->reviewheadimg;
        unlink($path1);
        $img_name = "1" . $id . "-" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));

        $product_image = upload_file($img_name, "image1", $customerId . "/images");
        $cond1 = ",reviewheadimg='$img_name''";
    }

    if ($_FILES[image2][name] != "") {
        $path2 = $basicUrl . "/images/slider/" . $res->image2;
        unlink($path2);
        $img_name = "2" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image2"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        $product_image1 = upload_file($img_name, "image2", $customerId . "/images");
        $cond2 = ",image2='$img_name'";
    }

    if ($_FILES[image3][name] != "") {

        $path3 = $basicUrl . "/images/slider/" . $res->image3;
        unlink($path3);
        $img_name = "3" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image3"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        $product_image2 = upload_file($img_name, "image3", $customerId . "/images");
        $cond3 = ",image3='$img_name'";
    }

    if ($_FILES[image4][name] != "") {
        $path4 = $basicUrl . "/images/slider/" . $res->image4;
        unlink($path4);
        $img_name = "4" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image4"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        /* if ($upload) {
          echo "File is valid, and was successfully uploaded.\n";
          } else {
          echo "Upload failed";
          } */

        $product_image3 = upload_file($img_name, "image4", $customerId . "/images");
        $cond4 = ",image4='$img_name'";
    }
    if ($_FILES[image5][name] != "") {

        $path4 = $basicUrl . "/images/slider/" . $res->image5;
        unlink($path4);
        $img_name = "home-award-" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image5"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        $product_image4 = upload_file($img_name, "image5", $customerId . "/images");
        $cond5 = ",image5='$img_name'";
    }
    if ($_FILES[image6][name] != "") {

        $path4 = $basicUrl . "/images/slider/" . $res->image6;
        unlink($path4);
        $img_name = "home-award1-" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image6"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        $product_image5 = upload_file($img_name, "image6", $customerId . "/images");
        $cond6 = ",image6='$img_name'";
    }

    if ($_FILES[image7][name] != "") {

        $path4 = $basicUrl . "/images/slider/" . $res->image7;
        unlink($path4);
        $img_name = "home-award2-" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image7"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        $product_image6 = upload_file($img_name, "image7", $customerId . "/images");
        $cond7 = ",image7='$img_name'";
    }

    if ($_FILES[image8][name] != "") {

        $path4 = $basicUrl . "/images/slider/" . $res->image8;
        unlink($path4);
        $img_name = "home-award3-" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image8"]["tmp_name"], array("public_id" => $img_name, "folder" => "reputize/team_members"));
        $product_image7 = upload_file($img_name, "image8", $customerId . "/images");
        $cond8 = ",image8='$img_name'";
    }
    $meta_title = $_REQUEST['h1tag'] . ',' . $_REQUEST['aptgalleryhead'];
    $filename = 'index';
    $dec1 = $_REQUEST['H1Content'];
    $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');

    if ($homRes > 0) {
        $ajaxclientCont->UpdateHomeContentData($customerId, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $testimonial_cate, $video_review_cate, $award_image);
    } else {
        $ajaxclientCont->AddHomeContentData($customerId, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $testimonial_cate, $video_review_cate, $award_image);
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
    }

    // $ajaxclientCont->UpdateHomeContentData($customerId, $cond1, $cond2, $cond3, $cond4, $cond5, $cond6, $cond7, $cond8, $testimonial_cate,$award_image);
    // $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $date);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "manage-home-page.php";
            </script>';
}

if (isset($_POST['create_static'])) {
    $new_page_url = $_POST['new_page_url'];
    $page_name = $_POST['page_name'];
    $main_data_mobile = $ajaxclientCont->getStaticMainPageDataForMobile($page_name);
    $main_data = $ajaxclientCont->getStaticMainPageData($page_name);
    $static_data = $ajaxclientCont->getStaticPageData($customerId, $page_name);
    //  print_r($static_data);
    // die;
    foreach ($main_data as $dt) {
        $pgnm = $dt->page_name;
        $pgdb = $dt->dbName;
        $pgcms = $dt->cms_page;
    }
    foreach ($main_data_mobile as $dt1) {
        $mo_pgnm = $dt1->page_name;
        $mo_page = $dt1->page;
        $mo_pgdb = $dt1->dbName;
        $mo_pgcms = $dt1->cms_page;
    }

    if ($static_data != null) {
        // echo '1';
        foreach ($static_data as $dt2) {
            $pgchecurl = $dt2->page_url;
        }
        if ($pgchecurl == '') {
            // echo '2';
            $pgupdate = $ajaxclientCont->UpdateStaticPageData($customerId, $page_name, $new_page_url);
        } else {
            //echo '3';
            $pgupdins = $ajaxclientCont->InsertStaticPageData($customerId, $page_name, $pgnm, $new_page_url, $pgdb, $pgcms);
        }
    } else {

        $newpgins = $ajaxclientCont->InsertNewPageData($customerId, $page_name, $pgnm, $new_page_url, $pgdb, $pgcms);
        //$mo_newpgins = $ajaxclientCont->InsertNewPageDataMobile($customerId, $mo_page, $mo_pgnm, $new_page_url, $mo_pgdb, $mo_pgcms);
        $pgdbins = $ajaxclientCont->insertDBPage($customerId, $new_page_url, $pgdb);
    }

    echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "manage-static-pages.php";
            </script>';
    exit;
}
if (isset($_POST['contactussave'])) {

    $type = $_POST['typeofsave'];

    $mapurl = $_POST['mapurl'];
    $tab1name = $_POST['tab1name'];
    $tab1heading = $_POST['tab1heading'];
    $tab1form = $_POST['tab1form'];
    $tab1subhead = $_POST['tab1subhead'];
    $tab1content = $_POST['tab1content'];
    $box1head = $_POST['box1head'];
    $box1content = $_POST['box1content'];
    $box2head = $_POST['box2head'];
    $box2content = $_POST['box2content'];
    $box3head = $_POST['box3head'];
    $box3content = $_POST['box3content'];
    $box4head = $_POST['box4head'];
    $box4content = $_POST['box4content'];
    $page_url = $_POST['page_url'];
    if ($_FILES[image_header][name] != "") {
        $img_header1 = "contactus-header" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image_header"]["tmp_name"], array("public_id" => $img_header1, "folder" => "reputize/ContactUs"));
        $img_header = ",header_img='$img_header1'";
    }

    $meta_title = $_REQUEST['tab1name'] . ',' . $_REQUEST['tab1heading'];
    $filename = $_REQUEST['page_url'];
    $dec1 = $_REQUEST['tab1content'];
    $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');
    if ($type == "newaddition") {
        //  echo '8';
        $newpgins = $ajaxclientCont->InsertContactUsPage($customerId, $mapurl, $tab1name, $tab1heading, $tab1form, $tab1subhead, $tab1content, $box1head, $box1content, $box2head, $box2content, $box3head, $box3content, $box4head, $box4content, $img_header);
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "manage-static-pages.php";
            </script>';
        exit;
    }

    if ($type == "updation") {
        $update = $ajaxclientCont->UpdateContactUsPage($customerId, $mapurl, $tab1name, $tab1heading, $tab1form, $tab1subhead, $tab1content, $box1head, $box1content, $box2head, $box2content, $box3head, $box3content, $box4head, $box4content, $img_header, $page_url);
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "manage-static-pages.php";
            </script>';
        exit;
    }
}
if (isset($_POST['edit_client_page'])) {
    $h1 = $_POST['h1'];
    $h1_cont = $_POST['h1_cont'];
    $box1 = $_POST['box1'];
    $box1_cont = $_POST['box1_cont'];
    $box2 = $_POST['box2'];
    $box2_cont = $_POST['box2_cont'];
    $box3 = $_POST['box3'];
    $box3_cont = $_POST['box3_cont'];
    $box4 = $_POST['box4'];
    $box4_cont = $_POST['box4_cont'];
    $h2 = $_POST['h2'];
    $h2_subhead1 = $_POST['h2_subhead1'];
    $h2_subhead1_cont = $_POST['h2_subhead1_cont'];
    $h2_subhead2 = $_POST['h2_subhead2'];
    $h2_subhead2_cont = $_POST['h2_subhead2_cont'];
    $h3 = $_POST['h3'];
    $h3_video1 = $_POST['h3_video1'];
    $h3_video2 = $_POST['h3_video2'];
    $page_url = $_POST['page_url'];
    if ($_FILES[image_header][name] != "") {
        $img_header1 = "header" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image_header"]["tmp_name"], array("public_id" => $img_header1, "folder" => "reputize/staticpage"));
        $img_header = ",header_img='$img_header1'";
    }

    if ($_FILES[box1_img][name] != "") {
        $box1_img = "box1_img" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["box1_img"]["tmp_name"], array("public_id" => $box1_img, "folder" => "reputize/staticpage"));
    }
    if ($_FILES[box2_img][name] != "") {
        $box2_img = "box2_img" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["box2_img"]["tmp_name"], array("public_id" => $box2_img, "folder" => "reputize/staticpage"));
    }
    if ($_FILES[box3_img][name] != "") {
        $box3_img = "box3_img" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["box3_img"]["tmp_name"], array("public_id" => $box3_img, "folder" => "reputize/staticpage"));
    }
    if ($_FILES[box4_img][name] != "") {
        $box4_img = "box4_img" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["box4_img"]["tmp_name"], array("public_id" => $box4_img, "folder" => "reputize/staticpage"));
    }
    //  die;
    $update = $ajaxclientCont->UpdateClientPage($customerId, $h1, $h1_cont, $box1, $box1_cont, $box2, $box2_cont, $box3, $box3_cont, $box4, $box4_cont, $h2, $h2_subhead1, $h2_subhead1_cont, $h2_subhead2, $h2_subhead2_cont, $h3, $h3_video1, $h3_video2, $img_header, $box1_img, $box2_img, $box3_img, $box4_img, $page_url);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "manage-static-pages.php";
            </script>';
    exit;
    //  $upd = "update static_page_clientsub set client_h1='$h1',client_h1_cont='$h1_cont',client_box1='$box1',client_box1_cont='$box1_cont',client_box2='$box2',client_box2_cont='$box2_cont',client_box3='$box3',client_box3_cont='$box3_cont',client_box4='$box4',client_box4_cont='$box4_cont',client_h2='$h2',client_h2_subhead1='$h2_subhead1',client_h2_subhead1_cont='$h2_subhead1_cont',client_h2_subhead2='$h2_subhead2',client_h2_subhead2_cont='$h2_subhead2_cont',client_h3='$h3',client_h3_video1='$h3_video1',client_h3_video2='$h3_video2' where customerID='$custID'";

    /*
      $checksel = "select * from static_page_clientsub where page_url='$pgurls' and customerID='$custID'";
      $checkquer = mysql_query($checksel);
      $checknum = mysql_num_rows($checkquer);
      if ($checknum == "0") {
      $ins = "insert into static_page_clientsub(customerID,page_url,client_h1,client_h1_cont,client_box1,client_box1_cont,client_box2,client_box2_cont,client_box3,client_box3_cont,client_box4,client_box4_cont,client_h2,client_h2_subhead1,client_h2_subhead1_cont,client_h2_subhead2,client_h2_subhead2_cont,client_h3,client_h3_video1,client_h3_video2) values('$custID','$pgurls','$h1','$h1_cont','$box1','$box1_cont','$box2','$box2_cont','$box3','$box3_cont','$box4','$box4_cont','$h2','$h2_subhead1','$h2_subhead1_cont','$h2_subhead2','$h2_subhead2_cont','$h3','$h3_video1','$h3_video2')";

      $insquer = mysql_query($ins);
      Header("location:edit_client.php?pageUrl=" . $pgurls);
      exit;
      } else {

      $updquer = mysql_query($upd);
      Header("location:edit_client.php?pageUrl=" . $pgurls);
      exit;
      }
     * 
     */
}
// ---------------Policy Page Start -------------------------------->
if (isset($_POST['tabupd'])) {
    if (isset($_POST['tab1'])) {
        $tab = $_POST['tab1'];
        $tabhead = $_POST['tab1heading'];
        $tabconte = $_POST['tab1content'];
        $tabno = $_POST['s1'];
        $update = $ajaxclientCont->UpdatePolicyPageData($tab, $tabhead, $tabconte, $tabno);
    }
    if (isset($_POST['tab2'])) {
        $tab = $_POST['tab2'];
        $tabhead = $_POST['tab2heading'];
        $tabconte = $_POST['tab2content'];
        $tabno = $_POST['s2'];
        $update = $ajaxclientCont->UpdatePolicyPageData($tab, $tabhead, $tabconte, $tabno);
    }
    if (isset($_POST['tab3'])) {
        $tab = $_POST['tab3'];
        $tabhead = $_POST['tab3heading'];
        $tabconte = $_POST['tab3content'];
        $tabno = $_POST['s3'];
        $update = $ajaxclientCont->UpdatePolicyPageData($tab, $tabhead, $tabconte, $tabno);
    }
    if (isset($_POST['tab4'])) {
        $tab = $_POST['tab4'];
        $tabhead = $_POST['tab4heading'];
        $tabconte = $_POST['tab4content'];
        $tabno = $_POST['s4'];
        $update = $ajaxclientCont->UpdatePolicyPageData($tab, $tabhead, $tabconte, $tabno);
    }
    if (isset($_POST['tab5'])) {
        $tab = $_POST['tab5'];
        $tabhead = $_POST['tab5heading'];
        $tabconte = $_POST['tab5content'];
        $tabno = $_POST['s5'];
        $update = $ajaxclientCont->UpdatePolicyPageData($tab, $tabhead, $tabconte, $tabno);
    }
    echo "<script> window.location='manage-static-pages.php'; </script>";
    exit;
}

if (isset($_POST['tabsub'])) {
    $tab = $_POST['tab'];
    $page_url = $_POST['page_url'];
    $tabhead = $_POST['tabheading'];
    $tabconte = $_POST['tabcontent'];
    $ins_policy = $ajaxclientCont->InsertPolicyPageData($customerId, $page_url, $tab, $tabhead, $tabconte);
    // $tabins = "insert into static_page_policy(customerID,tab_name,tab_heading,tab_content) values('$custID','$tab','$tabhead','$tabconte')";  
    echo "<script> window.location='manage-static-pages.php'; </script>";
    exit;
}

if (isset($_POST['newtabsub'])) {
    $tab = $_POST['newtab'];
    $page_url = $_POST['page_url'];
    $tabheading = $_POST['newtabheading'];
    $tabcontent = $_POST['newtabcontent'];
    $ins_policy = $ajaxclientCont->InsertPolicyPageData($customerId, $page_url, $tab, $tabheading, $tabcontent);
    // $newtabins = "insert into static_page_policy(customerID,tab_name,tab_heading,tab_content) values('$custID','$newtab','$newtabheading','$newtabcontent')";
    echo "<script> window.location='manage-static-pages.php'; </script>";
    exit;
}

// ---------------Food Page Start -------------------------------->

if (isset($_POST['foodsave'])) {
    $type = $_POST['typeofsave'];
    $h1 = $_POST['h1'];
    $subhead1 = $_POST['subhead1'];
    $box1icon = $_POST['box1icon'];
    $box1head = $_POST['box1head'];
    $box1content = $_POST['box1content'];
    $box2icon = $_POST['box2icon'];
    $box2head = $_POST['box2head'];
    $box2content = $_POST['box2content'];
    $box3icon = $_POST['box3icon'];
    $box3head = $_POST['box3head'];
    $box3content = $_POST['box3content'];
    $h2 = $_POST['h2'];
    $subhead2 = $_POST['subhead2'];
    $qadivhead = $_POST['qadivhead'];
    $question = $_POST['question'];
    $answer = $_POST['answer'];

    $head3 = $_POST['head3'];
    $iframe = $_POST['iframeSrc'];
    $page_url = $_POST['page_url'];

    // print_r($_FILES['file']);    
    $FileUpload = new File();
    //$fileName = $_FILES['file'];
    if (isset($_FILES['file'])) {
        $FileUpload->UploadFile($_FILES['file']);
    } else {
        echo 'file Was not submited';
    }

    if ($_FILES[image_header][name] != "") {

        $img_header1 = "food-header" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image_header"]["tmp_name"], array("public_id" => $img_header1, "folder" => "reputize/food_gallery"));
        $img_header = ",header_img='$img_header1'";
    }

    //$file = $_FILES['image_header']['name'];   
//echo $alt_tag."<br>";  


    if ($_FILES[image1][name] != "") {
        $path1 = SITE_DIR_PATH . "/images/" . $res[reviewheadimg];
        unlink($path1);
        $img_name = "foodMenu-" . date("Y_m_d_H_i_s");

        $product_image = upload_file($img_name, "image1", "images");
        $cond1 = ",pdf_url='$product_image'";
    }
    $meta_title = $_REQUEST['h1'] . ',' . $_REQUEST['h2'];
    $filename = $_REQUEST['page_url'];
    $dec1 = $_REQUEST['heading_content'];
    $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');

    if ($type == "newaddition") {
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        $ins_food = $ajaxclientCont->InsertNewFoodPageData($customerId, $h1, $subhead1, $box1icon, $box1head, $box1content, $box2icon, $box2head, $box2content, $box3icon, $box3head, $box3content, $h2, $subhead2, $qadivhead, $question, $answer, $head3, $iframe, $_FILES['file'], $img_header);
        //  $foodins = "insert into static_page_food(customerID,h1,subhead1,box1icon,box1head,box1content,box2icon,box2head,box2content,box3icon,box3head,box3content,h2,subhead2,qaheading,question,answer,head3,iframe) values('$custID','$h1','$subhead1','$box1icon','$box1head','$box1content','$box2icon','$box2head','$box2content','$box3icon','$box3head','$box3content','$h2','$subhead2','$qadivhead','$question','$answer','$head3','$iframe')";
    } else {
        $upd_food = $ajaxclientCont->UpdateFoodPageData($customerId, $h1, $subhead1, $box1icon, $box1head, $box1content, $box2icon, $box2head, $box2content, $box3icon, $box3head, $box3content, $h2, $subhead2, $qadivhead, $head3, $iframe, $cond1, $_FILES['file'], $img_header, $page_url);
        //$foodupd = "update static_page_food set h1='$h1',subhead1='$subhead1',box1icon='$box1icon',box1head='$box1head',box1content='$box1content',box2icon='$box2icon',box2head='$box2head',box2content='$box2content',box3icon='$box3icon',box3head='$box3head',box3content='$box3content',h2='$h2',subhead2='$subhead2',qaheading='$qadivhead',head3='$head3',iframe='$iframe' $cond1 where customerID='$custID'";
    }

    $quescn = 0;
    //   $selquesAns1 = "select * from static_page_food where customerID='$custID' order by s_no asc";
    $FoodPageData = $ajaxclientCont->getFoodPageMoreData($customerId);
    //  $selquesAnsQuer1 = mysql_query($selquesAns1);
    $sNoArr = '';
    /*
      while ($selQuesDat1 = mysql_fetch_array($selquesAnsQuer1)) {
      $sNoArr .= $selQuesDat1['s_no'] . "~";
      }
     * 
     */
    foreach ($FoodPageData as $dt) {
        $sNoArr .= $dt->s_no;
    }

    $orgSnoArr = explode("~", $sNoArr);

    foreach ($question as $ques) {
        $quesSno = $orgSnoArr[$quescn];

        $quesCont = $question[$quescn];

        $answCont = $answer[$quescn];

        $updQuesAns = $ajaxclientCont->UpdateFoodQuesAnswer($quesSno, $quesCont, $answCont);

        //  $updQuesAns = "update static_page_food set question='$quesCont',answer='$answCont' where s_no='$quesSno'";
        // echo $updQuesAns."<br><br>";
        // $updQuesQuer = mysql_query($updQuesAns);

        $quescn++;
    }
    // die;

    echo '<script type="text/javascript">
               alert("Succesfuly Update data");              
window.location = "manage-static-pages.php";
            </script>';
    exit;
}

//-------------Upload Photo For Food Gallery------------------------>
if (isset($_POST['submit_food_pic'])) {

    $cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;


    //require 'Cloudinary.php';
    //  require 'Uploader.php';
    //  require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $pro_id = $_POST['recordID'];

    $file = $_FILES['image_org']['name'];
    $alt_tag = $_POST['imageAlt1'];
//echo $alt_tag."<br>";  
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    if ($customerId == 1) {
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "food_gallery"));
    } else {
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/food_gallery"));
    }

    $propertyImg = $ajaxclientCont->AddFoodGalleryPhoto($customerId, $timestmp, $alt_tag);
    echo '<script type="text/javascript">
               alert("Succesfuly Upload Image");              
window.location = "manage-static-pages.php";
            </script>';
}


if (isset($_POST['addmoreques'])) {
    $ques = $_POST['addques'];
    $ans = $_POST['addans'];
    $addQuesAns = $ajaxclientCont->AddNewQuesAnsFood($customerId, $ques, $ans);
    echo "<script> window.location='manage-static-pages.php'; </script>";
    // $addmoreins = "insert into static_page_food(customerID,question,answer) values('$custID','$ques','$ans')";
    //$addmorequesquery = mysql_query($addmoreins);
}
if (isset($_POST['edit_about_page'])) {
    $page_url = $_POST['page_url'];
    $querystr = $_POST['querystr'];
    $h1 = $_POST['h1'];
    $h1_cont = $_POST['h1_cont'];
    $h2 = $_POST['h2'];
    $h2_cont = $_POST['h2_cont'];
    $collapse_h1 = $_POST['collapse_h1'];
    $collapse_h1_cont = $_POST['collapse_h1_cont'];
    $collapse_h2 = $_POST['collapse_h2'];
    $collapse_h2_cont = $_POST['collapse_h2_cont'];
    $collapse_h3 = $_POST['collapse_h3'];
    $collapse_h3_cont = $_POST['collapse_h3_cont'];
    $collapse_h4 = $_POST['collapse_h4'];
    $collapse_h4_cont = $_POST['collapse_h4_cont'];
    $h3 = $_POST['h3'];

    $metaTags = $ajaxclientCont->getMetaTagsData($customerId, $page_url);
    $metaRes = count($metaTags);
    //   $clientPageData = $ajaxclientCont->getAboutUsPageData($customerId);
//    print_r($clientPageData);
    // $checksel = "select * from static_page_tbl";
    // $checkquer = mysql_query($checksel);
    $checknum = count($clientPageData);

    if ($_FILES[image_header][name] != "") {
        $img_header1 = "aboutus-header" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image_header"]["tmp_name"], array("public_id" => $img_header1, "folder" => "reputize/aboutUs"));
        $img_header = ",header_img='$img_header1'";
    }

    $meta_title = $_REQUEST['h1'] . ',' . $_REQUEST['h2'];
    $filename = $_REQUEST['page_url'];
    $dec1 = $_REQUEST['h1_cont'];
    $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');

    if ($checknum == "0") {
        if ($metaRes == '0') {
            $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        }
        $ins_aboutus = $ajaxclientCont->InsertAboutUsData($customerId, $h1, $h1_cont, $h2, $h2_cont, $collapse_h1, $collapse_h1_cont, $collapse_h2, $collapse_h2_cont, $collapse_h3, $collapse_h3_cont, $collapse_h4, $collapse_h4_cont, $querystr, $h3, $img_header);
        //$ins = "insert into static_page_tbl(customerID,page_url,about_h1,about_h1_cont,about_h2,about_h2_cont,about_collapseh1,about_collapseh1_cont,about_collapseh2,about_collapseh2_cont,about_collapseh3,about_collapseh3_cont,about_collapseh4,about_collapseh4_cont,about_h3) values('1','$querystr','$h1','$h1_cont','$h2','$h2_cont','$collapse_h1','$collapse_h1_cont','$collapse_h2','$collapse_h2_cont','$collapse_h3','$collapse_h3_cont','$collapse_h4','$collapse_h4_cont','$h3')";
        echo '<script type="text/javascript">
                alert("Succesfuly Added Data");              
window.location = "manage-static-pages.php";
            </script>';
        exit;
    } else {
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        $upd = $ajaxclientCont->UpdateAboutUsData($customerId, $h1, $h1_cont, $h2, $h2_cont, $collapse_h1, $collapse_h1_cont, $collapse_h2, $collapse_h2_cont, $collapse_h3, $collapse_h3_cont, $collapse_h4, $collapse_h4_cont, $querystr, $h3, $img_header, $page_url);
        //  $upd = "update static_page_tbl set about_h1='$h1',about_h1_cont='$h1_cont',about_h2='$h2',about_h2_cont='$h2_cont',about_collapseh1='$collapse_h1',about_collapseh1_cont='$collapse_h1_cont',about_collapseh2='$collapse_h2',about_collapseh2_cont='$collapse_h2_cont',about_collapseh3='$collapse_h3',about_collapseh3_cont='$collapse_h3_cont',about_collapseh4='$collapse_h4',about_collapseh4_cont='$collapse_h4_cont',about_h3='$h3' where customerID='1' and page_url='$querystr'";
        // $updquer = mysql_query($upd);       
        echo '<script type="text/javascript">
                alert("Succesfuly updated Data");              
window.location = "manage-static-pages.php";
            </script>';
        exit;
    }
}
if (isset($_POST['whyperch_page'])) {

    $h1 = $_POST['h1'];
    $boxicon1 = $_POST['boxicon1'];
    $boxhead1 = $_POST['boxhead1'];
    $boxcontent1 = $_POST['boxcontent1'];
    $boxicon2 = $_POST['boxicon2'];
    $boxhead2 = $_POST['boxhead2'];
    $boxcontent2 = $_POST['boxcontent2'];
    $boxicon3 = $_POST['boxicon3'];
    $boxhead3 = $_POST['boxhead3'];
    $boxcontent3 = $_POST['boxcontent3'];

    $h2 = $_POST['h2'];
    $subhead2 = $_POST['subhead2'];
    $imgboxh1 = $_POST['imgboxh1'];
    $imgboximg1 = $_POST['imgboximg1'];
    $imgboxcontent1 = $_POST['imgboxcontent1'];
    $imgboxh2 = $_POST['imgboxh2'];
    $imgboximg2 = $_POST['imgboximg2'];
    $imgboxcontent2 = $_POST['imgboxcontent2'];

    if (isset($_POST['boxicon4'])) {
        $boxicon4 = $_POST['boxicon4'];
        $boxhead4 = $_POST['boxhead4'];
        $boxcontent4 = $_POST['boxcontent4'];
        $boxicon5 = $_POST['boxicon5'];
        $boxhead5 = $_POST['boxhead5'];
        $boxcontent5 = $_POST['boxcontent5'];
        $boxicon6 = $_POST['boxicon6'];
        $boxhead6 = $_POST['boxhead6'];
        $boxcontent6 = $_POST['boxcontent6'];
    }

    if (isset($_POST['imgboxh3'])) {
        $imgboxh3 = $_POST['imgboxh3'];
        $imgboximg3 = $_POST['imgboximg3'];
        $imgboxcontent3 = $_POST['imgboxcontent3'];
        $imgboxh4 = $_POST['imgboxh4'];
        $imgboximg4 = $_POST['imgboximg4'];
        $imgboxcontent4 = $_POST['imgboxcontent4'];
    }
}
if ($_POST['add_meta_tags']) {
    $date = date("Y/m/d");
    $pagename = $_POST['pagename'];
    $meta_title = $_POST['MetaTitle'];
    $filename = $_POST['filename'];
    $dec = $_POST['MetaDisc'];
    $cityID = $_POST['city_id'];
    $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
    if ($pagename == 'manage-home-page') {
        // updateTable("metaTags", " filename='$_REQUEST[filename]',MetaTitle='$MetaTitle',MetaDisc='$MetaDisc',MetaKwd='$MetaKwd',editdate='$date',othercode='$othercode' where customerID='$custID' and metano='$metano1' && filename='$url1' ");
        echo '<script type="text/javascript">
                alert("Succesfuly Added Data");              
window.location = "' . $pagename . '.php?pageUrlData=' . $filename . '";
            </script>';
        exit;
    } else if ($pagename == 'ajax_content_city') {
        // updateTable("metaTags", " filename='$_REQUEST[filename]',MetaTitle='$MetaTitle',MetaDisc='$MetaDisc',MetaKwd='$MetaKwd',editdate='$date',othercode='$othercode' where customerID='$custID' and metano='$metano1' && filename='$url1' ");
        echo '<script type="text/javascript">
                alert("Succesfuly Added Data");              
window.location = "' . $pagename . '.php?cityid=' . $cityID . '";
            </script>';
        exit;
    } else {
        echo '<script type="text/javascript">
                alert("Succesfuly Added Data");              
window.location = "' . $pagename . '.php?pageUrlData=' . $filename . '";
            </script>';
        exit;
    }

    //db_query("insert into metaTags set customerID='$custID',filename='$_REQUEST[filename]',MetaTitle='$MetaTitle',MetaDisc='$MetaDisc',MetaKwd='$MetaKwd',othercode='$othercode',editdate='$date'");
    //echo "insert into propertyImages set propertyID='$_REQUEST[recordID]',roomID='$_REQUEST[roomID]',imageURL='$product_image',imageType='$imageType1',imageAlt='$imageAlt1'";
    // $_SESSION[session_message] = $record_type . ' has been added successfully';
}
if (isset($_POST['update_meta_tags'])) {
    $metano = $_POST['metano'];
    $pagename = $_POST['pagename'];
    $filename = $_POST['filename'];
    $cityID = $_POST['city_id'];
    $propID = $_POST['propID'];

    $updateMetaTags = $ajaxclientCont->UpdateMetaTags($metano);
    if ($pagename == 'manage-home-page') {
        // updateTable("metaTags", " filename='$_REQUEST[filename]',MetaTitle='$MetaTitle',MetaDisc='$MetaDisc',MetaKwd='$MetaKwd',editdate='$date',othercode='$othercode' where customerID='$custID' and metano='$metano1' && filename='$url1' ");
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "' . $pagename . '.php";
            </script>';
        exit;
    } else if ($pagename == 'ajax_content_city') {
        // updateTable("metaTags", " filename='$_REQUEST[filename]',MetaTitle='$MetaTitle',MetaDisc='$MetaDisc',MetaKwd='$MetaKwd',editdate='$date',othercode='$othercode' where customerID='$custID' and metano='$metano1' && filename='$url1' ");
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "' . $pagename . '.php?cityid=' . $cityID . '";
            </script>';
        exit;
    } else if ($pagename == 'ajax_add_metatags') {
        // updateTable("metaTags", " filename='$_REQUEST[filename]',MetaTitle='$MetaTitle',MetaDisc='$MetaDisc',MetaKwd='$MetaKwd',editdate='$date',othercode='$othercode' where customerID='$custID' and metano='$metano1' && filename='$url1' ");
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "' . $pagename . '.php?propID=' . $propID . '";
            </script>';
        exit;
    } else {
        echo '<script type="text/javascript">
                alert("Succesfuly Updated Data");              
window.location = "' . $pagename . '.php?pageUrlData=' . $filename . '";
            </script>';
        exit;
    }
}
if ($id == "delete") {
    $metano = $_GET['metano'];
    $sql = "DELETE FROM metaTags WHERE metano='$metano' and customerID='$custID'";
    $deleteMetatag = $ajaxclientCont->DeleteMetaTags($customerId, $metano);
    echo "<script> window.location='manage-analytic-settings.php'; </script>";
    exit;
}
if ($_REQUEST["action_register"] == "update_analytic_code") {

    $analytic_code = $_POST['analytic_code'];
    $javascript_code = $_POST['javascript_code'];
    $update_code = $analyticsCont->UpdateAnalyticCode($customerId, $analytic_code, $javascript_code);
    // updateTable("tbl_analytic_code", " analyticcode='$_REQUEST[analyticcode]' where customerID='$custID'");

    echo '<script type="text/javascript">
                alert("Succesfuly Update Analytic Code");              
window.location = "ajax_add_analytic_code.php";
            </script>';

    exit;
}
if ($_REQUEST["action_register"] == "submit_analytic_code") {

    $analytic_code = $_POST['analytic_code'];
    $javascript_code = $_POST['javascript_code'];

    $add_code = $analyticsCont->AddAnalyticCode($customerId, $analytic_code, $javascript_code);
    //  $insanalytic = "insert into tbl_analytic_code(customerID,analyticcode) values('$custID','$_REQUEST[analyticcode]')";
    echo '<script type="text/javascript">
                alert("Succesfuly Added Analytic Code");              
window.location = "ajax_add_analytic_code.php";
            </script>';

    exit;
}
if (isset($_REQUEST['gallerysave'])) {
    $heading = $_POST['main_heading'];
    $type = $_POST['typeofsave'];
    if ($_FILES[image_header][name] != "") {
        $img_header = "gallery-header" . date("Y_m_d_H_i_s");
        $upload = \Cloudinary\Uploader::upload($_FILES["image_header"]["tmp_name"], array("public_id" => $img_header, "folder" => "reputize/gallery"));
    }
    $meta_title = $heading;
    $filename = $_REQUEST['page_url'];
    $dec = '';
    // $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');

    if ($type == "newaddition") {
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        $ins = $ajaxclientCont->AddGalleryHeading($customerId, $heading, $img_header);
    } else {
        $upd = $ajaxclientCont->UpdateGalleryHeading($customerId, $heading, $img_header);
    }
    echo '<script type="text/javascript">
                alert("Succesfuly Update data");              
window.location = "manage-static-pages.php";
            </script>';
    exit;
}
if (isset($_REQUEST['edit_terms_and_conditions'])) {
    $heading = $_REQUEST['heading'];
    $content = $_REQUEST['content'];
    $upd = $ajaxclientCont->UpdateTermsAndConditions($customerId, $heading, $content);
    echo '<script type="text/javascript">
                alert("Succesfuly Update data");              
window.location = "manage-static-pages.php";
            </script>';
    exit;
}
if (isset($_REQUEST['ourclientsub'])) {
    $type = $_POST['savecont'];
    $page_url = $_POST['page_url'];

    $h1 = $_POST['h1'];
    $h1content = $_POST['h1content'];
    $collapseh1 = $_POST['collapseh1'];
    $collapsecont1 = $_POST['collapsecont1'];
    $h2 = $_POST['h2'];

    $divhead1 = $_POST['divhead1'];
    $divcontent1 = $_POST['divcontent1'];

    $divhead2 = $_POST['divhead2'];
    $divcontent2 = $_POST['divcontent2'];

    $divhead3 = $_POST['divhead3'];
    $divcontent3 = $_POST['divcontent3'];
    $sidedivheading = $_POST['sidedivheading'];

    if (isset($_POST['collapseh2'])) {
        $collapseh2 = $_POST['collapseh2'];
        $collapsecont2 = $_POST['collapsecont2'];
        $upd1 = ",collapsehead2='$collapseh2',collapsecont2='$collapsecont2'";
    }
    if (isset($_POST['collapseh3'])) {
        $collapseh3 = $_POST['collapseh3'];
        $collapsecont3 = $_POST['collapsecont3'];
        $upd2 = ",collapsehead3='$collapseh3',collapsecont3='$collapsecont3'";
    }
    if (isset($_POST['collapseh4'])) {
        $collapseh4 = $_POST['collapseh4'];
        $collapsecont4 = $_POST['collapsecont4'];
        $upd3 = ",collapsehead4='$collapseh4',collapsecont4='$collapsecont4'";
    }

    if ($_FILES[divimg1][name] != "") {
        $nam = $_FILES[divimg1][name];
        $img_name1 = "ourclient1-" . date("Y_m_d_H_i_s");
        // $product_image1 = upload_file($img_name1, "divimg1", "images");
        $upload = \Cloudinary\Uploader::upload($_FILES["divimg1"]["tmp_name"], array("public_id" => $img_name1, "folder" => "reputize/our-client"));
        $cond1 = ",divimg1='$img_name1'";
    }

    if ($_FILES[divimg2][name] != "") {
        $nam = $_FILES[divimg2][name];
        $img_name2 = "ourclient2-" . date("Y_m_d_H_i_s");
        // $product_image2 = upload_file($img_name2, "divimg2", "images");
        $upload = \Cloudinary\Uploader::upload($_FILES["divimg2"]["tmp_name"], array("public_id" => $img_name2, "folder" => "reputize/our-client"));
        $cond2 = ",divimg2='$img_name2'";
    }

    if ($_FILES[divimg3][name] != "") {
        $nam = $_FILES[divimg3][name];
        $img_name3 = "ourclient3-" . date("Y_m_d_H_i_s");
        //$product_image3 = upload_file($img_name3, "divimg3", "images");
        $upload = \Cloudinary\Uploader::upload($_FILES["divimg3"]["tmp_name"], array("public_id" => $img_name3, "folder" => "reputize/our-client"));
        $cond3 = ",divimg3='$img_name3'";
    }
    $meta_title = $_REQUEST['h1'] . ',' . $_REQUEST['h2'];
    $filename = $_REQUEST['page_url'];
    $dec1 = $_REQUEST['h1content'];
    $dec = substr($dec1, 0, 250);
    $date = date('Y-m-d');

    if ($type == "Add Content") {
        // $insclient = "insert into static_page_clientmain(customerID,h1,h1content,collapsehead1,collapsecont1,h2,divhead1,divcontent1,divhead2,divcontent2,divhead3,divcontent3,sidedivheading) values('$custID','$h1','$h1content','$collapseh1','$collapsecont1','$h2','$divhead1','$divcontent1','$divhead2','$divcontent2','$divhead3','$divcontent3','$sidedivheading')";
        //$insclientquery = mysql_query($insclient);
        //$updcollapse = "update static_page_clientmain set h1='$h1' $upd1 $upd2 $upd3 $cond1 $cond2 $cond3 where customerID='$custID'";
        //$updcollapsequery = mysql_query($updcollapse);
        $addMetaTags = $ajaxclientCont->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);
        $ins = $ajaxclientCont->InsertOurClientPageData($customerId, $page_url, $h1, $h1content, $collapseh1, $collapsecont1, $h2, $divhead1, $divcontent1, $divhead2, $divcontent2, $divhead3, $divcontent3, $sidedivheading, $upd1, $upd2, $upd3, $cond1, $cond2, $cond3);
        echo '<script type="text/javascript">
                alert("Succesfuly Insert data");              
window.location = "manage-static-pages.php";
            </script>';
        exit;
    } elseif ($type == "Save Content") {
        // $updclient = "update static_page_clientmain set h1='$h1',h1content='$h1content',collapsehead1='$collapseh1',collapsecont1='$collapsecont1',h2='$h2',divhead1='$divhead1',divcontent1='$divcontent1',divhead2='$divhead2',divcontent2='$divcontent2',divhead3='$divhead3',divcontent3='$divcontent3',sidedivheading='$sidedivheading' $upd1 $upd2 $upd3 $cond1 $cond2 $cond3 where customerID='$custID'";
        //$updclientquery = mysql_query($updclient);
        $upd = $ajaxclientCont->UpdateOurClientPageData($customerId, $page_url, $h1, $h1content, $collapseh1, $collapsecont1, $h2, $divhead1, $divcontent1, $divhead2, $divcontent2, $divhead3, $divcontent3, $sidedivheading, $upd1, $upd2, $upd3, $cond1, $cond2, $cond3);
        echo '<script type="text/javascript">
                alert("Succesfuly Update data");              
window.location = "manage-static-pages.php";
            </script>';
        exit;
    }
}
 



