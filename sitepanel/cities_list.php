<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$propdata = new propertyData();
$propList1 = $inventoryCont->getPropertyListing($customerId);
?>
<?php
if (!empty($_POST["country_id"])) {
    $country_id = $_POST['country_id'];

    $cityData = $propdata->getAllCityListing($country_id);
    $rowCount = count($cityData);
}
//room option list
if ($rowCount > 0) {
    echo '<option value="">Select City</option>';
    foreach($cityData as $dt) {
        echo '<option value="' . $dt->id . '">' . $dt->name . '</option>';
    }
} else {
    echo '<option value="">City not available</option>';
}
?>
