<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getFoodPageData($customerId);
$FoodPageData = $ajaxclientCont->getFoodPageMoreData($customerId);
$pageData = $clientPageData[0];
$numb = count($clientPageData);
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>    
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">

                <!-- BEGIN CONTENT BODY -->

                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-equalizer font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Edit Food & Beverages Page</span>

                                        <span class="caption-helper">Add/Edit Food & Beverages Page Here..</span>

                                        <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Provide page content goes here!" data-original-title="Update page content" style="color:#7d6c0b;font-size:20px;"></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:227px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Food & Restaurant  Photo" /></span>                                         
                                                    <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -3px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    <span style="float:right; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;margin-right: 23px;margin-top: 15px" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                    <br><br><br><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_food" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag"/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_food" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->

                                <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                                <div class="portlet light bordered" id="add_photo" style="display:none;">			
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="updatePage_content.php">
                                            <div class="form-body">
                                                <h4 align="center" style="color:#E26A6A;"><b> Add New  Photo </b></h3><br><br>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Food Image
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
															  
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
																</div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="image_org"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Image Alt Tag
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="imageAlt1" type="text" class="form-control" />
                                                            <span class="help-block"> Provide your image alt tag</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <input name="recordID" type="hidden" value="<?php echo $propertyId ?>" />
                                                                <input type="submit" name="submit_food_pic" class="btn green" id="submit" value="Add Photo">
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </form>
                                        <div class="col-md-12">	
                                            <div class="portlet light portlet-fit bordered">
                                                <div class="portlet-body">
                                                    <div class=" mt-element-overlay">
                                                        <div class="row">
                                                            <?php
                                                            $FoodPhotoListdata = $ajaxclientCont->getFoodGalleryPhoto($customerId);

                                                            foreach ($FoodPhotoListdata as $PhotoList) {
                                                                ?>
                                                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                                                    <div class=" mt-overlay-1">
                                                                        <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_564,h_400,c_fill/<?php if ($customerId != 1) { ?>reputize<?php } ?>/food_gallery/<?php echo $PhotoList->image; ?>.jpg" />

                                                                        <div class="mt-overlay">
                                                                            <ul class="mt-info">
                                                                                <!--<li>
                                                                                    <a class="btn default btn-outline" href="javascript:;">
                                                                                        <i class="icon-magnifier"></i>
                                                                                    </a>
                                                                                </li>-->
                                                                                <!--  <li>
                                                                                      <a class="btn default btn-outline" href="../sitepanel/add_property.php?action=delete&imagesID=<?php echo $PhotoList->id; ?>">
                                                                                          <i class="icon-trash"></i>
                                                                                      </a>
                                                                                  </li>-->
                                                                            </ul>                                                                              
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                                <div class="portlet-body form">
                                    <form class="form-horizontal" name="formn" method="post" action="updatePage_content.php" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="expDiv" onclick="$('#general_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>
                                            <div id="general_content" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h1" value="<?php echo $pageData->h1; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Sub-Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="subhead1" value="<?php echo $pageData->subhead1; ?>" required />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Heading Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="heading_content" rows="6" required ><?php echo html_entity_decode($pageData->box1content); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Header Image
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
														<?php if($pageData->header_img == ''){ ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
														<?php }else{ ?>
														<img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/food_gallery/<?php echo $pageData->header_img; ?>.jpg" alt="" />
															
														<?php } ?>													
															</div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image_header"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Upload pdf Food Menu

                                                    </label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" type="file" name="file" />
                                                    </div>
                                                </div> 
                                            </div>
                                            <!-- <div class="form-group">
                                                 <label class="control-label col-md-3"> Box 1 Icon
                                                     <span class="required"> * </span>
                                                 </label>

                                                 <div class="col-md-4">
                                                     <input type="text" class="form-control" name="box1icon" value="<?php echo htmlspecialchars(html_entity_decode($pageData->box1icon)); ?>" required />
                                                 </div>
                                             </div>-->
                                            <div class="expDiv" onclick="$('#icon_boxes').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Icon Boxes</h3></div>
                                            <div id="icon_boxes" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Box 1 Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box1head" value="<?php echo $pageData->box1head; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Box 1 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="box1content" rows="6" required ><?php echo html_entity_decode($pageData->box1content); ?></textarea>
                                                    </div>
                                                </div>

                                                <!-- <div class="form-group">
                                                     <label class="control-label col-md-3"> Box 2 Icon
                                                         <span class="required"> * </span>
                                                     </label>
    
                                                     <div class="col-md-4">
                                                         <input type="text" class="form-control" name="box2icon" value="<?php echo htmlspecialchars(html_entity_decode($pageData->box2icon)); ?>" required />
                                                     </div>
                                                 </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Box 2 Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box2head" value="<?php echo $pageData->box2head; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Box 2 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="box2content" rows="6" required ><?php echo html_entity_decode($pageData->box2content); ?></textarea>
                                                    </div>
                                                </div>

                                                <!--<div class="form-group">
                                                    <label class="control-label col-md-3"> Box 3 Icon
                                                        <span class="required"> * </span>
                                                    </label>
    
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box3icon" value="<?php echo htmlspecialchars(html_entity_decode($pageData->box3icon)); ?>" required />
                                                    </div>
                                                </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Box 3 Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box3head" value="<?php echo $pageData->box3head; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Box 3 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="box3content" rows="6" required ><?php echo html_entity_decode($pageData->box3content); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="expDiv" onclick="$('#boxes_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3>Boxes Content</h3></div>
                                            <div id="boxes_content" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h2" value="<?php echo $pageData->h2; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Sub-Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="subhead2" value="<?php echo $pageData->subhead2; ?>" />
                                                    </div>
                                                </div>

                                                <!-- <div class="form-group">
                                                     <label class="control-label col-md-3"> Food Menu
                                                         <span class="required"> * </span>
                                                     </label>
                                                     <div class="fileinput fileinput-new" data-provides="fileinput">
                                                         <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                             <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                         <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                         <div>
                                                             <span class="btn default btn-file">
                                                                 <span class="fileinput-new"> Select image </span>
                                                                 <span class="fileinput-exists"> Change </span>
                                                                 <input type="file" name="image1"> </span>
                                                             <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                         </div>
                                                         Uploaded Image :- <a href="<?SITE_DIR_PATH;?>/images/<?php echo $res['image4']; ?>" target="_blank"><?php echo $res['image4']; ?></a>
                                                     </div>
                                                 </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Question Div Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="qadivhead" value="<?php echo $pageData->qaheading; ?>" />
                                                    </div>
                                                </div>

                                                <?php
                                                $selquesCnt = 1;
                                                //  $selquesAns = "select * from static_page_food where customerID='$custID' order by s_no asc";
                                                //  $selquesAnsQuer = mysql_query($selquesAns);
                                                foreach ($FoodPageData as $row) {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Box <?php echo $selquesCnt; ?> Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="question[]" value="<?php echo $row->question; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Box <?php echo $selquesCnt; ?> Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="answer[]" rows="6" ><?php echo html_entity_decode($row->answer); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $selquesCnt++;
                                                }
                                                ?>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 3
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="head3" value="<?php echo $pageData->head3; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Virtual Tour Iframe SRC
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="iframeSrc" value="<?php echo $pageData->iframe; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <?php
                                                        if ($numb == "0") {
                                                            ?>
                                                            <input type="hidden" name="typeofsave" value="newaddition" />
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <input type="hidden" name="typeofsave" value="updation" />
                                                            <?php
                                                        }
                                                        ?>
                                                        <input type="hidden"  name="page_url" value="<?php echo $pageData->page_url; ?>" />
                                                        <input type="submit" name="foodsave" class="btn green" id="submit" value="Save">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                                <!--    <span style="float:left;">
                                                        <input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:240px;font-size:14px;" data-html="true"  onclick="$('#add_more_ques').slideToggle();" value="Add More Tabs" />
                                                    </span><br><br>

                                                    <div class="portlet-body form" id="add_more_ques" style="display:none;">
                                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"> Question
                                                                        <span class="required"> * </span>
                                                                    </label>

                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control" name="addques" required />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group last">
                                                                    <label class="control-label col-md-3"> Answer
                                                                        <span class="required"> * </span>
                                                                    </label>

                                                                    <div class="col-md-9">
                                                                        <textarea class="ckeditor form-control"  name="addans" rows="6" required ></textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <input type="submit" name="addmoreques" class="btn green" id="submit" value="Add">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>           
        <div class="quick-nav-overlay"></div>

        <?php echo ajaxJsFooter(); ?>
    </body>
</html>
