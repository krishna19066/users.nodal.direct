<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$userType = $_SESSION['MyAdminUserType'];
$cityName = $_REQUEST['cityName'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
$propertyCont = new propertyData();

function generateSeoURL($string, $wordLimit = 0) {
    $separator = '-';

    if ($wordLimit != 0) {
        $wordArr = explode(' ', $string);
        $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
    }

    $quoteSeparator = preg_quote($separator, '#');

    $trans = array(
        '&.+?;' => '',
        '[^\w\d _-]' => '',
        '\s+' => $separator,
        '(' . $quoteSeparator . ')+' => $separator
    );

    $string = strip_tags($string);
    foreach ($trans as $key => $val) {
        $string = preg_replace('#' . $key . '#i' . (UTF8_ENABLED ? 'u' : ''), $val, $string);
    }

    $string = strtolower($string);

    return trim(trim($string, $separator));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php echo ajaxCssHeader(); ?>
        
        <script>
            $("#field1, #field2").keyup(function () {
                update();
            });

            function update() {
                $("#result").val($('#field1').val() + " " + $('#field2').val());
            }
        </script>
        <script>
            function AvoidSpace(event) {
                var k = event ? event.which : window.event.keyCode;
                if (k == 32)
                    return false;
            }
        </script>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Listing - <?php echo $cityName; ?>
                                <small>Listing All Service..</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-service.php">My Listing</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listing</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="">
                                <div class="portlet-title">
                <span style="float:left;">
				<a href="manage-service.php"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  value="List of City"/></a>
				</span>
                                </div>
                                
                            </div>
                        </div>                  
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!---------------------------------------------------------------- Add Service Section Ends --------------------------------------------------------------------->								

                            <?php
						
                            $serviceData = $propertyCont->GetAllServiceDataWithCity($customerId,$cityName);
                            if ($serviceData != NULL) {                              
                                    ?>

                                    <div class="row">
                                        <div class="col-lg-12 col-xs-12 col-sm-12" style="padding-right: 2px;">										
                                            <?php
                                            $count = 1;
                                            //  $propListing1 = $ajaxpropertyCont->getCityProperty($customerId, $cityName,$user_id);
                                            foreach ($serviceData as $Listing) {
                                                $name = $Listing->serviceName;
                                                $id = $Listing->id;
                                                if ($stat == "Y") {
                                                    $act_stat = "Active";
                                                } else {
                                                    $act_stat = "<span style='color:Red;'>Inactive</span>";
                                                }
                                                if ($feature == "Y") {
                                                    $fea_stat = "<span style='color:Yellow;'>Featured</span>";
                                                } else {
                                                    $fea_stat = "";
                                                }
                                                ?>            
                                                <div class="portlet box mt-list-item" style="background:#2AB4C0;border-bottom:2px solid #2AB4C0;">
                                                    <div class="portlet-title" onclick="$('#data_box<?php echo $count; ?>').slideToggle();" >
                                                        <div class="caption">
                                                            <center>
                                                                <?php echo $name; ?>
                                                            </center>
                                                        </div>
                                                        <div class="tools">
                                                            <span id="toggle_expand<?php echo $count; ?>">Expand</span>  
                                                        </div>
                                                    </div>
                                                    <div id="data_box<?php echo $count; ?>" style="display: none; padding: 20px; background: white; border: 1px solid #2AB4C0;color:#678098;">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-xs-6 col-sm-6" style="padding:0px; border-right:1px solid #74B0DA;">
                                                                <div>
                                                                    <table class="table_prop" style="width:98% ! important;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="width: 125px ! important;table-layout: fixed ! important;">Name </td>
                                                                                <td>: <?php echo $name; ?> </td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Rate </td>
                                                                                <td>: <?php echo $Listing->rate; ?> </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>City</td>
                                                                                <td>: <?php echo $Listing->cityName; ?></td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Rate Type </td>
                                                                                <td>: <?php echo $Listing->rate_type; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Country</td>

                                                                                <td>: <?php echo $Listing->countryName; ?></td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">To Date </td>

                                                                                <td>: <?php echo $Listing->to_date; ?> </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td></td>

                                                                                <td></td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Service.Add </td>
                                                                                <td>: <?php
                                                                                    $timestamp = $Listing->timestamp;
                                                                                    echo date("jS F, Y", strtotime($timestamp));
                                                                                    ?> </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4"></td>
                                                                            </tr>
                                                                            <tr style="border-bottom: 1px solid #ccc;">
                                                                                <td colspan="4"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>

                                                                                </td>
                                                                                <td colspan="3">

                                                                                </td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td>Category:</td>
                                                                                <td colspan="3">: 
                                                                                    <?php echo $service_type = $Listing->service_type; ?>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <br>
                                                                    <table style="width:100%;border-bottom: 1px solid #ccc;">
                                                                        <tbody>
                                                                            <tr><td></td>
                                                                                
                                                                                <td style="width:60%;">
                                                                                    <div class="btn-toolbar margin-bottom-10">
                                                                                        <div class="btn-group">
                                                                                        </div>
                                                                                        <a  href="edit-service.php?serviceID=<?php echo $Listing->id; ?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit

                                                                                            </button></a>

                                                                                        <button type="button" class="btn btn-danger">
                                                                                            <a <?php if ($mode == 'V') { ?> class="not-active" <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> href="experience-edit.php?id=delete&amp;recordID=<?php echo $propid; ?>" style="text-decoration:none;color:white;" onclick="return confirm('Are you sure you want to delete this record')">
                                                                                                <i class="fa fa-trash"></i>
                                                                                                Delete
                                                                                            </a>
                                                                                        </button>
                                                                                    </div><br>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                
                                                                    <p>&nbsp;&nbsp;
                                                                        <a href="service_view_photo.php?serviceID=<?php echo $id; ?>">Add/View Photo</a> |
                                                                        <a href="ajax_add_metatags.php?serviceID=<?php echo $id; ?>" >Add Meta Tags</a> 
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6 col-xs-6 col-sm-6" style="padding-left: 5px;padding-right: 5px;">
                                                                <h4> Content </h4><hr/>

                                                                <?php echo $Listing->description; ?>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>	
                                                <?php
                                                $count++;
                                        }
                                       
                                    } else {
                                        ?>
                                        <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                            <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#country').on('change', function () {
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: 'POST',
                    url: 'cities_list.php',
                    data: 'country_id=' + countryID,
                    success: function (html) {
                        $('#city').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#city').html('<option value="">Select Country first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>

            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>