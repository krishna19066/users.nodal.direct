<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$menuCont = new menuNavigationPage();
$staticPageCont = new staticPageData();
$propCount = new propertyData();
$getlanguage = $menuCont->GetLanguageData($customerId);
$getPropertyUrl = $menuCont->getPropertyUrls($customerId);
$getStaticUrl = $menuCont->getStaticPageUrls($customerId);
$getCityUrl = $menuCont->getCityUrls($customerId);
$getRoomUrl = $staticPageCont->getRoomPageUrlData($customerId);
$getSuperAdmin = $propCount->GetSupperAdminData($customerId);
$temlateType1 = $getSuperAdmin[0];
$temlateType = $temlateType1->template_type;
?>

<html lang="en">
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
     <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

        <script type = "text/javascript" >
            $(document).ready(function () {
                $('.reorder_link').on('click', function () {
                    $("ul.reorder-photos-list").sortable({tolerance: 'pointer'});
                    $('.reorder_link').html('save reordering');
                    $('.reorder_link').attr("id", "saveReorder");
                    $('#reorderHelper').slideDown('slow');
                    $('.sec_link').attr("href", "javascript:void(0);");
                    $('.sec_link').css("cursor", "move");
                    $("#saveReorder").click(function (e) {
                        if (!$("#saveReorder i").length) {
                            $(this).html('').prepend('<img src="ajax-loader.gif"/>');
                            $("ul.reorder-photos-list").sortable('destroy');
                            $("#reorderHelper").html("Reordering Photos - This could take a moment. Please don't navigate away from this page.").removeClass('light_box').addClass('notice notice_error');

                            var h = [];
                            $("ul.reorder-photos-list li").each(function () {
                                h.push($(this).attr('id').substr(9));
                            });

                            $.ajax({
                                type: "POST",
                                url: "orderUpdateSec.php",
                                data: {ids: " " + h + ""},
                                success: function () {
                                    window.location.reload();
                                }
                            });
                            return false;
                        }
                        e.preventDefault();
                    });
                });
            });
        </script>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div  class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Reorder Secondary Menu</span>

                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                </div>

                                <div style="width:100%;" class="clear"> 
                                    <span style="float:left;"> <a href="javascript:void(0);" class="reorder_link" id="saveReorder">Reorder Menu</a></span>

                                    <span style="float:right;"> <a href="manage_heder.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px; margin-left: 23px;"> Back </button></a></span>
                                    <span style="float:right; margin-left: 20px;">  <a href="manage-header22.php" class="btn green-haze btn-outline sbold uppercase ">Reorder Primary Menu</a></span>

                                    <br><br><br><br>
                                </div>
                            </div>
                        </div>
                        <center>
                            <div id="reorderHelper" class="light_box" style="display:none;">1. Drag  Drop Down Menu to reorder.<br>2. Click 'Save Reordering' when finished.</div>
                            <div class="gallery">
                                <?php
                                $ctn = 1;
                                foreach ($getlanguage as $language) {

                                    $name = $language->primary_link;
                                    $sequen = $language->primary_sequence;
                                    ?>
                                    <div class="panel-group">
                                        <div class="panel panel-default">

                                            <div class="panel-heading">
                                                <h4 class="panel-title">

                                                    <a data-toggle="collapse" href="#collapse<?php echo $ctn; ?>"> <?php echo $name ?></a>

                                                </h4>
                                            </div>
                                            <?php $getHeaderWithLink = $menuCont->GetHeaderFooterWithPriName($customerId, $name); ?>
                                            <div id="collapse<?php echo $ctn; ?>" class="panel-collapse collapse reorder_ul reorder-photos-list">

                                                <div class="panel-body ui-sortable-handle" id="image_li_<?php echo $language_1->s_no; ?>">

                                                </div>
                                                <ul class="reorder_ul reorder-photos-list">
                                                    <?php
                                                    // $propPhotoListdata = $ajaxpropertyPhotoCont->GetPropertyRoomPhoto($propID, $roomID);                                                     
                                                    foreach ($getHeaderWithLink as $language_1) {
                                                        $id = $language_1->s_no;
                                                        $nam = $language->primary_link;
                                                        $sequen = $language->primary_sequence;
                                                        ?>
                                                        <li id="image_li_<?php echo $id; ?>" class="ui-sortable-handle">
                                                            <a href="javascript:void(0);" style="float:none;" class="sec_link">
                                                                <?php echo $language_1->secondary_link; ?>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>

                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                    $ctn++;
                                }
                                ?>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
<style>
    body {

        margin: 0px;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        color: #4f5252;
        font-weight: 400;
    }
    .container{
        margin-top:50px;
        padding: 10px;
    }
    ul, ol, li {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .reorder_link {
        color: #3675B4;
        border: solid 2px #3675B4;
        border-radius: 3px;
        text-transform: uppercase;
        background: #fff;
        font-size: 18px;
        padding: 10px 20px;
        margin: 15px 15px 15px 0px;
        font-weight: bold;
        text-decoration: none;
        transition: all 0.35s;
        -moz-transition: all 0.35s;
        -webkit-transition: all 0.35s;
        -o-transition: all 0.35s;
        white-space: nowrap;
    }
    .reorder_link:hover {
        color: #fff;
        border: solid 2px #3675B4;
        background: #3675B4;
        box-shadow: none;
    }
    #reorder-helper{
        margin: 18px 10px;
        padding: 10px;
    }
    .light_box {
        background: #efefef;
        padding: 20px;
        margin: 15px 0;
        text-align: center;
        font-size: 1.2em;
    }

    /* image gallery */
    .gallery{ width:100%;  margin-top:100px;}
    .gallery ul{ margin:0; padding:0; list-style-type:none;}
    .gallery ul li{ padding:7px; border:2px solid #ccc;  margin:10px 7px; background:none; width:auto; height:auto;}
    .gallery img{ width:250px;}

    /* notice box */
    .notice, .notice a{ color: #fff !important; }
    .notice { z-index: 8888;padding: 10px;margin-top: 20px; }
    .notice a { font-weight: bold; }
    .notice_error { background: #E46360; }
    .notice_success { background: #657E3F; }
</style>

<!-- BEGIN CORE PLUGINS -->


<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

<script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>

