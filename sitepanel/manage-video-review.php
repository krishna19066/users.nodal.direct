<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$recordID = $_REQUEST[recordID];
//$roomID = $_REQUEST[roomID];
$id = $_REQUEST['id'];
//$conect_cloud = $cloud_data->connectCloudinaryAccount($customerId);
$get_testimonial = $propertyCont->GetTestimonialDataWithId($recordID);
//$get_room = $propertyCont->getRoomDetailData($recordID, $roomID);
//$superAdmin_data = $propertyCont->GetSupperAdminData($customerId);
//$superAdmin_res = $superAdmin_data[0];
?>
<?php
$redirect_script_name = "view-property-rooms.php?recordID=''";
@extract($_REQUEST);

//------------------------Add Testimonial---------------------------------------------------->
if (isset($_POST['add_video_review'])) {
$video_review_link = $_POST['video_review_link'];
$categoryName = $_POST['categoryName'];
//$ins = "insert into testimonials(customerID,review,photo,name,detail) values('$customerId','$review','$timestmp','$name','$occupation')";
// $quer = mysql_query($ins);
$ins = $propertyCont->AddVideoReviewData($customerId, $video_review_link, $categoryName);

echo '<script type="text/javascript">
                alert("Succesfuly Added ");              
window.location = "manage-video-review.php";
            </script>';
exit;
}
if (isset($_POST['update_testimonial'])) {

$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';

Cloudinary::config(array(
"cloud_name" => $cloud_cdnName,
 "api_key" => $cloud_cdnKey,
 "api_secret" => $cloud_cdnSecret
));

$file = $_FILES['image_org']['name'];
$timdat = date('Y-m-d');
$timtim = date('H-i-s');
$timestmp = $timdat . "_" . $timtim;

if ($file) {
$upload = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/testimonials"));
}
$review = $_POST['review'];
$occupation = $_POST['occupation'];
$name = $_POST['name'];
$update_testi = $propertyCont->UpdateTestimonialData($recordID, $timestmp);
echo '<script type="text/javascript">
                alert("Succesfuly Updated ");              
window.location = "edit-testimonials.php?id=edit&recordID=' . $recordID . '";
            </script>';
}
//admin_header();	
?>

<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <br><br><br>
                                <div class="portlet light bordered" style="height: 137px;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Add Video Review</span>
                                            <span class="caption-helper">Add Video Review</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Add Video Review  Those You Show your website!" data-original-title="Add Video Review" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Video Review" /></span>
                                </div>

                                <div class="portlet light bordered" id="add_photo" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Review Video Link
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="video_review_link" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide Review Review Video Link </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Category Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <select name="categoryName" id="property" class="form-control" required="">
                                                            <option value="">--- Select Category ---</option>
                                                            <?php
                                                            $cateList1 = $propertyCont->getTestionialCategory($customerId);
                                                            foreach ($cateList1 as $cat) {
                                                            ?>
                                                            <option value="<?php echo $cat->category_name; ?>"><?php echo $cat->category_name; ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-block"> Provide Review Category</span>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="submit" class="btn green" name="add_video_review" class="button" id="submit" value="Add Review" />

                                                <a href="javascript:history.back()" class="btn red">Go Back</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                            <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->

                            <?php
                            $videoReviewData = $propertyCont->GetAllVideoReviewData($customerId);
                            if ($videoReviewData != NULL) {
                            $cnt = 1;
                            $sl = $start + 1;
                            ?>                                       
                            <div class="row">
                                <div class="col-md-12">	
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div class=" mt-element-overlay">
                                                <div class="row">
                                                    <?php foreach ($videoReviewData as $dataRes) { ?>
                                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                        <div class=" mt-overlay-1" style="height: 20%;">
                                                            <iframe width="100%" height="100%" src="<?php echo $dataRes->review_link;  ?>" frameborder="0" allowfullscreen></iframe>                                                      
                                                           
                                                        </div>
                                                         <span> category:<?php echo $dataRes->category_name; ?></span>
                                                    </div>
                                                    <?php } ?>
                                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                           
                            } else {
                            ?>
                            <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                            </table>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

<?php
admin_footer();
?>