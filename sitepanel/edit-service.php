<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$propertyCont = new propertyData();
$service_Id = $_REQUEST['serviceID'];
$ser_data = $propertyCont->GetServiceDataWithServiceID($customerId, $service_Id);
//print_r($ser_data);
foreach($ser_data as $res){
    
}
?>
<?php
@extract($_REQUEST);

if (isset($_POST['update_service_sub'])) {
  
 $serviceName = $_REQUEST['serviceName'];
 $serviceURL = $_REQUEST['serviceURL'];
 $countryID = $_REQUEST['countryID'];
 $city_id = $_REQUEST['city_id'];
 $serviceType = $_REQUEST['serviceType'];
 $rate_type = $_REQUEST['rate_type'];
 $rate = $_REQUEST['rate'];
 $address = $_REQUEST['address'];
 $description = $_REQUEST['description'];
 $getcountryName = $propertyCont->GetcountryWithID($countryID);
 foreach($getcountryName as $dt){
     $countryName = $dt->name;
 }
 
 $getcityName = $propertyCont->GetcityWithID($city_id);
 foreach($getcityName as $rt){
     $cityName = $rt->name;
 }
    $update = $propertyCont->UpdateServiceData($customerId,$service_Id, $serviceName,$serviceURL,$countryName,$cityName,$countryID,$city_id,$serviceType,$rate_type,$rate,$description,$address,$short_description);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated");              
window.location = "manage-service.php";
            </script>';
}
//admin_header();	
?>

<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">

                                <br><br><br>
                                <div class="portlet light bordered" style="height: 137px;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Listing</span>
                                            <span class="caption-helper">Edit Listing</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Edit Service Those You Show your website!" data-original-title="Upadte Experience" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <span style="float:left;"><a href="manage-service.php"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true" value="List Of Listing" /></a></span>
                                </div>

                                <div class="portlet light bordered">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" action="" id="submit_form" method="post">
                                        <div class="form-wizard"                                          
                                                <!------------------------------------- Tab 1 (Basic Information) Starts ------------------------------------->

                                                <h3 class="block">Basic Listing Details</h3>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Listing Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="propertyName" class="form-control" name="serviceName" value="<?php echo $res->serviceName; ?>"  />
                                                        <span class="help-block"> Provide your Listing name </span>
                                                    </div>
                                                </div>
												 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Listing URL
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="PropertyUrl" class="form-control"  name="serviceURL" value="<?php echo $res->serviceURL; ?>" onkeypress="return AvoidSpace(event)" />
                                                        <span class="help-block"> Provide your desired Listing URL not .html </span>
                                                    </div>

                                                </div>
                                                <div class="form-group">

                                                    <label for="inputState" class="control-label col-md-3">Select Country</label>
                                                    <div class="col-md-4">
                                                        <select name="countryID" id="country" class="form-control" required="">
                                                           <option value="<?php echo $res->country_id; ?>"><?php echo $res->countryName; ?></option>
                                                            <option value="">--- Select Country ---</option>
                                                            <?php
                                                            $countryList1 = $propertyCont->getAllCountryListing();
                                                            foreach ($countryList1 as $countryList) {
                                                                ?>
                                                                <option value="<?php echo $countryList->id; ?>"><?php echo $countryList->name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputState" class="control-label col-md-3">Select City </label>
                                                     <div class="col-md-4">
                                                    <select name="city_id" id="city" class="form-control" required="">
                                                        <option value="<?php echo $res->city_id; ?>"><?php echo $res->cityName; ?></option>
                                                        <option value="">Select Country first</option>                                                                   
                                                    </select>
                                                </div>
                                                </div>
												<div class="form-group">
                                                    <label class="control-label col-md-3">Address
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="propertyName" class="form-control" name="address" value="<?php echo $res->address; ?>"  />
                                                        <span class="help-block"> Provide your address </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                 <label for="inputState" class="control-label col-md-3">Select Service Type</label>
                                                    <div class="col-md-4">
                                                        <select name="serviceType" id="country" class="form-control" required="">
                                                            <option value="<?php echo $res->service_type; ?>"><?php echo $res->service_type; ?></option>
                                                            <option value="">--- Select Type ---</option>                                                          
                                                               <?php
                                                            $serviceList1 = $propertyCont->getAllServiceType();
                                                            foreach ($serviceList1 as $List) {
                                                                ?>
                                                                <option value="<?php echo $List->service_name; ?>"><?php echo $List->service_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                               <!-- <div class="form-group">
                                                    <label class="control-label col-md-3">Room Type
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <table style="width:100%;">
                                                                <tr>
                                                                    <?php
                                                                    $roomTypCnt = 1;
                                                                    $roomTypList1 = $propertyCont->getAllRoomTyp($customerId);
                                                                    foreach ($roomTypList1 as $roomTypList) {
                                                                        ?>
                                                                        <td>
                                                                            <label>
                                                                                <input type="checkbox" name="room_type[]" value="<?php echo $roomTypList->apt_type_name; ?>" data-title="Male" /> <?php echo $roomTypList->apt_type_name; ?> 
                                                                            </label>
                                                                        </td>
                                                                        <?php
                                                                        if ($roomTypCnt % 3 == 0) {
                                                                            echo "</tr><tr>";
                                                                        }
                                                                        $roomTypCnt++;
                                                                    }
                                                                    ?>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="form_gender_error"> </div>
                                                        <span class="help-block">You can select multiple room types</span>
                                                    </div>
                                                </div>-->
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Rate (Price).
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="number" class="form-control" name="rate" value="<?php echo $res->rate; ?>" />
                                                        <span class="help-block">Provide Rate </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Rate Type
                                                    </label>
                                                    <input type="radio" name="rate_type" value="Weekly" <?php if($res->rate_type =='Weekly'){ ?> checked <?php } ?>/>&nbsp;Weekly &nbsp &nbsp &nbsp
                                                    <input type="radio" name="rate_type" value="Monthly" <?php if($res->rate_type =='Monthly'){ ?> checked <?php } ?>/>&nbsp; Monthly

                                                    <span class="help-block"> Select Rate Type</span>
                                                </div>
												  <div class="form-group">
                                                    <label class="control-label col-md-3">Short Description
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
												<input type="text" class="form-control" name="short_description" value="<?php echo $res->short_description; ?>" />
                                                        <span class="help-block">Provide Short Description </span>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label"> Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="description"><?php echo $res->description; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-3 col-md-9">                                                      
                                                        <input type="hidden" name="service_Id" value="<?php echo $service_Id; ?>"/>
                                                        <input type="submit" class="btn green button-submit" name="update_service_sub" value="Submit" />
                                                    </div>
                                                </div>

                                            </div>
                                    </form>
                                <!-- END FORM-->
                            </div>
                            
                      
                    </div>
                </div>
                </body>
                </html>
                <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL PLUGINS -->
                <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                <?php
                admin_footer();
                ?>