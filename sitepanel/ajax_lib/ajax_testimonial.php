<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$staticPageCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$propertyId = $_POST['propId'];

$PropertyList = $ajaxpropertyPhotoCont->getCityList($customerId); /// Get Property data
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId); /// Get Cloud Details
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
//$cloud_cdnKey = $cloud_keyData->api_key;
//$cloud_cdnSecret = $cloud_keyData->api_secret;

$testimonialData = $ajaxpropertyPhotoCont->GetTestimonialListData($customerId, $propertyId);
$reccnt = count($testimonialData);
//$qry = $testimonialData[0];
?>
<html lang="en">
    <head>
        <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="row">
                        <div  class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-equalizer font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Testimonials</span>

                                        <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                    </div>
                                </div>
                                <?php
                                if ($_SERVER['QUERY_STRING'] == true) {
                                    
                                } else {
                                    ?>
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Testimonial" /></span>
                                    <br><br><br><br>
                                    <?php
                                }
                                ?>
                            </div>
                            <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                            <div class="portlet light bordered" id="add_photo" <?php
                            if ($_SERVER['QUERY_STRING'] == true) {
                                
                            } else {
                                ?>style="display:none;" <?php } ?>>			
                                <div class="portlet-body form">
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php">
                                        <div class="form-body">
                                            <h4 align="center" style="color:#E26A6A;"><b> Add Reviewer Photo </b></h3><br><br>

                                                <label for="chkPassport">
                                                    <input type="checkbox" name="SignUp" id="videoReview" /> Video Review					
                                                </label>
                                                <hr/>
                                                <div class="TextReview">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Reviewer Image
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="image_org"> 
                                                                </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a><br>
                                                                <?php
                                                                if ($_SERVER['QUERY_STRING'] == true) {
                                                                    ?>
                                                                    <b>Selected Photo :</b> <a href="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_160,h_160,c_fill/reputize/testimonials/<?php echo $imgres['photo']; ?>.jpg" target="_blank"><?php echo $imgres['photo']; ?></a>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3">Review</label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control" name="review" rows="6">
                                                                <?php
                                                                if ($_SERVER['QUERY_STRING'] == true) {
                                                                    echo html_entity_decode($imgres['review']);
                                                                }
                                                                ?>
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Reviewer Name
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="name" type="text" class="form-control" <?php if ($_SERVER['QUERY_STRING'] == true) { ?> value="<?php echo html_entity_decode($imgres['name']); ?>" <?php } ?>/>
                                                            <span class="help-block"> Provide Reviewer Name</span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Reviewer Brief / Occupation
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="occupation" type="text" class="form-control" <?php if ($_SERVER['QUERY_STRING'] == true) { ?> value="<?php echo html_entity_decode($imgres['detail']); ?>" <?php } ?> />
                                                            <span class="help-block"> Provide Reviewer Brief / Occupation</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dvVideoReview" style="display:none;">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Video Url
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input name="video_review" type="text" class="form-control"/>
                                                            <span class="help-block"> Provide Video Review Url</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php
                                                            if ($_SERVER['QUERY_STRING'] == true) {
                                                                ?>
                                                                <input type="hidden" name="recordID" value="<?php echo $recordID; ?>" />
                                                                <input type="submit" name="update_testimonial" class="btn green" id="submit" value="Update Testimonial">
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="hidden" name="propertyID" value="<?php echo $propertyId; ?>" />
                                                                <input type="submit" name="submit_testimonial" class="btn green" id="submit" value="Save Testimonial">
                                                                <?php
                                                            }
                                                            ?>
                                                            <button type="button" class="btn default">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                        </div>

                        <?php
                        if ($_SERVER['QUERY_STRING'] == true) {
                            
                        } else {
                            if ($reccnt > 0) {
                                $cnt = 1;
                                $sl = $start + 1;
                                foreach ($testimonialData as $dataRes) {
                                    // $dataRes = ms_htmlentities_decode($dataRes);
                                    if ($sl % 2 != 0) {
                                        $color = "#ffffff";
                                    } else {
                                        $color = "#f6f6f6";
                                    }
                                    if ($cnt == "1") {
                                        $color = "#32c5d2";
                                    }
                                    if ($cnt == "2") {
                                        $color = "#3598dc";
                                    }
                                    if ($cnt == "3") {
                                        $color = "#36D7B7";
                                    }
                                    if ($cnt == "4") {
                                        $color = "#5e738b";
                                    }
                                    if ($cnt == "5") {
                                        $color = "#1BA39C";
                                    }
                                    if ($cnt == "6") {
                                        $color = "#32c5d2";
                                    }
                                    if ($cnt == "7") {
                                        $color = "#578ebe";
                                    }
                                    if ($cnt == "8") {
                                        $color = "#8775a7";
                                    }
                                    if ($cnt == "9") {
                                        $color = "#E26A6A";
                                    }
                                    if ($cnt == "10") {
                                        $color = "#29b4b6";
                                    }
                                    if ($cnt == "11") {
                                        $color = "#4B77BE";
                                    }
                                    if ($cnt == "12") {
                                        $color = "#c49f47";
                                    }
                                    if ($cnt == "13") {
                                        $color = "#67809F";
                                    }
                                    if ($cnt == "14") {
                                        $color = "#8775a7";
                                    }
                                    if ($cnt == "15") {
                                        $color = "#73CEBB";
                                    }
                                    ?>

                                    <div class="col-md-12">
                                        <div class="m-heading-1 m-bordered" style="border-left:10px solid <?php echo $color; ?>;border-right:1px solid <?php echo $color; ?>;border-bottom:1px solid <?php echo $color; ?>;border-top:1px solid <?php echo $color; ?>;">

                                            <div style="border:1px solid <?php echo $color; ?>; color:#fff; background:<?php echo $color; ?>; padding-left:12px;">

                                                <p> <b><span style="font-size:large"><?php echo $dataRes->name; ?></span></b>
                                                    <span style="color:#fff; float:right; font-size:x-large">
                                                        <span id="btn_div<?php echo $cnt; ?>">
                                                            <a href="edit-testimonials.php?id=edit&recordID=<?php echo $dataRes->slno; ?>" style="color:white;"><button type="button" name="<?php echo $cnt; ?>" class="btn" style="width:95px;background:black;">Edit</button></a>
                                                        </span>

                                                        <span id="btn_div<?php echo $cnt; ?>">
                                                            <a href="edit-testimonials.php?id=delete&recordID=<?php echo $dataRes->slno; ?>" style="color:white;"><button type="button" name="<?php echo $cnt; ?>" class="btn red" style="width:95px;">Delete</button></a>
                                                        </span>
                                                    </span><br>
                                            </div>

                                            <div class="row" style="padding:10px;">
                                                <div class="col-md-8">	
                                                    <table style="width:100%;">
                                                        <tr>
                                                            <td style="width:180px;">

                                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_160,h_100,c_fill/reputize/testimonials/<?php echo $dataRes->photo; ?>.jpg" />
                                                            </td>

                                                            <td>
                                                                <span>
                                                                    <b>Review: </b> <?php echo strip_tags(trim(ucfirst(substr($dataRes->review, 0, 280)))); ?>.</span>	</br><br>
                                                                <span><b>Occupation: </b><?php echo strip_tags(trim(ucfirst(substr($dataRes->detail, 0, 80)))); ?>.</span>	
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $cnt++;
                                }
                            } else {
                                ?>
                                <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                    <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                </table>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div> 
            </div>
        </div>

        <!-- BEGIN QUICK NAV -->
        <script>
            $(function () {
                $("#videoReview").click(function () {
                    if ($(this).is(":checked")) {
                        $(".TextReview").hide();
                        $(".dvVideoReview").show();
                    } else {
                        $(".TextReview").show();
                        $(".dvVideoReview").hide();
                    }
                });
            });
        </script>

        <div class="quick-nav-overlay"></div>
        <!-- BEGIN CORE PLUGINS -->
        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

        <script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>       
        <?php
        admin_footer();
        exit;
        ?>
    </body>
</html>