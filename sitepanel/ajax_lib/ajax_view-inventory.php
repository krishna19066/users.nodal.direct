<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxinventoryCont = new inventoryData();
?>
<?php
if (isset($_POST['propSelec'])) {
    $propertyName = $_POST['propSelec'];

    // $connectCloudinary = $ajaxpropertyPhotoCont->connectCloudinaryAccount($customerId);
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="favicon.ico" /> 
            <style>
                .header
                {
                    width:100%;
                    height:45px;
                    background:#1ABC9C;
                    padding-top:17px;
                    color:white;
                    font-size:25px;
                    font-weight:bold;
                    font-family:Arial;
                }
                .body
                {
                    width:100%;
                }
                .property
                {
                    width:100%;
                }
                .prop_select
                {
                    width:400px;
                    height:40px;
                    border:1px solid #93b058;
                    border-radius:5px;
                    box-shadow:1px 1px 1px #cde69a;
                    color:#5c6e37;
                    font-weight:bold;
                    font-size:15px;
                    padding-left:10px;
                }
                .prop_submit
                {
                    height:40px;
                    width:110px;
                    background:#05B8CC;
                    color:white;
                    border:none;
                    border-radius:3px;
                    font-weight:bold;
                }
                .rate
                {
                    width:100%;
                }
                .date_range
                {
                    width:100%;
                    border:1px solid #7FA6DD;
                    padding-top:20px;
                    padding-bottom:20px;
                    box-shadow:1px 1px 1px 1px #cbdbf1;
                    background:#f2f6fb;
                }
                .date_table
                {
                    width:100%;
                }
                .date_select
                {
                    width:210px;
                    height:40px;
                    border:1px solid #93b058;
                    border-radius:5px;
                    box-shadow:1px 1px 1px #cde69a;
                    color:#5c6e37;
                    font-weight:bold;
                    font-size:15px;
                    text-align:center;
                }
                .date_submit
                {
                    height:40px;
                    width:140px;
                    background:#05B8CC;
                    color:white;
                    border:none;
                    border-radius:3px;
                    font-weight:bold;
                }
                .room_select
                {
                    width:210px;
                    height:40px;
                    border:1px solid #93b058;
                    border-radius:5px;
                    box-shadow:1px 1px 1px #cde69a;
                    color:#5c6e37;
                    font-weight:bold;
                    font-size:15px;
                    text-align:center;
                }
                .rates
                {
                    width:auto;
                }
                .calendar
                {
                    width:100%;
                    border:1px solid #227298;
                    /*padding-right:10px;*/
                }
                .calendar_table
                {
                    width:100%;

                    border-collapse:collapse;
                    background:#227298;
                    color:white;
                    height:40px;
                }
                .calendar_data
                {
                    width:auto;

                    border-collapse:collapse;
                    background:white;
                    color:black;
                    height:40px;
                }

                .input_css {
                    background: #E26A6A;
                    border: 0px;
                    width: 99%;
                    height: 100%;
                    font-size: 13px;
                    text-align: center;
                }
                .rate_modify1
                {
                    width:90%;
                    padding:20px;
                    border:1px solid #7FA6DD;
                    box-shadow:1px 1px 1px 1px #cbdbf1;
                    background:#f2f6fb;
                }
                .room_select
                {
                    width:210px;
                    height:40px;
                    border:1px solid #93b058;
                    border-radius:5px;
                    box-shadow:1px 1px 1px #cde69a;
                    color:#5c6e37;
                    font-weight:bold;
                    font-size:15px;
                    text-align:center;
                }

            </style>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> PROPERTY DETAILS
                                    <small>View all the details of your Properties</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Revenue Management</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Inventory - <?php echo $propertyName; ?></span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" action="" id="submit_form" method="post">
                                            <div class="form-body">
                                                <h3 class="block">Select a Property</h3>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Property
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <select class="form-control" name="cityName" onchange="select_prop(this.value)">
                                                            <option value="">--- Select ---</option>
                                                            <?php
                                                            $propList1 = $ajaxinventoryCont->getPropertyListing($customerId);
                                                            foreach ($propList1 as $propList) {
                                                                ?>
                                                                <option value="<?php echo $propList->propertyName; ?>"><?php echo $propList->propertyName; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <span class="help-block"> Select your Property for which inventory is to be displayed.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> Inventory - <?php echo $propertyName; ?> </span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="form-horizontal" action="" id="submit_form" method="post">
                                            <div class="form-wizard">
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Property
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="cityName" onchange="select_prop(this.value)">
                                                                <option value="<?php $mon = date('m');
                                                        $year = date('Y');
                                                        echo $mon . "-" . $year; ?>">
                                                                    <?php
                                                                    $mon1 = date('M');
                                                                    $year = date('Y');
                                                                    echo $mon1 . " &nbsp " . $year;
                                                                    ?>
                                                                </option>


                                                                <?php
                                                                for ($i = 1; $i <= 12; $i++) {
                                                                    $mon1 = date('M', strtotime("+$i months"));
                                                                    $mon = date("m", strtotime("+$i months"));
                                                                    $year = date("Y", strtotime("+$i months"));
                                                                    echo "<option value='" . $mon . "-" . $year . "'>" . $mon1 . " &nbsp " . $year . "</option>";
                                                                }
                                                                ?>

                                                            </select>		
                                                            <span class="help-block"> Select month for the Inventory.</span>
                                                        </div>
                                                    </div>

                                                    <div id="new_calendar">
                                                        <div>
                                                            <input  name="next" id="next_month_year" value="<?php echo date('Y-m-d'); ?>" hidden /><span style="float:right; font-size:30px; padding-right:20px;"><a href="javascript:;" onclick="next_month('next')"  class="fa fa-arrow-circle-right"> </a></span>
                                                        </div><br><br>
                                                        <div class="rates">
                                                            <div class="calendar">
                                                                <table class="calendar_table">
                                                                    <tr>
                                                                        <?php
                                                                        $currentMonth = date('m');
                                                                        $monthName = $ajaxinventoryCont->getMonthFullName($currentMonth);
                                                                        ?>

                                                                        <td style="width:110px;background:#cbf1f3;border:0px;padding-left:5px;margin-left:0px;">
                                                                            <span class="caption-subject bold uppercase" style="color:black;"><center><?php echo $monthName . "<br>" . date('Y'); ?></span>  
                                                                        </td>

                                                                        <?php
                                                                        $toBePassedMon = date('m');
                                                                        $toBePassedDate = date('d');

                                                                        $ajaxinventoryCont->getCalendarHeader($toBePassedMon, $toBePassedDate);
                                                                        ?>
                                                                    </tr>
                                                                </table><br>

                                                                <?php
                                                                $propertyDetailsName = $ajaxinventoryCont->getPropDetailsByName($propertyName, $customerId);

                                                                $toBePassedYear = date('Y');

                                                                foreach ($propertyDetailsName as $propertyByNameData) {
                                                                    $propertyID = $propertyByNameData->propertyID;

                                                                    $roomDetails = $ajaxinventoryCont->getRoomDetails($propertyID);

                                                                    echo '<table class="calendar_data">';

                                                                    foreach ($roomDetails as $roomData) {
                                                                        $roomId = $roomData->roomID;
                                                                        $roomType = $roomData->roomType;
                                                                        $roomNo = $roomData->room_no;
                                                                        ?>
                                                                        <tr>
                                                                            <td style="width:110px;padding-left:5px;background:#ffdede;">
                                                                                <span class="caption-subject bold uppercase" style="color:black;"><center> <?php echo $roomType; ?> </center></span>
                                                                            </td>		
            <?php
            $roomInventoryData = $ajaxinventoryCont->getInventoryDetails($propertyID, $roomId, $toBePassedMon, $toBePassedYear);
            $inventoryDataCount = count($roomInventoryData);


            $displayInventory = $ajaxinventoryCont->displayInventory($inventoryDataCount, $propertyID, $roomType, $roomNo, $toBePassedMon, $toBePassedDate, $roomInventoryData);
            ?>
                                                                        </tr>
                                                                        <tr><td style="height:10px;"></td></tr>
                                                                            <?php
                                                                        }
                                                                        echo '</table>';
                                                                    }
                                                                    ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            
            <div class="page-footer">
                <div class="page-footer-inner"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
            <!-- BEGIN QUICK NAV -->
            <nav class="quick-nav">
                <a class="quick-nav-trigger" href="#0">
                    <span aria-hidden="true"></span>
                </a>
                <ul>
                    <li>
                        <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                            <span>Purchase Metronic</span>
                            <i class="icon-basket"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                            <span>Customer Reviews</span>
                            <i class="icon-users"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://keenthemes.com/showcast/" target="_blank">
                            <span>Showcase</span>
                            <i class="icon-user"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                            <span>Changelog</span>
                            <i class="icon-graph"></i>
                        </a>
                    </li>
                </ul>
                <span aria-hidden="true" class="quick-nav-bg"></span>
            </nav>
            <div class="quick-nav-overlay"></div>

            <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
            <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
            <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
            <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
            <!-- END THEME LAYOUT SCRIPTS -->

            <!-- BEGIN CORE PLUGINS -->
            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        </body>
    </html>
    <?php
}
?>