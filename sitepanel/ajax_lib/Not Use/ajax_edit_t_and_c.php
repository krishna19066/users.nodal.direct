<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
?>
<?php
if (isset($_POST['pageUrlData'])) {
    $pageUrl = $_POST['pageUrlData'];
    $clientPageData = $ajaxclientCont->getTermsAndConditionsPageData($customerId);    
    $terms = $clientPageData[0];
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

            <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
            <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>           
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

            <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>

            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="favicon.ico" /> 
            <link href="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />        			
            <link href="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />        		

            <link href="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />        		
            <!-- END PAGE LEVEL PLUGINS -->				
            <!-- BEGIN PAGE LEVEL STYLES -->        			
            <link href="http://www.hawthorntech.com/sitepanel/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />  

        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>


            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Terms And Conditions</span>
                                            <span class="caption-helper">Add/Edit Terms and Conditions Page Here..</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">											<input type="hidden" name="querystr" value="<?php echo $querystr; ?>" />
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="heading" value="<?php echo $terms->heading; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Terms & Conditions Content
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="content" rows="6"><?php echo $terms->content; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <input type="submit" name="edit_terms_and_conditions" class="btn green" id="submit" value="Save">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>					
                                        </form>
                                    </div>																
                                   

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                function team_mem(sln)
                {
                    var mydata = sln;
                    $.ajax({url: 'ajax_change_status.php',
                        data: {edit_team: mydata},
                        type: 'post',
                        success: function (output) {
                            $('#to_be_repl').html(output);
                        }
                    });
                }
            </script>

            <div class="quick-nav-overlay"></div>

            <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
            <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>         
            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>           
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>         
            <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>            
            <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
            <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
            <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
            <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
            <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
            <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

            <script src="http://www.hawthorntech.com/sitepanel/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
            <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>	
            <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->	
        
        </body>
    </html>
    <?php
}
?>