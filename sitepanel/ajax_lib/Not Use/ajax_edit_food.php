<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
?>
<?php
if (isset($_POST['pageUrlData'])) {
    $pageUrl = $_POST['pageUrlData'];
    $clientPageData = $ajaxclientCont->getFoodPageData($customerId);
    $FoodPageData = $ajaxclientCont->getFoodPageMoreData($customerId);
    $pageData = $clientPageData[0];
    $numb = count($clientPageData);
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="favicon.ico" /> 
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">

                    <!-- BEGIN CONTENT BODY -->

                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Food & Beverages Page</span>

                                            <span class="caption-helper">Add/Edit Food & Beverages Page Here..</span>

                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-body ">
                                                    <div style="width:100%;" class="clear"> 
                                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:227px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Food & Restaurant  Photo" /></span>                                         
                                                        <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>

                                                        <br><br><br><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                                    <div class="portlet light bordered" id="add_photo" style="display:none;">			
                                        <div class="portlet-body form">
                                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php">
                                                <div class="form-body">
                                                    <h4 align="center" style="color:#E26A6A;"><b> Add New  Photo </b></h3><br><br>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Food Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="image_org"> </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Image Alt Tag
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input name="imageAlt1" type="text" class="form-control" />
                                                                <span class="help-block"> Provide your image alt tag</span>
                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <input name="recordID" type="hidden" value="<?php echo $propertyId ?>" />
                                                                    <input type="submit" name="submit_food_pic" class="btn green" id="submit" value="Add Photo">
                                                                    <button type="button" class="btn default">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </form>
                                            <div class="col-md-12">	
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div class=" mt-element-overlay">
                                                            <div class="row">
                                                                <?php
                                                                $FoodPhotoListdata = $ajaxclientCont->getFoodGalleryPhoto($customerId);

                                                                foreach ($FoodPhotoListdata as $PhotoList) {
                                                                    ?>
                                                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                                                        <div class=" mt-overlay-1">
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_564,h_400,c_fill/<?php if ($customerId != 1) { ?>reputize<?php } ?>/food_gallery/<?php echo $PhotoList->image; ?>.jpg" />

                                                                            <div class="mt-overlay">
                                                                                <ul class="mt-info">
                                                                                    <!--<li>
                                                                                        <a class="btn default btn-outline" href="javascript:;">
                                                                                            <i class="icon-magnifier"></i>
                                                                                        </a>
                                                                                    </li>-->
                                                                                    <!--  <li>
                                                                                          <a class="btn default btn-outline" href="../sitepanel/add_property.php?action=delete&imagesID=<?php echo $PhotoList->id; ?>">
                                                                                              <i class="icon-trash"></i>
                                                                                          </a>
                                                                                      </li>-->
                                                                                </ul>                                                                              
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h1" value="<?php echo $pageData->h1; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Sub-Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="subhead1" value="<?php echo $pageData->subhead1; ?>" required />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Heading Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="heading_content" rows="6" required ><?php echo html_entity_decode($pageData->box1content); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Header Image
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image_header"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Upload pdf Food Menu
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" type="file" name="file" />
                                                    </div>
                                                </div> 

                                                <!-- <div class="form-group">
                                                     <label class="control-label col-md-3"> Box 1 Icon
                                                         <span class="required"> * </span>
                                                     </label>

                                                     <div class="col-md-4">
                                                         <input type="text" class="form-control" name="box1icon" value="<?php echo htmlspecialchars(html_entity_decode($pageData->box1icon)); ?>" required />
                                                     </div>
                                                 </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Box 1 Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box1head" value="<?php echo $pageData->box1head; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Box 1 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="box1content" rows="6" required ><?php echo html_entity_decode($pageData->box1content); ?></textarea>
                                                    </div>
                                                </div>

                                                <!-- <div class="form-group">
                                                     <label class="control-label col-md-3"> Box 2 Icon
                                                         <span class="required"> * </span>
                                                     </label>

                                                     <div class="col-md-4">
                                                         <input type="text" class="form-control" name="box2icon" value="<?php echo htmlspecialchars(html_entity_decode($pageData->box2icon)); ?>" required />
                                                     </div>
                                                 </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Box 2 Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box2head" value="<?php echo $pageData->box2head; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Box 2 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="box2content" rows="6" required ><?php echo html_entity_decode($pageData->box2content); ?></textarea>
                                                    </div>
                                                </div>

                                                <!--<div class="form-group">
                                                    <label class="control-label col-md-3"> Box 3 Icon
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box3icon" value="<?php echo htmlspecialchars(html_entity_decode($pageData->box3icon)); ?>" required />
                                                    </div>
                                                </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Box 3 Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="box3head" value="<?php echo $pageData->box3head; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Box 3 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="box3content" rows="6" required ><?php echo html_entity_decode($pageData->box3content); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h2" value="<?php echo $pageData->h2; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Sub-Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="subhead2" value="<?php echo $pageData->subhead2; ?>" />
                                                    </div>
                                                </div>

                                                <!-- <div class="form-group">
                                                     <label class="control-label col-md-3"> Food Menu
                                                         <span class="required"> * </span>
                                                     </label>
                                                     <div class="fileinput fileinput-new" data-provides="fileinput">
                                                         <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                             <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                         <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                         <div>
                                                             <span class="btn default btn-file">
                                                                 <span class="fileinput-new"> Select image </span>
                                                                 <span class="fileinput-exists"> Change </span>
                                                                 <input type="file" name="image1"> </span>
                                                             <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                         </div>
                                                         Uploaded Image :- <a href="<?SITE_DIR_PATH;?>/images/<?php echo $res['image4']; ?>" target="_blank"><?php echo $res['image4']; ?></a>
                                                     </div>
                                                 </div>-->

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Question Div Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="qadivhead" value="<?php echo $pageData->qaheading; ?>" />
                                                    </div>
                                                </div>

                                                <?php
                                                $selquesCnt = 1;
                                                //  $selquesAns = "select * from static_page_food where customerID='$custID' order by s_no asc";
                                                //  $selquesAnsQuer = mysql_query($selquesAns);
                                                foreach ($FoodPageData as $row) {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Box <?php echo $selquesCnt; ?> Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="question[]" value="<?php echo $row->question; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Box <?php echo $selquesCnt; ?> Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="answer[]" rows="6" ><?php echo html_entity_decode($row->answer); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $selquesCnt++;
                                                }
                                                ?>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 3
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="head3" value="<?php echo $pageData->head3; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Virtual Tour Iframe SRC
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="iframeSrc" value="<?php echo $pageData->iframe; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php
                                                            if ($numb == "0") {
                                                                ?>
                                                                <input type="hidden" name="typeofsave" value="newaddition" />
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="hidden" name="typeofsave" value="updation" />
                                                                <?php
                                                            }
                                                            ?>
                                                            <input type="hidden"  name="page_url" value="<?php echo $pageData->page_url; ?>" />
                                                            <input type="submit" name="foodsave" class="btn green" id="submit" value="Save">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                                <!--    <span style="float:left;">
                                                        <input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:240px;font-size:14px;" data-html="true"  onclick="$('#add_more_ques').slideToggle();" value="Add More Tabs" />
                                                    </span><br><br>

                                                    <div class="portlet-body form" id="add_more_ques" style="display:none;">
                                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3"> Question
                                                                        <span class="required"> * </span>
                                                                    </label>

                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control" name="addques" required />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group last">
                                                                    <label class="control-label col-md-3"> Answer
                                                                        <span class="required"> * </span>
                                                                    </label>

                                                                    <div class="col-md-9">
                                                                        <textarea class="ckeditor form-control"  name="addans" rows="6" required ></textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <input type="submit" name="addmoreques" class="btn green" id="submit" value="Add">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
            <div class="quick-nav-overlay"></div>

            <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
            <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
            <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
            <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
            <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
            <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>          
            <script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        </body>
    </html>
    <?php
}
?>