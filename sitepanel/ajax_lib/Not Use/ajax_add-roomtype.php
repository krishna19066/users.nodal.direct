<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_roomtype();"><i class="fa fa-plus"></i> &nbsp Add Room Type</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Add Room Type </span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" action="../sitepanel/add_property.php" id="submit_form" method="post">
                    <div class="form-wizard">
                        <div class="form-body">
                            <div class="tab-content"> 
                                <h3 class="block">Add Room Type</h3>

                                <div class="form-group">
                                    <label class="control-label col-md-3">  Room Type
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="room_typeName" required />

                                        <span class="help-block"> Provide your Room Type.</span>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back 
                                    </a>
                                    <input type="submit" class="btn green" name="add_room_type" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><br><br>