<?php
include "../admin-function.php";
//$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$customerId = $_SESSION['customerID'];
$analyticsCont = new analyticsPage();
?>
<?php
//$pageUrl = $_POST['pageUrlData'];
$metatagPageData = $ajaxclientCont->getMetaTagsData($customerId);
//print_r($metatagPageData);
$Metapage = $metatagPageData[0];
$numb = count($Metapage);
$cityUrlData = $ajaxclientCont->getCityUrlData($customerId);
$city_url = $cityUrlData[0];
$staticPageUrlData = $ajaxclientCont->getStaticPageUrlData($customerId);
$propertyPageUrlData = $ajaxclientCont->getPropertyPageUrlData($customerId);
$roomPageUrlData = $ajaxclientCont->getRoomPageUrlData($customerId);
?>
<!DOCTYPE html>
<html lang="en">  
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <div class="page-content" style="margin-left: 0;">
            <div class="row">
                <div class="col-md-12">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer font-red-sunglo"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase">Add New Meta Tags</span>
                                <span class="caption-helper">Please Add new Meta Tags</span>
                                <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->

                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;">
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3">File Url
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="filename" >
                                                <?php
                                                foreach ($metatagPageData as $meta) {
                                                    $selected_url = $meta->filename;
                                                }

                                                $numrowarr = '';
                                                $inurlchecknum = count($Metapage);
                                                ;
                                                $numrowarr .= $inurlchecknum . ",";
                                                if ($inurlchecknum > 0) {
                                                    
                                                } else {
                                                    ?>
                                                    <option <?php
                                                    if ($selected_url == 'index') {
                                                        echo "selected";
                                                    }
                                                    ?>>index</option>
                                                        <?php
                                                    }
                                                    ?>
                                                <!--------------------------------------------- Service Apartment URL ----------------------------------------------->
                                                <?php
                                                //$opt_sel = "select cityUrl from city_url where customerID='$custID' and status='Y' and cityUrl!=''";
                                                // $opt_quer = $city_url;
                                                foreach ($cityUrlData as $city) {
                                                    $saur = $city->cityUrl;
                                                    $metatagfilename = $ajaxclientCont->getMetaTagsByFileName($customerId, $saur);

                                                    //$surlchecksel = "select * from metaTags where filename='$saur'";
                                                    //  $surlcheckquery = mysql_query($surlchecksel);
                                                    $surlchecknum = count($metatagfilename);
                                                    $numrowarr .= $surlchecknum . ",";
                                                    if ($surlchecknum > 0) {
                                                        
                                                    } else {
                                                        ?>
                                                        <option <?php
                                                        if ($saur == $selected_url) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $saur; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>


                                                <!---------------------------------------------- Static Pages URL ---------------------------------------------->		
                                                <?php
                                                // $opt_sel1 = "select page_url from static_pages where customerID='$custID' and status='Y' and type='desktop' and page_url!=''";
                                                //$opt_quer1 = mysql_query($opt_sel1);
                                                foreach ($staticPageUrlData as $opt_row1) {
                                                    $orow = $opt_row1->page_url;
                                                    $metatagfilename = $ajaxclientCont->getMetaTagsByFileName($customerId, $orow);
                                                    $purlchecknum = count($metatagfilename);
                                                    $numrowarr .= $purlchecknum . ",";
                                                    if ($purlchecknum > 0) {
                                                        
                                                    } else {
                                                        ?>
                                                        <option <?php
                                                        if ($orow == $selected_url) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $orow; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                <!---------------------------------------------- Property Pages URL ---------------------------------------------->		
                                                <?php
                                                //  $opt_sel2 = "select propertyURL from propertyTable where customerID='$custID' and status='Y' and propertyURL!=''";
                                                // $opt_quer2 = mysql_query($opt_sel2);
                                                foreach ($propertyPageUrlData as $opt_row2) {
                                                    $prorow = $opt_row2->propertyURL;
                                                    $metatagfilename = $ajaxclientCont->getMetaTagsByFileName($customerId, $prorow);
                                                    $prourlchecknum = count($metatagfilename);
                                                    $numrowarr .= $prourlchecknum . ",";
                                                    if ($prourlchecknum > 0) {
                                                        
                                                    } else {
                                                        ?>
                                                        <option <?php
                                                        if ($prorow == $selected_url) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $prorow; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                <!---------------------------------------------- Property Pages URL ---------------------------------------------->		
                                                <?php
                                                // $opt_sel3 = "select room_url from propertyRoom where customerID='$custID' and status='Y' and room_url!=''";
                                                // $opt_quer3 = mysql_query($opt_sel3);
                                                foreach ($roomPageUrlData as $opt_quer3) {
                                                    $rrorow = $opt_row3->room_url;
                                                    $metatagfilename = $ajaxclientCont->getMetaTagsByFileName($customerId, $rrorow);
                                                    // $rourlchecksel = "select * from metaTags where filename='$rrorow' and customerID='$custID'";
                                                    //  $rourlcheckquery = mysql_query($rourlchecksel);
                                                    $rourlchecknum = count($metatagfilename);
                                                    $numrowarr .= $rourlchecknum . ",";
                                                    if ($rourlchecknum > 0) {
                                                        
                                                    } else {
                                                        ?>
                                                        <option <?php
                                                        if ($rrorow == $selected_url) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $rrorow; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Meta Title
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="MetaTitle" value="" />
                                            <span class="help-block"> Provide Meta Title </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Meta Description</label>
                                        <div class="col-md-9">
                                            <textarea class="ckeditor form-control" rows="3" name="MetaDisc"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Meta Keyword</label>
                                        <div class="col-md-9">
                                            <textarea class="ckeditor form-control" rows="3" name="MetaKwd"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Other Code</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" rows="3" name="othercode"></textarea>
                                        </div>  
                                    </div>   
                                    <input type="submit" style="margin-left: 398px;" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Tags" />

                            </form>

                            <!-- END FORM-->
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN FOOTER -->
    
</body>
</html>
