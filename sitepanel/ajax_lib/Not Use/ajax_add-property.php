<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $("input[name='tbl_property_type']").click(function () {

                    if ($("#radio-1-0").is(":checked")) {
                        $("#pgTypes").show();
                        $("#pgTypesHideDiv").hide();
                    } else {
                        $("#pgTypes").hide();
                        $("#pgTypesHideDiv").show();
                    }

                });
            });
            $(function () {
                $("input[name='bookingengiType']").click(function () {

                    if ($("#external").is(":checked")) {
                        $(".bookingEngine").show();
                       
                    } else {
                        $(".bookingEngine").hide();
                       
                    }

                });
            });
        </script>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add New Property
                                <small>Add a new property here..</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="index.html">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Property (Add Property)</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Fill The Form -
                                            <span class="step-title"> Step 1 of 4 </span>
                                        </span>
                                    </div>                                   
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="../sitepanel/add_property.php" id="submit_form" method="post">
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <ul class="nav nav-pills nav-justified steps">
                                                    <li>
                                                        <a href="#tab1" data-toggle="tab" class="step">
                                                            <span class="number"> 1 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Basic Info </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab2" data-toggle="tab" class="step">
                                                            <span class="number"> 2 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Location</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab3" data-toggle="tab" class="step active">
                                                            <span class="number"> 3 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i>Property Features</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab4" data-toggle="tab" class="step active">
                                                            <span class="number"> 4 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Social Links </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab5" data-toggle="tab" class="step">
                                                            <span class="number"> 5 </span>
                                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Confirm </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div id="bar" class="progress progress-striped" role="progressbar">
                                                    <div class="progress-bar progress-bar-success"> </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="alert alert-danger display-none">
                                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                    <div class="alert alert-success display-none">
                                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! 
                                                    </div>

                                                    <!------------------------------------- Tab 1 (Basic Information) Starts ------------------------------------->
                                                    <div class="tab-pane active" id="tab1">
                                                        <h3 class="block">Basic Property Details</h3>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="propertyName" />
                                                                <span class="help-block"> Provide your property name </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property URL
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="propertyURL" />
                                                                <span class="help-block"> Provide your desired property URL not .html </span>
                                                            </div>
                                                          
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Type
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <?php
                                                                $propTypList1 = $propertyCont->getAllPropTyp($customerId);
                                                                foreach ($propTypList1 as $propTypList) {
                                                                    ?>
                                                                    <input type="radio" name="tbl_property_type"<?php if ($propTypList->type_name == "PG") { ?> id="radio-1-0"<?php } ?> value="<?php echo $propTypList->type_id; ?>" /> &nbsp; <?php echo $propTypList->type_name; ?>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <span class="help-block"> Select your property type </span>
                                                            </div>
                                                        </div><br/>
                                                        <div id="pgTypes" style="<?php if ($propTypList->type_name == "PG") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Select PG Types
                                                                </label>
                                                                <input type="radio" name="pg_type" value="Girls PG"/>&nbsp; Girls PG &nbsp &nbsp
                                                                <input type="radio" name="pg_type" value="Boys PG"/>&nbsp; Boys PG &nbsp &nbsp
                                                                <input type="radio" name="pg_type" value="Both"/>&nbsp; Both
                                                                <span class="help-block"> Select your PG type </span>
                                                            </div>
                                                        </div><br/>
                                                        
                                                         <div id="pgTypesHideDiv">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Check in
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="number" class="form-control" name="checkIN" />
                                                                    <span class="help-block"> Provide Check In Time (24 Hrs. Format) </span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Check out
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="number" class="form-control" name="checkOut" />
                                                                    <span class="help-block"> Provide Check Out Time (24 Hrs. Format) </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Manager Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="managerName" />
                                                                <span class="help-block"> Name of Property Manager </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Manager Mobile No.
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" name="managerContactNO" />
                                                                <span class="help-block">Phone Number of Property Manager </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Phone No.
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" name="propertyPhone" />
                                                                <span class="help-block">Phone Number of Property Reception </span>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Property Email.
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="property_email" />
                                                                <span class="help-block">Email of Property Reception </span>
                                                            </div>
                                                        </div>
                                                          <div class="form-group">
                                                            <label class="control-label col-md-3">Room Type
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <div class="radio-list">
                                                                    <table style="width:100%;">
                                                                        <tr>
                                                                            <?php
                                                                            $roomTypCnt = 1;
                                                                            $roomTypList1 = $propertyCont->getAllRoomTyp($customerId);
                                                                            foreach ($roomTypList1 as $roomTypList) {
                                                                                ?>
                                                                                <td>
                                                                                    <label>
                                                                                        <input type="checkbox" name="room_type[]" value="<?php echo $roomTypList->apt_type_name; ?>" data-title="Male" /> <?php echo $roomTypList->apt_type_name; ?> 
                                                                                    </label>
                                                                                </td>
                                                                                <?php
                                                                                if ($roomTypCnt % 3 == 0) {
                                                                                    echo "</tr><tr>";
                                                                                }
                                                                                $roomTypCnt++;
                                                                            }
                                                                            ?>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="form_gender_error"> </div>
                                                                <span class="help-block">You can select multiple room types</span>
                                                            </div>
                                                        </div>

                                                         
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Featured
                                                            </label>
                                                            <input type="radio" name="featured" value="Y"/>&nbsp;Yes &nbsp &nbsp &nbsp
                                                            <input type="radio" name="featured" value="N"/>&nbsp; NO

                                                            <span class="help-block"> Provide Property Featured </span>
                                                        </div>  

                                                    </div>

                                                    <!-------------------------------------- Tab 1 (Basic Information) Ends -------------------------------------->


                                                    <!---------------------------------- Tab 2 (Property Information) Starts ---------------------------------->													
                                                    <div class="tab-pane" id="tab2">
                                                        <h3 class="block">Provide your Property Location</h3>
                                                        <!--<div class="form-group">
                                                            <label class="control-label col-md-3">No. Of Floors
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="floorsno" />
                                                                <span class="help-block"> No. of Floors in your property </span>
                                                            </div>
                                                        </div>-->
                                                        
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Address
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="address" />
                                                                <span class="help-block"> Provide your property address </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Enter Locality
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="locality"/>                                                                                                                           
                                                                <span class="help-block">Provide your property Locality </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">City
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="cityName">
                                                                    <?php
                                                                    $cityList1 = $propertyCont->getAllCity($customerId);
                                                                    foreach ($cityList1 as $cityList) {
                                                                        ?>
                                                                        <option value="<?php echo $cityList->cityName; ?>"><?php echo $cityList->cityName; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="help-block"> Provide your city name.</span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">State
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="state" />
                                                                <span class="help-block"> Provide your property state </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country</label>
                                                            <div class="col-md-4">
                                                                <select name="country" id="country_list" class="form-control">
                                                                    <option value=""></option>
                                                                    
                                                                    <option value="India">India</option>
                                                                  
                                                                </select>
                                                            </div>
                                                        </div><br/>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Display Map Address
                                                            </label>
                                                            <input type="radio" name="displayMap" value="show"/>&nbsp;Yes
                                                            <input type="radio" name="displayMap" value="hide"/>&nbsp; NO

                                                            <span class="help-block">Display Map Address </span>
                                                        </div>  <br/>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Google Map URL
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="gmapurl" />
                                                                <span class="help-block"> Provide Google Map URL of your Property </span>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Near by Locality
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="nearBy"></textarea>
                                                                <span class="help-block"> Provide deatils of nearby locality to your property </span>
                                                            </div>
                                                        </div>

                                                       
                                                    </div>

                                                    <!------------------------------------ Tab 2 (Property Information) Ends ------------------------------------>


                                                    <!------------------------------------ Tab 3 (Property Features) Starts ------------------------------------>

                                                    <div class="tab-pane" id="tab3">
                                                        <h3 class="block">Provide your property features</h3><br/>
                                                        
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Property Amenities
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <div class="radio-list">
                                                                    <table style="width:100%;">
                                                                        <tr>
                                                                            <?php
                                                                            $amntyCnt = 1;
                                                                            $amenityList1 = $propertyCont->getAmenitiesList();
                                                                            foreach ($amenityList1 as $amenityList) {
                                                                                ?>
                                                                                <td>
                                                                                    <label>
                                                                                        <input type="checkbox" name="room_amenity[]" value="<?php echo $amenityList->roomAmenties_name; ?>" data-title="Male" /> <?php echo $amenityList->roomAmenties_name; ?> 
                                                                                    </label>
                                                                                </td>
                                                                                <?php
                                                                                if ($amntyCnt % 3 == 0) {
                                                                                    echo "</tr><tr>";
                                                                                }
                                                                                $amntyCnt++;
                                                                            }
                                                                            ?>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="form_gender_error"> </div>
                                                                <span class="help-block">You can select multiple amenities</span>
                                                            </div>
                                                        </div>
                                                        <br/>                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Overview
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="propertyInfo"></textarea>
                                                                <span class="help-block"> Provide basic overview of your property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Policy
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="propertyPolicy"></textarea>
                                                                <span class="help-block"> Provide policies of your property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Features
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="propertyfeatures"></textarea>
                                                                <span class="help-block"> Provide important features of your property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Services & Amenities
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="servicesnamenities"></textarea>
                                                                <span class="help-block"> Provide services & amenities of your property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Safety and Security
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="safetynsecurity"></textarea>
                                                                <span class="help-block"> Provide security policies of your property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">In Apartment Facilities
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="inapartmentfacilities"></textarea>
                                                                <span class="help-block"> Provide In-apartment Features </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Kitchen Features
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="kitchenfeatures"></textarea>
                                                                <span class="help-block"> Provide dining details of your property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Entertainment Leisure
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-6">
                                                                <textarea class="ckeditor form-control" name="entertainmentleisure"></textarea>
                                                                <span class="help-block"> Provide entertainment & leisure facilities of your property </span>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>

                                                    <!------------------------------------ Tab 3 (Property Features) Ends ------------------------------------>	



                                                    <!------------------------------------ Tab 4 (Important URLS) Starts ------------------------------------>

                                                    <div class="tab-pane" id="tab4">
                                                        <h3 class="block">Provide important links and URLs</h3>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Trust You URL
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="trustURL" />
                                                                <span class="help-block"> Trust You URL of your property </span>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Select Booking Engine
                                                            </label>
                                                             <input type="radio" name="bookingengiType" id="internal" value="internal" checked=""/>&nbsp;Internal
                                                             <input type="radio" name="bookingengiType" id="external" value="external"/>&nbsp; External
                                                             <input type="radio" name="bookingengiType" id="none" value="none"/>&nbsp; None

                                                            <span class="help-block">Choose Booking Engine</span>
                                                        </div>  <br/>

                                                        <div class="form-group bookingEngine" style="display:none;">
                                                            <label class="control-label col-md-3">Booking URL
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="bookingURL" />
                                                                <span class="help-block"> Provide Booking URL of your Property </span>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Video URL
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="videoURL" />
                                                                <span class="help-block"> Provide Video/Youtube URL of your Property </span>
                                                            </div>
                                                        </div>


                                                       <!-- <div class="form-group">
                                                            <label class="control-label col-md-3">Property Tag Line
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="tagLine" />
                                                                <span class="help-block"> Provide your property Tag Line </span>
                                                            </div>
                                                        </div>-->


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Twitter Link
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="propertyTwitterLink" />
                                                                <span class="help-block"> Provide Twitter page Link for your property</span>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Facebook Link
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="propertyFacebookLink" />
                                                                <span class="help-block"> Provide Faceboom page link your Property </span>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Linkedin Link
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="propertyLinkedinLink" />
                                                                <span class="help-block"> Provide Linkedin page link for your Property </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Google Link
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="propertyGoogleLink" />
                                                                <span class="help-block"> Provide Google link for your Property </span>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <!------------------------------------ Tab 4 (Important URLS) Ends ------------------------------------>



                                                    <!------------------------------------ Tab 5 (Confirmation) Starts ------------------------------------>

                                                    <div class="tab-pane" id="tab5">
                                                        <h3 class="block">Confirm your information</h3>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3" >Are you sure you want to submit </label>
                                                           <div class="col-md-4">
                                                               <input type="checkbox" class="form-control" name="confirm" required="" style="margin-left: -250px;"/>  
                                                           </div>
                                                       
                                                        </div>   
                                                            
                                                  
                                                     <!--   <h4 class="form-section">Basic Information</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyName"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property URL:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyURL"> </p>
                                                            </div>
                                                        </div>
                                                          <div class="form-group">
                                                            <label class="control-label col-md-3">Check in:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="checkIN"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Check out:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="checkOut"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Manager Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="managerName"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Manager Mobile No.:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="managerContactNO"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Phone No.:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyPhone"> </p>
                                                            </div>
                                                        </div>

                                                        <h4 class="form-section">Property Location</h4>
                                                      
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Room Type:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="room_type"> </p>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Address:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="address"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">City:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="cityName"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">State:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="state"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="country"> </p>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Near by Locality:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="nearBy"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Amenities:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="room_amenity"> </p>
                                                            </div>
                                                        </div>
                                                        
                                                        <h4 class="form-section"> Property Features</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Overview:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyInfo"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Policy:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyPolicy"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Features:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyfeatures"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Services & Amenities:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="servicesnamenities"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Safety and Security:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="safetynsecurity"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">In Apartment Facilities:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="inapartmentfacilities"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Kitchen Features:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="kitchenfeatures"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Entertainment Leisure:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="entertainmentleisure"> </p>
                                                            </div>
                                                        </div>

                                                        <h4 class="form-section"> Social Links & URL</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Trust You URL:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="trustURL"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Booking URL:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="bookingURL"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Google Map URL:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="gmapurl"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Video URL:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="videoURL"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Twitter Link:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyTwitterLink"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Facebook Link:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyFacebookLink"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Linkedin Link:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyLinkedinLink"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Property Google Link:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="propertyGoogleLink"> </p>
                                                            </div>
                                                        </div>-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="javascript:;" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back </a>
                                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>
                                                        <input type="submit" class="btn green button-submit" name="add_prop_sub" value="Submit" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
          
           
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2018 &copy; 
                <a target="_blank" href="nodal.direct">Howthorn Technology Pvt Ltd</a> &nbsp;|&nbsp;
                
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN QUICK NAV -->
        

        <div class="quick-nav-overlay"></div>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>         
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>           
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>         
        <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>            
        <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

        <script src="http://www.hawthorntech.com/sitepanel/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>	
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>