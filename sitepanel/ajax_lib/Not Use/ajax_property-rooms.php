<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyRoomCont = new propertyData();
$cloud_keySel = $ajaxpropertyRoomCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
?>

<?php
if (isset($_POST['propRumId'])) {
    $propertyId = $_POST['propRumId'];

    $connectCloudinary = $ajaxpropertyRoomCont->connectCloudinaryAccount($customerId);
    $roomDetail = $ajaxpropertyRoomCont->getRoomDetail($propertyId);
    ?>
    <!DOCTYPE html>
    <html lang="en">
       <head>
        <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> ROOM DETAILS
                                <small>View all the details of your rooms</small>
                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="welcome.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View Property Rooms</span>
                        </li>
                    </ul>

                    <div  class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-equalizer font-red-sunglo"></i>
                                    <span class="caption-subject font-red-sunglo bold uppercase"><?= $propertyRes[propertyName]; ?></span>
                                    <span class="caption-helper"><?= $propertyRes[propertyName]; ?></span>
                                    <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                </div>
                            </div>
                            <span style="float:left;"> <a  onclick="load_add_rooms('<?php echo $propertyId; ?>');"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Add Rooms</button></a></span>

                            <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>

                            <br><br><br><br>
                        </div>
                    </div>

                    <div class="blog-page blog-content-1">
                        <div class="row">
                            <?php
                            $roomDetail = $ajaxpropertyRoomCont->getRoomDetail($propertyId);

                            foreach ($roomDetail as $roomData) {
                                $roomId = $roomData->roomID;
                                $roomPhotoDetail = $ajaxpropertyRoomCont->getRoomPhoto($propertyId, $roomId, "and type='home' order by imagesID desc limit 1");
                                $roomPhotoData = $roomPhotoDetail[0];
                                ?>

                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        $cnt = 1;
                        $sl = $start + 1;
                        foreach ($roomDetail as $dataRes) {
                            // $dataRes = ms_htmlentities_decode($dataRes);
                            if ($sl % 2 != 0) {
                                $color = "#ffffff";
                            } else {
                                $color = "#f6f6f6";
                            }
                            if ($cnt == "1") {
                                $color = "#32c5d2";
                            }
                            if ($cnt == "2") {
                                $color = "#3598dc";
                            }
                            if ($cnt == "3") {
                                $color = "#36D7B7";
                            }
                            if ($cnt == "4") {
                                $color = "#5e738b";
                            }
                            if ($cnt == "5") {
                                $color = "#1BA39C";
                            }
                            if ($cnt == "6") {
                                $color = "#32c5d2";
                            }
                            if ($cnt == "7") {
                                $color = "#578ebe";
                            }
                            if ($cnt == "8") {
                                $color = "#8775a7";
                            }
                            if ($cnt == "9") {
                                $color = "#E26A6A";
                            }
                            if ($cnt == "10") {
                                $color = "#29b4b6";
                            }
                            if ($cnt == "11") {
                                $color = "#4B77BE";
                            }
                            if ($cnt == "12") {
                                $color = "#c49f47";
                            }
                            if ($cnt == "13") {
                                $color = "#67809F";
                            }
                            if ($cnt == "14") {
                                $color = "#8775a7";
                            }
                            if ($cnt == "15") {
                                $color = "#73CEBB";
                            }
                            ?>

                            <div class="col-md-12">
                                <div class="m-heading-1 m-bordered" style="border-left:10px solid <?php echo $color; ?>;border-right:1px solid <?php echo $color; ?>;border-bottom:1px solid <?php echo $color; ?>;border-top:1px solid <?php echo $color; ?>;">
                                    <div style="border:1px solid <?php echo $color; ?>; color:#fff; background:<?php echo $color; ?>; padding-left:12px;">
                                        <p> <b><span style="font-size:large"><?= $dataRes->roomType; ?></span></b>
                                            <span style="color:#fff; float:right; font-size:x-large">
                                                <?php
                                                if ($dataRes->status == 'Y') {
                                                    ?>
                                                    <span id="btn_div<?php echo $cnt; ?>">
                                                        <button type="button" name="<?php echo $cnt; ?>" value="view-property-rooms.php?id=deactivate&recordID=<?php echo $dataRes->roomID; ?>" class="btn btn-danger" onclick="change_status(this);" style="width:95px;">Deactivate</button>
                                                    </span>
                                                    <!--<a href="<?//=SITE_ADMIN_URL?>/<?//=$script_name;?>?id=deactivate&recordID=<?//=$dataRes[roomID];?>" ><button type="button" class="btn btn-danger">Deactivate</button></a> &nbsp; &nbsp;-->
                                                    <?php
                                                }
                                                if ($dataRes->status == 'N' || $dataRes->status == 'D') {
                                                    ?>	
                                                    <span id="btn_div<?php echo $cnt; ?>">
                                                        <button type="button" name="<?php echo $cnt; ?>" value="view-property-rooms.php?id=activate&recordID=<?php echo $dataRes->roomID; ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;background:green;border:1px solid #36c6d3;">Activate</button>
                                                    </span>
                                                    <!--<a href="<?//=SITE_ADMIN_URL?>/<?//=$script_name;?>?id=activate&recordID=<?//=$dataRes[roomID];?>"><button type="button" class="btn" style="background:green;color:white;">Activate</button></a> &nbsp; &nbsp;-->
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($dataRes->status != 'D') {
                                                    ?>

                                                    <a href="edit-room.php?id=delete&recordID=<?= $dataRes->roomID ?>" onClick="return confirm('Are you sure you want to delete this record')" ><span style="color:#fff;" title="Delete" class=" fa fa-trash"></span></a>

                                                    <a href="edit-room.php?id=edit&recordID=<?= $dataRes->propertyID; ?>&roomID=<?= $dataRes->roomID; ?>"><span style="padding:0px 12px 0px 2px; color:#fff;" title="Edit" class="fa fa-edit"></span></a>
                                                    <?php
                                                }
                                                ?>
                                            </span><br>
                                    </div>
                                    <div class="row" style="padding:10px;">
                                        <div class="col-md-8">	
                                            <table style="width:100%;">
                                                <tr>
                                                    <td style="width:180px; padding: 10px;">
                                                        <?php
                                                        $id = $propertyRes->propertyID;
                                                        $rid = $dataRes->roomID;
                                                        //echo $id;
                                                        //    $imgsel = "select * from propertyImages where propertyID='$id' and roomID='$rid' and imageType='room' and type='home' order by imagesID desc limit 1";
                                                        //  $imquery = mysql_query($imgsel);
                                                        $roomPhotoDetail = $ajaxpropertyRoomCont->getRoomPhoto($propertyId, $rid, "and type='home' order by imagesID desc limit 1");
                                                        $roomPhotoData = $roomPhotoDetail[0];
                                                        foreach ($roomPhotoDetail as $roomPhoto) {
                                                            ?>
                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/w_700,h_700,c_fill/<?php if ($customerId != 1){ ?>reputize<?php } ?>/room/<?php echo $roomPhoto->imageURL; ?>.jpg" width="180" height="150" />
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php
                                                    $roomAmnty = $dataRes->roomAmenties;
                                                    $rommAmnty_br = explode("^", $roomAmnty);

                                                    $roomAmenities = '';
                                                    foreach ($rommAmnty_br as $roomAmntys) {
                                                        $roomAmenities .= $roomAmntys . " | ";
                                                    }
                                                    ?>
                                                    <td>
                                                        <span>
                                                            <b>Property Info: </b> <?php
                                                            $text = $dataRes->roomOverview;
                                                            echo html_entity_decode(substr($text,0,200)).'...';
                                                            ?></span>	</br><br>
                                                        <span><b>Property Amenties: </b><?php echo $roomAmenities; ?></span>	
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-md-4">
                                            <table class="table table_prop2" style="margin-left: 5px; float:right; border:1px solid <?php echo $color; ?>; text-align:center; width:97%;" >
                                                <thead style="background-color:<?php echo $color; ?>; color:#fff;">
                                                    <tr>
                                                        <th style="text-align:center">No. of Rooms</th>
                                                        <th style="text-align:center">Occupancy</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr>
                                                        <td> <?php echo $dataRes->room_no; ?> </td>
                                                        <td> <?php echo $dataRes->occupancy; ?> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <span style="float:left;padding-left:10px;">Status : 
                                        <?php
                                        if ($dataRes->status == 'Y') {
                                            ?>
                                            <b>Active</b> 
                                            <?php
                                        } elseif ($dataRes->status == 'D') {
                                            ?>
                                            <strong>Deleted</strong>
                                            <?php
                                        } else {
                                            ?>
                                            <strong>Inactive</strong>
                                            <?php
                                        }
                                        ?>		
                                    </span>
                                    <span style="float:right;padding-right:10px; ">
                                        <a  href="view-room-photo.php?recordID=<?= $dataRes->propertyID; ?>&roomID=<?= $dataRes->roomID; ?>">View Room Photo</a>|

                                        <a  href="add-room-rate.php?recordID=<?= $dataRes->propertyID; ?>&roomID=<?= $dataRes->roomID; ?>">Add Room Rate</a>
                                    </span><br>
                                </div>
                            </div>
                            <?php
                            $cnt++;
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </body>
    </html>
        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

                <script src="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

                <script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>


                <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

                <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

                <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

                <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>

                <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
                <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
                <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
                <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

                <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <?php
}
?>