<?php
include "../admin-function.php";
  $customerId = $_SESSION['customerID'];
 
$ajaxclientCont = new staticPageData();
?>
<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>
<?php
if (isset($_POST['pageUrlData'])) {
    $pageUrl = $_POST['pageUrlData']; 
  $clientPageData = $ajaxclientCont->getOurClientPageData($pageUrl, $customerId);
  //print_r($clientPageData);
  $pageData = $clientPageData[0];
  $ourclient = $pageData;
  $numb = count($clientPageData);
  
?>


<html lang="en">
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">

            <!-- BEGIN CONTAINER -->

            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">

                    <!-- BEGIN CONTENT BODY -->

                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Our Clients Page</span>

                                            <span class="caption-helper">Add/Edit Our Clients Page Here..</span>

                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h1" value="<?php echo $ourclient->h1; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Heading 1 Content
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="h1content" rows="6" required ><?php echo html_entity_decode($ourclient->h1content); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Collapsible Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="collapseh1" value="<?php echo $ourclient->collapsehead1; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Collapsible Content 1 
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="collapsecont1" rows="6" required ><?php echo html_entity_decode($ourclient->collapsecont1); ?></textarea>
                                                    </div>
                                                </div>

                                                <?php
                                                if ($numb == "0" || $ourclient->collapsehead2 != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsible Heading 2
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapseh2" value="<?php echo $ourclient->collapsehead2; ?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsible Content 2 
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapsecont2" rows="6"  ><?php echo html_entity_decode($ourclient->collapsecont2); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                if ($numb == "0" || $ourclient->collapsehead3 != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsible Heading 3
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapseh3" value="<?php echo $ourclient->collapsehead3; ?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsible Content 3
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapsecont3" rows="6"  ><?php echo html_entity_decode($ourclient->collapsecont3); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                if ($numb == "0" || $ourclient->collapsehead4 != '') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Collapsible Heading 4
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="collapseh4" value="<?php echo $ourclient->collapsehead4; ?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Collapsible Content 4
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="collapsecont4" rows="6"  ><?php echo html_entity_decode($ourclient->collapsecont4); ?></textarea>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h2" value="<?php echo $ourclient->h2; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Image Path 1
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="divimg1"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                        Uploaded Image :- <a href="<?SITE_DIR_PATH;?>/images/<?php echo $ourclient->divimg1; ?>" target="_blank"><?php echo $ourclient->divimg1; ?></a>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="divhead1" value="<?php echo $ourclient->divhead1; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Div Content 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="divcontent1" rows="6" required ><?php echo html_entity_decode($ourclient->divcontent1); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Image Path 2
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="divimg2"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                        Uploaded Image :- <a href="<?SITE_DIR_PATH;?>/images/<?php echo $ourclient->divimg2; ?>" target="_blank"><?php echo $ourclient->divimg2; ?></a>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="divhead2" value="<?php echo $ourclient->divhead2; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Div Content 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="divcontent2" rows="6" required ><?php echo html_entity_decode($ourclient->divcontent2); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Image Path 2
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="divimg3"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                        Uploaded Image :- <a href="<?SITE_DIR_PATH;?>/images/<?php echo $ourclient->divimg3; ?>" target="_blank"><?php echo $ourclient->divimg3; ?></a>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Div Heading 3
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="divhead3" value="<?php echo $ourclient->divhead3; ?>" required />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Div Content 3
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="divcontent3" rows="6" required ><?php echo html_entity_decode($ourclient->divcontent3); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Side Div Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="sidedivheading" value="<?php echo $ourclient->sidedivheading; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php
                                                            if ($numb == "0") {
                                                                ?>
                                                                <input type="hidden" name="savecont" value="Add Content">
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="hidden" name="savecont" value="Save Content">
                                                                <?php
                                                            }
                                                            ?>
                                                            <input type="hidden"  name="page_url" value="<?php echo $ourclient->page_url; ?>" />
                                                            <input type="submit" name="ourclientsub" class="btn green" id="submit" value="Save">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<!-- BEGIN PAGE LEVEL PLUGINS -->
      <div class="quick-nav-overlay"></div>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>      
        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>          
        <script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<?php
admin_footer();
exit;
?>
<?php
}
?>