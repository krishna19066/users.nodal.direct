<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
?>
<?php
if (isset($_POST['pageUrlData'])) {
    $pageUrl = $_POST['pageUrlData'];
    $clientPageData = $ajaxclientCont->getAboutUsPageData($customerId);
    $teammemdata = $ajaxclientCont->getTeamMember($customerId);
    $aboutus = $clientPageData[0];
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

            <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
            <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>           
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

            <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>

            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="favicon.ico" /> 
            <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />        			
            <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />        		

            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />        		
            <!-- END PAGE LEVEL PLUGINS -->				
            <!-- BEGIN PAGE LEVEL STYLES -->        			
            <link href="assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />  

        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>


            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit About Us Page</span>
                                            <span class="caption-helper">Add/Edit About Us Page Here..</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">											<input type="hidden" name="querystr" value="<?php echo $querystr; ?>" />
                                            <div class="form-body">
                                                <div class="expDiv" onclick="$('#general_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>
                                                <div id="general_content" style="display:none;">
                                                    <div class="form-group" >
                                                        <label class="control-label col-md-3"> Heading 1
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="h1" value="<?php echo $aboutus->about_h1; ?>" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Header Image

                                                        </label>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                                <?php if ($aboutus->heade_img) { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/aboutus/<?php echo $aboutus->header_img; ?>.jpeg" alt=""/>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="image_header"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Heading 1 Content
                                                            <!--<span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control" name="h1_cont" rows="6"><?php echo $aboutus->about_h1_cont; ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Heading 2
                                                           <!-- <span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="h2" value="<?php echo $aboutus->about_h2; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Heading 2 Content
                                                           <!-- <span class="required"> * </span>-->
                                                        </label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="h2_cont" rows="6"><?php echo $aboutus->about_h2_cont; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="expDiv" onclick="$('#box_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Boxed Contents </h3></div>
                                            <div id="box_content" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Collapsable Heading 1
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="collapse_h1" value="<?php echo $aboutus->about_collapseh1; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Collapsable Heading 1 Content
                                                       <!-- <span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="collapse_h1_cont" rows="6"><?php echo $aboutus->about_collapseh1_cont; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Collapsable Heading 2
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="collapse_h2" value="<?php echo $aboutus->about_collapseh2; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Collapsable Heading 2 Content
                                                       <!-- <span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="collapse_h2_cont" rows="6"><?php echo $aboutus->about_collapseh2_cont; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Collapsable Heading 3
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="collapse_h3" value="<?php echo $aboutus->about_collapseh3; ?>"/>
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Collapsable Heading 3 Content
                                                       <!-- <span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="collapse_h3_cont" rows="6"><?php echo $aboutus->about_collapseh3_cont; ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Collapsable Heading 4
                                                       <!-- <span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="collapse_h4" value="<?php echo $aboutus->about_collapseh4; ?>" />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Collapsable Heading 4 Content
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="collapse_h4_cont" rows="6"><?php echo $aboutus->about_collapseh4_cont; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="expDiv" onclick="$('#team-member').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Add Team Members</h3></div>
                                            <div id="team-member" style="display:none;">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 3
                                                        <!--<span class="required"> * </span>-->
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h3" value="<?php echo $aboutus->about_h3; ?>"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">	
                                                    <label class="control-label col-md-3"> Team Members</label>	
                                                    <div class="col-md-9">						
                                                        <div style="width:100%;border:1px solid grey;box-shadow:1px 1px 1px grey;padding:20px;">
                                                            <table style="width:100%;">				
                                                                <tr>						
                                                                    <th style="width:10%;padding-bottom:10px;">S. No. </th>	
                                                                    <th style="width:20%;padding-bottom:10px;"> Name </th>	
                                                                    <th style="width:20%;padding-bottom:10px;">Category</th>	
                                                                    <th style="width:30%;padding-bottom:10px;">Designation</th>	
                                                                    <th style="width:20%;padding-bottom:10px;"> Action </th>	
                                                                </tr>							
                                                                <?php
                                                                $teamcount = 1;
                                                                foreach ($teammemdata as $dt) {
                                                                    $slno = $dt->s_no;
                                                                    $name = $dt->name;
                                                                    ?>						
                                                                    <tr style="border-bottom:1px solid #b3adad;">	
                                                                        <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $teamcount; ?> </td>	
                                                                        <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $dt->name; ?> </td>	
                                                                        <td style="padding-top:8px;padding-bottom:8px;"> <?php echo $dt->depart; ?>
                                                                        </td><td style="padding-top:8px;padding-bottom:8px;"> <?php echo $dt->desig; ?> </td>
                                                                        <td style="padding-top:8px;padding-bottom:8px;">		
                                                                            <a href="add_static.php?type=delete&id=<?php echo $slno; ?>" class="btn btn-xs red" onClick="return confirm('Are you sure you want to delete this record')">Delete</a> &nbsp;	

                                                                            <a href="edit_team.php?type=edit&id=<?php echo $slno; ?>" class="btn btn-xs green">Edit</a> &nbsp;	
                                                                        </td>													
                                                                    </tr>													
                                                                    <?php
                                                                    $teamcount++;
                                                                }
                                                                ?>															
                                                            </table><br><br>	
                                                            <button type="button" class="btn btn-s blue" data-toggle="modal" href="#responsive"><i class="fa fa-plus"></i> Add More Team Members </button>
                                                        </div>												
                                                    </div>					
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden"  name="page_url" value="<?php echo $aboutus->page_url; ?>" />
                                                        <input type="submit" name="edit_about_page" class="btn green" id="submit" value="Save">
                                                    </div>
                                                </div>
                                            </div>
                                    </div>					
                                    </form>
                                </div>																
                                <div id="responsive" class="modal fade" tabindex="-1" data-width="760">								
                                    <div class="modal-header">									
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>	
                                        <h4 class="modal-title">Add Team Members</h4>					
                                    </div>									

                                    <div class="modal-body">							
                                        <form action="add_static.php" method="post" enctype="multipart/form-data">			
                                            <div class="row">						
                                                <div class="col-md-6">					
                                                    <h4>Name</h4>						
                                                    <p>							
                                                        <input class="form-control" type="text" name="teamName" placeholder="Enter Your Name"> 	
                                                    </p>												
                                                </div>										
                                                <div class="col-md-6">								
                                                    <h4>Department</h4>								
                                                    <p>										
                                                        <input class="form-control" type="text" name="teamDepart" placeholder="Enter Your Department"> 	
                                                    </p>													
                                                </div>												
                                                <div class="col-md-6">										
                                                    <h4>Designation</h4>										
                                                    <p>										
                                                        <input class="form-control" type="text" name="teamDesig" placeholder="Enter Your Designation"> 		
                                                    </p>													
                                                </div>										
                                                <div class="col-md-6">								
                                                    <h4>About Team Member</h4>						
                                                    <p>									
                                                        <textarea class="form-control" name="teamDescrip" rows="6" placeholder="Enter Your Description"> </textarea>	
                                                    </p>										
                                                </div>										
                                                <div class="col-md-6">							

                                                    <h4>Photo Team Member</h4>						
                                                    <p>								
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">		
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">		
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>	
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>	
                                                        <div>														
                                                            <span class="btn default btn-file">									
                                                                <span class="fileinput-new"> Select image </span>						
                                                                <span class="fileinput-exists"> Change </span>						
                                                                <input type="file" name="image_org"> </span>						
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>	
                                                        </div>													
                                                    </div>													
                                                    </p>												
                                                </div>											
                                            </div>	
                                            <div class="modal-footer">							
                                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>	
                                                <input type="submit" name="add_team" class="btn green" value="Save changes">				
                                            </div>	
                                        </form>									
                                    </div>										
                                    <div id="responsive_edit" class="modal fade" tabindex="-1" data-width="760">																				
                                        <div class="modal-header">																						
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>																						
                                            <h4 class="modal-title">Add Team Members</h4>																				
                                        </div>																														

                                        <form action="add_static.php" method="post" enctype="multipart/form-data">	
                                            <input type="hidden" name="url_page" value="<?php echo $querystr; ?>" />
                                            <div class="modal-body">																												
                                                <div class="row">	
                                                    <div id="to_be_repl" style="height:567px;min-width:400px;">

                                                    </div>
                                                </div>																																			
                                            </div>												

                                            <div class="modal-footer">																						
                                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>																						
                                                <input type="submit" name="update_team" class="btn green" value="Save changes">																			
                                            </div>	
                                        </form>	
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script>
            function team_mem(sln)
            {
                var mydata = sln;
                $.ajax({url: 'ajax_change_status.php',
                    data: {edit_team: mydata},
                    type: 'post',
                    success: function (output) {
                        $('#to_be_repl').html(output);
                    }
                });
            }
        </script>               
        <div class="quick-nav-overlay"></div>

        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>         
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>           
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>         
        <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>            
        <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

        <script src="http://www.hawthorntech.com/sitepanel/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>	
        <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->	

    </body>
    </html>
    <?php
}
?>