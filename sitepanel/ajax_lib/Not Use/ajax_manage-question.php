<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyPhotoCont = new propertyData();
$propertyId = $_POST['propQuestionId'];
$property_data = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
?>
<html lang="en">
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }




                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }

                tr:nth-child(odd){
                    background-color:#fff;
                }

                th {

                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                </style>

                <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> PROPERTY - <span style="color:#e44787;"><?php echo $propertyName; ?></span>

                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Manage Property Question</span>
                        </li>
                    </ul>
                    <?php
                    if (isset($_POST['propQuestionId'])) {
                        $propertyId = $_POST['propQuestionId'];

                        // $connectCloudinary = $ajaxpropertyPhotoCont->connectCloudinaryAccount($customerId);
                        ?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <span style="float:left; margin-top: -20px;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Questions" /></span>

                                            <span style="float:right; margin-top: -20px;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body form" id="add_photo" style="display:none;">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" action="../sitepanel/add_property.php" name="register_member_form" method="post" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Question
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="question"  value=""   />
                                            <input type="hidden" value="<?php echo $propertyId; ?>" name="propertyID">

                                        </div>
                                    </div>

                                    <div class="form-group last">
                                        <label class="control-label col-md-3">Answer</label>
                                        <div class="col-md-9">
                                            <textarea class="ckeditor form-control" name="answer" rows="6"> </textarea>
                                        </div>
                                    </div>                
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input name="recordID" type="hidden" value="12" />
                                                <input name="action_register" type="hidden" value="submit" />
                                                <input type="submit" class="btn green" name="submit_question" class="button" id="submit" value="Add Property Question" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>	

                        <div class="row">
                            <div class="col-md-12">	
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-body">
                                        <div style="padding-left:35px; color:rgba(0, 0, 0, 0.7);">
                                            <?php
                                            $propQuestListdata = $ajaxpropertyPhotoCont->getPropertyQuestions($propertyId);
                                            if ($propQuestListdata != NULL) {
                                                ?>
                                                <table class="ques_table" width="90%">
                                                    <tr style="padding:10px; border-bottom: 1px solid rgb(144, 139, 141);">
                                                        <th style="background-color:#fff; color:#e44787;">Questions List</th>
                                                        <th style="background-color:#fff; border-bottom: 1px solid #fff;"></th>
                                                    </tr>
                                                    <tbody>
                                                        <?php
                                                        foreach ($propQuestListdata as $propQuestList) {
                                                            ?>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr style="border-left: 10px solid <?php echo $color1; ?>;">
                                                                <td><?php echo $propQuestList->question; ?></td>
                                                                <td style="padding-left:50px; text-align:right;">
                                                                    <a href="manage-question.php?id=edit&qid=<?php echo $propQuestList->quesID; ?>&recordID=<?php echo $propQuestList->propertyID; ?>" class="btn btn-xs blue">
                                                                        <i class="fa fa-edit"></i> Edit 
                                                                    </a> &nbsp &nbsp &nbsp &nbsp 
                                                                    <a href="manage-question.php?id=delete&quesID1=<?php echo $propQuestList->quesID; ?>&recordID=<?php echo $propQuestList->propertyID; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-xs red">
                                                                        <i class="fa fa-trash"></i> Delete
                                                                    </a></span>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $cnt++;
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                            <?php } else { ?>
                                                <h3>No Record Found.....</h3>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>  
            <?php
        }
        ?>

        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2018 © Hawthorn Technologies Pvt. Ltd. All Rights Reserved. 

            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN QUICK NAV -->
        <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
            <ul>
                <li>
                    <a href="welcome.php"  class="active">
                        <span>Dashboard</span>
                        <i class="icon-basket"></i>
                    </a>
                </li>
                <li>
                    <a href="user_profile.php">
                        <span>My Profile</span>
                        <i class="icon-users"></i>
                    </a>
                </li>

                <li>
                    <a href="manage-enquiry.php">
                        <span> My Inquiry</span>
                        <i class="icon-graph"></i>
                    </a>
                </li>
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

        <script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>


        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

        <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>

        <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
    </body>
</html>