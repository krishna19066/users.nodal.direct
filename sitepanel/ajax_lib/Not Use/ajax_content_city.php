<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$cityId = $_POST['type_id'];
$cityListings1 = $propertyCont->getAllCityWithId($customerId, $cityId);
//print_r($cityListings1);
//die;
$res = $cityListings1[0];

//die;
?>
<head>
    <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
     
   
</head>
 
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_city();"><i class="fa fa-plus"></i> &nbsp Add City</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="margin-left: 0;">
        <div class="row">
            <div class="col-md-12">
                <br><br><br>
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-equalizer font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo bold uppercase">Edit <?php echo $typeCounter; ?> - <?= $res->cityName; ?></span>
                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="http://www.stayondiscount.com/sitepanel/manage-property-settings.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Heading H1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="h1" value="<?php echo $res->h1sa; ?>"  />
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?= $text; ?> City URL
                                        <span class="required"> * </span>
                                     </label>
                                    <div class="col-md-4">
                                        
                                        <input type="text" class="form-control input-inline input-medium" name="type_url" value="<?php echo $res->cityUrl; ?>" />
                                        <span class="help-inline"> .html </span>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3"> Slider Main Images
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="imageMain"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Heading H2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="h2" value="<?php echo $res->h2sa; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Heading H2 Content
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="h2content" value="<?php echo $res->h2sacontent; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Main Heading 1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="mainhead1" value="<?php echo $res->samainhead1; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Main Heading 2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="mainhead2" value="<?php echo $res->samainhead2; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Main Heading 3
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="mainhead3" value="<?php echo $res->samainhead3; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading 1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subhead1" value="<?php echo $res->sasubhead1; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading Content 1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subheadcontent1" value="<?php echo $res->sasubheadcontent1; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading 2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subhead2" value="<?php echo $res->sasubhead2; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading Content 2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subheadcontent2" value="<?php echo $res->sasubheadcontent2; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading 3
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subhead3" value="<?php echo $res->sasubhead3; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading Content 3
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subheadcontent3" value="<?php echo $res->sasubheadcontent3; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading 4
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subhead4" value="<?php echo $res->sasubhead4; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Sub Heading Content 4
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="subheadcontent4" value="<?php echo $res->sasubheadcontent4; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Main Heading 4
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="h3" value="<?php echo $res->h3sa; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?>  Award Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="awardhead" value="<?php echo $res->saawardhead; ?>"  />
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <label class="control-label col-md-3">Award Content</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="awardcontent" rows="6"> <?php echo $res->saawardcontent; ?></textarea>
                                    </div>
                                     
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"> Image1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image1"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group last">
                                    <label class="control-label col-md-3">Image 1 Content</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="saimg1content" rows="6"> <?php echo $res->saimage1Name; ?></textarea>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                        <label for="exampleInputFile" class="col-md-3 control-label">Award 1 Image</label>
                                        <div class="col-md-9">
                                                <input type="file" id="exampleInputFile" name="image1">
                                          
                                        </div>
                                </div>-->

                                <div class="form-group">
                                    <label class="control-label col-md-3"> Image2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image2"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group last">
                                    <label class="control-label col-md-3">Image 2 Content</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="saimg2content" rows="6"> <?php echo $res->saimage2Name; ?></textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3"> Image3
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="image3"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group last">
                                    <label class="control-label col-md-3">Image 3 Content</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="saimg3content" rows="6"> <?php echo $res->saimage3Name; ?></textarea>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                                       <label for="exampleInputFile" class="col-md-3 control-label">Award 2 Image</label>
                                       <div class="col-md-9">
                                               <input type="file" id="exampleInputFile" name="image2" >
                                         
                                       </div>
                               </div>-->
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?> Video Heading 
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="videohead" value="<?php echo $res->savideohead; ?>"  />
                                    </div>
                                </div> 
                                <div class="form-group last">
                                    <label class="control-label col-md-3">Video Content</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="videocontent" rows="6"> <?php echo $res->savideocontent; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?>  Video URL
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="videourl" value="<?php echo $res->savideourl; ?>"  />
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= $text; ?>  City Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="contentheading" value="<?php echo $res->sacontentheading; ?>"  />
                                    </div>
                                </div> 
                                <div class="form-group last">
                                    <label class="control-label col-md-3">City Content</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="content" rows="6"> <?php echo $res->sacontent; ?></textarea>
                                    </div>
                                </div>
                              <!--  <div class="form-group last">
                                    <label class="control-label col-md-3">Mobile Content 1</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="mobilecontent1" rows="6"> <?php echo $res->samobilecontent1; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <label class="control-label col-md-3">Mobile Content 2</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="mobilecontent2" rows="6"> <?php echo $res->samobilecontent2; ?></textarea>
                                    </div>
                                </div>-->

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <?php
                                            if ($res->cityID != '') {
                                                ?>
                                                <input type="submit" class="btn green" name="update_city_content"  class="button" value="Update <?= $record_type; ?>" />
                                                <input type="hidden" name="action" value="update_city_content">
                                                <input type="hidden" name="city_id" value="<?= $res->cityID; ?>">
                                                <input type="hidden" name="propertyCounter" value="<?= $res->propertyCounter; ?>">
                                                <?php
                                            }
                                            ?>
                                            <a href="manage-property-settings.php"><input type="button" name="back" class="btn red" value="Back" /></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
     
         
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>


