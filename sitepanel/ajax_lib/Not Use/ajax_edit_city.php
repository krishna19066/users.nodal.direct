<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$cityId = $_REQUEST['cityid'];
$cityListings1 = $propertyCont->getAllCityWithId($customerId, $cityId);
//print_r($cityListings1);

$res = $cityListings1[0];

if ($_REQUEST[action] == "update_record") {
    @extract($_REQUEST);
    echo $city_name = $res->cityName;
    die;
    // $type_url = getValidFileName($type_url);
    // $numCnt = getCount($table_name, " where cityName='$type_name' && cityID!='$type_id' ");
    $numCnt = count($cityListings1);
    if ($numCnt > 0) {
        $_SESSION[session_message] = $record_type . ' already exist';
        header("Location: manage-property-settings.php");
        exit;
    }

    // updateTable($table_name, " cityName='$type_name',property_type='$propertyCounter' where cityID='$type_id' and customerID='$custID'");
    $upd = $propertyCont->UpdateCityName($customerId, $cityId, $city_name, $propertyCounter);
    $_SESSION[session_message] = $record_type . ' has been updated successfully';
    header("Location: manage-property-settings.php");
    exit;
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_city();"><i class="fa fa-plus"></i> &nbsp Add City</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Edit City</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" name="formn" method="post" action="../sitepanel/manage-property-settings.php" enctype="multipart/form-data" onsubmit="return validate(this);">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"> City Name
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="city_name" value="<?= $res->cityName ?>" id="NOBLANK~Please enter category name~DM~">
                                <input type="hidden" name="cityName" value="<?php echo $res->cityName; ?>">
                                <span class="help-block"> Provide the City Name</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Property Type</label>
                            <div class="col-md-9">

                                <div class="mt-checkbox-inline">
                                    <label>	
                                        <?php
                                        $cityName = $res->cityName;
                                        $cityPropTypeList1 = $propertyCont->getCityPropertyType($customerId, $cityName);
                                       foreach($cityPropTypeList1 as $city_res){ 
                                           $propertyType = $city_res->property_type;
                                       
                                        $propTypList1 = $propertyCont->getAllPropTyp($customerId);
                                        //  print_r($propTypList1);
                                        $counter = 0;
                                        foreach ($propTypList1 as $dt) {
                                           $property_type = $dt->type_name;
                                           $res->property_type;
                                          //  $chk = "";
                                           if($property_type = $propertyType){
                                               $chk = 'checked';
                                           }
                                        }
                                        
                                            ?>                                    
                                        <input type="checkbox" name="tbl_property_type[]" value="<?php echo $property_type; ?>" <?php echo $chk; ?>  /> &nbsp; <?php echo $property_type; ?>&nbsp;
                                        <?php } ?>

                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <?php
                                    if (isset($res->cityID) == "") {
                                        ?>
                                        <input type="submit" name="add_record" class="btn green" id="submit" value="Add <?= $record_type; ?>">
                                        <input type="hidden" name="action" value="add_record">
                                        <input type="hidden" name="action" value="add_record">
                                        <?php
                                    } else {
                                        ?>
                                        <input type="submit" name="update_record" class="btn" id="submit" value="Update <?= $record_type; ?>">
                                        <input type="hidden" name="action" value="update_record">
                                        <input type="hidden" name="city_id" value="<?= $res->cityID; ?>">
                                        <?php
                                    }
                                    ?>
                                    <a href="manage-property-settings.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><br><br>