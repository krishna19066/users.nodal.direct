<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyPhotoCont = new propertyData();
$superAdmin_data = $ajaxpropertyPhotoCont->GetSupperAdminData($customerId);
$superAdmin_res = $superAdmin_data[0];
?>
<?php
if (isset($_POST['propRoomsId'])) {
    $propertyId = $_POST['propRoomsId'];

    // $connectCloudinary = $ajaxpropertyPhotoCont->connectCloudinaryAccount($customerId);
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="favicon.ico" /> 

            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }
                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }

                tr:nth-child(odd){
                    background-color:#fff;
                }

                th {

                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                </style>
            </head>

            <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
                <?php
                themeheader();
                ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> PROPERTY DETAILS
                                    <small>Add Property Rooms</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">My Properties</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Rooms</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <a class="pull-right">
                                              <!--  <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; margin: -20px;" onclick="load_add_property();"><i class="fa fa-plus"></i> &nbsp Add Property</button>-->
                                            </a>
                                            <a class="pull-left">
                                                <a href="manage-property.php" style="text-decoration: none; font-size: 20px"><i class="fa fa-backward" aria-hidden="true"></i> &nbsp Back</a>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> Add Property Rooms </span>
                                        </div>
                                       <!-- <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>-->
                                    </div>

                                    <?php
                                    //$roomRes = getResult("propertyTable", " where propertyID='$recordID'");
                                    $propRoomListdata = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
                                    $roomRes = $propRoomListdata[0];

                                    $roomTypearray = explode('^', $roomRes->roomType);
                                    //print_r($roomTypearray);
                                    $roomTypeCount77 = count($roomTypearray);
                                    (string)$roomTypeCount = $roomTypeCount77 - 1;
                                    $roomCount99 = $ajaxpropertyPhotoCont->getRoomDetail($propertyId);
                                    (string)$roomCount = count($roomCount99);
                                    foreach ($roomTypearray as $key => $value) {
                                        $roomTypeItem = $value;
                                        // $roomIDRes = getResult("roomType", " where apt_type_name='$roomTypeItem'");
                                        $roomData = $ajaxpropertyPhotoCont->getRoomTypesWithName($roomTypeItem);
                                        $roomIDRes = $roomData[0];
                                        // echo $roomIDRes->apt_type_name;
                                        // $propertyRoomRes = getResult("propertyRoom", " where propertyID='$recordID' && roomType='$roomTypeItem'");
                                        $propRoomData = $ajaxpropertyPhotoCont->getPropertyRoomType($propertyId, $roomTypeItem);

                                        $propertyRoomRes = $propRoomData[0];
                                        //  echo   $propertyRoomRes->roomType;
                                        //print_r($propertyRoomRes);
                                    }
                                    // echo  $text2 += count($propRoomData);
                                    
                                    
                                     if ($roomCount == $roomTypeCount) { 
                                         echo '<center><span class="caption-subject font-red bold uppercase"> Add Property Rooms Type </span></center>';
                                          echo  '<style>.formData{ display:none;}</style>';
                                          
                                     }else{
                                        // echo 'yes';                                                                            
                                     }
                                    ?>
                                    <div class = "portlet-body form formData">
                                        
                                        <form class = "form-horizontal" action = "../sitepanel/add_property.php" id = "submit_form" method = "post">
                                            <div class = "form-wizard">
                                                <div class = "form-body">
                                                    <div class = "tab-content">
                                                        <h3 class = "block">Room Details</h3>
                                                        <div class = "form-group">
                                                            <label class = "control-label col-md-3">Room Type
                                                                <span class = "required"> * </span>
                                                            </label>
                                                            <div class = "col-md-4">
                                                                <select class = "form-control" name = "roomType" required = "">
                                                                    <option value = "">Select</option>
                                                                    <?php
                                                                    //$roomRes = getResult("propertyTable", " where propertyID='$recordID'");
                                                                    $propRoomListdata = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
                                                                    $roomRes = $propRoomListdata[0];

                                                                    $roomTypearray = explode('^', $roomRes->roomType);

                                                                    foreach ($roomTypearray as $key => $value) {
                                                                        $roomTypeItem = $value;
                                                                        // $roomIDRes = getResult("roomType", " where apt_type_name='$roomTypeItem'");
                                                                        $roomData = $ajaxpropertyPhotoCont->getRoomTypesWithName($roomTypeItem);
                                                                        $roomIDRes = $roomData[0];
                                                                        // $propertyRoomRes = getResult("propertyRoom", " where propertyID='$recordID' && roomType='$roomTypeItem'");
                                                                        $propRoomData = $ajaxpropertyPhotoCont->getPropertyRoomType($propertyId, $roomTypeItem);
                                                                        $propertyRoomRes = $propRoomData[0];

                                                                        if (isset($roomIDRes->apt_type_id) != '' && isset($propertyRoomRes->roomID) == '') {
                                                                            ?>
                                                                            <option value="<?= $roomIDRes->apt_type_name; ?>"><?= $roomIDRes->apt_type_name; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                        <?php
                                                                    }
                                                                    ?>        
                                                                </select> 
                                                                <span class="help-block"> Provide your room type.</span>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" value="<?php echo $propertyId; ?>" name="propertyID">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Room Overview
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <textarea class="ckeditor form-control" name="roomOverview" required=""></textarea>
                                                                <span class="help-block"> Provide basic overview of your room </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Room Occupancy
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" max="100" name="occupancy" required="" />
                                                                <span class="help-block"> Provide your room occupancy </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">No. of Extra Beds
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" max="100" name="extra_bed" required="" />
                                                                <span class="help-block"> Provide extra bed no. </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">No. of Rooms / Type
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" max="100" name="room_num" required="" />
                                                                <span class="help-block"> Provide no. of rooms </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Room Amenities
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <div class="radio-list">
                                                                    <table style="width:100%;">
                                                                        <tr>
                                                                            <?php
                                                                            $amntyCnt = 1;

                                                                            $amenityList1 = $ajaxpropertyPhotoCont->getAmenitiesList();
                                                                            foreach ($amenityList1 as $amenityList) {
                                                                                ?>
                                                                                <td>
                                                                                    <label>
                                                                                        <input type="checkbox" name="businessType[]" value="<?php echo $amenityList->roomAmenties_name; ?>" /> <?php echo $amenityList->roomAmenties_name; ?> 
                                                                                    </label>
                                                                                </td>
                                                                                <?php
                                                                                if ($amntyCnt % 3 == 0) {
                                                                                    echo "</tr><tr>";
                                                                                }
                                                                                $amntyCnt++;
                                                                            }
                                                                            ?>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div id="form_gender_error"> </div>
                                                                <span class="help-block">You can select multiple amenities</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Daily Discount ?

                                                            </label>
                                                            <div class="col-md-4">
                                                                <input class="coupon_question" type="checkbox" id="coupon_question" name="coupon_question" value="1" onchange="valueChanged()"/>

                                                            </div>

                                                        </div>
                                                        <div class="form-group answer" style="display:none;">
                                                            <label class="control-label col-md-3">Daily Discount in (%) 
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" max="100" name="daily_dis"  />
                                                                <span class="help-block"> Provide your Daily Discount in (%)  </span>
                                                            </div>
                                                        </div>

                                                        <!--  <div class="form-group">
                                                              <label class="control-label col-md-3">Weekly Discount in (%) 
                                                                  <span class="required"> * </span>
                                                              </label>
                                                              <div class="col-md-4">
                                                                  <input type="text" class="form-control" max="100" name="weekly_dis" required="" />
                                                                  <span class="help-block"> Provide Weekly Discount in (%) . </span>
                                                              </div>
                                                          </div>

                                                          <div class="form-group">
                                                              <label class="control-label col-md-3">Monthly Discount in (%) 
                                                                  <span class="required"> * </span>
                                                              </label>
                                                              <div class="col-md-4">
                                                                  <input type="text" class="form-control" max="100" name="monthly_dis" required="" />
                                                                  <span class="help-block"> Provide Monthly Discount in (%)  </span>
                                                              </div>
                                                          </div>-->
                                                        <?php if ($superAdmin_res->template_type != 'multiple') { ?>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Room Url 
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control"  name="room_url" />
                                                                    <span class="help-block"> Provide Room URL  </span>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <a href="javascript:;" class="btn default button-previous">
                                                                <i class="fa fa-angle-left"></i> Back 
                                                            </a>
                                                            <input type="submit" class="btn green" name="add_prop_rooms" value="Submit" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script type="text/javascript">
                $(function () {
                    $("#coupon_question").on("click", function () {
                        $(".answer").toggle(this.checked);
                    });
                });
            </script>

            <?php
            admin_footer();
            exit;
            ?>    
            <script>
                function load_city_detail(urldata) {
                    var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                    $('#property_change').html(loadng);

                    $.ajax({url: 'ajax_lib/ajax_property.php',
                        data: {cityData: urldata},
                        type: 'post',
                        success: function (output) {
                            // alert(output);
                            $('#property_change').html(output);
                        }
                    });
                }
            </script>  

        </body>
    </html>
    <?php
}
?>