<?php
$subCityID = $_POST['subCityID'];
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$subCityData = $propertyCont->getSubCityWithID($subCityID);
$res = $subCityData[0];
$cityID = $res->cityID;
$cityListings1 = $propertyCont->getAllCityWithId($customerId, $cityID);
$res_city = $cityListings1[0];
$cityName = $res_city->cityName;
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_subcity();"><i class="fa fa-plus"></i> &nbsp Add City</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Edit Sub City</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" action="../sitepanel/add_property.php" id="submit_form" method="post">
                    <div class="form-wizard">
                        <div class="form-body">
                            <div class="tab-content"> 
                                <h3 class="block">Edit Sub City</h3>

                                <div class="form-group">
                                    <label class="control-label col-md-3"> Sub City Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="SubcityName" value="<?php echo $res->subCity; ?>" />

                                        <span class="help-block"> Edit your Sub City Name.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">City:</label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="cityID">
                                            <option value="<?php echo $cityID; ?>"><?php echo $cityName; ?></option>
                                            <?php
                                            $cityList1 = $propertyCont->getAllCity($customerId);
                                            foreach ($cityList1 as $cityList) {
                                                ?>
                                                <option value="<?php echo $cityList->cityID; ?>"><?php echo $cityList->cityName; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a onclick="load_sub_city_detail();" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back 
                                    </a>
                                    <input type="hidden" name="subCityID" value="<?php echo $subCityID; ?>">
                                    <input type="submit" class="btn green" name="edit_sub_city" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><br><br>
