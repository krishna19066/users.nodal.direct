<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$propVideoListdata = $ajaxpropertyPhotoCont->getVideoGallery($customerId);
?>
<?php
if (isset($_POST['pageUrlData'])) {
    $pageUrl = $_POST['pageUrlData'];
    $clientPageData = $ajaxclientCont->getAboutUsPageData($customerId);
    $teammemdata = $ajaxclientCont->getTeamMember($customerId);
    $aboutus = $clientPageData[0];
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

            <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
            <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>           
            <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

            <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>

            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <link rel="shortcut icon" href="favicon.ico" /> 
            <link href="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />        			
            <link href="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />        		

            <link href="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />        		
            <!-- END PAGE LEVEL PLUGINS -->				
            <!-- BEGIN PAGE LEVEL STYLES -->        			
            <link href="http://www.hawthorntech.com/sitepanel/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />  

        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>


            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Video Gallery Page</span>
                                            <span class="caption-helper">Add/Edit Video Gallery Page Here..</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-body ">
                                                    <div style="width:100%;" class="clear"> 
                                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="+ Add More Video" /></span>
                                                       <!-- <span style="float:left;"><input type="button" style="background:#363bd3;color:white;border:none;height:35px;width:180px;font-size:14px; margin-left: 18px;" data-html="true" onclick="load_ReorderpropertyPhoto(<?php echo $propertyId; ?>);" value="Reorder Property Photo" /></span>-->
                                                        <span style="float:right;"> <a href="manage-static-pages.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>

                                                        <br><br><br><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                                    <div class="portlet light bordered" id="add_photo" style="display:none;">			
                                        <div class="portlet-body form">
                                            <form class="form-horizontal" name="form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php">
                                                <div class="form-body">
                                                    <h4 align="center" style="color:#E26A6A;"><b> Add New Property Video </b></h3><br><br>                                                                                   
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Video Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input name="video_heading" type="text" class="form-control" />
                                                                <span class="help-block"> Provide your Heading</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Video Url
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input name="video_url" type="text" class="form-control" />
                                                                <span class="help-block"> Provide your Embed Url not iframe</span>
                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <input name="propertyID" type="hidden" value="<?php echo $propertyId ?>" />
                                                                    <input type="submit" name="submit_video_gallery" class="btn green" id="submit" value="Add Video">
                                                                    <button type="button" class="btn default">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->

                                    <div class="row">
                                        <div class="col-md-12">	

                                            <div class="portlet-body">
                                                <br><br><br>		

                                                <?php
                                                $cnt = 1;
                                                $sl = $start + 1;
                                                foreach ($propVideoListdata as $dataRes) {
                                                    //$dataRes = ms_htmlentities_decode($dataRes);
                                                    $video_heading = $dataRes->video_heading;
                                                    $video_url = $dataRes->video_url;
                                                    if ($video_url != null && $video_heading != NULL) {
                                                        if ($sl % 2 != 0) {
                                                            $color = "#ffffff";
                                                        } else {
                                                            $color = "#f6f6f6";
                                                        }
                                                        if ($cnt == "1") {
                                                            $color = "#32c5d2";
                                                        }
                                                        if ($cnt == "2") {
                                                            $color = "#3598dc";
                                                        }
                                                        if ($cnt == "3") {
                                                            $color = "#36D7B7";
                                                        }
                                                        if ($cnt == "4") {
                                                            $color = "#5e738b";
                                                        }
                                                        if ($cnt == "5") {
                                                            $color = "#1BA39C";
                                                        }
                                                        if ($cnt == "6") {
                                                            $color = "#32c5d2";
                                                        }
                                                        if ($cnt == "7") {
                                                            $color = "#578ebe";
                                                        }
                                                        if ($cnt == "8") {
                                                            $color = "#8775a7";
                                                        }
                                                        if ($cnt == "9") {
                                                            $color = "#E26A6A";
                                                        }
                                                        if ($cnt == "10") {
                                                            $color = "#29b4b6";
                                                        }
                                                        if ($cnt == "11") {
                                                            $color = "#4B77BE";
                                                        }
                                                        if ($cnt == "12") {
                                                            $color = "#c49f47";
                                                        }
                                                        if ($cnt == "13") {
                                                            $color = "#67809F";
                                                        }
                                                        if ($cnt == "14") {
                                                            $color = "#8775a7";
                                                        }
                                                        if ($cnt == "15") {
                                                            $color = "#73CEBB";
                                                        }
                                                        ?>
                                                        <div class="col-md-3 " style="width:385px;">
                                                            <!-- BEGIN Portlet PORTLET-->
                                                            <div class="portlet box" style="background:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>;">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-image"></i><a href="<?php echo $video_url; ?>" target="_blank" style="color:#fff;">View Video</a> 
                                                                    </div>

                                                                    <div class="actions">
                                                                        <button type="button" class="btn btn-default btn-sm" data-html="true"  data-html="true"  onclick="$('#edit_video<?= $dataRes->id; ?>').slideToggle();" value=""><i class="fa fa-plus"></i> Edit</button>
                                                                        <!--<a href="manage-slider.php?id=edit&recordID=<?= $dataRes->id; ?>&type=<?= $dataRes->type; ?>" class="btn btn-default btn-sm">
                                                                            <i class="fa fa-plus"></i> Edit 
                                                                        </a>-->											
                                                                        <a href="add_property.php?VideoID=<?= $dataRes->id; ?>&action2=VideoGalleryDelete" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-default btn-sm">
                                                                            <i class="fa fa-pencil"></i> Delete 
                                                                        </a>
                                                                    </div>
                                                                    <!---------------------------------------------------Edit Video-------------------------------------------->

                                                                    <div class="portlet light bordered" id="edit_video<?= $dataRes->id; ?>" style="display:none;">			
                                                                        <div class="portlet-body form">
                                                                            <form class="form-horizontal" name="form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php">
                                                                                <div class="form-body">                                                                                                                                                                    
                                                                                    <div class="form-group">                                                                                          
                                                                                        <div class="col-md-12">
                                                                                            <input name="video_heading" type="text" class="form-control" value="<?= $dataRes->video_heading; ?>" />
                                                                                            <span class="help-block"> Provide your Heading</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">                                                                                         
                                                                                        <div class="col-md-12">
                                                                                            <input name="video_url" type="text" class="form-control" value="<?= $dataRes->video_url; ?>" />
                                                                                            <span class="help-block"> Provide your Embed Url not iframe</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-actions">
                                                                                        <div class="row">
                                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                                <input name="videoID" type="hidden" value="<?= $dataRes->id; ?>" />
                                                                                                <input type="submit" name="Update_video_gallery" class="btn green" id="submit" value="Update">
                                                                                                <button type="button" class="btn default">Cancel</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    <!-----------------------------------------Edit Video End------------------------------------------------>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="" style="height:auto" data-handle-color="red">
                                                                        <iframe width="100%" height="20%" src="<?php echo $video_url; ?>" frameborder="0" allowfullscreen></iframe>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                                            <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                                        </table>
                                                        <?php
                                                    }
                                                    ?>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="myModal<?= $dataRes->id; ?>" role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Modal Header</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Some text in the modal.</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <?php
                                                    $cnt++;
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    function team_mem(sln)
                    {
                        var mydata = sln;
                        $.ajax({url: 'ajax_change_status.php',
                            data: {edit_team: mydata},
                            type: 'post',
                            success: function (output) {
                                $('#to_be_repl').html(output);
                            }
                        });
                    }
                </script>
                <div class="quick-nav-overlay"></div>

                <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>         
                <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
                <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>           
                <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>         
                <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>            
                <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
                <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
                <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
                <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

                <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
                <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

                <script src="http://www.hawthorntech.com/sitepanel/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
                <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>	
                <script src="http://www.hawthorntech.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL PLUGINS -->
                <!-- BEGIN PAGE LEVEL SCRIPTS -->	

        </body>
    </html>
    <?php
}
?>