<?php
include "../admin-function.php";
//$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$customerId = $_SESSION['customerID'];
$analyticsCont = new analyticsPage();
?>
<?php
//$pageUrl = $_POST['pageUrlData'];
$metatagPageData = $ajaxclientCont->getMetaTagsData($customerId);
//print_r($metatagPageData);
$Metapage = $metatagPageData[0];
$numb = count($Metapage);
$cityUrlData = $ajaxclientCont->getCityUrlData($customerId);
$city_url = $cityUrlData[0];
$staticPageUrlData = $ajaxclientCont->getStaticPageUrlData($customerId);
$propertyPageUrlData = $ajaxclientCont->getPropertyPageUrlData($customerId);
$roomPageUrlData = $ajaxclientCont->getRoomPageUrlData($customerId);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
    <?php
    themeheader();
    ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">


        <!-- BEGIN CONTAINER -->

        <div class="page-content">
            <div class="row">
                <div class="col-md-12">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer font-red-sunglo"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase">Add New Meta Tags</span>
                                <span class="caption-helper">Please Add new Meta Tags</span>
                                <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->

                            <form class="form-horizontal" name="formn" method="post" action="../sitepanel/manage-users.php" enctype="multipart/form-data" onsubmit="return validate(this);">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Name
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="type_name" value="<?= $res[name]; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                            <span class="help-block"> Provide Name of User</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Username (use as login id)
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="type_username" value="<?= $res[user_id]; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                            <span class="help-block"> Provide Login ID</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Password
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" class="form-control" name="type_password" value="<?= $res[pwd]; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                            <span class="help-block"> Provide User Password</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Confirm Password
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="password" class="form-control" name="type_repassword" value="<?= $res[pwd]; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                            <span class="help-block"> Provide User Re Password</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Email
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="type_email" value="<?= $res[email]; ?>" id="NOBLANK~Please enter category name~DM~" required>
                                            <span class="help-block"> Provide User Email</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"> User Type
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <div class="radio-list">
                                                <?php
                                                $typ = $res['user_type'];
                                                ?>

                                                <table>
                                                    <tr>
                                                        <td>
                                                    <center><input type="radio" id="radio-1-0" name="user_type" value="admin" required <?php
                                                        if ($res['user_type'] == true) {
                                                            if ($typ == "admin") {
                                                                echo "checked";
                                                            }
                                                        }
                                                        ?>/>
                                                        <center><span class="help-block"> Administrator</span>
                                                            </td>

                                                            <td style="padding-left:100px;">
                                                            <center><input type="radio" id="radio-1-1" name="user_type" value="staff" <?php
                                                                if ($res['user_type'] == true) {
                                                                    if ($typ == "staff") {
                                                                        echo "checked";
                                                                    }
                                                                }
                                                                ?>/>
                                                                <center><span class="help-block"> Staff</span>
                                                                    </td>
                                                                    </tr>
                                                                    </table>
                                                                    </div>
                                                                    <span class="required"> Please Select any one of the above to continue </span>
                                                                    </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Sections
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-4">
                                                                            <div id="dvadmin" style="<?php
                                                                            if ($res['user_type'] == true) {
                                                                                if ($res['user_type'] == "admin") {
                                                                                    ?>display:block; <?php } else { ?> display: none; <?php
                                                                                     }
                                                                                 } else {
                                                                                     ?>display: none <?php } ?>">
                                                                                You are giving full access.
                                                                                <div class="form-actions">
                                                                                    <div class="row">
                                                                                        <div class="col-md-offset-3 col-md-9">
                                                                                            <?php
                                                                                            if($id='edit' && $recordID!='')
                                                                                            { 
                                                                                            ?> 
                                                                                            <input type="submit" name="submit" class="btn green" id="submit" value="Edit User">
                                                                                            <input name="recordID" type="hidden" value="<?= $res[slno]; ?>" />
                                                                                            <input name="action" type="hidden" value="update_record" />
                                                                                            <?php
                                                                                            }
                                                                                           else	
                                                                                            {
                                                                                            ?>
                                                                                            <input type="submit" name="add_record" class="btn green" id="submit" value="Add User">
                                                                                            <input name="action" type="hidden" value="add_record" />
                                                                                            <?php
                                                                                            }
                                                                                            ?>
                                                                                            <a href="manage-users.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div id="dvstaff" style="<?php
                                                                            if ($res['user_type'] == true) {
                                                                                if ($res['user_type'] == "staff") {
                                                                                    ?>display:block; <?php } else { ?> display: none; <?php
                                                                                     }
                                                                                 } else {
                                                                                     ?>display: none <?php } ?>">
                                                                                 <?php
                                                                                 $section = $res['section'];
                                                                                 $map_sec = explode("^", $section);
                                                                                 ?>			

                                                                                <?php
                                                                                $counter = 0;
                                                                                foreach ($admin_section as $key => $value) {
                                                                                    if ($counter == 0) {
                                                                                        ?>
                                                                <!--<tr>-->
                                                                                        <?php
                                                                                    }
                                                                                    $chk = "";
                                                                                    if ($_SESSION['admin_section'] == $key) {
                                                                                        $chk = 'checked="checked"';
                                                                                    }
                                                                                    ?>
                                                                                    <div class="mt-checkbox-inline">
                                                                                        <label class="mt-checkbox">
                                                                                            <input type="checkbox" name="admin_section[]" id="inlineCheckbox21" value="<?= $key; ?>" <?= $chk; ?> <?php
                                                                                            foreach ($map_sec as $check_key) {
                                                                                                $che_key = $admin_section[$check_key];
                                                                                                if ($che_key == $value) {
                                                                                                    echo "checked";
                                                                                                }
                                                                                            }
                                                                                            ?>> <?= $value; ?>
                                                                                            <span></span>
                                                                                        </label>
                                                                                    </div>
                                                                                    <!--<tr>
                                                                                            <td width="50%"><input type="checkbox" name="admin_section[]"  value="<?//=$key;?>" <?//=$chk;?> /><?//=$value;?> </td>-->
                                                                                    <?php
                                                                                    $counter++;
                                                                                    if ($counter == 2) {
                                                                                        ?>
                                                                                        <!--</tr>-->
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                if ($counter == 1) {
                                                                                    ?>
                                                                <!--<td>&nbsp;</td>
                                                        </tr>-->
                                                                                    <?php
                                                                                }
                                                                                ?>

                                                                                <div class="form-actions">
                                                                                    <div class="row">
                                                                                        <div class="col-md-offset-3 col-md-9">
                                                                                            <? 
                                                                                            if($id='edit' && $recordID!='')
                                                                                            { 
                                                                                            ?> 
                                                                                            <input type="submit" name="submit" class="btn green" id="submit" value="Edit User">
                                                                                            <input name="recordID" type="hidden" value="<?= $res[slno]; ?>" />
                                                                                            <input name="action" type="hidden" value="update_record" />
                                                                                            <?
                                                                                            }
                                                                                            else	
                                                                                            {
                                                                                            ?>
                                                                                            <input type="submit" name="add_record" class="btn green" id="submit" value="Add User">
                                                                                            <input name="action" type="hidden" value="add_record" />
                                                                                            <?
                                                                                            }
                                                                                            ?>
                                                                                            <a href="manage-users.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    </div>

                                                                    </div>
                                                                    </form>

                                                                    <!-- END FORM-->
                                                                    </div>
                                                                    </div>                   
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    </div>
                                                                    <!-- BEGIN FOOTER -->
                                                                    <div class="page-footer">
                                                                        <div class="page-footer-inner"> 

                                                                        </div>
                                                                        <div class="scroll-to-top">
                                                                            <i class="icon-arrow-up"></i>
                                                                        </div>
                                                                    </div>
                                                                    <!-- END FOOTER -->
                                                                    <!-- BEGIN QUICK NAV -->
                                                                    <nav class="quick-nav">
                                                                        <a class="quick-nav-trigger" href="#0">
                                                                            <span aria-hidden="true"></span>
                                                                        </a>
                                                                        <ul>
                                                                            <li>
                                                                                <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                                                                                    <span>Purchase Metronic</span>
                                                                                    <i class="icon-basket"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                                                                                    <span>Customer Reviews</span>
                                                                                    <i class="icon-users"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="http://keenthemes.com/showcast/" target="_blank">
                                                                                    <span>Showcase</span>
                                                                                    <i class="icon-user"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                                                                                    <span>Changelog</span>
                                                                                    <i class="icon-graph"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                        <span aria-hidden="true" class="quick-nav-bg"></span>
                                                                    </nav>
                                                                    <div class="quick-nav-overlay"></div>

                                                                    <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                                                                    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                                                                    <!-- END CORE PLUGINS -->
                                                                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                                                                    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                                                                    <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
                                                                    <!-- END PAGE LEVEL PLUGINS -->
                                                                    <!-- BEGIN THEME GLOBAL SCRIPTS -->
                                                                    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
                                                                    <!-- END THEME GLOBAL SCRIPTS -->
                                                                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                                                    <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>
                                                                    <!-- END PAGE LEVEL SCRIPTS -->
                                                                    <!-- BEGIN THEME LAYOUT SCRIPTS -->
                                                                    <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
                                                                    <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
                                                                    <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
                                                                    <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
                                                                    <!-- END THEME LAYOUT SCRIPTS -->

                                                                    <!-- BEGIN CORE PLUGINS -->
                                                                    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                                                                    </body>
                                                                    </html>
