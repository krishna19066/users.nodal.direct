<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxinventoryUpdateCont = new inventoryData();
?>

<?php
if (isset($_POST['month_prop_name'])) {
    echo 'manth_prop_name';

    $data_date = $_POST['month_prop_name'];
    $dat = explode("-", $data_date);
    $mont = $dat[0];
    $yer = $dat[1];
    $property = $dat[2];
    $n_dat = $yer . "-" . $mont . "-01";
}

if (isset($_POST['date_time'])) {

    $data_date = $_POST['date_time'];

    $dat = explode("~", $data_date);
    $new_date = $dat[0];
    $mn_1 = explode("-", $new_date);
    $mn_2 = $mn_1[1];
    $mn_3 = $mn_1[0];
    $type = $dat[1];
    $property = $dat[2];
    if ($type == "prev") {
        if ($mn_2 == "03") {
            $n_dat = $mn_3 . "-02-01";
            $org_date = explode("-", $n_dat);
            $mont = $org_date[1];
            $yer = $org_date[0];
        } else {
            $n_dat1 = strtotime("-1 month", strtotime($new_date));
            $n_dat = date("Y-m-d", $n_dat1);
            $org_date = explode("-", $n_dat);
            $mont = $org_date[1];
            $yer = $org_date[0];
        }
    } else {
        if ($mn_2 == "01") {
            $n_dat = $mn_3 . "-02-01";
            $org_date = explode("-", $n_dat);
            $mont = $org_date[1];
            $yer = $org_date[0];
        } else {
            $n_dat1 = strtotime("+1 month", strtotime($new_date));
            $n_dat = date("Y-m-d", $n_dat1);
            $org_date = explode("-", $n_dat);
            $mont = $org_date[1];
            $yer = $org_date[0];
        }
    }
}
?>

<?php
if (isset($_POST['month_prop_name']) || isset($_POST['date_time'])) {

    $currentMonth = $mont;
    $monthName = $ajaxinventoryUpdateCont->getMonthFullName($currentMonth);
    ?>
    <div>
        <?php
        $curr_mon = date('m');
        $curr_year = date('Y');
        if ($mont == $curr_mon && $yer == $curr_year) {
            
        } else {
            ?>
            <input  name="prev" id="prev_month_year" value="<?php echo $n_dat; ?>" hidden /> <span style="float:left; font-size:30px;"><a href="javascript:;" onclick="next_month('prev')" class="fa fa-arrow-circle-left"> </a></span>
            <?php
        }
        ?>

        <input  name="next" id="next_month_year" value="<?php echo $n_dat; ?>" hidden /><span style="float:right; font-size:30px; padding-right:20px;"><a href="javascript:;" onclick="next_month('next')"  class="fa fa-arrow-circle-right"> </a></span>
    </div><br><br>
    <div class="rates">													
        <div class="calendar">
            <?php //echo "<h1>".$n_dat."</h1>";  ?>
            <table class="calendar_table">
                <tr>
                    <td style="width:110px;background:#cbf1f3;border:0px;padding-left:5px;margin-left:0px;"><span class="caption-subject bold uppercase" style="color:black;"><center><?php echo $monthName . "<br>" . $yer; ?></span>  </td>
                    <?php
                    $mon = $mont;
                    $mm1 = date('m');
                    $yy1 = date('Y');
                    $dd1 = date('d');
                    if ($mon == "01" || $mon == "03" || $mon == "05" || $mon == "07" || $mon == "08" || $mon == "10" || $mon == "12") {
                        for ($i = 1; $i <= 31; $i++) {
                            ?>
                            <td style="<?php if ($i == $dd1 && $yer == $yy1 && $mon == $mm1) { ?>background:#36c6d3;<?php } ?>width:3%;height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                                <?php echo $i; ?>
                                <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                            </td>			
                            <?php
                            //$date = date('Y-m-d', strtotime("+$i days"));
                            //echo $date."<br>";
                        }
                    } elseif ($mon == "04" || $mon == "06" || $mon == "09" || $mon == "11") {
                        for ($i = 1; $i <= 30; $i++) {
                            ?>
                            <td style="<?php if ($i == $dd1 && $yer == $yy1 && $mon == $mm1) { ?>background:#36c6d3;<?php } ?>width:3%;height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                                <?php echo $i; ?>
                                <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                            </td>
                            <?php
                        }
                    } elseif ($mon == "02") {
                        for ($i = 1; $i <= 28; $i++) {
                            ?>
                            <td style="<?php if ($i == $dd1 && $yer == $yy1 && $mon == $mm1) { ?>background:#36c6d3;<?php } ?>width:3%;height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                                <?php echo $i; ?>
                                <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                            </td>
                            <?php
                        }
                    }
                    ?>
                </tr>
            </table>
            <br>

            <?php
            $mon = $mont;
            $year = $yer;
            $dd = date('d');
            $yy = date('Y');
            $mm = date('m');

            $propertyName = $property;

            $propertyDetailsName = $ajaxinventoryUpdateCont->getPropDetailsByName($propertyName, $customerId);
            //  print_r($propertyDetailsName);
            //   die;

            $toBePassedYear = $year;
            $toBePassedMon = $mon;
            // $toBePassedDate = 


            foreach ($propertyDetailsName as $propertyByNameData) {
                $propertyID = $propertyByNameData->propertyID;

                $roomDetails = $ajaxinventoryUpdateCont->getRoomDetails($propertyID);

                echo '<table class="calendar_data">';

                foreach ($roomDetails as $roomData) {
                    $roomId = $roomData->roomID;
                    $roomType = $roomData->roomType;
                    $roomNo = $roomData->room_no;
                    ?>
                    <tr>
                        <td style="width:110px;padding-left:5px;background:#ffdede;">
                            <span class="caption-subject bold uppercase" style="color:black;"><center> <?php echo $roomType; ?> </center></span>
                        </td>		
                        <?php
                        $roomInventoryData = $ajaxinventoryUpdateCont->getInventoryDetails($propertyID, $roomId, $toBePassedMon, $toBePassedYear);
                        $inventoryDataCount = count($roomInventoryData);

                        $displayInventory = $ajaxinventoryUpdateCont->displayInventory($inventoryDataCount, $propertyID, $roomType, $roomNo, $toBePassedMon, $toBePassedDate, $roomInventoryData);
                        ?>
                    </tr>
                    <tr><td style="height:10px;"></td></tr>
                        <?php
                    }
                    echo '</table>';
                }
                ?>		
        </div>
    </div>
    <?php
}
?>
