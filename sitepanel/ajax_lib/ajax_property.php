<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyCont = new propertyData();
$inquiryCont = new manageEnquiry();
$mode = $_SESSION['mode'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
?>
<style>
    .not-active {
        pointer-events: none;
        cursor: default;
        opacity: 0.6;
    }
</style>


<?php
if (isset($_POST['cityData'])) {
    $cityName = $_POST['cityData'];
    $customerId = $_SESSION['customerID'];
    ?>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12" style="padding-right: 2px;">										
            <?php
            $count = 1;
            $propListing1 = $ajaxpropertyCont->getCityProperty($customerId, $cityName,$user_id);
            foreach ($propListing1 as $propListing) {
                $stat = $propListing->status;
                $affiliate = $propListing->affilate;
                $feature = $propListing->feature;
                if ($stat == "Y") {
                    $act_stat = "Active";
                } else {
                    $act_stat = "<span style='color:Red;'>Inactive</span>";
                }
                if ($feature == "Y") {
                    $fea_stat = "<span style='color:Yellow;'>Featured</span>";
                } else {
                    $fea_stat = "";
                }
                ?>
                <!-- END SAMPLE TABLE PORTLET-->
                <!-- BEGIN SAMPLE TABLE PORTLET-->

                <div class="portlet box mt-list-item" style="background:#2AB4C0;border-bottom:2px solid #2AB4C0;">
                    <div class="portlet-title" onclick="expand_me(<?php echo $count; ?>)">
                        <div class="caption">
                            <center>
                                <?php
                                if ($act_stat == "Inactive") {
                                    ?>
                                    <i><?php echo $propListing->propertyName; ?> &nbsp (<?php echo $act_stat; ?>) &nbsp <?php echo $fea_stat; ?></i>
                                    <?php
                                } else {
                                    ?>
                                    <?php echo $propListing->propertyName; ?> &nbsp (<?php echo $act_stat; ?>) &nbsp <?php echo $fea_stat; ?>
                                    <?php
                                }
                                ?>
                            </center>
                        </div>
                        <div class="tools">
                            <span id="toggle_expand<?php echo $count; ?>">Expand</span>  
                        </div>
                    </div>
                    <div id="data_box<?php echo $count; ?>" style="display: none; padding: 20px; background: white; border: 1px solid #2AB4C0;color:#678098;">
                        <div class="row">
                            <div class="col-lg-6 col-xs-6 col-sm-6" style="padding:0px; border-right:1px solid #74B0DA;">
                                <div>
                                    <table class="table_prop" style="width:98% ! important;">
                                        <tbody>
                                            <tr>
                                                <td style="width: 125px ! important;table-layout: fixed ! important;"> Total Room </td>
                                                <td>: <?php
                                                    $propid = $propListing->propertyID;
                                                    $roomDet1 = $ajaxpropertyCont->getRoomDetail($propid);
                                                    echo count($roomDet1);
                                                    ?> </td>
                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;"> City Name </td>
                                                <td>: <?php echo $propListing->cityName; ?> </td>
                                            </tr>
                                            <tr>
                                                <td> Check in</td>
                                                <td>: <?php echo $propListing->checkIN; ?>:00</td>
                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;"> Check Out </td>
                                                <td>: <?php echo $propListing->checkOut; ?>:00 </td>
                                            </tr>
                                            <tr>
                                                <td> Property Photos</td>
                                                <?php
                                                $propid = $propListing->propertyID;
                                                $PropCountCont = $ajaxpropertyCont->getPropertyPhotoCount($propid, $customerId);
                                                $propCount = count($PropCountCont);
                                                ?>
                                                <td>: <?php echo $propCount; ?></td>
                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Property Type </td>
                                                <?php
                                                $proptypeid = $propListing->tbl_property_type;

                                                $propTypeNam = $ajaxpropertyCont->getPropertyTypeName($proptypeid, $customerId);
                                                foreach ($propTypeNam as $dt) {
                                                    
                                                }
                                                // print_r($propTypeNam);
                                                //$propTypeNam = $propTypeNam1[0];
                                                ?>
                                                <td>: <?php echo $dt->type_name; ?> </td>
                                            </tr>
                                            <tr>
                                                <td>Manager Name</td>
                                                <td>: <?php echo $propListing->managerName; ?></td>
                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Mobile No </td>
                                                <td>: <?php echo $propListing->managerContactNO; ?> </td>
                                            </tr>
                                            <tr>
                                                <td>Booking Engine</td>
                                                <td>: <?php echo $propListing->booking_engine; ?></td>
                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Property Add </td>
                                                <td>: <?php
                                                    $timestamp = $propListing->timestamp;
                                                    echo date("jS F, Y", strtotime($timestamp));
                                                    ?> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr style="border-bottom: 1px solid #ccc;">
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!--Active Form-->
                                                </td>
                                                <td colspan="3">
                                                    <!--: Inquiry Now | Book Now | Review us | FAQ-->
                                                </td>
                                                <!-- To be Mapped -->
                                            </tr>
                                            <tr>
                                                <td>Active Features</td>
                                                <td colspan="3">: 
                                                    <?php
                                                    $gmap = $propListing->gmapurl;
                                                    $view = '';
                                                    $review = '';
                                                    $trust = $propListing->trustURL;
                                                    if ($view !== '') {
                                                        echo "360* View|";
                                                    }
                                                    if ($gmap !== '') {
                                                        echo "Google Map|";
                                                    }
                                                    if ($review !== '') {
                                                        echo "Reviews|";
                                                    }
                                                    if ($trust !== '') {
                                                        echo "TrustYouAPI";
                                                    }
                                                    ?>
                                                    Google Map|																								
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <table style="width:100%;border-bottom: 1px solid #ccc;">
                                        <tbody>
                                            <tr>
                                                <td style="width:30%;">
                                                    <font style="font-size:18px;">&nbsp; Staus: 
                                                    <?php
                                                    if ($act_stat == "Inactive") {
                                                        ?>
                                                        <b id="stat_id<?php echo $count; ?>"><?php echo "<font style='color:#F36A5A;'>" . $act_stat . "</font>"; ?></b>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <b id="stat_id<?php echo $count; ?>"><?php echo "<font style='color:#1BBC9B;'>" . $act_stat . "</font>"; ?></b>
                                                        <?php
                                                    }
                                                    ?>
                                                    </font><br><br>
                                                </td>
                                                <?php
                                                $propid = $propListing->propertyID;
                                                ?>
                                                <td style="width:60%;">
                                                    <div class="btn-toolbar margin-bottom-10">
                                                        <div class="btn-group">
                                                            <?php
                                                            if ($stat == "Y") {
                                                                ?>
                                                                <div id="btn_div<?php echo $count; ?>">
                                                                    <button type="button" name="<?php echo $count; ?>" value="manage-property.php?id=deactivate&recordID=<?php echo $propid; ?>" class="btn btn-primary" onclick="change_status(this);" style="width:95px; <?php if($mode == 'V'){ ?> cursor: not-allowed; <?php } ?> " <?php if($mode == 'V'){ ?> disabled title="Only view mode" <?php } ?> >Deactivate</button>
                                                                </div>
                                                                <?php
                                                            } else if ($stat == 'N') {
                                                                ?>
                                                                <div id="btn_div<?php echo $count; ?>">
                                                                    <button type="button" name="<?php echo $count; ?>" value="manage-property.php?id=activate&recordID=<?php
                                                                    echo $propid;
                                                                    ;
                                                                    ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;background:#36c6d3;border:1px solid #36c6d3; <?php if($mode == 'V'){ ?> cursor: not-allowed; <?php } ?> " <?php if($mode == 'V'){ ?> disabled title="Only view mode" <?php } ?> >Activate</button>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="btn-group">
                                                            <?php
                                                            if ($affiliate == "Y") {
                                                                ?>
                                                                <div id="btn_div<?php echo $count; ?>">
                                                                    <button type="button" name="aff<?php echo $count; ?>" value="manage-property.php?id=notaffiliate&recordID=<?php echo $propid; ?>" class="btn btn-warning" onclick="change_status(this);" style="width:95px; <?php if($mode == 'V'){ ?> cursor: not-allowed; <?php } ?> " <?php if($mode == 'V'){ ?> disabled title="Only view mode" <?php } ?> >Not Affiliate</button>
                                                                </div>
                                                                <?php
                                                            } else if ($affiliate == 'N') {
                                                                ?>
                                                                <div id="btn_div<?php echo $count; ?>">
                                                                    <button type="button" name="aff<?php echo $count; ?>" value="manage-property.php?id=affiliate&recordID=<?php
                                                                    echo $propid;
                                                                    ;
                                                                    ?>" class="btn btn-primary <?php if ($mode == 'V') { ?> not-active  <?php } ?> " <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> onclick="change_status(this)" style="width:95px;background:#36c6d3;border:1px solid #36c6d3;">Affiliate</button>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>

                                                        <a  href="ajax_edit-property.php?propId=<?php echo $propid; ?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit

                                                            </button></a>

                                                        <button type="button" class="btn btn-danger">
                                                            <a <?php if ($mode == 'V') { ?> class="not-active" <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> href="manage-property.php?id=delete&amp;recordID=<?php echo $propid; ?>" style="text-decoration:none;color:white;"   onclick="return confirm('Are you sure you want to delete this record')">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                            </a>
                                                        </button>
                                                    </div><br>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <p>&nbsp;&nbsp;
                                        <?php
                                        $date = date('m');
                                        if ($date == "01") {
                                            $mon = "January";
                                        }
                                        if ($date == "02") {
                                            $mon = "February";
                                        }
                                        if ($date == "03") {
                                            $mon = "March";
                                        }
                                        if ($date == "04") {
                                            $mon = "April";
                                        }
                                        if ($date == "05") {
                                            $mon = "May";
                                        }
                                        if ($date == "06") {
                                            $mon = "June";
                                        }
                                        if ($date == "07") {
                                            $mon = "July";
                                        }
                                        if ($date == "08") {
                                            $mon = "August";
                                        }
                                        if ($date == "09") {
                                            $mon = "September";
                                        }
                                        if ($date == "10") {
                                            $mon = "October";
                                        }
                                        if ($date == "11") {
                                            $mon = "Novemver";
                                        }
                                        if ($date == "12") {
                                            $mon = "December";
                                        }
                                        ?>
                                        Booking Table for <b><?php echo $mon; ?></b>
                                    </p>
                                    <span style="float:right;margin-top: -33px;"><a href="google-my-business.php?propID=<?php echo base64_encode($propid); ?>">Google My Business Review</a></span>
                                    <table class="table table_prop2" style="margin-left: 5px; text-align:center; width:98%;">
                                        <thead style="background:#8775A7;">
                                            <tr>
                                                <th style="text-align:center">Total Inquiry</th>
                                                <th style="text-align:center">Instant Booking</th>
                                                <th style="text-align:center">Call Back </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php
                                                $propName = $propListing->propertyName;
                                                //$propID = $propListing->propertyName;
                                                $totalBookingDat1 = $inquiryCont->GetPropertyInquiry($customerId, $propid);
                                                //$totalBookingDat1 = $ajaxpropertyCont->getTotalBooking($mon, $propName, $customerId, '');
                                                $totalBookingDat = count($totalBookingDat1);

                                                //$confirmBookingDat1 = $ajaxpropertyCont->getTotalBooking($mon, $propName, $customerId, ' and status in("Active Booking","Completed Booking")');
                                                $confirmBookingDat1 = $inquiryCont->GetPropertyInquiryType($customerId, $propid, 'instantBooking');
                                                $confirmBookingDat = count($confirmBookingDat1);

                                                // $upcomingBookingDat1 = $ajaxpropertyCont->getTotalBooking($mon, $propName, $customerId, ' and status="Inactive Booking"');
                                                $upcomingBookingDat = $inquiryCont->GetPropertyInquiryType($customerId, $propid, 'RequestaCall');
                                                $upcomingBookingDat = count($upcomingBookingDat1);

                                                $cancelled = "0";
                                                ?>
                                                <td> <?php echo $totalBookingDat; ?> </td>
                                                <td> <?php echo $confirmBookingDat; ?> </td>
                                                <td> <?php echo $upcomingBookingDat; ?> </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <p>&nbsp;&nbsp;
                                        <?php
                                        $propid = $propListing->propertyID;
                                        ?>
                                    <form action="" method="post"/>
                                    <input type="hidden" name="propID" />

                                    </form>
                                    <a href="ajax_view-property-photo.php?propID=<?php echo base64_encode($propid); ?>">View Property Photo</a>  | 
                                    <a href="ajax_manage-question.php?propID=<?php echo $propid; ?>" onclick="load_manage_question('<?php echo $propid; ?>');">Manage Questions</a>|
                                    <a href="ajax_awards.php?propID=<?php echo $propid; ?>" onclick="load_manage_awards('<?php echo $propid; ?>');">Manage Awards</a>  | 
                                    <!--<a onclick="load_manage_testimonial('<?php echo $propid; ?>');">Manage Testimonial</a> |-->
                                    <a href="ajax_foodmenu.php?propID=<?php echo $propid; ?>" onclick="load_manage_foodmenu('<?php echo $propid; ?>');">Add Food Menu</a> |
                                    <a href="ajax_trip_advisor.php?propID=<?php echo $propid; ?>" onclick="load_manage_tripAdvisor('<?php echo $propid; ?>');">Visit Trip Advisor</a> | 
                                   <!-- <a onclick="load_manage_review('<?php echo $propid; ?>');">Add Review</a>--> 
                                    <a href="ajax_add_metatags.php?propID=<?php echo $propid; ?>" >Add Meta Tags</a> |
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-xs-6 col-sm-6" style="padding-left: 5px;padding-right: 5px;">
                                <h4> Property Room's Details </h4>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $count; ?>" style="float:right;margin-bottom: 9px;" >Set Currency</button>
                                <table class="table table_prop2 table_prop3">
                                    <thead style="vertical-align: top !important;background:#8775A7;">
                                        <tr>
                                            <th style="width: 150px; vertical-align: top; text-align:left">Room Type</th>
                                            <th style="vertical-align: top; text-align:center">No. Of Rooms</th>
                                            <th style="width: 90px; vertical-align: top; text-align:center">Price</th>
                                          <!--  <th style="vertical-align: top; text-align:center">Booked Rooms</th>
                                            <th style="vertical-align: top; text-align:center">Available Rooms </th>-->
                                        </tr>
                                    </thead>
                                    <?php
                                    $roomDet1 = $ajaxpropertyCont->getRoomDetail($propid);
                                    foreach ($roomDet1 as $roomDet) {
                                        ?>
                                        <tbody style="text-align:center;">
                                            <tr>
                                                <td style="text-align:left;"> <?php echo $roomDet->roomType; ?></td>
                                                <td> <?php echo $roomDet->room_no; ?> </td>
                                                <td> <?php echo $roomDet->roomPriceINR; ?> </td>
                                               <!-- <td> 10 </td><!-- To be Mapped -->
                                               <!-- <td> 5 </td><!-- To be Mapped -->
                                            </tr>                              
                                        </tbody>
                                        <?php
                                    }
                                    ?>
                                </table>
                                <br>
                                <p style="padding:8px;">
                                    <?php
                                    $propid = $propListing->propertyID;
                                    ?>
                                    <a href="ajax_add-rooms.php?propID=<?php echo $propid; ?>">Add Rooms</a> | 
                                    <a href="ajax_view-rooms.php?propID=<?php echo $propid; ?>" >View Property Rooms</a> 
                                    <!--<a href="/update_room_rate.php?recordID=">Add Room Rates</a>-->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>	 
                <!-- Modal -->
                <div id="myModal<?php echo $count; ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Choose Currency</h4>
                            </div>
                            <div class="modal-body">
                                <p>Set Currency Property Here.</p>
                                <form class="form-horizontal" method="post" enctype="multipart/form-data" action="manage-property.php">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Choose Currency
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <select name="currency" class="form-control" required>
                                                <option value="">--Select Currency--</option>
                                                <option value="INR">India Rupee(INR)</option>
                                                <option value="dollar">Dollar</option>
                                                <option value="euro">Euro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="submit" class="btn green" name="update_currency" class="button" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?> id="submit" value="Save" />
                                                <input name="propID" type="hidden" value="<?php echo $propid; ?>" />
                                                <!--<input name="roomID" type="hidden" value="<?php echo $room; ?>" />
                                                <a href="manage-property.php?recordID=<?php echo $recordID; ?>"><input type="button" name="back" class="btn red" value="Back" /></a>-->	
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>


                <?php
                $count++;
            }
            ?>
        </div>
    </div>


    <script>
        function change_status(urldata) {

            var conf = confirm('Are you sure you want to proceed?');
            if (conf == true)
            {
                var but = urldata.name;

                var mydata = but + '~' + urldata.value;


                var replace = 'btn_div' + but;

                $.ajax({url: 'ajax_lib/ajax_change_status.php',
                    data: {url_string: mydata},
                    type: 'post',
                    success: function (output) {
                        var spl = output.split("~");
                        document.getElementById(replace).innerHTML = spl[0];
                        document.getElementById('stat_id' + but).innerHTML = spl[1];
                    }
                });
            } else
            {
                exit();
            }
        }
    </script>
    <script>
        function load_edit_property(propId) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#add_prop').html(loadng);

            $.ajax({url: 'ajax_lib/ajax_edit-property.php',
                data: {propId: propId},
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#add_prop').html(output);
                }
            });
        }
    </script>
    <script>
        function load_manage_awards(propId) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#add_prop').html(loadng);

            $.ajax({url: 'ajax_lib/ajax_awards.php',
                data: {propId: propId},
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#add_prop').html(output);
                }
            });
        }
    </script>
    <script>
        function load_manage_testimonial(propId) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#add_prop').html(loadng);

            $.ajax({url: 'ajax_lib/ajax_testimonial.php',
                data: {propId: propId},
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#add_prop').html(output);
                }
            });
        }
    </script>
    <script>
        function load_manage_foodmenu(propId) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#add_prop').html(loadng);

            $.ajax({url: 'ajax_lib/ajax_foodmenu.php',
                data: {propId: propId},
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#add_prop').html(output);
                }
            });
        }
    </script>
    <script>
        function load_manage_tripAdvisor(propId) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#add_prop').html(loadng);

            $.ajax({url: 'ajax_lib/ajax_trip_advisor.php',
                data: {propId: propId},
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#add_prop').html(output);
                }
            });
        }
    </script>
    <script>
        function load_manage_review(propId) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#add_prop').html(loadng);

            $.ajax({url: 'ajax_lib/ajax_add_review.php',
                data: {propId: propId},
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#add_prop').html(output);
                }
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("body").on('keyup', '#filter-text', function (e) {
                e.preventDefault();
                var txt = $('#“filter-text').val();
                var regEx = new RegExp($.map($(this).val().trim().split(' '), function (v) {
                    return '(?=.*?' + v + ')';
                }).join(''), 'i');

                $('.mt-list-item').hide().filter(function () {
                    return regEx.exec($(this).text());
                }).show();
            });
        });
    </script>
    <?php
}
?>