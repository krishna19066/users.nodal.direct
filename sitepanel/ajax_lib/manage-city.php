<?php
	include "cms_functions.php";
	user_locked();
	staff_restriction();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
	<head>
		<?php
		include_once("../include/main-inc.php");
		//include_once("../ajax_functions.php");
		check_session('MyCpanel','index.php');
		/*-------------------------------- CustomerID Updation -------------------------------------------*/
		$custID = $_SESSION['customerID'];

		################ Paging Starts From Here ##################
		@extract($_REQUEST);
		$start = (intval($start)==0 or $start=="")?$start=0:$start;
		$pagesize = intval($pagesize)==0?$pagesize=10:$pagesize;

		$heading="Manage City";
		$record_type="city";
		$script_name="manage-city.php";
		$table_name ="city_url";

		if($id=="delete")
			{
				//echo "ggg";
				$SARes=getResult("propertyTable"," where customerID='$custID' && cityName='$cityName' && status='Y' limit 1","propertyID");	
				if($SARes[propertyID]=='')
					{		
						//echo $type_id."hhh";
						//updateTable("city_url"," status='d' where cityID='$type_id' ");	
						$sql="DELETE FROM city_url WHERE cityID='$type_id' and customerID='$custID'";
						mysql_query($sql);
					
						$_SESSION[session_message]='Record has been deleted from the admin panel.';
						header("Location: ".SITE_ADMIN_URL."/".$script_name);
					
					}
				else
					{
						$_SESSION[session_message]='This city have Property Listing. You can not delete this city directly.';	
					}	
			}

		if($id=="activate"){


		updateTable("city_url"," status='Y' where cityID='$type_id' and customerID='$custID'");	
		//$sql="DELETE FROM city_url WHERE cityID='$type_id'";
		//mysql_query($sql);
			
		$_SESSION[session_message]='Record has been Activate from the admin panel.';
		//header("Location: ".SITE_ADMIN_URL."/".$script_name);
			exit;

		}


		//admin_header();

		?>
	</head>
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
		<div class="page-wrapper">
			<!-- BEGIN CONTAINER -->
			<?php  
				themeheader();
			?>
			<div class="page-container">
				<?php
					admin_header();
				?>
				
				<?php
					$qry1="select * from $table_name where customerID='$custID'";
					$keyword=trim($keyword);
					if($keyword!=""){
					  $qry1.=" and (cityName LIKE '%$keyword%')"; 
					}

					if($_REQUEST[sortName]!=''){
						$qry1 .= " order by $_REQUEST[sortName] $_REQUEST[sortBy]";
					}
					else {
						$qry1 .= " order by cityID asc";
					}

					$reccnt = NumRows(db_query($qry1));
					$qry1.=" limit $start, $pagesize";
					#echo $qry1;
					$qry= db_query($qry1);
					echo pageNavigation($reccnt,$pagesize,$start,$link);
					//print_message(); 
				?>
				
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
						<div class="row">
							<div  class="col-md-12">
								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption">
											<i class="icon-equalizer font-red-sunglo"></i>
											<span class="caption-subject font-red-sunglo bold uppercase">Manage City Page</span>
											<span class="caption-helper">Get the details of all city and manage it .</span>
											<span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
										</div>
									</div>
									<span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add New City" /></span><br><br>
								</div>
								<!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
								<div class="portlet light bordered" id="add_photo" style="display:none;">
									<div class="portlet-body form">
										<form class="form-horizontal" name="formn" method="post" action="add-city.php" enctype="multipart/form-data" onsubmit="return validate(this);">
											<div class="form-body">
												<div class="form-group">
													<label class="control-label col-md-3"> City Name
														<span class="required"> * </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="type_name" value="<?=$type_name?>" id="NOBLANK~Please enter category name~DM~">
														<span class="help-block"> Provide the City Name</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-3 control-label">Hotel Type</label>
													<div class="col-md-9">
														<div class="mt-checkbox-inline">
															<?php
																$selproptype = "select * from tbl_property_type where customerID='$custID'";
																$proptypquer = mysql_query($selproptype);
																while($proptypdata = mysql_fetch_array($proptypquer))
																	{
															?>
																		<label class="mt-checkbox">	
																			<input type="checkbox" name="propertyCounter[]" id="inlineCheckbox21" value="<?php echo $proptypdata['type_name']; ?>"/><?php echo $proptypdata['type_name']; ?>
																			<span></span>
																		</label>
															<?php
																	}
															?>
															<!--<label class="mt-checkbox">	
																<input type="checkbox" name="ghCounter" id="inlineCheckbox21" value="Y"  <?//=$chk1;?>/>Guest House
																<span></span>
															</label>-->
														</div>
													</div>
												</div>
												
												<div class="form-actions">
													<div class="row">
														<div class="col-md-offset-3 col-md-9">
															<?php
																if(isset($res['cityID'])=="")
																	{
															?>
																		<input type="submit" name="add_record" class="btn green" id="submit" value="Add <?=$record_type;?>">
																		<input type="hidden" name="action" value="add_record">
																		<input type="hidden" name="action" value="add_record">
															<?php
																	}
																else
																	{
															?>
																		<input type="submit" name="update_record" class="btn" id="submit" value="Update <?=$record_type;?>">
																		<input type="hidden" name="action" value="update_record">
																		<input type="hidden" name="type_id" value="<?=$res[cityID];?>">
															<?php
																	}
															?>
															
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							
							<?php
								if($reccnt>0)
									{
										$cnt = 1;
										$citysel = "select * from city_url where customerID='$custID' group by cityName";
										$cityquery = mysql_query($citysel);
										while($cityrow = mysql_fetch_array($cityquery))
											{
												$cityName = $cityrow['cityName'];
												$cityID = $cityrow['cityID'];
												
												if($cnt=="1")
													{$color = "#32c5d2";}
												if($cnt=="2")
													{$color = "#3598dc";}
												if($cnt=="3")
													{$color = "#36D7B7";}
												if($cnt=="4")
													{$color = "#5e738b";}
												if($cnt=="5")
													{$color = "#1BA39C";}
												if($cnt=="6")
													{$color = "#32c5d2";}
												if($cnt=="7")
													{$color = "#578ebe";}
												if($cnt=="7")
													{$color = "#8775a7";}
												if($cnt=="7")
													{$color = "#E26A6A";}
												if($cnt=="7")
													{$color = "#29b4b6";}
												if($cnt=="7")
													{$color = "#4B77BE";}
												if($cnt=="7")
													{$color = "#c49f47";}
												if($cnt=="7")
													{$color = "#67809F";}
												if($cnt=="7")
													{$color = "#8775a7";}
												if($cnt=="7")
													{$color = "#73CEBB";}
							?>
												<div class="col-md-12">
													<div class="m-heading-1  m-bordered" style="padding:0px; border-color:<?php echo $color; ?>;">
														<div style="border:1px solid <?php echo $color; ?>; color:#fff; background:<?php echo $color; ?>;">
															<p> <b><span style="font-size:large"><?php echo $cityrow['cityName']; ?></span></b>
															<span style="color:#fff; float:right; font-size:x-large">
															<a href="<?=$script_name;?>?id=delete&type_id=<?php echo $cityID; ?>" onClick="return confirm('Are you sure you want to Delete this record')"><span style="color:#fff;" class=" fa fa-trash"></span></a>
															<a href="add-city.php?type_id=<?php echo $cityID; ?>"><span style="padding:0px 12px; 0px 12px; color:#fff;" class="fa fa-edit"></span></a></span>
															<?php
																$countsel = "select count(propertyID) from propertyTable where cityName='$cityName' and customerID='$custID'";
																$countquer = mysql_query($countsel);
																while($countrow = mysql_fetch_array($countquer))
																	{
																		$countTotal = $countrow['count(propertyID)'];
																	}
															?>
															<br>Total Property :  <b><?php echo $countTotal; ?></b> </p>                                    
														</div>                                    
														<div style="padding:30px 10px 10px 10px;">
														
															
															<?php
																$citNam = $cityrow['cityName'];
																$selcit = "select * from city_url where customerID='$custID' and cityName='$citNam'";
																$selcitquery = mysql_query($selcit);
																while($selcitydata = mysql_fetch_array($selcitquery))
																	{
																		$proTyp = $selcitydata['property_type'];
															?>
																		<?php
																			$typesel = "select * from tbl_property_type where type_name='$proTyp' and customerID='$custID'";
																			$typequer = mysql_query($typesel);
																			while($typerow = mysql_fetch_array($typequer))
																				{
																					$type = $typerow['type_id'];
																					$actsel = "select count(propertyID) from propertyTable where cityName='$cityName' and tbl_property_type='$type' and customerID='$custID'";
																					$actquery = mysql_query($actsel);
																					while($actrow = mysql_fetch_array($actquery))
																						{
																							$actTotal = $actrow['count(propertyID)'];
																						}
																				}
																		?>
																		<span> <b><?php echo $selcitydata['property_type']; ?> </b>  - Total Property : <b><?php echo $actTotal; ?></b></span> 
																		
																		<a style="float:right;" href="add-city-data.php?id=edit&typeCounter=<?php echo $selcitydata['property_type']; ?>&type_id=<?php echo $cityID; ?>">Edit Content</a> <br>
															
															<?php
																	}
															?>
														</div>
													</div>
												</div>
							<?php
												$cnt++;
											}
							?>
										
										
								
								<?php
							}
							else{
								?>
								<table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
								 <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
								</table>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>



<?php
admin_footer();	
exit;
?>