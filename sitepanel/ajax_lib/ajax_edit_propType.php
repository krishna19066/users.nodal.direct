<?php
 $proTypeID = $_REQUEST['proTypeID'];
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$propType = $propertyCont->GetPropertyTypeWithID($proTypeID);
$res = $propType[0];
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_propertytype();"><i class="fa fa-plus"></i> &nbsp Add Property Type</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Edit Property Type </span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" action="../sitepanel/add_property.php" id="submit_form" method="post">
                    <div class="form-wizard">
                        <div class="form-body">
                            <div class="tab-content"> 
                                <h3 class="block">Edit Property Type</h3>

                                <div class="form-group">
                                    <label class="control-label col-md-3">  Property Type
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="type_name" value="<?php echo $res->type_name; ?>" />

                                        <span class="help-block"> Edit your Property Type.</span>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a onclick="load_property_type();" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back 
                                    </a>
                                    <input type="hidden" name="propTypeID" value="<?php echo $proTypeID; ?>">
                                    <input type="submit" class="btn green" name="edit_property_type" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><br><br>
