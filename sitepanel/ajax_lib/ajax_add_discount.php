<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
$propertyCont = new propertyData();
$propertyData = new inventoryData();
$discountData = new discountOffers();
$discount_res = $discountData->getDiscounts($customerId);
$prop_resData = $propertyData->getPropertyListing($customerId,$user_id);
$res_prop = $prop_resData[0];

?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <a href="manage-discounts.php"> <button style="background:red;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;"><i class="fa fa-backward"></i> &nbsp Back</button></a>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Add New Discount</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>



            <?php
            if (!isset($_POST['propSelect'])) {
                if ($_SERVER['QUERY_STRING'] == false) {
                    ?>
                    <?php
                    if (isset($_SESSION['propError'])) {
                        ?>
                        <center><font style="font-size:22px;color:#990000;"> <?php echo $_SESSION['propError']; ?> </font></center>
                        <?php
                        unset($_SESSION['propError']);
                    }
                    ?>

                    <div class="portlet-body form">
                        <form class="form-horizontal" method="post" action="../sitepanel/add-new-discount.php" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Property Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select name="propertyName" class="help-inline" id="property_checkin_time_am_pm" data-prefill="">
                                            <option value="">Select</option>
                                            <?php
                                            //$sele = "select propertyName from propertyTable where customerID='$custID'";
                                            //$quer1 = mysql_query($sele);
                                            foreach ($prop_resData as $prop) {
                                                ?>
                                            <option value="<?php echo $prop->propertyName;  ?>"> <?php echo $prop->propertyName; ?> </option>
                                                <?php
                                            }
                                            ?>  
                                        </select>
                                        <span class="help-block"> Select Desired Property</span>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <input type="submit" name="propSelect" class="btn green" id="submit" value="Go" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>
                                            <a href="manage-discounts.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php
                }
            }
            ?>


            <?php
            if (!isset($_POST['propSelect'])) {
                if ($_SERVER['QUERY_STRING'] == true) {
                    $rec = $recordID;
                  //  $pro_sel = "select * from tbl_discount where discountID='$rec'";
                  //  $pro_quer = mysql_query($pro_sel);
                    foreach ($discount_res as $prop_row) {
                        $property = $pro_row->propertyName;
                        $room_nam = $pro_row->roomName;
                        $disc = $pro_row->discountName;
                        $disc_typ = $pro_row->discountType;
                        $disc_per = $pro_row->discountValue;
                        $fro = $pro_row->discountFrom;
                        $to = $pro_row->discountTo;
                        $lastmin = $pro_row->lastMinute;
                        $earlb = $pro_row->earlybird;
                        $longst = $pro_row->longStay;
                    }
                   // $ro_sel = "select * from propertyRoom where roomID='$room_nam'";
                   // $ro_quer = mysql_query($ro_sel);
                    $result_room = $propertyData->getRoomDetailsWithRoomID($room_nam);
                     foreach ($result_room as $res_room) {
                        $room_act = $res_room->roomType;
                    }
                    ?>
                    <div class="portlet-body form">
                        <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                            <input type="hidden" name="recordID" value="<?php echo $rec; ?>" />
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"> Property Selected
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="propName" value="<?php echo $property; ?>" readonly="" >
                                        <span class="help-block"> Selected Property Name</span>
                                    </div>
                                </div>

                                <div class="form-group last">
                                    <label class="control-label col-md-3">Select Room
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <table>
                                            <tr>
                                                <?php
                                                $selRoom = "select propertyID from propertyTable where propertyName='$property'";
                                                $querRoom = mysql_query($selRoom);
                                                while ($ro = mysql_fetch_array($querRoom)) {
                                                    $ide = $ro['propertyID'];
                                                    $sele = "select roomType,roomID from propertyRoom where propertyID='$ide'";
                                                    $quer1 = mysql_query($sele);
                                                    while ($r = mysql_fetch_array($quer1)) {
                                                        ?>
                                                        <td style="padding-left:50px;">
                                                            <div class="mt-checkbox-inline">
                                                                <label class="mt-checkbox">
                                                                    <input type="checkbox" name="roomName[]" id="inlineCheckbox21" value="<?php echo $r['roomID']; ?>" <?php
                                                                    if ($r['roomType'] == $room_act) {
                                                                        echo "checked";
                                                                    }
                                                                    ?>> <?php echo $r['roomType']; ?>
                                                                    <span></span>
                                                                </label>

                                                            </div>
                                                        </td>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"> Discount Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="discountName" value="<?php echo $disc; ?>">
                                        <span class="help-block"> Name of Discount</span>
                                    </div>
                                </div>

                                <div class="form-group last">
                                    <label class="control-label col-md-3"> Discount Type
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="radio-list">
                                            <table>
                                                <tr>
                                                    <td>
                                                <center><input type="radio" name="discountType" value="basicdeal" required <?php
                                                    if ($disc_typ == "basicdeal") {
                                                        echo "checked";
                                                    }
                                                    ?> />
                                                    <center><span class="help-block"> Basic Deal</span>
                                                        </td>

                                                        <td style="padding-left:50px;">
                                                        <center><input type="radio" id="radio-1-0" name="discountType" value="lastminute" <?php
                                                            if ($disc_typ == "lastminute") {
                                                                echo "checked";
                                                            }
                                                            ?> />
                                                            <center><span class="help-block"> Last Minute</span>
                                                                </td>

                                                                <td style="padding-left:50px;">
                                                                <center><input type="radio" id="radio-1-1" name="discountType" value="earlybird" <?php
                                                                    if ($disc_typ == "earlybird") {
                                                                        echo "checked";
                                                                    }
                                                                    ?> />
                                                                    <center><span class="help-block"> Early Bird</span>
                                                                        </td>

                                                                        <td style="padding-left:50px;">
                                                                        <center><input type="radio" id="radio-1-2" name="discountType" value="longstay" <?php
                                                                            if ($disc_typ == "longstay") {
                                                                                echo "checked";
                                                                            }
                                                                            ?> />
                                                                            <center><span class="help-block"> Long Stay</span>
                                                                                </td>
                                                                                </tr>
                                                                                </table>
                                                                                </div>
                                                                                </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"> Discount Percentage
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-4">
                                                                                        <input type="text" class="form-control" name="discountPercent" value="<?php echo $disc_per; ?>">
                                                                                        <span class="help-block"> Provide Discount Percentage</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"> Discount From
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-4">
                                                                                        <input type="date" class="form-control" name="discountFrom" value="<?php echo $fro; ?>">
                                                                                        <span class="help-block"> Provide Discount Initiation Date</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"> Discount To
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-4">
                                                                                        <input type="date" class="form-control" name="discountTo" value="<?php echo $to; ?>">
                                                                                        <span class="help-block"> Provide Discount Conclusion Date</span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group last">
                                                                                    <label class="control-label col-md-3"> Discount Specifications
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-9">
                                                                                        <div id="dvlastminute" style="<?php if ($disc_typ == "lastminute") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                                                            <font style="color:#2d89b7;">CheckIn within following Days of Booking *</font> &nbsp &nbsp
                                                                                            <input type="text" name="lastMinute" class="discountText" value="<?php echo $lastmin; ?>" />
                                                                                        </div>

                                                                                        <div id="dvearlybird" style="<?php if ($disc_typ == "earlybird") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                                                            <font style="color:#2d89b7;">Check in before following Days of booking *</font> &nbsp &nbsp
                                                                                            <input type="text" name="earlyBird" class="discountText" value="<?php echo $earlb; ?>" />
                                                                                        </div>

                                                                                        <div id="dvlongstay" style="<?php if ($disc_typ == "longstay") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                                                            <font style="color:#2d89b7;">Need to stay following days * <font> &nbsp &nbsp
                                                                                            <input type="text" name="longStay" class="discountText" value="<?php echo $longst; ?>" /><br><br><br>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-actions">
                                                                                    <div class="row">
                                                                                        <div class="col-md-offset-3 col-md-9">
                                                                                            <input type="submit" name="editDiscount" class="btn green" id="submit" value="Edit Discount">
                                                                                            <a href="manage-discounts.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                </form>
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>


                                                                        <?php
                                                                        if (isset($_POST['propSelect'])) {
                                                                            $property = $_POST['propertyName'];
                                                                            //echo $property;
                                                                            if ($property == "") {
                                                                                Header("location:add-new-discount.php");
                                                                                $_SESSION['propError'] = "Please Select A Valid Property Name..";
                                                                            }
                                                                            ?>
                                                                            <div class="portlet-body form">
                                                                                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                                                                    <div class="form-body">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3"> Property Selected
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-4">
                                                                                                <input type="text" class="form-control" name="propName" value="<?php echo $property; ?>" readonly="" >
                                                                                                <span class="help-block"> Selected Property Name</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group last">
                                                                                            <label class="control-label col-md-3">Select Room
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-9">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <?php
                                                                                                        $selRoom = "select propertyID from propertyTable where propertyName='$property'";
                                                                                                        $querRoom = mysql_query($selRoom);
                                                                                                        while ($ro = mysql_fetch_array($querRoom)) {
                                                                                                            $ide = $ro['propertyID'];
                                                                                                            $sele = "select roomType,roomID from propertyRoom where propertyID='$ide'";
                                                                                                            $quer1 = mysql_query($sele);
                                                                                                            while ($r = mysql_fetch_array($quer1)) {
                                                                                                                ?>
                                                                                                                <td style="padding-left:50px;">
                                                                                                                    <div class="mt-checkbox-inline">
                                                                                                                        <label class="mt-checkbox">
                                                                                                                            <input type="checkbox" name="roomName[]" id="inlineCheckbox21" value="<?php echo $r['roomID']; ?>"> <?php echo $r['roomType']; ?>
                                                                                                                            <span></span>
                                                                                                                        </label>

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                                <?php
                                                                                                            }
                                                                                                        }
                                                                                                        ?>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3"> Discount Name
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-4">
                                                                                                <input type="text" class="form-control" name="discountName">
                                                                                                <span class="help-block"> Name of Discount</span>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group last">
                                                                                            <label class="control-label col-md-3"> Discount Type
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-9">
                                                                                                <div class="radio-list">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                        <center><input type="radio" name="discountType" value="basicdeal" required />
                                                                                                            <center><span class="help-block"> Basic Deal</span>
                                                                                                                </td>

                                                                                                                <td style="padding-left:50px;">
                                                                                                                <center><input type="radio" id="radio-1-0" name="discountType" value="lastminute" />
                                                                                                                    <center><span class="help-block"> Last Minute</span>
                                                                                                                        </td>

                                                                                                                        <td style="padding-left:50px;">
                                                                                                                        <center><input type="radio" id="radio-1-1" name="discountType" value="earlybird" />
                                                                                                                            <center><span class="help-block"> Early Bird</span>
                                                                                                                                </td>

                                                                                                                                <td style="padding-left:50px;">
                                                                                                                                <center><input type="radio" id="radio-1-2" name="discountType" value="longstay" />
                                                                                                                                    <center><span class="help-block"> Long Stay</span>
                                                                                                                                        </td>
                                                                                                                                        </tr>
                                                                                                                                        </table>
                                                                                                                                        </div>
                                                                                                                                        </div>
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="control-label col-md-3"> Discount Percentage
                                                                                                                                                <span class="required"> * </span>
                                                                                                                                            </label>
                                                                                                                                            <div class="col-md-4">
                                                                                                                                                <input type="text" class="form-control" name="discountPercent">
                                                                                                                                                <span class="help-block"> Provide Discount Percentage</span>
                                                                                                                                            </div>
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="control-label col-md-3"> Discount From
                                                                                                                                                <span class="required"> * </span>
                                                                                                                                            </label>
                                                                                                                                            <div class="col-md-4">
                                                                                                                                                <input type="date" class="form-control" name="discountFrom">
                                                                                                                                                <span class="help-block"> Provide Discount Initiation Date</span>
                                                                                                                                            </div>
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="control-label col-md-3"> Discount To
                                                                                                                                                <span class="required"> * </span>
                                                                                                                                            </label>
                                                                                                                                            <div class="col-md-4">
                                                                                                                                                <input type="date" class="form-control" name="discountTo">
                                                                                                                                                <span class="help-block"> Provide Discount Conclusion Date</span>
                                                                                                                                            </div>
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group last">
                                                                                                                                            <label class="control-label col-md-3"> Discount Specifications
                                                                                                                                                <span class="required"> * </span>
                                                                                                                                            </label>
                                                                                                                                            <div class="col-md-9">
                                                                                                                                                <div id="dvlastminute" style="display:none;"> 
                                                                                                                                                    <font style="color:#2d89b7;">CheckIn within following Days of Booking *</font> &nbsp &nbsp
                                                                                                                                                    <input type="text" name="lastMinute" class="discountText" />
                                                                                                                                                </div>

                                                                                                                                                <div id="dvearlybird" style="display:none;"> 
                                                                                                                                                    <font style="color:#2d89b7;">Check in before following Days of booking *</font> &nbsp &nbsp
                                                                                                                                                    <input type="text" name="earlyBird" class="discountText" />
                                                                                                                                                </div>

                                                                                                                                                <div id="dvlongstay" style="display:none;"> 
                                                                                                                                                    <font style="color:#2d89b7;">Need to stay following days * <font> &nbsp &nbsp
                                                                                                                                                    <input type="text" name="longStay" class="discountText" /><br><br><br>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>

                                                                                                                                        <div class="form-actions">
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                                                                    <input type="submit" name="addDiscount" class="btn green" id="submit" value="Add Discount">
                                                                                                                                                    <a href="manage-discounts.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        </div>
                                                                                                                                        </form>
                                                                                                                                        </div>
                                                                                                                                        <?php
                                                                                                                                    }
                                                                                                                                    ?>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    </div>
                                                                                                                                    




