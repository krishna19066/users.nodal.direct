<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$propertyId = $_POST['propertyId'];
?>
<head>
    <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

</head>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
    <?php
    themeheader();
    ?>
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php
        admin_header();
        ?>

        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="background:#e9ecf3;">
                <div class="page-head">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1> PROPERTY DETAILS
                            <small>View all the details of your Properties</small>
                        </h1>
                    </div>
                </div>

                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="welcome.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="welcome.php">My Properties</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>View / Add Property Video</span>
                    </li>
                </ul>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-body ">
                                <div style="width:100%;" class="clear"> 
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Property Video" /></span>
                                   <!-- <span style="float:left;"><input type="button" style="background:#363bd3;color:white;border:none;height:35px;width:180px;font-size:14px; margin-left: 18px;" data-html="true" onclick="load_ReorderpropertyPhoto(<?php echo $propertyId; ?>);" value="Reorder Property Photo" /></span>-->
                                    <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>
                                    <span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> View Property Photo </button></a></span>

                                    <br><br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                <div class="portlet light bordered" id="add_photo" style="display:none;">			
                    <div class="portlet-body form">
                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php">
                            <div class="form-body">
                                <h4 align="center" style="color:#E26A6A;"><b> Add New Property Video </b></h3><br><br>                                                                                   
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Video Heading
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input name="video_heading" type="text" class="form-control" />
                                            <span class="help-block"> Provide your Heading</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Video Url
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input name="video_url" type="text" class="form-control" />
                                            <span class="help-block"> Provide your Embed Url not iframe</span>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input name="propertyID" type="hidden" value="<?php echo $propertyId ?>" />
                                                <input type="submit" name="submit_video_gallery" class="btn green" id="submit" value="Add Video">
                                                <button type="button" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->

                <div class="row">
                    <div class="col-md-12">	
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-body">
                                <div class=" mt-element-overlay">
                                    <div class="row">
                                        <?php
                                        $propVideoListdata = $ajaxpropertyPhotoCont->getPropertyVideo($propertyId, $customerId);
                                        if ($propVideoListdata != NULL) {

                                            foreach ($propVideoListdata as $propVideo) {
                                                ?>
                                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                                    <div class=" mt-overlay-1" style="height: 25%;">
                                                          <iframe width="100%" height="100%" src="<?php echo $propVideo->video_url; ?>" frameborder="0" allowfullscreen></iframe>
                                                        <!--<img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_564,h_400,c_fill/<?php if ($customerId != 1) { ?>reputize<?php } ?>/property/<?php echo $propPhotoList->imageURL; ?>.jpg" />-->

                                                        <div class="mt-overlay">
                                                            <ul class="mt-info">
                                                                <li>
                                                                    <a class="btn default btn-outline" href="javascript:;">
                                                                        <i class="icon-magnifier"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a class="btn default btn-outline" href="../sitepanel/add_property.php?action2=deleteVideo&videoID=<?php echo $propVideo->id; ?>">
                                                                        <i class="icon-trash"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>


                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }
                                        } else {
                                            ?>

                                            <center><div>
                                                    <h3>No Record Found..</h3>
                                                </div></center> 
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="quick-nav-overlay"></div>
                    <!-- BEGIN CORE PLUGINS -->
                    <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

                    <!-- END CORE PLUGINS -->
                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    <script src="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                    <script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL PLUGINS -->
                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                    <script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL SCRIPTS -->
                    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

                    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
                    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

                    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>

                    <script src="assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>

                    <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
                    <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
                    <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
                    <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

                    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
