<?php
include "../admin-function.php";

$customerId = $_SESSION['customerID'];
$propertytypeCont = new propertyData();
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_propertytype();"><i class="fa fa-plus"></i> &nbsp Add Property Type</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php
    $propertytypeCont1 = $propertytypeCont->getAllPropTyp($customerId);
    foreach ($propertytypeCont1 as $propertytypeData) {
        $propertytypeName = $propertytypeData->type_name;
        $proTypeID = $propertytypeData->type_id;
        ?>
        <div class="col-md-6">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"><?php echo $propertytypeName; ?></span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="actions">
                        <a onclick="load_edit_propType(<?php echo $proTypeID ?>);" class="btn btn-circle btn-default">
                            <i class="fa fa-pencil"></i> Edit </a>
                        <a href="manage-property-settings.php?id=delete_propType&propTypeID=<?php echo $proTypeID; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-circle btn-danger">
                            <i class="fa fa-trash"></i> Delete </a>
                        <!--<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height:200px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                        <?php
                        $cityPropTypeList1 = $propertytypeCont->getAllPropTyp($customerId);
                        foreach ($cityPropTypeList1 as $cityPropTypeData) {
                            $cityPropType = $cityPropTypeData->type_name;
                            $cityPropTypeID = $cityPropTypeData->type_id;
                            ?>

                            <?php
                            $cityPropTypeDataList1 = $propertytypeCont->getCityPropertyByType($customerId, 'All', $cityPropTypeID);
                            $cityPropTypeCount = count($cityPropTypeDataList1);
                            ?>
                            <p> Total Properties : <?php echo $cityPropTypeCount; ?> </p>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
        <?php
    }
    ?>
</div><br><br>
<script>
    function load_edit_propType(proTypeID) {
        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
        $('#cityCont').html(loadng);


        //var replace = 'btn_div' + but;
        // alert(cityid);

        $.ajax({url: 'ajax_lib/ajax_edit_propType.php',
            type: 'post',
            data: {proTypeID: proTypeID},
            success: function (output) {
                // alert(output);
                $('#cityCont').html(output);
            }
        });
    }
</script>