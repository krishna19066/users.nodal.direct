<?php
include "../admin-function.php";
$customerId = $_SESSION['customerID'];
$subCityCont = new propertyData();
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body ">
                <div style="width:100%;" class="clear"> 
                    <a class="pull-right">
                        <a href="ajax_add-roomtype.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;float: right;"><i class="fa fa-plus"></i> &nbsp Add Room Type</button></a>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php
    $subCityListings1 = $subCityCont->getAllRoomTyp($customerId);
    foreach ($subCityListings1 as $subCityData) {
        $subCityName = $subCityData->apt_type_name;
        $roomTypeID = $subCityData->apt_type_id;
        ?>
        <div class="col-md-6">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <i class="icon-speech font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"><?php echo $subCityName; ?></span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="actions">
                        <a href="ajax_edit_roomType.php?roomTypeID=<?php echo $roomTypeID; ?>" onclick="load_edit_roomType(<?php echo $roomTypeID ?>);" class="btn btn-circle btn-default">
                            <i class="fa fa-pencil"></i> Edit </a>
                            <a href="manage-property-settings.php?id=delete_roomType&roomTypeID=<?php echo $roomTypeID; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-circle btn-danger">
                            <i class="fa fa-trash"></i> Delete </a>
                        <!--<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>-->
                    </div>
                </div>

            </div>
            <!-- END Portlet PORTLET-->
        </div>
        <?php
    }
    ?>
</div><br><br>
<script>
    function load_edit_roomType(roomTypeID) {
        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
        $('#cityCont').html(loadng);


        //var replace = 'btn_div' + but;
        // alert(cityid);

        $.ajax({url: 'ajax_lib/ajax_edit_roomType.php',
            type: 'post',
            data: {roomTypeID: roomTypeID},
            success: function (output) {
                // alert(output);
                $('#cityCont').html(output);
            }
        });
    }
</script>