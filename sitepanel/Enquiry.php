<?php

include "/home/hawthorntech/public_html/stayondiscount.com/sitepanel/admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$enquiryData = new manageEnquiry();
//$getHeader = $menuCont->GetHeaderFooterData($customerId);
$id = $_GET['id'];
$inqid = $_GET['inqid'];

if ($id == "delete") {
    $recordID = $_GET['recordID'];
    // $del = "delete from inquiry where slno='$recordID'";
    // $delquer = mysql_query($del);
    $delete_enq = $enquiryData->DeleteEnquiryData($recordID);

    // updateTable($table_name, " status='D' where slno='$recordID'");
    Header("Location:manage-enquiry.php");
    exit;
}
if (isset($_POST['update_record'])) {

    $stat = $_POST['act'];
    $nam = $_POST['us_nam'];
    $maill = $_POST['us_mail'];
    $com_up = $_POST['comment_update'];
    $sl = $_POST['slno'];

    if ($stat == "Select") {
        $_SESSION['err'] = "Plese Select a Valid Status";
        Header("Location:manage-enquiry.php");
        exit;
    } else {
        // $upd = "update inquiry set comments='$com_up',com_stat='$stat' where slno='$sl'";
        // $que = mysql_query($upd);
        $upd = $enquiryData->UpdateInquiryStatus($customerId, $nam, $maill, $com_up, $sl, $stat);
        $ins = $enquiryData->AddComments($customerId, $sl, $com_up, $stat, $nam);

        $_SESSION['err'] = "Your Status has been Updated";
        Header("Location:manage-enquiry.php");
        exit;
    }
}
if (isset($_REQUEST['AddComments_Sub'])) {
    $stat = $_POST['act'];
    $nam = $_POST['us_nam'];   
    $addComment = $_POST['addComment'];
    $sl = $_POST['slno'];
    $ins = $enquiryData->AddComments($customerId, $sl, $addComment, $stat, $nam);
    Header("Location:manage-enquiry.php");
    exit;
}

if (isset($_POST['send_mail'])) {

    $nam = $_POST['us_nam'];
    echo $maill = $_POST['us_mail'];
    $sl = $_POST['slno'];
    $com = $_POST['comm'];
    $comm1 = html_entity_decode($com);
    $mail_sig1 = $enquiryData->GetMailSignature();
    //$mail_sig1 = "select * from inquiry_mail_signature";
    // $mail_qu = mysql_query($mail_sig1);
    foreach ($mail_sig1 as $row) {
        $mail_signat = $row->signature;
    }
    //------------------------------------------------------------ Mailing Section Starts ---------------------------------------------------------------------//
    if ($_POST && isset($_FILES['mail_attachment'])) {
        //---------------------------------------- Reply Mailing Starts --------------------------------------------------//
        $com_act = $comm1 . "<br><br><br>" . $mail_signat;
        $sub = "Re - Your Inquiry Made";
        $from_email = 'tech@theperch.in'; //from mail, it is mandatory with some hosts
        $recipient_email = $maill; //recipient email (most cases it is your personal email)
        //Capture POST data from HTML form and Sanitize them, 
        //$sender_name    = filter_var($_POST["sender_name"], FILTER_SANITIZE_STRING); //sender name
        //$reply_to_email = filter_var($_POST["sender_email"], FILTER_SANITIZE_STRING); //sender email used in "reply-to" header
        $subject = $sub; //get subject from HTML form
        $message = $com_act; //message

        /* //don't forget to validate empty fields 
          if(strlen($sender_name)<1){
          die('Name is too short or empty!');
          }
         */

        //Get uploaded file data
        $file_tmp_name = $_FILES['mail_attachment']['tmp_name'];
        $file_name = $_FILES['mail_attachment']['name'];
        $file_size = $_FILES['mail_attachment']['size'];
        $file_type = $_FILES['mail_attachment']['type'];
        $file_error = $_FILES['mail_attachment']['error'];

        //----------------------------------------------------------------------- Mail Without Attachment ------------------------------------------------------------------//
        if ($file_error > 0) {
            $mes = $com_act;
            $to_mail = $maill;
            //$to_email="tech@theperch.in";
            //$subject1="Call Enquiry from ThePerch.in";
            $from = 'sales@theperch.in';

            $headers = "From: sales@theperch.in\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-Type: text/html; charset=utf-8\r\n";
            $headers.= "X-Priority: 1\r\n";

            mail($to_mail, $sub, $mes, $headers);

            //  $mail_ins = "update inquiry set comm='$com' where slno='$sl'";
            //   $mail_ins_quer = mysql_query($mail_ins);
            $upd = $enquiryData->UpdateInquiryMail($com, $sl);

            $_SESSION['mail_sent'] = "Your mail has successfully been sent.";
            Header("Location:manage-enquiry.php");
            exit;
        }
        //----------------------------------------------------------------------- Mail Without Attachment Ends ------------------------------------------------------------------//
        //----------------------------------------------------------------------- Mail With Attachment Starts ------------------------------------------------------------------//
        else {
            //read from the uploaded file & base64_encode content for the mail
            $handle = fopen($file_tmp_name, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $encoded_content = chunk_split(base64_encode($content));

            $boundary = md5("sanwebe");
            //header
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From: sales@theperch.in\r\n";
            $headers .= "Reply-To: " . $maill . "" . "\r\n";
            $headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";

            //plain text 
            $body = "--$boundary\r\n";
            $body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $body .= "Content-Transfer-Encoding: base64\r\n\r\n";
            $body .= chunk_split(base64_encode($message));

            //attachment
            $body .= "--$boundary\r\n";
            $body .="Content-Type: $file_type; name=" . $file_name . "\r\n";
            $body .="Content-Disposition: attachment; filename=" . $file_name . "\r\n";
            $body .="Content-Transfer-Encoding: base64\r\n";
            $body .="X-Attachment-Id: " . rand(1000, 99999) . "\r\n\r\n";
            $body .= $encoded_content;

            $sentMail = @mail($recipient_email, $subject, $body, $headers);
            if ($sentMail) { //output success or failure messages
                // $mail_ins = "update inquiry set comm='$com' where slno='$sl'";
                // $mail_ins_quer = mysql_query($mail_ins);
                $upd = $enquiryData->UpdateInquiryMail($com, $sl);

                $_SESSION['mail_sent'] = "Your mail has successfully been sent.";
                Header("Location:manage-enquiry.php");
            } else {
                die('Could not send mail! Please check your PHP mail configuration.');
            }
        }
    }
    //------------------------------------------------------------ Mailing Section Ends ---------------------------------------------------------------------//
}
?>