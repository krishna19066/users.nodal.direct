<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$enquiryData = new manageEnquiry();
//$get_mail = $enquiryData->GetMailTemplate($customerId);
?>
<?php
if (empty($type_id)) {
    $heading = "Add New Mail Template";
}


$script_name = "edit_mail_template.php";
$redirect_script_name = "manage_mail_template.php";

#################### Add Record ##################
//admin_header();
//$con = mysql_connect("localhost","root","","tes") or die("no connection");
$id = $_SERVER['QUERY_STRING'];
//$sel = "select * from mail_templates where s_no='$id'";
$quer = mysql_query($sel);
$get_mail = $enquiryData->GetMailTemplateWithID($id);
foreach ($get_mail as $row) {
    ?>
    <html>

        <head>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }

                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }

                tr:nth-child(odd){
                    background-color:#fff;
                }

                th {

                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}

                .image img {
                    -webkit-transition: all 1s ease; /* Safari and Chrome */
                    -moz-transition: all 1s ease; /* Firefox */
                    -ms-transition: all 1s ease; /* IE 9 */
                    -o-transition: all 1s ease; /* Opera */
                    transition: all 1s ease;
                }

                .image:hover img {
                    -webkit-transform:scale(1.25); /* Safari and Chrome */
                    -moz-transform:scale(1.25); /* Firefox */
                    -ms-transform:scale(1.25); /* IE 9 */
                    -o-transform:scale(1.25); /* Opera */
                    transform:scale(1.25);
                }		

            </style>
            <script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

            <?php
            adminCss();
            ?>
        </head>


        <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

            <div class="page-wrapper">
                <!-- BEGIN CONTAINER -->
                <?php
                themeheader();
                ?>
                <div class="page-container">
                    <?php
                    admin_header();
                    ?>

                    <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content">
                            <div class="add_temp" style="background:white;">
                                <div class="row">
                                    <div  class="col-md-12">
                                        <div class="portlet light bordered" style="padding-bottom:15px ! important;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-equalizer font-red-sunglo"></i>
                                                    <span class="caption-subject font-red-sunglo bold uppercase"><?php echo $row->name; ?> </span>
                                                    <span class="caption-helper">Edit your email template..</span>
                                                </div>
                                            </div>

                                            <div class="portlet-body form" style="display:block;" id="your_email_template"><br><br>
                                                <form  action="mail_template_insert.php" method="post">
                                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                                    <div class="form-body">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <div class="form-group form-md-line-input" >
                                                                        <input type="text" name="temp_name1" class="form-control" id="form_control_1" value="<?php echo $row->name; ?>">
                                                                        <label for="form_control_1">Template Name</label>
                                                                        <span class="help-block">Change the name you wish to give to your template..</span>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group form-md-line-input" >
                                                                        <input type="text" name="temp_type1" class="form-control" id="form_control_1" value="<?php echo $row->type; ?>" readonly="" />
                                                                        <label for="form_control_1">Template Type</label>
                                                                        <span class="help-block">Type of your Template..</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>	
                                                                    <div class="form-group form-md-line-input">
                                                                        <input type="text" name="mail_from" class="form-control" id="form_control_1" value="<?php echo $row->mail_from; ?>" />
                                                                        <label for="form_control_1">Mail From</label>
                                                                        <span class="help-block">Change the mail address of the sender..</span>
                                                                    </div>
                                                                </td>
                                                                <td>	
                                                                    <div class="form-group form-md-line-input">
                                                                        <input type="text" name="mail_to" name="mail_from" class="form-control" id="form_control_1" value="<?php echo $row->mail_to; ?>">
                                                                        <label for="form_control_1">Mail To</label>
                                                                        <span class="help-block">Change the mail address of the reciever..</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-group form-md-line-input" id="cc_email" style="display:none;">
                                                                        <input type="text" name="add_cc" class="form-control" id="form_control_1" value="<?php echo $row->mail_cc; ?>" />
                                                                        <label for="form_control_1">Add CC</label>
                                                                        <span class="help-block">Change the mail address of the CC..</span>
                                                                    </div>
                                                                </td>
                                                                <td>

                                                                    <div class="form-group form-md-line-input pull-right">
                                                                        <a href="javascript:;" class="btn btn-sm grey-cascade" onclick="$('#cc_email').toggle()"> Add CC
                                                                            <i class="fa fa-plus"></i></a>
                                                                    </div>													
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-group form-md-line-input" >
                                                                        <input type="text" class="form-control" name="temp_subject" id="form_control_1" value="<?php echo $row->subject; ?>">
                                                                        <label for="form_control_1">Template Subject</label>
                                                                        <span class="help-block">Change the subject of the mail..</span>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="form-group form-md-line-input" >
                                                                        <input type="text" name="temp_name" class="form-control" id="form_control_1" value="<?php echo $row->form1; ?>" readonly="" />
                                                                        <label for="form_control_1">Assign This Template To Which Form</label>
                                                                        <span class="help-block">Assignation of the template..</span>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2">
                                                                    <div class="form-group form-md-line-input">
                                                                        <textarea name="mobpropertyInfo" class="comment_post form-control" rows="5" cols="70"><?php echo $row->body; ?> </textarea>
                                                                        <label for="form_control_1">Template Body</label>
                                                                        <script type="text/javascript">
                                                                            CKEDITOR.replace('mobpropertyInfo');
                                                                        </script>													
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2">
                                                                    <div class="form-group form-md-line-input">
                                                                        <textarea name="temp_signature" class="comment_post form-control" rows="5" cols="70"><?php echo $row->signature; ?>  </textarea>
                                                                        <label for="form_control_1">Template Signature</label>
                                                                        <script type="text/javascript">
                                                                            CKEDITOR.replace('temp_signature');
                                                                        </script>													
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div class="form-actions noborder pull-right">
                                                                        <input type="submit" value="Submit" name="update_template" class="btn blue" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </html>


    <?php
}
?>
<?php
admin_footer();
exit;
?>							
