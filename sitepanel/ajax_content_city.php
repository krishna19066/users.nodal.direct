<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$propertyCont = new propertyData();
$ajaxclientCont = new staticPageData();
$cityId = $_REQUEST['cityid'];
$cityListings1 = $propertyCont->getAllCityWithId($customerId, $cityId);
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
//print_r($cloud_keyData);
$bucket = $cloud_keyData->bucket;
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
$base_url = $cloud_keyData->website;
$res = $cityListings1[0];
$pageUrl = $res->cityUrl;
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];

$super_admin_1 = $propertyCont->GetSupperAdminData($admincustId);
$super_admin = $super_admin_1[0];
$template = $super_admin->template_selected;
?>
<!DOCTYPE html>
<html lang="en">
    <head>      
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header();
            ?>         
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit City Content
                                <small>Update City content here..</small>
                            </h1>
                        </div>                    
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property-settings.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">(Edit City Content)</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                                <a class="pull-right">
                                                    <a href="manage-property-settings.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;margin-left: 9px;"><i class="fa fa-eye"></i> &nbsp View All City List</button></a>                                               
                                                    <a href="ajax_add-roomtype.php"> <button style="background:#3683d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; "><i class="fa fa-plus"></i> &nbsp Add Room Type</button></a>
                                                    <a href="ajax_add-city.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; margin-right: 21px;"><i class="fa fa-plus"></i> &nbsp Add City</button></a>
                                                </a>
                                                <?php if ($pageUrl) { ?>
                                                    <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;margin-top: 19px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit - <?php echo $res->property_type; ?> - <?= $res->cityName; ?></span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Provide city content here!" data-original-title="Update city content" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div><hr/>
                                </div>
                                <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" name="city_id" value="<?= $res->cityID; ?>">
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_content_city" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>/>
                                                <?php } else { ?>
                                                    <input type="hidden" name="city_id" value="<?= $res->cityID; ?>">
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_content_city" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"<?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->

                                <div class="portlet-body form">
                                    
                                     <?php                              
                                switch ($template) {
                                    case "multiple1":                                       
                                        include 'templates/city/city-1.php';
                                        break;
                                    case "multiple2":                                      
                                          include 'templates/city/city-1.php';
                                        break;
                                    case "multiple3":                                      
                                            include 'templates/city/city-1.php';
                                        break;
                                     case "multiple4":                                      
                                           include 'templates/city/city-1.php';
                                        break;
                                    case "single1":                                      
                                             include 'templates/city/city-1.php';
                                        break;
                                    case "single2":                                      
                                             include 'templates/city/city-2.php';
                                        break;
                                    
                                    default:
                                       // echo "Your favorite color is neither red, blue, nor green!";
                                }
                                ?>
                                 
                                    <div class="portlet box yellow">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-gift"></i>City Listing Page Preview </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>                                      
                                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <iframe src="<?php echo $base_url; ?>/<?php echo $res->cityUrl; ?>.html" height="600" style="width: 100%;" ></iframe>                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <?php admin_footer(); ?>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
         <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    </body>
</html>