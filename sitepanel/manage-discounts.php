<?php
include "admin-function.php";

// include "cms_functions.php";
// user_locked();
// staff_restriction();
$customerId = $_SESSION['customerID'];
$discountCont = new discountOffers();
if ($_REQUEST['id'] == 'deactive') {
    $recordiD = $_REQUEST['recordID'];
    //$upd = "update tbl_discount set status='$stat' where discountID='$pro'";
    $upd = $discountCont->UpdateStatusDeactive($recordiD);
    echo '<script type="text/javascript">
                alert("Succesfuly Deactivate Discount");              
window.location = "manage-discounts.php";
            </script>';
    exit;
}
if ($_REQUEST['id'] == 'active') {
    echo $recordiD = $_REQUEST['recordID'];
    $recordiD = $_REQUEST['recordID'];
    //$upd = "update tbl_discount set status='$stat' where discountID='$pro'";
    $upd = $discountCont->UpdateStatusActive($recordiD);
    echo '<script type="text/javascript">
                alert("Succesfuly Activate Discount");              
window.location = "manage-discounts.php";
            </script>';

    exit;
}
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
                
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }
                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }
                tr:nth-child(odd){
                    background-color:#fff;
                }
                th {
                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                /* pagenation btn */
                        .paginate_button{
                            padding: 5px;
                        }
                </style>

                <!-- BEGIN THEME LAYOUT STYLES -->
                <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
                <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->
            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE DISCOUNTS & OFFERS
                                    <small>View and Edit Discounts and Offers</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Revenue Management</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Discounts</span>
                            </li>
                        </ul>
                        <div id="top_menu_container">
                            <ul id="top_menu">
                                <a href="manage-discounts.php" style="text-decoration:none;"><li class="OurMenuActive">Manage Discounts</li></a>
                                <li onclick="load_sub_city_detail();">Add / Manage Discounts</li>
                            </ul>
                        </div>
                        <div id="cityCont">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                                <a class="pull-right">
                                                    <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_discount();"><i class="fa fa-plus"></i> &nbsp Add Discount</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="background: white";>
                                
                                    <div style="padding-left:35px; color:rgba(0, 0, 0, 0.7);">
                                        
                                            <!-- This is the test file content the links for the footer only page Manage-enquiry.php -->
                                            <?php include 'test.php'; ?>
                                            <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
                                            <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
                                            <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
                                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                                            <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
                                            <script>
                                                $(document).ready(function () {
                                                    $('#example').DataTable();
                                                });
                                            </script>
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Property Details</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                $count = 1;
                                $discountList1 = $discountCont->getDiscounts($customerId);
                                foreach ($discountList1 as $discountData) {
                                    ?>
                                                         <tr style="    border-left: 10px solid <?php echo $color1; ?>;line-height:25px;">
                                                    <td style="width:50%;"><b><span style="color:<?php echo $color1; ?>;">&nbsp; <font color="black"> Name: </font><?php echo $discountData->propertyName; ?> </span></b><br>
                                                            &nbsp;<span><b>Discount Percentage: </b><?php echo $discountData->discountValue; ?></span><br>
                                                                 &nbsp;<span><b>Discount Name:</b> <?php echo ucwords($discountData->discountName); ?></span>	
                                                                </td>
                                                                <td style="padding-left:50px; text-align:right;">
                                                                    <?php
                                                                    $stat = $discountData->status;
                                                                    if ($stat == "Y") {
                                                                        $act_stat = "Active";
                                                                    } else if ($discountData->status == 'D') {
                                                                        $act_stat = "Deleted";
                                                                    } else {
                                                                        $act_stat = "Inactive";
                                                                    }
                                                                    //echo $dataRes[discountID];
                                                                    ?>
                                                                    <div class="btn-group">
                                                                        <?php
                                                                        if ($discountData->status == "Y") {
                                                                            ?>
                                                                            <div id="btn_div<?php echo $count; ?>">
                                                                                <a href="manage-discounts.php?id=deactive&recordID=<?php echo $discountData->discountID; ?>" class="btn btn-primary">
                                                                                    Deactivate 
                                                                                </a>
                                                                               <!-- <button type="button" name="<?php echo $count; ?>" value="manage-discounts.php?id=deactivate&recordID=<?php echo $discountData->discountID; ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;">Deactivate</button>-->
                                                                            </div>
                                                                            <?php
                                                                        } elseif ($discountData->status == 'N') {
                                                                            ?>
                                                                            <div id="btn_div<?php echo $count; ?>">
                                                                                <a href="manage-discounts.php?id=active&recordID=<?php echo $discountData->discountID; ?>" class="btn btn-primary" style="width:95px;background:#36c6d3;border:1px solid #36c6d3;">
                                                                                    Activate 
                                                                                </a>
                                                                               <!-- <button type="button" name="<?php echo $count; ?>" value="manage-discounts.php?id=activate&recordID=<?php echo $discountData->discountID; ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;background:#36c6d3;border:1px solid #36c6d3;">Activate</button>-->
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </div> &nbsp
                                                                    <a href="add-new-discount.php?id=edit&recordID=<?php echo $discountData->discountID; ?>" class="btn btn-s blue">
                                                                        <i class="fa fa-edit"></i> Edit 
                                                                    </a>
                                                                    <a onClick="deleteRecod('tbl_discount~discountID=<?php echo $discountData->discountID; ?>')" class="btn btn-s red"> <!-- onclick = function('TableName'~RecordID) -->
                                                                        <i class="fa fa-trash"></i> Delete
                                                                    </a></span>
                                                                </td>
                                                             
                                                                </tr>

                                                        <?php
                                                        $ctn++;
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Property Details</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                                                </div>
                                <br>
                                    <br>
                                                                    </div><br><br>
                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            </body>
                                                                            </html>
                                                                            <script>
                                                                                $('#top_menu li').click(function () {
                                                                                    $('#top_menu li').removeClass('OurMenuActive');
                                                                                    $(this).addClass('OurMenuActive');
                                                                                });
                                                                            </script>

                                                                            <script>
                                                                                function load_add_discount() {
                                                                                    var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                                    $('#cityCont').html(loadng);


                                                                                    //var replace = 'btn_div' + but;

                                                                                    $.ajax({url: 'ajax_lib/ajax_add_discount.php',
                                                                                        type: 'post',
                                                                                        success: function (output) {
                                                                                            // alert(output);
                                                                                            $('#cityCont').html(output);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            </script>
                                                                            <script>
                                                                                function load_property_type() {
                                                                                    var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                                    $('#cityCont').html(loadng);


                                                                                    //var replace = 'btn_div' + but;

                                                                                    $.ajax({url: 'ajax_lib/ajax_property_type.php',
                                                                                        type: 'post',
                                                                                        success: function (output) {
                                                                                            // alert(output);
                                                                                            $('#cityCont').html(output);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            </script>

                                                                            <script>
                                                                                function deleteRecod(delData) {
                                                                                    $.ajax({url: 'deleteData.php',
                                                                                        data: {deleteData: delData},
                                                                                        type: 'post',
                                                                                        success: function (output) {
                                                                                            // alert(output);
                                                                                            alert('Data Deleted Successfully');
                                                                                            location.reload();
                                                                                        }
                                                                                    });
                                                                                }
                                                                            </script>
                                                                            <!-- BEGIN PAGE LEVEL PLUGINS -->
                                                                            <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                                                                            <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                                                                            <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                                                                            <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                                                                            <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                                                                            <!-- END PAGE LEVEL PLUGINS -->
                                                                            <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                                                            <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
                                                                            <!-- END PAGE LEVEL SCRIPTS -->
                                                                            <?php
                                                                       //     admin_footer();
                                                                            ?>        
                                                                            </div>	