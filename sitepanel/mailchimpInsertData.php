<?php

include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
require_once('mailchimp/MC_OAuth2Client.php');
require_once('mailchimp/MC_RestClient.php');
require_once('mailchimp/miniMCAPI.class.php');
$client = new MC_OAuth2Client();
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$manageInquiry = new manageEnquiry();
$roomDataCount = new propertyData();
$getresult_1 = $inventoryCont->getMailChimpData($customerId);
$resCount = count($getresult_1);
//print_r($getresult_1);
$getresult_1 = $getresult_1[0];
$status = $getresult_1->status;
$apikey = $getresult_1->api_key;
$login_name = $getresult_1->login_name;
$list_id = $getresult_1->list_id;
$inquery_res = $manageInquiry->GetAllEnquiryWithCustomerId($customerId);
//print_r($inquery_res);
foreach ($inquery_res as $dt) {
    $name = $dt->name;
    $phone = $dt->mobile;
    $email = $dt->email;
    //$user_data[] = array('FNAME' => $name, 'PHONE' => $phone, 'Address' => $email);
}
//print_r($user_data);
//$fname = $_POST['fname'];
//$lname = $_POST['lname'];
//$email = $_POST['email'];
if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
    // MailChimp API credentials
    $apiKey = $apikey;
    $listID = $list_id;

    // MailChimp API URL
    $memberID = md5(strtolower($email));
    $dataCenter = substr($apiKey, strpos($apiKey, '-') + 1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

    // member information
    $json = json_encode([
        'email_address' => $email,
        'status' => 'subscribed',
        'merge_fields' => [
            'FNAME' => $name,
            'PHONE' => $phone
          
        ]
    ]);
    // send a HTTP POST request with curl
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    // store the status message based on response code
    if ($httpCode == 200) {
        echo '<p style="color: #34A853">You have successfully subscribed to CodexWorld.</p>';
    } else {
        switch ($httpCode) {
            case 214:
                $msg = 'You are already subscribed.';
                break;
            default:
                $msg = 'Some problem occurred, please try again.';
                break;
        }
        echo '<p style="color: #EA4335">' . $msg . '</p>';
    }
} else {
    echo '<p style="color: #EA4335">Please enter valid email address.</p>';
}

// redirect to homepage
header('location:MailChimp.php');
