<?php
include "admin-function.php";
checkUserLogin();
$propertyId = base64_decode($_REQUEST['propID']);

require_once 'GMB/google-api-php-client/src/Google/autoload.php'; // or wherever autoload.php is located
include 'GMB/google-api-php-client/src/Google/MyBusiness.php'; // or wherever autoload.php is located
//Declare your Google Client ID, Google Client secret and Google redirect uri in  php variables
$google_client_id = '718435963727-e5ln6214dsn6ki7fq9j1ho1bug39aras.apps.googleusercontent.com';
$google_client_secret = '4l-MyMKLPtyAkBDDeUlCdaNq';
$google_redirect_uri = 'http://www.users.nodal.direct/sitepanel/google-my-business.php'; // for live: http://www.users.nodal.direct/sitepanel/google-my-business.php

//setup new google client
$client = new Google_Client();
//$client = new Google_Service_MyBusiness();
$client->setApplicationName('My application name');
$client->setClientid($google_client_id);
$client->setClientSecret($google_client_secret);
$client->setRedirectUri($google_redirect_uri);
$client->setAccessType('online');
$client->setScopes('https://www.googleapis.com/auth/plus.business.manage');
$googleImportUrl = $client->createAuthUrl();
$mybusinessService = new Google_Service_MyBusiness($client);
$reviews = $mybusinessService->accounts;
//print_r($reviews);

$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$roomDataCount = new propertyData();
$getresult_1 = $inventoryCont->getGoogleMyBusinessData($customerId);
$property_data = $roomDataCount->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
$resCount = count($getresult_1);
//print_r($getbookingEngine_1);
$getresult = $getresult_1[0];
$status = $getresult->status;
$get_review = $inventoryCont->getGoogleActiveReview($customerId, $propertyId);
if (isset($_GET['type'])) {
    $type = $_REQUEST['type'];
    $account_id = $_REQUEST['account_id'];
    if ($type == 'disconnect') {
        $sta = 'N';
    } else {
        // $sta = 'N';
    }
    $upd = $inventoryCont->UpdateGoogleMyBusinessStatus($sta, $customerId, $account_id);
    header('location:manage-3rd-api.php');
}

if (isset($_POST['google_review_status'])) {

    $status = $_REQUEST['status'];
    $propoertyName = $_REQUEST['propertyName'];
    $propertyID = $_REQUEST['propID'];
    $propertyID_1 = base64_encode($propertyID);
    if ($status == 'activate') {
        echo $sta = 'Y';
    } else {
        $sta = 'N';
    }

    $upd = $inventoryCont->UpdateGoogleReviewStatus($sta, $customerId, $propoertyName);
    $upd2 = $inventoryCont->UpdateGoogleReview($sta, $customerId, $propoertyName, $propertyID);
    header('location:google-my-business.php?propID=' . $propertyID_1);
}
if (isset($_GET['status'])) {

    $status = $_REQUEST['status'];
    $propoertyName = $_REQUEST['propertyName'];
    $propertyID = $_REQUEST['propID'];
    $propertyID_1 = base64_encode($propertyID);
    if ($status == 'activate') {
        $sta = 'Y';
    } else {
        $sta = 'N';
    }

    $upd = $inventoryCont->UpdateGoogleReviewStatus($sta, $customerId, $propoertyName);
    $upd2 = $inventoryCont->UpdateGoogleReview($sta, $customerId, $propoertyName, $propertyID);
    header('location:google-my-business.php?propID=' . $propertyID_1);
}
?>

<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>

<?php
session_start();
?>
<html>
    <head>
        <style>
            .card{
                margin:50px;
                float:left;
                position:relative;             
                color:white;
                font-weight:bold;
            }

            .card input[type="radio"]
            {
                margin-top:-35px;
                margin-bottom:10px;
            }

            .card i{			
                font-size:55px;
                z-index:2;
                margin-top: 50%;
            }
            .overly{
                display:none;
                text-align:center;
                top:0;
                width:100%;
                height:300px;
                position:absolute;
                background:rgba(0, 0, 0, 0.5);
                color:#fff;			
            }

            .dn{
                display:none;
            }
            .db{
                display:block;
            }
            .card > :nth-child(3){
                display:block;
            }
        </style>
        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Connect To Google My Business</span>
                                                <span class="caption-helper"><?php echo $propertyName; ?></span>
                                            </div>
                                        </div>
                                        <!--<span style="float:left;"> <a href="booking-engine.php"><button style="background:#FF9800;color:white;border:none;height:35px;width:160px;font-size:14px; margin-top:35px;"><i class="fa fa-plus"></i> &nbsp Booking Engine</button></a></span><br><br>-->
                                       <!-- <span style="float:right;"> <a href="manage_rates.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Rates</button></a></span><br><br>-->
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="form-body">
                                            <div class="body">
                                                <div class="property portlet light bordered">
                                                    <?php

//curl function
                                                    function curl($url, $post = "") {
                                                        $curl = curl_init();
                                                        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
                                                        curl_setopt($curl, CURLOPT_URL, $url);
                                                        //The URL to fetch. This can also be set when initializing a session with curl_init().
                                                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                                                        //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
                                                        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
                                                        //The number of seconds to wait while trying to connect.
                                                        if ($post != "") {
                                                            curl_setopt($curl, CURLOPT_POST, 5);
                                                            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
                                                        }
                                                        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
                                                        //The contents of the "User-Agent: " header to be used in a HTTP request.
                                                        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
                                                        //To follow any "Location: " header that the server sends as part of the HTTP header.
                                                        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);
                                                        //To automatically set the Referer: field in requests where it follows a Location: redirect.
                                                        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
                                                        //The maximum number of seconds to allow cURL functions to execute.
                                                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                                                        //To stop cURL from verifying the peer's certificate.
                                                        $contents = curl_exec($curl);
                                                        curl_close($curl);
                                                        return $contents;
                                                    }

//google response with contact. We set a session and redirect back
                                                    if (isset($_GET['code'])) {
                                                        $auth_code = $_GET["code"];
                                                        $_SESSION['google_code'] = $auth_code;
                                                    }
                                                    /*
                                                      Check if we have session with our token code and retrieve all contacts, by sending an authorized GET request to the following URL : https://www.google.com/m8/feeds/contacts/default/full
                                                      Upon success, the server responds with a HTTP 200 OK status code and the requested contacts feed. For more informations about parameters check Google API contacts documentation
                                                     */
                                                    if (isset($_SESSION['google_code'])) {
                                                        $auth_code = $_SESSION['google_code'];
                                                        $max_results = 200;
                                                        $fields = array(
                                                            'code' => urlencode($auth_code),
                                                            'client_id' => urlencode($google_client_id),
                                                            'client_secret' => urlencode($google_client_secret),
                                                            'redirect_uri' => urlencode($google_redirect_uri),
                                                            'grant_type' => urlencode('authorization_code')
                                                        );
                                                        //print_r($fields);
                                                        $post = '';
                                                        foreach ($fields as $key => $value) {
                                                            $post .= $key . '=' . $value . '&';
                                                        }
                                                        $post = rtrim($post, '&');


                                                        $result = curl('https://accounts.google.com/o/oauth2/token', $post);
                                                        $response = json_decode($result);
                                                        // print_r($response);
                                                        // die;
                                                        $accesstoken = $response->access_token;
                                                        //$url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results='.$max_results.'&alt=json&v=3.0&oauth_token='.$accesstoken;
                                                        $url = 'https://mybusiness.googleapis.com/v4/accounts?oauth_token=' . $accesstoken;
                                                        $xmlresponse = curl($url);
                                                        //print_r($xmlresponse);
                                                        $contacts = json_decode($xmlresponse, true);

                                                        $res = $contacts['accounts'];
                                                        //print_r($res);
                                                        $account_id = $res[0]['name'];
                                                        $accountName = $res[0]['accountName'];
                                                        $type = $res[0]['type'];
                                                        $url9 = 'https://mybusiness.googleapis.com/v4/' . $account_id . '/locations?oauth_token=' . $accesstoken; // Get Location & Acoount list
                                                        //$url9 = 'https://mybusiness.googleapis.com/v4/accounts/106210626692903564522/locations/17375209541208459910/reviews?oauth_token='.$accesstoken; // Get Review List
                                                        $xmlresponse9 = curl($url9);
                                                        // print_r($xmlresponse9);

                                                        $all_location = json_decode($xmlresponse9, true);
                                                        $res_data = $all_location['locations'];
                                                        // print_r($res_data);
                                                        $total_property = $all_location['totalSize'];
                                                        foreach ($res_data as $dt) {
                                                            $location_id = $dt['name'];
                                                            $locationName = $dt['locationName'];
                                                            $primaryPhone = $dt['primaryPhone'];
                                                            $ins = $inventoryCont->InsertGoogleMyBusiness($customerId, $account_id, $accountName, $location_id, $locationName, $primaryPhone, $total_property, $accesstoken);

                                                            $url9 = 'https://mybusiness.googleapis.com/v4/' . $location_id . '/reviews?oauth_token=' . $accesstoken; // Get Review List
                                                            $xmlresponse_review = curl($url9);
                                                            //print_r($xmlresponse_review);

                                                            $all_review = json_decode($xmlresponse_review, true);

                                                            $avg_ratting = $all_review['averageRating'];
                                                            $totalReviewCount = $all_review['totalReviewCount'];
                                                            $review = $all_review['reviews'];
                                                            //print_r($review);
                                                            foreach ($review as $r) {
                                                                $starRating = $r['starRating'];
                                                                $review_1 = $r['comment'];
                                                                $review = str_replace("'", "\'", $review_1);
                                                                $createTime = $r['createTime'];
                                                                $updateTime = $r['updateTime'];
                                                                $reviewerName = $r['reviewer']['displayName'];
                                                                $profilePhotoUrl = $r['reviewer']['profilePhotoUrl'];

                                                                $ins = $inventoryCont->InsertGoogleMyBusinessReview($customerId, $account_id, $locationName, $reviewerName, $starRating, $review, $createTime, $updateTime, $profilePhotoUrl, $avg_ratting, $totalReviewCount);
                                                                //$location = $all_location['locations'][0];
                                                            }
                                                        }
                                                        echo '<h3>Successfully Connected Google My Business:<span style="color: #009688;"> <a href="manage-3rd-api.php">Back</a></span></h3>';
                                                    }
                                                    ?>

                                                    <!-- BEGIN Portlet PORTLET-->
                                                    <div class="portlet light">
                                                        <div class="portlet-title">
                                                            <div class="caption font-green-sharp">
                                                                <i class="icon-speech font-red-sunglo"></i>
                                                                <span class="caption-subject bold uppercase">Google My Business</span>
                                                                <span class="caption-helper"></span>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <!-- END Portlet PORTLET-->
                                                    <?php
                                                    $resCount = count($getresult_1);
                                                    if ($resCount > 0) {
                                                        ?>
                                                        <?php if ($getresult->review_status != 'Y') { ?>
                                                            <div class="property portlet light bordered" style="height: 195px;">  

                                                                <form action="" method="post">
                                                                    <div class="form-row">
                                                                        <div class="form-group col-md-3">
                                                                            <label for="inputState">Select property</label>
                                                                            <select name="propertyName" id="property" class="form-control" required="">
                                                                                <option value="">--- Select property ---</option>

                                                                                <?php
                                                                                foreach ($getresult_1 as $propList) {
                                                                                    ?>
                                                                                    <option value="<?php echo $propList->location_name; ?>"><?php echo $propList->location_name; ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>     
                                                                        <?php if ($getresult->review_status == 'Y') { ?>
                                                                            <input type="hidden" name="status" value="deactivate" />
                                                                            <div class="form-group col-md-6">
                                                                                <label for="inputEmail4"></label>
                                                                                <input type="hidden" name="propID" value="<?php echo $propertyId; ?>" />
                                                                                <input type="submit" name="google_review_status" value="Deactive Google Review" class=" btn btn-danger" style=" margin-top: 25px;" />
                                                                            </div>

                                                                        <?php } else { ?>
                                                                            <input type="hidden" name="status" value="activate"/>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="inputEmail4"></label>
                                                                                <input type="hidden" name="propID" value="<?php echo $propertyId; ?>" />
                                                                                <input type="submit" name="google_review_status" value="Active Google Review" class=" btn btn-success" style=" margin-top: 25px;" />
                                                                            </div>

                                                                        <?php } ?>

                                                                    </div>
                                                                </form>

                                                            </div>
                                                            <p>*NOTE: Please select property those you want to display google review on your website.</p>
                                                        <?php } ?>
                                                        <?php if (count($get_review) > 0) { ?>
                                                            <table class="table">

                                                                <thead>
                                                                    <tr>
                                                                        <th style="width:30%">Property Name</th>
                                                                        <th style="width:10%">Total Review</th>
                                                                        <th style="width:10%">Average Ratting</th>
                                                                        <th style="width:15%"><span style="color:gray;">Status</span></th>
                                                                        <th style="width:15%"><span style="color:gray;">Action</span></th>

                                                                    </tr>
                                                                </thead>
                                                                <?php
                                                                foreach ($get_review as $dt) {
                                                                    
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <th style="width:30%"><?php echo $dt->PropertyName; ?></th>
                                                                    <th style="width:10%"><span style="color:green;"><?php echo $dt->totalReviewCount; ?></span></th> 
                                                                    <th style="width:10%"><span style="color:Red;"><?php echo number_format($dt->avg_ratting, 2); ?></span></th> 
                                                                    <th style="width:10%"><span style="color:gray;"><?php echo $dt->status; ?></span></th>
                                                                    <th style="width:15%"> <?php if ($getresult->review_status == 'Y') { ?>
                                                                            <a href="google-my-business.php?status=deactivate&propID=<?php echo $propertyId; ?>&propertyName=<?php echo $getresult->location_name; ?>" onClick="return confirm('Are you sure you want to deactivate')" class="btn btn-danger" style="background:#d3365a;color:white;border:none;height:35px;width:160px;font-size:14px;">
                                                                                <i class="fa fa-connectdevelop"></i> Review Deactivate </a>
                                                                        <?php } else { ?>
                                                                            <a href="google-my-business.php?status=activate&propID=<?php echo $propertyId; ?>&propertyName=<?php echo $getresult->location_name; ?>" onClick="return confirm('Are you sure you want to active review')" class="btn btn-success">
                                                                                <i class="fa fa-connectdevelop"></i> Review Activate </a>
                                                                        <?php } ?></th>

                                                                </tr>


                                                            </table>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#property').on('change', function () {
            var propertyID = $(this).val();
            if (propertyID) {
                $.ajax({
                    type: 'POST',
                    url: 'property_room.php',
                    data: 'property_id=' + propertyID,
                    success: function (html) {
                        $('#room').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#room').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>
<script language="JavaScript">
    $('#select-all').click(function (event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';                      ?>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>