<?php
include "admin-function.php";
//$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$customerId = $_SESSION['customerID'];
$analyticsCont = new analyticsPage();
?>
<?php
//$pageUrl = $_POST['pageUrlData'];
$analyticsData = $analyticsCont->getAnalyticCodeData($customerId);
//print_r($analyticsData);
$res1num = count($analyticsData);
foreach ($analyticsData as $dt) {
     $res_code = $dt->analyticcode;
 	$res_code2 = $dt->analyticcode2;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Analytic code
                                <small>Add a new Analytic code here..</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property-settings.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">(Analytic code)</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                               <!-- <a class="pull-right">
                                                    <a href="manage-analytic-settings.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:190px;font-size:14px;"><i class="fa fa-eye"></i> &nbsp View All Meta tag List</button></a>                                               
                                                    <a href="ajax_add_meta_tag.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -12px;"><i class="fa fa-plus"></i> &nbsp Add Meta Tags</button></a>
                                                     <a href="ajax_add_analytic_code.php"><button style="background:#ff9800;color:white;border:none;height:35px;width:190px;font-size:14px; float: right;margin-top: -12px; margin-right: 21px;"><i class="fa fa-plus"></i> &nbsp Add Google Analytics</button></a>
                                                </a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                    <div class="form-body">
                                      							
					<div class="form-group">
                                                <label class="control-label col-md-3">Analytic Code
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="analytic_code" value="<?php echo $res_code2;  ?>" placeholder=""/>
                                                    <span class="help-block"> Provide Analytic Code.</span>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-3">javaScript code for the Google analytic.
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <textarea class="form-control" name="javascript_code"><?php echo $res_code;  ?></textarea>
                                                    <span class="help-block"> Provide Analytic Code.</span>
                                                </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if ($res1num == "0") {
                                                        ?>
                                                        <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Add Analytic code" />
                                                        <input name="action_register" type="hidden" value="submit_analytic_code" />
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update Analytic code" />
                                                        <input name="action_register" type="hidden" value="update_analytic_code" />
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->


            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>