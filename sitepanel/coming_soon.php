<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$getHomeSlider = $propertyCont->GetHomepageSliderData($customerId);

$reccnt = count($getHomeSlider);
?>
<?php
$redirect_script_name = "manage-home-page-slider.php";

$pageTitle = 'Home Page - Desktop Slider';
/* -------------------------------- CustomerID Updation ------------------------------------------- */

$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);

$cloud_keyData = $cloud_keySel[0];
//print_r($cloud_keyData);
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;

require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';


Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));




//admin_header();
?>


<html>
    <head>
        <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>                          
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Coming Soon....</span>
                                            <span class="caption-helper">Coming Soon.....</span>
                                          
                                        </div>
                                    </div><br>
                                    <span style="float:right;"> <a href="welcome.php"><button style="background:#1ba39c;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-top: -39px;"> Go to Dashboard </button></a></span>
                                </div>
                                <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                                <div class="portlet light bordered">	
                                    <div class="portlet-body form">
                                        <span>Coming Soon this Feature</span>
                                    </div>
                                </div>
                            </div>
                            <a href="welcome.php"><img src="https://preview.ibb.co/f0WjaU/coming_soon.png" alt="coming_soon" border="0"></a><br /><br />

                            <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                        </div>                    
                        <br><br><br>		
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->
</body>
</html>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>

<?php
admin_footer();
?>