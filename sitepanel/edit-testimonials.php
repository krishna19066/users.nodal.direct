<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$ajaxclientCont = new staticPageData();
$recordID = $_REQUEST[recordID];
//$roomID = $_REQUEST[roomID];
$id = $_REQUEST['id'];
//$conect_cloud = $cloud_data->connectCloudinaryAccount($customerId);
$get_testimonial = $propertyCont->GetTestimonialDataWithId($recordID);
//$get_room = $propertyCont->getRoomDetailData($recordID, $roomID);
//$superAdmin_data = $propertyCont->GetSupperAdminData($customerId);
//$superAdmin_res = $superAdmin_data[0];
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';
Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));
?>
<?php
$redirect_script_name = "view-property-rooms.php?recordID=''";
@extract($_REQUEST);

/* -------------------------------- CustomerID Updation ------------------------------------------- */
if ($id == "delete") {   
    //  updateTable($table_name, " status='D' where roomID='$recordID' ");
     $upd_status = $propertyCont->DeleteTestimonialData($recordID);
   // $_SESSION[session_message] = $record_type . ' has been Delelted successfully.';
    echo '<script type="text/javascript">
                alert("Succesfuly Delelted ");                
window.location = "manage-testimonial.php";
            </script>';
}


if ($id = 'edit' && $recordID != '') {

    $res_testi = $get_testimonial[0];
}
if (isset($_POST['update_testimonial'])) {

    if ($_FILES['image_org']['name'] != '') {
        $file = $_FILES['image_org']['name'];
        $timdat = date('Y-m-d');
        $timtim = date('H-i-s');
        $timestmp = $timdat . "_" . $timtim;
        $upload = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/testimonials"));
        $cond = ",photo='$timestmp'";
    }
    $review = $_POST['review'];
    $occupation = $_POST['occupation'];
    $name = $_POST['name'];
    $update_testi = $propertyCont->UpdateTestimonialData($recordID, $cond);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated ");                
window.location = "edit-testimonials.php?id=edit&recordID=' . $recordID . '";
            </script>';
}
//admin_header();	
?>

<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="assets/dist/sweetalert.js"></script>
        <link rel="stylesheet" href="assets/dist/sweetalert.css">
        <?php
        adminCss();
        ?>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <br><br><br>
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Testimonial</span>
                                            <span class="caption-helper">Please Edit Testimonial</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <span style="float:right;"> <a href="manage-testimonial.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Add Testimonial</button></a></span><br><br>
                                </div>
                                <div class="portlet-body form" style="background: white;">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                        <div class="form-body">
                                            <?php
                                            if ($id = 'edit' && $recordID != '') {
                                                ?>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Review</label>
                                                    <span class="required"> * </span>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" name="review" rows="6" required> <?= $res_testi->review; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Reviewer Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="name" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide Reviewer Name</span>
                                                    </div>
                                                </div>

                                                <!--<div class="form-group">
                                                    <label class="control-label col-md-3"> Reviewer Brief / Occupation

                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="occupation" type="text" class="form-control"  value="<?php echo html_entity_decode($res_testi->detail); ?>"  />
                                                        <span class="help-block"> Provide Reviewer Brief / Occupation</span>
                                                    </div>
                                                </div>-->


                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Reviewer Image

                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if ($res_testi->photo == '') { ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                        <?php } else { ?>
                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_150,c_fill/reputize/testimonials/<?php echo $res_testi->photo; ?>.jpg" alt="" /> 
                                                        <?php } ?>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image_org"> 
                                                        </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a><br>
                                                       <!-- <b>Selected Photo :</b> <a href="http://res.cloudinary.com/<?php //echo $cloud_cdnName;       ?>/image/upload/w_160,h_160,c_fill/reputize/testimonials/<?php //echo $res_testi->photo;       ?>.jpg" target="_blank"><?php // echo $res_testi->photo;       ?></a>-->

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Testimonial Category</label>
                                                <div class="col-md-9">
                                                    <div class="mt-checkbox-inline">
                                                        <?php
                                                        //$roomTypeSQL = "select * from roomAmenties";
                                                        //$roomTypequery = db_query($roomTypeSQL);
                                                        $testCatData = $ajaxclientCont->getTestimonialCategoryList($customerId);
                                                        $counter = 0;
                                                        $CategoryName = explode(',', $res_testi->category_name);

                                                        foreach ($testCatData as $catTypeRes) {
                                                            if ($counter == 0) {
                                                                ?>

                                                                <?php
                                                            }
                                                            $chk = "";
                                                            if (in_array($catTypeRes->category_name, $CategoryName)) {
                                                                $chk = 'checked="checked"';
                                                            }
                                                            ?>

                                                            <label class="mt-checkbox">
                                                                <input type="radio" id="inlineCheckbox21" name="testimonial_cate" value="<?= $catTypeRes->category_name; ?>"  <?= $chk; ?>/><?= $catTypeRes->category_name; ?>
                                                                <span></span>
                                                            </label>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">

                                                        <input name="action_register" type="hidden" value="save" />
                                                        <input type="submit" class="btn green" name="update_testimonial" class="button" id="submit" onclick="SweetAlert();" value="Update" />

                                                        <a href="javascript:history.back()" class="btn red">Go Back</a>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                </div>

                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>    
</body>
</html>
<script>
    function SweetAlert() {
         swal("Successfuly Update!");
         return false;
    }
</script>
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<?php
admin_footer();
?>