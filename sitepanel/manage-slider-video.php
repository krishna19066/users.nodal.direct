<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$getHomeSlider = $propertyCont->GetHomepageSliderVideo($customerId);
$slider_video = $getHomeSlider[0];
$videoSlider = $slider_video->videoSlider;

//$reccnt = count($getHomeSlider);
?>
<?php
$redirect_script_name = "manage-home-page-slider.php";

$pageTitle = 'Home Page - Desktop Slider';
/* -------------------------------- CustomerID Updation ------------------------------------------- */

$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);

$cloud_keyData = $cloud_keySel[0];
//print_r($cloud_keyData);
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';

Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));

if ($_POST['action_register'] == "save") {

    @extract($_POST);
    $name = $_FILES[image1][name];     

    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;
    $upd = $propertyCont->AddHomePageVideoSlider($customerId, $timestmp);
	
    if ($customerId == 1) {
        // \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "homepage-slider"));
    } else {
		
      //$cloudUpload=  \Cloudinary\Uploader::upload($name, array("public_id" => $timestmp, "folder" => "reputize/homepage-slider"));
        // \Cloudinary\Uploader::upload($_FILES["image1"]["name"], array("resource_type" => "video","folder" => "reputize/homepage-slider","public_id" => $timestmp,));
        \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("folder" => "reputize/homepage-slider", "public_id" => $timestmp, "resource_type" => "video"));   
    }

     echo '<script type="text/javascript">
      alert("Succesfuly added Video Slider");
      window.location = "manage-slider-video.php";
      </script>';

      exit;
     
}
if ($_REQUEST['id'] == 'edit' && $_REQUEST['recordID'] != '') {
    $recordID = $_REQUEST['recordID'];
    $homeSlider = $propertyCont->HomeSliderWithID($recordID);
    $res = $homeSlider[0];
}
if ($_REQUEST['action_register'] == "update") {
    echo $name = $_FILES[image1][name];
    die;
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;
    $img_name = $timestmp;

    \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/homepage-slider"));
    \Cloudinary\Uploader::upload_large("my_large_video.mp4", array("resource_type" => "video", "chunk_size" => 6000000));

    // $ins = "update HomeSlider set hsimg='$timestmp',hshead1='$hshead1',hshead2='$hshead2',linktext='$linktext',linkURL='$linkURL',type='$type' where slno='$_REQUEST[slno]'";
    // $quer = mysql_query($ins);
    $upd = $propertyCont->UpdateHomeSlider($img_name);

    echo '<script type="text/javascript">
                alert("Succesfuly update image");              
window.location = "manage-slider.php";
            </script>';

    exit;
}
if ($_REQUEST['id'] == 'delete' && $_REQUEST['imagesID1'] != '') {

    $imagesID1 = $_REQUEST['imagesID1'];
    $det = $propertyCont->DeleteHomeSlider($imagesID1);
}
if ($type == 'home' && $id == 'edit') {
    $pageTitle = "Update Image in Home Page Slider";
} elseif ($type == 'home' && $id == '') {
    $pageTitle = "Add New Image in Home Page Slider";
}
//admin_header();
?>


<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Manage Home Page Slider</span>
                                            <span class="caption-helper">Manage your home page slider Video</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div><br>
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Slider Video" /></span><br><br>
                                    <span style="float:right;"> <a href="manage-slider.php"><button style="background:#1ba39c;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-top: -39px;">Add Slider Image </button></a></span>
                                </div>
                                <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                                <div class="portlet light bordered" id="add_photo" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <input type="hidden" value="<?php echo $lan; ?>" name="language">

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Slider Video
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select Video </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image1"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">

                                                            <?php
                                                            if ($id = 'edit' && $recordID != '') {
                                                                ?>
                                                                <input name="slno" type="hidden" value="<?= $recordID; ?>" />
                                                                <input name="type" type="hidden" value="<?= $res->type; ?>" />
                                                                <input name="action_register" type="hidden" value="update" />
                                                                <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update Video" />
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input name="action_register" type="hidden" value="save" />
                                                                <input name="type" type="hidden" value="home" />
                                                                <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Add Video" />
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>

                            <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->


                        </div>

                        <?php
//  $qry1 = "select * from $table_name where lan_code='$lan' and customerID='$custID'";
// $reccnt = NumRows(db_query($qry1));
// $qry = db_query($qry1);
//print_message(); 
                        ?>

                        <br><br><br>		
                        <div style="margin-top:200px;">
                            <?php
                            if ($videoSlider != NULL) {
                                ?>
                                <div class="col-md-3 " style="width:385px;">
                                    <!-- BEGIN Portlet PORTLET-->
                                    <div class="portlet box" style="background:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>;">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-image"></i><a href="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/video/upload/v1528110270/reputize/homepage-slider/<?php echo $videoSlider; ?>.webm" target="_blank" style="color:#fff;">View Video</a> 
                                            </div>

                                        </div>
                                        <div class="portlet-body">

             <!--<img class="img-responsive" src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/video/upload/w_323,h_150,c_fill/reputize/homepage-slider/<?php echo $videoSlider; ?>.mp4" width="100%" style="height: 150px;">-->
                                            <video controls='0' loop='1' poster='https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/video/upload/v1528110270/reputize/homepage-slider/<?php echo $videoSlider; ?>.webm'>
                                                <source src='https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/video/upload/v1528110270/reputize/homepage-slider/<?php echo $videoSlider; ?>.webm' type='video/webm'>
                                                <source src='https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/video/upload/v1528110270/reputize/homepage-slider/<?php echo $videoSlider; ?>.mp4' type='video/mp4'>
                                                <source src='https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/video/upload/v1528110270/reputize/homepage-slider/<?php echo $videoSlider; ?>.ogv' type='video/ogg'>                                        
                                            </video>

                                        </div>
                                    </div>
                                </div>

    <?php
} else {
    ?>
                                <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                    <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                </table>
    <?php
}
?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->
</body>
</html>
<script>
    function load_SliderVideo() {
        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
        $('#add_prop').html(loadng);

        $.ajax({url: 'ajax_lib/ajax_video_slider.php',
            //data: {propertyId: propertyId},
            type: 'post',
            success: function (output) {
                // alert(output);
                $('#add_prop').html(output);
            }
        });
    }
</script>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>

<?php
admin_footer();
?>