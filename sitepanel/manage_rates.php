<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$roomDataCount = new propertyData();
$getbookingEngine_1 = $inventoryCont->getbookingEngineData($customerId);
//print_r($getbookingEngine_1);
$getbookingEngine = $getbookingEngine_1[0];
$res_booking_eng = $getbookingEngine->status;
$booking_engine = $getbookingEngine->booking_engine;
?>

<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>

<?php
//$con = mysqli_connect("localhost","root","","perch") or die("no connection");
session_start();
?>
<html>

    <head>
        <style>
            .header
            {
                width:100%;
                height:45px;
                background:#1ABC9C;
                padding-top:17px;
                color:white;
                font-size:25px;
                font-weight:bold;
                font-family:Arial;
            }
            .body
            {
                width:100%;
            }
            .property
            {
                width:100%;
            }
            .prop_select
            {
                width:400px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                padding-left:10px;
            }
            .prop_submit
            {
                height:40px;
                width:110px;
                background:#05B8CC;
                color:white;
                border:none;
                border-radius:3px;
                font-weight:bold;
            }
            .rate
            {
                width:100%;
            }
            .date_range
            {
                width:100%;
                border:1px solid #7FA6DD;
                padding-top:20px;
                padding-bottom:20px;
                box-shadow:1px 1px 1px 1px #cbdbf1;
                background:#f2f6fb;
            }
            .date_table
            {
                width:100%;
            }
            .date_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .date_submit
            {
                height:40px;
                width:140px;
                background:#05B8CC;
                color:white;
                border:none;
                border-radius:3px;
                font-weight:bold;
            }
            .room_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .rates
            {
                width:auto;
            }
            .calendar
            {
                width:98%;
                border:1px solid #227298;

                padding-right:10px;
            }
            .calendar_table
            {
                width:100%;

                border-collapse:collapse;
                background:#227298;
                color:white;
                height:40px;
            }
            .calendar_data
            {
                width:auto;

                border-collapse:collapse;
                background:white;
                color:black;
                height:40px;
            }

            .input_css {
                background: #E26A6A;
                border: 0px;
                width: 99%;
                height: 100%;
                font-size: 13px;
                text-align: center;
            }
            .rate_modify1
            {
                width:90%;
                padding:20px;
                border:1px solid #7FA6DD;
                box-shadow:1px 1px 1px 1px #cbdbf1;
                background:#f2f6fb;
            }
            .room_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }

        </style>

        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">

                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Manage Property Rates</span>
                                                <span class="caption-helper">Manage your property room rates</span>
                                            </div>
                                        </div>
                                         
                                        <span style="float:right;"> <a href="manage_inventory.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Inventory</button></a></span><br><br>

                                        <div class="portlet-body form">
                                            <div class="form-body">
                                                <div class="body">
                                                    <div class="property portlet light bordered" style="height: 195px;">
                                                        <?php if ($res_booking_eng != 'Y') { ?>
                                                            <form action="" method="post">
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-3">
                                                                        <label for="inputState">Select property</label>
                                                                        <select name="propertyID" id="property" class="form-control" required="">
                                                                            <option value="">--- Select property ---</option>
                                                                            <?php
                                                                            $propList1 = $inventoryCont->getPropertyListing($customerId,$user_id);
                                                                            foreach ($propList1 as $propList) {
                                                                                ?>
                                                                                <option value="<?php echo $propList->propertyID; ?>"><?php echo $propList->propertyName; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group col-md-3">
                                                                        <label for="inputState">Select property Room</label>
                                                                        <select name="room_id" id="room" class="form-control" required="">
                                                                            <option value="">Select Property first</option>                                                                   
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group col-md-3">
                                                                        <label for="inputEmail4">From Date</label>
                                                                        <input type="date" name="from_date" class="form-control" id="inputEmail4" required="">
                                                                    </div>
                                                                    <div class="form-group col-md-3">
                                                                        <label for="inputPassword4">To Date</label>
                                                                        <input type="Date" name="to_date" class="form-control" id="inputPassword4" required="" >
                                                                    </div><br/><br/>
                                                                    <div class="form-group col-md-3">
                                                                        <label for="inputEmail4">Price</label>
                                                                        <input type="number" name="price" class="form-control" id="inputEmail4" placeholder="Price" required="">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="inputEmail4"></label>
                                                                        <input type="submit" name="updateRate" value="Update Rate" class=" btn btn-primary" style=" margin-top: 25px;" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?> />
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        <?php } else { ?>
                                                            <h3>You are connected <span style="color: #009688;"><?php echo $booking_engine; ?></span> </h3>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <?php
                                        if (isset($_POST['updateRate'])) {
                                            $from_date = $_POST['from_date'];
                                            $to_date = $_POST['to_date'];
                                            $property_id = $_POST['propertyID'];
                                            $price = $_POST['price'];
                                            $room_id = $_POST['room_id'];

                                            $date_from = $from_date;
                                            $date_from = strtotime($date_from); // Convert date to a UNIX timestamp  
// Specify the end date. This date can be any English textual format  
                                            $date_to = $to_date;
                                            $date_to = strtotime($date_to); // Convert date to a UNIX timestamp  
// Loop from the start date to end date and output all dates inbetween  
                                            for ($i = $date_from; $i <= $date_to; $i+=86400) {

                                                //echo date("Y-m-d", $i) . '<br />';
                                                $fr_yr = date("Y", $i);
                                                $fr_month = date("m", $i);
                                                $jsonDate = date("d", $i);

                                                if ($jsonDate == '01') {
                                                    $jsonDate = '1';
                                                }
                                                if ($jsonDate == '02') {
                                                    $jsonDate = '2';
                                                }
                                                if ($jsonDate == '03') {
                                                    $jsonDate = '3';
                                                }
                                                if ($jsonDate == '04') {
                                                    $jsonDate = '4';
                                                }
                                                if ($jsonDate == '05') {
                                                    $jsonDate = '5';
                                                }
                                                if ($jsonDate == '06') {
                                                    $jsonDate = '6';
                                                }
                                                if ($jsonDate == '07') {
                                                    $jsonDate = '7';
                                                }
                                                if ($jsonDate == '08') {
                                                    $jsonDate = '8';
                                                }
                                                if ($jsonDate == '09') {
                                                    $jsonDate = '9';
                                                }
                                                $jsonDate;
                                                $fr_month;
                                                $fr_yr;

                                                $selectRateQueryJson = $inventoryCont->getInventoryRateDetails($property_id, $room_id, $fr_month, $fr_yr);

                                                $rateNumRowJson = count($selectRateQueryJson);
                                                if ($rateNumRowJson == 0) {
                                                    //echo 'insert';
                                                    //echo $jsonDate;
                                                    $insertdata = $inventoryCont->InsertInventoryRateData($customerId, $jsonDate, $property_id, $room_id, $fr_month, $fr_yr, $price);
                                                } else {
                                                    //echo 'up';
                                                    $upd_data = $inventoryCont->UpdateInventoryRateData($jsonDate, $price, $property_id, $room_id, $fr_month, $fr_yr);
                                                }
                                            }
                                            echo '<script type="text/javascript">
                alert("Inventory Update Succcesfuly");              
window.location = "manage_rates.php";
            </script>';
                                            ?>



                                            <div id="new_calendar">

                                                <div class="rates">
                                                    <?php
                                                    // $jsonUrlSelect = "select * from googlesheet where customerID='$custID'";
                                                    /*   $jsonUrlSelect = $inventoryCont->getGoogleSheetData($property);
                                                      $googleFolder = $jsonUrlSelect[0];

                                                      //$selectUrlJsonQuer = mysql_query($jsonUrlSelect);
                                                      foreach ($jsonUrlSelect as $jsonUrlDat) {
                                                      $unparsed_json = file_get_contents($jsonUrlDat->sheetUrl);


                                                      $json_object = json_decode($unparsed_json);
                                                      //print_r($json_object);
                                                      //	die;
                                                      foreach ($json_object as $jsonArr) {
                                                      //print_r($jsonArr);
                                                      $propNameJson = $jsonArr->{'Property Name'};
                                                      // echo $jsonArr->{'Property Name'};
                                                      // die();
                                                      //$selectPropJson = "select * from propertyTable where propertyName='$propNameJson'";
                                                      $selectPropJson = $propertyCont->getPropertyWithName($propNameJson);

                                                      //  $selectPropJsonQuery = mysql_query($selectPropJson);

                                                      foreach ($selectPropJson as $propJsonData) {
                                                      $propJsonId = $propJsonData->propertyID;
                                                      }

                                                      $roomJsonName = $jsonArr->{'Room Name'};

                                                      //$selectRoomJson = "select * from propertyRoom where propertyID='$propJsonId' and roomType='$roomJsonName'";
                                                      // $selectRoomJsonQuery = mysql_query($selectRoomJson);
                                                      $selectRoomJsonQuery = $roomDataCount->getPropertyRoomType($propJsonId, $roomJsonName);
                                                      foreach ($selectRoomJsonQuery as $roomJsonData) {
                                                      $roomJsonId = $roomJsonData->roomID;
                                                      }

                                                      $jsonDate = $jsonArr->{'Date'};
                                                      $jsonMonth = $jsonArr->{'Month'};
                                                      $jsonYear = $jsonArr->{'Year'};
                                                      $jsonPrice = $jsonArr->{'Price'};

                                                      // $selectRateJson = "select * from manage_rate where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";
                                                      // $selectRateQueryJson = mysql_query($selectRateJson);
                                                      $selectRateQueryJson = $inventoryCont->getInventoryRateDetails($propJsonId, $roomJsonId, $jsonMonth, $jsonYear);
                                                      $rateNumRowJson = count($selectRateQueryJson);
                                                      if ($rateNumRowJson == 0) {
                                                      $insertdata = $inventoryCont->InsertInventoryRateData($customerId, $jsonDate, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear, $jsonPrice);
                                                      // $insertRateJson = "insert into manage_rate(property_id,room_id,month,year,`$jsonDate`) values('$propJsonId','$roomJsonId','$jsonMonth','$jsonYear','$jsonPrice')";
                                                      //$queryInsJson = mysql_query($insertRateJson);
                                                      $msg = 'Successfuly Insert Data';
                                                      } else {
                                                      $upd_data = $inventoryCont->UpdateInventoryRateData($jsonDate, $jsonPrice, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear);
                                                      //  $insertRateJson = "update manage_rate set`$jsonDate`='$jsonPrice' where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";
                                                      //  $queryInsJson = mysql_query($insertRateJson);
                                                      $msg = 'Successfuly Updated Data';
                                                      }
                                                      }
                                                      }
                                                     * */
                                                    ?>


                                                </div>
                                            </div><br><br>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#property').on('change', function () {
            var propertyID = $(this).val();
            if (propertyID) {
                $.ajax({
                    type: 'POST',
                    url: 'property_room.php',
                    data: 'property_id=' + propertyID,
                    success: function (html) {
                        $('#room').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#room').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';    ?>
<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>