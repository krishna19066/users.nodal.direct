<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$cityCont = new propertyData();
$themeCount = new menuNavigationPage();
$fun_resData = $cityCont->get_Cloud_AdminDetails($customerId);
$theme_data = $themeCount->getThemeColor($customerId);
//print_r($resData);
$resData = $theme_data[0];
$NoOfRow = count($theme_data);

if(isset($_POST['insert_theme_color'])){
    $ins = $themeCount->InsertThemeColor($customerId);
     echo '<script type="text/javascript">
                alert("Succesfuly Update Theme Setting");              
                window.location = "theme-color-setting.php";
            </script>';
    exit;
}

//------------Update theme color  ------------------------------->
if (isset($_POST['update_theme_color'])) {
    $heading_color = $_REQUEST['heading_color'];
    $icon_color = $_REQUEST['icon_color'];
    $header_color = $_REQUEST['header_color'];
    $footer_color = $_REQUEST['footer_color'];
    $upd = $themeCount->UpdateThemeColor($customerId, $header_color, $footer_color, $heading_color, $icon_color);
    echo '<script type="text/javascript">
                alert("Succesfuly Update Theme Setting");              
                window.location = "theme-color-setting.php";
            </script>';
    exit;
}
if(isset($_POST['reset_theme_setting'])){
    $upd = $themeCount->ResetThemeSetting($customerId);
    echo '<script type="text/javascript">
                alert("Succesfuly Reset Theme Setting");              
                window.location = "theme-color-setting.php";
            </script>';
    exit;
}
?>

<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }
                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>

            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />

            <!-- END THEME LAYOUT STYLES -->

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Manage Theme Color
                                    <small>Select theme color here</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="theme-color-setting.php">Theme Setting</a>
                                <i class="fa fa-circle"></i>
                            </li>
                        </ul>
                        <div id="top_menu_container">
                            <ul id="top_menu">
                                <a href="manage-gen-setting.php" style="text-decoration:none;"><li class="OurMenuActive">Theme Color</li></a>
                                <!-- <li onclick="load_food_menu();">Food Menu</li>
                                <li onclick="load_upload_logo();">Update Logo</li>
                                <li onclick="load_fevicon();">Update Fevicon </li>-->
                                <form action="" method="post">
                                    <input type="submit" name="reset_theme_setting" class="btn btn-warning" value="Reset Setting"/>
                                </form>
                            </ul>
                        </div>
                        <div id="cityCont">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <div style="width:100%;" class="clear"> 
                                                <span>*Note: After update theme color then go to website and CTRL+Shift+R or refresh 2-3 times.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="background: white;">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;">
                                        <div class="form-body">
                                            <div class="expDiv" onclick="$('#HeaderFooter').slideToggle();" style="background:#2AB4C0;border-bottom:2px solid #2AB4C0;"><h3 style="color:white;">Header Footer </h3></div>
                                            <div id="HeaderFooter" style="display:none;"><br/>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Header Menu
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        BG Color: <input type="text" class="form-control jscolor" name="header_color"  value="<?= $resData->header_color; ?>"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        Font Size (px):  <input type="number" class="form-control" name="header_font_size"  value="<?= $resData->header_font_size; ?>"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        Font Family: <select name="header_font_family" class="form-control">
                                                            <option value="<?php echo $resData->header_font_family; ?>"><?php echo $resData->header_font_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Footer
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        BG Color:<input type="text" class="form-control jscolor" name="footer_color"  value="<?= $resData->footer_color; ?>"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        Font Size (px):<input type="number" class="form-control" name="footer_font_size"  value="<?= $resData->footer_font_size; ?>"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        Font Family: <select name="footer_font_family" class="form-control">
                                                            <option value="<?php echo $resData->footer_font_family; ?>"><?php echo $resData->footer_font_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="expDiv" onclick="$('#buttonIcon').slideToggle();" style="background:#2AB4C0;border-bottom:2px solid #2AB4C0;"><h3 style="color:white;">Button & Icon </h3></div>
                                            <div id="buttonIcon" style="display:none;"><br/>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Button Color
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control jscolor" name="button_color"  value="<?= $resData->button_color; ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Button Color Hover
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control jscolor" name="button_color_hover"  value="<?= $resData->button_color_hover; ?>" />
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Icon Color.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control jscolor" name="icon_color"  value="<?= $resData->icon_color; ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Icon Color Hover.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control jscolor" name="icon_color_hover"  value="<?= $resData->icon_color_hover; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="expDiv" onclick="$('#heading').slideToggle();" style="background:#2AB4C0;border-bottom:2px solid #2AB4C0;"><h3 style="color:white;">Heading's</h3></div>
                                            <div id="heading" style="display:none;"><br/>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">H1 Color,Size,Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        H1 Color: <input type="text" class="form-control jscolor" name="h1_color"  value="<?= $resData->h1_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H1 Size(px) : <input type="number" class="form-control" name="h1_size"  value="<?= $resData->h1_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H1 Family: <select name="h1_family" class="form-control">
                                                            <option value="<?php echo $resData->h1_family; ?>"><?php echo $resData->h1_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">H2 Color,Size,Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        H2 Color: <input type="text" class="form-control jscolor" name="h2_color"  value="<?= $resData->h2_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H2 Size(px) : <input type="number" class="form-control" name="h2_size"  value="<?= $resData->h2_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H2 Family: <select name="h2_family" class="form-control">
                                                            <option value="<?php echo $resData->h2_family; ?>"><?php echo $resData->h2_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">H3 Color,Size,Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        H3 Color: <input type="text" class="form-control jscolor" name="h3_color"  value="<?= $resData->h3_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H3 Size(px) : <input type="number" class="form-control" name="h3_size"  value="<?= $resData->h3_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H3 Family: <select name="h3_family" class="form-control">
                                                            <option value="<?php echo $resData->h3_family; ?>"><?php echo $resData->h3_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">H4 Color,Size,Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        H4 Color: <input type="text" class="form-control jscolor" name="h4_color"  value="<?= $resData->h3_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H4 Size (px) : <input type="number" class="form-control" name="h4_size"  value="<?= $resData->h4_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H4 Family: <select name="h4_family" class="form-control">
                                                            <option value="<?php echo $resData->h4_family; ?>"><?php echo $resData->h4_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">H5 Color,Size,Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        H5 Color: <input type="text" class="form-control jscolor" name="h5_color"  value="<?= $resData->h5_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H5 Size(px) : <input type="number" class="form-control" name="h5_size"  value="<?= $resData->h5_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H5 Family: <select name="h5_family" class="form-control">
                                                            <option value="<?php echo $resData->h5_family; ?>"><?php echo $resData->h5_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">H6 Color,Size,Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        H6 Color: <input type="text" class="form-control jscolor" name="h6_color"  value="<?= $resData->h6_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H6 Size(px) : <input type="number" class="form-control" name="h6_size"  value="<?= $resData->h6_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        H6 Family: <select name="h6_family" class="form-control">
                                                            <option value="<?php echo $resData->h6_family; ?>"><?php echo $resData->h6_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Content Font size & Family.
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-2">
                                                        Font color(all content): <input type="text" class="form-control jscolor" name="content_font_color"  value="<?= $resData->content_font_color; ?>" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        Font Size(all content, size in px):  <input type="number" class="form-control" name="content_font_size"  value="<?= $resData->content_font_size; ?>" />
                                                    </div>
                                                    <div class="col-md-2">

                                                        Font Family(all content): <select name="content_font_family" class="form-control">
                                                           <option value="<?php echo $resData->content_font_family; ?>"><?php echo $resData->content_font_family; ?></option>
                                                            <option value="">Select Font Family</option>
                                                            <option value="Arial">Arial</option>
                                                            <option value="Helvetica">Helvetica</option>
                                                            <option value="Times New Roman">Times New Roman</option>
                                                            <option value="Courier New">Courier New</option>
                                                            <option value="Verdana">Verdana</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Palatino">Palatino</option>
                                                            <option value="Garamond">Garamond</option>
                                                            <option value="Comic Sans MS">Comic Sans MS</option>                                                          
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9"> 
                                            <?php if ($NoOfRow > 0) { ?>
                                                <input name="update_theme_color" type="hidden" value="update" />
                                            <?php } else { ?>
                                                <input name="insert_theme_color" type="hidden" value="insert" />
                                            <?php } ?>
                                            <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update" />

                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
            </div>

            </div>
            </div>

        </body>
    </html>

    <script>
        $('#top_menu li').click(function () {
            $('#top_menu li').removeClass('OurMenuActive');
            $(this).addClass('OurMenuActive');
        });
    </script>

    <script>
        function load_upload_logo() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_upload-logo.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_fevicon() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_upload-fevicon.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_room_type() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_room_type.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_room_type() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_room_type.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_city() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_city.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_edit_city(cityid) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_edit_city.php',
                type: 'post',
                data: {cityid: cityid},
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_content_city(type_id) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_content_city.php',
                type: 'post',
                data: {type_id: type_id},
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_subcity() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add_subcity.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_add_propertytype() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add-propertytype.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_roomtype() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add-roomtype.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
    <script src="js/jscolor.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <?php
    admin_footer();
    ?>        
</div>	