<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$propertyCont = new propertyData();
$template_data = $propertyCont->GetSupperAdminData($customerId);
$template_res = $template_data[0];
//print_r($template_res);
$select_temp = $template_res->template_selected;
if ($select_temp == 'single1') { // anand satluj template
    $about_us = 'https://preview.ibb.co/dp7GDA/screencapture-anandatthesatluj-about-us-html-2018-10-29-13-41-47.png';
    $contact_us = 'https://preview.ibb.co/ixErfq/screencapture-anandatthesatluj-contact-us-html-2018-10-29-13-42-28.png';
    $gallery = 'https://preview.ibb.co/j68Z0q/screencapture-anandatthesatluj-gallery-html-2018-10-29-13-43-36.png';
    $our_client = 'images/why-us.png';
    $client_sub = 'images/why-us.png';
    $policy = 'https://preview.ibb.co/nxCNmV/policies-html.png';
    $term_and_cond = 'https://preview.ibb.co/eAPUYA/term-and-conditions-html.png';
    $food = 'https://preview.ibb.co/jKAaOA/dine-with-us.png';
    $why_us = 'https://preview.ibb.co/kk8L6V/activities-html.png';
} elseif ($select_temp == 'single2') { // React js template
    $about_us = 'https://preview.ibb.co/g3mMfq/about-us.png';
    $contact_us = 'https://preview.ibb.co/edj2mV/contact-us.png';
    $gallery = 'https://preview.ibb.co/f7xoRV/gallery.png';
    $our_client = 'https://preview.ibb.co/gviTRV/services.png';
    $client_sub = 'https://preview.ibb.co/gviTRV/services.png';
    $policy = 'https://preview.ibb.co/nxCNmV/policies-html.png';
    $term_and_cond = 'https://preview.ibb.co/eAPUYA/term-and-conditions-html.png';
    $food = 'https://preview.ibb.co/gviTRV/services.png';
    $why_us = 'https://preview.ibb.co/gviTRV/services.png';
} elseif ($select_temp == 'multiple1') { // Perch Temp
    //call Google PageSpeed Insights API
       /* $siteURL = "https://www.theperch.in/about-us.html";
        $googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$siteURL&screenshot=true");

        //decode json data
        $googlePagespeedData = json_decode($googlePagespeedData, true);

        //screenshot data
        $screenshot = $googlePagespeedData['screenshot']['data'];
        $screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);

        //display screenshot image
        echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";
       $about_us =  '\"data:image/jpeg;base64,"'.$screenshot.'"\"';
       */
    $about_us = 'https://preview.ibb.co/fsw9GV/screencapture-theperch-in-about-us-html-2018-10-29-13-27-40.png';
    $contact_us = 'https://preview.ibb.co/ngfGVq/screencapture-theperch-in-contact-us-html-2018-10-29-13-29-30.png';
    $gallery = 'https://preview.ibb.co/j68Z0q/screencapture-anandatthesatluj-gallery-html-2018-10-29-13-43-36.png';
    $our_client = 'https://preview.ibb.co/iG4tAq/screencapture-theperch-in-corporate-clients-html-2018-10-29-13-34-01.png';
    $client_sub = 'https://preview.ibb.co/iG4tAq/screencapture-theperch-in-corporate-clients-html-2018-10-29-13-34-01.png';
    $policy = 'https://preview.ibb.co/j6wUiA/screencapture-theperch-in-policies-html-2018-10-29-13-33-12.png';
    $term_and_cond = 'https://preview.ibb.co/j6wUiA/screencapture-theperch-in-policies-html-2018-10-29-13-33-12.png';
    $food = 'https://preview.ibb.co/bA4Aqq/screencapture-theperch-in-food-html-2018-10-29-13-30-27.png';
    $why_us = 'https://preview.ibb.co/ngReiA/screencapture-theperch-in-why-the-perch-html-2018-10-29-13-31-00.png';
} elseif ($select_temp == 'multiple2') {// startlit temp
    $about_us = 'https://preview.ibb.co/bS22Lq/about-starlit.png';
    $contact_us = 'https://preview.ibb.co/kiLHmV/contact.png';
    $gallery = 'https://preview.ibb.co/cwg1DA/screencapture-cinnamonstays-in-gallery-html-2018-10-29-13-39-57.png';
    $our_client = 'https://preview.ibb.co/k0GobV/starlit-services.png';
    $client_sub = 'https://preview.ibb.co/k0GobV/starlit-services.png';
    $policy = 'images/policy.png';
    $term_and_cond = 'images/policy.png';
    $food = 'https://preview.ibb.co/bA4Aqq/screencapture-theperch-in-food-html-2018-10-29-13-30-27.png';
    $why_us = 'https://preview.ibb.co/dJcTbV/upcoming-properties.png';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
<?php echo ajaxCssHeader(); ?>
        <style>
            .card{
                margin:50px;
                float:left;
                position:relative;
                border:1px solid grey;
                background:black;
                color:white;
                font-weight:bold;
            }

            .card input[type="radio"]
            {
                margin-top:10px;
                margin-bottom:10px;
            }

            .card i{			
                font-size:55px;
                z-index:2;
                margin-top: 50%;
            }
            .overly{
                display:none;
                text-align:center;
                top:0;
                width:100%;
                height:300px;
                position:absolute;
                background:rgba(0, 0, 0, 0.5);
                color:#fff;			
            }

            .dn{
                display:none;
            }
            .db{
                display:block;
            }
            .card > :nth-child(3){
                display:block;
            }

        </style>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
<?php
themeheader();
?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
<?php
admin_header('manage-static-pages.php');
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add New Static Page
                                <small>Add a new static page here..</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="index.html">Home & Static Page</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add New Static Page</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body ">
                                    <div style="width:100%;" class="clear"> 
                                        <a class="pull-right">
                                            <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View Page</button></a>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light bordered">
                                <div class="portlet-body form">
                                    <form class="form-horizontal" name="formn" method="post" action="updatePage_content.php" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-12"><center>Choose a Static Page</center></label><br>
                                                <div class="abc">
                                                    <div class="card clear">
                                                        <img src="<?php echo $about_us; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="about-us" />
                                                        <center> About US page </center><br>
                                                    </div>		

                                                    <div class="card">
                                                        <img src="<?php echo $our_client; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="our-clients" />
                                                        <center> Our Clients page </center><br>
                                                    </div>		

                                                    <div class="card">
                                                        <img src="<?php echo $contact_us; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="contact-us" />
                                                        <center> Contact US page </center><br>
                                                    </div>		

                                                    <div class="card">
                                                        <img src="<?php echo $policy; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="policies" />
                                                        <center> Policies page </center><br>
                                                    </div>
                                                    <div class="card">
                                                        <img src="<?php echo $policy; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="term-and-conditions" />
                                                        <center> Term & Conditions </center><br>
                                                    </div>

                                                    <div class="card">
                                                        <img src="<?php echo $food; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="food" />
                                                        <center> Food page </center><br>
                                                    </div>

                                                    <div class="card">
                                                        <img src="<?php echo $why_us; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="whyPerch" />
                                                        <center> Why Us page </center><br>
                                                    </div>

                                                    <div class="card">
                                                        <img src="<?php echo $client_sub; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="clientsub" />
                                                        <center> Clients page </center><br>
                                                    </div>
                                                    <div class="card">
                                                        <img src="<?php echo $gallery; ?>" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="gallery" />
                                                        <center> Gallery page </center><br>
                                                    </div>
                                                    <div class="card">
                                                        <img src="images/video.png" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="video-gallery" />
                                                        <center>  Video Gallery page </center><br>
                                                    </div>
                                                    <div class="card">
                                                        <img src="images/service.png" width="200px" height="300px"/>
                                                        <div class="overly dn"><i class="fa fa-check"></i></div>
                                                        <input class="form-control" class="khwedk" type="radio" name="page_name" value="services" />
                                                        <center>Service page </center><br>
                                                    </div>
                                                </div>
                                            </div><br><br>

                                            Enter the Page URL : <input type="text" name="new_page_url" style="width:300px;height:35px;padding-left:10px;color:grey;" /><br><br>

                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="submit" name="create_static" class="btn green" id="submit" value="Add" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">

                <!-- END QUICK SIDEBAR -->
            </div>              
            <div class="quick-nav-overlay"></div>

<?php echo ajaxJsFooter(); ?>
    </body>
</html>