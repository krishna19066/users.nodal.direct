<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$ajaxclientCont = new staticPageData();
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getContactUsPageData($pageUrl, $customerId);
//  print_r($clientPageData);
$ajaxpropertyPhotoCont = new propertyData();
$pageData = $clientPageData[0];
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$bucket = $cloud_keyData->bucket;
$cloud_cdnName = $cloud_keyData->cloud_name;
$base_url = $cloud_keyData->website;

$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
//  die;
$superAdmin_data = $ajaxpropertyPhotoCont->GetSupperAdminData($customerId);
$superAdmin_res = $superAdmin_data[0];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>  
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> CONTACT US PAGE DETAILS
                                <small>View / edit all the details of your client page</small>
                            </h1>
                        </div>
                    </div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-static-pages.php">Home & Static Page</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Edit Contact us Page Details</span>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body ">
                                    <div style="width:100%;" class="clear"> 
                                        <a class="pull-right">
                                            <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                        </a>
                                        <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ----------Start FORM------------------------------------->
                    <div class="portlet light bordered" id="add_meta" style="display:none;">	
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Meta Title
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                            <span class="help-block"> Provide Meta Title </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Meta Description</label>
                                        <div class="col-md-9">
                                            <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <?php if ($metaRes > 0) { ?>
                                        <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                        <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                        <input type="hidden" class="form-control" name="pagename" value="ajax_edit_contactus" />
                                        <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>/>
                                    <?php } else { ?>
                                        <input type="hidden" class="form-control" name="pagename" value="ajax_edit_contactus" />
                                        <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                        <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>/>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>

                    <!-- END FORM-->

                    <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->


                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Edit Contact us Page </span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <span style="float:right;"><a href="<?php echo $base_url; ?>/<?php echo $pageUrl; ?>.html" target="_blank"><input type="button" style="background:#03A9F4;color:white;border:none;height:35px;width:180px;font-size:14px;margin-right: 11px;margin-top: 7px;" data-html="true"  value="Preview Page" /></a></span><br/><br/>
                                    <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">
                                        <div class="form-body">

                                            <?php if ($superAdmin_res->template_type == 'single') { ?>

                                                <div class="expDiv" onclick="$('#box_content').slideToggle();" style="cursor: pointer; border: 1px solid #ececec"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Boxed Contents </h3></div>

                                                <div id="box_content" style="display:none;">

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="well" onclick="$('#content_box_1').slideToggle();" style="cursor:pointer;">
                                                                        <h4 class="text-danger"><span class="label label-danger pull-right">Edit</span><i class="fa fa-thumbs-up fa_line_height"></i> Address 1 </h4>
                                                                        <hr/>
                                                                        <strong><?php
                                                                            if ($pageData->address1head) {
                                                                                echo $pageData->address1head;
                                                                            } else {
                                                                                echo 'Heding here..';
                                                                            }
                                                                            ?>:</strong><?php
                                                                        if ($pageData->address1) {
                                                                            echo html_entity_decode($pageData->address1);
                                                                        } else {
                                                                            echo 'Address here..';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="well" onclick="$('#content_box_2').slideToggle();" style="cursor:pointer;">
                                                                        <h4 class="text-success"><span class="label label-success pull-right">Edit</span><i class="fa fa-map-marker fa_line_height"></i> Address 2 </h4>
                                                                        <hr/>
                                                                        <strong><?php
                                                                            if ($pageData->address2head) {
                                                                                echo $pageData->address2head;
                                                                            } else {
                                                                                echo 'Heding here..';
                                                                            }
                                                                            ?>:</strong><?php
                                                                        if ($pageData->address2) {
                                                                            echo html_entity_decode($pageData->address2);
                                                                        } else {
                                                                            echo ' Other Address here..';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="well" onclick="$('#content_box_3').slideToggle();" style="cursor:pointer;">
                                                                        <h4 class="text-primary"><span class="label label-primary pull-right">Edit</span><i class="fa fa-envelope  fa_line_height"></i> Email </h4>
                                                                        <hr/>
                                                                        <strong><?php
                                                                            if ($pageData->emailhead) {
                                                                                echo $pageData->emailhead;
                                                                            } else {
                                                                                echo 'Heding here..';
                                                                            }
                                                                            ?>:</strong><?php
                                                                        if ($pageData->email) {
                                                                            echo html_entity_decode($pageData->email);
                                                                        } else {
                                                                            echo ' Email Address here..';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="well" onclick="$('#content_box_4').slideToggle();" style="cursor:pointer;">
                                                                        <h4 class="text-success"><span class="label label-success pull-right">Edit</span><i class="fa fa-phone fa_line_height"></i> Phone </h4>
                                                                        <hr/>
                                                                        <strong><?php
                                                                            if ($pageData->phonehead) {
                                                                                echo $pageData->phonehead;
                                                                            } else {
                                                                                echo 'Heding here..';
                                                                            }
                                                                            ?>:</strong><?php
                                                                        if ($pageData->phone) {
                                                                            echo html_entity_decode($pageData->phone);
                                                                        } else {
                                                                            echo 'Phone Number here..';
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div><!--/row-->    
                                                        </div><!--/col-12-->
                                                    </div><!--/row-->

                                                    <div id="content_box_1" style="display:none;"><br/>
                                                        <table width="100%">
                                                            <td><hr /></td>
                                                            <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Address 1</td>
                                                            <td><hr /></td>
                                                        </table>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Address Heading 1
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box1head" value="<?php echo $pageData->address1head; ?>" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Address 1 Content
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control"  name="box1content" rows="6" required ><?php echo html_entity_decode($pageData->address1); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="content_box_2" style="display:none;"><br/>
                                                        <table width="100%">
                                                            <td><hr /></td>
                                                            <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Address 2</td>
                                                            <td><hr /></td>
                                                        </table>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Address Heading 2
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box2head" value="<?php echo $pageData->address2head; ?>" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3"> Address 2 Content
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control"  name="box2content" rows="6" required ><?php echo html_entity_decode($pageData->address2); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="content_box_3" style="display:none;"><br/>
                                                        <table width="100%">
                                                            <td><hr /></td>
                                                            <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;"> Email Address</td>
                                                            <td><hr /></td>
                                                        </table>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Email Heading
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box3head" value="<?php echo $pageData->emailhead; ?>" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3"> Email Content
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control"  name="box3content" rows="6" required ><?php echo html_entity_decode($pageData->email); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="content_box_4" style="display:none;"><br/>
                                                        <table width="100%">
                                                            <td><hr /></td>
                                                            <td style="width:1px; padding: 0 10px; white-space: nowrap;color: #E91E63;">Phone Number</td>
                                                            <td><hr /></td>
                                                        </table>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Phone Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="box4head" value="<?php echo $pageData->phonehead; ?>" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3"> Phone Number
                                                                <span class="required"> * </span>
                                                            </label>

                                                            <div class="col-md-9">
                                                                <textarea class="ckeditor form-control"  name="box4content" rows="6" required ><?php echo html_entity_decode($pageData->phone); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <div class="expDiv" onclick="$('#general_content').slideToggle();" style="cursor: pointer; border: 1px solid #ececec"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>

                                            <div id="general_content" style="display:none;"><br/>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Header Image
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                            <?php if ($pageData->heade_img) { ?>
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                            <?php } else { ?>
                                                                <?php if($bucket){ ?>
                                                                     <img src="https://<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $pageData->header_img;  ?><?php if($customerId=='1'){ ?>.jpg<?php } ?>" alt=""/>
                                                                    <?php }else{ ?>
                                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/contactus/<?php echo $pageData->header_img; ?>.jpeg" alt=""/>
                                                            <?php }  } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image_header"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Main Heading
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="tab1name" value="<?php echo $pageData->tab1_name; ?>" required="" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-5">

                                                        <div class="well" onclick="$('#left_content').slideToggle();" style="cursor:pointer;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Add/Edit Google map</span><i class="fa fa-map-marker fa_line_height"></i> Google Map </h4><hr/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2"> Map URL
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" name="mapurl" value="<?php echo $pageData->map_url; ?>" required />
                                                                </div>
                                                            </div>
                                                            <?php if ($pageData->map_url != '') { ?>
                                                                <iframe  src="<?php echo $pageData->map_url; ?>" width="100%" height="390" frameBorder="0" frameborder="0" allowfullscreen="" ></iframe>
                                                            <?php } else { ?>
                                                                <div style="width:100%;height: 390px; border: 1px solid gray;">
                                                                    <center> <h3>Map Not available </h3></center>
                                                                </div>
                                                            <?php } ?>

                                                        </div>

                                                    </div>
                                                    <?php if ($superAdmin_res->template_type != 'single') { ?>
                                                        <div class="col-md-7">
                                                            <div class="well" onclick="$('#right_content').slideToggle();" style="cursor:pointer;">
                                                                <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Right Side Content</h4><hr/>
                                                                <div class="row">


                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Tab 1 Heading
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" name="tab1heading" value="<?php echo $pageData->tab1_heading; ?>"  />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Tab 1 Form Heading
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" name="tab1form" value="<?php echo $pageData->tab1_formName; ?>" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Tab 1 Sub-Heading
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-6">
                                                                            <input type="text" class="form-control" name="tab1subhead" value="<?php echo $pageData->tab1_subheading; ?>" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3"> Email Content
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="email" class="form-control" name="box3content" value="<?php echo html_entity_decode($pageData->email); ?>" required="" >
<!--                                                                            <textarea class="ckeditor form-control"   rows="6" required ><?php echo html_entity_decode($pageData->email); ?></textarea>-->
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3"> Phone Number
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="box4content" value=" <?php echo html_entity_decode($pageData->phone); ?> " >
<!--                                                                            <textarea class="ckeditor form-control"   rows="6" required ></textarea>-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3">Address 1 Content
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="box1content" value="<?php echo html_entity_decode($pageData->address1); ?>" required >
<!--                                                                            <textarea class="ckeditor form-control"   rows="6"  ></textarea>-->
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3"> Tab 1 Content
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <textarea class="ckeditor form-control"  name="tab1content" rows="6" required ><?php echo html_entity_decode($pageData->tab1_content); ?></textarea>
                                                                        </div>
                                                                    </div>








                                                                </div>

                                                            </div>

                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                        </div><!--/row-->    

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if ($clientPageData) {
                                                        ?>
                                                        <input type="hidden"  name="page_url" value="<?php echo $pageData->page_url; ?>" />
                                                        <input type="hidden" name="typeofsave" value="updation" />

                                                        <?php
                                                    } else {
                                                        ?>
                                                        <input type="hidden" name="typeofsave" value="newaddition" />
                                                        <?php
                                                    }
                                                    ?>
                                                    <input type="submit" name="contactussave" class="btn green" id="submit" value="Save Contact Us Page" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?>>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN QUICK SIDEBAR -->                   
        <!-- END QUICK SIDEBAR -->
    </div>



    <div class="quick-nav-overlay"></div>
    <?php echo ajaxJsFooter(); ?>
</body>
</html>
