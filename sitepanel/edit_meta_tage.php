<?php
include "admin-function.php";
//$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$customerId = $_SESSION['customerID'];
$analyticsCont = new analyticsPage();
?>

<?php
$id = $_GET['id'];
$metano = $_GET['metano'];
$url = $_GET['url'];
//$pageUrl = $_POST['pageUrlData'];

$metatagPageData = $ajaxclientCont->getMetaTagsByID($customerId, $metano);

$Metapage = $metatagPageData[0];
$numb = count($Metapage);
$cityUrlData = $ajaxclientCont->getCityUrlData($customerId);
$city_url = $cityUrlData[0];
$staticPageUrlData = $ajaxclientCont->getStaticPageUrlData($customerId);
$propertyPageUrlData = $ajaxclientCont->getPropertyPageUrlData($customerId);
$roomPageUrlData = $ajaxclientCont->getRoomPageUrlData($customerId);
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>  
            <?php echo ajaxCssHeader(); ?>      
        </head>


        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>          
            <div class="clearfix"> </div>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE ANALYTICS & SEO
                                    <small>Edit Your Meta Tags</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">ANALYTICS & SEO</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Edit Meta Tags</span>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" style="height: 80px;">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <a class="pull-right">
                                                <a href="manage-analytic-settings.php"> <button style="background:#36c6d3;color:white;border:none;height:35px;width:190px;font-size:14px; float: right;"><i class="fa fa-eye"></i> &nbsp View All Meta tag List</button></a>                                               
                                                <a href="ajax_add_meta_tag.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right; margin-right: 13px;"><i class="fa fa-plus"></i> &nbsp Add Meta Tags</button></a>
                                                <a href="ajax_add_analytic_code.php"><button style="background:#ff9800;color:white;border:none;height:35px;width:190px;font-size:14px; float: right; margin-right: 21px;"><i class="fa fa-plus"></i> &nbsp Add Google Analytics</button></a>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="property_change" style="background: white;">
                            <div class="row">
                                <div class="col-md-12">	
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;">
                                        <div class="form-body" style="margin-top: 22px;">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">File URL</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="filename" value="<?php echo $Metapage->filename; ?>" readonly />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $metano; ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Meta Title
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="MetaTitle" value="<?= $Metapage->MetaTitle; ?>" />
                                                    <span class="help-block"> Provide Meta Title </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Description</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" rows="3" name="MetaDisc"><?= $Metapage->MetaDisc; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Keyword</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" rows="3" name="MetaKwd"><?= $Metapage->MetaKwd; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Other Code</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" rows="3" name="othercode"><?= $Metapage->othercode; ?></textarea>
                                                </div>  
                                            </div>     
                                            <a href="manage-analytic-settings.php"><input type="button" name="back" class="btn red" value="Back" style="margin: 154px;margin-top: 23px;" /></a>

                                            <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tags" style="margin: 154px;margin-top: 23px;" />
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </body>

        <script>
            $('#top_menu li').click(function () {
                $('#top_menu li').removeClass('OurMenuActive');
                $(this).addClass('OurMenuActive');
            });
        </script>
        <script>
            function load_city_detail(urldata) {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#property_change').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_property.php',
                    data: {cityData: urldata},
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#property_change').html(output);
                    }
                });
            }
        </script>


        <script>
            function load_add_property() {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_add-property.php',
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>

        <script>
            function load_property_photo(propertyPhotoId) {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_view-property-photo.php',
                    data: {propPhotoId: propertyPhotoId},
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>

        <script>
            function load_manage_question(propertyQuestionId) {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_manage-question.php',
                    data: {propQuestionId: propertyQuestionId},
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>

        <script>
            function load_add_rooms(propertyRoomsId) {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_add-rooms.php',
                    data: {propRoomsId: propertyRoomsId},
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>

        <script>
            function load_view_rooms(propRoomId) {
                var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                $('#add_prop').html(loadng);

                $.ajax({url: 'ajax_lib/ajax_property-rooms.php',
                    data: {propRumId: propRoomId},
                    type: 'post',
                    success: function (output) {
                        // alert(output);
                        $('#add_prop').html(output);
                    }
                });
            }
        </script>


        <script>
            function expand_me(id_name) {

                var span_name = "toggle_expand" + id_name;
                var data_name = "data_box" + id_name;

                if (document.getElementById(span_name).innerHTML === "Expand") {
                    document.getElementById(span_name).innerHTML = "Collapse";
                    document.getElementById(data_name).style.display = "block";
                } else {
                    document.getElementById(span_name).innerHTML = "Expand";
                    document.getElementById(data_name).style.display = "none";
                }

            }


        </script>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <?php
        admin_footer();
        ?>
    </html>
</div>