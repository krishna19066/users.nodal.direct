<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
$propertyCont = new menuNavigationPage();
$roomDataCount = new propertyData();
?>

<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>

<?php
//$con = mysqli_connect("localhost","root","","perch") or die("no connection");
session_start();
?>
<html>

    <head>
        <style>
            .header
            {
                width:100%;
                height:45px;
                background:#1ABC9C;
                padding-top:17px;
                color:white;
                font-size:25px;
                font-weight:bold;
                font-family:Arial;
            }
            .body
            {
                width:100%;
            }
            .property
            {
                width:100%;
            }
            .prop_select
            {
                width:400px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                padding-left:10px;
            }
            .prop_submit
            {
                height:40px;
                width:110px;
                background:#05B8CC;
                color:white;
                border:none;
                border-radius:3px;
                font-weight:bold;
            }
            .rate
            {
                width:100%;
            }
            .date_range
            {
                width:100%;
                border:1px solid #7FA6DD;
                padding-top:20px;
                padding-bottom:20px;
                box-shadow:1px 1px 1px 1px #cbdbf1;
                background:#f2f6fb;
            }
            .date_table
            {
                width:100%;
            }
            .date_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .date_submit
            {
                height:40px;
                width:140px;
                background:#05B8CC;
                color:white;
                border:none;
                border-radius:3px;
                font-weight:bold;
            }
            .room_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .rates
            {
                width:auto;
            }
            .calendar
            {
                width:98%;
                border:1px solid #227298;

                padding-right:10px;
            }
            .calendar_table
            {
                width:100%;

                border-collapse:collapse;
                background:#227298;
                color:white;
                height:40px;
            }
            .calendar_data
            {
                width:auto;

                border-collapse:collapse;
                background:white;
                color:black;
                height:40px;
            }

            .input_css {
                background: #E26A6A;
                border: 0px;
                width: 99%;
                height: 100%;
                font-size: 13px;
                text-align: center;
            }
            .rate_modify1
            {
                width:90%;
                padding:20px;
                border:1px solid #7FA6DD;
                box-shadow:1px 1px 1px 1px #cbdbf1;
                background:#f2f6fb;
            }
            .room_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }

        </style>

        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">

                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Manage Property Inventory</span>
                                                <span class="caption-helper">Manage your property room Inventory</span>
                                            </div>
                                        </div>
                                        <span style="float:right;"> <a href="manage_rates.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Rates</button></a></span><br><br>

                                        <div class="portlet-body form">
                                            <div class="form-body">
                                                <div class="body">
                                                    <?php
                                                    if (!isset($_POST['prop_submit'])) {
                                                        ?>
                                                        <?php
                                                        if (isset($_SESSION['propError'])) {
                                                            ?>
                                                            <center><font style="font-weight:bold;font-size:22px;color:#FF0066;"> <?php echo $_SESSION['propError']; ?> </font></center>
                                                            <?php
                                                            unset($_SESSION['propError']);
                                                        }
                                                        ?>
                                                        <div class="property">
                                                            <form action="" method="post">
                                                                <select name="hotel" class="prop_select">
                                                                    <option value="">--- Select property ---</option>
                                                                    <?php
                                                                    $propList1 = $inventoryCont->getPropertyListing($customerId);
                                                                    foreach ($propList1 as $propList) {
                                                                        ?>
                                                                        <option value="<?php echo $propList->propertyName; ?>"><?php echo $propList->propertyName; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select> &nbsp 

                                                                <input type="submit" name="prop_submit" value="Apply" class="prop_submit" />
                                                            </form>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                    if (isset($_POST['prop_submit'])) {
                                                        $property = $_POST['hotel'];
                                                        //echo $property;
                                                        if ($property == "") {
                                                            Header("location:manage_inventorys.php");
                                                            $_SESSION['propError'] = "Please Select A Valid Property Name..";
                                                        }
                                                        ?>
                                                        <div class="property">
                                                            <form action="" method="post">
                                                                <select name="hotel" class="prop_select">
                                                                    <option value=""> Select Property </option> 
                                                                    <?php
                                                                    // $sele = "select propertyName from propertyTable where customerID='$custID'";
                                                                    //$quer1 = mysql_query($sele);
                                                                    $propList1 = $inventoryCont->getPropertyListing($customerId);
                                                                    foreach ($propList1 as $propList) {
                                                                        ?>
                                                                        <option <?php
                                                                        if ($propList->propertyName == $property) {
                                                                            echo "selected";
                                                                        }
                                                                        ?>> <?php echo $propList->propertyName; ?> </option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                </select> &nbsp 

                                                                <input type="submit" name="prop_submit" value="Apply" class="prop_submit" />
                                                            </form>
                                                        </div><br>
                                                        <h3 style="color:#5B9C64;font-size:25px;font-weight:bold;"> <center> <?php echo $property; ?> </center></h3><br>
                                                          <h4 style="color:#5B9C64;font-size:18px;font-weight:bold;"> <center> Inventory are Updated through google Sheet. Please refer below link </center></h4><br>
                                                          
                                                        <br>
                                                        <br>
                                                       <!-- <center><div style="width:300px">
                                                                <select name="month_list" style="width:300px;height:35px;background:#eff8fd;color:#2f5e77;font-size:18px;border:1px solid #afddf7;boder-radius:5px;padding-left:10px;" id="get_month" onchange="select_dd()">
                                                                    <option value="<?php
                                                                    $mon = date('m');
                                                                    $year = date('Y');
                                                                    echo $mon . "-" . $year;
                                                                    ?>">
                                                                                <?php
                                                                                $mon1 = date('M');
                                                                                $year = date('Y');
                                                                                echo $mon1 . " &nbsp " . $year;
                                                                                ?>
                                                                    </option>


                                                                    <?php
                                                                    for ($i = 1; $i <= 12; $i++) {
                                                                        $mon1 = date('M', strtotime("+$i months"));
                                                                        $mon = date("m", strtotime("+$i months"));
                                                                        $year = date("Y", strtotime("+$i months"));
                                                                        echo "<option value='" . $mon . "-" . $year . "'>" . $mon1 . " &nbsp " . $year . "</option>";
                                                                    }
                                                                    ?>

                                                                </select>																	
                                                            </div></center>-->
                                                        <div id="new_calendar">
                                                               <!-- <div><!--<input  name="prev" id="prev_month_year" value="<?php //echo date('Y-m-d');            ?>" hidden /> <span style="float:left; font-size:30px;"><a href="javascript:;" onclick="next_month('prev')" class="fa fa-arrow-circle-left"> </a></span>
                                                                <input  name="next" id="next_month_year" value="<?php echo date('Y-m-d'); ?>" hidden /><span style="float:right; font-size:30px; padding-right:20px;"><a href="javascript:;" onclick="next_month('next')"  class="fa fa-arrow-circle-right"> </a></span>
                                                            </div><br><br> --->
                                                            <div class="rates">
                                                                <?php
                                                                // $jsonUrlSelect = "select * from googlesheet where customerID='$custID'";
                                                                $jsonUrlSelect = $inventoryCont->getGoogleSheetData($property);
                                                                 $googleFolder = $jsonUrlSelect[0];

                                                                //$selectUrlJsonQuer = mysql_query($jsonUrlSelect);
                                                                foreach ($jsonUrlSelect as $jsonUrlDat) {
                                                                    $unparsed_json = file_get_contents($jsonUrlDat->sheetUrl);


                                                                    $json_object = json_decode($unparsed_json);
//print_r($json_object);
                                                                    //	die;
                                                                    foreach ($json_object as $jsonArr) {
                                                                        //print_r($jsonArr);
                                                                        $propNameJson = $jsonArr->{'Property Name'};
                                                                        // echo $jsonArr->{'Property Name'};
                                                                        // die();
                                                                        //$selectPropJson = "select * from propertyTable where propertyName='$propNameJson'";
                                                                        $selectPropJson = $propertyCont->getPropertyWithName($propNameJson);

                                                                        //  $selectPropJsonQuery = mysql_query($selectPropJson);

                                                                        foreach ($selectPropJson as $propJsonData) {
                                                                            $propJsonId = $propJsonData->propertyID;
                                                                        }

                                                                        $roomJsonName = $jsonArr->{'Room Name'};

                                                                        //$selectRoomJson = "select * from propertyRoom where propertyID='$propJsonId' and roomType='$roomJsonName'";
                                                                        // $selectRoomJsonQuery = mysql_query($selectRoomJson);
                                                                        $selectRoomJsonQuery = $roomDataCount->getPropertyRoomType($propJsonId, $roomJsonName);
                                                                        foreach ($selectRoomJsonQuery as $roomJsonData) {
                                                                            $roomJsonId = $roomJsonData->roomID;
                                                                        }

                                                                        $jsonDate = $jsonArr->{'Date'};
                                                                        $jsonMonth = $jsonArr->{'Month'};
                                                                        $jsonYear = $jsonArr->{'Year'};
                                                                        $jsonPrice = $jsonArr->{'Inventory'};

                                                                        // $selectRateJson = "select * from manage_inventory where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";
                                                                        // $selectRateQueryJson = mysql_query($selectRateJson);
                                                                        $selectRateQueryJson = $inventoryCont->getInventoryDetails($propJsonId, $roomJsonId, $jsonMonth, $jsonYear);
                                                                        $rateNumRowJson = count($selectRateQueryJson);
                                                                        if ($rateNumRowJson == 0) {
                                                                            $insertdata = $inventoryCont->InsertInventoryData($customerId,$jsonDate, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear, $jsonPrice);
                                                                            // $insertRateJson = "insert into manage_inventory(property_id,room_id,month,year,`$jsonDate`) values('$propJsonId','$roomJsonId','$jsonMonth','$jsonYear','$jsonPrice')";
                                                                            //$queryInsJson = mysql_query($insertRateJson);
                                                                             $msg = 'Successfuly Insert Data';
                                                                        } else {
                                                                            $upd_data = $inventoryCont->UpdateInventoryData($jsonDate, $jsonPrice, $propJsonId, $roomJsonId, $jsonMonth, $jsonYear);
                                                                            //  $insertRateJson = "update manage_inventory set`$jsonDate`='$jsonPrice' where property_id='$propJsonId' and room_id='$roomJsonId' and month='$jsonMonth' and year='$jsonYear'";
                                                                            //  $queryInsJson = mysql_query($insertRateJson);
                                                                             $msg = 'Successfuly Updated Data';
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                                <?php $sheetCount = count($jsonUrlSelect);
                                                                if($sheetCount != 0){ ?>
                                                                 <h4 style="color:#5B9C64;font-size:18px;font-weight:bold;"> <center><a href="<?php echo $googleFolder->folderUrl; ?>" target="_blank"><?php echo $googleFolder->folderUrl; ?></a></center></h4><br>
                                                                <?php }else { ?>
                                                                 <h4 style="color:#5B9C64;font-size:18px;font-weight:bold;"> <center>Link Not Available.<a href="add-googlesheet-url.php"> Add Google Sheet Url</a></center></h4><br>
                                                                 <?php } ?>
                                                               <!-- <div class="calendar">
                                                                    <table class="calendar_table">
                                                                        <tr>
                                                                            <?php
                                                                            $dat_m = date('m');
                                                                            if ($dat_m == "01") {
                                                                                $dat_mon = "January";
                                                                            }
                                                                            if ($dat_m == "02") {
                                                                                $dat_mon = "February";
                                                                            }
                                                                            if ($dat_m == "03") {
                                                                                $dat_mon = "March";
                                                                            }
                                                                            if ($dat_m == "04") {
                                                                                $dat_mon = "April";
                                                                            }
                                                                            if ($dat_m == "05") {
                                                                                $dat_mon = "May";
                                                                            }
                                                                            if ($dat_m == "06") {
                                                                                $dat_mon = "June";
                                                                            }
                                                                            if ($dat_m == "07") {
                                                                                $dat_mon = "July";
                                                                            }
                                                                            if ($dat_m == "08") {
                                                                                $dat_mon = "August";
                                                                            }
                                                                            if ($dat_m == "09") {
                                                                                $dat_mon = "September";
                                                                            }
                                                                            if ($dat_m == "10") {
                                                                                $dat_mon = "October";
                                                                            }
                                                                            if ($dat_m == "11") {
                                                                                $dat_mon = "November";
                                                                            }
                                                                            if ($dat_m == "12") {
                                                                                $dat_mon = "December";
                                                                            }
                                                                            ?>

                                                                            <td style="width:110px;background:#cbf1f3;border:0px;padding-left:5px;margin-left:0px;"><span class="caption-subject bold uppercase" style="color:black;"><center><?php echo $dat_mon . "<br>" . date('Y'); ?></span>  </td>
                                                                            <?php
                                                                            $mon = date('m');
                                                                            $dd1 = date('d');
                                                                            if ($mon == "01" || $mon == "03" || $mon == "05" || $mon == "07" || $mon == "08" || $mon == "10" || $mon == "12") {
                                                                                for ($i = 1; $i <= 31; $i++) {
                                                                                    ?>
                                                                                    <td style="width:3%;<?php if ($i == $dd1) { ?>background:#36c6d3;<?php } ?>height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                                                                                        <?php echo $i; ?>
                                                                                        <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                                                                                    </td>			
                                                                                    <?php
                                                                                    //$date = date('Y-m-d', strtotime("+$i days"));
                                                                                    //echo $date."<br>";
                                                                                }
                                                                            } elseif ($mon == "04" || $mon == "06" || $mon == "09" || $mon == "11") {
                                                                                for ($i = 1; $i <= 30; $i++) {
                                                                                    ?>
                                                                                    <td style="width:3%;height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                                                                                        <?php echo $i; ?>
                                                                                        <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                                                                                    </td>
                                                                                    <?php
                                                                                }
                                                                            } elseif ($mon == "02") {
                                                                                for ($i = 1; $i <= 28; $i++) {
                                                                                    ?>
                                                                                    <td style="width:3%;height:60px;border-right:1px solid white;line-height:0px;text-align:center;">
                                                                                        <?php echo $i; ?>
                                                                                        <input type="hidden" name="hidden_id[]" value="<?php echo $i; ?>" />
                                                                                    </td>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </tr>
                                                                    </table>
                                                                    <br>



                                                                    <?php
                                                                    $mon = date('m');
                                                                    $year = date('Y');
                                                                    $dd = date('d');

                                                                    if ($mon == "01" || $mon == "03" || $mon == "05" || $mon == "07" || $mon == "08" || $mon == "10" || $mon == "12") {
                                                                        //$selec1 = "select propertyID from propertyTable where propertyName='$property' and customerID='$custID'";
                                                                        $res_data = $inventoryCont->getPropertyDetailsWithCustomerID($property, $customerId);
                                                                      
                                                                        // $que1 = mysql_query($selec1);
                                                                        foreach ($res_data as $ro1) {
                                                                            $idd = $ro1->propertyID;
                                                                            $res_data2 = $inventoryCont->getRoomDetails($idd);
                                                                            //  $selec2 = "select * from propertyRoom where propertyID='$idd'";
                                                                            //  $que2 = mysql_query($selec2);
                                                                            echo '<table class="calendar_data">';
                                                                            foreach ($res_data2 as $ro2) {
                                                                                $rid = $ro2->roomID;
                                                                                echo '<tr>';
                                                                                ?>

                                                                                <td style="width:110px;padding-left:5px;background:#ffdede;"><span class="caption-subject bold uppercase" style="color:black;"><center> <?php echo $ro2->roomType; ?> </center></span></td>

                                                                                <?php
                                                                                $rnam = $ro2->roomType;
                                                                                $res_data3 = $inventoryCont->getInventoryDetails($idd, $rid, $mon, $year);
                                                                                // $selec3 = "select * from manage_inventory where customerID='$custID' and property_id='$idd' and room_id='$rid' and month='$mon' and year='$year'";
                                                                                // $que3 = mysql_query($selec3);
                                                                                $numb1 = count($res_data3);
                                                                                if ($numb1 == "0") {

                                                                                    for ($j = 1; $j <= 31; $j++) {
                                                                                        $pass = $rnam . "~" . $j;
                                                                                        ?>


                                                                                        <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;<?php if ($j < $dd) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                                                                            <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro2->room_no; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" <?php if ($j < $dd) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                        </td>

                                                                                        <?php
                                                                                    }
                                                                                } else {
                                                                                    foreach ($res_data3 as $ro3) {

                                                                                        for ($j = 1; $j <= 31; $j++) {
                                                                                            $pass = $rnam . "~" . $j;
                                                                                            if ($ro3->$j == null) {
                                                                                                ?>


                                                                                                <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;<?php if ($j < $dd) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                                                                                    <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro2->room_no; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" <?php if ($j < $dd) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                                </td>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>

                                                                                                <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($ro3->$j == "0") { ?><?php if ($j < $dd) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $dd) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                                                                                    <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro3->$j; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" style="<?php if ($ro3->$j == "0") { ?><?php if ($j < $dd) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $dd) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                                </td>

                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?>


                                                                                <?php
                                                                                echo '</tr><tr><td style="height:10px;"></td></tr>';
                                                                                //echo '<table class="calendar_data"><tr><td style="height:1px;"></td></tr></table>';
                                                                            }
                                                                            echo '</table>';
                                                                        }
                                                                    } elseif ($mon == "04" || $mon == "06" || $mon == "09" || $mon == "11") {
                                                                        $res_data = $inventoryCont->getPropertyDetailsWithCustomerID($property, $customerId);
                                                                        // $selec1 = "select propertyID from propertyTable where propertyName='$property' and customerID='$custID'";
                                                                        // $que1 = mysql_query($selec1);
                                                                        foreach ($res_data as $ro1) {
                                                                            $idd = $ro1->propertyID;
                                                                            //  $selec2 = "select * from propertyRoom where propertyID='$idd' and customerID='$custID'";
                                                                            //  $que2 = mysql_query($selec2);
                                                                            $res_data2 = $inventoryCont->getRoomDetails($idd);
                                                                            echo '<table class="calendar_data">';
                                                                            foreach ($res_data2 as $ro2) {
                                                                                $rid = $ro2->roomID;
                                                                                echo '<tr>';
                                                                                ?>

                                                                                <td id="input_css<?php echo $pass; ?>" style="width:110px;padding-left:5px;background:#ffdede;"><span class="caption-subject bold uppercase" style="color:black;"><center> <?php echo $ro2->roomType; ?> </center></span></td>

                                                                                <?php
                                                                                $rnam = $ro2->roomType;
                                                                                // $selec3 = "select * from manage_inventory where property_id='$idd' and room_id='$rid' and month='$mon' and year='$year' and customerID='$custID'";
                                                                                // $que3 = mysql_query($selec3);
                                                                                $res_data3 = $inventoryCont->getInventoryDetails($idd, $rid, $mon, $year);
                                                                                $numb1 = count($res_data3);
                                                                                if ($numb1 == "0") {

                                                                                    for ($j = 1; $j <= 30; $j++) {
                                                                                        $pass = $rnam . "~" . $j;
                                                                                        ?>

                                                                                        <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;<?php if ($j < $dd) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                                                                            <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro2->room_no; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" <?php if ($j < $dd) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];             ?>
                                                                                        </td>
                                                                                        <?php
                                                                                    }
                                                                                } else {
                                                                                    foreach ($res_data3 as $ro3) {

                                                                                        for ($j = 1; $j <= 30; $j++) {
                                                                                            $pass = $rnam . "~" . $j;
                                                                                            if ($ro3->$j == null) {
                                                                                                ?>


                                                                                                <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;<?php if ($j < $dd) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                                                                                    <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro2->room_no; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" <?php if ($j < $dd) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                                </td>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>

                                                                                                <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($ro3->$j == "0") { ?><?php if ($j < $dd) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $dd) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                                                                                    <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro3->$j; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" style="<?php if ($ro3->$j == "0") { ?><?php if ($j < $dd) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $dd) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                                </td>

                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?>


                                                                                <?php
                                                                                echo '</tr><tr><td style="height:10px;"></td></tr>';
                                                                            }
                                                                            echo '</table>';
                                                                        }
                                                                    } elseif ($mon == "02") {
                                                                        $res_data = $inventoryCont->getPropertyDetailsWithCustomerID($property, $customerId);
                                                                        // $selec1 = "select propertyID from propertyTable where propertyName='$property' and customerID='$custID'";
                                                                        // $que1 = mysql_query($selec1);
                                                                        foreach ($res_data as $ro1) {
                                                                            $idd = $ro1->propertyID;
                                                                            // $selec2 = "select * from propertyRoom where propertyID='$idd' and customerID='$custID'";
                                                                            // $que2 = mysql_query($selec2);
                                                                            $res_data2 = $inventoryCont->getRoomDetails($idd);
                                                                            echo '<table class="calendar_data">';
                                                                            foreach ($res_data2 as $ro2) {
                                                                                $rid = $ro2->roomID;
                                                                                echo '<tr>';
                                                                                ?>

                                                                                <td class="input_css<?php echo $j; ?>" style="width:110px;padding-left:5px;background:#ffdede;"><span class="caption-subject bold uppercase" style="color:black;"><center> <?php echo $ro2['roomType']; ?> </center></span></td>

                                                                                <?php
                                                                                $rnam = $ro2->roomType;
                                                                                $res_data3 = $inventoryCont->getInventoryDetails($idd, $rid, $mon, $year);
                                                                                //  $selec3 = "select * from manage_inventory where property_id='$idd' and room_id='$rid' and month='$mon' and year='$year' and customerID='$custID'";
                                                                                // $que3 = mysql_query($selec3);
                                                                                $numb1 = count($res_data3);
                                                                                if ($numb1 == "0") {

                                                                                    for ($j = 1; $j <= 28; $j++) {
                                                                                        $pass = $rnam . "~" . $j;
                                                                                        ?>

                                                                                        <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;<?php if ($j < $dd) { ?>background:#efefef; <?php } else { ?>background:white; <?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                                                                            <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro2->room_no; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" <?php if ($j < $dd) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                        </td>
                                                                                        <?php
                                                                                    }
                                                                                } else {
                                                                                    while ($ro3 = mysql_fetch_array($que3)) {

                                                                                        for ($j = 1; $j <= 28; $j++) {
                                                                                            $pass = $rnam . "~" . $j;
                                                                                            if ($ro3->$j == null) {
                                                                                                ?>	
                                                                                                <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;<?php if ($j < $dd) { ?>background:#efefef; <?php } else { ?>background:white;<?php } ?>border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;">
                                                                                                    <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro2->room_no; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" <?php if ($j < $dd) { ?> style="background:#efefef;color:black;opacity:0.5;" <?php } else { ?>style="background:white;"<?php } ?> <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                                </td>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>

                                                                                                <td id="input_css<?php echo $pass; ?>" style="width:3%;height:25px;border:1px solid black;font-size:11px;padding-top:10px;padding-bottom:10px;line-height:0px;text-align:center;<?php if ($ro3[$j] == "0") { ?><?php if ($j < $dd) { ?>color:black;opacity:0.5;background:#E26A6A;<?php } else { ?>background:#E26A6A;<?php } ?><?php } else { ?><?php if ($j < $dd) { ?>background:#efefef;color:black; <?php } else { ?>background:white;<?php } ?><?php } ?>">
                                                                                                    <input name="<?php echo $rnam . "~" . $idd; ?>" id="<?php echo $pass; ?>" type="text" value="<?php echo $ro3->$j; ?>" onchange="update_avail('<?php echo $pass; ?>')" class="input_css" style="<?php if ($ro3->$j == "0") { ?><?php if ($j < $dd) { ?>color:white;background:#E26A6A;<?php } else { ?>color:white;background:#E26A6A; <?php } ?><?php } else { ?><?php if ($j < $dd) { ?>background:#efefef;color:black;opacity:0.5; <?php } else { ?>color:black;background:white;<?php } ?><?php } ?>" <?php if ($j < $dd) { ?> readonly="" <?php } ?> /><?php //echo $fet1[$j];            ?>
                                                                                                </td>

                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?>


                                                                                <?php
                                                                                echo '</tr><tr><td style="height:10px;"></td></tr>';
                                                                            }
                                                                            echo '</table>';
                                                                        }
                                                                    }
                                                                    ?>


                                                                </div> --->


                                                            </div>
                                                        </div><br><br>
                                                      <!--  <input type="button" name="add_primary"  onclick="toggle();" value="Bulk Date Modification" style="background:#505050;border:none;color:white;height:60px;width:220px;font-weight:bold;" />
                                                        <div class="modify" id="newpost" style="display:none;"><br><br>
                                                            <form action="modify_inventory.php" method="post"> 
                                                                <center><div class="rate_modify1">
                                                                        <!--<h3 style="color:#5B9C64;font-size:25px;font-weight:bold;"> <center> <?php //echo $property;              ?> </center></h3><br>-->

                                                                     <!--   <center><div style="width:50%;">
                                                                                <font style="width:20%;color:#5B9C64;font-size:18px;font-weight:bold;">
                                                                                Select Room Type: 
                                                                                </font> &nbsp &nbsp
                                                                                <?php
                                                                                $res_data = $inventoryCont->getPropertyDetailsWithCustomerID($property, $customerId);
                                                                                //  $sel = "select propertyID from propertyTable where propertyName='$property' and customerID='$custID'";
                                                                                // $quer = mysql_query($sel);
                                                                                foreach ($res_data as $row) {
                                                                                    $id = $row->propertyID;
                                                                                    // $roosel = "select * from propertyRoom where propertyID='$id'";
                                                                                    $res_data2 = $inventoryCont->getRoomDetails($idd);
                                                                                    $rooquer = mysql_query($roosel);
                                                                                    echo "<select name='rooms' class='room_select'>";
                                                                                    foreach ($res_data2 as $roorow) {
                                                                                        ?>
                                                                                        <option><?php echo $roorow->roomType; ?></option>

                                                                                        <?php
                                                                                    }
                                                                                    echo "</select>";
                                                                                }
                                                                                ?>
                                                                            </div></center><br><br>

                                                                        <div class="newboxes">
                                                                            <input type="hidden" name="prop_id" value="<?php echo $id; ?>" />
                                                                            <center><table style="width:100%;">
                                                                                    <tr>
                                                                                        <td style="color:#5B9C64;font-size:18px;font-weight:bold;">
                                                                                            Bulk Modification :
                                                                                        </td>
                                                                                        <td style="color:#227298;font-size:18px;font-weight:bold;">
                                                                                            No. of Rooms <input type="text" name="multiple_day_price" class="rate_mod" />
                                                                                        </td>
                                                                                        <td style="color:#227298;font-size:18px;font-weight:bold;">
                                                                                            From: <input type="date" name="from_date" class="rate_date" />
                                                                                        </td>
                                                                                        <td style="color:#227298;font-size:18px;font-weight:bold;">
                                                                                            To: <input type="date" name="to_date" class="rate_date" />
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td>
                                                                                            <br><br><input type="submit" name="prop_submit2" value="Apply" class="prop_submit" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table></center>
                                                                        </div>
                                                                    </div></center>
                                                            </form>
                                                        </div> -->
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script >
    function update_avail(id_no) {

        var value_arr = id_no.split("~");
        var value = value_arr[1];
        var mon_year = document.getElementById('next_month_year').value;
        var data = document.getElementById(id_no).value;
        if (data == 0)
        {
            var color = 'background:#E26A6A;color:white;';
            var color1 = 'background:#E26A6A;color:white;border:1px solid black;';
        } else
        {
            var color = 'background:white;color:black;';
            var color1 = 'background:white;color:black;border:1px solid black;';
        }
        var field_name = document.getElementById(id_no).name;
        var all_value = value + "~" + field_name + "~" + data + "~" + mon_year;

        var update_css = 'input_css' + id_no;

        $.ajax({url: 'ajax.php',
            data: {action: all_value},
            type: 'post',
            success: function (output) {
                document.getElementById(id_no).style = color;
                document.getElementById(update_css).style = color1;
            }
        });
    }
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';   ?>
  <?php
        echo "<script >
				function next_month(navigt){
					
					var id_name =navigt+'_month_year';
					var data=document.getElementById(id_name).value;
					var	send_data=data + '~' + navigt + '~" . $property . "' ;
					   
					$.ajax({ url: 'ajax_lib/ajax_update-inventory.php',
						data: {date_time: send_data},
						type: 'post',
						success: function(output) {
							document.getElementById('new_calendar').innerHTML=output;
						}
					});
				} 
			</script>";
        ?>

        <?php
        echo "<script >
				function select_dd(){
					var data=document.getElementById('get_month').value;
					var send_data= data + '-" . $property . "';
					   
					$.ajax({ url: 'ajax_lib/ajax_update-inventory.php',
					data: {month_prop_name: send_data},
					type: 'post',
					success: function(output) {
						document.getElementById('new_calendar').innerHTML=output;
					}
				});
			}
			</script>";
        ?>
  <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>