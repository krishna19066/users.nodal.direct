<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$getHomeSlider = $propertyCont->GetHomepageSliderData($customerId);
$getHomeSliderdata = $getHomeSlider[0];
$reccnt = count($getHomeSlider);

$propertyContS3 = new propertyData();
$getHomeSliderS3 = $propertyContS3->S3GetHomepageSliderData($customerId);
$getHomeSliderdataS3 = $getHomeSliderS3[0];

$reccntS3 = count($getHomeSliderS3);
?>
<?php
$redirect_script_name = "manage-home-page-slider.php";

$pageTitle = 'Home Page - Desktop Slider';
/* -------------------------------- CustomerID Updation ------------------------------------------- */

$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);

$cloud_keyData = $cloud_keySel[0];
$bucket = $cloud_keyData->bucket;
//print_r($cloud_keyData);
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;

require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';


Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));

if ($_POST['action_register'] == "save") {

    @extract($_POST);
    $langua = $_POST['language'];
    if ($_FILES[image1][name] != "") {

        //$propertyName11 = getValidFileName($_REQUEST[hshead1]);
        // $img_name = $type . "-" . $propertyName11 . "-" . date("Y_m_d_H_i_s");
        // $product_image = upload_file($img_name, "image1", "images/slider");
        $timdat = date('Y-m-d');
        $timtim = date('H-i-s');
        $timestmp = $timdat . "_" . $timtim;
        $img_name = $timestmp;

        // db_query("insert into HomeSlider set lan_code='$langua',hshead1='$_REQUEST[hshead1]',hshead2='$_REQUEST[hshead2]',linktext='$_REQUEST[linktext]',linkURL='$_REQUEST[linkURL]',hsimg='$product_image',type='$type'");
        $ins = $propertyCont->InsertHomePageSlider($customerId, $langua, $img_name, $type);
        if ($customerId == 1) {
            \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "homepage-slider"));
        } else {
            \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/homepage-slider"));
        }

        echo '<script type="text/javascript">
                alert("Succesfuly added image");              
window.location = "manage-slider.php";
            </script>';

        exit;
    }
}
if ($_REQUEST['id'] == 'edit' && $_REQUEST['recordID'] != '') {
    $recordID = $_REQUEST['recordID'];
    $homeSlider = $propertyCont->HomeSliderWithID($recordID);
    $res = $homeSlider[0];
}
if ($_REQUEST['action_register'] == "update") {
    if ($_FILES[image1][name] != "") {
        $timdat = date('Y-m-d');
        $timtim = date('H-i-s');
        $timestmp = $timdat . "_" . $timtim;
        $img_name = $timestmp;
        if ($customerId == 1) {
            \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "homepage-slider"));
            $cond = ",hsimg='$img_name'";
        } else {
            \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/homepage-slider"));
            $cond = ",hsimg='$img_name'";
        }
        // \Cloudinary\Uploader::upload($_FILES["image1"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/homepage-slider"));
        // $cond = ",hsimg='$img_name'";
    }

    // $ins = "update HomeSlider set hsimg='$timestmp',hshead1='$hshead1',hshead2='$hshead2',linktext='$linktext',linkURL='$linkURL',type='$type' where slno='$_REQUEST[slno]'";
    // $quer = mysql_query($ins);
    $upd = $propertyCont->UpdateHomeSlider($cond);

    echo '<script type="text/javascript">
                alert("Succesfuly update image");              
window.location = "manage-slider.php";
            </script>';
    exit;
}
if ($_REQUEST['id'] == 'delete' && $_REQUEST['imagesID1'] != '') {

    $imagesID1 = $_REQUEST['imagesID1'];
    $det = $propertyCont->DeleteHomeSlider($imagesID1);
    echo '<script type="text/javascript">
                alert("Succesfuly update image");              
window.location = "manage-slider.php";
            </script>';
}
if ($type == 'home' && $id == 'edit') {
    $pageTitle = "Update Image in Home Page Slider";
} elseif ($type == 'home' && $id == '') {
    $pageTitle = "Add New Image in Home Page Slider";
}
//admin_header();
?>
<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>                          
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <script type='text/javascript'>
                    function ValidateSize(file) {
                        var FileSize = file.files[0].size / 1024 / 1024; // in MB
                        if (FileSize > 2) {
                            alert("Image size exceeds 2 MB! We can't accept! please choose less than 2 MB");
                            // var file = $(file).val(''); //for clearing with Jquery
                            var file = document.getElementById('file').value = '';
                        } else {

                        }
                    }
                </script>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Manage Home Page Slider</span>
                                            <span class="caption-helper">Manage your home page slider images</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Upload home page slider here!" data-original-title="Manage Home Page Slider" style="color:#7d6c0b;font-size:20px;"></span>

                                        </div>
                                    </div><br>
                                    <?php if ($bucket) { ?>
                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:200px;font-size:14px;" data-html="true"   onclick=" $('#add_photo2').slideToggle(); " data-step="2" data-intro="Click Here and add image and shoe home page slider!" value="Upload Image on AWS (S3) " /></span><br><br>
                                    <?php } else { ?>
                                        <span><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:200px;font-size:14px;" data-html="true"   onclick="$('#add_photo2').hide(); $('#add_photo1').slideToggle();" data-step="2" data-intro="Click Here and add image and shoe home page slider!" value="Upload Image on cloudinary " /></span>
                                    <?php } ?>
                                </div>
                                <!-- This code for the togle of the btn  -->

                                <?php if($bucket){ ?>
                                <!-- This is for AWS (S3) -->
                                <div class="portlet light bordered collapse" id="add_photo2" <?php if ($_REQUEST['id'] != 'edit') { ?> style="display:none;" <?php } ?> >	
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Upload Image on AWS (S3)</span>
                                        </div>
                                    </div><br>
                                    <div class="portlet-body form">
                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick=" $('#add_photo_aws').slideToggle();" data-step="2" data-intro="Click Here and add image and shoe home page slider!" value="Add Slider Image" /></span><br><br>
                                        <span style="float:left;margin-left: 198px;"> <a href="manage-slider-order.php"><button style="background:#1ba39c;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-top: -39px;" data-step="3" data-intro="If you want to change images order click here !">Order Change  </button></a></span>

                                    </div>

                                    <!---------------------------------------------------------------- Add Photo Section Starts AWS --------------------------------------------------------------------->
                                    <div class="portlet light bordered" id="add_photo_aws" <?php if ($_REQUEST['id'] != 'edit') { ?> style="display:none;" <?php } ?>>	
                                        <div class="portlet-body form">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-equalizer font-red-sunglo"></i>
                                                    <span class="caption-subject font-red-sunglo bold uppercase">AWS (S3) </span>
                                                </div>
                                            </div><br>

                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="S3-createObject.php" style="display:inline;" onsubmit="return validate_register_member_form();">

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Slider Heading 1 :	

                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="hshead1"  value="<?= $res->hshead1; ?>" required="" />
                                                            <span class="help-block"> Also an alt tag for image </span>
                                                        </div>
                                                    </div>

                                                 
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Slider Image
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <?php
                                                                if ($id = 'edit' && $recordID != '') {
                                                                    ?>
                                                                    <img class="img-responsive" src="//<?php echo $bucket; ?>.s3.amazonaws.com/<?php echo $res->hsimg; ?>">
                                                                <?php } else { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } ?>

                                                            </div>

                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="image1" data-max-size="2048" <?php if ($id != 'edit') { ?> required="" <?php } ?> id="file" onchange="ValidateSize(this)"> </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                <span style="font-size:12px; color: red;">Note:image size should be less than 2MB </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">

                                                                <?php
                                                                if ($id = 'edit' && $recordID != '') {
                                                                    ?>
                                                                    <input name="slno" type="hidden" value="<?= $recordID; ?>" />
                                                                    <input name="type" type="hidden" value="<?= $res->type; ?>" />
                                                                    <input name="action_register" type="hidden" value="update" />
                                                                    <input type="submit" class="btn green" name="s3update" class="button" id="submit" value="Update Image" />
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <input name="action_register" type="hidden" value="s3submit" />
                                                                    <input name="type" type="hidden" value="home" />
                                                                    <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Add Image on S3" />
                                                                    <?php
                                                                }
                                                                ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>


                            </div><!-- End AWS(S3) -->
                                <?php } else{ ?>

                            <!-- This is for cloudinary -->
                            <div class="portlet light bordered collapse" id="add_photo1" <?php if ($_REQUEST['id'] != 'edit') { ?> style="display:none;padding-bottom:280px;" <?php } else { ?><?php } ?>   >	
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-equalizer font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Upload Image on Cloudinary</span>
                                    </div>
                                </div><br>
                                <div class="portlet-body form">
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick=" $('#add_photo').slideToggle();" data-step="2" data-intro="Click Here and add image and shoe home page slider!" value="Add Slider Image" /></span><br><br>
                                    <span style="float:left;margin-left: 198px;"> <a href="manage-slider-order.php"><button style="background:#1ba39c;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-top: -39px;" data-step="3" data-intro="If you want to change images order click here !">Order Change  </button></a></span>
                                </div>
                                <div class="portlet light bordered" id="add_photo" <?php if ($_REQUEST['id'] != 'edit') { ?> style="display:none;" <?php } ?>>	
                                    <div class="portlet-body form">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase"> Cloudinary </span>
                                            </div>
                                        </div><br>
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <input type="hidden" value="<?php echo $lan; ?>" name="language">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Slider Heading 1 :	
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="hshead1"  value="<?= $res->hshead1; ?>" required="" />
                                                        <span class="help-block"> Also an alt tag for image </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Slider Heading 2:</label>
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="hshead2" value="<?= $res->hshead2; ?>"  />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Button Text</label>
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="linktext" value="<?= $res->linktext; ?>"  />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Button Link :</label>
                                                    <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="linkURL" value="<?= $res->linkURL; ?>"  />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Slider Image
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <?php
                                                            if ($id = 'edit' && $recordID != '') {
                                                                ?>
                                                                <img class="img-responsive" src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/w_323,h_250,c_fill/<?php if ($customerId != 1) { ?>reputize/<?php } ?>homepage-slider/<?php echo $res->hsimg; ?>.jpg">
                                                            <?php } else { ?>
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                            <?php } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image1" data-max-size="2048" <?php if ($id != 'edit') { ?> required="" <?php } ?> id="file" onchange="ValidateSize(this)"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            <span style="font-size:12px; color: red;">Note:image size should be less than 2MB </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--<div class="form-group">
                                                        <label for="exampleInputFile" class="col-md-3 control-label">Slider Image</label>
                                                        <div class="col-md-9">
                                                                <input type="file" name="image1" id="exampleInputFile">
                                                        </div>
                                                </div>-->
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">

                                                            <?php
                                                            if ($id = 'edit' && $recordID != '') {
                                                                ?>
                                                                <input name="slno" type="hidden" value="<?= $recordID; ?>" />
                                                                <input name="type" type="hidden" value="<?= $res->type; ?>" />
                                                                <input name="action_register" type="hidden" value="update" />
                                                                <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update Image" />
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input name="action_register" type="hidden" value="save" />
                                                                <input name="type" type="hidden" value="home" />
                                                                <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Add Image" />
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>

                        </div><!-- End cloudinary -->
                                <?php } ?>
                        <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                    </div>

                    <?php
//  $qry1 = "select * from $table_name where lan_code='$lan' and customerID='$custID'";
// $reccnt = NumRows(db_query($qry1));
// $qry = db_query($qry1);
//print_message(); 
                    ?>
                    <?php if ($bucket) { ?>
                        <div style="margin-top:50px;">
                            <?php
                            if ($reccntS3 > 0) {
                                $cnt = 1;
                                $sl = $start + 1;
                                foreach ($getHomeSliderS3 as $dataRes) {
                                    //$dataRes = ms_htmlentities_decode($dataRes);
                                    if ($sl % 2 != 0) {
                                        $color = "#ffffff";
                                    } else {
                                        $color = "#f6f6f6";
                                    }
                                    if ($cnt == "1") {
                                        $color = "#32c5d2";
                                    }
                                    if ($cnt == "2") {
                                        $color = "#3598dc";
                                    }
                                    if ($cnt == "3") {
                                        $color = "#36D7B7";
                                    }
                                    if ($cnt == "4") {
                                        $color = "#5e738b";
                                    }
                                    if ($cnt == "5") {
                                        $color = "#1BA39C";
                                    }
                                    if ($cnt == "6") {
                                        $color = "#32c5d2";
                                    }
                                    if ($cnt == "7") {
                                        $color = "#578ebe";
                                    }
                                    if ($cnt == "8") {
                                        $color = "#8775a7";
                                    }
                                    if ($cnt == "9") {
                                        $color = "#E26A6A";
                                    }
                                    if ($cnt == "10") {
                                        $color = "#29b4b6";
                                    }
                                    if ($cnt == "11") {
                                        $color = "#4B77BE";
                                    }
                                    if ($cnt == "12") {
                                        $color = "#c49f47";
                                    }
                                    if ($cnt == "13") {
                                        $color = "#67809F";
                                    }
                                    if ($cnt == "14") {
                                        $color = "#8775a7";
                                    }
                                    if ($cnt == "15") {
                                        $color = "#73CEBB";
                                    }
                                    ?>
                                    <div class="col-md-3 " style="width:385px;">
                                        <!-- BEGIN Portlet PORTLET-->
                                        <div class="portlet box" style="background:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-image"></i><a href="//<?php echo $bucket; ?>.s3.amazonaws.com/<?= $dataRes->hsimg; ?>" target="_blank" style="color:#fff;">View Image</a> 
                                                </div>

                                                <div class="actions">
                                                    <a href="manage-slider.php?id=edit&recordID=<?= $dataRes->slno; ?>&type=<?= $dataRes->type; ?>" class="btn btn-default btn-sm">
                                                        <i class="fa fa-plus"></i> Edit 
                                                    </a>											
                                                    <a href="manage-slider.php?id=delete&imagesID1=<?= $dataRes->slno ?>&type=<?= $dataRes->type; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-default btn-sm">
                                                        <i class="fa fa-pencil"></i> Delete 
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="" style="height:auto" data-handle-color="red">
                                                    <img class="img-responsive" src="//<?php echo $bucket; ?>.s3.amazonaws.com/<?= $dataRes->hsimg; ?>" width="100%" style="height: 150px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $cnt++;
                                }
                                ?>
                                <?php
                            } else {
                                ?>
                                <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                    <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } else { ?>
                        <div style="margin-top:50px;">
                            <?php
                            if ($reccnt > 0) {
                                $cnt = 1;
                                $sl = $start + 1;
                                foreach ($getHomeSlider as $dataRes) {
                                    //$dataRes = ms_htmlentities_decode($dataRes);
                                    if ($sl % 2 != 0) {
                                        $color = "#ffffff";
                                    } else {
                                        $color = "#f6f6f6";
                                    }
                                    if ($cnt == "1") {
                                        $color = "#32c5d2";
                                    }
                                    if ($cnt == "2") {
                                        $color = "#3598dc";
                                    }
                                    if ($cnt == "3") {
                                        $color = "#36D7B7";
                                    }
                                    if ($cnt == "4") {
                                        $color = "#5e738b";
                                    }
                                    if ($cnt == "5") {
                                        $color = "#1BA39C";
                                    }
                                    if ($cnt == "6") {
                                        $color = "#32c5d2";
                                    }
                                    if ($cnt == "7") {
                                        $color = "#578ebe";
                                    }
                                    if ($cnt == "8") {
                                        $color = "#8775a7";
                                    }
                                    if ($cnt == "9") {
                                        $color = "#E26A6A";
                                    }
                                    if ($cnt == "10") {
                                        $color = "#29b4b6";
                                    }
                                    if ($cnt == "11") {
                                        $color = "#4B77BE";
                                    }
                                    if ($cnt == "12") {
                                        $color = "#c49f47";
                                    }
                                    if ($cnt == "13") {
                                        $color = "#67809F";
                                    }
                                    if ($cnt == "14") {
                                        $color = "#8775a7";
                                    }
                                    if ($cnt == "15") {
                                        $color = "#73CEBB";
                                    }
                                    ?>
                                    <div class="col-md-3 " style="width:385px;">
                                        <!-- BEGIN Portlet PORTLET-->
                                        <div class="portlet box" style="background:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-image"></i><a href="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/<?php if ($customerId != 1) { ?>reputize/<?php } ?>homepage-slider/<?php echo $dataRes->hsimg; ?>.jpg" target="_blank" style="color:#fff;">View Image</a> 
                                                </div>

                                                <div class="actions">
                                                    <a href="manage-slider.php?id=edit&recordID=<?= $dataRes->slno; ?>&type=<?= $dataRes->type; ?>" class="btn btn-default btn-sm">
                                                        <i class="fa fa-plus"></i> Edit 
                                                    </a>											
                                                    <a href="manage-slider.php?id=delete&imagesID1=<?= $dataRes->slno ?>&type=<?= $dataRes->type; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-default btn-sm">
                                                        <i class="fa fa-pencil"></i> Delete 
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="" style="height:auto" data-handle-color="red">
                                                    <img class="img-responsive" src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/w_323,h_150,c_fill/<?php if ($customerId != 1) { ?>reputize/<?php } ?>homepage-slider/<?php echo $dataRes->hsimg; ?>.jpg" width="100%" style="height: 150px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $cnt++;
                                }
                                ?>
                                <?php
                            } else {
                                ?>
                                <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                    <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } ?>
                    <br><br>                       
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<!-- END FORM-->

</body>
</html>
<script type="text/javascript">
    if (RegExp('multipage', 'gi').test(window.location.search)) {
        introJs().start();
    }
</script>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>

<?php
admin_footer();
?>