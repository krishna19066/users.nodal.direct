<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$GalleryPageData = $ajaxclientCont->getGalleryData($customerId);
$pageData = $GalleryPageData[0];
$numb = count($GalleryPageData);
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$base_url = $cloud_keyData->website;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
       <?php echo ajaxCssHeader(); ?>     
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>        
        <div class="clearfix"> </div>

        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-equalizer font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Edit Gallery Page</span>

                                        <span class="caption-helper">Edit Gallery Page Here..</span>

                                        <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <a class="pull-right">
                                                        <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    </a>
                                                    <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_gallery" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag"/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_gallery" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag"/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ---------------------------------------------------->

                                <div class="portlet-body form">
                                    <form class="form-horizontal" name="formn" method="post" action="updatePage_content.php" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Heading 1
                                                    <span class="required"> * </span>
                                                </label>

                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="main_heading" value="<?php echo $pageData->main_heading; ?>" required />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Header Image
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                         <?php if ($pageData->header_img == '') { ?>
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                            <?php } else { ?>
                                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/gallery/<?php echo $pageData->header_img; ?>.jpeg" alt=""/>
                                                            <?php } ?>
														</div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image_header"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <?php
                                                        if ($numb == "0") {
                                                            ?>
                                                            <input type="hidden" name="typeofsave" value="newaddition" />
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <input type="hidden" name="typeofsave" value="updation" />
                                                            <?php
                                                        }
                                                        ?>
                                                        <input type="submit" name="gallerysave" class="btn green" id="submit" value="Save">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="quick-nav-overlay"></div>
       <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
         <?php echo ajaxJsFooter(); ?>
    </body>
</html>
