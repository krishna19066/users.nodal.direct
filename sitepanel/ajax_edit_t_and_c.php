<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getTermsAndConditionsPageData($customerId);
$terms = $clientPageData[0];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>    
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>


        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-equalizer font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Edit Terms And Conditions</span>
                                        <span class="caption-helper">Add/Edit Terms and Conditions Page Here..</span>
                                        <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <a class="pull-right">
                                                        <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <form class="form-horizontal" name="formn" method="post" action="../sitepanel/updatePage_content.php" enctype="multipart/form-data">											<input type="hidden" name="querystr" value="<?php echo $querystr; ?>" />
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Heading 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="heading" value="<?php echo $terms->heading; ?>" required />
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3"> Terms & Conditions Content
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control"  name="content" rows="6"><?php echo $terms->content; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="submit" name="edit_terms_and_conditions" class="btn green" id="submit" value="Save">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>					
                                    </form>
                                </div>																


                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                function team_mem(sln)
                {
                    var mydata = sln;
                    $.ajax({url: 'ajax_change_status.php',
                        data: {edit_team: mydata},
                        type: 'post',
                        success: function (output) {
                            $('#to_be_repl').html(output);
                        }
                    });
                }
            </script>

            <div class="quick-nav-overlay"></div>
            <?php echo ajaxJsFooter(); ?>
    </body>
</html>
