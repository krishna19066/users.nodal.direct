<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
//$cloud_data = new adminFunction();
$propID = $_REQUEST[recordID];
$roomID = $_REQUEST[roomID];
$id = $_REQUEST['id'];
$imagesID = $_REQUEST[imagesID1];

//$conect_cloud = $cloud_data->connectCloudinaryAccount($customerId);
$get_prop = $propertyCont->GetPropertyDataWithId($propID);
$get_room = $propertyCont->getRoomDetailData($propID, $roomID);
?>
<?php
$redirect_script_name = "edit-alt-tag.php";
@extract($_REQUEST);


//$propertyRes = getResult("propertyImages", " where imagesID='$imagesID1'");
$get_image = $propertyCont->GetRoomPhotoWithImageID($imagesID);
$propertyRes = $get_image[0];
if ($propertyRes->imagesID == "") {
    header("Location: manage-property.php");
    exit;
}

if ($_REQUEST[action_register] == "update" && $imagesID != "") {
    @extract($_REQUEST);
    //echo $imageAlt1;


    //updateTable("propertyImages", " imageAlt='$imageAlt1' where imagesID='$imagesID'");
    $upd_tag = $propertyCont->UpdateImageAltTag($imagesID,$imageAlt1);


    $_SESSION[session_message] = $record_type . ' has been updated successfully';
    header("Location:".$redirect_script_name);
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <br><br><br>
                                            <div class="portlet light bordered">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="icon-equalizer font-red-sunglo"></i>
                                                        <span class="caption-subject font-red-sunglo bold uppercase">Edit Room Photo</span>
                                                        <span class="caption-helper">Please Add New Photo</span>
                                                        <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                                    </div>
                                                </div>

                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="<?$_SERVER['PHP_SELF'];?>" style="display:inline;" onsubmit="return validate_register_member_form();">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Image Alt Tag
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="imageAlt1"  value="<?= $propertyRes->imageAlt; ?>"   />
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <input name="imagesID" type="hidden" value="<?= $propertyRes->imagesID; ?>" />
                                                                        <input name="action_register" type="hidden" value="update" />
                                                                        <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Edit Alt Tag" />

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                            </body>
                                            </html>








                                            <?php
                                            admin_footer();
                                            ?>