<?php
include "admin-function.php";

$propertyName = $_REQUEST['propertyName'];
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$propertyData = new inventoryData();
$discountData = new discountOffers();
$discount_res = $discountData->getDiscounts($customerId);
$prop_resData = $propertyData->getPropertyListing($customerId);
?>
<?php
if (isset($_POST['addDiscount'])) {
    $discountType = $_POST['discountType'];
    $discountName = $_POST['discountName'];
    $discountFrom = $_POST['discountFrom'];
    $discountTo = $_POST['discountTo'];
    $propName = $_POST['propName'];
    $roomName = implode(",", $_POST['roomName']);
    $discountPercent = $_POST['discountPercent'];
    $lastMinute = $_POST['lastMinute'];
    $earlyBird = $_POST['earlyBird'];
    $longStay = $_POST['longStay'];
   
   // $ins = "insert into tbl_discount(customerID,discountType,discountName,discountFrom,discountTo,propertyName,roomName,discountValue,lastMinute,earlybird,longStay) values('$custID','$discountType','$discountName','$discountFrom','$discountTo','$propName','$roomName','$discountPercent','$lastMinute','$earlyBird','$longStay')";
    //$quer = mysql_query($ins);
    $ins = $discountData->AddNewDiscount($customerId, $discountType, $discountName, $discountFrom, $discountTo, $propName, $roomName, $discountPercent, $lastMinute, $earlyBird, $longStay);
    echo '<script type="text/javascript">
                alert("Succesfuly added discount");              
window.location = "manage-discounts.php";
            </script>';

    exit;
}
if (isset($_POST['editDiscount'])) {
    $recID = $_POST['recordID'];
    $discountType = $_POST['discountType'];
    $discountName = $_POST['discountName'];
    $discountFrom = $_POST['discountFrom'];
    $discountTo = $_POST['discountTo'];
    $propName = $_POST['propName'];
    $roomName = implode(",", $_POST['roomName']);
    $discountPercent = $_POST['discountPercent'];
    $lastMinute = $_POST['lastMinute'];
    $earlyBird = $_POST['earlyBird'];
    $longStay = $_POST['longStay'];

    $upd = "update tbl_discount set discountType='$discountType',discountName='$discountName',discountFrom='$discountFrom',discountTo='$discountTo',propertyName='$propName',roomName='$roomName',discountValue='$discountPercent',lastMinute='$lastMinute',earlybird='$earlyBird',longStay='$longStay' where discountID='$recID' and customerID='$custID'";
    //$up_quer = mysql_query($upd);
    $upd = $discountData->UpdateDiscount($recID,$discountType,$discountName,$discountFrom,$discountTo,$propName,$roomName,$discountPercent,$lastMinute,$earlyBird,$longStay);
     echo '<script type="text/javascript">
                alert("Succesfuly update discount");              
window.location = "manage-discounts.php";
            </script>';

    exit;
}
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>
            <style>
                .ques_table tbody:before {
                    content: "-";
                    display: block;
                    line-height: 1em;
                    color: transparent;
                }
                .ques_table	tbody  tr {border-bottom: 1px solid rgba(0, 0, 0, 0.05); }
                tr:nth-child(odd){
                    background-color:#fff;
                }
                th {
                    padding:15px;
                    font-size:large;}
                td {
                    padding:10px;}
                </style>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                <script type="text/javascript">
                    $(function () {
                        $("input[name='discountType']").click(function () {

                            if ($("#radio-1-0").is(":checked")) {
                                $("#dvlastminute").show();
                            } else {
                                $("#dvlastminute").hide();
                            }
                            if ($("#radio-1-1").is(":checked")) {
                                $("#dvearlybird").show();
                            } else {
                                $("#dvearlybird").hide();
                            }
                            if ($("#radio-1-2").is(":checked")) {
                                $("#dvlongstay").show();
                            } else {
                                $("#dvlongstay").hide();
                            }
                        });
                    });
                </script>

                <!-- BEGIN THEME LAYOUT STYLES -->
                <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
                <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
                 <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE DISCOUNTS & OFFERS
                                    <small>View and Edit Discounts and Offers</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Revenue Management</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Discounts</span>
                            </li>
                        </ul>

                        <div id="top_menu_container">
                            <ul id="top_menu">
                                <a href="manage-discounts.php" style="text-decoration:none;"><li class="OurMenuActive">Manage Discounts</li></a>
                                <li onclick="load_sub_city_detail();">Add / Manage Discounts</li>
                            </ul>
                        </div>

                        <div id="cityCont">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 
                                                <a class="pull-right">
                                                <!--    <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_discount();"><i class="fa fa-plus"></i> &nbsp Add Discount</button> -->
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!isset($_POST['propSelect'])) {
                            if ($_SERVER['QUERY_STRING'] == true) {
                                $rec = $_REQUEST['recordID'];

                                //  $pro_sel = "select * from tbl_discount where discountID='$rec'";
                                //  $pro_quer = mysql_query($pro_sel);
                                $dis_res = $discountData->getDiscountWithId($rec);
                               
                                
                                foreach ($dis_res as $pro_row) {
                                    $property = $pro_row->propertyName;
                                    $room_nam = $pro_row->roomName;
                                    $room_dis_id = explode(',',$room_nam);
                                   // print_r($room_dis_id);
                                    $disc = $pro_row->discountName;
                                    $disc_typ = $pro_row->discountType;
                                    $disc_per = $pro_row->discountValue;
                                    $fro = $pro_row->discountFrom;
                                    $to = $pro_row->discountTo;
                                    $lastmin = $pro_row->lastMinute;
                                    $earlb = $pro_row->earlybird;
                                    $longst = $pro_row->longStay;
                                }
                                // $ro_sel = "select * from propertyRoom where roomID='$room_nam'";
                                // $ro_quer = mysql_query($ro_sel);
                                $result_room = $propertyData->getRoomDetailsWithRoomID($room_nam);
                               
                                foreach ($result_room as $res_room) {
                                   $room_act = $res_room->roomType;
                                }
                                ?>
                                <div class="portlet-body form" style="background: white;">
                                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                        <input type="hidden" name="recordID" value="<?php echo $rec; ?>" />
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Property Selected
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="propName" value="<?php echo $property; ?>" readonly="" >
                                                        <span class="help-block"> Selected Property Name</span>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Select Room
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <table>
                                                        <tr>
                                                            <?php
                                                          //  $selRoom = "select propertyID from propertyTable where propertyName='$property'";
                                                           // $querRoom = mysql_query($selRoom);
                                                              $result_prop = $propertyData->getPropertyWithNameData($property);
                                                           foreach ($result_prop as $prop) {
                                                            $ide = $prop->propertyID;
                                                            //  $sele = "select roomType,roomID from propertyRoom where propertyID='$ide'";
                                                            $room_data = $propertyCont->getRoomDetail($ide);
                                                            // $quer1 = mysql_query($sele);
                                                            foreach ($room_data as $r) {        ?>
                                                                    <td style="padding-left:50px;">
                                                                        <div class="mt-checkbox-inline">
                                                                            <label class="mt-checkbox">
                                                                                <input type="checkbox" name="roomName[]" id="inlineCheckbox21" value="<?php echo $r->roomID; ?>" <?php
                                                                                if (in_array($r->roomID, $room_dis_id)) {
                                                                                    echo "checked";
                                                                                }
                                                                                ?>> <?php echo $r->roomType; ?>
                                                                                    <span></span>
                                                                            </label>

                                                                        </div>
                                                                    </td>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> Discount Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="discountName" value="<?php echo $disc; ?>">
                                                        <span class="help-block"> Name of Discount</span>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3"> Discount Type
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="radio-list">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <center><input type="radio" name="discountType" value="basicdeal" required <?php
                                                                        if ($disc_typ == "basicdeal") {
                                                                            echo "checked";
                                                                        }
                                                                        ?> />
                                                                        <center><span class="help-block"> Basic Deal</span>
                                                                            </td>

                                                                            <td style="padding-left:50px;">
                                                                                <center><input type="radio" id="radio-1-0" name="discountType" value="lastminute" <?php
                                                                                    if ($disc_typ == "lastminute") {
                                                                                        echo "checked";
                                                                                    }
                                                                                    ?> />
                                                                                    <center><span class="help-block"> Last Minute</span>
                                                                                        </td>

                                                                                        <td style="padding-left:50px;">
                                                                                            <center><input type="radio" id="radio-1-1" name="discountType" value="earlybird" <?php
                                                                                                if ($disc_typ == "earlybird") {
                                                                                                    echo "checked";
                                                                                                }
                                                                                                ?> />
                                                                                                <center><span class="help-block"> Early Bird</span>
                                                                                                    </td>

                                                                                                    <td style="padding-left:50px;">
                                                                                                        <center><input type="radio" id="radio-1-2" name="discountType" value="longstay" <?php
                                                                                                            if ($disc_typ == "longstay") {
                                                                                                                echo "checked";
                                                                                                            }
                                                                                                            ?> />
                                                                                                            <center><span class="help-block"> Long Stay</span>
                                                                                                                </td>
                                                                                                                </tr>

                                                                                                                </table>
                                                                                                                </div>
                                                                                                                </div>
                                                                                                                </div>

                                                                                                                <div class="form-group">
                                                                                                                    <label class="control-label col-md-3"> Discount Percentage
                                                                                                                        <span class="required"> * </span>
                                                                                                                    </label>
                                                                                                                    <div class="col-md-4">
                                                                                                                        <input type="text" class="form-control" name="discountPercent" value="<?php echo $disc_per; ?>">
                                                                                                                            <span class="help-block"> Provide Discount Percentage</span>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group">
                                                                                                                    <label class="control-label col-md-3"> Discount From
                                                                                                                        <span class="required"> * </span>
                                                                                                                    </label>
                                                                                                                    <div class="col-md-4">
                                                                                                                        <input type="date" class="form-control" name="discountFrom" value="<?php echo $fro; ?>">
                                                                                                                            <span class="help-block"> Provide Discount Initiation Date</span>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group">
                                                                                                                    <label class="control-label col-md-3"> Discount To
                                                                                                                        <span class="required"> * </span>
                                                                                                                    </label>
                                                                                                                    <div class="col-md-4">
                                                                                                                        <input type="date" class="form-control" name="discountTo" value="<?php echo $to; ?>">
                                                                                                                            <span class="help-block"> Provide Discount Conclusion Date</span>
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                                <div class="form-group last">
                                                                                                                    <label class="control-label col-md-3"> Discount Specifications
                                                                                                                        <span class="required"> * </span>
                                                                                                                    </label>
                                                                                                                    <div class="col-md-9">
                                                                                                                        <div id="dvlastminute" style="<?php if ($disc_typ == "lastminute") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                                                                                            <font style="color:#2d89b7;">CheckIn within following Days of Booking *</font> &nbsp &nbsp
                                                                                                                            <input type="text" name="lastMinute" class="discountText" value="<?php echo $lastmin; ?>" />
                                                                                                                        </div>

                                                                                                                        <div id="dvearlybird" style="<?php if ($disc_typ == "earlybird") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                                                                                            <font style="color:#2d89b7;">Check in before following Days of booking *</font> &nbsp &nbsp
                                                                                                                            <input type="text" name="earlyBird" class="discountText" value="<?php echo $earlb; ?>" />
                                                                                                                        </div>

                                                                                                                        <div id="dvlongstay" style="<?php if ($disc_typ == "longstay") { ?>display:block; <?php } else { ?>display:none; <?php } ?>"> 
                                                                                                                            <font style="color:#2d89b7;">Need to stay following days * <font> &nbsp &nbsp
                                                                                                                                    <input type="text" name="longStay" class="discountText" value="<?php echo $longst; ?>" /><br><br><br>
                                                                                                                                                </div>
                                                                                                                                                </div>
                                                                                                                                                </div>

                                                                                                                                                <div class="form-actions">
                                                                                                                                                    <div class="row">
                                                                                                                                                        <div class="col-md-offset-3 col-md-9">
                                                                                                                                                            <input type="submit" name="editDiscount" class="btn green" id="submit" value="Edit Discount">
                                                                                                                                                                <a href="manage-discounts.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                </div>
                                                                                                                                                </form>
                                                                                                                                                </div>
                                                                                                                                                <?php
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        ?>


                                                                                                                                        <?php
                                                                                                                                        if (isset($_POST['propSelect'])) {
                                                                                                                                            $property = $_POST['propertyName'];
                                                                                                                                            //echo $property;
                                                                                                                                            if ($property == "") {
                                                                                                                                                Header("location:add-new-discount.php");
                                                                                                                                                $_SESSION['propError'] = "Please Select A Valid Property Name..";
                                                                                                                                            }
                                                                                                                                            ?>
                                                                                                                                            <div class="portlet-body form" style="background: white;">
                                                                                                                                                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                                                                                                                                    <div class="form-body">
                                                                                                                                                        <div class="form-group">
                                                                                                                                                            <label class="control-label col-md-3"> Property Selected
                                                                                                                                                                <span class="required"> * </span>
                                                                                                                                                            </label>
                                                                                                                                                            <div class="col-md-4">
                                                                                                                                                                <input type="text" class="form-control" name="propName" value="<?php echo $property; ?>" readonly="" >
                                                                                                                                                                    <span class="help-block"> Selected Property Name</span>
                                                                                                                                                            </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="form-group last">
                                                                                                                                                            <label class="control-label col-md-3">Select Room
                                                                                                                                                                <span class="required"> * </span>
                                                                                                                                                            </label>
                                                                                                                                                            <div class="col-md-9">
                                                                                                                                                                <table>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <?php
                                                                                                                                                                        //  $selRoom = "select propertyID from propertyTable where propertyName='$property'";
                                                                                                                                                                        // $querRoom = mysql_query($selRoom);
                                                                                                                                                                        $result_prop = $propertyData->getPropertyWithNameData($property);
                                                                                                                                                                        foreach ($result_prop as $prop) {
                                                                                                                                                                            $ide = $prop->propertyID;
                                                                                                                                                                            //  $sele = "select roomType,roomID from propertyRoom where propertyID='$ide'";
                                                                                                                                                                            $room_data = $propertyCont->getRoomDetail($ide);
                                                                                                                                                                            // $quer1 = mysql_query($sele);
                                                                                                                                                                            foreach ($room_data as $r) {
                                                                                                                                                                                ?>
                                                                                                                                                                                <td style="padding-left:50px;">
                                                                                                                                                                                    <div class="mt-checkbox-inline">
                                                                                                                                                                                        <label class="mt-checkbox">
                                                                                                                                                                                            <input type="checkbox" name="roomName[]" id="inlineCheckbox21" value="<?php echo $r->roomID; ?>"> <?php echo $r->roomType; ?>
                                                                                                                                                                                                <span></span>
                                                                                                                                                                                        </label>

                                                                                                                                                                                    </div>
                                                                                                                                                                                </td>
                                                                                                                                                                                <?php
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                        ?>
                                                                                                                                                                    </tr>
                                                                                                                                                                </table>
                                                                                                                                                            </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="form-group">
                                                                                                                                                            <label class="control-label col-md-3"> Discount Name
                                                                                                                                                                <span class="required"> * </span>
                                                                                                                                                            </label>
                                                                                                                                                            <div class="col-md-4">
                                                                                                                                                                <input type="text" class="form-control" name="discountName">
                                                                                                                                                                    <span class="help-block"> Name of Discount</span>
                                                                                                                                                            </div>
                                                                                                                                                        </div>

                                                                                                                                                        <div class="form-group last">
                                                                                                                                                            <label class="control-label col-md-3"> Discount Type
                                                                                                                                                                <span class="required"> * </span>
                                                                                                                                                            </label>
                                                                                                                                                            <div class="col-md-9">
                                                                                                                                                                <div class="radio-list">
                                                                                                                                                                    <table>
                                                                                                                                                                        <tr>
                                                                                                                                                                            <td>
                                                                                                                                                                                <center><input type="radio" name="discountType" value="basicdeal" required />
                                                                                                                                                                                    <center><span class="help-block"> Basic Deal</span>
                                                                                                                                                                                        </td>

                                                                                                                                                                                        <td style="padding-left:50px;">
                                                                                                                                                                                            <center><input type="radio" id="radio-1-0" name="discountType" value="lastminute" />
                                                                                                                                                                                                <center><span class="help-block"> Last Minute</span>
                                                                                                                                                                                                    </td>

                                                                                                                                                                                                    <td style="padding-left:50px;">
                                                                                                                                                                                                        <center><input type="radio" id="radio-1-1" name="discountType" value="earlybird" />
                                                                                                                                                                                                            <center><span class="help-block"> Early Bird</span>
                                                                                                                                                                                                            </td>

                                                                                                                                                                                                            <td style="padding-left:50px;">
                                                                                                                                                                                                            <center><input type="radio" id="radio-1-2" name="discountType" value="longstay" />
                                                                                                                                                                                                            <center><span class="help-block"> Long Stay</span>
                                                                                                                                                                                                            </td>
                                                                                                                                                                                                            </tr>
                                                                                                                                                                                                            </table>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <label class="control-label col-md-3"> Discount Percentage
                                                                                                                                                                                                            <span class="required"> * </span>
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                            <div class="col-md-4">
                                                                                                                                                                                                            <input type="text" class="form-control" name="discountPercent">
                                                                                                                                                                                                            <span class="help-block"> Provide Discount Percentage</span>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <label class="control-label col-md-3"> Discount From
                                                                                                                                                                                                            <span class="required"> * </span>
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                            <div class="col-md-4">
                                                                                                                                                                                                            <input type="date" class="form-control" name="discountFrom">
                                                                                                                                                                                                            <span class="help-block"> Provide Discount Initiation Date</span>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <label class="control-label col-md-3"> Discount To
                                                                                                                                                                                                            <span class="required"> * </span>
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                            <div class="col-md-4">
                                                                                                                                                                                                            <input type="date" class="form-control" name="discountTo">
                                                                                                                                                                                                            <span class="help-block"> Provide Discount Conclusion Date</span>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div class="form-group last">
                                                                                                                                                                                                            
                                                                                                                                                                                                            <div class="col-md-9">
                                                                                                                                                                                                            <div id="dvlastminute" style="display:none;"> 
                                                                                                                                                                                                              <label class="control-label col-md-3"> Discount Specifications
                                                                                                                                                                                                            <span class="required"> * </span>
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                            <font style="color:#2d89b7;">CheckIn within following Days of Booking *</font> &nbsp &nbsp
                                                                                                                                                                                                            <input type="text" name="lastMinute" class="discountText" />
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div id="dvearlybird" style="display:none;"> 
                                                                                                                                                                                                              <label class="control-label col-md-3"> Discount Specifications
                                                                                                                                                                                                            <span class="required"> * </span>
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                            <font style="color:#2d89b7;">Check in before following Days of booking *</font> &nbsp &nbsp
                                                                                                                                                                                                            <input type="text" name="earlyBird" class="discountText" />
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div id="dvlongstay" style="display:none;"> 
                                                                                                                                                                                                          <label class="control-label col-md-3"> Discount Specifications
                                                                                                                                                                                                            <span class="required"> * </span>
                                                                                                                                                                                                            </label>
                                                                                                                                                                                                            <font style="color:#2d89b7;">Need to stay following days * <font> &nbsp &nbsp
                                                                                                                                                                                                            <input type="text" name="longStay" class="discountText" /><br><br><br>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>

                                                                                                                                                                                                            <div class="form-actions">
                                                                                                                                                                                                            <div class="row">
                                                                                                                                                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                                                                                                                                            <input type="submit" name="addDiscount" class="btn green" id="submit" value="Add Discount">
                                                                                                                                                                                                            <a href="manage-discounts.php"><input type="button" name="back" class="btn red" value="Back" /></a>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </form>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                                            <?php
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                        ?>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </div>

                                                                                                                                                                                                        </body>
                                                                                                                                                                                                        </html>

                                                                                                                                                                                                        <script>
                                                                                                                                                                                                        $('#top_menu li').click(function () {
                                                                                                                                                                                                        $('#top_menu li').removeClass('OurMenuActive');
                                                                                                                                                                                                        $(this).addClass('OurMenuActive');
                                                                                                                                                                                                        });
                                                                                                                                                                                                        </script>

                                                                                                                                                                                                        <script>
                                                                                                                                                                                                        function load_add_discount() {
                                                                                                                                                                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                                                                                                                                                        $('#cityCont').html(loadng);


                                                                                                                                                                                                        //var replace = 'btn_div' + but;

                                                                                                                                                                                                        $.ajax({url: 'ajax_lib/ajax_add_discount.php',
                                                                                                                                                                                                        type: 'post',
                                                                                                                                                                                                        success: function (output) {
                                                                                                                                                                                                        // alert(output);
                                                                                                                                                                                                        $('#cityCont').html(output);
                                                                                                                                                                                                        }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        }
                                                                                                                                                                                                        </script>
                                                                                                                                                                                                        <script>
                                                                                                                                                                                                        function load_property_type() {
                                                                                                                                                                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                                                                                                                                                                        $('#cityCont').html(loadng);


                                                                                                                                                                                                        //var replace = 'btn_div' + but;

                                                                                                                                                                                                        $.ajax({url: 'ajax_lib/ajax_property_type.php',
                                                                                                                                                                                                        type: 'post',
                                                                                                                                                                                                        success: function (output) {
                                                                                                                                                                                                        // alert(output);
                                                                                                                                                                                                        $('#cityCont').html(output);
                                                                                                                                                                                                        }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        }
                                                                                                                                                                                                        </script>

                                                                                                                                                                                                        <script>
                                                                                                                                                                                                        function deleteRecod(delData) {
                                                                                                                                                                                                        $.ajax({url: 'deleteData.php',
                                                                                                                                                                                                        data: {deleteData: delData},
                                                                                                                                                                                                        type: 'post',
                                                                                                                                                                                                        success: function (output) {
                                                                                                                                                                                                        // alert(output);
                                                                                                                                                                                                        alert('Data Deleted Successfully');
                                                                                                                                                                                                        location.reload();
                                                                                                                                                                                                        }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        }
                                                                                                                                                                                                        </script>
                                                                                                                                                                                                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                                                                                                                                                                                                        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                                                                                                                                                                                                        <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                                                                                                                                                                                                        <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                                                                                                                                                                                                        <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                                                                                                                                                                                                        <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                                                                                                                                                                                                        <!-- END PAGE LEVEL PLUGINS -->
                                                                                                                                                                                                        <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                                                                                                                                                                                        <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
                                                                                                                                                                                                        <!-- END PAGE LEVEL SCRIPTS -->
                                                                                                                                                                                                                                                        <?php
                                                                                                                                                                                                                                                        admin_footer();
                                                                                                                                                                                                                                                        ?>        
                                                                                                                                                                                                        </div>	