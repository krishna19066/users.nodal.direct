<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$res = $cityListings1[0];
//$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];

$cloud_cdnName = $cloud_keyData->cloud_name;
$user_email = $cloud_keyData->email;
$hotel_name = $cloud_keyData->companyName;
$phone = $cloud_keyData->phone;
$website = $cloud_keyData->website;

$property_data = $propertyCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;

$super_admin_1 = $propertyCont->GetSupperAdminData($customerId);
$super_admin = $super_admin_1[0];
$status = $super_admin->status;
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> View - <span style="color:#e44787;">Invoice</span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Billing Account Information</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><h3>View Invoice</h3></span>                 
                                        <span style="float:right;"> <a href="#"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>
                                        <!--<span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Video Gallery </button></a></span>-->
                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row"> 
                                            <div class="col-lg-12 col-md-612 col-sm-12 col-xs-12">
                                                <h3>Billing Information</h3>
                                                <p></p>  
                                                <?php 
                                                $billing_data = $propertyCont->getInvoiceData($customerId);
                                                if($billing_data){
                                                foreach($billing_data as $dt){
                                                ?>
                             
                                                    <div class="notice notice-info">
                                                        <strong>GENERATE INVOICE:</strong> <?php 
                                                        
                                                        $date = $dt->recieved_date;
                                                         echo date("jS  M, Y", strtotime($date));
                                                        ?><br/>
                                                        <span style="font-size:10px;">This is the legal name of your business. It will appear on all invoices and emails sent to your customers.</span>
                                                        <span style="float:right;"><a href="http://www.stayondiscount.com/superadmin/bills/<?php echo $dt->url; ?>" target="_blank" class="btn btn-primary">View Invoice</a></span>
                                                    </div>
                                                <?php } ?>
                                                <?php }else{ ?>
                                                <span><h3>You have no invoices.</h3></span>
                                                <?php } ?>  
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>                                    
                                </div>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .notice {
            padding: 15px;
            background-color: #fafafa;
            border-left: 6px solid #7f7f84;
            margin-bottom: 10px;
            -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
        }
        .notice-sm {
            padding: 10px;
            font-size: 80%;
        }
        .notice-lg {
            padding: 35px;
            font-size: large;
        }
        .notice-success {
            border-color: #80D651;
        }
        .notice-success>strong {
            color: #80D651;
        }
        .notice-info {
            border-color: #45ABCD;
        }
        .notice-info>strong {
            color: #45ABCD;
        }
        .notice-warning {
            border-color: #FEAF20;
        }
        .notice-warning>strong {
            color: #FEAF20;
        }
        .notice-danger {
            border-color: #d73814;
        }
        .notice-danger>strong {
            color: #d73814;
        }
    </style>


    <?php echo ajaxJsFooter(); ?>
</body>
</html>