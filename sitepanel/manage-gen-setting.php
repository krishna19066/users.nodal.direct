<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$cityCont = new propertyData();
$fun_resData = $cityCont->get_Cloud_AdminDetails($customerId);
//print_r($resData);
$resData = $fun_resData[0];

$id = $_GET['id'];
//------------Update Base Url ------------------------------->
if (isset($_POST['update_baseurl'])) {
    $basicUrl = $_REQUEST['base_url'];
    $reservationNo = $_REQUEST['reservation_no'];
    $center_email = $_REQUEST['center_email'];
    $fb_link = $_REQUEST['fb_link'];
    $tw_link = $_REQUEST['tw_link'];
    $insta_link = $_REQUEST['insta_link'];
    $linkedin_link = $_REQUEST['linkedin_link'];
    $trip_advisor = $_REQUEST['trip_advisor'];
    $google_link = $_REQUEST['google_link'];
    $upd = $cityCont->UpdateBaseUrl($customerId, $basicUrl, $reservationNo,$center_email,$fb_link,$tw_link,$insta_link,$linkedin_link,$trip_advisor,$google_link);
    header("Location: manage-gen-setting.php");
    exit;
}
//------------Upload Logo ------------------------------->
if (isset($_POST['submit_pic'])) {
    $cloud_keySel = $cityCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));

    $file = $_FILES['image_org']['name'];
    $alt_tag = $_POST['imageAlt1'];
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/logo"));
    $propertyImg = $cityCont->UpdateLogo($customerId, $timestmp, $alt_tag);
    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "manage-gen-setting.php";
            </script>';
}
//-------------End Upload Logo------------------------>
//------------Upload Fevicon Icon ------------------------------->
if (isset($_POST['submit_pic_fevicon'])) {
    $cloud_keySel = $cityCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));

    $file = $_FILES['image_org']['name'];
    // $alt_tag = $_POST['imageAlt1']; 
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/fevicon"));
    $propertyImg = $cityCont->UploadFevicon($customerId, $timestmp);
    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "manage-gen-setting.php";
            </script>';
}
?>

<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>

            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE Gen Setting
                                    <small>Update Logo,base url etc</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Gen Setting</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Base Url</span>
                            </li>
                        </ul>

                        <div id="top_menu_container">
                            <ul id="top_menu">
                                <a href="manage-gen-setting.php" style="text-decoration:none;"><li class="OurMenuActive">Base Url</li></a>
                                <!-- <li onclick="load_food_menu();">Food Menu</li>-->
                                <li onclick="load_upload_logo();">Update Logo</li>
                                <li onclick="load_fevicon();">Update Fevicon </li>
                            </ul>
                        </div>

                        <div id="cityCont">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Update Base URL
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="base_url"  value="<?= $resData->website; ?>" placeholder="Enter Your Domain Name" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Update Center Reservation No.
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="reservation_no"  value="<?= $resData->reservationNo; ?>" placeholder="Enter Your Mobile No." required />
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Update Center Email.
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="center_email"  value="<?= $resData->center_email; ?>" placeholder="Enter Your Email." required />
                                                </div>
                                            </div>

                                            <hr/>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Face Book  Link
                                                    <span class=""></span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="fb_link"  value="<?= $resData->fb_link; ?>" placeholder="Enter Your FB Link"/>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Google+  Link
                                                    <span class=""></span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="google_link"  value="<?= $resData->google_plus_link; ?>" placeholder="Enter Your Google+ Link"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Instagram  Link
                                                    <span class=""></span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="insta_link"  value="<?= $resData->insta_link; ?>" placeholder="Enter Your Instagram Link" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Twitter  Link
                                                    <span class=""></span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="tw_link"  value="<?= $resData->tw_link; ?>" placeholder="Enter Your Twitter Link"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Linkedin   Link
                                                    <span class=""></span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="linkedin_link"  value="<?= $resData->linkedin_link; ?>" placeholder="Enter Your Linkedin Link"/>
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="control-label col-md-3">Trip Advisor  Link
                                                    <span class=""></span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="trip_advisor"  value="<?= $resData->trip_advisor; ?>" placeholder="Enter Your Trip Advisor Link"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">                          
                                                    <input name="update_baseurl" type="hidden" value="update" />
                                                    <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update Base URL" />

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Portlet PORTLET-->
                </div>

            </div>
            </div>

        </body>
    </html>

    <script>
        $('#top_menu li').click(function () {
            $('#top_menu li').removeClass('OurMenuActive');
            $(this).addClass('OurMenuActive');
        });
    </script>

    <script>
        function load_upload_logo() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_upload-logo.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_fevicon() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_upload-fevicon.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_room_type() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_room_type.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_room_type() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_room_type.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_city() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_city.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_edit_city(cityid) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_edit_city.php',
                type: 'post',
                data: {cityid: cityid},
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_content_city(type_id) {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;
            // alert(cityid);

            $.ajax({url: 'ajax_lib/ajax_content_city.php',
                type: 'post',
                data: {type_id: type_id},
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_subcity() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add_subcity.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <script>
        function load_add_propertytype() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add-propertytype.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>

    <script>
        function load_add_roomtype() {
            var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
            $('#cityCont').html(loadng);


            //var replace = 'btn_div' + but;

            $.ajax({url: 'ajax_lib/ajax_add-roomtype.php',
                type: 'post',
                success: function (output) {
                    // alert(output);
                    $('#cityCont').html(output);
                }
            });
        }
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <?php
    admin_footer();
    ?>        
</div>	