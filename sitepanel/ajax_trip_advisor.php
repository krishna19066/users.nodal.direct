<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$staticPageCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$propertyId = $_POST['propId'];
$resData1 = $ajaxpropertyPhotoCont->GetPropertyDataWithId($propertyId);
$resData = $resData1[0];
?>
<html lang="en">
    <head>
         <?php echo ajaxCssHeader(); ?>
    </head>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header();
            ?>

            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> PROPERTY Add Visit Trip Advisor
                                <small>Update Visit Trip Advisor</small>
                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="welcome.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Add Visit Trip Advisor</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body ">
                                    <div style="width:100%;" class="clear"> 


                                        <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>

                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!---------------------------------------------------------------- Add Food Menu --------------------------------------------------------------------->

                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row">
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/add_property.php" style="display:inline;">
                                                    <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Visit Trip Advisor 
                                                                <span >  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <textarea class="form-control" name="visirTrip_advisor" rows="7" placeholder="Enter Your Trip Advisor Url"> <?= $resData->visirTrip_advisor; ?></textarea>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Visit 2 Trip Advisor 
                                                                <span >  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <textarea class="form-control" name="visirTrip_advisor_2" rows="7" placeholder="Enter Your Trip Advisor Url"> <?= $resData->visirTrip_advisor_2; ?></textarea>

                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">  
                                                                    <input type="hidden" name="propertyID" value="<?php echo $propertyId ?>"/>
                                                                    <input name="visit_trip_Advisor" type="hidden" value="update" />
                                                                    <input type="submit" class="btn green" name="submit" class="button" id="submit" value="Update URL" />

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <!-- BEGIN CORE PLUGINS -->
     <?php echo ajaxJsFooter(); ?>
</body>
</html>