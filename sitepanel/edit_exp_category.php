<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$inventoryCont = new inventoryData();
$menuCont = new menuNavigationPage();
$propertyCount = new propertyData();
$id = $_REQUEST['id'];
$get_res = $propertyCount->getExpCategoryWithID($id,$customerId);
$res = $get_res[0];
?>
<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);

function slugify($text) {
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}
?>

<?php
session_start();
if($type == 'edit'){
     'edit';
}else{
     'delete';
}
if (isset($_POST['update_experience_cate'])) {
    $categoryName = $_POST['CategoryName'];
    $url = slugify($categoryName);
    $update = $propertyCount->UpdateExpCategory($id,$customerId,$categoryName,$content,$url);
    echo '<script type="text/javascript">
                alert("Succesfuly Update Experience Category");              
window.location = "experience-category.php";
            </script>';
}
?>
<html>
    experience
    <head>
        <style>
            .header
            {
                width:100%;
                height:45px;
                background:#1ABC9C;
                padding-top:17px;
                color:white;
                font-size:25px;
                font-weight:bold;
                font-family:Arial;
            }
            .body
            {
                width:100%;
            }
            .property
            {
                width:100%;
            }
            .prop_select
            {
                width:400px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                padding-left:10px;
            }
            .prop_submit
            {
                height:40px;
                width:110px;
                background:#05B8CC;
                color:white;
                border:none;
                border-radius:3px;
                font-weight:bold;
            }
            .rate
            {
                width:100%;
            }
            .date_range
            {
                width:100%;
                border:1px solid #7FA6DD;
                padding-top:20px;
                padding-bottom:20px;
                box-shadow:1px 1px 1px 1px #cbdbf1;
                background:#f2f6fb;
            }
            .date_table
            {
                width:100%;
            }
            .date_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .date_submit
            {
                height:40px;
                width:140px;
                background:#05B8CC;
                color:white;
                border:none;
                border-radius:3px;
                font-weight:bold;
            }
            .room_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }
            .rates
            {
                width:auto;
            }
            .calendar
            {
                width:98%;
                border:1px solid #227298;

                padding-right:10px;
            }
            .calendar_table
            {
                width:100%;

                border-collapse:collapse;
                background:#227298;
                color:white;
                height:40px;
            }
            .calendar_data
            {
                width:auto;

                border-collapse:collapse;
                background:white;
                color:black;
                height:40px;
            }

            .input_css {
                background: #E26A6A;
                border: 0px;
                width: 99%;
                height: 100%;
                font-size: 13px;
                text-align: center;
            }
            .rate_modify1
            {
                width:90%;
                padding:20px;
                border:1px solid #7FA6DD;
                box-shadow:1px 1px 1px 1px #cbdbf1;
                background:#f2f6fb;
            }
            .room_select
            {
                width:210px;
                height:40px;
                border:1px solid #93b058;
                border-radius:5px;
                box-shadow:1px 1px 1px #cde69a;
                color:#5c6e37;
                font-weight:bold;
                font-size:15px;
                text-align:center;
            }

        </style>

        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> Edit Experience Category </span>
                                        </div>
                                        <div class="actions">
                                            <a href="experience-category.php"> <span style="float:right;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  value="Add Experience Category" /></span></a>
                                        </div>
                                    </div>                                  
                               
                                        <div class="portlet-body form">
                                            <form class="form-horizontal" action="" id="submit_form" method="post">
                                                <div class="form-wizard">
                                                    <div class="form-body">
                                                        <div class="tab-content"> 
                                                            <h3 class="block">Edit Experience Category</h3>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Enter Category
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="CategoryName" value="<?php echo $res->category_name ?>" />

                                                                    <span class="help-block"> Provide your Experience Category.</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3">Content</label>
                                                                <span class="required"> * </span>
                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control" name="content" rows="6" required><?php echo $res->content ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <a href="javascript:;" class="btn default button-previous">
                                                                    <i class="fa fa-angle-left"></i> Back 
                                                                </a>
                                                                <input type="submit" class="btn green" name="update_experience_cate" value="Update" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>                              
                                    </div>
                                </div>
                            </div>
                        </div><br><br>                      
                </div>
                </body>
                </html>

                <?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';      ?>

                <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL SCRIPTS -->
                <?php
                admin_footer();
                exit;
                ?>