<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$staticPageCont = new staticPageData();
$propertyCont = new propertyData();
$PropertyList = $propertyCont->getCityList($customerId);
//$reccnt = count($cityListings1);
//print_r($cityListings1);
?>
<?php
@extract($_REQUEST);
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;

date_default_timezone_set("Asia/Kolkata");

require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';

Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));
?>

<?php
if (isset($_POST['update_testimonial'])) {
    $recordID = $_POST['recordID'];
    $imagehead = $_POST['imagehead'];
    $file = $_FILES['image_org']['imagehead'];

    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;
    // echo $timestmp;
    // die;
    //echo $alt_tag."<br>";
    $roo_id = "0";

    if ($_FILES['image_org']['imagehead'] != "") {
        $selimgs = "select awardphoto from awards where slno='$recordID'";
        $selimgsquer = mysql_query($selimgs);
        $selimgsdata = mysql_fetch_array($selimgsquer);
        $photo_path = $selimgsdata['awardphoto'];

        $imact = "reputize/awards/" . $photo_path;
        \Cloudinary\Uploader::destroy($imact, array("invalidate" => TRUE));

        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/awards"));

        $cond = ",awardphoto='$timestmp'";
    }

    // $ins = "update awards set imagehead='$imagehead' $cond where customerID='$custID' and slno='$recordID'";
    // $quer = mysql_query($ins);

    Header("location:add-award.php");
    exit;
}
?>

<?php
if (isset($_POST['submit_testimonial'])) {
    $recordID = $_POST['recordID'];
    $imagehead = $_POST['imagehead'];
    $file = $_FILES['image_org']['imagehead'];

    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;
    // echo $timestmp;
    // die;
    //echo $alt_tag."<br>";
    $roo_id = "0";

    \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/awards"));

    //$ins = "insert into awards(customerID,imagehead,awardphoto) values('$custID','$imagehead','$timestmp')";
    // $quer = mysql_query($ins);

    Header("location:edit-awards.php");
    exit;
}
?>

<?php
$awardData = $propertyCont->GetAwardListData($customerId);
//$qry1 = "select * from awards where customerID='$custID' order by slno desc";
$reccnt = count($awardData);
$qry = $awardData[0];

if ($_SERVER['QUERY_STRING'] == true) {
    //  $qry20 = "select * from awards where customerID='$custID' and slno='$recordID'";
    //   $qry4 = mysql_query($qry20);
    //   $imgres = mysql_fetch_array($qry4);
}
?>

<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="https://www.theperch.in/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
            <div class="page-wrapper">
                <!-- BEGIN CONTAINER -->
                <?php
                themeheader();
                ?>
                <div class="page-container">
                    <?php
                    admin_header();
                    ?>

                    <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content">
                            <div class="row">
                                <div  class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Awards</span>

                                                <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                            </div>
                                        </div>
                                        <?php
                                        if ($_SERVER['QUERY_STRING'] == true) {
                                            
                                        } else {
                                            ?>
                                            <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Awards" /></span>
                                            <br><br><br><br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                                    <div class="portlet light bordered" id="add_photo" <?php
                                    if ($_SERVER['QUERY_STRING'] == true) {
                                        
                                    } else {
                                        ?>style="display:none;" <?php } ?>>			
                                        <div class="portlet-body form">
                                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="">
                                                <div class="form-body">

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Select Property
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="roomType" >
                                                                <option value="">Select</option>
                                                                <?php
                                                                foreach ($PropertyList as $res) {
                                                                    ?>
                                                                    <option value="<?= $res->propertyID; ?>"><?= $res->propertyName; ?></option>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </select> 
                                                            <span class="help-block"> Provide your room type.</span>
                                                        </div>
                                                    </div>

                                                    <h4 align="left" style="color:#E26A6A;"><b> Add Award Photo </b></h3><br><br>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Award Image
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select image </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="image_org"> 
                                                                    </span>
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a><br>
                                                                    <?php
                                                                    if ($_SERVER['QUERY_STRING'] == true) {
                                                                        ?>
                                                                        <b>Selected Photo :</b> <a href="http://res.cloudinary.com/the-perch/image/upload/w_160,h_160,c_fill/reputize/awards/<?php echo $imgres['awardphoto']; ?>.jpg" target="_blank"><?php echo $imgres['awardphoto']; ?></a>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"> Awards Heading
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input name="imagehead" type="text" class="form-control" <?php if ($_SERVER['QUERY_STRING'] == true) { ?> value="<?php echo html_entity_decode($imgres['imagehead']); ?>" <?php } ?>/>
                                                                <span class="help-block"> Provide Award Heading</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <?php
                                                                    if ($_SERVER['QUERY_STRING'] == true) {
                                                                        ?>
                                                                        <input type="hidden" name="recordID" value="<?php echo $recordID; ?>" />
                                                                        <input type="submit" name="update_testimonial" class="btn green" id="submit" value="Update Awards">
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <input type="submit" name="submit_testimonial" class="btn green" id="submit" value="Save Awards">
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <button type="button" class="btn default">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                                </div>

                                <?php
                                if ($_SERVER['QUERY_STRING'] == true) {
                                    
                                } else {
                                    if ($reccnt > 0) {
                                        $cnt = 1;
                                        $sl = $start + 1;
                                        while ($dataRes = mysql_fetch_array($qry)) {
                                            $dataRes = ms_htmlentities_decode($dataRes);
                                            if ($sl % 2 != 0) {
                                                $color = "#ffffff";
                                            } else {
                                                $color = "#f6f6f6";
                                            }
                                            if ($cnt == "1") {
                                                $color = "#32c5d2";
                                            }
                                            if ($cnt == "2") {
                                                $color = "#3598dc";
                                            }
                                            if ($cnt == "3") {
                                                $color = "#36D7B7";
                                            }
                                            if ($cnt == "4") {
                                                $color = "#5e738b";
                                            }
                                            if ($cnt == "5") {
                                                $color = "#1BA39C";
                                            }
                                            if ($cnt == "6") {
                                                $color = "#32c5d2";
                                            }
                                            if ($cnt == "7") {
                                                $color = "#578ebe";
                                            }
                                            if ($cnt == "8") {
                                                $color = "#8775a7";
                                            }
                                            if ($cnt == "9") {
                                                $color = "#E26A6A";
                                            }
                                            if ($cnt == "10") {
                                                $color = "#29b4b6";
                                            }
                                            if ($cnt == "11") {
                                                $color = "#4B77BE";
                                            }
                                            if ($cnt == "12") {
                                                $color = "#c49f47";
                                            }
                                            if ($cnt == "13") {
                                                $color = "#67809F";
                                            }
                                            if ($cnt == "14") {
                                                $color = "#8775a7";
                                            }
                                            if ($cnt == "15") {
                                                $color = "#73CEBB";
                                            }
                                            ?>

                                            <div class="col-md-12">
                                                <div class="m-heading-1 m-bordered" style="border-left:10px solid <?php echo $color; ?>;border-right:1px solid <?php echo $color; ?>;border-bottom:1px solid <?php echo $color; ?>;border-top:1px solid <?php echo $color; ?>;">

                                                    <div style="border:1px solid <?php echo $color; ?>; color:#fff; background:<?php echo $color; ?>; padding-left:12px;">

                                                        <p> <b><span style="font-size:large"><?php echo $dataRes['imagehead']; ?></span></b>
                                                            <span style="color:#fff; float:right; font-size:x-large">
                                                                <span id="btn_div<?php echo $cnt; ?>">
                                                                    <a href="edit-awards.php?id=edit&recordID=<?php echo $dataRes['slno']; ?>" style="color:white;"><button type="button" name="<?php echo $cnt; ?>" class="btn" style="width:95px;background:black;">Edit</button></a>
                                                                </span>

                                                                <span id="btn_div<?php echo $cnt; ?>">
                                                                    <a href="edit-awards.php?id=delete&recordID=<?php echo $dataRes['slno']; ?>" style="color:white;"><button type="button" name="<?php echo $cnt; ?>" class="btn red" style="width:95px;">Delete</button></a>
                                                                </span>
                                                            </span><br>
                                                    </div>

                                                    <div class="row" style="padding:10px;">
                                                        <div class="col-md-8">	
                                                            <table style="width:100%;">
                                                                <tr>
                                                                    <td style="width:180px;">
                                                                        <img src="http://res.cloudinary.com/the-perch/image/upload/w_160,h_100,c_fill/reputize/awards/<?php echo $dataRes['awardphoto']; ?>.jpg" />
                                                                    </td>

                                                                    <td>
                                                                        <span>
                                                                            <b>Award Heading: </b><? echo strip_tags(trim(ucfirst(substr($dataRes['imagehead'], 0, 80))));?>..</span>	
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $cnt++;
                                        }
                                    } else {
                                        ?>
                                        <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                            <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                        </table>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </html>
    <!-- BEGIN CORE PLUGINS -->
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <?php
    admin_footer();
    exit;
    ?>

