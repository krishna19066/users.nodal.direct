<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyId = $_REQUEST['propID'];
$mode = $_SESSION['mode'];
$ajaxpropertyRoomCont = new propertyData();
$cloud_keySel = $ajaxpropertyRoomCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$bucket=$cloud_keyData->bucket;
$property_data = $ajaxpropertyRoomCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
//$connectCloudinary = $ajaxpropertyRoomCont->connectCloudinaryAccount($customerId);
//   $roomDetail = $ajaxpropertyRoomCont->getRoomDetail($propertyId);
if(isset($_REQUEST['type']) == 'activate'){
    $roomID = $_REQUEST['recordID'];
    $status = $_REQUEST['status'];
    $propID = $_REQUEST['propID'];
    if($status == 'Y'){
        echo  $status = 'Y';
    }else{
      echo   $status = 'N';
    }    
    $upd = $ajaxpropertyRoomCont->UpdateRoomStatus($roomID,$status);
    echo '<script type="text/javascript">
                alert("Succesfuly Update");              
window.location = "ajax_view-rooms.php?propID=' . $propID .'";
            </script>';
}
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>
    </head>
</head>

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
    <?php
    themeheader();
    ?>
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php
        admin_header();
        ?>

        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="background:#e9ecf3;">
                <div class="page-head">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1><span style="color:#e44787;"><?php echo $propertyName; ?></span> - ROOM DETAILS
                            <small>View all the details of your rooms</small>
                        </h1>
                    </div>
                </div>

                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="welcome.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="ajax_view-rooms.php">My Properties</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>View Property Rooms</span>
                    </li>
                </ul>

                <div  class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-equalizer font-red-sunglo"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase"><?= $propertyRes[propertyName]; ?></span>
                                <span class="caption-helper"><?= $propertyRes[propertyName]; ?></span>
                                <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                            </div>
                        </div>
                        <span style="float:left;"> <a href="ajax_add-rooms.php?propID=<?php echo $propertyId; ?>" onclick="load_add_rooms('<?php echo $propertyId; ?>');"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Add Rooms</button></a></span>

                        <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>

                        <br><br><br><br>
                    </div>
                </div>

                <div class="blog-page blog-content-1">
                    <div class="row">
                        <?php
                        $roomDetail = $ajaxpropertyRoomCont->getRoomDetail($propertyId);

                        foreach ($roomDetail as $roomData) {
                            $roomId = $roomData->roomID;
                            $roomPhotoDetail = $ajaxpropertyRoomCont->getRoomPhoto($propertyId, $roomId, "and type='home' order by imagesID desc limit 1");
                            $roomPhotoData = $roomPhotoDetail[0];
                            ?>

                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    $cnt = 1;
                    $sl = $start + 1;
                    foreach ($roomDetail as $dataRes) {
                        // $dataRes = ms_htmlentities_decode($dataRes);
                        if ($sl % 2 != 0) {
                            $color = "#ffffff";
                        } else {
                            $color = "#f6f6f6";
                        }
                        if ($cnt == "1") {
                            $color = "#32c5d2";
                        }
                        if ($cnt == "2") {
                            $color = "#3598dc";
                        }
                        if ($cnt == "3") {
                            $color = "#36D7B7";
                        }
                        if ($cnt == "4") {
                            $color = "#5e738b";
                        }
                        if ($cnt == "5") {
                            $color = "#1BA39C";
                        }
                        if ($cnt == "6") {
                            $color = "#32c5d2";
                        }
                        if ($cnt == "7") {
                            $color = "#578ebe";
                        }
                        if ($cnt == "8") {
                            $color = "#8775a7";
                        }
                        if ($cnt == "9") {
                            $color = "#E26A6A";
                        }
                        if ($cnt == "10") {
                            $color = "#29b4b6";
                        }
                        if ($cnt == "11") {
                            $color = "#4B77BE";
                        }
                        if ($cnt == "12") {
                            $color = "#c49f47";
                        }
                        if ($cnt == "13") {
                            $color = "#67809F";
                        }
                        if ($cnt == "14") {
                            $color = "#8775a7";
                        }
                        if ($cnt == "15") {
                            $color = "#73CEBB";
                        }
                        ?>

                        <div class="col-md-12">
                            <div class="m-heading-1 m-bordered" style="border-left:10px solid <?php echo $color; ?>;border-right:1px solid <?php echo $color; ?>;border-bottom:1px solid <?php echo $color; ?>;border-top:1px solid <?php echo $color; ?>;">
                                <div style="border:1px solid <?php echo $color; ?>; color:#fff; background:<?php echo $color; ?>; padding-left:12px;">
                                    <p> <b><span style="font-size:large"><?= $dataRes->roomType; ?></span></b>
                                        <span style="color:#fff; float:right; font-size:x-large">
                                            <?php
                                            if ($dataRes->status == 'Y') {
                                                ?>
                                                <span id="btn_div<?php echo $cnt; ?>">
                                                    <a href="ajax_view-rooms.php?type=activate&recordID=<?php echo $dataRes->roomID; ?>&propID=<?php echo $dataRes->propertyID; ?>&status=N"><button type="button" name="<?php echo $cnt; ?>" value="view-property-rooms.php?id=deactivate&recordID=<?php echo $dataRes->roomID; ?>" class="btn btn-danger" onclick="change_status(this);" style="width:95px;" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>Deactivate</button></a>
                                                </span>
                                                <!--<a href="<?//=SITE_ADMIN_URL?>/<?//=$script_name;?>?id=deactivate&recordID=<?//=$dataRes[roomID];?>" ><button type="button" class="btn btn-danger">Deactivate</button></a> &nbsp; &nbsp;-->
                                                <?php
                                            }
                                            if ($dataRes->status == 'N' || $dataRes->status == 'D') {
                                                ?>	
                                                <span id="btn_div<?php echo $cnt; ?>">
                                                    <a href="ajax_view-rooms.php?type=activate&recordID=<?php echo $dataRes->roomID; ?>&propID=<?php echo $dataRes->propertyID; ?>&status=Y"><button type="button" name="<?php echo $cnt; ?>" value="view-property-rooms.php?id=activate&recordID=<?php echo $dataRes->roomID; ?>" class="btn btn-primary" onclick="change_status(this)" style="width:95px;background:green;border:1px solid #36c6d3;" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>Activate</button></a>
                                                </span>
                                                <!--<a href="<?//=SITE_ADMIN_URL?>/<?//=$script_name;?>?id=activate&recordID=<?//=$dataRes[roomID];?>"><button type="button" class="btn" style="background:green;color:white;">Activate</button></a> &nbsp; &nbsp;-->
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($dataRes->status != 'D') {
                                                ?>

                                                <a href="edit-room.php?id=delete&recordID=<?= $dataRes->roomID ?>" onClick="return confirm('Are you sure you want to delete this record')" style="color:#fff; <?php if($mode == 'V'){ ?> pointer-events: none; cursor: not-allowed;opacity: 0.6; <?php } ?>"><span title="Delete" class=" fa fa-trash"></span></a>

                                                <a href="edit-room.php?id=edit&recordID=<?= $dataRes->propertyID; ?>&roomID=<?= $dataRes->roomID; ?>"><span style="padding:0px 12px 0px 2px; color:#fff;" title="Edit" class="fa fa-edit"></span></a>
                                                <?php
                                            }
                                            ?>
                                        </span><br>
                                </div>
                                <div class="row" style="padding:10px;">
                                    <div class="col-md-8">	
                                        <table style="width:100%;">
                                            <tr>
                                                <td style="width:180px; padding: 10px;">
                                                    <?php
                                                    $id = $propertyRes->propertyID;
                                                    $rid = $dataRes->roomID;
                                                    //echo $id;
                                                    //    $imgsel = "select * from propertyImages where propertyID='$id' and roomID='$rid' and imageType='room' and type='home' order by imagesID desc limit 1";
                                                    //  $imquery = mysql_query($imgsel);
                                                    $roomPhotoDetail = $ajaxpropertyRoomCont->getRoomPhoto($propertyId, $rid, "and type='home' and roomFeatureStat = 'Y' order by imagesID desc limit 1");
                                                    $roomPhotoData = $roomPhotoDetail[0];
                                                    $imgCount = count($roomPhotoDetail);
                                                    foreach ($roomPhotoDetail as $roomPhoto) {
                                                        ?>
                                                        
                                                    <?php if($roomPhoto->img_resource ==='S3'){ ?>
                                                    <img src="//<?php echo $bucket; ?>.s3.amazonaws.com/<?= $roomPhoto->imageURL; ?><?php if ($customerId == 1){ ?>.jpg<?php } ?>" width="180" height="150" />
                                                    <?php } else{ ?> 
                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/w_700,h_700,c_fill/<?php if ($customerId != 1) { ?>reputize<?php } ?>/room/<?php echo $roomPhoto->imageURL; ?>.jpg" width="180" height="150" />
                                                    <?php } ?>
                                                         
                                                         <?php
                                                    }
                                                    ?>
                                                </td>
                                                <?php
                                                $roomAmnty = $dataRes->roomAmenties;
                                                $rommAmnty_br = explode("^", $roomAmnty);

                                                $roomAmenities = '';
                                                foreach ($rommAmnty_br as $roomAmntys) {
                                                    $roomAmenities .= $roomAmntys . " | ";
                                                }
                                                ?>
                                                <td>
                                                    <span>
                                                        <b>Property Info: </b> <?php
                                                        $text = $dataRes->roomOverview;
                                                        echo html_entity_decode(substr($text, 0, 200)) . '...';
                                                        ?></span>	</br><br>
                                                    <span><b>Property Amenties: </b><?php echo $roomAmenities; ?></span>	
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class="col-md-4">
                                        <table class="table table_prop2" style="margin-left: 5px; float:right; border:1px solid <?php echo $color; ?>; text-align:center; width:97%;" >
                                            <thead style="background-color:<?php echo $color; ?>; color:#fff;">
                                                <tr>
                                                    <th style="text-align:center">No. of Rooms</th>
                                                    <th style="text-align:center">Occupancy</th>
                                                    <th style="text-align:center">Room Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr>
                                                    <td> <?php echo $dataRes->room_no; ?> </td>
                                                    <td> <?php echo $dataRes->occupancy; ?> </td>
                                                    <td> <?php echo $dataRes->roomPriceINR; ?> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <span style="float:left;padding-left:10px;">Status : 
                                    <?php
                                    if ($dataRes->status == 'Y') {
                                        ?>
                                        <b>Active</b> 
                                        <?php
                                    } elseif ($dataRes->status == 'D') {
                                        ?>
                                        <strong>Deleted</strong>
                                        <?php
                                    } else {
                                        ?>
                                        <strong>Inactive</strong>
                                        <?php
                                    }
                                    ?>		
                                </span>
                                <span style="float:right;padding-right:10px; ">
                                    <a  href="view-room-photo.php?recordID=<?= $dataRes->propertyID; ?>&roomID=<?= $dataRes->roomID; ?>">View Room Photo</a>

                                   <!-- <a  href="add-room-rate.php?recordID=<?= $dataRes->propertyID; ?>&roomID=<?= $dataRes->roomID; ?>">Add Room Rate</a>-->
                                </span><br>
                            </div>
                        </div>
                        <?php
                        $cnt++;
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <?php echo ajaxJsFooter(); ?>
</body>
</html>

