<?php
	include "admin-function.php";
	
	// include "cms_functions.php";
	// user_locked();
	// staff_restriction();
	$customerId = $_SESSION['customerID'];
	$inquiryCont = new adminFunction();
?>
<?php
	/*if($id=="delete") 
		{
			$upd = "update propertyTable set status='D' where propertyID='$recordID'";
			$upd_que = mysql_query($upd);
			Header("location:manage-property.php");
		}*/
?>
<div id="add_prop">
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html lang="en">
		<head>
			<link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
			<link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
			
			<style>
				#top_menu_container
					{
						background-color: #f5f8fd;
						border-color: #8bb4e7;
						color: #678098;
						margin: 0 0 20px;
						border-left: 5px solid #8bb4e7;
					}
				
				#top_menu
					{
						list-style: none;
						display: flex;
						justify-content: space-between;
						align-items: center;
						margin: 0px;
						
					}
				#top_menu li
					{
						padding: 15px 20px;
						cursor: pointer;
					}
				.OurMenuActive
					{
						background-color: #5c9cd1;
						color: white;
					}
			</style>
			<style>
				.table_prop  td
					{
						padding:3px 0px 3px 5px;
						bordder:0.5px solid #000;
				
					}
			
				.table_prop2 th 
					{
						color: #fff;
					}
			
				.table_prop2	tr:nth-child(even) 
					{
						background-color: #E9EDEF;
					}		
			
				.table_prop td:first-child 
					{
						font-weight:bold;
					}
			
				.table_prop td:nth-child(3) 
					{
						font-weight:bold;
						width: 120px;
					}
			
				.table.table_prop 
					{
						table-layout:fixed; 
					}
			
				.table_prop3 
					{
						table-layout:fixed;
					}
			
				.data_box 
					{
						padding:50px ! important;
					}
			</style>
			
			<!-- BEGIN THEME LAYOUT STYLES -->
			<link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
			<link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
			<link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
			<!-- END THEME LAYOUT STYLES -->
			
			<?php  
				adminCss();
			?>
		</head>
		
		<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
			<?php
				themeheader();
			?>
			<!-- BEGIN HEADER & CONTENT DIVIDER -->
			<div class="clearfix"> </div>
			<!-- END HEADER & CONTENT DIVIDER -->
			
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<?php
					admin_header();
				?>
				
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<div class="page-content" style="background:#e9ecf3;">
						<div class="page-head">
							<!-- BEGIN PAGE TITLE -->
							<div class="page-title">
								<h1> MANAGE Inquiries
									<small>View and Edit Inquiries Details</small>
								</h1>
							</div>
						</div>
						
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="welcome.php">Home</a>
								<i class="fa fa-circle"></i>
							</li>
							<li>
								<a href="welcome.php">Inquires</a>
								<i class="fa fa-circle"></i>
							</li>
							<li>
								<span>Booking Inquiries</span>
							</li>
						</ul>
						
						<div id="top_menu_container">
							<ul id="top_menu">
								<a href="all-inquiries.php" style="text-decoration:none;"><li class="OurMenuActive">Booking Inquiries</li></a>
								<li onclick="">Call Back Inquires</li>
								<li onclick="">Contact Us Inquiries</li>
								<li onclick="">Subscription</li>
							</ul>
						</div>
						
						<div id="cityCont">
						<div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Buttons </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="sample_2_wrapper" class="dataTables_wrapper no-footer"><div class="row"><div class="col-md-12"><div class="dt-buttons"><a class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="#"><span>Print</span></a><a class="dt-button buttons-copy buttons-html5 btn default" tabindex="0" aria-controls="sample_2" href="#"><span>Copy</span></a><a class="dt-button buttons-pdf buttons-html5 btn default" tabindex="0" aria-controls="sample_2" href="#"><span>PDF</span></a><a class="dt-button buttons-excel buttons-html5 btn default" tabindex="0" aria-controls="sample_2" href="#"><span>Excel</span></a><a class="dt-button buttons-csv buttons-html5 btn default" tabindex="0" aria-controls="sample_2" href="#"><span>CSV</span></a><a class="dt-button btn default" tabindex="0" aria-controls="sample_2" href="#"><span>Reload</span></a></div></div></div><div class="row"><div class="col-md-6 col-sm-12"><div class="dataTables_length" id="sample_2_length"></div></div><div class="col-md-6 col-sm-12"><div id="sample_2_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="sample_2"></label></div></div></div><div class="table-scrollable"><table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_2" role="grid" aria-describedby="sample_2_info">
                                  
                                        <tbody>
  
                                        <div class="panel-group accordion" id="accordion3">
											<?php
											
									$inquiryList = $inquiryCont->getInquiryData($customerId);
									
									foreach($inquiryList as $inquiryData)
										{
											
											$inquiryName = $inquiryData -> name;
								?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#<?php echo $inquiryName; ?>" aria-expanded="false"> <?php echo $inquiryName; ?></a>
                                                </h4>
                                            </div>
                                            <div id="<?php echo $inquiryName; ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <p> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut. </p>
                                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                        </p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
										}
								?>
                                    </div>
								
								
								
							</tbody>
                                    </table></div><div class="row"><div class="col-md-5 col-sm-12"><div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Showing 1 to 10 of 43 entries</div></div><div class="col-md-7 col-sm-12"><div class="dataTables_paginate paging_bootstrap_number" id="sample_2_paginate"><ul class="pagination" style="visibility: visible;"><li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li></ul></div></div></div></div>
                                </div>
                            </div>
						<br><br>
						</div>
					</div>
				</div>
			</div>
		</body>
	</html>
			
	<script>
		$('#top_menu li').click(function (){
			$('#top_menu li').removeClass('OurMenuActive');
			$(this).addClass('OurMenuActive');
		});
	</script>

	<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
		<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
		<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
		<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<?php
		admin_footer();
	?>        
</div>	