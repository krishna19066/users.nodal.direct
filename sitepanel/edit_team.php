<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$staticPagedata = new staticPageData();
$slno = $_REQUEST['id'];
$getTeamMember = $staticPagedata->GetTeamMemberWithID($slno);
$selectdetdata = $getTeamMember[0];
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
 $bucket = $cloud_keyData->bucket;

?>

<?php
if ($id == "delete") {
    $recordID = $_GET['recordID'];
    //$upd = "update propertyTable set status='D' where propertyID='$recordID'";
    // $upd_que = mysql_query($upd);
    $upd = $propertyCont->DeletePropertyData($recordID);
    Header("location:manage-property.php");
}
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>

            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Team Member Details
                                    <small>Edit Team Member</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">My Properties</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Edit</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="property_change">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">	

                                    <form <?php if($bucket){ ?>action="S3-createObject.php"<?php }else{ ?> action="add_static.php"<?php } ?> method="post" enctype="multipart/form-data">	
                                        <input type="hidden" name="url_page" value="<?php echo $querystr; ?>" />
                                        <input type = "hidden" name = "sln" value = "<?php echo $slno; ?>" />
                                        <div class = "col-md-6">
                                            <h4>Name</h4>
                                            <p>
                                                <input class = "form-control" type = "text" name = "teamName" value = "<?php echo $selectdetdata->name; ?>">
                                            </p>
                                        </div>
                                        <div class = "col-md-6">
                                            <h4>Department</h4>
                                            <p>
                                                <input class = "form-control" type = "text" name = "teamDepart" value = "<?php echo $selectdetdata->depart; ?>">
                                            </p>
                                        </div>
                                        <div class = "col-md-6">
                                            <h4>Designation</h4>
                                            <p>
                                                <input class = "form-control" type = "text" name = "teamDesig" value = "<?php echo $selectdetdata->desig; ?>">
                                            </p>
                                        </div>

                                        <div class = "col-md-6">
                                            <h4>About Team Member</h4>
                                            <p>
                                                <textarea class = "form-control" name = "teamDescrip" rows = "6" ><?php echo $selectdetdata->about;
                ?> </textarea>								
                                            </p>																										
                                        </div>	

                                        <div class="col-md-6">																												
                                            <h4>About Team Member</h4>																												
                                            <p>																														
                                                <div class="fileinput fileinput-new" data-provides="fileinput">																				
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php  $img= $selectdetdata->pic_url;
                                                                if($img==''){
                                                        ?>
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                <?php } else{ ?>
                                                        <img src=" <?php if($bucket){ ?>https://<?php echo $bucket;  ?>.s3.amazonaws.com/<?php echo $selectdetdata->pic_url; ?><?php } else{ ?> http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/team_members/<?php echo $selectdetdata->pic_url; ?>.jpg<?php } ?>" alt="" />
                                                                <?php } ?>
                                                    </div>	

                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>	

                                                    <div>																									
                                                        <span class="btn default btn-file">																					
                                                            <span class="fileinput-new"> Select image </span>	

                                                            <span class="fileinput-exists"> Change </span>	

                                                            <input type="file" name="image_org"> 																	
                                                        </span>		

                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>	<br>
                                                            Selected Photo : <a href="<?php if($bucket){?>https://<?php echo $bucket;  ?>.s3.amazonaws.com/<?php echo $selectdetdata->pic_url; ?> <?php } else{ ?> http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/team_members/<?php echo $selectdetdata->pic_url; ?>.jpg<?php } ?>"> <?php echo $selectdetdata->pic_url; ?> </a>
                                                    </div>																														
                                                </div>																												
                                            </p>																										
                                        </div>
                                        <div class="modal-footer">																						
                                               <a href="manage-static-pages.php"><input type="button" name="back" class="btn red" value="Back" /></a>																						
                                            <input type="submit" name="update_team" class="btn green" value="Save changes">																			
                                        </div>	
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>



        </script>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?= SITE_ADMIN_URL; ?>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

        <script src="assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <?php
        admin_footer();
        ?>
    </html>
</div>