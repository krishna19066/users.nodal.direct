<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$res = $cityListings1[0];
//$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];

$cloud_cdnName = $cloud_keyData->cloud_name;
$user_email = $cloud_keyData->email;
$hotel_name = $cloud_keyData->companyName;
$phone = $cloud_keyData->phone;


$propertyId = base64_decode($_REQUEST['propID']);

$property_data = $propertyCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;

$super_admin_1 = $propertyCont->GetSupperAdminData($customerId);
$super_admin = $super_admin_1[0];
$status = $super_admin->status;
$publish = $_REQUEST['publish'];
if ($publish == 'Yes') {

    $subject = 'Request for website Live';
    $mess = "Hello Team, <br/><p> We Request to Publish our Website</p>  <br /><br /> Client Name :- " . $hotel_name . "<br /><br /> Email : " . $user_email . "<br /><br /> Mobile : " . $phone . " ";
    $to = 'support@nodal.direct';
   // $to = 'balkrishn.sharma@nodal.direct';
    // $cc = $r['mail_cc'];
    $from = $user_email;
    $url = 'https://api.sendgrid.com/';
    $user = 'kunalsingh2000';
    $pass = 'P@ssw0rd';
    $json_string = array('to' => array($to), 'category' => 'inquiry');
    $params = array(
        'api_user' => 'kunalsingh2000',
        'api_key' => 'P@ssw0rd',
        'x-smtpapi' => json_encode($json_string),
        'to' => $to,
        'subject' => $subject,
        'html' => $mess,
        'replyto' => $to,
        'cc' => $cc,
        'fromname' => $hotel_name,
        'text' => 'testing body',
        'from' => $from,
    );
    
    $request = $url . 'api/mail.send.json';
    $session = curl_init($request);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
    echo '<script type="text/javascript">
                alert("Thank You! Your Request has been successfuly submited");              
window.location = "site-visibility.php";
            </script>';
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Site - <span style="color:#e44787;">Visibility</span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">Setting</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View Site Visibility</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><h2>Site Visibility</h2></span>                 
                                       
                                        <!--<span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Video Gallery </button></a></span>-->

                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row"> 
                                            <div class="col-lg-12 col-md-612 col-sm-12 col-xs-12">
                                                <h2>Site Visibility</h2>
                                                <p>Your site is currently Private and only you can see it. You can change your site's visibility at any time.</p>  

                                                <!-- Material unchecked -->
                                                <div class="form-check">

                                                    <input type="radio" class="form-check-input"  title="Upgrade to Publish" <?php if ($status == 'live') { ?> checked <?php } else { ?> disabled <?php } ?> >
                                                    <label class="form-check-label" for="materialUnchecked">Public</label><br/>
                                                    <span style="font-size:10px; margin-left: 16px;">Anyone can see this site.</span>
                                                </div><br/>

                                                <!-- Material checked -->
                                                <div class="form-check">
                                                    <input type="radio" class="form-check-input" <?php if ($status != 'live') { ?> checked  <?php } else { ?> disabled <?php } ?>>
                                                    <label class="form-check-label" for="materialChecked">Private</label><br/>
                                                    <span style="font-size:10px; margin-left: 16px;">Only you can see the site.</span>
                                                </div>
                                                <span style=""> <a href="site-visibility.php?publish=Yes" onClick="return confirm('Are you sure you want to Publish Website')"><button style="background:#009688;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;margin-top: 32px;"> Upgrade to Publish </button></a></span>

                                            </div>                                            
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        /* Style the element that is used to open and close the accordion class */
        p.accordion {
            background-color: #efefef;;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            text-align: left;              
            border: none;
            outline: none;
            transition: 0.4s;
            margin-bottom:10px;

        }
        /* Add a background color to the accordion if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
        p.accordion.active, p.accordion:hover {
            background-color: #ddd;
        }
        /* Unicode character for "plus" sign (+) */
        p.accordion:after {
            content: '+';
            font-size: 18px;
            color: #777;
            float: right;
            margin-left: 5px;
        }
        /* Unicode character for "minus" sign (-) */
        p.accordion.active:after {
            content: "-";
        }
        /* Style the element that is used for the panel class */
        div.panel {
            padding: 0 18px;
            background-color: white;
            max-height: 0;
            overflow: hidden;
            transition: 0.4s ease-in-out;
            opacity: 0;
            margin-bottom:10px;
        }
        div.panel.show {
            opacity: 1;
            max-height: 500px; /* Whatever you like, as long as its more than the height of the content (on all screen sizes) */
        }
    </style>

    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            var acc = document.getElementsByClassName("accordion");
            var panel = document.getElementsByClassName('panel');
            for (var i = 0; i < acc.length; i++) {
                acc[i].onclick = function () {
                    var setClasses = !this.classList.contains('active');
                    setClass(acc, 'active', 'remove');
                    setClass(panel, 'show', 'remove');
                    if (setClasses) {
                        this.classList.toggle("active");
                        this.nextElementSibling.classList.toggle("show");
                    }
                }
            }
            function setClass(els, className, fnName) {
                for (var i = 0; i < els.length; i++) {
                    els[i].classList[fnName](className);
                }
            }
        });
    </script>
    <script type="text/javascript">

        /***********************************************
         * Cross browser Marquee II- � Dynamic Drive (www.dynamicdrive.com)
         * This notice MUST stay intact for legal use
         * Visit http://www.dynamicdrive.com/ for this script and 100s more.
         ***********************************************/
        var video = document.getElementById("myVideoBefore");
        var videoo = document.getElementById("myVideoAfter");
        var beforei = document.getElementById("beforeicon");
        var afteri = document.getElementById("aftericon");
        function playVideoo() {
            if (videoo.paused) {
                videoo.play();
                afteri.classList.add('fa-pause-circle-o');
                afteri.classList.remove('fa-play-circle-o');
                //btn.innerHTML = "Pause";
            } else {
                videoo.pause();
                afteri.classList.add('fa-play-circle-o');
                afteri.classList.remove('fa-pause-circle-o');
                //btn.innerHTML = "Play";
            }
        }

        function playVideo() {
            if (video.paused) {
                video.play();
                beforei.classList.add('fa-pause-circle-o');
                beforei.classList.remove('fa-play-circle-o');
                //btn.innerHTML = "Pause";
            } else {
                video.pause();
                beforei.classList.add('fa-play-circle-o');
                beforei.classList.remove('fa-pause-circle-o');
                //btn.innerHTML = "Play";
            }
        }
    </script>

    <?php echo ajaxJsFooter(); ?>
</body>
</html>