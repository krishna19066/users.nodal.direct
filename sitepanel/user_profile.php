<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
$userData = new UserDetails();
$staticPageCont = new staticPageData();
$porpCont = new propertyData();
$user_keySel_1 = $userData->GetUserdataWithUserID($customerId, $user_id);
$user_keyData = $user_keySel_1[0];
$cloud_keySel = $porpCont->get_Cloud_AdminDetails($customerId);

//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$cloud_cdnKey = $cloud_keyData->api_key;
$cloud_cdnSecret = $cloud_keyData->api_secret;
require 'Cloudinary.php';
require 'Uploader.php';
require 'Api.php';


Cloudinary::config(array(
    "cloud_name" => $cloud_cdnName,
    "api_key" => $cloud_cdnKey,
    "api_secret" => $cloud_cdnSecret
));
?>
<?php
if (isset($_POST['det_sub'])) {
    $uid = $_POST['user_id'];
    $full_name = $_POST['full_name'];
    $phone = $_POST['phone'];
    $occup = $_POST['occup'];
    $about = $_POST['about'];
    $company = $_POST['company'];
    // $user_update = "update admin_details set name='$full_name',phone='$phone',occupation='$occup',about='$about',companyName='$company' where user_id='$uid'";
    $user_update = $porpCont->UpdateUserDetails($customerId, $full_name, $phone, $occup, $about, $company, $user_id);
    //$upd_quer = mysql_query($user_update);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated User Data");              
window.location = "user_profile.php";
            </script>';
}
if (isset($_POST['pass_sub'])) {
    $uid = $_POST['user_id'];
    //$old_pass = $_POST['old_pass'];
    $new_pass = $_POST['new_pass'];
    $re_pass = $_POST['re_new_pass'];
    if ($new_pass == $re_pass) {
        // $upd_pass = "update admin_details set pwd='$new_pass' where user_id='$uid'";
        $upd_pass = $porpCont->UpdatePassword($customerId, $new_pass, $user_id);
        // $pass_quer = mysql_query($upd_pass);
        if ($upd_pass == true) {
            echo '<script type="text/javascript">
                alert("Succesfuly Updated Password");              
window.location = "user_profile.php#tab_1_3";
            </script>';
        }
    } else {
        echo '<script type="text/javascript">
                alert("Does Not matche password");              
window.location = "user_profile.php#tab_1_3";
            </script>';
    }
}

if (isset($_POST['pic_sub'])) {
    $us_id = $_POST['user_id'];
    $file = $_FILES['user_pict']['name'];
    $alt_tag = $_POST['imageAlt1'];
    $nam = $us_id;
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    \Cloudinary\Uploader::upload($_FILES["user_pict"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/admin_user"));

    // $ins = "update admin_details set photo='$nam' where user_id='$us_id'";
    //$quer = mysql_query($ins);
    $update_pic = $porpCont->UpdateUserPic($customerId, $timestmp, $user_id);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated User Pic");              
window.location = "user_profile.php#tab_1_2";
            </script>';
}
?>

<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE USER PROFILE
                                    <small>View and manage User</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Manage User</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage user</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <a class="pull-right">
                                               <!-- <button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;" onclick="load_add_static_page();"><i class="fa fa-plus"></i> &nbsp Add Page</button>-->
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> Manage User</span>
                                        </div>
                                    </div>
                                    <div class="profile-sidebar">
                                        <!-- PORTLET MAIN -->
                                        <div class="portlet light profile-sidebar-portlet ">
                                            <!-- SIDEBAR USERPIC -->
                                            <div class="profile-userpic">
                                                <?php
// $picsel = "select * from admin_details where user_id='$id1'";
//$picquer = mysql_query($picsel);
                                                if ($userType == 'user') {
                                                    $picpath = $user_keyData->photo;
                                                } else {
                                                    $picpath = $cloud_keyData->photo;
                                                }
                                                ?>
                                                <?php
                                                if ($picpath == '') {
                                                    ?>
                                                    <img src="https://u.o0bc.com/avatars/no-user-image.gif" class="img-responsive" alt=""> </div>
                                                <?php
                                            } else {
                                                ?>
                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/g_face,w_150,h_150,c_fill/reputize/admin_user/<?php echo $picpath; ?>.jpg" class="img-responsive" alt=""> </div>
                                            <?php
                                        }
                                        ?>

                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"><?php echo $username; ?> </div>
                                            <div class="profile-usertitle-job"> <?php echo $type; ?> </div>
                                        </div>

                                        <div class="profile-usermenu">
                                            <ul class="nav">

                                                <li class="active">
                                                    <a href="user_profile.php">
                                                        <i class="icon-settings"></i> Account Settings </a>
                                                </li>

                                            </ul>
                                        </div>
                                        <!-- END MENU -->
                                    </div>
                                    <div class="portlet light ">

                                        <div>
                                            <h4 class="profile-desc-title"> <?php echo $res->user_id; ?></h4>
                                            <?php
                                            if ($res->about == '') {
                                                ?>
                                                <span class="profile-desc-text"> No Details Yet. Please Update About Field </span>
                                                <?php
                                            } else {
                                                ?>
                                                <span class="profile-desc-text"><?php echo $res->about; ?> </span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <!-- END PORTLET MAIN -->
                                </div>

                                <!-- END PORTLET MAIN -->
                            </div>
                            <?php
                            if ($status == 'true') {
                                echo '<h1 class="page-title" style="color:red;">' . $msg . '</h1>';
                                //unset($_SESSION['pass_change']);
                            } elseif ($status == 'false') {
                                echo '<h1 class="page-title" style="color:red;">' . $msg . '</h1>';
                                //unset($_SESSION['pass_mismatch']);
                            }
                            ?> 
                            <div class="portlet-body">
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_2" data-toggle="tab">Change Picture</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                        </li>
                                                        <!--<li>
                                                            <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                        </li>-->
                                                    </ul>
                                                </div>
                                                <?php
                                                if ($userType == 'user') {
                                                    $picpath = $user_keyData->photo;
                                                    $name = $user_keyData->managerName;
                                                    $phone = $user_keyData->mobile;
                                                } else {
                                                    $picpath = $cloud_keyData->photo;
                                                    $name = $cloud_keyData->name;
                                                    $phone = $cloud_keyData->phone;
                                                }
                                                ?>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">
                                                            <form role="form" action="" method="post">
                                                                <input type="hidden" name="user_id" value="<?php echo $res->user_id; ?>" />
                                                                <div class="form-group">
                                                                    <label class="control-label">Full Name</label>
                                                                    <input type="text" value="<?php echo $name; ?>" name="full_name" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="text" value="<?php echo $phone; ?>" name="phone" class="form-control" /> </div>
                                                                    <?php if($userType != 'user'){ ?>
                                                                <div class="form-group">
                                                                    <label class="control-label">Occupation</label>
                                                                    <input type="text" value="<?php echo $res->occupation; ?>" name="occup" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">About</label>
                                                                    <textarea class="form-control" rows="3" name="about"><?php echo $res->about; ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Company</label>
                                                                    <input type="text" value="<?php echo $res->companyName; ?>" name="company" class="form-control" /> 
                                                                </div>
                                                                 <?php } ?>
                                                                <div class="margiv-top-10">
                                                                    <input type="submit" value="Save Changes" name="det_sub" class="btn green" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> /> 
                                                                    <a href="welcome.php" class="btn default"> Cancel </a>
                                                                </div>
                                                                   
                                                            </form>
                                                        </div>
                                                        <!-- END PERSONAL INFO TAB -->
                                                        <!-- CHANGE AVATAR TAB -->
                                                        <div class="tab-pane" id="tab_1_2">
                                                            <?php
                                                            if ($userType == 'user') {
                                                                $picpath1 = $user_keyData->photo;
                                                            } else {
                                                                 $picpath1 = $cloud_keyData->photo;
                                                            }
                                                            ?>
                                                            <p> Change your Profile Picture </p>
                                                            <form action="" enctype="multipart/form-data" method="post">
                                                                <input type="hidden" name="user_id" value="<?php echo $res->user_id; ?>" />
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <?php if ($picpath = '') { ?>
                                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                            <?php } else { ?>
                                                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/g_face,c_fill/reputize/admin_user/<?php echo $picpath1; ?>.jpg" class="img-responsive" alt=""> 
                                                                            <?php } ?>
                                                                        </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Select image </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="user_pict"> </span>
                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix margin-top-10">
                                                                        <span class="label label-danger">NOTE! </span>
                                                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                    </div>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <input type="submit" value="Save Changes" name="pic_sub" class="btn green" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> /> 
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END CHANGE AVATAR TAB -->
                                                        <!-- CHANGE PASSWORD TAB -->
                                                        <div class="tab-pane" id="tab_1_3">
                                                            <form action="" method="post">
                                                                <input type="hidden" name="user_id" value="<?php echo $res->user_id; ?>" />
                                                                <!--<div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" name="old_pass" class="form-control" /> </div>-->
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" name="new_pass" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" name="re_new_pass" class="form-control" /> </div>
                                                                <div class="margin-top-10">
                                                                    <input type="submit" value="Change Password" name="pass_sub" class="btn green" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> /> 
                                                                    <a href="welcome.php" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

</body>

<script>
    function load_add_static_page() {
        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
        $('#add_prop').html(loadng);

        $.ajax({url: 'ajax_lib/ajax_add-static-page.php',
            type: 'post',
            success: function (output) {
                // alert(output);
                $('#add_prop').html(output);
            }
        });
    }
</script>

<script>
    function load_static_edit(inoundData) {
        var pageData = inoundData.split("~");
        var pageName = pageData[0];
        var pageUrl = pageData[1];

        var pageLocation = 'ajax_lib/ajax_' + pageName + '.php';
        // alert(pageLocation);
        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
        $('#add_prop').html(loadng);

        $.ajax({url: pageLocation,
            data: {pageUrlData: pageUrl},
            type: 'post',
            success: function (output) {
                // alert(output);
                $('#add_prop').html(output);
            }
        });
    }
</script>
<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<?php
admin_footer();
?>
</html>
</div>
