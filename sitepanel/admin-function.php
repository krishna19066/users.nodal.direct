<?php
define('SITE_URL_PATH', $_SERVER["DOCUMENT_ROOT"] . '/');
//include "/home/hawthorntech/public_html/stayondiscount.com/include/class/cmsClasses.php";
//include "include/class/cmsClasses.php";
//include "../include/class/cmsClasses.php";
include "/var/www/html/users.nodal.direct/include/class/cmsClasses.php";
$admincustId = $_SESSION['customerID'];
$porpCont = new propertyData();
$cloud_keySel = $porpCont->get_Cloud_AdminDetails($admincustId);
//print_r($cloud_keySel);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$userpic = $cloud_keyData->photo;
?>
<?php

function checkUserLogin() {
    if (!isset($_SESSION['MyAdminUserID']) && !isset($_SESSION['customerID']) && !isset($_SESSION['MyAdminUserType'])) {
        header("location:index.php");
        exit;
    }
} ?>
<?php
function ajaxCssHeader(){ ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />       
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />      
        <link href="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />    
        <link rel="shortcut icon" href="favicon.ico" /> 
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		  <link rel="stylesheet" href="https://www.stayondiscount.com/sitepanelcss/flags.css"> 
<?php } ?>
 <?php       
function ajaxJsFooter(){ ?>
     <div class="page-footer">
            <div class="page-footer-inner"> 2018 &copy; 
                <a target="_blank" href="nodal.direct">Howthorn Technology Pvt Ltd</a> &nbsp;|&nbsp;
                
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>              
        <div class="quick-nav-overlay"></div>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>         
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>           
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/scripts/app.min.js" type="text/javascript"></script>         
        <script src="https://www.stayondiscount.com/sitepanel/assets/pages/scripts/form-wizard-property.js" type="text/javascript"></script>            
        <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>	
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>	
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>      
        <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        
<script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://www.stayondiscount.com/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
  
<?php } ?>

<?php
function adminCss() {
    ?>	
    <meta https-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
   
    <!-- END THEME LAYOUT STYLES -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-96x96.png">    
      <link href="https://www.stayondiscount.com/sitepanel/intro/introjs.css" rel="stylesheet">
      <script type="text/javascript" src="https://www.stayondiscount.com/sitepanel/intro/intro.js"></script>
	  <link rel="stylesheet" href="https://www.stayondiscount.com/sitepanel/css/flags.css"> 
    <style>
        .clear {
            clear: both;
            content: "";
            display: table;
        }
    </style>
   
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- <script type="text/javascript" src="https://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">-->
    <![CDATA[
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
    ]]>
    </script>
    <?php
}
?>

<?php

function themeheader() {
       
    $admincustId = $_SESSION['customerID'];
    $adminFuncCont = new adminFunction();
    $porpCont = new propertyData();
    ?>
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="welcome.php">
                    <img src="assets/layouts/layout4/img/nodal-logo.jpg" alt="logo" class="logo-default" style="height: 45px;margin-top: 16px;" /> </a>
                <div class="menu-toggler sidebar-toggler">
                    <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
			
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>

            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
			
                <!-- BEGIN HEADER SEARCH BOX -->
                <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                <!-- <form class="search-form" action="page_general_search_2.html" method="GET">
                     <div class="input-group">
                         <input type="text" class="form-control input-sm" placeholder="Search..." name="query">
                         <span class="input-group-btn">
                             <a href="javascript:;" class="btn submit">
                                 <i class="icon-magnifier"></i>
                             </a>
                         </span>
                     </div>
                 </form>-->
                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <?php
                        //  Comment on Notifications Section for later changes
                        ?>
                        <li class="separator hide"> </li>

                        <!-- BEGIN TODO DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-calendar"></i>
                                <?php
                                $newBookNum1 = $adminFuncCont->newBookingNumber(date('Y-m-d'), $admincustId);
                                $booknum = count($newBookNum1);
                                if ($booknum == "0") {
                                    
                                } else {
                                    ?>
                                    <span class="badge badge-primary"><?php echo $booknum; ?></span>
                                    <?php
                                }
                                ?>
                            </a>

                            <ul class="dropdown-menu extended tasks">
                                <li class="external">
                                    <h3>
                                        <span class="bold"><?php echo $booknum; ?> new</span> bookings
                                    </h3>
                                    <a href="manage-booking.php">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                        <?php
                                        foreach ($newBookNum1 as $bookrow) {
                                            ?>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="photo">
                                                        <i class="fa fa-plus"></i>
                                                    </span>
                                                    <span class="subject">
                                                        <span class="from"> Name : </span>
                                                        <span class="time"><?php echo $bookrow->name; ?></span>
                                                    </span>
                                                    <span class="message"><?php echo $bookrow->property_enquired; ?></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END TODO DROPDOWN -->
                        <!-- BEGIN INBOX DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-envelope-open"></i>
                                <?php
                                $inquiryNum1 = $adminFuncCont->newInquiryNumber('0', $admincustId);
                                $cnn = count($newBookNum1);

                                if ($cnn == "0") {
                                    
                                } else {
                                    ?>
                                    <span class="badge badge-danger"> <?php echo $cnn; ?> </span>
                                    <?php
                                }
                                ?>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>You have
                                        <span class="bold"><?php echo $cnn; ?></span> Inquiries Unattended </h3>
                                    <a href="manage-enquiry.php">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                        <?php
                                        $inquiryData = $adminFuncCont->getInquiryData('',$admincustId, 'order by slno desc limit 5');
                                        foreach ($inquiryData as $inr1) {
                                            $nam = $inr1->name;
                                            $redat = $inr1->recvDate;
                                            $inty = $inr1->propertyName;
                                            ?>
                                            <li>
                                                <a href="#">
                                                    <span class="subject">
                                                        <span class="from"> <?php echo $nam; ?> </span>
                                                        <span class="time"><?php echo $redat; ?></span>
                                                    </span>
                                                    <?php
                                                    if ($inty == "") {
                                                        
                                                    } else {
                                                        ?>
                                                        <span class="message"><?php echo $inty; ?></span>
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END INBOX DROPDOWN -->
                        <li class="separator hide"> </li>



                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user dropdown-dark">
                            <?php
                            $id1 = $_SESSION['MyAdminUserID'];

                            $picrow = $adminFuncCont->getUserData($id1, $admincustId);
                            $cloud_keySel = $porpCont->get_Cloud_AdminDetails($admincustId);
                            //print_r($cloud_keySel);
//print_r($cloud_keySel);
                            $cloud_keyData = $cloud_keySel[0];
                            $cloud_cdnName = $cloud_keyData->cloud_name;
                            $userpic = $cloud_keyData->photo;

                            if ($userpic == '') {
                                ?>
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php echo $id1; ?>  </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="<?php echo $id1; ?>" class="img-circle" src="https://u.o0bc.com/avatars/no-user-image.gif" /> 
                                </a>
                                <?php
                            } else {
                                ?>
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php echo $id1; ?>  </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="<?php echo $res->user_id; ?>" class="img-circle" src="https://res.cloudinary.com/<?php echo $cloud_cdnName ?>/image/upload/w_150,h_150,c_thumb/reputize/admin_user/<?php echo $userpic; ?>.jpg" /> 
                                </a>
                                <?php
                            }
                            ?>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="user_profile.php">
                                        <i class="icon-user"></i> My Profile 
                                    </a>
                                </li>

                                <li>
                                    <a href="user_profile.php#tab_1_3">
                                        <i class="icon-calendar"></i> Change Password
                                    </a>
                                </li>

                                <?php
                                if (isset($_SESSION['MyAdminUserType'])) {
                                    $forMenushowType = $_SESSION['MyAdminUserType'];
                                    ?>
                                    <!-- <li>
                                         <a href="all-invoices.php">
                                             <i class="icon-envelope-open"></i> My Invoices
                                             <span class="badge badge-danger"> 3 </span>
                                         </a>
                                     </li>-->
                                    <?php
                                }
                                ?>

                                <!--<li>
                                        <a href="app_todo_2.html">
                                                <i class="icon-rocket"></i> My Tasks
                                                <span class="badge badge-success"> 7 </span>
                                        </a>
                                </li>-->

                                <li class="divider"> </li>

                                <!-- <li>
                                     <a href="user_lock.php?<?php echo $id1; ?>">
                                         <i class="icon-lock"></i> Lock Screen </a>
                                 </li>-->

                                <li>
                                    <a href="index.php?id1=Logout">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <?php
}
?>
<?php

function menuAdmintype() { ?>
   <?php
    $admincustId = $_SESSION['customerID'];
    $porpCont = new propertyData();
    $super_admin_1 = $porpCont->GetSupperAdminData($admincustId);
    $super_admin = $super_admin_1[0];
    $multi_vendor = $super_admin->multi_vendor;
    // print_r($super_admin_1);
    $userType = $_SESSION['MyAdminUserType'];
    $na = strtok($_SERVER["REQUEST_URI"], '?');
    $multi_vendor = $super_admin->multi_vendor;
    $exp_section = $super_admin->experience_section;
	$service_section = $super_admin->service_section;
    // $na =  "{$_SERVER['REQUEST_URI']}";
    //echo $file =  $_SERVER['SCRIPT_NAME'];
	 $cloud_keySel = $porpCont->get_Cloud_AdminDetails($admincustId);                          
     $cloud_keyData = $cloud_keySel[0];
	 $language = $cloud_keyData->language;
	 switch ($language) {
		case "English":
			$lan = 'en';
			break;
		case "French":
			$lan = 'fr';
			break;
		case "German":
			$lan = 'de';
			break;
		case "Spanish":
			$lan = 'es';
			break;
		case "Japanese":
			$lan = 'ja';
			break;
		default:
			$lan = 'en';
	}
    ?>
    <li class="nav-item start <?php if ($na == "/sitepanel/welcome.php") { ?> active open <?php } ?> ">
        <a href="javascript:;" class="nav-link nav-toggle" data-step="1" data-intro="Bussiness dashboard you can see latest inquiry graph etc.!">
            <i class="icon-home"></i>
            <span class="title">Business Dashboard</span>
            <span class="selected"></span>
            <span class="arrow open"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item start <?php if ($na == "/sitepanel/welcome.php") { ?> active open <?php } ?> ">
                <a href="welcome.php" class="nav-link ">
                    <span class="title">Business Dashboard </span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
    </li>
	 <?php if ($service_section != 'Y') { ?>
    <li class="nav-item start <?php if ($na == "/sitepanel/manage-property.php" || $na == "/sitepanel/manage-property-settings.php" || $na == "/sitepanel/manage-service.php" || $na == "/sitepanel/ajax_add-city.php" || $na == "/sitepanel/ajax_add_subcity.php" || $na == "/sitepanel/ajax_add-roomtype.php" || $na == "/sitepanel/ajax_edit_city.php" || $na == "/sitepanel/ajax_content_city.php" || $na == "/sitepanel/ajax_edit_subcity.php" || $na == "/sitepanel/ajax_edit_roomType.php" || $na == "/sitepanel/ajax_add-property.php" || $na == "/sitepanel/ajax_view-property-photo.php" || $na == "/sitepanel/img-sequence.php" || $na == "/sitepanel/ajax_manage-question.php" || $na == "/sitepanel/manage-question.php" || $na == "/sitepanel/ajax_awards.php" || $na == "/sitepanel/ajax_foodmenu.php" || $na == "/sitepanel/ajax_trip_advisor.php" || $na == "/sitepanel/ajax_add-rooms.php" || $na == "/sitepanel/ajax_view-rooms.php" || $na == "/sitepanel/edit-room.php" || $na == "/sitepanel/add-room-rate.php" || $na == "/sitepanel/ajax_edit-property.php") { ?> active open <?php } ?> ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-building"></i>
            <span class="title">My Properties</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">				
            <li class="nav-item <?php if ($na == "/sitepanel/manage-property.php" || $na == "/sitepanel/ajax_add-property.php" || $na == "/sitepanel/ajax_view-property-photo.php" || $na == "/sitepanel/img-sequence.php" || $na == "/sitepanel/ajax_manage-question.php" || $na == "/sitepanel/manage-question.php" || $na == "/sitepanel/ajax_awards.php" || $na == "/sitepanel/ajax_foodmenu.php" || $na == "/sitepanel/ajax_trip_advisor.php" || $na == "/sitepanel/ajax_add-rooms.php" || $na == "/sitepanel/ajax_view-rooms.php" || $na == "/sitepanel/edit-room.php" || $na == "/sitepanel/add-room-rate.php" || $na == "/sitepanel/ajax_edit-property.php") { ?> active <?php } ?>  ">
                <a href="manage-property.php" class="nav-link" >
                    <span class="title">Property</span>
                </a>
            </li>			
            <?php if ($userType != 'user') { ?>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-property-settings.php" || $na == "/sitepanel/ajax_add-city.php" || $na == "/sitepanel/ajax_add_subcity.php" || $na == "/sitepanel/ajax_add-roomtype.php" || $na == "/sitepanel/ajax_edit_city.php" || $na == "/sitepanel/ajax_content_city.php" || $na == "/sitepanel/ajax_edit_subcity.php" || $na == "/sitepanel/ajax_edit_roomType.php") { ?> active <?php } ?>  ">
                    <a href="manage-property-settings.php" class="nav-link ">
                        <span class="title">Settings</span>
                    </a>
                </li>
            <?php } ?>			
        </ul>
    </li>
	<?php } ?>
	  <?php if ($service_section == 'Y') { ?>
        <li class="nav-item start <?php if ($na == "/sitepanel/manage-service.php"  || $na == "/sitepanel/service_view_photo.php?serviceID" || $na == "/sitepanel/edit-service.php?serviceID") { ?> active open <?php } ?> ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-building"></i>
                <span class="title">My Listing</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">         		
                <li class="nav-item <?php if ($na == "/sitepanel/manage-service.php" ||  $na == "/sitepanel/ajax_add_subcity.php") { ?> active <?php } ?>  ">
                    <a href="manage-service.php" class="nav-link ">
                        <span class="title">Listing</span>
                    </a>
                </li> 
					<li class="nav-item <?php if ($na == "/sitepanel/manage-service.php" ||  $na == "/sitepanel/ajax_add_subcity.php") { ?> active <?php } ?>  ">
                    <a href="user-listing.php" class="nav-link">
                        <span class="title">Users</span>
                    </a>
                </li>  				
            </ul>
        </li>
    <?php } ?>
    <li class="nav-item <?php if ($na == "/sitepanel/manage-enquiry.php" || $na == "/sitepanel/manage-email-subscribe.php") { ?> active <?php } ?>  ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-users"></i>
            <span class="title">Manage Sales & Lead</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <?php if ($userType != 'user') { ?>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-enquiry.php") { ?> active <?php } ?>  ">
                    <a href="manage-enquiry.php" class="nav-link ">
                        <span class="title">Inquiries & Campaigns</span>
                    </a>
                </li>
            <?php } else { ?>
                <li class="nav-item <?php if ($na == "/sitepanel/user-manage-enquiry.php") { ?> active <?php } ?>  ">
                    <a href="user-manage-enquiry.php" class="nav-link ">
                        <span class="title">Inquiries & Campaigns</span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($userType != 'user') { ?>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-email-subscribe.php") { ?> active <?php } ?>  ">
                    <a href="manage-email-subscribe.php" class="nav-link ">
                        <span class="title">Email Subscribe</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </li>
    <?php if ($userType != 'user') { ?>
        <li class="nav-item <?php if ($na == "/sitepanel/manage-static-pages.php" || $na == "/sitepanel/manage-home-page.php" || $na == "/sitepanel/manage-slider.php" || $na == "/sitepanel/ajax_edit_client.php" || $na == "/sitepanel/ajax_edit_aboutUs.php" || $na == "/sitepanel/ajax_edit_contactus.php" || $na == "/sitepanel/ajax_edit_ourclient.php" || $na == "/sitepanel/ajax_edit_policy.php" || $na == "/sitepanel/ajax_edit_t_and_c.php" || $na == "/sitepanel/ajax_edit_food.php" || $na == "/sitepanel/ajax_edit_whyperch.php" || $na == "/sitepanel/ajax_edit_gallery.php") { ?> active open <?php } ?> ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-file-text"></i>
                <span class="title">Home & Static Page</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item <?php if ($na == "/sitepanel/manage-static-pages.php" || $na == "/sitepanel/ajax_edit_client.php" || $na == "/sitepanel/ajax_edit_aboutUs.php" || $na == "/sitepanel/ajax_edit_contactus.php" || $na == "/sitepanel/ajax_edit_ourclient.php" || $na == "/sitepanel/ajax_edit_policy.php" || $na == "/sitepanel/ajax_edit_t_and_c.php" || $na == "/sitepanel/ajax_edit_food.php" || $na == "/sitepanel/ajax_edit_whyperch.php" || $na == "/sitepanel/ajax_edit_gallery.php") { ?> active <?php } ?>">
                    <a href="manage-static-pages.php" class="nav-link ">
                        <span class="title">Static Pages</span>
                    </a>
                </li>

                <li class="nav-item <?php if ($na == "/sitepanel/manage-home-page.php") { ?> active <?php } ?> ">
                    <a href="manage-home-page.php" class="nav-link ">
                        <span class="title">Home Page</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-slider.php") { ?> active <?php } ?>  ">
                    <a href="manage-slider.php" class="nav-link ">
                        <span class="title">Home Page Slider</span>
                    </a>
                </li>

                <!-- <li class="nav-item ">
                     <a href="manage-language.php" class="nav-link ">
                         <span class="title">Multi-Language</span>
                     </a>
                 </li>-->
            </ul>
        </li>
    <?php } ?>
    <li class="nav-item <?php if ($na == "/sitepanel/manage_inventory.php" || $na == "/sitepanel/manage-discounts.php" || $na == "/sitepanel/manage-booking.php" || $na == "/sitepanel/add-googlesheet-url.php" || $na == "/sitepanel/manage_rates.php" || $na == "/sitepanel/booking-engine.php") { ?> active open <?php } ?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-money"></i>
            <span class="title">Revenue Management</span>
            <span class="arrow"></span>
        </a>

        <ul class="sub-menu">
		 <?php if ($service_section != 'Y') { ?>
            <li class="nav-item <?php if ($na == "/sitepanel/manage_inventory.php" || $na == "/sitepanel/manage_rates.php") { ?> active <?php } ?> ">
                <a href="manage_inventory.php" class="nav-link">
                    <span class="title">Inventory & Rates</span>
                </a>
            </li>
            <li class="nav-item <?php if ($na == "/sitepanel/manage-discounts.php") { ?> active <?php } ?>  ">
                <a href="manage-discounts.php" class="nav-link">
                    <span class="title">Discount & Offers</span>
                </a>
            </li>          
            <li class="nav-item">
                <a href="booking-engine.php" class="nav-link">
                    <span class="title">Channel Manager</span>
                </a>
            </li>
			 <?php } ?>
			 <?php if ($userType != 'user') { ?>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-booking.php") { ?> active <?php } ?>">
                    <a href="manage-booking.php" class="nav-link">
                        <span class="title">Manager Booking</span>
                    </a>
                </li>
            <?php } ?>
           <!-- <li class="nav-item <?php if ($na == "/sitepanel/add-googlesheet-url.php") { ?> active <?php } ?>">
                <a href="add-googlesheet-url.php" class="nav-link nav-toggle">
                    <span class="title">Add Google Sheet Url</span>
                </a>
            </li>-->
		
        </ul>
    </li>
    <?php if ($userType != 'user') { ?>
        <li class="nav-item <?php if ($na == "/sitepanel/coming_soon.php" || $na == "/sitepanel/manage-analytic-settings.php" || $na == "/sitepanel/ajax_add_analytic_code.php" || $na == "/sitepanel/ajax_add_meta_tag.php") { ?> active open <?php } ?>  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-server"></i>
                <span class="title">Analytics & SEO</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <!-- <li class="nav-item">
                <!--  manage-analytic-code.php-->
                <!--   <a href="coming_soon.php" class="nav-link ">
                      <span class="title">Google Analytics & GMB</span>
                  </a>
              </li>

              <li class="nav-item <?php if ($na == "/sitepanel/manage-analytic-settings.php" || $na == "/sitepanel/ajax_add_analytic_code.php" || $na == "/sitepanel/ajax_add_meta_tag.php") { ?> active <?php } ?>">
                  <a href="manage-analytic-settings.php" class="nav-link ">
                      <span class="title">Settings</span>
                  </a>
              </li>-->
                <li class="nav-item <?php if ($na == "/sitepanel/manage-analytic-settings.php" || $na == "/sitepanel/ajax_add_analytic_code.php" || $na == "/sitepanel/ajax_add_meta_tag.php") { ?> active <?php } ?>">
                    <a href="ajax_add_analytic_code.php" class="nav-link">
                        <span class="title">Add Google Analytics Code</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item <?php if ($na == "/sitepanel/manage_heder.php" || $na == "/sitepanel/add_mail_template.php" || $na == "/sitepanel/manage-misc-setting.php" || $na == "/sitepanel/manage-gen-setting.php" || $na == "/sitepanel/manage_footer.php" || $na == "/sitepanel/theme-color-setting.php" ) { ?> active open <?php } ?> ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user"></i>
                <span class="title">Admin Settings</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">             
                <li class="nav-item <?php if ($na == "/sitepanel/manage_heder.php") { ?> active <?php } ?>  ">
                    <a href="manage_heder.php" class="nav-link ">
                        <span class="title">Menu Navigation</span>
                    </a>
                </li>

                <li class="nav-item <?php if ($na == "/sitepanel/add_mail_template.php") { ?> active <?php } ?> ">
                    <a href="add_mail_template.php" class="nav-link ">
                        <span class="title">Mail Templates</span>
                    </a>
                </li>

                <li class="nav-item <?php if ($na == "/sitepanel/manage-misc-setting.php") { ?> active <?php } ?>  ">
                    <a href="manage-misc-setting.php" class="nav-link ">
                        <span class="title">Chat Visit</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-gen-setting.php") { ?> active <?php } ?>  ">
                    <a href="manage-gen-setting.php" class="nav-link ">
                        <span class="title">Gen Settings</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/theme-color-setting.php") { ?> active <?php } ?>  ">
                    <a href="theme-color-setting.php" class="nav-link ">
                        <span class="title">Theme Color</span>
                    </a>
                </li>
				 
				 <li class="nav-item <?php if ($na == "/sitepanel/site-visibility.php") { ?> active <?php } ?> ">
                    <a href="site-visibility.php" class="nav-link">
                        <span class="title">Site Visibility</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/language-region.php") { ?> active <?php } ?> ">
                    <a href="language-region.php" class="nav-link">
                        <span class="title">Language & Region</span>
                    </a>
                </li>

                <li class="nav-item <?php if ($na == "/sitepanel/social-link.php") { ?> active <?php } ?> ">
                    <a href="social-link.php" class="nav-link">
                        <span class="title">Social Link</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/billing-account.php") { ?> active <?php } ?> ">
                    <a href="billing-account.php" class="nav-link">
                        <span class="title">Billing Account</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/business-information.php") { ?> active <?php } ?> ">
                    <a href="business-information.php" class="nav-link">
                        <span class="title">Business Information</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/cookies-banner.php") { ?> active <?php } ?> ">
                    <a href="cookies-banner.php" class="nav-link">
                        <span class="title">Cookies banner</span>
                    </a>
                </li>
                <!-- <li class="nav-item  ">
                     <a href="manage-review.php" class="nav-link ">
                         <span class="title"> Manage Review</span>
                     </a>
                 </li>-->
            </ul>
        </li>
        <li class="nav-item <?php if ($na == "/sitepanel/manage-testimonial.php" || $na == "/sitepanel/testimonial-category.php" || $na == '/sitepanel/manage-video-review.php') { ?> active open <?php } ?> ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-quote-left" aria-hidden="true"></i>
                <span class="title">Testimonial</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item <?php if ($na == "/sitepanel/manage-testimonial.php") { ?> active <?php } ?>">
                    <!--  manage-analytic-code.php-->
                    <a href="manage-testimonial.php" class="nav-link ">
                        <span class="title">Add Testimonial</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/manage-video-review.php") { ?> active <?php } ?>">
                    <!--  manage-analytic-code.php-->
                    <a href="manage-video-review.php" class="nav-link ">
                        <span class="title">Add Video Review</span>
                    </a>
                </li>
                <li class="nav-item <?php if ($na == "/sitepanel/testimonial-category.php") { ?> active <?php } ?>">
                    <a href="testimonial-category.php" class="nav-link ">
                        <span class="title">Review Category</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php if ($exp_section == 'Y') { ?>
            <li class="nav-item <?php if ($na == "/sitepanel/manage-experience.php" || $na == "/sitepanel/experience-category.php" || $na == '/sitepanel/experience-view-photo.php') { ?> active open <?php } ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-quote-left" aria-hidden="true"></i>
                    <span class="title">Experience</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if ($na == "/sitepanel/manage-experience.php") { ?> active <?php } ?>">
                        <!--  manage-analytic-code.php-->
                        <a href="manage-experience.php" class="nav-link">
                            <span class="title">Add Experience</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if ($na == "/sitepanel/experience-category.php") { ?> active <?php } ?>">
                        <a href="experience-category.php" class="nav-link">
                            <span class="title"> Add Experience Category</span>
                        </a>
                    </li>
                </ul>
            </li>
        <?php } ?>
    <?php } ?>
    <li class="nav-item <?php if ($na == "/sitepanel/nodal-video.php") { ?> active open <?php } ?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-question-circle"></i>
            <span class="title">Help & FAQ</span>
            <span class="arrow"></span>
        </a>

        <ul class="sub-menu">
            <li class="nav-item">
                <a href="coming_soon.php" class="nav-link ">
                    <span class="title">FAQ</span>
                </a>
            </li>

            <li class="nav-item ">
                <a href="coming_soon.php" class="nav-link ">
                    <span class="title">Raise Ticket</span>
                </a>
            </li>

            <li class="nav-item <?php if ($na == "/sitepanel/nodal-video.php") { ?> active <?php } ?>">
                <a href="nodal-video.php" class="nav-link ">
                    <span class="title">Videos</span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="coming_soon.php" class="nav-link ">
                    <span class="title">Nodal Journey</span>
                </a>
            </li>
        </ul>
    </li>
    <?php if ($userType != 'user') { ?>
        <?php if ($multi_vendor == 'YES') { ?>
            <li class="nav-item <?php if ($na == "/sitepanel/add_user.php") { ?> active open <?php } ?> || <?php if ($na == "/sitepanel/view_user.php") { ?> active <?php } ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-question-circle"></i>
                    <span class="title">User Section</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">            
                    <li class="nav-item <?php if ($na == "/sitepanel/add_user.php") { ?> active <?php } ?>">
                        <a href="add_user.php" class="nav-link ">
                            <span class="title">Add User</span>
                        </a>
                    </li>

                    <li class="nav-item <?php if ($na == "/sitepanel/view_user.php") { ?> active <?php } ?>  ">
                        <a href="view_user.php" class="nav-link ">
                            <span class="title">View User</span>
                        </a>
                    </li>
                </ul>
            </li>
        <?php } ?>
        <li class="nav-item <?php if ($na == "/sitepanel/MailChimp.php") { ?> active open <?php } ?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-question-circle"></i>
                <span class="title">Third Party API</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="manage-3rd-api.php" class="nav-link">
                        <span class="title">API Integration</span>
                    </a>
                </li>
                <!-- <li class="nav-item">
                     <a href="MailChimp.php" class="nav-link">
                         <span class="title">MailChimp</span>
                     </a>
                 </li>

                 <li class="nav-item">
                     <a href="google-my-business.php" class="nav-link">
                         <span class="title">Google My Business</span>
                     </a>
                 </li> -->        
            </ul>
        </li>
		<style>
     .skiptranslate { display:none; } 
         </style>
                   			
		 <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-language" aria-hidden="true"></i>
                <span class="title">Change Language</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu ">
                <div id="google_translate_element" style="display: none"></div><script type="text/javascript">
                    function googleTranslateElementInit() {
                        new google.translate.TranslateElement({pageLanguage: '<?php echo $lan; ?>', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                    }
                </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>                                         
                <div class="nav-holder main-menu dropdown" style="margin-top: -19px;">
                    <nav style="margin-top:20px;">
                        <li style="display:block;">
                            <a href="#"  id="dLabel" class="act-link notranslate btn dropdown-toggle" style="border: 1px solid;"><img class="flag flag-<?php echo $lan; ?>"> <?php echo $language; ?>  <i class="fa fa-caret-down"></i></a>
                            <!--second level -->
                            <ul class="dropdown-menu language_dropdown sub-menu">
                                <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="English" ><img class="flag flag-en"> English </a> </li>
                                <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="French" ><img class="flag flag-fr">French</a></li>                           
                                <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="German" ><img class="flag flag-de">German</a></li>
                                <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="Spanish" ><img class="flag flag-es">Spanish </a></li>                         
                                <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="Japanese" ><img class="flag flag-ja"> Japanese</a></li>                         
                               
                            </ul>            
                        </li>
                    </nav>
                </div>

                <script>
                    $(".dropdown-menu li a").click(function () {
                        var selText = $(this).text();
                        var img = $(this).parent().find("img").attr("class");
                        $(this).parents('.dropdown').find('.dropdown-toggle').html('<img class="' + img + '">' + selText + ' <span class="caret"></span>');
                    });
                    function translate(lang) {
                        var $frame = $('.goog-te-menu-frame:first');
                        if (!$frame.size()) {
                            alert("Error: Could not find Google translate frame.");
                            return false;
                        }
                        $frame.contents().find('.goog-te-menu2-item span.text:contains(' + lang + ')').get(0).click();
                        return false;
                    }
                    $(".dropdown-menu.language_dropdown").on("click", "a", function () {
                        translate($(this).attr("id"));
                    });
                </script>       
            </ul>
        </li>
    <?php } ?>
	<?php if($admincustId != '49'){ ?>
		<a id="startButton" class="btn btn-large btn-success" href="javascript:void(0);" onclick="startIntro();">Show Me Tutorial</a>
	<?php } ?>
    <!--<a class="btn btn-large btn-success" href="javascript:void(0);" onclick="startIntro();">Show me how</a>-->
 
<?php } ?>

<?php
function admin_header($custNa) {
    $adminFuncCont = new adminFunction();
    $ur1 = $_SERVER['REQUEST_URI'];
    $ur = explode("/", $ur1);
    $na_nw = $ur[2];
    $na_br = explode("?", $na_nw);
    $na = $na_br[0];

    if ($custNa != '') {
        $na = $custNa;
    }
    ?>
    <?php
    if (!isset($_SESSION['MyAdminUserID'])) {
        
    }

    if (isset($_SESSION['MyAdminUserID'])) {
        ?>
        <div class="page-sidebar-wrapper">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->

                <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">			
                    <?php
                    include "../include/arrays.inc.php";
                    $id = $_SESSION['MyAdminUserID'];

                    $admincustId = $_SESSION['customerID'];
                    $lanarr = $adminFuncCont->getLangData($admincustId, '(status="Y"||status="N")');
                    $lanarray = explode("^", $lanarr);

                    $row = $adminFuncCont->getUserData($id, $admincustId);

                    //echo $value."<br>";
                    $lan = $_SERVER['QUERY_STRING'];
                    
                        $type = $row->user_type;

                        if ($type == "admin") {
                            menuAdmintype($na);
                        } else {
                            $cont = $row->section;
                            $a = explode("^", $cont);
                            foreach ($a as $value) {
                                $t = $admin_section[$value];
                                //menuUsertype($t, $na);
								menuAdmintype($na);
                            }
                        }
                     
                    ?>
                </ul>
            </div>
        </div>

        <?php
    }
}
?>



<?php

function admin_footer() {
    ?>

    <div id="footer">
        <div class="copyright"> 2018 © Hawthorn Technologies Pvt. Ltd.&nbsp;All Rights Reserved. <br /></div> 
    </div>
    <?php
    $inquiryGrapCont = new inquiryGraph();
    $jan_data = $inquiryGrapCont->getInquiryGraphData('01', $admincustId);
    $feb_data = $inquiryGrapCont->getInquiryGraphData('02', $admincustId);
    $mar_data = $inquiryGrapCont->getInquiryGraphData('03', $admincustId);
    $apr_data = $inquiryGrapCont->getInquiryGraphData('04', $admincustId);
    $may_data = $inquiryGrapCont->getInquiryGraphData('05', $admincustId);
    $jun_data = $inquiryGrapCont->getInquiryGraphData('06', $admincustId);
    $jul_data = $inquiryGrapCont->getInquiryGraphData('07', $admincustId);
    $aug_data = $inquiryGrapCont->getInquiryGraphData('08', $admincustId);
    $sep_data = $inquiryGrapCont->getInquiryGraphData('09', $admincustId);
    $oct_data = $inquiryGrapCont->getInquiryGraphData('10', $admincustId);
    $nov_data = $inquiryGrapCont->getInquiryGraphData('11', $admincustId);
    $dec_data = $inquiryGrapCont->getInquiryGraphData('12', $admincustId);
    $anb = "[
							['JAN', $jan_data],
							['FEB', $feb_data],
							['MAR', $mar_data],
							['APR', $apr_data],
							['MAY', $may_data],
							['JUN', $jun_data],
							['JUL', $jul_data],
							['AUG', $aug_data],
							['SEP', $sep_data],
							['OCT', $oct_data],
							['NOV', $nov_data],
							['DEC', $dec_data]
						]";
    echo '<script>var arrayFromPhp = ' . $anb . ';</script>';
    ?>
     <!-- BEGIN CORE PLUGINS -->
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="https://www.stayondiscount.com/sitepanel/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="https://www.stayondiscount.com/sitepanel/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
      <script type="text/javascript">
      function startIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                intro: "Welcome To Nadal Direct Bussiness board!"
              },
              { 
                intro: "First of all add city <b>Side menu->My Property->setting->Add City </b>"
              },
              { 
                intro: " add Room Type <b>Side menu->My Property->setting->Add Room type, like 1BHK,2BHL etc.  </b>"
              },
              { 
                intro: "add property<b>Side menu->My Property->Property->Add Property </b>"
              }
             
            ]
          });
          intro.start();
      }
    </script>

    <?php
}
?>

<?php

function admin_change_pass() {
    // $res=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");
    ?>
    <script type="text/javascript">
        function validate_form() {
            var frm = document.chgPwd;
            if (frm.username.value == 0) {
                alert("Please enter username");
                frm.username.focus();
                return false;
            }
            if (frm.old_password.value == 0) {
                alert("Please enter your current password");
                frm.old_password.focus();
                return false;
            }
            if (frm.password.value == 0) {
                alert("Please enter your new password");
                frm.password.focus();
                return false;
            }
            if (frm.repassword.value == 0) {
                alert("Please confirm your new password");
                frm.repassword.focus();
                return false;
            }
            if (frm.password.value != frm.repassword.value) {
                alert("Password and Confirm Password mismatched");
                frm.repassword.focus();
                return false;
            }
        }
    </script>

    <form action="" method="post" name="chgPwd" id="chgPwd" onsubmit="return validate_form();">
        <table width="550" border="0" align="center" cellpadding="0" cellspacing="0" class="tableForm"> 
            <tr>
                <td class="tdLabel" colspan="2" align="center">
                    <?php
                    if ($_SESSION[session_message] != '') {
                        echo '<font color="#C05813">' . print_message() . '</font><br>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td width="120" class="tdLabel">Username:<font color="red">*</font></td>
                <td><input type="text" name="username" value="<?= $res[user_id]; ?>" class="textfield"></td>
            </tr>

            <tr>
                <td width="120" class="tdLabel">Current Password:<font color="red">*</font></td>
                <td><input type="password" name="old_password" class="textfield" /></td>
            </tr>

            <tr>
                <td class="tdLabel">New Password:<font color="red">*</font></td>
                <td><input type="password" name="password" class="textfield"></td>
            </tr>

            <tr>
                <td class="tdLabel">Confirm Password:<font color="red">*</font></td>
                <td><input type="password" name="repassword" class="textfield"></td>
            </tr>

            <tr>
                <td class="label">&nbsp;</td>
                <td>
                    <input type="image" name="imageField" src="images/buttons/submit.gif" />
                    <input type="hidden" name="action" value="Update">
                </td>
            </tr> 
        </table> 
    </form>
    <?php
}
?>
			