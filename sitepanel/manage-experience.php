<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$propertyCont = new propertyData();
$recordID = $_REQUEST[recordID];
//$roomID = $_REQUEST[roomID];
$id = $_REQUEST['id'];
?>
<?php
@extract($_REQUEST);
function slugify($text){
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
      return 'n-a';
    }
    return $text;
}

//------------------------Add Testimonial---------------------------------------------------->
if (isset($_POST['Add_experience'])) {
 
    $cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    $record_type = "Property Photo";
    $script_name = "view-property-photo.php";
    $table_name = "propertyImages";
    $slug_url = slugify($name);

    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
    $review = $_POST['review'];
    $name = $_POST['name'];
    $categoryName_1 = $_POST['categoryName'];
    $categoryName = implode(',', $categoryName_1);

    $ins = $propertyCont->AddExperienceData($customerId, $name, $categoryName, $city, $state, $rate, $from_date, $to_date, $exp_content,$slug_url);

    echo '<script type="text/javascript">
                alert("Succesfuly Added Exprience");              
window.location = "manage-experience.php";
            </script>';
    exit;
}
if (isset($_POST['update_testimonial'])) {

    $cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
    $cloud_keyData = $cloud_keySel[0];
    $cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';

    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));

    $file = $_FILES['image_org']['name'];
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;

    if ($file) {
        $upload = \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/testimonials"));
    }
    $review = $_POST['review'];
    $occupation = $_POST['occupation'];
    $name = $_POST['name'];
    $update_testi = $propertyCont->UpdateTestimonialData($recordID, $timestmp);
    echo '<script type="text/javascript">
                alert("Succesfuly Updated ");              
window.location = "edit-testimonials.php?id=edit&recordID=' . $recordID . '";
            </script>';
}
//admin_header();	
?>

<html>
    <head>
        <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">

                                <br><br><br>
                                <div class="portlet light bordered" style="height: 137px;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Add Experience</span>
                                            <span class="caption-helper">Add Experience</span>
                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="!Add Experience Those You Show your website!" data-original-title="Add Experience" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_experience').slideToggle();" value="Add Experience" /></span>
                                </div>

                                <div class="portlet light bordered" id="add_experience" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">                                               
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="name" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide Reviewer Name</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">City
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="city" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->city); ?>" required="" />
                                                        <span class="help-block"> Provide city Name</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">State
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="state" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->state); ?>" required="" />
                                                        <span class="help-block"> Provide state Name</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Rate
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input name="rate" type="text" class="form-control" value="<?php echo html_entity_decode($res_testi->rate); ?>" required="" />
                                                        <span class="help-block"> Provide rate</span>
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Select Room
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <table>
                                                            <tr>
                                                                <?php
                                                                //    $selRoom = "select propertyID from propertyTable where propertyName='$property'";
                                                                //   $querRoom = mysql_query($selRoom);
                                                                //   while ($ro = mysql_fetch_array($querRoom)) {
                                                                //      $ide = $ro['propertyID'];
                                                                $cateList1 = $propertyCont->getExperienceCategory($customerId);
                                                                foreach ($cateList1 as $cat) {
                                                                    ?>
                                                                    <td style="padding-left:50px;">
                                                                        <div class="mt-checkbox-inline">
                                                                            <label class="mt-checkbox">
                                                                                <input type="checkbox" name="categoryName[]" id="inlineCheckbox21" value="<?php echo $cat->category_name; ?>" <?php
                                                                                if ($cat->category_name == $room_act) {
                                                                                    echo "checked";
                                                                                }
                                                                                ?>> <?php echo $cat->category_name; ?>
                                                                                <span></span>
                                                                            </label>

                                                                        </div>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                // }
                                                                ?>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Date
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <input name="from_date" type="date" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide From date</span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input name="to_date" type="date" class="form-control" value="<?php echo html_entity_decode($res_testi->name); ?>" required="" />
                                                        <span class="help-block"> Provide To date</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                 <label class="control-label col-md-3"> Image
 
                                                 </label>
                                                 <div class="fileinput fileinput-new" data-provides="fileinput">
                                                     <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                         <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                     <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                     <div>
                                                         <span class="btn default btn-file">
                                                             <span class="fileinput-new"> Select image </span>
                                                             <span class="fileinput-exists"> Change </span>
                                                             <input type="file" name="image_org"> 
                                                         </span>
                                                         <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a><br>
                                                         <b>Selected Photo :</b> 
 
                                                     </div>
                                                 </div>
                                             </div>-->
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">Content</label>
                                                <span class="required"> * </span>
                                                <div class="col-md-9">
                                                    <textarea class="ckeditor form-control" name="exp_content" rows="6" required> <?= $res_testi->exp_content; ?></textarea>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="submit" class="btn green" name="Add_experience" class="button" id="submit" value="Add Experience" <?php if ($mode == 'V') { ?> disabled <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> />

                                                <a href="javascript:history.back()" class="btn red">Go Back</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                            <!---------------------------------------------------------------- Add experience Section Ends --------------------------------------------------------------------->

                            <?php
                            $experienceData = $propertyCont->GetAllExperienceData($customerId);
                            if ($experienceData != NULL) {
                               
                                    ?>

                                    <div class="row">
                                        <div class="col-lg-12 col-xs-12 col-sm-12" style="padding-right: 2px;">										
                                            <?php
                                            $count = 1;
                                            //  $propListing1 = $ajaxpropertyCont->getCityProperty($customerId, $cityName,$user_id);
                                            foreach ($experienceData as $Listing) {
                                                $name = $Listing->name;
                                                $id = $Listing->id;
                                                if ($stat == "Y") {
                                                    $act_stat = "Active";
                                                } else {
                                                    $act_stat = "<span style='color:Red;'>Inactive</span>";
                                                }
                                                if ($feature == "Y") {
                                                    $fea_stat = "<span style='color:Yellow;'>Featured</span>";
                                                } else {
                                                    $fea_stat = "";
                                                }
                                                ?>            
                                                <div class="portlet box mt-list-item" style="background:#2AB4C0;border-bottom:2px solid #2AB4C0;">
                                                    <div class="portlet-title" onclick="$('#data_box<?php echo $count; ?>').slideToggle();" >
                                                        <div class="caption">
                                                            <center>
                                                                <?php echo $name; ?>
                                                            </center>
                                                        </div>
                                                        <div class="tools">
                                                            <span id="toggle_expand<?php echo $count; ?>">Expand</span>  
                                                        </div>
                                                    </div>
                                                    <div id="data_box<?php echo $count; ?>" style="display: none; padding: 20px; background: white; border: 1px solid #2AB4C0;color:#678098;">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-xs-6 col-sm-6" style="padding:0px; border-right:1px solid #74B0DA;">
                                                                <div>
                                                                    <table class="table_prop" style="width:98% ! important;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="width: 125px ! important;table-layout: fixed ! important;">Name </td>
                                                                                <td>: <?php echo $name; ?> </td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Rate </td>
                                                                                <td>: <?php echo $Listing->rate; ?> </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>City</td>
                                                                                <td>: <?php echo $Listing->city; ?></td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">From Date </td>
                                                                                <td>: <?php echo $Listing->from_date; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>State</td>

                                                                                <td>: <?php echo $Listing->state; ?></td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">To Date </td>

                                                                                <td>: <?php echo $Listing->to_date; ?> </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td></td>

                                                                                <td></td>
                                                                                <td style="text-align:left; border-left:1px solid rgba(153, 153, 153, 0.21);padding-left: 20px;">Exp.Add </td>
                                                                                <td>: <?php
                                                                                    $timestamp = $Listing->timestamp;
                                                                                    echo date("jS F, Y", strtotime($timestamp));
                                                                                    ?> </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4"></td>
                                                                            </tr>
                                                                            <tr style="border-bottom: 1px solid #ccc;">
                                                                                <td colspan="4"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>

                                                                                </td>
                                                                                <td colspan="3">

                                                                                </td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td>Category:</td>
                                                                                <td colspan="3">: 
                                                                                    <?php echo $category = $Listing->category; ?>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <br>
                                                                    <table style="width:100%;border-bottom: 1px solid #ccc;">
                                                                        <tbody>
                                                                            <tr><td></td>
                                                                                
                                                                                <td style="width:60%;">
                                                                                    <div class="btn-toolbar margin-bottom-10">
                                                                                        <div class="btn-group">
                                                                                        </div>
                                                                                        <a  href="edit-experience.php?expID=<?php echo $Listing->id; ?>"><button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit

                                                                                            </button></a>

                                                                                        <button type="button" class="btn btn-danger">
                                                                                            <a <?php if ($mode == 'V') { ?> class="not-active" <?php } ?> <?php if ($mode == 'V') { ?> title="only view mode" <?php } ?> href="experience-edit.php?id=delete&amp;recordID=<?php echo $propid; ?>" style="text-decoration:none;color:white;" onclick="return confirm('Are you sure you want to delete this record')">
                                                                                                <i class="fa fa-trash"></i>
                                                                                                Delete
                                                                                            </a>
                                                                                        </button>
                                                                                    </div><br>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                
                                                                    <p>&nbsp;&nbsp;
                                                                        <a href="experience-view-photo.php?expID=<?php echo $id; ?>">Add/View Photo</a>
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6 col-xs-6 col-sm-6" style="padding-left: 5px;padding-right: 5px;">
                                                                <h4> Content </h4><hr/>

                                                                <?php echo $Listing->exp_content; ?>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>	
                                                <?php
                                                $count++;
                                        }
                                       
                                    } else {
                                        ?>
                                        <table width="100%"  border="0" cellpadding="6" cellspacing="2" class="lightBorder">
                                            <tr><td><br /><b>No Record Found.............</b><br /><br /></td></tr>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </body>
                </html>
                <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                <!-- END PAGE LEVEL PLUGINS -->
                <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                <?php
                admin_footer();
                ?>