<?php
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$id = $_GET['id'];
$propertyID = $_REQUEST['recordID'];
if ($id == "delete") {
    $quesID = $_GET['quesID1'];
    $propertyID = $_REQUEST['recordID'];
    $upd = $propertyCont->DeleteQuestion($quesID);
    //Header("location:manage-property.php");
    header("location:ajax_manage-question.php?propID=" . $propertyID);
}
if ($id == 'edit') {
    $quesID = $_GET['qid'];
    $getQuestionData = $propertyCont->GetQuestionData($quesID);
    // print_r($getQuestionData);
    foreach ($getQuestionData as $data) {
        
    }
    if (isset($_POST['upd_question'])) {
        $question = $_POST['question'];
        $answer = $_POST['answer'];
        $propertyID = $_REQUEST['recordID'];
        $update = $propertyCont->UpdateQuesAns($question, $answer, $quesID);
        header("location:ajax_manage-question.php?propID=" . $propertyID);
    }
}
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <?php echo ajaxCssHeader(); ?>      
            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>


            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Manage Property Question
                                    <small>Edit Question</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">My Properties</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Property Question</span>
                            </li>
                        </ul>   
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Edit Questions" /></span>
                                            
                                            <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>
                                            <span style="float:right;"> <a href="ajax_manage-question.php?propID=<?php echo $propertyID; ?>"><button style="background:#36c6d3;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px; margin-right: 12px;"><i class="fa fa-plus"></i> Add Question </button></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body form" id="add_photo">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" action="" name="register_member_form" method="post" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Question
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="question"  value="<?php echo $data->question; ?>"   />
                                            <input type="hidden" value="<?php echo $quesID; ?>" name="questionID">

                                        </div>
                                    </div>

                                    <div class="form-group last">
                                        <label class="control-label col-md-3">Answer</label>
                                        <div class="col-md-9">
                                            <textarea class="ckeditor form-control" name="answer" rows="6" value="<?php echo $data->answer; ?>"><?php echo $data->answer; ?> </textarea>
                                        </div>
                                    </div>                
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input name="recordID" type="hidden" value="<?php echo $propertyID; ?>" />
                                                <input name="update_question" type="hidden" value="submit" />
                                                <input type="submit" class="btn green" name="upd_question" class="button" id="submit" value="UpdateProperty Question" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>          
            <?php echo ajaxJsFooter(); ?>
            </div>	