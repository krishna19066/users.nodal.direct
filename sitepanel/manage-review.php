<?php
//include "/home/hawthorntech/public_html/stayondiscount.com/sitepanel/admin-function.php";
include "admin-function.php";
checkUserLogin();
$customerId = $_SESSION['customerID'];
$ajaxclientCont = new staticPageData();
$reviewData = $ajaxclientCont->getCustomerReview($customerId);
//print_r($reviewData);
if (isset($_REQUEST['ActiveReview'])) {
    $status = 'Active';
    $reviewID = $_REQUEST['reviewID'];
    $upd = $ajaxclientCont->UpdateReviewStatus($customerId, $reviewID, $status);
    echo '<script type="text/javascript">
                alert("Succesfuly Activate Review");              
window.location = "manage-review.php";
            </script>';
}
if (isset($_REQUEST['InactiveReview'])) {
    $status = 'Inactive';
    $reviewID = $_REQUEST['reviewID'];
    $upd = $ajaxclientCont->UpdateReviewStatus($customerId, $reviewID, $status);
    echo '<script type="text/javascript">
                alert("Succesfuly Inactivate Review");              
window.location = "manage-review.php";
            </script>';
}
?>
<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <?php
            adminCss();
            ?>
        </head>
        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE REVIEW
                                    <small>View and manage Rview</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="welcome.php">Manage Review</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Review</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase">Manage Review </span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <!-- <div class="pull-right">
                                             <div class="btn-group">
                                                 <button type="button" class="btn btn-success btn-filter" data-target="All">All</button>
                                                 <button type="button" class="btn btn-warning btn-filter" data-target="Active">Active</button>
                                                 <button type="button" class="btn btn-danger btn-filter" data-target="Inactive">Inactive</button>
                                        <!--<button type="button" class="btn btn-default btn-filter" data-target="all">Todos</button>-->
                                        <!-- </div>
                                     </div>-->

                                        <table class="table table-filter">
                                            <tbody>
                                                <?php
                                                If ($reviewData != NULL) {
                                                    foreach ($reviewData as $res) {
                                                        ?>
                                                        <tr data-status="All">
                                                            <td>
                                                                <div class="ckbox">
                                                                    <form action="" method="POST">
                                                                        <input type="hidden"  name="reviewID" value="<?php echo $res->s_no; ?>">
                                                                        <?php if ($res->status == 'Inactive') { ?>
                                                                            <input type="submit" name="ActiveReview" class="btn btn-warning btn-filter" data-target="Active" value="Active">
                                                                        <?php } else { ?>
                                                                            <input type="submit" name="InactiveReview" class="btn btn-danger btn-filter" data-target="Inctive" value="Inctive">
                                                                        <?php } ?>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:;" class="star">
                                                                    <i> <?php echo $star = $res->rating; ?></i> <i class="glyphicon glyphicon-star"></i>
                                                                </a>
                                                            </td>
                                                            <td style="max-width:0px;">
                                                                <div class="media">
                                                                    <a href="#" class="pull-left">
                                                                       <i class="glyphicon glyphicon-user"></i>
                                                                    </a>
                                                                    <div class="media-body" style="width: auto;">
                                                                        <span class="media-meta pull-right"><?php echo $res->timestamp; ?></span>
                                                                        <h4 class="title">
                                                                            <?php echo $res->name; ?>
                                                                            <span class="pull-right  <?php if ($res->status == 'Inactive') { ?> text-danger <?php } ?>">(<?php echo $res->status; ?>)</span>
                                                                        </h4>

                                                                        <p class="summary"><?php echo $res->review; ?></p>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                <h3>No Record Found</h3>
                                            <?php } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                                   
                    </div>
                </div>
            </div>
</div>
<style>
    /*    --------------------------------------------------
:: General
-------------------------------------------------- */
    body {
        font-family: 'Open Sans', sans-serif;
        color: #353535;
    }
    .content h1 {
        text-align: center;
    }
    .content .content-footer p {
        color: #6d6d6d;
        font-size: 12px;
        text-align: center;
    }
    .content .content-footer p a {
        color: inherit;
        font-weight: bold;
    }

    /*	--------------------------------------------------
            :: Table Filter
            -------------------------------------------------- */
    .panel {
        border: 1px solid #ddd;
        background-color: #fcfcfc;
    }
    .panel .btn-group {
        margin: 15px 0 30px;
    }
    .panel .btn-group .btn {
        transition: background-color .3s ease;
    }
    .table-filter {
        background-color: #fff;
        border-bottom: 1px solid #eee;
    }
    .table-filter tbody tr:hover {
        cursor: pointer;
        background-color: #eee;
    }
    .table-filter tbody tr td {
        padding: 10px;
        vertical-align: middle;
        border-top-color: #eee;
    }
    .table-filter tbody tr.selected td {
        background-color: #eee;
    }
    .table-filter tr td:first-child {
        width: 38px;
    }
    .table-filter tr td:nth-child(2) {
        width: 35px;
    }
    .ckbox {
        position: relative;
    }
    .ckbox input[type="checkbox"] {
        opacity: 0;
    }
    .ckbox label {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .ckbox label:before {
        content: '';
        top: 1px;
        left: 0;
        width: 18px;
        height: 18px;
        display: block;
        position: absolute;
        border-radius: 2px;
        border: 1px solid #bbb;
        background-color: #fff;
    }
    .ckbox input[type="checkbox"]:checked + label:before {
        border-color: #2BBCDE;
        background-color: #2BBCDE;
    }
    .ckbox input[type="checkbox"]:checked + label:after {
        top: 3px;
        left: 3.5px;
        content: '\e013';
        color: #fff;
        font-size: 11px;
        font-family: 'Glyphicons Halflings';
        position: absolute;
    }
    .table-filter .star {
        color: #ccc;
        text-align: center;
        display: block;
    }
    .table-filter .star.star-checked {
        color: #F0AD4E;
    }
    .table-filter .star:hover {
        color: #ccc;
    }
    .table-filter .star.star-checked:hover {
        color: #F0AD4E;
    }
    .table-filter .media-photo {
        width: 35px;
    }
    .table-filter .media-body {
        display: block;
        /* Had to use this style to force the div to expand (wasn't necessary with my bootstrap version 3.3.6) */
    }
    .table-filter .media-meta {
        font-size: 11px;
        color: #999;
    }
    .table-filter .media .title {
        color: #2BBCDE;
        font-size: 14px;
        font-weight: bold;
        line-height: normal;
        margin: 0;
    }
    .table-filter .media .title span {
        font-size: .8em;
        margin-right: 20px;
    }
    .table-filter .media .title span.pagado {
        color: #5cb85c;
    }
    .table-filter .media .title span.pendiente {
        color: #f0ad4e;
    }
    .table-filter .media .title span.cancelado {
        color: #d9534f;
    }
    .table-filter .media .summary {
        font-size: 14px;
    }
</style>
</body>
<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

<!-- BEGIN CORE PLUGINS -->
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://www.theperch.in/sitepanel/assets/global/plugins/jquery.min.js" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://www.theperch.in/sitepanel/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
    function load_static_edit(inoundData) {
        var pageData = inoundData.split("~");
        var pageName = pageData[0];
        var pageUrl = pageData[1];

        var pageLocation = 'ajax_lib/ajax_' + pageName + '.php';
        // alert(pageLocation);
        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
        $('#add_prop').html(loadng);

        $.ajax({url: pageLocation,
            data: {pageUrlData: pageUrl},
            type: 'post',
            success: function (output) {
                // alert(output);
                $('#add_prop').html(output);
            }
        });
    }
    $(document).ready(function () {

        $('.star').on('click', function () {
            $(this).toggleClass('star-checked');
        });

        $('.ckbox label').on('click', function () {
            $(this).parents('tr').toggleClass('selected');
        });

        $('.btn-filter').on('click', function () {
            var $target = $(this).data('target');
            if ($target != 'all') {
                $('.table tr').css('display', 'none');
                $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
            } else {
                $('.table tr').css('display', 'none').fadeIn('slow');
            }
        });

    });
</script>

<?php
admin_footer();
?>
</html>
</div>
