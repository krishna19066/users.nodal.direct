<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$propertyId = 11;
?>
<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        jQuery(function ($) {
            var panelList = $('#draggablePanelList');

            panelList.sortable({
                // Only make the .panel-heading child elements support dragging.
                // Omit this to make then entire <li>...</li> draggable.
                handle: '.panel-heading',
                update: function () {
                    $('.panel', panelList).each(function (index, elem) {
                        var $listItem = $(elem),
                                newIndex = $listItem.index();
                               // alert(newIndex);

                        // Persist the new indices.
                    });
                }
            });
        });

    </script>
    <?php
    adminCss();
    ?>
</head>
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
    <?php
    themeheader();
    ?>
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <?php
        admin_header();
        ?>

        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="background:#e9ecf3;">
                <div class="page-head">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1> PROPERTY DETAILS
                            <small>View all the details of your Properties</small>
                        </h1>
                    </div>
                </div>

                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="welcome.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="welcome.php">My Properties</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>View / Add Property Photo</span>
                    </li>
                </ul>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-body ">
                                <div style="width:100%;" class="clear"> 
                                    <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Property Photo" /></span>

                                    <span style="float:right;"> <a href="manage-property.php"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Back </button></a></span>

                                    <br><br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <ul id="draggablePanelList" class="list-unstyled">

                    <?php
                    $propPhotoListdata = $ajaxpropertyPhotoCont->getPropertyPhoto($propertyId);
                    $ctn = 1;
                    foreach ($propPhotoListdata as $propPhotoList) {
                        ?>
                        <div class="row">
                            <div class="col-xs-8">
                                <li class="panel panel-info">
                                    <div class="panel-heading"><?php echo $propPhotoList->imagesID;  ?></div>
                                    <div class="panel-body"><img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_200,h_100,c_fill/reputize/property/<?php echo $propPhotoList->imageURL; ?>.jpg" /></div>
                                </li>
                            </div>
                        </div>
                        <?php
                        $ctn++;
                    }
                    ?>
                </ul>               
            </div>
            <!-------------------------End----------------------------------------->
        </div>
    </div>


    <style>
        #draggablePanelList .panel-heading {
            cursor: move;
        }
        #draggablePanelList2 .panel-heading {
            cursor: move;
        }
    </style>