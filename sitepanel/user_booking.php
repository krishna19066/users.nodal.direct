<?php
include "admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$propertyData = new propertyData();
$userData = new UserDetails();

$getAllBooking = $userData->get_all_service_booking($customerId);
$reccnt = count($getAllBooking);
?>

<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <?php
            adminCss();
            ?>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
            <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>

            <script>
                $(document).ready(function () {
                    $('#example').DataTable();
                });
            </script>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>         
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Manage Booking
                                    <small>Booking Listing</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>

                            <li>
                                <span>Booking List</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Booking List</span>
                                        </div>
                                        <div class='button'><br>
                                            <a href="#" id ="export" role='button'><button style="background:hsl(166, 78%, 46%);border:none;color:white;width:170px;height:40px;float:right;font-weight:bold;margin-top: -15px;">Export CSV</button></a>
                                            <a href="user-listing.php" role='button'><button style="background:#FFC107;border:none;color:white;width:170px;height:40px;float:right;font-weight:bold;margin-top:-15px; margin-right:18px;">User List</button></a>
                                        </div><br><br>
                                    </div>

                                    <div class="portlet-body">                                     
                                        <div class="row">
                                            <div class="inbox">                                                
                                                <div class="col-md-12" style="padding-left:0px">
                                                    <!-- This is the test file content the links for the footer only page Manage-enquiry.php -->
                                            <?php include 'test.php'; ?>
                                            <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
                                            <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
                                            <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
                                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                                            <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
                                            <script>
                                                $(document).ready(function () {
                                                    $('#example').DataTable();
                                                });
                                            </script>
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th width="2%">Sl No.</th>
                                                                <th width="8%">Booking Date</th>
                                                                <th width="8%">Booking ID</th>
                                                                <th width="9%">Seller</th>
                                                                <th width="9%">User</th>
                                                                <th width="10%">Listing Name</th>     
                                                                <th width="10%">Price($)</th>								
                                                                <th width="8%">Status</th>
                                                                <th width="8%">Actions</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $ctn = 1;
                                                            foreach ($getAllBooking as $res) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $ctn; ?></td>
                                                                    <td>
                                                                        <?php
                                                                        echo $start_date = $res->start_date;
                                                                        ?>
                                                                    </td>
                                                                    <td> <span style="color:green"><?php echo $res->booking_id; ?></span></td>
                                                                    <td>
                                                                        <?php
                                                                        $seller_id = $res->seller_id;
                                                                        $get_seller_data = $userData->get_user_seller($seller_id);
                                                                        foreach ($get_seller_data as $seller) {
                                                                            echo $seller_name = $seller->managerName;
                                                                        }
                                                                        ?>
                                                                    </td>								   
                                                                    <td>
                                                                        <?php
                                                                        $user_id = $res->user_id;
                                                                        $get_user_data = $userData->get_user_seller($user_id);
                                                                        foreach ($get_user_data as $user) {
                                                                            echo $user_name = $user->managerName;
                                                                        }
                                                                        ?>
                                                                    </td>	
                                                                    <td>
                                                                        <?php
                                                                        $service_id = $res->service_id;
                                                                        $get_service_data = $userData->get_service_data($service_id);
                                                                        foreach ($get_service_data as $service) {
                                                                            echo $service_name = $service->serviceName;
                                                                        }
                                                                        ?>
                                                                    </td>	
                                                                    <td>
                                                                        <?php
                                                                        $service_id = $res->service_id;
                                                                        $get_service_data = $userData->get_service_data($service_id);
                                                                        foreach ($get_service_data as $service) {
                                                                            $rate = $service->rate;
                                                                        }
                                                                        $wallafy_rate = ($rate * 6) / 100;
                                                                        $service_pro_rate = $rate - $wallafy_rate;
                                                                        echo 'Total: $' . $rate . '<br/>';
                                                                        echo '<span style="color:green;"> Wallafy: $' . $wallafy_rate . '<span><br/>';
                                                                        echo '<span style="color:blue;">Payable : $' . $service_pro_rate . '<span><br/>';
                                                                        ?>
                                                                    </td>										
                                                                    <td>
                                                                        <?php
                                                                        $status = $res->status;
                                                                        if ($status == 'delivered') {
                                                                            echo "<span style='color:#2196F3;'>" . $status . "</span>";
                                                                        }
                                                                        if ($status == 'cancel') {
                                                                            echo "<span style='color:#f44336;'>" . $status . "</span>";
                                                                        }
                                                                        if ($status == 'pending') {
                                                                            echo "<span style='color:#ff9800;'>" . $status . "</span>";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td>

                                                                        <a href = "#" class = "btn btn-sm btn-outline blue" data-toggle="modal" data-target="#myModal<?php echo $user_id = $res->user_id; ?>"><i class = "fa fa-eye"></i> View Details</a>
                                                                    </td>

                                                                </tr>
                                                                <!-- Modal -->
                                                            <div id="myModal<?php echo $user_id = $res->user_id; ?>" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content" style="top: 58px;">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title"><?php
                                                                                $user_id = $res->user_id;
                                                                                $get_user_data = $userData->get_user_seller($user_id);
                                                                                foreach ($get_user_data as $user) {
                                                                                    echo $managerName = $user->managerName;
                                                                                }
                                                                                ?>
                                                                                <h5>Username:<?php echo $user_name = $user->user_id; ?></h5>
                                                                                <h5 style="float:right;">Type:<?php echo $user_type = $user->user_type; ?></h5>
                                                                            </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $user_id = $res->user_id;
                                                                            $get_user_data = $userData->get_user_seller($user_id);
                                                                            foreach ($get_user_data as $user) {
                                                                                
                                                                            }
                                                                            ?>
                                                                            <div class="col-md-6" style="margin-top: 16px;">Bank Name : <?php echo $user->bank_name; ?></div>
                                                                            <div class="col-md-6" style="margin-top: 16px;">Account No. :<?php echo $user->account_no; ?></div>
                                                                            <div class="col-md-6">IFSC Code: <?php echo $user->ifsc_code; ?></div>
                                                                            <div class="col-md-6">Holder Name: <?php echo $user->bank_account_holder; ?> </div>

                                                                        </div>
                                                                        <div class="modal-footer">

                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <?php
                                                            $ctn++;
                                                        }
                                                        ?>

                                                        </tbody>         
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </body>
                                <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                                <!-- END PAGE LEVEL PLUGINS -->
<!--                                <script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.min.js'></script>-->


                                <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                                <?php
                              //  admin_footer();
                                ?>
                                </html>
                            </div>