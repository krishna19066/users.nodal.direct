<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
 if($customerId){ 
$propertyCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
    $cloud_cdnKey = $cloud_keyData->api_key;
    $cloud_cdnSecret = $cloud_keyData->api_secret;
    require 'Cloudinary.php';
    require 'Uploader.php';
    require 'Api.php';
    Cloudinary::config(array(
        "cloud_name" => $cloud_cdnName,
        "api_key" => $cloud_cdnKey,
        "api_secret" => $cloud_cdnSecret
    ));
$exp_Id = $_REQUEST['expID'];
$exp_data = $propertyCont->getExperienceDataWithID($customerId,$exp_Id);
//print_r($exp_data);
$exp_res = $exp_data[0];
$name = $exp_res->name;
$action = $_REQUEST['action'];
if($action == 'delete'){
    $imagesID = $_REQUEST['imagesID'];
    $exp_id = $_REQUEST['expID'];
    $delete = $propertyCont->deleteExpPhoto($imagesID,$customerId);
    echo '<script type="text/javascript">
                alert("Succesfuly delete image");              
window.location = "experience-view-photo.php?expID=' . $exp_id . '";
            </script>';
}
$type = $_REQUEST['type'];
if($type =='feature'){
   $feature = $_REQUEST['feature'];
   $imagesID = $_REQUEST['imagesID'];
   $exp_id = $_REQUEST['expID'];
    $upd = $propertyCont->SetFeatureExpPhoto($feature,$imagesID,$customerId);
    echo '<script type="text/javascript">
                alert("Succesfuly set feature");              
window.location = "experience-view-photo.php?expID=' . $exp_id . '";
            </script>';
}

if (isset($_POST['submit_pic'])) {
    $exp_id = $_POST['expID'];
    $pro_type = "property";
    $file = $_FILES['image_org']['name'];
    $alt_tag = $_POST['imageAlt1'];
    $timdat = date('Y-m-d');
    $timtim = date('H-i-s');
    $timestmp = $timdat . "_" . $timtim;  
        \Cloudinary\Uploader::upload($_FILES["image_org"]["tmp_name"], array("public_id" => $timestmp, "folder" => "reputize/experience"));
    $propertyImg = $propertyCont->AddExperiencePhoto($customerId,$exp_Id,$timestmp, $alt_tag);
    echo '<script type="text/javascript">
                alert("Succesfuly Upload Image");              
window.location = "experience-view-photo.php?expID=' . $exp_id . '";
            </script>';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
		 <script type='text/javascript'>
                function ValidateSize(file) {
                    var FileSize = file.files[0].size / 1024 / 1024; // in MB
                    if (FileSize > 2) {
                        alert("Image size exceeds 2 MB! We can't accept! please choose less than 2 MB");
                        // var file = $(file).val(''); //for clearing with Jquery
                        var file = document.getElementById('file').value = '';
                    } else {

                    }
                }
            </script>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                       <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Experience - <span style="color:#e44787;"><?php echo $name; ?></span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Add Experience photo </a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View/Add Photo</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body ">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_photo').slideToggle();" value="Add Photo" /></span>
                                        <span style="float:right;"><a href="manage-experience.php"><input type="button" style="background:#a836d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true" value="List Of Experience" /></a></span>
                                        
                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!---------------------------------------------------------------- Add Photo Section Starts --------------------------------------------------------------------->
                    <div class="portlet light bordered" id="add_photo" style="display:none;">			
                        <div class="portlet-body form">
                            <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="">
                                <div class="form-body">
                                    <h4 align="center" style="color:#E26A6A;"><b> Add Photo </b></h3><br><br>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Image
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="image_org" id="file" onchange="ValidateSize(this)"> </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    <span style="font-size:12px; color: red;">Note:image size should be less than 2MB </span>
                                                </div>
                                            </div>
                                        </div>
                                     
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> Image Alt Tag
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input name="imageAlt1" type="text" class="form-control"/>
                                                <span class="help-block"> Provide your image alt tag</span>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <input name="expID" type="hidden" value="<?php echo $exp_Id ?>" />
                                                    <input type="submit" name="submit_pic" class="btn green" id="submit" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?> value="Add Photo">
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!---------------------------------------------------------------- Add Photo Section Ends --------------------------------------------------------------------->
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row">
                                            <?php
                                            $PhotoListdata = $propertyCont->getExpreriencePhoto($exp_Id);
                                           // print_r($PhotoListdata);
                                            if ($PhotoListdata != NULL) {
                                                foreach ($PhotoListdata as $PhotoList) {
                                                    ?>
                                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                                        <div class=" mt-overlay-1">
                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/w_564,h_400,c_fill/reputize/experience/<?php echo $PhotoList->imageURL; ?>.jpg" />
                                                            <div class="mt-overlay">
                                                                <ul class="mt-info">
                                                                    <li>
                                                                        <?php if($PhotoList->feature == 'Y'){ ?>
                                                                        <a class="btn default btn-outline" href="experience-view-photo.php?expID=<?php echo $exp_Id;  ?>&type=feature&feature=N&imagesID=<?php echo $PhotoList->id; ?>" <?php if($mode == 'V'){ ?> style=" pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>
                                                                            <i class="icon-magnifier"></i> Set Feature
                                                                        </a>
                                                                        <?php }else{ ?>
                                                                         <a style='color: red;' class="btn default btn-outline" href="experience-view-photo.php?expID=<?php echo $exp_Id;  ?>&type=feature&feature=Y&imagesID=<?php echo $PhotoList->id; ?>" <?php if($mode == 'V'){ ?> style=" pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>
                                                                            <i class="icon-magnifier"></i> Unset Feature
                                                                        </a>
                                                                        <?php } ?>
                                                                    </li>
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="experience-view-photo.php?expID=<?php echo $exp_Id;  ?>&action=delete&imagesID=<?php echo $PhotoList->id; ?>" <?php if($mode == 'V'){ ?> style=" pointer-events: none; cursor: not-allowed;opacity: 0.6;" <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>
                                                                            <i class="icon-trash"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                              
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <center> <h3>No Photo Found.....</h3></center>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <?php } ?>
<?php echo ajaxJsFooter(); ?>
</body>
</html>