<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$propertyCont = new propertyData();
$res = $cityListings1[0];
//$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $propertyCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$propertyId = base64_decode($_REQUEST['propID']);
//$propertyId = $propID;
//$connectCloudinary = $propertyCont->connectCloudinaryAccount($customerId);
$property_data = $propertyCont->GetPropertyDataWithId($propertyId);
$property_res = $property_data[0];
$propertyName = $property_res->propertyName;
?>
<!DOCTYPE html>
<html lang="en">
    <head>  
        <?php echo ajaxCssHeader(); ?>      
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <div class="clearfix"> </div>      
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>          
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Nodal - <span style="color:#e44787;">Videos</span></h1>
                            </div>
                        </div>                   
                    </div>                    
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="manage-property.php">My Properties</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>View Nodal Video</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <div style="width:100%;" class="clear"> 
                                        <span style="float:left;"><h2>Help/Support</h2></span>                 
                                        <span style="float:right;"> <a href="javascript:window.open('img-sequence.php?propertyId=<?php echo $propertyId; ?>', 'title', 'location=yes');"><button style="background:red;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;margin-left: 16px;"> Back </button></a></span>
                                        <!--<span style="float:right;"> <a onclick="load_videoGallery('<?php echo $propertyId; ?>');"><button style="background:graytext;color:white;font-weight:bold;border:none;height:35px;width:160px;font-size:14px;"> Video Gallery </button></a></span>-->

                                        <br><br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col-md-12">	
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-body">
                                    <div class=" mt-element-overlay">
                                        <div class="row"> 
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <h2>Property Section</h2>
                                                <p class="accordion">Q1.Nodal CMS - How to add property images?</p>
                                                <div class="panel">
                                                    <p>You can add property image go to <Strong>My Property->Property->Select any city those you want to upload image->Click on property listing->View Property Photo->then click on Add Property Photo button</Strong>.

                                                    <div class=" mt-overlay-1">
                                                        <iframe width="80%" height="324px" src="https://www.youtube.com/embed/okC8BuRp2L8" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                    </div>  
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <h2>Room Section</h2>
                                                <p class="accordion">Q1.Nodal CMS - How to add images in property rooms page?</p>
                                                <div class="panel">
                                                    <p>You can add property image go to <Strong>My Property->Property->Select any city those you want to upload image->Click on property listing->View Property Photo->then click on Add Property Photo button</Strong>.
                                                   
                                                        <div class=" mt-overlay-1">
                                                            <iframe width="80%" height="324px" src="https://www.youtube.com/embed/tMesJ95F96o" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                        </div>                                                 
                                                    </p>
                                                </div>
                                                <p class="accordion">Q2.Nodal CMS - How To add/edit Room Type?</p>
                                                <div class="panel">
                                                    <p>You can add Room Type go to <Strong>My Property->setting->Select tab View/Add Room type->Click on Add Room type button</Strong>.
                                                   
                                                        <div class=" mt-overlay-1">
                                                            <iframe width="80%" height="324px" src="https://www.youtube.com/embed/Cryh5ypCx6U" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                        </div>                                                 
                                                    </p>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <h2>Home/Static Page Section</h2>
                                                <p class="accordion">Q1.Nodal CMS - How to Add header/footer menu?</p>
                                                <div class="panel">
                                                    <p>You can add header Or Footer menu go to <Strong>Admin Settings->Menu Navigation->Add Primary menu Button</Strong>.

                                                    <div class=" mt-overlay-1">
                                                        <iframe width="80%" height="324px" src="https://www.youtube.com/embed/Z3c2vej2m5Q" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                    </div>  
                                                  
                                                </div>
                                                 <p class="accordion">Q2.Nodal CMS - How to Update Home Page Slider Images?</p>
                                                <div class="panel">
                                                    <p>You can add home page slider go to <Strong>Home & static page->Home Page Slider->Click on Add Slider Image</Strong>.

                                                    <div class=" mt-overlay-1">
                                                        <iframe width="80%" height="324px" src="https://www.youtube.com/embed/6Vwhl2yp79M" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                    </div>                                                   
                                                </div>
                                                 <p class="accordion">Q3.Nodal CMS - How to edit Theme Settings?</p>
                                                <div class="panel">
                                                    <p>You can change theme color go to <Strong>Admin Settings->Theme Color</Strong>.
                                                    <div class=" mt-overlay-1">
                                                        <iframe width="80%" height="324px" src="https://www.youtube.com/embed/YGQ6_YJhIFw" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <h2>Other Section</h2>
                                                <p class="accordion">Q1.Nodal.direct - Hotel Sales Platform?</p>
                                                <div class="panel">
                                                    <p>You can add property image go to <Strong>My Property->Property->Select any city those you want to upload image->Click on property listing->View Property Photo->then click on Add Property Photo button</Strong>.
                                                   
                                                        <div class=" mt-overlay-1">
                                                            <iframe width="80%" height="324px" src="https://www.youtube.com/embed/PbNL3Yioi8k" frameborder="0" allowfullscreen></iframe>                                                                                                       
                                                        </div>                                                 
                                                    </p>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            /* Style the element that is used to open and close the accordion class */
            p.accordion {
                background-color: #efefef;;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                text-align: left;              
                border: none;
                outline: none;
                transition: 0.4s;
                margin-bottom:10px;

            }
            /* Add a background color to the accordion if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
            p.accordion.active, p.accordion:hover {
                background-color: #ddd;
            }
            /* Unicode character for "plus" sign (+) */
            p.accordion:after {
                content: '+';
                font-size: 18px;
                color: #777;
                float: right;
                margin-left: 5px;
            }
            /* Unicode character for "minus" sign (-) */
            p.accordion.active:after {
                content: "-";
            }
            /* Style the element that is used for the panel class */
            div.panel {
                padding: 0 18px;
                background-color: white;
                max-height: 0;
                overflow: hidden;
                transition: 0.4s ease-in-out;
                opacity: 0;
                margin-bottom:10px;
            }
            div.panel.show {
                opacity: 1;
                max-height: 500px; /* Whatever you like, as long as its more than the height of the content (on all screen sizes) */
            }
        </style>

        <script>
            document.addEventListener("DOMContentLoaded", function (event) {
                var acc = document.getElementsByClassName("accordion");
                var panel = document.getElementsByClassName('panel');
                for (var i = 0; i < acc.length; i++) {
                    acc[i].onclick = function () {
                        var setClasses = !this.classList.contains('active');
                        setClass(acc, 'active', 'remove');
                        setClass(panel, 'show', 'remove');
                        if (setClasses) {
                            this.classList.toggle("active");
                            this.nextElementSibling.classList.toggle("show");
                        }
                    }
                }
                function setClass(els, className, fnName) {
                    for (var i = 0; i < els.length; i++) {
                        els[i].classList[fnName](className);
                    }
                }
            });
        </script>
        <script type="text/javascript">

            /***********************************************
             * Cross browser Marquee II- � Dynamic Drive (www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit http://www.dynamicdrive.com/ for this script and 100s more.
             ***********************************************/
            var video = document.getElementById("myVideoBefore");
            var videoo = document.getElementById("myVideoAfter");
            var beforei = document.getElementById("beforeicon");
            var afteri = document.getElementById("aftericon");
            function playVideoo() {
                if (videoo.paused) {
                    videoo.play();
                    afteri.classList.add('fa-pause-circle-o');
                    afteri.classList.remove('fa-play-circle-o');
                    //btn.innerHTML = "Pause";
                } else {
                    videoo.pause();
                    afteri.classList.add('fa-play-circle-o');
                    afteri.classList.remove('fa-pause-circle-o');
                    //btn.innerHTML = "Play";
                }
            }

            function playVideo() {
                if (video.paused) {
                    video.play();
                    beforei.classList.add('fa-pause-circle-o');
                    beforei.classList.remove('fa-play-circle-o');
                    //btn.innerHTML = "Pause";
                } else {
                    video.pause();
                    beforei.classList.add('fa-play-circle-o');
                    beforei.classList.remove('fa-pause-circle-o');
                    //btn.innerHTML = "Play";
                }
            }
        </script>

        <?php echo ajaxJsFooter(); ?>
    </body>
</html>