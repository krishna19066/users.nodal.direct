<?php
define('SITE_URL_PATH', $_SERVER["DOCUMENT_ROOT"] . '/');
include "/home/hawthorntech/public_html/stayondiscount.com/include/class/cmsClasses.php";


$admincustId = $_SESSION['customerID'];
?>
<?php

function checkUserLogin() {
    if (!isset($_SESSION['MyAdminUserID']) && !isset($_SESSION['customerID']) && !isset($_SESSION['MyAdminUserType'])) {
        header("location:index.php");
        exit;
    }
}

function adminCss() {
    ?>	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <style>
        .clear {
            clear: both;
            content: "";
            display: table;
        }
    </style>
    <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <?php
}
?>

<?php

function themeheader() {
    $adminFuncCont = new adminFunction();
    ?>
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="index.html">
                    <img src="assets/layouts/layout4/img/nodal-logo.jpg" alt="logo" class="logo-default" style="height: 45px;margin-top: 16px;" /> </a>
                <div class="menu-toggler sidebar-toggler">
                    <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN PAGE ACTIONS -->
            <!-- DOC: Remove "hide" class to enable the page header actions -->
            <div class="page-actions">
                <div class="btn-group">
                    <button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="hidden-sm hidden-xs">Actions&nbsp;</span>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="javascript:;">
                                <i class="icon-docs"></i> New Post </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-tag"></i> New Comment </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-share"></i> Share </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-flag"></i> Comments
                                <span class="badge badge-success">4</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-users"></i> Feedbacks
                                <span class="badge badge-danger">2</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE ACTIONS -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
                <!-- BEGIN HEADER SEARCH BOX -->
                <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                <form class="search-form" action="page_general_search_2.html" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" placeholder="Search..." name="query">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <?php
                        //  Comment on Notifications Section for later changes
                        ?>


                        <li class="separator hide"> </li>

                        <!-- BEGIN TODO DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-calendar"></i>
                                <?php
                                $newBookNum1 = $adminFuncCont->newBookingNumber(date('Y-m-d'), $admincustId);
                                $booknum = count($newBookNum1);
                                if ($booknum == "0") {
                                    
                                } else {
                                    ?>
                                    <span class="badge badge-primary"><?php echo $booknum; ?></span>
                                    <?php
                                }
                                ?>
                            </a>

                            <ul class="dropdown-menu extended tasks">
                                <li class="external">
                                    <h3>
                                        <span class="bold"><?php echo $booknum; ?> new</span> bookings
                                    </h3>
                                    <a href="manage_booking.php">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                        <?php
                                        foreach ($newBookNum1 as $bookrow) {
                                            ?>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="photo">
                                                        <i class="fa fa-plus"></i>
                                                    </span>
                                                    <span class="subject">
                                                        <span class="from"> Name : </span>
                                                        <span class="time"><?php echo $bookrow->name; ?></span>
                                                    </span>
                                                    <span class="message"><?php echo $bookrow->property_enquired; ?></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END TODO DROPDOWN -->


                        <!-- BEGIN INBOX DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-envelope-open"></i>
                                <?php
                                $inquiryNum1 = $adminFuncCont->newInquiryNumber('0', $admincustId);
                                $cnn = count($newBookNum1);

                                if ($cnn == "0") {
                                    
                                } else {
                                    ?>
                                    <span class="badge badge-danger"> <?php echo $cnn; ?> </span>
                                    <?php
                                }
                                ?>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>You have
                                        <span class="bold"><?php echo $cnn; ?></span> Inquiries Unattended </h3>
                                    <a href="all-members.php">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                        <?php
                                        $inquiryData = $adminFuncCont->getInquiryData('', $admincustId, 'order by slno desc limit 5');
                                        foreach ($inquiryData as $inr1) {
                                            $nam = $inr1->name;
                                            $redat = $inr1->recvDate;
                                            $inty = $inr1->propertyName;
                                            ?>
                                            <li>
                                                <a href="#">
                                                    <span class="subject">
                                                        <span class="from"> <?php echo $nam; ?> </span>
                                                        <span class="time"><?php echo $redat; ?></span>
                                                    </span>
                                                    <?php
                                                    if ($inty == "") {
                                                        
                                                    } else {
                                                        ?>
                                                        <span class="message"><?php echo $inty; ?></span>
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END INBOX DROPDOWN -->
                        <li class="separator hide"> </li>



                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user dropdown-dark">
                            <?php
                            $id1 = $_SESSION['MyAdminUserID'];

                            $picrow = $adminFuncCont->getUserData($id1, $admincustId);
                            $picpath = $picrow->photo;

                            if ($picpath == '') {
                                ?>
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php echo $id1; ?>  </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="<?php echo $id1; ?>" class="img-circle" src="https://u.o0bc.com/avatars/no-user-image.gif" /> 
                                </a>
                                <?php
                            } else {
                                ?>
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php echo $id1; ?>  </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="<?php echo $id1; ?>" class="img-circle" src="http://res.cloudinary.com/the-perch/image/upload/w_150,h_150,c_thumb/reputize/admin_user/<?php echo $picpath; ?>.jpg" /> 
                                </a>
                                <?php
                            }
                            ?>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="user_profile.php">
                                        <i class="icon-user"></i> My Profile 
                                    </a>
                                </li>

                                <li>
                                    <a href="user_profile.php#tab_1_3">
                                        <i class="icon-calendar"></i> Change Password
                                    </a>
                                </li>

                                <?php
                                if (isset($_SESSION['MyAdminUserType'])) {
                                    $forMenushowType = $_SESSION['MyAdminUserType'];
                                    ?>
                                    <li>
                                        <a href="all-invoices.php">
                                            <i class="icon-envelope-open"></i> My Invoices
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>

                                <!--<li>
                                        <a href="app_todo_2.html">
                                                <i class="icon-rocket"></i> My Tasks
                                                <span class="badge badge-success"> 7 </span>
                                        </a>
                                </li>-->

                                <li class="divider"> </li>

                                <li>
                                    <a href="user_lock.php?<?php echo $id1; ?>">
                                        <i class="icon-lock"></i> Lock Screen </a>
                                </li>

                                <li>
                                    <a href="index.php?id1=Logout">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <?php
}
?>

<!----------------------------------------------------------------------------- Menu Functions Starts -------------------------------------------------------------->

<?php

function menuAdmintype($na) {
    ?>
    <?php
    if ($na == "welcome.php") {
        ?>
        <li class="nav-item start active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Business Dashboard</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item start active open">
                    <a href="welcome.php" class="nav-link ">
                        <span class="title">Business Dashboard </span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item start ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Business Dashboard</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item start ">
                    <a href="welcome.php" class="nav-link ">
                        <span class="title">Business Dashboard</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>

    <?php
    if ($na == "manage-property.php" || $na == "add-new-property.php" || $na == "add-room.php" || $na == "view-property-rooms.php" || $na == "add-room.php" || $na == "view-room-photo.php" || $na == "add-room-rate.php" || $na == "view-property-photo.php" || $na == "edit-alt-tag.php" || $na == "manage-question.php" || $na == "google_my_business.php" || $na == "manage-city.php" || $na == "add-city-data.php" || $na == "manage-sub-city.php" || $na == "add-sub-city.php" || $na == "manage-property-type.php" || $na == "add-property-type.php" || $na == "manage-room-type.php" || $na == "add-property-room.php" || $na == "ajax_add-property.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-building"></i>
                <span class="title">My Properties</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "manage-property.php" || $na == "add-new-property.php" || $na == "add-room.php" || $na == "view-property-rooms.php" || $na == "add-room.php" || $na == "view-room-photo.php" || $na == "add-room-rate.php" || $na == "view-property-photo.php" || $na == "edit-alt-tag.php" || $na == "manage-question.php" || $na == "google_my_business.php" || $na == "ajax_add-property.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-property.php" class="nav-link ">
                            <span class="title">Property</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-property.php" class="nav-link ">
                            <span class="title">Property</span>
                        </a>
                    </li>
                  
                    <?php
                }
                if ($na == "manage-city.php" || $na == "add-city-data.php" || $na == "manage-sub-city.php" || $na == "add-sub-city.php" || $na == "manage-property-type.php" || $na == "add-property-type.php" || $na == "manage-room-type.php" || $na == "add-property-room.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-property-settings.php" class="nav-link ">
                            <span class="title">Settings</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-property-settings.php" class="nav-link ">
                            <span class="title">Settings</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-building"></i>
                <span class="title">My Properties</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-property.php" class="nav-link ">
                        <span class="title">Property</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="manage-property-settings.php" class="nav-link ">
                        <span class="title">Settings</span>
                    </a>
                </li>
              
            </ul>
        </li>
        <?php
    }
    ?>


    <?php
    if ($na == "email-signature.php" || $na == "all-members.php" || $na == "manage-inquiry.php" || $na == "email-campaign.php" || $na == "sms-campaign.php" || $na == "nodal-templates.php" || $na == "sms-configuration.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">Manage Sales & Lead</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "all-members.php" || $na == "email-campaign.php" || $na == "sms-campaign.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-enquiry.php" class="nav-link ">
                            <span class="title">Inquiries & Campaigns</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-enquiry.php" class="nav-link ">
                            <span class="title">Inquiries & Campaigns</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "email-signature.php" || $na == "nodal-templates.php" || $na == "sms-configuration.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Settings</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Settings</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>			
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">Manage Sales & Lead</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-enquiry.php" class="nav-link ">
                        <span class="title">Inquiries & Campaigns</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="nodal-templates.php" class="nav-link ">
                        <span class="title">Settings</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>


    <?php
    if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php" || $na == "create_static_page.php" || $na == "edit_aboutUs.php" || $na == "edit_ourclient.php" || $na == "edit_policy.php" || $na == "edit_contactus.php" || $na == "edit_food.php" || $na == "edit_whyperch.php" || $na == "edit_client.php" || $na == "edit-testimonials.php" || $na == "edit-awards.php" || $na == "manage-home-page-slider.php" || $na == "manage-slider.php" || $na == "add-language.php" || $na == "manage-language.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-file-text"></i>
                <span class="title">Home & Static Page</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "manage-static-pages.php" || $na == "edit_aboutUs.php" || $na == "edit_ourclient.php" || $na == "edit_policy.php" || $na == "edit_contactus.php" || $na == "edit_food.php" || $na == "edit_whyperch.php" || $na == "edit_client.php" || $na == "edit-testimonials.php" || $na == "edit-awards.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-static-pages.php" class="nav-link ">
                            <span class="title">Static Pages</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-static-pages.php" class="nav-link ">
                            <span class="title">Static Pages</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php" || $na == "manage-home-page-slider.php" || $na == "manage-slider.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="../ajax_lib/ajax_home-page.php" class="nav-link ">
                            <span class="title">Home Page</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="../ajax_lib/ajax_home-page.php" class="nav-link ">
                            <span class="title">Home Page</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "add-language.php" || $na == "manage-language.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-home-page.php" class="nav-link ">
                            <span class="title">Multi-Language</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Multi-Language</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-file-text"></i>
                <span class="title">Home & Static Page</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-static-pages.php" class="nav-link ">
                        <span class="title">Static Pages</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="manage-home-page.php" class="nav-link ">
                        <span class="title">Home Page</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="manage-language.php" class="nav-link ">
                        <span class="title">Multi-Language</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>


    <?php
    if ($na == "manage_inventory.php" || $na == "manage_rates.php" || $na == "add-new-discount.php" || $na == "manage-discounts.php" || $na == "channel-manager.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-money"></i>
                <span class="title">Revenue Management</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "manage_inventory.php" || $na == "manage_rates.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage_inventory.php" class="nav-link ">
                            <span class="title">Inventory & Rates</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage_inventory.php" class="nav-link ">
                            <span class="title">Inventory & Rates</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "add-new-discount.php" || $na == "manage-discounts.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-discounts.php" class="nav-link ">
                            <span class="title">Discount & Offers</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-discounts.php" class="nav-link ">
                            <span class="title">Discount & Offers</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "channel-manager.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Channel Manager</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Channel Manager</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-money"></i>
                <span class="title">Revenue Management</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage_inventory.php" class="nav-link ">
                        <span class="title">Inventory & Rates</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="manage-discounts.php" class="nav-link ">
                        <span class="title">Discount & Offers</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="channel-manager.php" class="nav-link ">
                        <span class="title">Channel Manager</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>



    <?php
    if ($na == "analytics.php" || $na == "manage-analytic-code.php" || $na == "managae-meta-tags.php" || $na == "add-meta-tags.php" || $na == "google_my_business.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-server"></i>
                <span class="title">Analytics & SEO</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "analytics.php" || $na == "google_my_business.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Google Analytics & GMB</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Google Analytics & GMB</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "manage-analytic-settings.php" || $na == "managae-meta-tags.php" || $na == "add-meta-tags.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-analytic-settings.php" class="nav-link ">
                            <span class="title">Settings</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-analytic-settings.php" class="nav-link ">
                            <span class="title">Settings</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-server"></i>
                <span class="title">Analytics & SEO</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-analytic-code.php" class="nav-link ">
                        <span class="title">Google Analytics & GMB</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="manage-analytic-settings.php" class="nav-link ">
                        <span class="title">Settings</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>



    <?php
    if ($na == "add-author.php" || $na == "manage-author.php" || $na == "manage-categories.php" || $na == "add-category.php" || $na == "manage-posts.php" || $na == "add-post.php" || $na == "email-subscription.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-rss"></i>
                <span class="title">Blog Management</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "add-author.php" || $na == "manage-author.php" || $na == "manage-categories.php" || $na == "add-category.php" || $na == "manage-posts.php" || $na == "add-post.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Blogs</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Blogs</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "email-subscription.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Newsletter</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Newsletter</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-rss"></i>
                <span class="title">Blog Management</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-analytic-code.php" class="nav-link ">
                        <span class="title">Blogs</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Newsletter</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>



    <li class="heading">
        <h3 class="uppercase">Admin</h3>
    </li>

    <?php
    if ($na == "add-user.php" || $na == "manage-users.php" || $na == "manage_heder.php" || $na == "manage_footer.php" || $na == "request-template.php" || $na == "manage-sitemap.php" || $na == "misc_setting.php" || $na == "add_mail_template.php" || $na == "manage_mail_template.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user"></i>
                <span class="title">Admin Settings</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "manage-users.php" || $na == "add-user.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-users.php" class="nav-link ">
                            <span class="title">Users</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage-users.php" class="nav-link ">
                            <span class="title">Users</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "manage_heder.php" || $na == "manage_footer.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage_heder.php" class="nav-link ">
                            <span class="title">Menu Navigation</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="manage_heder.php" class="nav-link ">
                            <span class="title">Menu Navigation</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "request-template.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Template Request</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Template Request</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "manage-sitemap.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Sitemap Management</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Sitemap Management</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "add_mail_template.php" || $na == "manage_mail_template.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="add_mail_template.php" class="nav-link ">
                            <span class="title">Mail Templates</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="add_mail_template.php" class="nav-link ">
                            <span class="title">Mail Templates</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "misc_setting.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Misc. Settings</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Misc. Settings</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user"></i>
                <span class="title">Admin Settings</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-analytic-code.php" class="nav-link ">
                        <span class="title">Users</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="manage_heder.php" class="nav-link ">
                        <span class="title">Menu Navigation</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Template Request</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Sitemap Management</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="add_mail_template.php" class="nav-link ">
                        <span class="title">Mail Templates</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Misc. Settings</span>
                    </a>
                </li>
                 <li class="nav-item  ">
                    <a href="manage-gen-setting.php" class="nav-link ">
                        <span class="title">Gen Settings</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>


    <?php
    if ($na == "help_faq.php" || $na == "raise_ticket.php" || $na == "help_videos.php" || $na == "our_journey.php") {
        ?>
        <li class="nav-item active open">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-question-circle"></i>
                <span class="title">Help & FAQ</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <?php
                if ($na == "help_faq.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">FAQ</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">FAQ</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "raise_ticket.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Raise Ticket</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Raise Ticket</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "help_videos.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Videos</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Videos</span>
                        </a>
                    </li>
                    <?php
                }
                if ($na == "our_journey.php") {
                    ?>
                    <li class="nav-item start active open">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Nodal Journey</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Nodal Journey</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    } else {
        ?>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-question-circle"></i>
                <span class="title">Help & FAQ</span>
                <span class="arrow"></span>
            </a>

            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="manage-analytic-code.php" class="nav-link ">
                        <span class="title">FAQ</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Raise Ticket</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Videos</span>
                    </a>
                </li>

                <li class="nav-item  ">
                    <a href="managae-meta-tags.php" class="nav-link ">
                        <span class="title">Nodal Journey</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php
    }
    ?>
    <?php
}

function menuUsertype($t, $na) {
    ?>
    <?php
    if ($t == "Properties") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Properties</h3>
        </li>
        <?php
        if ($na == "manage-property.php" || $na == "add-new-property.php" || $na == "add-room.php" || $na == "view-property-rooms.php" || $na == "add-room.php" || $na == "view-room-photo.php" || $na == "add-room-rate.php" || $na == "view-property-photo.php" || $na == "edit-alt-tag.php" || $na == "manage-question.php" || $na == "google_my_business.php") {
            ?>
            <li class="nav-item active open">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-user-plus"></i>
                    <span class="title">Property Details</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <?php
                    if ($na == "manage-property.php" || $na == "add-new-property.php" || $na == "add-room.php" || $na == "view-property-rooms.php" || $na == "add-room.php" || $na == "view-room-photo.php" || $na == "add-room-rate.php" || $na == "view-property-photo.php" || $na == "edit-alt-tag.php" || $na == "manage-question.php") {
                        ?>
                        <li class="nav-item start active open">
                            <a href="manage-property.php" class="nav-link ">
                                <span class="title">Property</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="nav-item  ">
                            <a href="manage-property.php" class="nav-link ">
                                <span class="title">Property</span>
                            </a>
                        </li>
                        <?php
                    }
                    if ($na == "google_my_business.php") {
                        ?>
                        <li class="nav-item start active open">
                            <a href="google_my_business.php" class="nav-link ">
                                <span class="title">Google My Business</span>
                            </a>
                        </li>
                        <?php
                    } else {
                        ?>	
                        <li class="nav-item  ">
                            <a href="google_my_business.php" class="nav-link ">
                                <span class="title">Google My Business</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-user-plus"></i>
                    <span class="title">Property Details</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="manage-property.php" class="nav-link ">
                            <span class="title">Property</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="google_my_business.php" class="nav-link ">
                            <span class="title">Google My Business</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        if ($na == "manage-city.php" || $na == "add-city-data.php" || $na == "manage-sub-city.php" || $na == "add-sub-city.php" || $na == "manage-property-type.php" || $na == "add-property-type.php" || $na == "manage-room-type.php" || $na == "add-property-room.php") {
            ?>
            <li class="nav-item active open">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Settings</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <?php
                    if ($na == "manage-city.php" || $na == "add-city-data.php") {
                        ?>
                        <li class="nav-item start active open">
                            <a href="manage-city.php" class="nav-link ">
                                <span class="title">City</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="nav-item  ">
                            <a href="manage-city.php" class="nav-link ">
                                <span class="title">City</span>
                            </a>
                        </li>
                        <?php
                    }
                    if ($na == "manage-sub-city.php" || $na == "add-sub-city.php") {
                        ?>
                        <li class="nav-item start active open">
                            <a href="manage-sub-city.php" class="nav-link ">
                                <span class="title">Sub City</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="nav-item  ">
                            <a href="manage-sub-city.php" class="nav-link ">
                                <span class="title">Sub City</span>
                            </a>
                        </li>
                        <?php
                    }
                    if ($na == "manage-property-type.php" || $na == "add-property-type.php") {
                        ?>
                        <li class="nav-item start active open">
                            <a href="manage-property-type.php" class="nav-link ">
                                <span class="title">Property Type</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="nav-item  ">
                            <a href="manage-property-type.php" class="nav-link ">
                                <span class="title">Property Type</span>
                            </a>
                        </li>
                        <?php
                    }
                    if ($na == "manage-room-type.php" || $na == "add-property-room.php") {
                        ?>
                        <li class="nav-item start active open">
                            <a href="manage-room-type.php" class="nav-link ">
                                <span class="title">Room Type</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="nav-item  ">
                            <a href="manage-room-type.php" class="nav-link ">
                                <span class="title">Room Type</span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="manage-city.php" class="nav-link ">
                            <span class="title">City</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="manage-sub-city.php" class="nav-link ">
                            <span class="title">Sub City</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="manage-property-type.php" class="nav-link ">
                            <span class="title">Property Type</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="manage-room-type.php" class="nav-link ">
                            <span class="title">Room Type</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Sales & Lead Management") {
        ?>		  
        <li class="heading">
            <h3 class="uppercase">Sales & Lead Management</h3>
        </li>

        <?php
        if ($na == "email-signature.php" || $na == "all-members.php" || $na == "manage-inquiry.php" || $na == "email-campaign.php" || $na == "sms-campaign.php" || $na == "add_new_booking.php" || $na == "manage_booking.php" || $na == "nodal-templates.php" || $na == "sms-configuration.php") {
            ?>
            <?php
            if ($na == "all-members.php") {
                ?>
                <li class="nav-item active open">
                    <a href="all-members.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Online Inquiries</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="all-members.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Online Inquiries</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "email-campaign.php") {
                ?>
                <li class="nav-item active open">
                    <a href="email-campaign.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Email Campaign</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="email-campaign.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Email Campaign</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "sms-campaign.php") {
                ?>
                <li class="nav-item active open">
                    <a href="sms-campaign.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">SMS Campaign</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="sms-campaign.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">SMS Campaign</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "add_new_booking.php" || $na == "manage_booking.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage_booking.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Bookings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage_booking.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Bookings</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "email-signature.php" || $na == "nodal-templates.php" || $na == "sms-configuration.php") {
                ?>
                <li class="nav-item active open">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php
                        if ($na == "email-signature.php") {
                            ?>
                            <li class="nav-item start active open">
                                <a href="email-signature.php" class="nav-link ">
                                    <span class="title">Email Signature</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item  ">
                                <a href="email-signature.php" class="nav-link ">
                                    <span class="title">Email Signature</span>
                                </a>
                            </li>
                            <?php
                        }
                        if ($na == "nodal-templates.php") {
                            ?>
                            <li class="nav-item start active open">
                                <a href="nodal-templates.php" class="nav-link ">
                                    <span class="title">Templates</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item  ">
                                <a href="nodal-templates.php" class="nav-link ">
                                    <span class="title">Templates</span>
                                </a>
                            </li>
                            <?php
                        }
                        if ($na == "sms-configuration.php") {
                            ?>
                            <li class="nav-item start active open">
                                <a href="sms-configuration.php" class="nav-link ">
                                    <span class="title">SMS configuration</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item  ">
                                <a href="sms-configuration.php" class="nav-link ">
                                    <span class="title">SMS configuration</span>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="email-signature.php" class="nav-link ">
                                <span class="title">Email Signature</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="nodal-templates.php" class="nav-link ">
                                <span class="title">Templates</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="sms-configuration.php" class="nav-link ">
                                <span class="title">SMS configuration</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="all-members.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Online Inquiries</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="email-campaign.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Email Campaign</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="sms-campaign.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">SMS Campaign</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage_booking.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Bookings</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="email-signature.php" class="nav-link ">
                            <span class="title">Email Signature</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="nodal-templates.php" class="nav-link ">
                            <span class="title">Templates</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="sms-configuration.php" class="nav-link ">
                            <span class="title">SMS configuration</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Static Pages") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Static Pages</h3>
        </li>

        <?php
        if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php" || $na == "manage-static-pages.php" || $na == "edit_aboutUs.php" || $na == "edit_ourclient.php" || $na == "edit_policy.php" || $na == "edit_contactus.php" || $na == "edit_food.php" || $na == "edit_whyperch.php" || $na == "edit_client.php" || $na == "edit-testimonials.php" || $na == "edit-awards.php" || $na == "manage-home-page-slider.php" || $na == "manage-slider.php" || $na == "add-language.php" || $na == "manage-language.php") {
            ?>
            <?php
            if ($na == "manage-static-pages.php" || $na == "edit_aboutUs.php" || $na == "edit_ourclient.php" || $na == "edit_policy.php" || $na == "edit_contactus.php" || $na == "edit_food.php" || $na == "edit_whyperch.php" || $na == "edit_client.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage-static-pages.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Manage Static Pages</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage-static-pages.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Manage Static Pages</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage-home-page.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Home Page</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage-home-page.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Home Page</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "manage-home-page-slider.php" || $na == "manage-slider.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Home Page Slider</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Home Page Slider</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "add-language.php" || $na == "manage-language.php") {
                ?>
                <li class="nav-item active open">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start active open">
                            <a href="manage-language.php" class="nav-link ">
                                <span class="title">Multi Language</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="manage-language.php" class="nav-link ">
                                <span class="title">Multi Language</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="manage-static-pages.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Manage Static Pages</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage-home-page.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Home Page</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Home Page Slider</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="manage-language.php" class="nav-link ">
                            <span class="title">Multi Language</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Revenue Management - Inventory and Rates") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Revenue Management - Inventory and Rates</h3>
        </li>

        <?php
        if ($na == "manage_inventory.php" || $na == "manage_rates.php" || $na == "add-new-discount.php" || $na == "manage-discounts.php" || $na == "channel-manager.php") {
            ?>
            <?php
            if ($na == "manage_inventory.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage_inventory.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Inventory</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage_inventory.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Inventory</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage_rates.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Rates</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage_rates.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Rates</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "add-new-discount.php" || $na == "manage-discounts.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage-discounts.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Discount & Offers</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage-discounts.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Discount & Offers</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "channel-manager.php") {
                ?>
                <li class="nav-item active open">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start active open">
                            <a href="channel-manager.php" class="nav-link ">
                                <span class="title">Channel Manager Integration</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="channel-manager.php" class="nav-link ">
                                <span class="title">Channel Manager Integration</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="manage_inventory.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Inventory</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage_rates.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Rates</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage-discounts.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Discount & Offers</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="channel-manager.php" class="nav-link ">
                            <span class="title">Channel Manager Integration</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Analytics & SEO") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Analytics & SEO</h3>
        </li>

        <?php
        if ($na == "analytics.php" || $na == "manage-analytic-code.php" || $na == "managae-meta-tags.php" || $na == "add-meta-tags.php") {
            ?>
            <?php
            if ($na == "analytics.php") {
                ?>
                <li class="nav-item active open">
                    <a href="analytics.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Google Analytics</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="analytics.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Google Analytics</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "manage-analytic-code.php" || $na == "managae-meta-tags.php" || $na == "add-meta-tags.php") {
                ?>
                <li class="nav-item active open">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php
                        if ($na == "manage-analytic-code.php") {
                            ?>
                            <li class="nav-item start active open">
                                <a href="manage-analytic-code.php" class="nav-link ">
                                    <span class="title">Manage Analytics Code</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item  ">
                                <a href="manage-analytic-code.php" class="nav-link ">
                                    <span class="title">Manage Analytics Code</span>
                                </a>
                            </li>
                            <?php
                        }
                        if ($na == "managae-meta-tags.php" || $na == "add-meta-tags.php") {
                            ?>
                            <li class="nav-item start active open">
                                <a href="managae-meta-tags.php" class="nav-link ">
                                    <span class="title">SEO Meta Tags</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item  ">
                                <a href="managae-meta-tags.php" class="nav-link ">
                                    <span class="title">SEO Meta Tags</span>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="manage-analytic-code.php" class="nav-link ">
                                <span class="title">Manage Analytics Code</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="managae-meta-tags.php" class="nav-link ">
                                <span class="title">SEO Meta Tags</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="analytics.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Google Analytics</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="manage-analytic-code.php" class="nav-link ">
                            <span class="title">Manage Analytics Code</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="managae-meta-tags.php" class="nav-link ">
                            <span class="title">SEO Meta Tags</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Blog Management") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Admin</h3>
        </li>

        <?php
        if ($na == "add-user.php" || $na == "manage-users.php" || $na == "manage_heder.php" || $na == "manage_footer.php" || $na == "request-template.php" || $na == "manage-sitemap.php" || $na == "misc_setting.php") {
            ?>
            <?php
            if ($na == "manage-users.php" || $na == "add-user.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage-users.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Manage Users</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage-users.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Manage Users</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "manage_heder.php" || $na == "manage_footer.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage_heder.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Menu Navigation</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage_heder.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Menu Navigation</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "request-template.php") {
                ?>
                <li class="nav-item active open">
                    <a href="request-template.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Template Request</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="request-template.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Template Request</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "manage-sitemap.php") {
                ?>
                <li class="nav-item active open">
                    <a href="manage-sitemap.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Sitemap Management</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="manage-sitemap.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Sitemap Management</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "misc_setting.php") {
                ?>
                <li class="nav-item active open">
                    <a href="misc_setting.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Misc. Settings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="misc_setting.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Misc. Settings</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="manage-users.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Manage Users</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage_heder.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Menu Navigation</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="request-template.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Template Request</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="manage-sitemap.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Sitemap Management</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="misc_setting.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Misc. Settings</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Email Templates") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Email Templates</h3>
        </li>

        <?php
        if ($na == "add_mail_template.php" || $na == "manage_mail_template.php") {
            ?>
            <?php
            if ($na == "manage-users.php" || $na == "add-user.php") {
                ?>
                <li class="nav-item active open">
                    <a href="add_mail_template.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Mail Templates</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="add_mail_template.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Mail Templates</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="add_mail_template.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Mail Templates</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <?php
        }
        ?>
        <?php
    }
    if ($t == "Help & FAQ") {
        ?>
        <li class="heading">
            <h3 class="uppercase">Help & FAQ</h3>
        </li>

        <?php
        if ($na == "help_faq.php" || $na == "raise_ticket.php" || $na == "help_videos.php" || $na == "our_journey.php") {
            ?>
            <?php
            if ($na == "help_faq.php") {
                ?>
                <li class="nav-item active open">
                    <a href="help_faq.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">FAQ</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="help_faq.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">FAQ</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "raise_ticket.php") {
                ?>
                <li class="nav-item active open">
                    <a href="raise_ticket.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Raise Ticket</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="raise_ticket.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Raise Ticket</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "help_videos.php") {
                ?>
                <li class="nav-item active open">
                    <a href="help_videos.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Videos</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="help_videos.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Videos</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "our_journey.php") {
                ?>
                <li class="nav-item active open">
                    <a href="our_journey.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Nodal Journey</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="our_journey.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Nodal Journey</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            if ($na == "misc_setting.php") {
                ?>
                <li class="nav-item active open">
                    <a href="misc_setting.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Misc. Settings</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item  ">
                    <a href="misc_setting.php" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Misc. Settings</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <li class="nav-item  ">
                <a href="help_faq.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">FAQ</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="raise_ticket.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Raise Ticket</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="help_videos.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Videos</span>
                    <span class="arrow"></span>
                </a>
            </li>

            <li class="nav-item  ">
                <a href="our_journey.php" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Nodal Journey</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <?php
        }
        ?>
        <?php
    }
    ?>
    <?php
}
?>

<!----------------------------------------------------------------------------- Menu Functions Ends -------------------------------------------------------------->


<?php

function admin_header($custNa) {
    $adminFuncCont = new adminFunction();
    $ur1 = $_SERVER['REQUEST_URI'];
    $ur = explode("/", $ur1);
    $na_nw = $ur[2];
    $na_br = explode("?", $na_nw);
    $na = $na_br[0];

    if ($custNa != '') {
        $na = $custNa;
    }
    ?>
    <?php
    if (!isset($_SESSION['MyAdminUserID'])) {
        
    }

    if (isset($_SESSION['MyAdminUserID'])) {
        ?>
        <div class="page-sidebar-wrapper">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->

                <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">			
                    <?php
                    include "../include/arrays.inc.php";
                    $id = $_SESSION['MyAdminUserID'];

                    $admincustId = $_SESSION['customerID'];
                    $lanarr = $adminFuncCont->getLangData($admincustId, '(status="Y"||status="N")');
                    $lanarray = explode("^", $lanarr);

                    $row = $adminFuncCont->getUserData($id, $admincustId);

                    //echo $value."<br>";
                    $lan = $_SERVER['QUERY_STRING'];
                    if ($lan == "") {
                        $type = $row->user_type;

                        if ($type == "admin") {
                            menuAdmintype($na);
                        } else {
                            $cont = $row->section;
                            $a = explode("^", $cont);
                            foreach ($a as $value) {
                                $t = $admin_section[$value];
                                menuUsertype($t, $na);
                            }
                        }
                    } elseif (in_array($lan, $lanarray)) {
                        $type = $row->user_type;
                        if ($type == "admin") {
                            ?>
                            <li class="heading">
                                <h3 class="uppercase">Static Pages</h3>
                            </li>

                            <?php
                            if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php" || $na == "edit-testimonials.php" || $na == "edit-awards.php" || $na == "manage-home-page-slider.php" || $na == "manage-slider.php") {
                                ?>
                                <?php
                                if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php") {
                                    ?>
                                    <li class="nav-item active open">
                                        <a href="manage-home-page.php" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Home Page</span>
                                            <span class="selected"></span>
                                            <span class="arrow open"></span>
                                        </a>
                                    </li>
                                    <?php
                                } else {
                                    ?>
                                    <li class="nav-item  ">
                                        <a href="manage-home-page.php" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Home Page</span>
                                            <span class="arrow"></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if ($na == "manage-home-page-slider.php" || $na == "manage-slider.php") {
                                    ?>
                                    <li class="nav-item active open">
                                        <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Home Page Slider</span>
                                            <span class="selected"></span>
                                            <span class="arrow open"></span>
                                        </a>
                                    </li>
                                    <?php
                                } else {
                                    ?>
                                    <li class="nav-item  ">
                                        <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Home Page Slider</span>
                                            <span class="arrow"></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                                <?php
                            } else {
                                ?>
                                <li class="nav-item  ">
                                    <a href="manage-home-page.php" class="nav-link nav-toggle">
                                        <i class="fa fa-users"></i>
                                        <span class="title">Home Page</span>
                                        <span class="arrow"></span>
                                    </a>
                                </li>

                                <li class="nav-item  ">
                                    <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                                        <i class="fa fa-users"></i>
                                        <span class="title">Home Page Slider</span>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>



                            <li class="heading">
                                <h3 class="uppercase">Admin</h3>
                            </li>

                            <?php
                            if ($na == "manage_heder.php" || $na == "manage_footer.php") {
                                ?>
                                <?php
                                if ($na == "manage_heder.php" || $na == "manage_footer.php") {
                                    ?>
                                    <li class="nav-item active open">
                                        <a href="manage_heder.php" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Menu Navigation</span>
                                            <span class="selected"></span>
                                            <span class="arrow open"></span>
                                        </a>
                                    </li>
                                    <?php
                                } else {
                                    ?>
                                    <li class="nav-item  ">
                                        <a href="manage_heder.php" class="nav-link nav-toggle">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Menu Navigation</span>
                                            <span class="arrow"></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                                <?php
                            } else {
                                ?>
                                <li class="nav-item  ">
                                    <a href="manage_heder.php" class="nav-link nav-toggle">
                                        <i class="fa fa-users"></i>
                                        <span class="title">Menu Navigation</span>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>																	
                            <?php
                        } else {
                            $cont = $row->section;
                            $a = explode("^", $cont);

                            foreach ($a as $value) {
                                $t = $admin_section[$value];

                                if ($t == "Static Pages") {
                                    ?>
                                    <li class="heading">
                                        <h3 class="uppercase">Static Pages</h3>
                                    </li>

                                    <?php
                                    if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php" || $na == "edit-testimonials.php" || $na == "edit-awards.php" || $na == "manage-home-page-slider.php" || $na == "manage-slider.php") {
                                        ?>
                                        <?php
                                        if ($na == "manage-home-page.php" || $na == "manage-home-page-mobile.php") {
                                            ?>
                                            <li class="nav-item active open">
                                                <a href="manage-home-page.php" class="nav-link nav-toggle">
                                                    <i class="fa fa-users"></i>
                                                    <span class="title">Home Page</span>
                                                    <span class="selected"></span>
                                                    <span class="arrow open"></span>
                                                </a>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li class="nav-item  ">
                                                <a href="manage-home-page.php" class="nav-link nav-toggle">
                                                    <i class="fa fa-users"></i>
                                                    <span class="title">Home Page</span>
                                                    <span class="arrow"></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        if ($na == "manage-home-page-slider.php" || $na == "manage-slider.php") {
                                            ?>
                                            <li class="nav-item active open">
                                                <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                                                    <i class="fa fa-users"></i>
                                                    <span class="title">Home Page Slider</span>
                                                    <span class="selected"></span>
                                                    <span class="arrow open"></span>
                                                </a>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li class="nav-item  ">
                                                <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                                                    <i class="fa fa-users"></i>
                                                    <span class="title">Home Page Slider</span>
                                                    <span class="arrow"></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    } else {
                                        ?>
                                        <li class="nav-item  ">
                                            <a href="manage-home-page.php" class="nav-link nav-toggle">
                                                <i class="fa fa-users"></i>
                                                <span class="title">Home Page</span>
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

                                        <li class="nav-item  ">
                                            <a href="manage-home-page-slider.php" class="nav-link nav-toggle">
                                                <i class="fa fa-users"></i>
                                                <span class="title">Home Page Slider</span>
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                if ($t == "Admin") {
                                    ?>
                                    <li class="heading">
                                        <h3 class="uppercase">Admin</h3>
                                    </li>

                                    <?php
                                    if ($na == "manage_heder.php" || $na == "manage_footer.php") {
                                        ?>
                                        <?php
                                        if ($na == "manage_heder.php" || $na == "manage_footer.php") {
                                            ?>
                                            <li class="nav-item active open">
                                                <a href="manage_heder.php" class="nav-link nav-toggle">
                                                    <i class="fa fa-users"></i>
                                                    <span class="title">Menu Navigation</span>
                                                    <span class="selected"></span>
                                                    <span class="arrow open"></span>
                                                </a>
                                            </li>
                                            <?php
                                        } else {
                                            ?>
                                            <li class="nav-item  ">
                                                <a href="manage_heder.php" class="nav-link nav-toggle">
                                                    <i class="fa fa-users"></i>
                                                    <span class="title">Menu Navigation</span>
                                                    <span class="arrow"></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    } else {
                                        ?>
                                        <li class="nav-item  ">
                                            <a href="manage_heder.php" class="nav-link nav-toggle">
                                                <i class="fa fa-users"></i>
                                                <span class="title">Menu Navigation</span>
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                        }
                    } else {
                        $type = $row->user_type;
                        if ($type == "admin") {
                            menuAdmintype($na);
                        } else {
                            $cont = $row->section;
                            $a = explode("^", $cont);

                            foreach ($a as $value) {
                                $t = $admin_section[$value];

                                menuUsertype($t, $na);
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>

        <?php
    }
}
?>


<!--------------------------------------------------------------==============================================--------------------------------------------------------------->


<?php

function admin_footer() {
    ?>
    <div id="footer">
        <div class="copyright"> 2014 © Metronic. Admin Dashboard Template. <?= WEBSITE_NAME; ?>&nbsp;All Rights Reserved. <br /></div> 
    </div>
    <?php
    $inquiryGrapCont = new inquiryGraph();
    $jan_data = $inquiryGrapCont->getInquiryGraphData('01', $admincustId);
    $feb_data = $inquiryGrapCont->getInquiryGraphData('02', $admincustId);
    $mar_data = $inquiryGrapCont->getInquiryGraphData('03', $admincustId);
    $apr_data = $inquiryGrapCont->getInquiryGraphData('04', $admincustId);
    $may_data = $inquiryGrapCont->getInquiryGraphData('05', $admincustId);
    $jun_data = $inquiryGrapCont->getInquiryGraphData('06', $admincustId);
    $jul_data = $inquiryGrapCont->getInquiryGraphData('07', $admincustId);
    $aug_data = $inquiryGrapCont->getInquiryGraphData('08', $admincustId);
    $sep_data = $inquiryGrapCont->getInquiryGraphData('09', $admincustId);
    $oct_data = $inquiryGrapCont->getInquiryGraphData('10', $admincustId);
    $nov_data = $inquiryGrapCont->getInquiryGraphData('11', $admincustId);
    $dec_data = $inquiryGrapCont->getInquiryGraphData('12', $admincustId);


    // $year_org = date('Y');
    // $jan = "select count(recvDate) from inquiry where year='$year_org' and month='01'";
    // $janquer = mysql_query($jan);
    // while($janrow = mysql_fetch_array($janquer))
    // {
    // $jan_data = $janrow['count(recvDate)'];
    // }
    // $feb = "select count(recvDate) from inquiry where year='$year_org' and month='02'";
    // $febquer = mysql_query($feb);
    // while($febrow = mysql_fetch_array($febquer))
    // {
    // $feb_data = $febrow['count(recvDate)'];
    // }
    // $mar = "select count(recvDate) from inquiry where year='$year_org' and month='03'";
    // $marquer = mysql_query($mar);
    // while($marrow = mysql_fetch_array($marquer))
    // {
    // $mar_data = $marrow['count(recvDate)'];
    // }
    // $apr = "select count(recvDate) from inquiry where year='$year_org' and month='04'";
    // $aprquer = mysql_query($apr);
    // while($aprrow = mysql_fetch_array($aprquer))
    // {
    // $apr_data = $aprrow['count(recvDate)'];
    // }
    // $may = "select count(recvDate) from inquiry where year='$year_org' and month='05'";
    // $mayquer = mysql_query($may);
    // while($mayrow = mysql_fetch_array($mayquer))
    // {
    // $may_data = $mayrow['count(recvDate)'];
    // }
    // $jun = "select count(recvDate) from inquiry where year='$year_org' and month='06'";
    // $junquer = mysql_query($jun);
    // while($junrow = mysql_fetch_array($junquer))
    // {
    // $jun_data = $junrow['count(recvDate)'];
    // }
    // $jul = "select count(recvDate) from inquiry where year='$year_org' and month='07'";
    // $julquer = mysql_query($jul);
    // while($julrow = mysql_fetch_array($julquer))
    // {
    // $jul_data = $junrow['count(recvDate)'];
    // }
    // $aug = "select count(recvDate) from inquiry where year='$year_org' and month='08'";
    // $augquer = mysql_query($aug);
    // while($augrow = mysql_fetch_array($augquer))
    // {
    // $aug_data = $augrow['count(recvDate)'];
    // }
    // $sep = "select count(recvDate) from inquiry where year='$year_org' and month='09'";
    // $sepquer = mysql_query($sep);
    // while($seprow = mysql_fetch_array($sepquer))
    // {
    // $sep_data = $seprow['count(recvDate)'];
    // }
    // $oct = "select count(recvDate) from inquiry where year='$year_org' and month='10'";
    // $octquer = mysql_query($oct);
    // while($octrow = mysql_fetch_array($octquer))
    // {
    // $oct_data = $octrow['count(recvDate)'];
    // }
    // $nov = "select count(recvDate) from inquiry where year='$year_org' and month='11'";
    // $novquer = mysql_query($nov);
    // while($novrow = mysql_fetch_array($novquer))
    // {
    // $nov_data = $novrow['count(recvDate)'];
    // }
    // $dec = "select count(recvDate) from inquiry where year='$year_org' and month='12'";
    // $decquer = mysql_query($dec);
    // while($decrow = mysql_fetch_array($decquer))
    // {
    // $dec_data = $decrow['count(recvDate)'];
    // }

    $anb = "[
							['JAN', $jan_data],
							['FEB', $feb_data],
							['MAR', $mar_data],
							['APR', $apr_data],
							['MAY', $may_data],
							['JUN', $jun_data],
							['JUL', $jul_data],
							['AUG', $aug_data],
							['SEP', $sep_data],
							['OCT', $oct_data],
							['NOV', $nov_data],
							['DEC', $dec_data]
						]";
    echo '<script>var arrayFromPhp = ' . $anb . ';</script>';
    ?>

    <?php
    // $siteVisitGrapCont = new inquiryGraph();
    // $week1 = $siteVisitGrapCont -> getSiteVisitGraphData('0',$admincustId);
    // $week2 = $siteVisitGrapCont -> getSiteVisitGraphData('1',$admincustId);
    // $week3 = $siteVisitGrapCont -> getSiteVisitGraphData('2',$admincustId);
    // $week4 = $siteVisitGrapCont -> getSiteVisitGraphData('3',$admincustId);
    // $week5 = $siteVisitGrapCont -> getSiteVisitGraphData('4',$admincustId);
    // $visit_sel1 = "select * from google_analytic where week='0'";
    // $visit_quer1 = mysql_query($visit_sel1);
    // while($visit_row1 = mysql_fetch_array($visit_quer1))
    // {
    // $week1 = $visit_row1['sess'];
    // }
    // $visit_sel2 = "select * from google_analytic where week='1'";
    // $visit_quer2 = mysql_query($visit_sel2);
    // while($visit_row2 = mysql_fetch_array($visit_quer2))
    // {
    // $week2 = $visit_row2['sess'];
    // }
    // $visit_sel3 = "select * from google_analytic where week='2'";
    // $visit_quer3 = mysql_query($visit_sel3);
    // while($visit_row3 = mysql_fetch_array($visit_quer3))
    // {
    // $week3 = $visit_row3['sess'];
    // }
    // $visit_sel4 = "select * from google_analytic where week='3'";
    // $visit_quer4 = mysql_query($visit_sel4);
    // while($visit_row4 = mysql_fetch_array($visit_quer4))
    // {
    // $week4 = $visit_row4['sess'];
    // }
    // $visit_sel5 = "select * from google_analytic where week='4'";
    // $visit_quer5 = mysql_query($visit_sel5);
    // while($visit_row5 = mysql_fetch_array($visit_quer5))
    // {
    // $week5 = $visit_row5['sess'];
    // }
    // $site_data = "[
    // ['29 Nov', $week1],
    // ['6 Dec', $week2],
    // ['13 Dec', $week3],
    // ['21 Dec', $week4],
    // ['28 Dec', $week5]
    // ]";
    // echo '<script> alert('.$site_data.'); </script>';
    // echo '<script>var site_vis = ' . $site_data . ';</script>';
    ?>

    <!-- BEGIN CORE PLUGINS -->
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
    <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
    <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
    <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->






    <?php
    /*
      <!-- BEGIN CORE PLUGINS -->
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
      <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
      <!-- END CORE PLUGINS -->
      <!-- BEGIN PAGE LEVEL PLUGINS -->
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/moment.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL PLUGINS -->
      <!-- BEGIN THEME GLOBAL SCRIPTS -->
      <script src="<?=SITE_ADMIN_URL;?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>
      <!-- END THEME GLOBAL SCRIPTS -->
      <!-- BEGIN PAGE LEVEL SCRIPTS -->
      <!--<script>var arrayFromPhp = <?php //echo json_encode($anb); ?></script>-->
      <script src="<?=SITE_ADMIN_URL;?>/assets/pages/scripts/dashboard.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/pages/scripts/login.min.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL SCRIPTS -->
      <!-- BEGIN THEME LAYOUT SCRIPTS -->
      <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
      <script src="<?=SITE_ADMIN_URL;?>/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
      <!-- END THEME LAYOUT SCRIPTS -->

     */
    ?>
    <?php
}
?>

<?php

function admin_change_pass() {
    // $res=getResult("admin_details"," where user_id='".$_SESSION['MyAdminUserID']."' and user_type='".$_SESSION['MyAdminUserType']."'");
    ?>
    <script type="text/javascript">
        function validate_form() {
            var frm = document.chgPwd;
            if (frm.username.value == 0) {
                alert("Please enter username");
                frm.username.focus();
                return false;
            }
            if (frm.old_password.value == 0) {
                alert("Please enter your current password");
                frm.old_password.focus();
                return false;
            }
            if (frm.password.value == 0) {
                alert("Please enter your new password");
                frm.password.focus();
                return false;
            }
            if (frm.repassword.value == 0) {
                alert("Please confirm your new password");
                frm.repassword.focus();
                return false;
            }
            if (frm.password.value != frm.repassword.value) {
                alert("Password and Confirm Password mismatched");
                frm.repassword.focus();
                return false;
            }
        }
    </script>

    <form action="" method="post" name="chgPwd" id="chgPwd" onsubmit="return validate_form();">
        <table width="550" border="0" align="center" cellpadding="0" cellspacing="0" class="tableForm"> 
            <tr>
                <td class="tdLabel" colspan="2" align="center">
                    <?php
                    if ($_SESSION[session_message] != '') {
                        echo '<font color="#C05813">' . print_message() . '</font><br>';
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td width="120" class="tdLabel">Username:<font color="red">*</font></td>
                <td><input type="text" name="username" value="<?= $res[user_id]; ?>" class="textfield"></td>
            </tr>

            <tr>
                <td width="120" class="tdLabel">Current Password:<font color="red">*</font></td>
                <td><input type="password" name="old_password" class="textfield" /></td>
            </tr>

            <tr>
                <td class="tdLabel">New Password:<font color="red">*</font></td>
                <td><input type="password" name="password" class="textfield"></td>
            </tr>

            <tr>
                <td class="tdLabel">Confirm Password:<font color="red">*</font></td>
                <td><input type="password" name="repassword" class="textfield"></td>
            </tr>

            <tr>
                <td class="label">&nbsp;</td>
                <td>
                    <input type="image" name="imageField" src="images/buttons/submit.gif" />
                    <input type="hidden" name="action" value="Update">
                </td>
            </tr> 
        </table> 
    </form>
    <?php
}
?>
			