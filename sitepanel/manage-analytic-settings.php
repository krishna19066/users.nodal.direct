<?php
include "admin-function.php";

// include "cms_functions.php";
// user_locked();
// staff_restriction();
$customerId = $_SESSION['customerID'];
$analyticsCont = new analyticsPage();
$userData = $analyticsCont->getUsersData($customerId);
$userDataRes = $userData[0];
$base_url = $userDataRes->website;
?>
<div id="add_prop">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
        <head>
            <style>
                #top_menu_container
                {
                    background-color: #f5f8fd;
                    border-color: #8bb4e7;
                    color: #678098;
                    margin: 0 0 20px;
                    border-left: 5px solid #8bb4e7;
                }

                #top_menu
                {
                    list-style: none;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin: 0px;

                }
                #top_menu li
                {
                    padding: 15px 20px;
                    cursor: pointer;
                }
                .OurMenuActive
                {
                    background-color: #5c9cd1;
                    color: white;
                }
            </style>
            <style>
                .table_prop  td
                {
                    padding:3px 0px 3px 5px;
                    bordder:0.5px solid #000;

                }

                .table_prop2 th 
                {
                    color: #fff;
                }

                .table_prop2	tr:nth-child(even) 
                {
                    background-color: #E9EDEF;
                }		

                .table_prop td:first-child 
                {
                    font-weight:bold;
                }

                .table_prop td:nth-child(3) 
                {
                    font-weight:bold;
                    width: 120px;
                }

                .table.table_prop 
                {
                    table-layout:fixed; 
                }

                .table_prop3 
                {
                    table-layout:fixed;
                }

                .data_box 
                {
                    padding:50px ! important;
                }
            </style>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
            <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME LAYOUT STYLES -->

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> Manage Analytic Setting
                                    <small>View and Edit Meta Tags and Analytic Code</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="manage-property-settings.php">Properties Setting</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Manage Analytic Setting</span>
                            </li>
                        </ul>

                       <!-- <div id="top_menu_container">
                            <ul id="top_menu">
                                <a href="manage-analytic-settings.php" style="text-decoration:none;"><li class="OurMenuActive">Manage Meta Tags</li></a>

                                <li onclick="load_add_metatag();">Add Meta Tags</li>
                                <li onclick="load_analytic_code();">Add Google Analytics</li>
                            </ul>
                        </div>-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-body ">
                                        <div style="width:100%;" class="clear"> 
                                            <a class="pull-right">
                                                <a href="ajax_add_meta_tag.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -12px;"><i class="fa fa-plus"></i> &nbsp Add Meta Tags</button></a>
                                                <a href="ajax_add_analytic_code.php"><button style="background:#ff9800;color:white;border:none;height:35px;width:190px;font-size:14px; float: right;margin-top: -12px; margin-right: 21px;"><i class="fa fa-plus"></i> &nbsp Add Google Analytics</button></a>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="cityCont" style="background:white;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body ">
                                            <div style="width:100%;" class="clear"> 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <?php
                                $count = 1;
                                $metaTagsList1 = $analyticsCont->getMetaTagsData($customerId);
                                foreach ($metaTagsList1 as $metaTagsData) {
                                    ?>
                                    <div style="padding-left:35px; color:rgba(0, 0, 0, 0.7);">
                                        <table class="ques_table" width="90%">
                                            <tbody>
                                                <?php
                                                if ($count == "1") {
                                                    $color1 = "#32c5d2";
                                                }
                                                if ($count == "2") {
                                                    $color1 = "#3598dc";
                                                }
                                                if ($count == "3") {
                                                    $color1 = "#36D7B7";
                                                }
                                                if ($count == "4") {
                                                    $color1 = "#5e738b";
                                                }
                                                if ($count == "5") {
                                                    $color1 = "#1BA39C";
                                                }
                                                if ($count == "6") {
                                                    $color1 = "#32c5d2";
                                                }
                                                if ($count == "7") {
                                                    $color1 = "#578ebe";
                                                }
                                                if ($count == "8") {
                                                    $color1 = "#8775a7";
                                                }
                                                if ($count == "9") {
                                                    $color1 = "#E26A6A";
                                                }
                                                if ($count == "10") {
                                                    $color1 = "#29b4b6";
                                                }
                                                if ($count == "11") {
                                                    $color1 = "#4B77BE";
                                                }
                                                if ($count == "12") {
                                                    $color1 = "#c49f47";
                                                }
                                                if ($count == "13") {
                                                    $color1 = "#67809F";
                                                }
                                                if ($count == "14") {
                                                    $color = "#8775a7";
                                                }
                                                if ($count == "15") {
                                                    $color1 = "#73CEBB";
                                                }
                                                ?>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr style="border-left: 10px solid <?php echo $color1; ?>;line-height:25px;">
                                                    <td style="width:70%;"><b style="padding: 10px;"> <span style="color:<?php echo $color1; ?>;"> URl :<?php echo $base_url; ?>/<?php echo $metaTagsData->filename; ?>.html </span></b><br/>
                                                        <span> <b style="padding: 10px;"> Title: </b> <?php echo $metaTagsData->MetaTitle; ?> </span><br/>
                                                        <span><b style="padding: 10px;"> Meta Keyword:</b> <?php echo $metaTagsData->MetaKwd; ?></span>	
                                                    </td>
                                                    <td style="padding-left:50px; text-align:right;">
                                                        <a href="edit_meta_tage.php?id=edit&metano=<?php echo $metaTagsData->metano; ?>&url=<?php echo $metaTagsData->filename; ?>" class="btn btn-xs blue">
                                                            <i class="fa fa-edit"></i> Edit 
                                                        </a> &nbsp &nbsp &nbsp &nbsp 
                                                        <a href="updatePage_content.php?id=delete&metano=<?php echo $metaTagsData->metano; ?>" onClick="return confirm('Are you sure you want to delete this record')" class="btn btn-xs red"> 
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><br><br>
                                            <?php
                                            $count++;
                                        }
                                        ?>
                                        </div><br><br>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                </body>
                                                </html>

                                                <script>
                                                    $('#top_menu li').click(function () {
                                                        $('#top_menu li').removeClass('OurMenuActive');
                                                        $(this).addClass('OurMenuActive');
                                                    });
                                                </script>

                                                <script>
                                                    function load_add_metatag() {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#cityCont').html(loadng);


                                                        //var replace = 'btn_div' + but;

                                                        $.ajax({url: 'ajax_lib/ajax_add_meta_tag.php',
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#cityCont').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>
                                                <script>
                                                    function load_analytic_code() {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#cityCont').html(loadng);


                                                        //var replace = 'btn_div' + but;

                                                        $.ajax({url: 'ajax_lib/ajax_add_analytic_code.php',
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#cityCont').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>

                                                <script>
                                                    function load_property_type() {
                                                        var loadng = '<center><img src="loading.gif" style="width:200px;" /></center>';
                                                        $('#cityCont').html(loadng);


                                                        //var replace = 'btn_div' + but;

                                                        $.ajax({url: 'ajax_lib/ajax_property_type.php',
                                                            type: 'post',
                                                            success: function (output) {
                                                                // alert(output);
                                                                $('#cityCont').html(output);
                                                            }
                                                        });
                                                    }
                                                </script>
                                                <!-- BEGIN PAGE LEVEL PLUGINS -->
                                                <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
                                                <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                                                <!-- END PAGE LEVEL PLUGINS -->
                                                <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                                <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
                                                <!-- END PAGE LEVEL SCRIPTS -->
                                                <?php
                                                admin_footer();
                                                ?>        
                                                </div>	