<?php
include "admin-function.php";
checkUserLogin();

$customerId = $_SESSION['customerID'];
$propertyData = new propertyData();
$enquiryData = new manageEnquiry();

$getAllEnquiry = $enquiryData->GetAllManageBookingData($customerId);
$reccnt = count($getAllEnquiry);

$inqid = $_GET['inqid'];
if ($inqid == 'InactiveBooking') {
    $inqid = 'Inactive Booking';
    $getAllEnquiry = $enquiryData->GetBookingStatus($customerId, $inqid);
}
if ($inqid == 'Completed') {

    $getAllEnquiry = $enquiryData->GetBookingStatus($customerId, $inqid);
}


$start = (intval($start) == 0 or $start == "") ? $start = 0 : $start;
$pagesize = intval($pagesize) == 0 ? $pagesize = 10 : $pagesize;
$heading = "Manage Booking";
$record_type = "Inquiry";
$table_name = "manage_booking";
$cond = "where customerID='$customerId'";
?>

<div id="add_prop">
    <html lang="en">
        <head>
            <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
            <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

            <?php
            adminCss();
            ?>
        </head>

        <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
            <?php
            themeheader();
            ?>
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>         
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="background:#e9ecf3;">
                        <div class="page-head">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1> MANAGE Booking
                                    <small>View and manage your Booking</small>
                                </h1>
                            </div>
                        </div>

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="welcome.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>

                            <li>
                                <span>Booking</span>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> Manage Booking </span>
                                        </div>
                                        <div class='button'><br>
                                            <a href="#" id ="export" role='button'><button style="background:hsl(166, 78%, 46%);border:none;color:white;width:170px;height:40px;float:right;font-weight:bold;margin-top: -15px;">Export CSV</button></a>
                                        </div><br><br>
                                    </div>
                                    <div style="float:right;padding:10px;"><i class="fa fa-star" style="color:gold;font-size:20px;"></i> &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i><font style="font-size:18px;"> &nbsp; Not Commented </font></div><br><br>
                                    <?php
                                    if (isset($_SESSION['err'])) {
                                        ?>
                                        <center><span class="caption-subject font-dark sbold uppercase"><?php echo "<font style='color:red;'>" . $_SESSION['err'] . "</font>"; ?></span></center>
                                        <?php
                                        unset($_SESSION['err']);
                                    }
                                    if (isset($_SESSION['mail_sent'])) {
                                        ?>
                                        <center><span class="caption-subject font-dark sbold uppercase"><?php echo "<font style='color:red;'>" . $_SESSION['mail_sent'] . "</font>"; ?></span></center>
                                        <?php
                                        unset($_SESSION['mail_sent']);
                                    }
                                    ?>
                                    <div class='container' style="display:none;"> 
                                        <div id="dvData">
                                            <table>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Contact</th>
                                                    <th>Amount</th>
                                                </tr>
                                                <?php
                                                $csvsel = $enquiryData->GetAllManageBookingData($customerId);
// $csvsel = "select * from inquiry order by slno desc";
// $csvquer = mysql_query($csvsel);
                                                foreach ($csvsel as $csvrow) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $csvrow->name; ?></td>
                                                        <td><?php echo $csvrow->email; ?></td>
                                                        <td><?php echo $csvrow->phone; ?> </td>
                                                        <td><?php echo $csvrow->token_amount; ?> </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php
                                        if ($reccnt > 0) {
                                            ?>
                                            <div class="row">

                                                <div class="inbox">
                                                    <div class="col-md-3" style="margin-top:65px;padding-right:0px">
                                                        <div class="inbox-sidebar">
                                                            <ul class="inbox-nav" style="color:#36c6d3; width:200px;" >
                                                                <li style="list-style: none !important;border-bottom:1px solid #ddd; padding:10px;<?php if ($inqid == "") { ?> border-left:5px solid #F44336; <?php } ?> color:#d4645b;" class='tabs' onclick="update_tab(this)">
                                                                    <a href="manage-booking.php">
                                                                        <span href="javascript:;" name="all" ><b> All Booking </b></span>                                                   
                                                                    </a>
                                                                </li>										
                                                                <li class="tabs" style="list-style: none !important;border-bottom:1px solid #ddd;padding:10px;<?php if ($inqid == "Inactive Booking") { ?>   border-left:5px solid #F44336;color:#d4645b; <?php } ?> " onclick="update_tab(this)">
                                                                    <a href="manage-booking.php?inqid=InactiveBooking" style="color:#36c6d3;">
                                                                        <span href="javascript:;" name="InactiveBooking"  ><b> Inactive Booking </b></span>
                                                                    </a>
                                                                </li>
                                                                <li class="tabs" style="list-style: none !important;border-bottom:1px solid #ddd;padding:10px;<?php if ($inqid == "Completed") { ?>   border-left:5px solid #F44336;color:#d4645b; <?php } ?> " onclick="update_tab(this)">
                                                                    <a href="manage-booking.php?inqid=Completed" style="color:#36c6d3;">
                                                                        <span href="javascript:;" name="Completed" ><b> Completed </b></span>
                                                                    </a>
                                                                </li>
                                                               <!-- <li class="tabs" style="list-style: none !important;border-bottom:1px solid #ddd;padding:10px;<?php if ($inqid == "contactus") { ?>   border-left:5px solid #F44336;color:#d4645b; <?php } ?> " onclick="update_tab(this)">
                                                                    <a href="manage-enquiry.php?inqid=contactus" style="color:#36c6d3;">
                                                                        <span href="javascript:;" name="contactus"  ><b> Customer-inquiry </b></span>
                                                                    </a>
                                                                </li>
                                                                <?php //if ($schedule_count > 0) { ?>
                                                                    <li class="tabs" style="list-style: none !important;border-bottom:1px solid #ddd;padding:10px;<?php if ($inqid == "schedule-visit") { ?>   border-left:5px solid #F44336;color:#d4645b; <?php } ?> " onclick="update_tab(this)">
                                                                        <a href="manage-enquiry.php?inqid=schedule-visit" style="color:#36c6d3;">
                                                                            <span href="javascript:;" name="schedule-visit"><b> Schedule-inquiry</b></span>
                                                                        </a>
                                                                    </li>-->
                                                                <?php //} ?>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-9" style="padding-left:0px">

                                                        <div class="inbox-body">
                                                            <div class="inbox-content" style="">
                                                                <div id="replace_inbox">
                                                                    <table class="table table-striped table-advance table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    <p><b>Name</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Email</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Status</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Contact Us</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Amount Pay</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Booking Date</b></p>
                                                                                </th>
                                                                                <th style="text-align:center">
                                                                                    <p><b> </b></p>
                                                                                </th>
                                                                                <th style="text-align:left">
                                                                                    <p><b> Action</b></p>
                                                                                </th>
                                                                                <th> </th>

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="inq_tab">

                                                                            <?php
                                                                            $cnt = 1;
                                                                            $sl = $start + 1;
                                                                            foreach ($getAllEnquiry as $data) {
                                                                                //  $data = ms_htmlentities_decode($data);
                                                                                if ($sl % 2 != 0) {
                                                                                    $color = "#ffffff";
                                                                                } else {
                                                                                    $color = "#f6f6f6";
                                                                                }
                                                                                if ($cnt == "1") {
                                                                                    $color1 = "#32c5d2";
                                                                                }
                                                                                if ($cnt == "2") {
                                                                                    $color1 = "#3598dc";
                                                                                }
                                                                                if ($cnt == "3") {
                                                                                    $color1 = "#36D7B7";
                                                                                }
                                                                                if ($cnt == "4") {
                                                                                    $color1 = "#5e738b";
                                                                                }
                                                                                if ($cnt == "5") {
                                                                                    $color1 = "#1BA39C";
                                                                                }
                                                                                if ($cnt == "6") {
                                                                                    $color1 = "#32c5d2";
                                                                                }
                                                                                if ($cnt == "7") {
                                                                                    $color1 = "#578ebe";
                                                                                }
                                                                                if ($cnt == "8") {
                                                                                    $color1 = "#8775a7";
                                                                                }
                                                                                if ($cnt == "9") {
                                                                                    $color1 = "#E26A6A";
                                                                                }
                                                                                if ($cnt == "10") {
                                                                                    $color1 = "#29b4b6";
                                                                                }
                                                                                if ($cnt == "11") {
                                                                                    $color1 = "#4B77BE";
                                                                                }
                                                                                if ($cnt == "12") {
                                                                                    $color1 = "#c49f47";
                                                                                }
                                                                                if ($cnt == "13") {
                                                                                    $color1 = "#67809F";
                                                                                }
                                                                                if ($cnt == "14") {
                                                                                    $color = "#8775a7";
                                                                                }
                                                                                if ($cnt == "15") {
                                                                                    $color1 = "#73CEBB";
                                                                                }
                                                                                ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<tr onclick="$('#div<?php // echo $cnt;                                         ?>').toggle('1000'); hdsh(this);" >-->
                                                                                <tr onclick="hdsh(<?php echo $cnt; ?>);">

                                                                                    <td class="view-message hidden-xs"> <?php echo $data->name; ?> </td>
                                                                                    <td class="view-message view-message"> <?php echo $data->email; ?> </td>
                                                                                    <td class="view-message view-message"> <?php echo $data->status; ?> </td>
                                                                                    <td class="view-message inbox-small-cells"> <?php echo $data->phone; ?></td>
                                                                                    <td class="view-message inbox-small-cells"><span style="color:green;">₹ <?php echo $data->token_amount; ?></span></td>
                                                                                    <td class="view-message text-center"> <?php echo $data->timestamp; ?> </td>
                                                                                    <td style='padding:5px; text-align:right;'><button type="button" class=" btn-info btn" data-toggle="modal" data-target="#myModal<?php echo $data->s_no; ?>">View Details</button></td>
                                                                                    <td class="view-message text-center">  
                                                                                        <a href="manage-booking.php?inqid=<?php echo $inqid; ?>&id=delete&recordID=<?php echo $data->s_no; ?>" title="Delete" onClick="return confirm('Are you sure you want to delete this record')">
                                                                                            <i class="fa fa-trash" style="color:red;"></i>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>                                                                           
                                                                                <tr id="#query_1">

                                                                                </tr>

                                                                                <!-- Modal -->
                                                                            <div class="modal fade" id="myModal<?php echo $data->s_no; ?>" role="dialog">
                                                                                <div class="modal-dialog">
                                                                                    <?php $id = $data->s_no; ?>
                                                                                    <!-- Modal content-->
                                                                                    <div class="modal-content" style="width: 814px;">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                            <h4 class="modal-title"><b>Booking Details:<?php echo $data->s_no; ?></b></h4>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <?php
                                                                                            $booking_data = $enquiryData->GetBookingByID($id, $customerId);
                                                                                            $res = $booking_data[0];
                                                                                            $propertyId = $res->prop_id;

                                                                                            $property_result = $propertyData->GetPropertyDataWithId($propertyId);
                                                                                            $prop_res = $property_result[0];
                                                                                            $propertName = $prop_res->propertyName;
                                                                                            ?>
                                                                                            <div class="row">
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>UserName:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->name ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Email:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->email ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Mobile:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->phone ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Status:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->status ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Property:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $propertName ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Moving Date:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->moving_date ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Status:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->status ?></span>
                                                                                                </div><br/>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span>Amount Pay:</span>
                                                                                                </div>
                                                                                                <div class=" col-md-6 col-lg-6 "> 
                                                                                                    <span><?php echo $res->token_amount ?></span>
                                                                                                </div><br/>

                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="modal-footer" style="margin-top: 14px;">
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <?php
                                                                            $cnt++;
                                                                        }
                                                                        ?>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>	<br><br>
                                                <?php echo "<br><br>" . pageNavigation($reccnt, $pagesize, $start, $link); ?>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="row">

                                                <div class="inbox">
                                                    <div class="col-md-3" style="margin-top:65px;padding-right:0px">
                                                        <div class="inbox-sidebar">
                                                            <ul class="inbox-nav" style="color:#36c6d3; width:200px;" >
                                                                <li style="list-style: none !important;border-bottom:1px solid #ddd; padding:10px; border-left:5px solid #F44336;color:#d4645b;" class='tabs' onclick="update_tab(this)">
                                                                    <a href="manage-booking.php">
                                                                        <span href="javascript:;" name="all" ><b> All Members </b></span>                                                   
                                                                    </a>
                                                                </li>										
                                                                <li class="tabs" style="list-style: none !important; padding:10px;" class='tabs' onclick="update_tab(this)">
                                                                    <a href="manage-booking.php?inqid=InactiveBooking" style="color:#36c6d3;">
                                                                        <span href="javascript:;" name="InactiveBooking"  ><b> Call back </b></span>
                                                                    </a>
                                                                </li>
                                                                <li style="list-style: none !important; padding:10px;" class='tabs' onclick="update_tab(this)">
                                                                    <a href="manage-booking.php?inqid=Completed" style="color:#36c6d3;">
                                                                        <span href="javascript:;" name="Completed" ><b> Instant booking </b></span>
                                                                    </a>
                                                                </li>
                                                                <!--<li style="list-style: none !important; padding:10px;" class='tabs' onclick="update_tab(this)">
                                                                    <a href="manage-enquiry.php?inqid=contactus" style="color:#36c6d3;">
                                                                        <span href="javascript:;" name="contactus"  ><b> Customer-inquiry </b></span>
                                                                    </a>
                                                                </li>
                                                                <?php // if ($schedule_count > 0) {  ?>
                                                                    <li style="list-style: none !important; padding:10px;" class='tabs' onclick="update_tab(this)">
                                                                        <a href="manage-enquiry.php?inqid=schedule-visit" style="color:#36c6d3;">
                                                                            <span href="javascript:;" name="schedule-visit"  ><b> Customer-inquiry </b></span>
                                                                        </a>
                                                                    </li>-->
                                                                <?php //}  ?>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-9" style="padding-left:0px">

                                                        <div class="inbox-body">
                                                            <div class="inbox-content" style="">
                                                                <div id="replace_inbox">
                                                                    <table class="table table-striped table-advance table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    <p></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Name</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Email</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Status</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Contact No</b></p>
                                                                                </th>
                                                                                <th>
                                                                                    <p><b>Amount Pay</b></p>
                                                                                </th>
                                                                                <th style="text-align:center">
                                                                                    <p><b> Booking Date</b></p>
                                                                                </th>
                                                                                <th style="text-align:center">
                                                                                    <p><b> Action</b></p>
                                                                                </th>																	
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td> Sorry! No Records Found....</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>

                        .page-header { position: relative; }
                        .reviews {
                            color: #555;    
                            font-weight: bold;
                            margin: 10px auto 20px;
                        }
                        .notes {
                            color: #999;
                            font-size: 12px;
                        }
                        .media .media-object { max-width: 60px; }
                        .media-body { position: relative; }
                        .media-date { 
                            position: absolute; 
                            right: 25px;
                            top: 25px;
                        }
                        .media-date li { padding: 0; }
                        .media-date li:first-child:before { content: ''; }
                        .media-date li:before { 
                            content: '.'; 
                            margin-left: -2px; 
                            margin-right: 2px;
                        }
                        .media-comment { margin-bottom: 20px; }
                        .media-replied { margin: 0 0 20px 50px; }
                        .media-replied .media-heading { padding-left: 6px; }

                        .btn-circle {
                            font-weight: bold;
                            font-size: 12px;
                            padding: 6px 15px;
                            border-radius: 20px;
                        }
                        .btn-circle span { padding-right: 6px; }
                        .embed-responsive { margin-bottom: 20px; }
                        .tab-content {

                            border: 1px solid #ddd;
                            border-top: 0;

                        }

                        .custom-input-file:hover .uploadPhoto { display: block; }
                        .anyClass {
                            height:350px;
                            overflow-y: scroll;
                        }
                        .anyClass {
                            height:350px;
                            overflow-y: scroll;
                        }
                    </style>
                    </body>


                    <script src="<?= SITE_ADMIN_URL; ?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL PLUGINS -->
                    <script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.min.js'></script>
                    <script>
                                                                function hdsh(tro)
                                                                {
                                                                    var tr_len = document.getElementById('inq_tab').children.length / 2;
                                                                    for (i = 1; i <= tr_len; i++)
                                                                    {
                                                                        if (i == tro)
                                                                        {
                                                                            // document.getElementById('div' + i).style.display = 'block';
                                                                            $('#div' + i).slideToggle();
                                                                        } else
                                                                        {
                                                                            document.getElementById('div' + i).style.display = 'none';
                                                                        }
                                                                    }
                                                                }

                    </script>
                    <script type='text/javascript'>
                        $(document).ready(function () {

                            console.log("HELLO")
                            function exportTableToCSV($table, filename) {
                                var $headers = $table.find('tr:has(th)')
                                        , $rows = $table.find('tr:has(td)')

                                        // Temporary delimiter characters unlikely to be typed by keyboard
                                        // This is to avoid accidentally splitting the actual contents
                                        , tmpColDelim = String.fromCharCode(11) // vertical tab character
                                        , tmpRowDelim = String.fromCharCode(0) // null character

                                        // actual delimiter characters for CSV format
                                        , colDelim = '","'
                                        , rowDelim = '"\r\n"';

                                // Grab text from table into CSV formatted string
                                var csv = '"';
                                csv += formatRows($headers.map(grabRow));
                                csv += rowDelim;
                                csv += formatRows($rows.map(grabRow)) + '"';

                                // Data URI
                                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                                // For IE (tested 10+)
                                if (window.navigator.msSaveOrOpenBlob) {
                                    var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
                                        type: "text/csv;charset=utf-8;"
                                    });
                                    navigator.msSaveBlob(blob, filename);
                                } else {
                                    $(this)
                                            .attr({
                                                'download': filename
                                                , 'href': csvData
                                                        //,'target' : '_blank' //if you want it to open in a new window
                                            });
                                }

                                //------------------------------------------------------------
                                // Helper Functions 
                                //------------------------------------------------------------
                                // Format the output so it has the appropriate delimiters
                                function formatRows(rows) {
                                    return rows.get().join(tmpRowDelim)
                                            .split(tmpRowDelim).join(rowDelim)
                                            .split(tmpColDelim).join(colDelim);
                                }
                                // Grab and format a row from the table
                                function grabRow(i, row) {

                                    var $row = $(row);
                                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                                    var $cols = $row.find('td');
                                    if (!$cols.length)
                                        $cols = $row.find('th');

                                    return $cols.map(grabCol)
                                            .get().join(tmpColDelim);
                                }
                                // Grab and format a column from the table 
                                function grabCol(j, col) {
                                    var $col = $(col),
                                            $text = $col.text();

                                    return $text.replace('"', '""'); // escape double quotes

                                }
                            }


                            // This must be a hyperlink
                            $("#export").click(function (event) {
                                // var outputFile = 'export'
                                var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                                outputFile = outputFile.replace('.csv', '') + '.csv'

                                // CSV
                                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);

                                // IF CSV, don't do event.preventDefault() or return false
                                // We actually need this to be a typical hyperlink
                            });
                        });
                    </script>
                    <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
                    <?php
                    admin_footer();
                    ?>
                    </html>
                </div>