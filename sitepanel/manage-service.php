<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$userType = $_SESSION['MyAdminUserType'];
if ($userType == 'user') {
    $user_id = $_SESSION['MyAdminUserID'];
} else {
    $user_id = '';
}
$propertyCont = new propertyData();
$staticData = new staticPageData();
function slugify($text){
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
      return 'n-a';
    }
    return $text;
}

if(isset($_REQUEST['add_service_sub'])){
 $serviceName = $_REQUEST['serviceName'];
 $serviceURL1 = $_REQUEST['serviceURL'];
 $countryID = $_REQUEST['countryID'];
 $city_id = $_REQUEST['city_id'];
 $serviceType = $_REQUEST['serviceType'];
 $rate_type = $_REQUEST['rate_type'];
 $rate = $_REQUEST['rate'];
 $description = $_REQUEST['description'];
 $address = $_REQUEST['address'];
 $short_description = $_REQUEST['short_description'];
 $getcountryName = $propertyCont->GetcountryWithID($countryID);
 foreach($getcountryName as $dt){
     $countryName = $dt->name;
 }
  if($serviceURL1 == ''){
	  $serviceURL99 = slugify($serviceName);
	  $serviceURL = $serviceURL99 .'-in-'.$countryName;
 }else{
	 $serviceURL = $serviceURL1;
 }
 
 $getcityName = $propertyCont->GetcityWithID($city_id);
 foreach($getcityName as $rt){
     $cityName = $rt->name;
 }
 $meta_title = $serviceName;
 $filename = $serviceURL;
 $dec1 = $description;
 $dec = substr($dec1, 0, 250);
 $date = date('Y-m-d');
 $addMetaTags = $staticData->AddMetaTags($customerId, $meta_title, $filename, $dec, $date);

  $ins = $propertyCont->AddService($customerId, $serviceName,$serviceURL,$countryName,$cityName,$countryID,$city_id,$serviceType,$rate_type,$rate,$description,$address,$short_description);
        echo '<script type="text/javascript">
                alert("Succesfuly Added Service");              
window.location = "manage-service.php";
            </script>';
    
    
}

function generateSeoURL($string, $wordLimit = 0) {
    $separator = '-';

    if ($wordLimit != 0) {
        $wordArr = explode(' ', $string);
        $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
    }

    $quoteSeparator = preg_quote($separator, '#');

    $trans = array(
        '&.+?;' => '',
        '[^\w\d _-]' => '',
        '\s+' => $separator,
        '(' . $quoteSeparator . ')+' => $separator
    );

    $string = strip_tags($string);
    foreach ($trans as $key => $val) {
        $string = preg_replace('#' . $key . '#i' . (UTF8_ENABLED ? 'u' : ''), $val, $string);
    }

    $string = strtolower($string);

    return trim(trim($string, $separator));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php echo ajaxCssHeader(); ?>
        
        <script>
            $("#field1, #field2").keyup(function () {
                update();
            });

            function update() {
                $("#result").val($('#field1').val() + " " + $('#field2').val());
            }
        </script>
        <script>
            function AvoidSpace(event) {
                var k = event ? event.which : window.event.keyCode;
                if (k == 32)
                    return false;
            }
        </script>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">	
        <?php
        themeheader();
        ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php
            admin_header('manage-property.php');
            ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add New Service
                                <small>Add a new Service here..</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="index.html">My Listing</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listing (Add Listing)</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="">
                                <div class="portlet-title">
                <span style="float:left;"><input type="button" style="background:#36c6d3;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_Service').slideToggle();" value="Add Listing" /></span>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" action="" id="submit_form" method="post">
                                        <div class="form-wizard">
                                            <div class="form-body" id="add_Service" style="display:none;">

                                                <!------------------------------------- Tab 1 (Basic Information) Starts ------------------------------------->

                                                <h3 class="block">Basic Listing Details</h3>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Listing Name
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="propertyName" class="form-control" name="serviceName"  />
                                                        <span class="help-block"> Provide your Listing name </span>
                                                    </div>
                                                </div>
												

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Service URL
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="PropertyUrl" class="form-control"  name="serviceURL" onkeypress="return AvoidSpace(event)" />
                                                        <span class="help-block"> Provide your desired Service URL not .html </span>
                                                    </div>

                                                </div>
                                                <div class="form-group">

                                                    <label for="inputState" class="control-label col-md-3">Select Country</label>
                                                    <div class="col-md-4">
                                                        <select name="countryID" id="country" class="form-control" required="">
                                                            <option value="">--- Select Country ---</option>
                                                            <?php
                                                            $countryList1 = $propertyCont->getAllCountryListing();
                                                            foreach ($countryList1 as $countryList) {
                                                                ?>
                                                                <option value="<?php echo $countryList->id; ?>"><?php echo $countryList->name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputState" class="control-label col-md-3">Select City </label>
                                                     <div class="col-md-4">
                                                    <select name="city_id" id="city" class="form-control" required="">
                                                        <option value="">Select Country first</option>                                                                   
                                                    </select>
                                                </div>
                                                </div>
												 <div class="form-group">
                                                    <label class="control-label col-md-3">Address
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="address" class="form-control" name="address"/>
                                                        <span class="help-block"> Provide your address </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                 <label for="inputState" class="control-label col-md-3">Select Service Type</label>
                                                    <div class="col-md-4">
                                                        <select name="serviceType" id="country" class="form-control" required="">
                                                            <option value="">--- Select Type ---</option>                                                          
                                                               <?php
                                                            $serviceList1 = $propertyCont->getAllServiceType();
                                                            foreach ($serviceList1 as $List) {
                                                                ?>
                                                                <option value="<?php echo $List->service_name; ?>"><?php echo $List->service_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                               <!-- <div class="form-group">
                                                    <label class="control-label col-md-3">Room Type
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="radio-list">
                                                            <table style="width:100%;">
                                                                <tr>
                                                                    <?php
                                                                    $roomTypCnt = 1;
                                                                    $roomTypList1 = $propertyCont->getAllRoomTyp($customerId);
                                                                    foreach ($roomTypList1 as $roomTypList) {
                                                                        ?>
                                                                        <td>
                                                                            <label>
                                                                                <input type="checkbox" name="room_type[]" value="<?php echo $roomTypList->apt_type_name; ?>" data-title="Male" /> <?php echo $roomTypList->apt_type_name; ?> 
                                                                            </label>
                                                                        </td>
                                                                        <?php
                                                                        if ($roomTypCnt % 3 == 0) {
                                                                            echo "</tr><tr>";
                                                                        }
                                                                        $roomTypCnt++;
                                                                    }
                                                                    ?>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="form_gender_error"> </div>
                                                        <span class="help-block">You can select multiple room types</span>
                                                    </div>
                                                </div>-->
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Rate (Price).
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="number" class="form-control" name="rate" />
                                                        <span class="help-block">Provide Rate </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Rate Type
                                                    </label>
                                                    <input type="radio" name="rate_type" value="Weekly"/>&nbsp;Weekly &nbsp &nbsp &nbsp
                                                    <input type="radio" name="rate_type" value="Monthly"/>&nbsp; Monthly

                                                    <span class="help-block"> Select Rate Type</span>
                                                </div>
												 <div class="form-group">
                                                    <label class="control-label col-md-3">Short Description
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="short_description" />
                                                        <span class="help-block">Provide Short Description </span>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label"> Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-3 col-md-9">                                                      
                                                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>"/>
                                                        <input type="submit" class="btn green button-submit" name="add_service_sub" value="Submit" />
                                                    </div>
                                                </div>

                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!---------------------------------------------------------------- Add Service Section Ends --------------------------------------------------------------------->
				
				  <div id="property_change">
                            <div class="row">
                                <div class="col-md-12">	
                                    <?php
                                    $cityListings1 = $propertyCont->getAllServiceCityListing($customerId);
                                    foreach ($cityListings1 as $cityListings) {
                                        $cityName = $cityListings->cityName;
										$countryName = $cityListings->countryName;
                                        ?>
                                       <a href="service-listing.php?cityName=<?php echo $cityName; ?>"> <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-list-item"><br><br>
                                                    <div class="dashboard-stat2 bordered">
                                                        <div class="display">
                                                            <div class="number">
                                                                <h3 class="font-green-sharp">
                                                                    <span><?php echo $cityName; ?></span>                                                                  
                                                                </h3>
														  <small class="" style="color:#ff9800!important;"><?php echo $countryName; ?></small>
                                                            </div>
                                                        </div>
                                                        <div class="progress-info">
                                                            <div class="progress">
                                                                <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
                                                                    <span class="sr-only"><?php echo $cityName; ?></span>
                                                                </span>
                                                            </div>
                                                            <div class="status">
                                                                <div class="status-title">Total Listing : </div>
                                                                <?php
                                                                $propertyCount = $propertyCont->GetAllServiceDataWithCity($customerId,$cityName);
                                                                $propCount = count($propertyCount);
                                                                ?>
                                                                <div class="status-number"><?php echo $propCount; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div></a>
                                                    <?php
                                                }
                                                ?>

                                                </div>
                                                </div>
                                                </div>								                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#country').on('change', function () {
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: 'POST',
                    url: 'cities_list.php',
                    data: 'country_id=' + countryID,
                    success: function (html) {
                        $('#city').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#city').html('<option value="">Select Country first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>

            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php echo ajaxJsFooter(); ?>
    </body>
</html>