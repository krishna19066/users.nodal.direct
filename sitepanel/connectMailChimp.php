<?php
include "admin-function.php";
checkUserLogin();

require_once('mailchimp/MC_OAuth2Client.php');
require_once('mailchimp/MC_RestClient.php');
require_once('mailchimp/miniMCAPI.class.php');
$client = new MC_OAuth2Client();
$customerId = $_SESSION['customerID'];
$inventoryCont = new inventoryData();
?>

<?php
/* -------------------------------- CustomerID Updation ------------------------------------------- */
@extract($_REQUEST);
?>

<?php
session_start();
?>
<html>
    <head>
        <style>
            .card{
                margin:50px;
                float:left;
                position:relative;             
                color:white;
                font-weight:bold;
            }

            .card input[type="radio"]
            {
                margin-top:-35px;
                margin-bottom:10px;
            }

            .card i{			
                font-size:55px;
                z-index:2;
                margin-top: 50%;
            }
            .overly{
                display:none;
                text-align:center;
                top:0;
                width:100%;
                height:300px;
                position:absolute;
                background:rgba(0, 0, 0, 0.5);
                color:#fff;			
            }

            .dn{
                display:none;
            }
            .db{
                display:block;
            }
            .card > :nth-child(3){
                display:block;
            }
        </style>
        <script>
            var toggle = function () {
                var mydiv = document.getElementById('newpost');
                if (mydiv.style.display === 'block' || mydiv.style.display === '')
                    mydiv.style.display = 'none';
                else
                    mydiv.style.display = 'block'
            }
        </script>
        <?php
        adminCss();
        ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN CONTAINER -->
            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <div class="row">
                            <br><br>
                            <div class="rate" style="width:100%;">
                                <div class="col-md-12">
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-equalizer font-red-sunglo"></i>
                                                <span class="caption-subject font-red-sunglo bold uppercase">Connect To Mail Chimp</span>
                                                <span class="caption-helper">MailChimp</span>
                                            </div>
                                        </div>
                                        <!--<span style="float:left;"> <a href="booking-engine.php"><button style="background:#FF9800;color:white;border:none;height:35px;width:160px;font-size:14px; margin-top:35px;"><i class="fa fa-plus"></i> &nbsp Booking Engine</button></a></span><br><br>-->
                                       <!-- <span style="float:right;"> <a href="manage_rates.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px;"><i class="fa fa-plus"></i> &nbsp Manage Rates</button></a></span><br><br>-->
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="form-body">
                                            <div class="body">
                                                <div class="property portlet light bordered">

                                                    <?php if (isset($_GET['error'])) { ?>

                                                        <h2>Complete Failure!: <?= $_GET['error'] ?></h2>
                                                        <br/><br/><br/>
                                                        This likely means that you have entered a bad client_id or client_secret
                                                        <em>or</em> you are being awesome and checking to see this failure occur.
                                                        <br/><br/><br/>
                                                        <a href="<?= $client->getLoginUri() ?>">Try Again?</a>
                                                        <?php
                                                    } else {
                                                        $session = $client->getSession();
                                                        if (!$session) {
                                                            ?>

                                                            <h2>Hrm, we received an auth code: <?= $_GET['code'] ?>, but were unable to retrieve the access_token</h2>
                                                            This likely means that you waited too long to exchange your <strong>code</strong> for an access token 
                                                            <em>or</em> you are being awesome and checking to see this failure occur.
                                                            <br/><br/><br/>
                                                            <a href="<?= $client->getLoginUri() ?>">Try Again?</a>
                                                            </body>
                                                            </html>
                                                            <?php
                                                        } else {
                                                            ?>

                                                            <h2>You are Successfully Connect! oauth token details:</h2>
                                                            <pre>
                                                                <?= print_r($session, true) ?>
                                                            </pre>
                                                            <?php
                                                            $rest = new MC_RestClient($session);
                                                            $data = $rest->getMetadata();
                                                            ?>
                                                            And here are the results of the follow-up metadata call:
                                                            <pre>
                                                                <?= print_r($data, true) ?>
                                                            </pre>           
                                                            Putting all of that data together, you can now use that to either:
                                                            <ol>
                                                                <li>Configure the endpoint in your application to <strong><?= $data['api_endpoint'] ?></strong></li>
                                                                <li>Create a standard formatted API Key using the OAuth2 access token (<?= $session['access_token'] ?>) and the data center (<?= $data['dc'] ?>) :
                                                                    <strong><?= $session['access_token'] ?>-<?= $data['dc'] ?></strong> which can be passed into most API wrappers.
                                                                </li>
                                                            </ol>
                                                            <h2>Want to see an authorization fail?</h2>
                                                            The <strong>code</strong> being exchange for an OAuth Access Token expires within 30 seconds - simply refresh this page in a minute.

                                                            <h3>Want proof? Here are 5 of your lists:</h3>
                                                            <ol>
                                                                <?php
                                                                $apikey = $session['access_token'] . '-' . $data['dc'];
                                                                $api = new MCAPI($apikey);
                                                                $api->useSecure(true);
                                                                $lists = $api->lists('', 0, 5);
                                                                $res = $lists['data'];
                                                                //print_r($res);
                                                                foreach ($lists['data'] as $list) {
                                                                    ?>
                                                                    <li><?= $list['name'] ?> with <?= $list['stats']['member_count'] ?> subscribers <?= $list['id'] ?></li>
                                                                    <?php
                                                                }
                                                                ?>
                                                                </ul>
                                                                <br/><br/><br/>
                                                                <a href="<?= $client->getLoginUri() ?>">Try Again?</a>
                                                                <?php
                                                                $access_token = $session['access_token'];
                                                                $dc = $data['dc'];
                                                                $list_id = $list['id'];
                                                                $user_id = $data['user_id'];
                                                                $api_key = $session['access_token'] . '-' . $data['dc'];
                                                                $api_endpoint = $data['api_endpoint'];
                                                                $accountname = $data['accountname'];
                                                                $login_id = $data['login']['login_id'];
                                                                $login_name = $data['login']['login_name'];
                                                                $login_email = $data['login']['login_email'];
                                                                $inse = $inventoryCont->ConnectMailChimp($customerId, $access_token, $dc, $list_id, $user_id, $api_key, $api_endpoint, $accountname,$login_id,$login_name,$login_email);
                                                               header('location:MailChimp.php');
                                                                ?>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#property').on('change', function () {
            var propertyID = $(this).val();
            if (propertyID) {
                $.ajax({
                    type: 'POST',
                    url: 'property_room.php',
                    data: 'property_id=' + propertyID,
                    success: function (html) {
                        $('#room').html(html);
                        //  $('#city').html('<option value="">Select state first</option>'); 
                    }
                });
            } else {
                $('#room').html('<option value="">Select property first</option>');
                // $('#city').html('<option value="">Select state first</option>'); 
            }
        });

    });
</script>
<script language="JavaScript">
    $('#select-all').click(function (event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }
    });
</script>
<?php // echo '<script>var prop_type = ' ."~".$property . ';</script>';       ?>

<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/autosize/autosize.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<?php
admin_footer();
exit;
?>