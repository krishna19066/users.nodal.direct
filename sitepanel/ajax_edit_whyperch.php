<?php
include "admin-function.php";
$customerId = $_SESSION['customerID'];
$mode = $_SESSION['mode'];
$ajaxclientCont = new staticPageData();
$ajaxpropertyPhotoCont = new propertyData();
$cloud_keySel = $ajaxpropertyPhotoCont->get_Cloud_AdminDetails($customerId);
$cloud_keyData = $cloud_keySel[0];
$cloud_cdnName = $cloud_keyData->cloud_name;
$base_url = $cloud_keyData->website;
?>
<?php
$pageUrl = $_REQUEST['pageUrlData'];
$clientPageData = $ajaxclientCont->getWhyPerchPageData($pageUrl, $customerId);
$pageData = $clientPageData[0];
$numb = count($clientPageData);
$metaTags = $ajaxclientCont->getMetaTagsData($customerId, $pageUrl);
$metaRes = count($metaTags);
$mets_res = $metaTags[0];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo ajaxCssHeader(); ?>    
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php
        themeheader();
        ?>        
        <div class="clearfix"> </div>       
        <div class="page-container">
            <?php
            admin_header();
            ?>
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#e9ecf3;">
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1> Why US PAGE DETAILS
                                <small>View / edit all the details of your static page</small>
                            </h1>
                        </div>
                    </div>

                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="welcome.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="welcome.php">Home & Static Page</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Edit Contact us Page Details</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Edit Why us Page </span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-body ">
                                                <div style="width:100%;" class="clear"> 
                                                    <a class="pull-right">
                                                        <a href="manage-static-pages.php"><button style="background:#36c6d3;color:white;border:none;height:35px;width:160px;font-size:14px; float: right;margin-top: -21px;"><i class="fa fa-eye"></i> &nbsp View All Page</button></a>
                                                    </a>
                                                    <span style="float:left; margin-top: -20px;"><input type="button" style="background:#ff9800;color:white;border:none;height:35px;width:180px;font-size:14px;" data-html="true"  onclick="$('#add_meta').slideToggle();" value="Add Meta Tags" /></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ----------Start FORM------------------------------------->
                                <div class="portlet light bordered" id="add_meta" style="display:none;">	
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" name="register_member_form" method="post" enctype="multipart/form-data" action="../sitepanel/updatePage_content.php" style="display:inline;" onsubmit="return validate_register_member_form();">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Meta Title
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="MetaTitle" value="<?php echo $mets_res->MetaTitle; ?>" />
                                                        <span class="help-block"> Provide Meta Title </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Meta Description</label>
                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control" rows="3" name="MetaDisc"><?php echo $mets_res->MetaDisc; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <?php if ($metaRes > 0) { ?>
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $mets_res->filename; ?>" />
                                                    <input type="hidden" class="form-control" name="metano" value="<?php echo $mets_res->metano; ?>" />                                                     
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_whyperch" />
                                                    <input type="submit" class="btn green" name="update_meta_tags" class="button" id="submit" value="Update Meta Tag" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>/>
                                                <?php } else { ?>
                                                    <input type="hidden" class="form-control" name="pagename" value="ajax_edit_whyperch" />
                                                    <input type="hidden" class="form-control" name="filename" value="<?php echo $pageUrl; ?>" />
                                                    <input type="submit" class="btn green" name="add_meta_tags" class="button" id="submit" value="Add Meta Meta Tag" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                                <!-- END FORM-->

                                <!---------------------------------------------------------------- Add Meta Tag Section Ends ------------------------------------------------------------->                           
                                <div class="portlet-body form">
                                    <span style="float:right;"><a href="<?php echo $base_url; ?>/<?php echo $pageUrl; ?>.html" target="_blank"><input type="button" style="background:#03A9F4;color:white;border:none;height:35px;width:180px;font-size:14px;margin-right: 11px;margin-top: 7px;" data-html="true"  value="Preview Page" /></a></span><br/><br/>
                                    <form class="form-horizontal" name="formn" method="post" action="update_whyus.php" enctype="multipart/form-data">
                                        <div class="form-body">

                                            <div class="expDiv" onclick="$('#general_content').slideToggle();" style="cursor: pointer; border: 1px solid #ececec"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Icon Boxes(Required)</h3></div>

                                            <div id="general_content" style="display:none;"><br/>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 1
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h1" value="<?php echo $pageData->h1; ?>" required />
                                                        <input type="hidden" name="pageUrl" value="<?php echo $pageUrl; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Header Image
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px; align:center;">
                                                            <?php if ($pageData->header_img == '') { ?>
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                            <?php } else { ?>
                                                                <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/staticpage/<?php echo $pageData->header_img; ?>.jpeg" alt=""/>    
                                                            <?php } ?>
                                                        </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="image_header"> </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="well" onclick="$('#content_box_1').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-danger"><span class="label label-danger pull-right">Edit</span><i class="fa fa-thumbs-up fa_line_height"></i> Box - 1 </h4>
                                                                </div>
                                                                <hr/>
                                                                <div id="content_box_1" style="display:none;">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Box 1 Icon Path
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="boxicon1" value="<?php echo html_entity_decode(htmlspecialchars($pageData->box1_icon)); ?>" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Box 1 Heading
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="boxhead1" value="<?php echo $pageData->box1_head; ?>" />
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3"> Box 1 Content
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <textarea class="ckeditor form-control"  name="boxcontent1" rows="6" required ><?php echo html_entity_decode($pageData->box1_content); ?></textarea>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="well" onclick="$('#content_box_2').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-success"><span class="label label-success pull-right">Edit</span><i class="fa fa-map-marker fa_line_height"></i> Box - 2 </h4>                                                                  
                                                                </div>                                                           
                                                                <hr/>
                                                                <div id="content_box_2" style="display:none;">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Box 2 Icon Path
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="boxicon2" value="<?php echo html_entity_decode(htmlspecialchars($pageData->box2_icon)); ?>" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Box 2 Heading
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="boxhead2" value="<?php echo $pageData->box2_head; ?>" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3"> Box 2 Content
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <textarea class="ckeditor form-control"  name="boxcontent2" rows="6" required ><?php echo html_entity_decode($pageData->box2_content); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="well" onclick="$('#content_box_3').slideToggle();" style="cursor:pointer;">
                                                                    <h4 class="text-primary"><span class="label label-primary pull-right">Edit</span><i class="fa fa-cutlery  fa_line_height"></i> Box - 3 </h4>
                                                                </div>
                                                                <hr/>
                                                                <div id="content_box_3" style="display:none;">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Box 3 Icon Path
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="boxicon3" value="<?php echo html_entity_decode(htmlspecialchars($pageData->box3_icon)); ?>" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3"> Box 3 Heading
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" name="boxhead3" value="<?php echo $pageData->box3_head; ?>"  />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3"> Box 3 Content
                                                                            <span class="required"> * </span>
                                                                        </label>

                                                                        <div class="col-md-9">
                                                                            <textarea class="ckeditor form-control"  name="boxcontent3" rows="6" required ><?php echo html_entity_decode($pageData->box3_content); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div><!--/row-->    
                                                    </div><!--/col-12-->
                                                </div><!--/row-->
                                            </div>
                                            <div class="expDiv" onclick="$('#box_img').slideToggle();" style="cursor: pointer; border: 1px solid #ececec"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Images Boxed  </h3></div>

                                            <div id="box_img" style="display:none;"><br/>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"> Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" name="h2" value="<?php echo $pageData->h2; ?>"  />
                                                    </div>
                                                </div>

                                                <div class="form-group last">
                                                    <label class="control-label col-md-3"> Subheading for Heading 2
                                                        <span class="required"> * </span>
                                                    </label>

                                                    <div class="col-md-9">
                                                        <textarea class="ckeditor form-control"  name="subhead2" rows="6" required ><?php echo html_entity_decode($pageData->subhead2); ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <!-----------------------------------------------Image Box -1 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_1').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image Box -1 </h4><hr/>
                                                            <center> <?php if ($pageData->imgbox_h1) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h1; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img1 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img1; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content1) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content1); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-danger pull-center">Edit Content</span> </h4><hr/>
                                                        </div>
                                                        <div id="image_box_1" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 1
                                                                    <span class="required"> * </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh1" value="<?php echo $pageData->imgbox_h1; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 1
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img1 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img1; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg1"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 1
                                                                    <span class="required"> * </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent1" rows="6" required ><?php echo html_entity_decode($pageData->imgbox_content1); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -1 End ----------------------------->
                                                    <!-----------------------------------------------Image Box -2 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_2').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image - 2 </h4><hr/>                                                          
                                                            <center> <?php if ($pageData->imgbox_h2) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h2; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img2 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img2; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content2) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content2); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-success pull-center">Edit Content</span> </h4><hr/>                                                                                                                      
                                                        </div>
                                                        <div id="image_box_2" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 2
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh2" value="<?php echo $pageData->imgbox_h2; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 2
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img2 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img2; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg2"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 2
                                                                    <span class="required"> * </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent2" rows="6" required ><?php echo html_entity_decode($pageData->imgbox_content2); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -2 END ----------------------------->


                                                    <!-----------------------------------------------Image Box -3 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_3').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image Box -3 </h4><hr/>
                                                            <center> <?php if ($pageData->imgbox_h3) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h3; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img3 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img3; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content3) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content3); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-danger pull-center">Edit Content</span> </h4><hr/>
                                                        </div>
                                                        <div id="image_box_3" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 3
                                                                    <span class="required"></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh3" value="<?php echo $pageData->imgbox_h3; ?>"  />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 3
                                                                    <span class=""> </span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img3 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img3; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg3"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 3
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent3" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content3); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -3 End ----------------------------->
                                                    <!-----------------------------------------------Image Box -4 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_4').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image - 4 </h4><hr/>                                                          
                                                            <center> <?php if ($pageData->imgbox_h4) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h4; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img4 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img4; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content4) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content4); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-success pull-center">Edit Content</span> </h4><hr/>                                                                                                                      
                                                        </div>
                                                        <div id="image_box_4" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 4
                                                                    <span class="required"></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh4" value="<?php echo $pageData->imgbox_h4; ?>" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 4
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img4 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img4; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg4"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 4
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent4" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content4); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -4 END ----------------------------->

                                                    <!-----------------------------------------------Image Box -5 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_5').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image Box -5 </h4><hr/>
                                                            <center> <?php if ($pageData->imgbox_h5) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h5; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img5 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img5; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content5) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content5); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-danger pull-center">Edit Content</span> </h4><hr/>
                                                        </div>
                                                        <div id="image_box_5" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 5
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh5" value="<?php echo $pageData->imgbox_h5; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 5
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img5 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img5; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg5"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 5
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent5" rows="6" ><?php echo html_entity_decode($pageData->imgbox_content5); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -5 End ----------------------------->
                                                    <!-----------------------------------------------Image Box -6 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_6').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image - 6 </h4><hr/>                                                          
                                                            <center> <?php if ($pageData->imgbox_h6) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h6; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img6 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img6; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content6) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content6); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-success pull-center">Edit Content</span> </h4><hr/>                                                                                                                      
                                                        </div>
                                                        <div id="image_box_6" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 6
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh6" value="<?php echo $pageData->imgbox_h6; ?>" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 6
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img6 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img6; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg6"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 6
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent6" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content6); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -6 END----------------------------->

                                                    <!-----------------------------------------------Image Box -7 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_7').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image Box -7 </h4><hr/>
                                                            <center> <?php if ($pageData->imgbox_h7) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h7; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img7 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img7; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content7) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content7); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-danger pull-center">Edit Content</span> </h4><hr/>
                                                        </div>
                                                        <div id="image_box_7" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 7
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh7" value="<?php echo $pageData->imgbox_h7; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 7
                                                                    <span class=""> </span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img7 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img7; ?>.jpeg" alt=""/>    
                                                                        <?php } ?> 
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg7"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 7
                                                                    <span class="">  </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent7" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content7); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -7 End ----------------------------->
                                                    <!-----------------------------------------------Image Box -8 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_8').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image - 8 </h4><hr/>                                                          
                                                            <center> <?php if ($pageData->imgbox_h8) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h8; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img8 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img8; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content8) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content8); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-success pull-center">Edit Content</span> </h4><hr/>                                                                                                                      
                                                        </div>
                                                        <div id="image_box_8" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 8
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh8" value="<?php echo $pageData->imgbox_h8; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 8
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img8 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img8; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg8"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 8
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent8" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content8); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -8 END----------------------------->
                                                    <!-----------------------------------------------Image Box -9 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_9').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image Box -9 </h4><hr/>
                                                            <center> <?php if ($pageData->imgbox_h9) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h9; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img9 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img9; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content9) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content9); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-danger pull-center">Edit Content</span> </h4><hr/>
                                                        </div>
                                                        <div id="image_box_9" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 9
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh9" value="<?php echo $pageData->imgbox_h9; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 9
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                                                        <?php if ($pageData->imgbox_img9 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img9; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg9"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 9
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent9" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content9); ?></textarea>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -9 End ----------------------------->
                                                    <!-----------------------------------------------Image Box -10 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_10').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image - 10 </h4><hr/>                                                          
                                                            <center> <?php if ($pageData->imgbox_h10) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h10; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img10 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img10; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content10) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content10); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-success pull-center">Edit Content</span> </h4><hr/>                                                                                                                      
                                                        </div>
                                                        <div id="image_box_10" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 10
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" name="imgboxh10" value="<?php echo $pageData->imgbox_h10; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 10
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img10 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img10; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg10"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 10
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent10" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content10); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -10 END----------------------------->
                                                     <!-----------------------------------------------Image Box -11 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_11').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-danger"><span class="label label-danger pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image Box -11 </h4><hr/>
                                                            <center> <?php if ($pageData->imgbox_h11) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h11; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img11 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img11; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content11) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content11); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-danger pull-center">Edit Content</span> </h4><hr/>
                                                        </div>
                                                        <div id="image_box_11" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 11
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="imgboxh11" value="<?php echo $pageData->imgbox_h11; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 11
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                                                        <?php if ($pageData->imgbox_img11 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img11; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg11"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 11
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent11" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content11); ?></textarea>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -11 End ----------------------------->
                                                    <!-----------------------------------------------Image Box -12 ----------------------------->
                                                    <div class="col-md-6">
                                                        <div class="well" onclick="$('#image_box_12').slideToggle();" style="cursor:pointer;height: 570px;">
                                                            <h4 class="text-success"><span class="label label-success pull-right">Edit Content</span><i class="fa fa-thumbs-up fa_line_height"></i> Image - 12 </h4><hr/>                                                          
                                                            <center> <?php if ($pageData->imgbox_h12) { ?>                                                          
                                                                    <h3><strong><?php echo $pageData->imgbox_h12; ?></strong></h3>
                                                                <?php } else { ?>
                                                                    <h3><strong>Heading Here...</strong></h3>
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_img12 == '') { ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                <?php } else { ?>
                                                                    <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img2; ?>.jpeg" alt="" style="width:100%;height: 250px;"/>    
                                                                <?php } ?><br/>
                                                                <?php if ($pageData->imgbox_content12) { ?>                                                          
                                                                    <span><?php echo html_entity_decode($pageData->imgbox_content12); ?></span>
                                                                <?php } else { ?>
                                                                    <span><p>some text here..</p></span>
                                                                <?php } ?></center>
                                                            <h4 class="text-success"><span class="label label-success pull-center">Edit Content</span> </h4><hr/>                                                                                                                      
                                                        </div>
                                                        <div id="image_box_12" style="display:none;"><br/>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Heading 12
                                                                    <span class=""></span>
                                                                </label>

                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" name="imgboxh12" value="<?php echo $pageData->imgbox_h12; ?>"  />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"> Image Box Image 12
                                                                    <span class=""></span>
                                                                </label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <?php if ($pageData->imgbox_img12 == '') { ?>
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                                        <?php } else { ?>
                                                                            <img src="http://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/c_fill/reputize/whyperch/<?php echo $pageData->imgbox_img12; ?>.jpeg" alt=""/>    
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="imgboximg12"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group last">
                                                                <label class="control-label col-md-3"> Image Box Content 12
                                                                    <span class=""> </span>
                                                                </label>

                                                                <div class="col-md-9">
                                                                    <textarea class="ckeditor form-control"  name="imgboxcontent12" rows="6"  ><?php echo html_entity_decode($pageData->imgbox_content12); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-----------------------------------------------Image Box -12 END----------------------------->
                                                </div><!--/row-->  

                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <?php
                                                        if ($numb == "0") {
                                                            ?>
                                                            <input type="hidden" name="savecont" value="Add Content">
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <input type="hidden" name="savecont" value="Save Content">
                                                            <?php
                                                        }
                                                        ?>

                                                        <input type="submit" name="whyperchsub" class="btn green" id="submit" value="Save Content" <?php if($mode == 'V'){ ?> disabled <?php } ?> <?php if($mode == 'V'){ ?> title="only view mode" <?php } ?>>
                                                    </div>
                                                </div>
                                            </div>                                    
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <?php echo ajaxJsFooter(); ?>
</body>
</html>
