<?php
include "cms_functions.php";
user_locked();
staff_restriction();
?>

<?php
include_once("../include/main-inc.php");
check_session('MyCpanel', 'index.php');
/* -------------------------------- CustomerID Updation ------------------------------------------- */
$custID = $_SESSION['customerID'];
@extract($_REQUEST);
?>

<?php
$sel = "select * from static_page_contactus where customerID='$custID'";
$quer = mysql_query($sel);
$numb = mysql_num_rows($quer);
$contactus = mysql_fetch_array($quer);
?>

<?php
if (isset($_POST['contactussave'])) {
    $type = $_POST['typeofsave'];

    $mapurl = $_POST['mapurl'];
    $tab1name = $_POST['tab1name'];
    $tab1heading = $_POST['tab1heading'];
    $tab1form = $_POST['tab1form'];
    $tab1subhead = $_POST['tab1subhead'];
    $tab1content = $_POST['tab1content'];

    $box1head = $_POST['box1head'];
    $box1content = $_POST['box1content'];
    $box2head = $_POST['box2head'];
    $box2content = $_POST['box2content'];
    $box3head = $_POST['box3head'];
    $box3content = $_POST['box3content'];
    $box4head = $_POST['box4head'];
    $box4content = $_POST['box4content'];

    if ($type == "newaddition") {
        $newins = "insert into static_page_contactus(customerID,map_url,tab1_name,tab1_heading,tab1_formName,tab1_subheading,tab1_content,address1head,address1,address2head,address2,emailhead,email,phonehead,phone) values('$custID','$mapurl','$tab1name','$tab1heading','$tab1form','$tab1subhead','$tab1content','$box1head','$box1content','$box2head','$box2content','$box3head','$box3content','$box4head','$box4content')";

        $newinsquery = mysql_query($newins);
        echo "<script> window.location='edit_contactus.php'; </script>";
        exit;
    }

    if ($type == "updation") {
        $updins = "update static_page_contactus set map_url='$mapurl',tab1_name='$tab1name',tab1_heading='$tab1heading',tab1_formName='$tab1form',tab1_subheading='$tab1subhead',tab1_content='$tab1content',address1head='$box1head',address1='$box1content',address2head='$box2head',address2='$box2content',emailhead='$box3head',email='$box3content',phonehead='$box4head',phone='$box4content' where customerID='$custID'";

        $updquery = mysql_query($updins);
        echo "<script> window.location='edit_contactus.php'; </script>";
        exit;
    }
}
?>
<html>
    <head>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">

            <!-- BEGIN CONTAINER -->

            <?php
            themeheader();
            ?>
            <div class="page-container">
                <?php
                admin_header();
                ?>

                <div class="page-content-wrapper">

                    <!-- BEGIN CONTENT BODY -->

                    <div class="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Edit Contact Us Page</span>

                                            <span class="caption-helper">Add/Edit Contact Us Page Here..</span>

                                            <span class="fa fa-question-circle popovers" aria-hidden="true" data-container="body" data-trigger="hover" data-placement="right" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in right" style="color:#7d6c0b;font-size:20px;"></span>
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="form-horizontal" name="formn" method="post" action="" enctype="multipart/form-data">
                                            <div class="form-body">

                                                <div class="expDiv" onclick="$('#general_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> General Contents (Required)</h3></div>

                                                <div id="general_content" style="display:none;">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Map URL
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="mapurl" value="<?php echo $contactus['map_url']; ?>" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab 1 Name
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="tab1name" value="<?php echo $contactus['tab1_name']; ?>" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab 1 Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="tab1heading" value="<?php echo $contactus['tab1_heading']; ?>" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab 1 Form Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="tab1form" value="<?php echo $contactus['tab1_formName']; ?>" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Tab 1 Sub-Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="tab1subhead" value="<?php echo $contactus['tab1_subheading']; ?>" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Tab 1 Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="tab1content" rows="6" required ><?php echo html_entity_decode($contactus['tab1_content']); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="expDiv" onclick="$('#box_content').slideToggle();"><i class="fa fa-plus pull-left" aria-hidden="true" ></i><h3> Boxed Contents </h3></div>

                                                <div id="box_content" style="display:none;">

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Box 1 Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="box1head" value="<?php echo $contactus['address1head']; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Box 1 Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="box1content" rows="6" required ><?php echo html_entity_decode($contactus['address1']); ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Box 2 Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="box2head" value="<?php echo $contactus['address2head']; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Box 2 Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="box2content" rows="6" required ><?php echo html_entity_decode($contactus['address2']); ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Box 3 Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="box3head" value="<?php echo $contactus['emailhead']; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Box 3 Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="box3content" rows="6" required ><?php echo html_entity_decode($contactus['email']); ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3"> Box 4 Heading
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control" name="box4head" value="<?php echo $contactus['phonehead']; ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3"> Box 4 Content
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control"  name="box4content" rows="6" required ><?php echo html_entity_decode($contactus['phone']); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <?php
                                                            if ($numb == "0") {
                                                                ?>
                                                                <input type="hidden" name="typeofsave" value="newaddition" />
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="hidden" name="typeofsave" value="updation" />
                                                                <?php
                                                            }
                                                            ?>
                                                            <input type="submit" name="contactussave" class="btn green" id="submit" value="Save">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
<?php
admin_footer();
exit;
?>